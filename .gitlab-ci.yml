workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^release\//

include:
  local: build-scripts/gitlab.common.yaml

stages:
  - build
  - test
  - publish
  - deploy
  - automation

build:
  stage: build
  needs: []
  script:
    - ./gradlew assemble

prepare-children-pipelines:
  stage: build
  script:
    - ./gradlew :generateChildrenPipelines
  cache:
    paths:
      - .gradle/wrapper/
  artifacts:
    paths:
      - child-pipeline.*.yaml

test:
  stage: test
  needs: ['build', 'prepare-children-pipelines']
  trigger:
    strategy: depend
    include:
      - artifact: child-pipeline.check.yaml
        job: prepare-children-pipelines

docker:
  stage: publish
  needs: ['build', 'prepare-children-pipelines']
  trigger:
    strategy: depend
    include:
      - artifact: child-pipeline.buildkitPushRelease.yaml
        job: prepare-children-pipelines

build Testing APK:
  image: $CI_REGISTRY/salutdoc/cicd-image:android
  stage: publish
  needs:
    - build
  rules:
    - when: manual
      allow_failure: true
  artifacts:
    paths:
      - apps/mobile-native/android/app/build/outputs/apk/development/release/app-development-release.apk
  script:
    - >-
      ./gradlew apps:mobile-native:androidFastlaneTestingApk
      -Pandroid.upload.key.password="$ANDROID_UPLOAD_KEY_PASSWORD"
      -Pandroid.upload.keystore.password="$ANDROID_UPLOAD_KEYSTORE_PASSWORD"
      -Pandroid.upload.json_key="$ANDROID_UPLOAD_JSON_KEY"

build Production APK:
  image: $CI_REGISTRY/salutdoc/cicd-image:android
  stage: publish
  needs:
    - build
  rules:
    - when: manual
      allow_failure: true
  artifacts:
    paths:
      - apps/mobile-native/android/app/build/outputs/apk/production/release/app-production-release.apk
  script:
    - >-
      ./gradlew apps:mobile-native:androidFastlaneProductionApk
      -Pandroid.upload.key.password="$ANDROID_UPLOAD_KEY_PASSWORD"
      -Pandroid.upload.keystore.password="$ANDROID_UPLOAD_KEYSTORE_PASSWORD"
      -Pandroid.upload.json_key="$ANDROID_UPLOAD_JSON_KEY"

.deploy-server:
  image: $CI_REGISTRY/salutdoc/cicd-image:helm
  stage: deploy
  needs: ['docker']
  cache:
    paths: []
  before_script: []

deploy to Test:
  extends: .deploy-server
  environment:
    name: test
    kubernetes:
      namespace: default
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9.]+$/'
      variables:
        IMAGE_TAG: "$CI_COMMIT_TAG"
      when: manual
      allow_failure: true
    - variables:
        IMAGE_TAG: "$CI_COMMIT_SHA"
      when: manual
      allow_failure: true
  script:
    - >-
      helm upgrade
      --install
      --values k8s/test.yaml
      --set global.image.tag=$IMAGE_TAG
      salutdoc
      ./k8s/helm/salutdoc

deploy to Production:
  extends: .deploy-server
  environment:
    name: production
    kubernetes:
      namespace: default
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9.]+$/'
      when: manual
      allow_failure: true
  script:
    - >-
      helm upgrade
      --install
      --values k8s/production.yaml
      --set global.image.tag=$CI_COMMIT_TAG
      salutdoc
      ./k8s/helm/salutdoc

deploy Android App to Google Play:
  image: $CI_REGISTRY/salutdoc/cicd-image:android
  stage: deploy
  needs:
    - build
  artifacts:
    paths:
      - apps/mobile-native/android/app/build/outputs/bundle/production/release/app-production-release.aab
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9.]+$/'
      when: on_success
  script:
    - >-
      ./gradlew apps:mobile-native:androidFastlaneDeploy
      -Pandroid.upload.key.password="$ANDROID_UPLOAD_KEY_PASSWORD"
      -Pandroid.upload.keystore.password="$ANDROID_UPLOAD_KEYSTORE_PASSWORD"
      -Pandroid.upload.json_key="$ANDROID_UPLOAD_JSON_KEY"

deploy iOS App to Testflight:
  stage: deploy
  needs:
    - build
  tags:
    - macos
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9.]+$/'
      when: on_success
  timeout: 2h
  artifacts:
    when: always
    paths:
      - apps/mobile-native/ios/logs/**/*
  variables:
    LC_ALL: "en_US.UTF-8"
    LANG: "en_US.UTF-8"
    LANGUAGE: "en_US.UTF-8"
  script:
    - >-
      IOS_BUILD_NUMBER=$(curl -s -f
      --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}"
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/variables/IOS_BUILD_NUMBER"
      | jq  -r '.value'
      )
    - let IOS_BUILD_NUMBER=IOS_BUILD_NUMBER+1
    - >-
      curl -s -f
      --request PUT
      --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}"
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/variables/IOS_BUILD_NUMBER"
      --form "value=${IOS_BUILD_NUMBER}"
    - security unlock-keychain -p salutdoc login.keychain
    - >-
      ./gradlew apps:mobile-native:iosFastlaneProductionAlpha
      -Pios.build.number="$IOS_BUILD_NUMBER"

.git-automation:
  image: alpine
  stage: automation
  needs: []
  cache:
    paths: []
  before_script:
    - apk add --no-cache git curl openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git remote set-url origin git@gitlab.com:$CI_PROJECT_PATH
    - git config --global user.email "salutdoc-agent@yandex.ru"
    - git config --global user.name "Agent SalutDoc"

create release:
  extends: .git-automation
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        BUMP: minor
      when: manual
      allow_failure: true
    - if: $CI_COMMIT_BRANCH =~ /^release\//
      variables:
        BUMP: patch
      when: manual
      allow_failure: true
  script:
    - PREV_VERSION_TAG=$(git describe --abbrev=0 --first-parent --match v\*)
    - NEXT_VERSION=$(./build-scripts/bin/version-bump ${PREV_VERSION_TAG:1} $BUMP)
    - git tag -a v$NEXT_VERSION -m "Version $NEXT_VERSION"
    - git push origin v$NEXT_VERSION
    - |
      if [[ $BUMP == "minor" ]];
      then
        git branch release/$NEXT_VERSION
        git push origin release/$NEXT_VERSION
      fi

cascade merge:
  extends: .git-automation
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^release\//
      when: always
  script:
    - git fetch origin $CI_DEFAULT_BRANCH:$CI_DEFAULT_BRANCH $CI_COMMIT_BRANCH:$CI_COMMIT_BRANCH
    - git checkout $CI_DEFAULT_BRANCH
    - |
      if git merge --no-ff --no-edit $CI_COMMIT_BRANCH;
      then
        git push origin $CI_DEFAULT_BRANCH
      else
        curl \
          --request POST \
          --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
          --form "source_branch=$CI_COMMIT_BRANCH" \
          --form "target_branch=$CI_DEFAULT_BRANCH" \
          --form "title=$CI_COMMIT_BRANCH cascade merge conflict" \
          --form "assignee_id=$GITLAB_USER_ID" \
          https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests
      fi
