const packageOverrides = {
  '@react-native-community/datetimepicker': (pkg) => {
    makePeersOptional(pkg, 'react-native-windows')
  },
  '@apollo/react-hooks': (pkg) => {
    makePeersOptional(pkg, 'react-dom')
  },
  'react-use': (pkg) => {
    transformToPeers(pkg, 'nano-css')
    makePeersOptional(pkg, 'nano-css', 'react-dom')
  },
  '@aws-sdk/middleware-retry': (pkg) => {
    transformToPeers(pkg, 'react-native-get-random-values')
    makePeersOptional(pkg, 'react-native-get-random-values')
  }
}

function transformToPeers (pkg, ...deps) {
  for (const dep of deps) {
    pkg.peerDependencies[dep] = pkg.dependencies[dep]
    delete pkg.dependencies[dep]
  }
}

function makePeersOptional (pkg, ...deps) {
  for (const dep of deps) {
    pkg.peerDependenciesMeta = {
      ...pkg.peerDependenciesMeta,
      [dep]: {
        optional: true
      }
    }
  }
}

module.exports = {
  hooks: {
    readPackage (pkg, ctx) {
      if (pkg.name in packageOverrides) {
        packageOverrides[pkg.name](pkg, ctx)
      }
      return pkg
    }
  }
}
