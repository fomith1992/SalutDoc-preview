import { QueryResult } from '@apollo/react-common'

import { ApolloError } from 'apollo-client'

export function dispatch<TData, TVariables, TResult> (
  result: QueryResult<TData, TVariables>,
  callbacks: {
    onLoading: (result?: QueryResult<TData, TVariables>) => TResult
    onError: (error: ApolloError, result: QueryResult<TData, TVariables>) => TResult
    onSuccess: (data: TData, result: QueryResult<TData, TVariables>) => TResult
  }
): TResult {
  if (result.loading) {
    return callbacks.onLoading(result)
  }
  if (result.error != null) {
    return callbacks.onError(result.error, result)
  }
  return callbacks.onSuccess(result.data as TData, result)
}

export type Nullable<T> = {
  [P in keyof T]?: T[P] | null
}

export function expectDefined<T> (val: T): NonNullable<T> {
  if (val == null) {
    throw Error(`Expected 'val' to be defined, but received ${String(val)}`)
  }
  return val as NonNullable<T>
}

export function assertDefined<T> (val: T): asserts val is NonNullable<T> {
  expectDefined(val)
}

type NullsOf<T> = Extract<T, null | undefined>

export function flatMap<T, R> (value: T, mapper: (value: NonNullable<T>) => R): R | NullsOf<T> {
  return value == null ? value as NullsOf<T> : mapper(value as NonNullable<T>)
}
