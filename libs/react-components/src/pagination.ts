import _ from 'lodash'
import { DependencyList, Dispatch, useCallback, useMemo, useState } from 'react'
import { flatMap } from './util'

export function mergeEdges<TData extends { edges: readonly any[] } | null | undefined> (lhs: TData, rhs: TData): TData {
  if (lhs == null || lhs.edges.length === 0 || rhs == null || rhs.edges.length === 0) {
    return _.isEmpty(lhs?.edges) ? rhs : lhs
  }
  return {
    ...rhs,
    edges: [...lhs.edges, ...rhs.edges]
  }
}

export function appendEdges<T extends { edges: readonly any[] }> (lhs: T, rhs: T | null | undefined): T {
  if (rhs == null) {
    return lhs
  }
  return {
    ...rhs,
    edges: [...lhs.edges, ...rhs.edges]
  }
}

export interface Connection<T> {
  edges: Array<{
    node: T
  }>
}

export interface ForwardConnection<T> extends Connection<T> {
  pageInfo: {
    endCursor: string | null
    hasNextPage: boolean
  }
}

export interface BackwardConnection<T> extends Connection<T> {
  pageInfo: {
    startCursor: string | null
    hasPreviousPage: boolean
  }
}

export interface CursorPagination<T extends Connection<any>> {
  connection?: T
  hasMore: boolean
  loadMore: () => Promise<T>

  loadingMore: boolean
  loadingInitial: boolean
  loading: boolean
}

function useDependentState<T> (initialState: T, deps?: DependencyList): [T, Dispatch<T>] {
  const token = useMemo(() => ({}), deps ?? [])
  const [wrapper, setWrapper] = useState({
    state: initialState,
    token
  })
  const setState = useCallback((state: T): void => {
    setWrapper({
      state,
      token
    })
  }, [token])
  if (wrapper.token === token) {
    return [wrapper.state, setState]
  } else {
    return [initialState, setState]
  }
}

export function useCursorPagination<T extends Connection<any>> (
  options: {
    prepend?: T['edges']
    append?: T['edges']
    initial?: {
      data?: T | null
      loading: boolean
    }
    queryNext: (current?: T) => Promise<T>
    merge: (current: T, next: T) => T
    hasMore: (current: T) => boolean
  },
  deps?: DependencyList
): CursorPagination<T> {
  const { prepend, append, initial, queryNext, merge, hasMore } = options
  const [state, setState] = useDependentState<{
    connection?: T
    loadPromise?: Promise<T>
  }>({}, deps)

  const { connection, loadPromise } = state
  const loadingInitial = connection == null && initial != null && initial.loading
  const effective = connection ?? initial?.data ?? undefined

  let loadMore: () => Promise<T>
  if (loadingInitial) {
    loadMore = async () => await Promise.reject(new Error('initial page is still loading'))
  } else if (loadPromise != null) {
    loadMore = async () => await loadPromise
  } else {
    loadMore = async (): Promise<T> => {
      const loadPromise = queryNext(effective)
        .then(
          page => {
            const connection = effective == null ? page : merge(effective, page)
            setState({ connection })
            return connection
          },
          (reason) => {
            setState({ connection })
            throw reason
          }
        )
      // TODO not sure what if query returns resolved proxy
      // TODO may setLoadPromise(undefined) have already be called?
      setState({
        connection,
        loadPromise
      })
      return loadPromise
    }
  }

  return {
    connection: flatMap(effective, connection => ({
      ...connection,
      edges: [...prepend ?? [], ...connection.edges, ...append ?? []]
    })),
    loadingInitial: loadingInitial,
    loadingMore: loadPromise != null,
    loading: loadingInitial || loadPromise != null,
    hasMore: !loadingInitial && (effective == null || hasMore(effective)),
    loadMore
  }
}

export function useForwardCursorPagination<T extends ForwardConnection<any>> (
  options: {
    prepend?: T['edges']
    initial?: {
      data?: T | null
      loading: boolean
    }
    query: (after: string | null) => Promise<T>
  },
  deps?: DependencyList
): CursorPagination<T> {
  const { prepend, initial, query } = options
  return useCursorPagination({
    prepend,
    initial,
    queryNext: async current => await query(current?.pageInfo.endCursor ?? null),
    merge: (current, next) => ({
      ...next,
      edges: [...current.edges, ...next.edges]
    }),
    hasMore: current => current.pageInfo.hasNextPage
  }, deps)
}

export function useBackwardCursorPagination<T extends BackwardConnection<any>> (
  options: {
    append?: T['edges']
    initial?: {
      data?: T | null
      loading: boolean
    }
    query: (before: string | null) => Promise<T>
  },
  deps?: DependencyList
): CursorPagination<T> {
  const { append, initial, query } = options
  return useCursorPagination({
    append,
    initial,
    queryNext: async current => await query(current?.pageInfo.startCursor ?? null),
    merge: (current, next) => ({
      ...next,
      edges: [...next.edges, ...current.edges]
    }),
    hasMore: current => current.pageInfo.hasPreviousPage
  }, deps)
}
