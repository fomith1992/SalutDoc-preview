import { FORM_ERROR, FormApi, FormState, FormSubscription } from 'final-form'
import React, { useCallback, useMemo } from 'react'
import { Form, useField, useForm, useFormState, FormSpy } from 'react-final-form'
import * as yup from 'yup'
import { assertDefined } from './util'

export { FORM_ERROR } from 'final-form'

export interface FieldState<V> {
  value: V | undefined
  dirty: boolean // value has changed
  touched: boolean // field has been visited
  error: string | null // field validation error
  active: boolean // field is currently focused
  submitting: boolean // form is being submitted
}

export interface FieldHandlers<V> {
  name: string
  onChange: (value: V | undefined) => void
  onBlur: () => void
  onFocus: () => void
}

type FieldStateSubscription = {
  [P in keyof FieldState<any>]?: true
}

type DefaultFieldStateSubscription = {
  [P in keyof FieldState<any>]: true
}

const defaultFieldStateSubscription = {
  value: true,
  submitting: true,
  dirty: true,
  touched: true,
  active: true,
  error: true
}

type PickEnabledFieldSubscriptions<V, S extends FieldStateSubscription> = Pick<FieldState<V>, {
  [P in keyof FieldState<V>]: S[P] extends true ? P : never
}[keyof FieldState<V>]>

type SubmissionErrors<T extends object> = {
  [K in keyof T]?: string
} & {
  [FORM_ERROR]?: string
}

export type SubmissionHandler<T extends object> = (data: T) => SubmissionErrors<T> | Promise<SubmissionErrors<T>>

export function hasErrors (errors: SubmissionErrors<any>): boolean {
  return Object.keys(errors).length !== 0
}

export interface Formatter<I, O> {
  format: (value: I | undefined) => O

  parse: (value: O) => I | undefined
}

export type FormDescriptor<T extends object> = {
  Form: React.ComponentType<{
    initialValues?: Partial<T>
    onSubmit: SubmissionHandler<T>
    children?: React.ReactNode

    onSubmitted?: (errors: SubmissionErrors<T>, formApi: FormApi<T>) => void
  }>

  FormSpy: React.ComponentType<{
    subscription?: FormSubscription
    onChange: (formState: FormState<T>) => void
  }>

  Field: <N extends keyof T, V = T[N], S extends FieldStateSubscription = DefaultFieldStateSubscription>(props: {
    name: N
    subscription?: S
    formatter?: Formatter<T[N], V>
    children: (props: FieldHandlers<V> & PickEnabledFieldSubscriptions<V, S>) => React.ReactNode
  }) => React.ReactElement

  Submit: (props: {
    children: (props: {
      valid: boolean
      submitting: boolean
      onSubmit: () => void
    }) => React.ReactNode
  }) => React.ReactElement
}

function handleYupError<T extends object> (e: unknown): SubmissionErrors<T> {
  if (yup.ValidationError.isError(e)) {
    return e.inner
      .map(inner => ({
        [inner.path]: inner.message
      }))
      .reduce(Object.assign, {}) as SubmissionErrors<T>
  } else {
    throw e
  }
}

export function createFormDescriptor<T extends object> (
  schema: yup.ObjectSchema<T>
): FormDescriptor<T> {
  return {
    Form: ({ initialValues, onSubmit, children, onSubmitted }) => {
      const render = useCallback(() => children, [children])
      const formValues = useMemo(() => initialValues, [])
      return (
        <Form<T>
          initialValues={formValues}
          onSubmit={(values, formApi, callback) => {
            // callback is in fact mandatory parameter, this is a bug in final-form types
            assertDefined(callback)

            schema.validate(values, {
              abortEarly: false
            })
              .then(onSubmit, handleYupError)
              .catch(error => {
                console.log('unexpected error while submitting a form', error)
                const message = error instanceof Error ? error.message : 'unknown error'
                return { [FORM_ERROR]: message }
              })
              .then(errors => {
                callback(errors)
                onSubmitted?.(errors, formApi)
              })
              .catch(error => {
                console.log('unexpected error after submitting a form', error)
              })
          }}
          validate={values => {
            try {
              schema.validateSync(values, {
                abortEarly: false
              })
              return {}
            } catch (e) {
              return handleYupError(e)
            }
          }}
          subscription={{ /* nothing */ }}
          render={render}
        />
      )
    },
    Field: ({ name, formatter, subscription = defaultFieldStateSubscription, children }) => {
      const { input, meta } = useField(name as string, {
        subscription: {
          value: subscription.value,
          dirty: subscription.dirty,
          touched: subscription.touched,
          active: subscription.active,
          error: subscription.error,
          submitError: subscription.error,
          submitFailed: subscription.error,
          modifiedSinceLastSubmit: subscription.error as false, // WTF?! (typings are broken)
          submitting: subscription.submitting
        },
        allowNull: true,
        parse: formatter?.parse ?? (value => value),
        format: formatter?.format ?? (value => value)
      })
      const error = meta.submitFailed === true && meta.modifiedSinceLastSubmit === false ? meta.submitError : meta.error
      return (
        <>
          {children({
            name,
            dirty: meta.dirty,
            touched: meta.touched,
            active: meta.active,
            error: error ?? null,
            submitting: meta.submitting,
            value: input.value,
            onBlur: input.onBlur,
            onChange: input.onChange,
            onFocus: input.onFocus
          } as any /* trust me, i'm an engineer */)}
        </>
      )
    },
    Submit: ({ children }) => {
      const { submit } = useForm('Submit')
      const { hasValidationErrors, submitting } = useFormState({
        subscription: {
          hasValidationErrors: true,
          submitting: true
        }
      })
      return (
        <>
          {children({
            valid: !hasValidationErrors,
            submitting,
            onSubmit: submit
          })}
        </>
      )
    },
    FormSpy: ({ subscription, onChange }) => {
      return (
        <FormSpy<T>
          subscription={subscription}
          onChange={onChange}
        />
      )
    }
  }
}
