import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  COMMENTS_SERVICE_URI: t.string
})

export const commentsConfig = registerAs('comments-config', () => t.transformOrThrow(configSchema)(process.env))
