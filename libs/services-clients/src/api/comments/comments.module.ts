import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { commentsServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { commentsConfig } from './comments.config'
import { COMMENTS_CLIENT } from './comments.constants'
import { CommentsService } from './comments.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: COMMENTS_CLIENT,
        imports: [
          ConfigModule.forFeature(commentsConfig)
        ],
        inject: [commentsConfig.KEY],
        useFactory: (config: ConfigType<typeof commentsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.COMMENTS_SERVICE_URI,
            protoPath: commentsServicePath,
            package: 'api.comments',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    CommentsService
  ],
  exports: [
    CommentsService
  ]
})
export class CommentsModule {
}
