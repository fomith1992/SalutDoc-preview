import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { CommentsServiceClient } from '@salutdoc/protos/lib/gen/api/comments'
import { Metadata, status } from 'grpc'
import { COMMENTS_CLIENT } from './comments.constants'
import { Comment, transformComment } from './comments.schema'

@Injectable()
export class CommentsService {
  private readonly comments: CommentsServiceClient

  constructor (@Inject(COMMENTS_CLIENT) comments: ClientGrpc) {
    this.comments = comments.getService('CommentsService')
  }

  async getComment (commentId: string, metadata: Metadata): Promise<Comment | null> {
    try {
      return await this.comments.getComment({
        id: commentId
      }, metadata).toPromise().then(transformComment)
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }
}
