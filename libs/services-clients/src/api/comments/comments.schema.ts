import * as t from '@salutdoc/utils/lib/transform'

const commentSchema = t.type({
  id: t.string,
  author: t.type({
    id: t.string
  })
})

export const transformComment = t.transformOrThrow(commentSchema)

export type Comment = t.OutputOf<typeof commentSchema>
