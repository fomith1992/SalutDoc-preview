import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  QUESTIONS_SERVICE_URI: t.string
})

export const questionsConfig = registerAs('questions-config', () => t.transformOrThrow(configSchema)(process.env))
