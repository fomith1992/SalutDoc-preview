import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const question = pipe(
  schema['.api.questions.Question'],
  validatedShape({
    authorUser: nonnull(),
    createdAt: nonnull()
  })
)

export const answer = t.type({
  id: t.string,
  authorUser: t.type({
    id: t.string
  })
})

export type Question = t.OutputOf<typeof question>

export type Answer = t.OutputOf<typeof answer>
