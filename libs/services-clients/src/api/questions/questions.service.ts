import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { QuestionsServiceClient } from '@salutdoc/protos/lib/gen/api/questions'
import { Metadata, status } from 'grpc'
import { QUESTIONS_CLIENT } from './questions.constants'
import { Answer, Question, answer, question } from './questions.schema'
import * as t from '@salutdoc/utils/lib/transform'

@Injectable()
export class QuestionsService {
  private readonly questions: QuestionsServiceClient

  constructor (@Inject(QUESTIONS_CLIENT) questions: ClientGrpc) {
    this.questions = questions.getService('QuestionsService')
  }

  async getQuestion (questionId: string, metadata?: Metadata): Promise<Question | null> {
    try {
      return await this.questions.getQuestion({
        id: questionId
      }, metadata).toPromise().then(t.transformOrThrow(question))
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }

  async getAnswer (answerId: string): Promise<Answer | null> {
    try {
      return await this.questions.getAnswer({
        id: answerId
      }).toPromise().then(t.transformOrThrow(answer))
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }
}
