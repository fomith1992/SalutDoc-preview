import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, questionsServicePath } from '@salutdoc/protos'
import { questionsConfig } from './questions.config'
import { QUESTIONS_CLIENT } from './questions.constants'
import { QuestionsService } from './questions.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: QUESTIONS_CLIENT,
        imports: [
          ConfigModule.forFeature(questionsConfig)
        ],
        inject: [questionsConfig.KEY],
        useFactory: (config: ConfigType<typeof questionsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.QUESTIONS_SERVICE_URI,
            protoPath: questionsServicePath,
            package: 'api.questions',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    QuestionsService
  ],
  exports: [
    QuestionsService
  ]
})
export class QuestionsModule {
}
