import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { UserFollowServiceClient } from '@salutdoc/protos/lib/gen/api/user-follow'
import * as t from '@salutdoc/utils/lib/transform'
import { Observable } from 'rxjs'
import * as $ from 'rxjs/operators'
import { USER_FOLLOW_CLIENT } from './user-follow.constants'
import { UserFollow, userFollowSchema } from './user-follow.schema'

@Injectable()
export class UserFollowService {
  private readonly userFollow: UserFollowServiceClient

  constructor (@Inject(USER_FOLLOW_CLIENT) userFollow: ClientGrpc) {
    this.userFollow = userFollow.getService('UserFollowService')
  }

  listAllFollowStatuses (data: {
    followerId?: string
    followeeId?: string
  }): Observable<UserFollow> {
    const { followerId, followeeId } = data
    return this.userFollow.listAllFollowStatuses({
      followerId,
      followeeId
    }).pipe(
      $.map(t.transformOrThrow(userFollowSchema))
    )
  }
}
