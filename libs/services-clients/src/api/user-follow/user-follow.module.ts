import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, userFollowServicePath } from '@salutdoc/protos'
import { userFollowConfig } from './user-follow.config'
import { USER_FOLLOW_CLIENT } from './user-follow.constants'
import { UserFollowService } from './user-follow.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: USER_FOLLOW_CLIENT,
        imports: [
          ConfigModule.forFeature(userFollowConfig)
        ],
        inject: [userFollowConfig.KEY],
        useFactory: (config: ConfigType<typeof userFollowConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.USER_FOLLOW_SERVICE_URI,
            protoPath: userFollowServicePath,
            package: 'api.user_follow',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    UserFollowService
  ],
  exports: [
    UserFollowService
  ]
})
export class UserFollowModule {
}
