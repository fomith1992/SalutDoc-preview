import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  USER_FOLLOW_SERVICE_URI: t.string
})

export const userFollowConfig = registerAs('user-follow-config',
  () => t.transformOrThrow(configSchema)(process.env))
