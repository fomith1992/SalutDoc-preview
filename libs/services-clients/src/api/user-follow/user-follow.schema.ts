import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { validatedShape, nonnull } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'

export const userFollowSchema = pipe(
  schema['.api.user_follow.UserFollow'],
  validatedShape({
    follower: nonnull(),
    followee: nonnull(),
    since: nonnull()
  })
)

export type UserFollow = t.OutputOf<typeof userFollowSchema>
