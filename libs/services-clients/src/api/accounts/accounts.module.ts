import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { accountsServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { accountsConfig } from './accounts.config'
import { ACCOUNTS_CLIENT } from './accounts.constants'
import { AccountsService } from './accounts.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: ACCOUNTS_CLIENT,
        imports: [
          ConfigModule.forFeature(accountsConfig)
        ],
        inject: [accountsConfig.KEY],
        useFactory: (config: ConfigType<typeof accountsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.ACCOUNTS_SERVICE_URI,
            protoPath: accountsServicePath,
            package: 'api.accounts',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    AccountsService
  ],
  exports: [
    AccountsService
  ]
})
export class AccountsModule {

}
