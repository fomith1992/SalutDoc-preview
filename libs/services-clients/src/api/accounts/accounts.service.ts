import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { AccountsServiceClient } from '@salutdoc/protos/lib/gen/api/accounts'
import { status } from 'grpc'
import { ACCOUNTS_CLIENT } from './accounts.constants'
import { Account, transformAccount } from './accounts.schema'

@Injectable()
export class AccountsService {
  private readonly accounts: AccountsServiceClient

  constructor (@Inject(ACCOUNTS_CLIENT) accounts: ClientGrpc) {
    this.accounts = accounts.getService('AccountsService')
  }

  async getAccountByPhoneNumber (data: {
    phoneNumber: string
  }): Promise<Account | null> {
    try {
      return await this.accounts
        .getAccountByPhoneNumber({
          phoneNumber: data.phoneNumber
        })
        .toPromise()
        .then(transformAccount)
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }

  async createAccount (data: {
    phoneNumber: string
  }): Promise<Account> {
    return await this.accounts
      .createAccount({
        phoneNumber: data.phoneNumber
      })
      .toPromise()
      .then(transformAccount)
  }
}
