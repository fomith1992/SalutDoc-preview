import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  ACCOUNTS_SERVICE_URI: t.string
})

export const accountsConfig = registerAs('accounts-config',
  () => t.transformOrThrow(configSchema)(process.env)
)
