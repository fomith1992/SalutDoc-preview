import * as t from '@salutdoc/utils/lib/transform'

const accountSchema = t.type({
  id: t.string,
  phoneNumber: t.string
})

export const transformAccount = t.transformOrThrow(accountSchema)

export type Account = t.OutputOf<typeof accountSchema>
