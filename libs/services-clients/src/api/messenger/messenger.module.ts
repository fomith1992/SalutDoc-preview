import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, messengerServicePath } from '@salutdoc/protos'
import { messengerConfig } from './messenger.config'
import { MESSENGER_CLIENT } from './messenger.constants'
import { MessengerService } from './messenger.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: MESSENGER_CLIENT,
        imports: [
          ConfigModule.forFeature(messengerConfig)
        ],
        inject: [messengerConfig.KEY],
        useFactory: (config: ConfigType<typeof messengerConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.MESSENGER_SERVICE_URI,
            protoPath: messengerServicePath,
            package: 'api.messenger',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    MessengerService
  ],
  exports: [
    MessengerService
  ]
})
export class MessengerModule {
}
