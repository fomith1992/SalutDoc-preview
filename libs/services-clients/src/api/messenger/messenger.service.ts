import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { MessengerServiceClient } from '@salutdoc/protos/lib/gen/api/messenger'
import { Metadata } from 'grpc'
import { MESSENGER_CLIENT } from './messenger.constants'
import { isMember, IsMember, consultationChat, ConsultationChat } from './messenger.schema'
import * as t from '@salutdoc/utils/lib/transform'

@Injectable()
export class MessengerService {
  private readonly messenger: MessengerServiceClient

  constructor (@Inject(MESSENGER_CLIENT) messenger: ClientGrpc) {
    this.messenger = messenger.getService('MessengerService')
  }

  async isMemberOfChat (chatId: string, userId: string, metadata?: Metadata): Promise<IsMember | null> {
    return await this.messenger.isMemberOfChat({
      chatId,
      userId
    }, metadata).toPromise().then(t.transformOrThrow(isMember))
  }

  async getConsultationChat (
    data: {
      chatId?: string
      consultationId?: string
    }, metadata?: Metadata): Promise<ConsultationChat | null> {
    return await this.messenger.getConsultationChat({
      chatId: data.chatId,
      consultationId: data.consultationId
    }, metadata).toPromise().then(t.transformOrThrow(consultationChat))
  }
}
