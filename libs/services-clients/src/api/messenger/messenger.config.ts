import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  MESSENGER_SERVICE_URI: t.string
})

export const messengerConfig = registerAs('messenger-client-config', () => t.transformOrThrow(configSchema)(process.env))
