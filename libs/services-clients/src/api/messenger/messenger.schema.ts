import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape, string } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const isMember = pipe(
  schema['.api.messenger.IsMemberOfChatResponse'],
  validatedShape({
    isMember: nonnull()
  })
)
export type IsMember = t.OutputOf<typeof isMember>

export const consultationChat = pipe(
  schema['.api.messenger.ConsultationChat'],
  validatedShape({
    id: string.min(1),
    consultation: nonnull()
  })
)
export type ConsultationChat = t.OutputOf<typeof consultationChat>
