import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const consultationSchema = pipe(
  schema['.api.consultations.Consultation'],
  validatedShape({
    createdAt: nonnull(),
    patient: nonnull()
  })
)

export const startedConsultationSchema = pipe(
  schema['.api.consultations.Consultation'],
  validatedShape({
    createdAt: nonnull(),
    patient: nonnull(),
    doctor: nonnull()
  })
)
export const transformConsultation = t.transformOrThrow(consultationSchema)

export type Consultation = t.OutputOf<typeof consultationSchema>
