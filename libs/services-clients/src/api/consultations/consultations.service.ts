import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { Metadata, status } from 'grpc'
import { ConsultationsServiceClient, PaymentStatus } from '@salutdoc/protos/lib/gen/api/consultations'
import { Consultation, transformConsultation } from './consultations.schema'
import { CONSULTATIONS_CLIENT } from './consultations.constants'

@Injectable()
export class ConsultationsService {
  private readonly consultations: ConsultationsServiceClient

  constructor (@Inject(CONSULTATIONS_CLIENT) consultations: ClientGrpc) {
    this.consultations = consultations.getService('ConsultationsService')
  }

  async getConsultation (consultationId: string, metadata?: Metadata): Promise<Consultation | null> {
    try {
      return await this.consultations.getConsultation({
        id: consultationId
      }, metadata).toPromise().then(transformConsultation)
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }

  async payConsultation (
    data: {
      consultationId: string
      paymentId: string
      status: PaymentStatus
    },
    metadata?: Metadata
  ): Promise<Consultation> {
    return await this.consultations
      .payConsultation({
        id: data.consultationId,
        paymentId: data.paymentId,
        status: data.status
      }, metadata)
      .toPromise()
      .then(transformConsultation)
  }
}
