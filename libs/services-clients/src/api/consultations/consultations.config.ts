import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  CONSULTATIONS_SERVICE_URI: t.string
})

export const consultationsClientConfig = registerAs(
  'consultations-client-config',
  () => t.transformOrThrow(configSchema)(process.env)
)
