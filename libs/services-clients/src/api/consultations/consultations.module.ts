import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { consultationsServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { ConsultationsService } from './consultations.service'
import { consultationsClientConfig } from './consultations.config'
import { CONSULTATIONS_CLIENT } from './consultations.constants'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: CONSULTATIONS_CLIENT,
        imports: [
          ConfigModule.forFeature(consultationsClientConfig)
        ],
        inject: [consultationsClientConfig.KEY],
        useFactory: (config: ConfigType<typeof consultationsClientConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.CONSULTATIONS_SERVICE_URI,
            protoPath: consultationsServicePath,
            package: 'api.consultations',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    ConsultationsService
  ],
  exports: [
    ConsultationsService
  ]
})
export class ConsultationsModule {
}
