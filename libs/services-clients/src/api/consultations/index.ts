export { ConsultationsModule } from './consultations.module'
export { Consultation } from './consultations.schema'
export { ConsultationsService } from './consultations.service'
export * as consultationsSchema from './consultations.schema'
