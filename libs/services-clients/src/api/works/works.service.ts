import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { PortfolioServiceClient } from '@salutdoc/protos/lib/gen/api/portfolio'
import { Metadata, status } from 'grpc'
import { WORKS_CLIENT } from './works.constants'
import { Work, work } from './works.schema'
import * as t from '@salutdoc/utils/lib/transform'

@Injectable()
export class WorksService {
  private readonly works: PortfolioServiceClient

  constructor (@Inject(WORKS_CLIENT) works: ClientGrpc) {
    this.works = works.getService('PortfolioService')
  }

  async getWork (workId: string, metadata?: Metadata): Promise<Work | null> {
    try {
      return await this.works.getWork({
        id: workId
      }, metadata).toPromise().then(t.transformOrThrow(work))
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }
}
