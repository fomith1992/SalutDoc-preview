import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const work = pipe(
  schema['.api.portfolio.Work'],
  validatedShape({
    ownerUser: nonnull(),
    createdAt: nonnull()
  })
)

export type Work = t.OutputOf<typeof work>
