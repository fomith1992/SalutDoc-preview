import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, portfolioServicePath } from '@salutdoc/protos'
import { worksConfig } from './works.config'
import { WORKS_CLIENT } from './works.constants'
import { WorksService } from './works.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: WORKS_CLIENT,
        imports: [
          ConfigModule.forFeature(worksConfig)
        ],
        inject: [worksConfig.KEY],
        useFactory: (config: ConfigType<typeof worksConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.WORKS_SERVICE_URI,
            protoPath: portfolioServicePath,
            package: 'api.portfolio',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    WorksService
  ],
  exports: [
    WorksService
  ]
})
export class WorksModule {
}
