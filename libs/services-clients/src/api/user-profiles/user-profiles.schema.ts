import * as t from '@salutdoc/utils/lib/transform'
import { RoleType } from '@salutdoc/protos/lib/gen/api/user-profiles'

const userProfileSchema = t.type({
  id: t.string,
  firstName: t.string,
  lastName: t.string,
  role: t.literal(RoleType.USER, RoleType.DOCTOR, RoleType.MODERATOR)
})

export const transformUserProfile = t.transformOrThrow(userProfileSchema)

export type UserProfile = t.OutputOf<typeof userProfileSchema>
