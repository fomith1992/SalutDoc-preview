import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, userProfilesServicePath } from '@salutdoc/protos'
import { userProfilesConfig } from './user-profiles.config'
import { USER_PROFILES_CLIENT } from './user-profiles.constants'
import { UserProfilesService } from './user-profiles.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: USER_PROFILES_CLIENT,
        imports: [
          ConfigModule.forFeature(userProfilesConfig)
        ],
        inject: [userProfilesConfig.KEY],
        useFactory: (config: ConfigType<typeof userProfilesConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.USER_PROFILES_SERVICE_URI,
            protoPath: userProfilesServicePath,
            package: 'api.user_profiles',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    UserProfilesService
  ],
  exports: [
    UserProfilesService
  ]
})
export class UserProfilesModule {
}
