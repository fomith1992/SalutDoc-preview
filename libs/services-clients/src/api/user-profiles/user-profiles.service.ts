import { LocalDate } from '@js-joda/core'
import { Inject, Injectable } from '@nestjs/common'
import { status } from 'grpc'
import { ClientGrpc } from '@nestjs/microservices'
import { UserProfilesServiceClient } from '@salutdoc/protos/lib/gen/api/user-profiles'
import { transformUserProfile, UserProfile } from './user-profiles.schema'

@Injectable()
export class UserProfilesService {
  private readonly userProfiles: UserProfilesServiceClient

  constructor (@Inject('USER_PROFILES_CLIENT') userProfiles: ClientGrpc) {
    this.userProfiles = userProfiles.getService('UserProfilesService')
  }

  async getUserProfile (data: {
    id: string
  }): Promise<UserProfile | null> {
    try {
      return await this.userProfiles.getUserProfile({ id: data.id }).toPromise().then(transformUserProfile)
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }

  async createUserProfile (data: {
    accountId: string
    firstName: string
    lastName: string
    birthday: LocalDate
    gender: string
    role: string
  }): Promise<UserProfile> {
    return await this.userProfiles.createUserProfile({
      accountId: data.accountId,
      firstName: data.firstName,
      lastName: data.lastName,
      gender: data.gender,
      birthday: { epochDays: data.birthday.toEpochDay() },
      role: data.role
    }).toPromise().then(transformUserProfile)
  }
}
