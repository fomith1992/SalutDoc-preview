import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  USER_PROFILES_SERVICE_URI: t.string
})

export const userProfilesConfig = registerAs('user-profiles-config',
  () => t.transformOrThrow(configSchema)(process.env))
