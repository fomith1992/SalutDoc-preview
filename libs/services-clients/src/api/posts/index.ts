export { PostsModule } from './posts.module'
export { Post } from './posts.schema'
export { PostsService } from './posts.service'
export * as postsSchema from './posts.schema'
