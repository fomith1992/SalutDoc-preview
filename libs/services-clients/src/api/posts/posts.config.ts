import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  POSTS_SERVICE_URI: t.string
})

export const postsConfig = registerAs('posts-config', () => t.transformOrThrow(configSchema)(process.env))
