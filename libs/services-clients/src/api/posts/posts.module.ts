import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { postsServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { PostsService } from './posts.service'
import { postsConfig } from './posts.config'
import { POSTS_CLIENT } from './posts.constants'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: POSTS_CLIENT,
        imports: [
          ConfigModule.forFeature(postsConfig)
        ],
        inject: [postsConfig.KEY],
        useFactory: (config: ConfigType<typeof postsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.POSTS_SERVICE_URI,
            protoPath: postsServicePath,
            package: 'api.posts',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    PostsService
  ],
  exports: [
    PostsService
  ]
})
export class PostsModule {
}
