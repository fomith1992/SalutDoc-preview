import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const postSchema = pipe(
  schema['.api.posts.Post'],
  validatedShape({
    createdAt: nonnull(),
    site: nonnull(),
    author: nonnull()
  })
)

export type Post = t.OutputOf<typeof postSchema>
