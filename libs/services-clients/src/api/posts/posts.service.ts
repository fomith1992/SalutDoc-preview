import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { PostsServiceClient } from '@salutdoc/protos/lib/gen/api/posts'
import * as t from '@salutdoc/utils/lib/transform'
import { Metadata, status } from 'grpc'
import { POSTS_CLIENT } from './posts.constants'
import { Post, postSchema } from './posts.schema'

@Injectable()
export class PostsService {
  private readonly posts: PostsServiceClient

  constructor (@Inject(POSTS_CLIENT) posts: ClientGrpc) {
    this.posts = posts.getService('PostsService')
  }

  async getPost (postId: string, metadata?: Metadata): Promise<Post | null> {
    try {
      return await this.posts.getPost({
        id: postId
      }, metadata).toPromise().then(t.transformOrThrow(postSchema))
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }
}
