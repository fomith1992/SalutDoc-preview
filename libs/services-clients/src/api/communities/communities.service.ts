import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { CommunitiesServiceClient } from '@salutdoc/protos/lib/gen/api/communities'
import * as t from '@salutdoc/utils/lib/transform'
import { status } from 'grpc'
import { COMMUNITIES_CLIENT } from './communities.constants'
import { Community, communitySchema } from './communities.schema'

@Injectable()
export class CommunitiesService {
  private readonly communities: CommunitiesServiceClient

  constructor (@Inject(COMMUNITIES_CLIENT) communities: ClientGrpc) {
    this.communities = communities.getService('CommunitiesService')
  }

  async getCommunity (communityId: string): Promise<Community | null> {
    try {
      return await this.communities.getCommunity({
        id: communityId
      }).toPromise().then(t.transformOrThrow(communitySchema))
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }
}
