import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const communitySchema = pipe(
  schema['.api.communities.Community'],
  validatedShape({
    // TODO for some reason following results in `never` type inferred
    // type: equals(CommunityType.PUBLIC, CommunityType.PRIVATE),
    createdAt: nonnull()
  })
)

export type Community = t.OutputOf<typeof communitySchema>
