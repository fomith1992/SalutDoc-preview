import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  COMMUNITIES_SERVICE_URI: t.string
})

export const communitiesConfig = registerAs('communities-config', () => t.transformOrThrow(configSchema)(process.env))
