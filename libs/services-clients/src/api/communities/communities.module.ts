import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { communitiesServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { CommunitiesService } from './communities.service'
import { communitiesConfig } from './communities.config'
import { COMMUNITIES_CLIENT } from './communities.constants'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: COMMUNITIES_CLIENT,
        imports: [
          ConfigModule.forFeature(communitiesConfig)
        ],
        inject: [communitiesConfig.KEY],
        useFactory: (config: ConfigType<typeof communitiesConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.COMMUNITIES_SERVICE_URI,
            protoPath: communitiesServicePath,
            package: 'api.communities',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    CommunitiesService
  ],
  exports: [
    CommunitiesService
  ]
})
export class CommunitiesModule {
}
