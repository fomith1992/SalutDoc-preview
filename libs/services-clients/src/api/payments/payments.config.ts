import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  PAYMENTS_SERVICE_URI: t.string
})

export const paymentsClientConfig = registerAs(
  'payments-client-config',
  () => t.transformOrThrow(configSchema)(process.env)
)
