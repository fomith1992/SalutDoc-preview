import { schema } from '@salutdoc/protos'
import * as t from '@salutdoc/utils/lib/transform'
import { nonnull, validatedShape } from '@salutdoc/utils/lib/validation'
import { pipe } from 'fp-ts/function'
import 'long'

export const paymentSchema = pipe(
  schema['.api.payments.Payment'],
  validatedShape({
    payer: nonnull(),
    entityRef: nonnull(),
    status: nonnull()
  })
)

export const transformPayment = t.transformOrThrow(paymentSchema)

export type Payment = t.OutputOf<typeof paymentSchema>
