export { PaymentsModule } from './payments.module'
export { Payment } from './payments.schema'
export { PaymentsService } from './payments.service'
export * as paymentsSchema from './payments.service'
