import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { paymentsServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { PaymentsService } from './payments.service'
import { paymentsClientConfig } from './payments.config'
import { PAYMENTS_CLIENT } from './payments.constants'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: PAYMENTS_CLIENT,
        imports: [
          ConfigModule.forFeature(paymentsClientConfig)
        ],
        inject: [paymentsClientConfig.KEY],
        useFactory: (config: ConfigType<typeof paymentsClientConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.PAYMENTS_SERVICE_URI,
            protoPath: paymentsServicePath,
            package: 'api.payments',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    PaymentsService
  ],
  exports: [
    PaymentsService
  ]
})
export class PaymentsModule {
}
