import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { Metadata, status } from 'grpc'
import { PaymentsServiceClient } from '@salutdoc/protos/lib/gen/api/payments'
import { Payment, transformPayment } from './payments.schema'
import { PAYMENTS_CLIENT } from './payments.constants'

@Injectable()
export class PaymentsService {
  private readonly payments: PaymentsServiceClient

  constructor (@Inject(PAYMENTS_CLIENT) payments: ClientGrpc) {
    this.payments = payments.getService('PaymentsService')
  }

  async getPayment (paymentId: string, metadata?: Metadata): Promise<Payment | null> {
    try {
      return await this.payments.getPayment({
        id: paymentId
      }, metadata).toPromise().then(transformPayment)
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }
}
