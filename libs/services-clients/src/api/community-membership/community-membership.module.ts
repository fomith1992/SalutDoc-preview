import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, communityMembershipServicePath } from '@salutdoc/protos'
import { communityMembershipConfig } from './community-membership.config'
import { COMMUNITY_MEMBERSHIP_CLIENT } from './community-membership.constants'
import { CommunityMembershipService } from './community-membership.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: COMMUNITY_MEMBERSHIP_CLIENT,
        imports: [
          ConfigModule.forFeature(communityMembershipConfig)
        ],
        inject: [communityMembershipConfig.KEY],
        useFactory: (config: ConfigType<typeof communityMembershipConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.COMMUNITY_MEMBERSHIP_SERVICE_URI,
            protoPath: communityMembershipServicePath,
            package: 'api.community_membership',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    CommunityMembershipService
  ],
  exports: [
    CommunityMembershipService
  ]
})
export class CommunityMembershipModule {
}
