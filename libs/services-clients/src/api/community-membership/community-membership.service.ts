import { Inject, Injectable } from '@nestjs/common'
import { status } from 'grpc'
import { ClientGrpc } from '@nestjs/microservices'
import { CommunityMembershipServiceClient, Status } from '@salutdoc/protos/lib/gen/api/community-membership'
import { Observable } from 'rxjs'
import { COMMUNITY_MEMBERSHIP_CLIENT } from './community-membership.constants'
import { transformCommunityMembership, CommunityMembership } from './community-membership.schema'
import * as $ from 'rxjs/operators'

@Injectable()
export class CommunityMembershipService {
  private readonly communityMembership: CommunityMembershipServiceClient

  constructor (@Inject(COMMUNITY_MEMBERSHIP_CLIENT) communityMembership: ClientGrpc) {
    this.communityMembership = communityMembership.getService('CommunityMembershipService')
  }

  async getMembershipStatus (data: {
    userId: string
    communityId: string
  }): Promise<CommunityMembership | null> {
    try {
      return await this.communityMembership.getMembershipStatus({
        userId: data.userId,
        communityId: data.communityId
      }).toPromise().then(transformCommunityMembership)
    } catch (e) {
      if (e.code === status.NOT_FOUND) {
        return null
      } else {
        throw e
      }
    }
  }

  listAllMembershipStatuses (data: {
    userId?: string
    communityId?: string
    status?: keyof typeof statusMap
  }): Observable<CommunityMembership> {
    const { userId, communityId, status } = data
    return this.communityMembership.listAllMembershipsStatuses({
      userId,
      communityId,
      status: status == null ? undefined : statusMap[status]
    }).pipe(
      $.map(transformCommunityMembership)
    )
  }
}

const statusMap = {
  ACTIVE: Status.ACTIVE,
  INACTIVE: Status.INACTIVE,
  INVITE: Status.INVITE,
  REQUEST: Status.REQUEST
}
