import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  COMMUNITY_MEMBERSHIP_SERVICE_URI: t.string
})

export const communityMembershipConfig = registerAs('community-membership-config',
  () => t.transformOrThrow(configSchema)(process.env))
