import * as t from '@salutdoc/utils/lib/transform'
import { Status } from '@salutdoc/protos/lib/gen/api/community-membership'

const communityMembershipSchema = t.type({
  id: t.string,
  user: t.type({
    id: t.string
  }),
  community: t.type({
    id: t.string
  }),
  status: t.literal(Status.ACTIVE, Status.INACTIVE, Status.INVITE, Status.REQUEST)
})

export const transformCommunityMembership = t.transformOrThrow(communityMembershipSchema)

export type CommunityMembership = t.OutputOf<typeof communityMembershipSchema>
