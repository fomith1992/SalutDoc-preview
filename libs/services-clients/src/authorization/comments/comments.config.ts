import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  COMMENTS_AUTHORIZATION_SERVICE_URI: t.string
})

export const commentsConfig = registerAs('comments-auth-config', () => t.transformOrThrow(configSchema)(process.env))
