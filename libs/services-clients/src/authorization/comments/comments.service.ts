import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { CommentsAuthorizationServiceClient } from '@salutdoc/protos/lib/gen/authorization/comments'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { COMMENTS_AUTH_CLIENT } from './comments.constants'

@Injectable()
export class CommentsAuthorizationService {
  private readonly comments: CommentsAuthorizationServiceClient

  constructor (@Inject(COMMENTS_AUTH_CLIENT) comments: ClientGrpc) {
    this.comments = comments.getService('CommentsAuthorizationService')
  }

  async checkGetWorkCommentAccess (
    data: {
      workId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkGetCommentEntityAccess({
      workId: data.workId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetPostCommentAccess (
    data: {
      postId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkGetCommentEntityAccess({
      postId: data.postId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetQuestionCommentAccess (
    data: {
      questionId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkGetCommentEntityAccess({
      questionId: data.questionId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreatePostCommentAccess (
    data: {
      authorId: string
      postId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkCreateCommentAccess({
      authorId: data.authorId,
      postId: data.postId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateWorkCommentAccess (
    data: {
      authorId: string
      workId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkCreateCommentAccess({
      authorId: data.authorId,
      workId: data.workId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateQuestionCommentAccess (
    data: {
      authorId: string
      questionId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkCreateCommentAccess({
      authorId: data.authorId,
      questionId: data.questionId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateCommentReplyAccess (
    data: {
      authorId: string
      commentId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkCreateCommentReplyAccess({
      authorId: data.authorId,
      commentId: data.commentId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkEditCommentAccess (
    data: {
      commentId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkEditCommentAccess({
      commentId: data.commentId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkDeleteCommentAccess (
    data: {
      commentId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.comments.checkDeleteCommentAccess({
      commentId: data.commentId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
