import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { commentsAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { commentsConfig } from './comments.config'
import { COMMENTS_AUTH_CLIENT } from './comments.constants'
import { CommentsAuthorizationService } from './comments.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: COMMENTS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(commentsConfig)
        ],
        inject: [commentsConfig.KEY],
        useFactory: (config: ConfigType<typeof commentsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.COMMENTS_AUTHORIZATION_SERVICE_URI,
            protoPath: commentsAuthorizationServicePath,
            package: 'authorization.comments',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    CommentsAuthorizationService
  ],
  exports: [
    CommentsAuthorizationService
  ]
})
export class CommentsAuthorizationModule {
}
