import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { Metadata } from 'grpc'
import {
  CommunitiesAuthorizationServiceClient,
  CommunityType
} from '@salutdoc/protos/lib/gen/authorization/communities'
import { Access, transformAccess } from '../access.schema'
import { COMMUNITIES_AUTH_CLIENT } from './communities.constants'

@Injectable()
export class CommunitiesAuthorizationService {
  private readonly communities: CommunitiesAuthorizationServiceClient

  constructor (@Inject(COMMUNITIES_AUTH_CLIENT) communities: ClientGrpc) {
    this.communities = communities.getService('CommunitiesAuthorizationService')
  }

  async checkCreateCommunityAccess (
    data: {
      userId: string
      type: CommunityType
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communities.checkCreateCommunityAccess({
      userId: data.userId,
      type: data.type
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
