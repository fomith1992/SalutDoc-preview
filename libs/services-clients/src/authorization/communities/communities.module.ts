import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { communitiesAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { communitiesConfig } from './communities.config'
import { COMMUNITIES_AUTH_CLIENT } from './communities.constants'
import { CommunitiesAuthorizationService } from './communities.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: COMMUNITIES_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(communitiesConfig)
        ],
        inject: [communitiesConfig.KEY],
        useFactory: (config: ConfigType<typeof communitiesConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.COMMUNITIES_AUTHORIZATION_SERVICE_URI,
            protoPath: communitiesAuthorizationServicePath,
            package: 'authorization.communities',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    CommunitiesAuthorizationService
  ],
  exports: [
    CommunitiesAuthorizationService
  ]
})
export class CommunitiesAuthorizationModule {
}
