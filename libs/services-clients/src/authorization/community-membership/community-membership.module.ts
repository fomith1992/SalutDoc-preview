import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { communityMembershipAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { communityMembershipConfig } from './community-membership.config'
import { COMMUNITY_MEMBERSHIP_AUTH_CLIENT } from './community-membership.constants'
import { CommunityMembershipAuthorizationService } from './community-membership.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: COMMUNITY_MEMBERSHIP_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(communityMembershipConfig)
        ],
        inject: [communityMembershipConfig.KEY],
        useFactory: (config: ConfigType<typeof communityMembershipConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.COMMUNITY_MEMBERSHIP_AUTHORIZATION_SERVICE_URI,
            protoPath: communityMembershipAuthorizationServicePath,
            package: 'authorization.community_membership',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    CommunityMembershipAuthorizationService
  ],
  exports: [
    CommunityMembershipAuthorizationService
  ]
})
export class CommunityMembershipAuthorizationModule {
}
