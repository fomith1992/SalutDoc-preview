import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  COMMUNITY_MEMBERSHIP_AUTHORIZATION_SERVICE_URI: t.string
})

export const communityMembershipConfig = registerAs('community-membership-auth-config', () => t.transformOrThrow(configSchema)(process.env))
