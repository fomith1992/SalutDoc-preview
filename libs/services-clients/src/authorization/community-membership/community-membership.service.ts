import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { CommunityMembershipAuthorizationServiceClient, Status } from '@salutdoc/protos/lib/gen/authorization/community-membership'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { COMMUNITY_MEMBERSHIP_AUTH_CLIENT } from './community-membership.constants'

@Injectable()
export class CommunityMembershipAuthorizationService {
  private readonly communityMembership: CommunityMembershipAuthorizationServiceClient

  constructor (@Inject(COMMUNITY_MEMBERSHIP_AUTH_CLIENT) communityMembership: ClientGrpc) {
    this.communityMembership = communityMembership.getService('CommunityMembershipAuthorizationService')
  }

  async checkFollowAccess (
    data: {
      userId: string
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkFollowAccess({
      userId: data.userId,
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkUnfollowAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkUnfollowAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkRevokeAccess (
    data: {
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkRevokeAccess({
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkSendCommunityInviteAccess (
    data: {
      userId: string
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkSendCommunityInviteAccess({
      userId: data.userId,
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCancelCommunityInviteAccess (
    data: {
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkCancelCommunityInviteAccess({
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkRejectCommunityInviteAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkRejectCommunityInviteAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkConfirmCommunityInviteAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkConfirmCommunityInviteAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkSendCommunityApplicationAccess (
    data: {
      userId: string
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkSendCommunityApplicationAccess({
      userId: data.userId,
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCancelCommunityApplicationAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkCancelCommunityApplicationAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkRejectCommunityApplicationAccess (
    data: {
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkRejectCommunityApplicationAccess({
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkConfirmCommunityApplicationAccess (
    data: {
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkConfirmCommunityApplicationAccess({
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetMembershipsCountAccess (
    data: {
      userId?: string
      communityId?: string
      status?: Status
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.checkGetMembershipsCountAccess({
      userId: data.userId,
      communityId: data.communityId,
      status: data.status
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async canViewMembership (
    data: {
      userId?: string
      communityId?: string
      status?: Status
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.communityMembership.canViewMembership({
      userId: data.userId,
      communityId: data.communityId,
      status: data.status
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
