import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { QuestionsAuthorizationServiceClient } from '@salutdoc/protos/lib/gen/authorization/questions'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { QUESTIONS_AUTH_CLIENT } from './questions.constants'

@Injectable()
export class QuestionsAuthorizationService {
  private readonly questions: QuestionsAuthorizationServiceClient

  constructor (@Inject(QUESTIONS_AUTH_CLIENT) questions: ClientGrpc) {
    this.questions = questions.getService('QuestionsAuthorizationService')
  }

  async checkCreateQuestionAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkCreateQuestionAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkEditQuestionAccess (
    data: {
      questionId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkEditQuestionAccess({
      questionId: data.questionId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkArchiveQuestionAccess (
    data: {
      questionId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkArchiveQuestionAccess({
      questionId: data.questionId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateAnswerAccess (
    data: {
      userId: string
      questionId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkCreateAnswerAccess({
      questionId: data.questionId,
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkEditAnswerAccess (
    data: {
      answerId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkEditAnswerAccess({
      answerId: data.answerId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkArchiveAnswerAccess (
    data: {
      answerId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkArchiveAnswerAccess({
      answerId: data.answerId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkApproveAnswerAccess (
    data: {
      questionId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkApproveAnswerAccess({
      questionId: data.questionId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkDisapproveAnswerAccess (
    data: {
      questionId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.questions.checkDisapproveAnswerAccess({
      questionId: data.questionId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
