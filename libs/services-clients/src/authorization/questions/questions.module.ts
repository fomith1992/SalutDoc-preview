import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, questionsAuthorizationServicePath } from '@salutdoc/protos'
import { questionsConfig } from './questions.config'
import { QUESTIONS_AUTH_CLIENT } from './questions.constants'
import { QuestionsAuthorizationService } from './questions.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: QUESTIONS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(questionsConfig)
        ],
        inject: [questionsConfig.KEY],
        useFactory: (config: ConfigType<typeof questionsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.QUESTIONS_AUTHORIZATION_SERVICE_URI,
            protoPath: questionsAuthorizationServicePath,
            package: 'authorization.questions',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    QuestionsAuthorizationService
  ],
  exports: [
    QuestionsAuthorizationService
  ]
})
export class QuestionsAuthorizationModule {
}
