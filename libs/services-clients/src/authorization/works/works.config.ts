import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  WORKS_AUTHORIZATION_SERVICE_URI: t.string
})

export const worksConfig = registerAs('works-auth-config', () => t.transformOrThrow(configSchema)(process.env))
