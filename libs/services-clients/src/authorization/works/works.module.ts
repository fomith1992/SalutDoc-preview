import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, worksAuthorizationServicePath } from '@salutdoc/protos'
import { worksConfig } from './works.config'
import { WORKS_AUTH_CLIENT } from './works.constants'
import { WorksAuthorizationService } from './works.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: WORKS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(worksConfig)
        ],
        inject: [worksConfig.KEY],
        useFactory: (config: ConfigType<typeof worksConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.WORKS_AUTHORIZATION_SERVICE_URI,
            protoPath: worksAuthorizationServicePath,
            package: 'authorization.works',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    WorksAuthorizationService
  ],
  exports: [
    WorksAuthorizationService
  ]
})
export class WorksAuthorizationModule {
}
