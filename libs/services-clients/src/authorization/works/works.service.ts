import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { WorksAuthorizationServiceClient } from '@salutdoc/protos/lib/gen/authorization/works'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { WORKS_AUTH_CLIENT } from './works.constants'

@Injectable()
export class WorksAuthorizationService {
  private readonly works: WorksAuthorizationServiceClient

  constructor (@Inject(WORKS_AUTH_CLIENT) works: ClientGrpc) {
    this.works = works.getService('WorksAuthorizationService')
  }

  async checkCreateWorkAccess (
    data: {
      userId: string
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.works.checkCreateWorkAccess({
      userId: data.userId,
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkEditWorkAccess (
    data: {
      workId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.works.checkEditWorkAccess({
      workId: data.workId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkArchiveWorkAccess (
    data: {
      workId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.works.checkArchiveWorkAccess({
      workId: data.workId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
