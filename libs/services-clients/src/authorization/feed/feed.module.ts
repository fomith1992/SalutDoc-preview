import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, feedAuthorizationServicePath } from '@salutdoc/protos'
import { feedConfig } from './feed.config'
import { FEED_AUTH_CLIENT } from './feed.constants'
import { FeedAuthorizationService } from './feed.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: FEED_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(feedConfig)
        ],
        inject: [feedConfig.KEY],
        useFactory: (config: ConfigType<typeof feedConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.FEED_AUTHORIZATION_SERVICE_URI,
            protoPath: feedAuthorizationServicePath,
            package: 'authorization.feed',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    FeedAuthorizationService
  ],
  exports: [
    FeedAuthorizationService
  ]
})
export class FeedAuthorizationModule {
}
