import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { FeedAuthorizationServiceClient } from '@salutdoc/protos/lib/gen/authorization/feed'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { FEED_AUTH_CLIENT } from './feed.constants'

@Injectable()
export class FeedAuthorizationService {
  private readonly feed: FeedAuthorizationServiceClient

  constructor (@Inject(FEED_AUTH_CLIENT) works: ClientGrpc) {
    this.feed = works.getService('FeedAuthorizationService')
  }

  async checkListUserPersonalEntriesAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.feed.checkListUserPersonalEntriesAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkListUserFeedEntriesAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.feed.checkListUserFeedEntriesAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkListCommunityEntriesAccess (
    data: {
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.feed.checkListCommunityEntriesAccess({
      communityId: data.communityId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
