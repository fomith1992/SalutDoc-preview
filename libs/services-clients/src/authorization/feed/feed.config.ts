import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  FEED_AUTHORIZATION_SERVICE_URI: t.string
})

export const feedConfig = registerAs('feed-auth-config', () => t.transformOrThrow(configSchema)(process.env))
