import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  CONSULTATIONS_AUTHORIZATION_SERVICE_URI: t.string
})

export const consultationsConfig = registerAs('consultations-auth-config', () => t.transformOrThrow(configSchema)(process.env))
