import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import {
  ConsultationsAuthorizationServiceClient
} from '@salutdoc/protos/lib/gen/authorization/consultations'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { CONSULTATIONS_AUTH_CLIENT } from './consultations.constants'

@Injectable()
export class ConsultationsAuthorizationService {
  private readonly consultations: ConsultationsAuthorizationServiceClient

  constructor (@Inject(CONSULTATIONS_AUTH_CLIENT) consultations: ClientGrpc) {
    this.consultations = consultations.getService('ConsultationsAuthorizationService')
  }

  async canViewConsultation (
    data: {
      patientId?: string
      doctorId?: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.canViewConsultation({
      patientId: data.patientId,
      doctorId: data.doctorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateConsultationAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkCreateConsultationAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkAddAnamnesisAccess (
    data: {
      consultationId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkAddAnamnesisAccess({
      consultationId: data.consultationId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkAddReportAccess (
    data: {
      consultationId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkAddReportAccess({
      consultationId: data.consultationId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkAddDoctorAndStartConsultationAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkAddDoctorAndStartConsultationAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetConsultationsCountAccess (
    data: {
      doctorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkGetConsultationsCountAccess({
      doctorId: data.doctorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetFreeConsultationsCountAccess (
    _data: {},
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkGetFreeConsultationsCountAccess({}, metadata).toPromise()
      .then(transformAccess)
  }

  async checkPayConsultationAccess (
    data: {
      consultationId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.consultations.checkPayConsultationAccess({
      consultationId: data.consultationId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkListConsultationsForModeratorAccess (metadata: Metadata): Promise<Access> {
    return await this.consultations
      .checkListConsultationsForModeratorAccess({}, metadata)
      .toPromise()
      .then(transformAccess)
  }
}
