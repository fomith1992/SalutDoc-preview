import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { consultationsAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { consultationsConfig } from './consultations.config'
import { CONSULTATIONS_AUTH_CLIENT } from './consultations.constants'
import { ConsultationsAuthorizationService } from './consultations.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: CONSULTATIONS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(consultationsConfig)
        ],
        inject: [consultationsConfig.KEY],
        useFactory: (config: ConfigType<typeof consultationsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.CONSULTATIONS_AUTHORIZATION_SERVICE_URI,
            protoPath: consultationsAuthorizationServicePath,
            package: 'authorization.consultations',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    ConsultationsAuthorizationService
  ],
  exports: [
    ConsultationsAuthorizationService
  ]
})
export class ConsultationsAuthorizationModule {
}
