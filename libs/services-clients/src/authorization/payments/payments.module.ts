import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { paymentsAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { paymentsConfig } from './payments.config'
import { PAYMENTS_AUTH_CLIENT } from './payments.constants'
import { PaymentsAuthorizationService } from './payments.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: PAYMENTS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(paymentsConfig)
        ],
        inject: [paymentsConfig.KEY],
        useFactory: (config: ConfigType<typeof paymentsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.PAYMENTS_AUTHORIZATION_SERVICE_URI,
            protoPath: paymentsAuthorizationServicePath,
            package: 'authorization.payments',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    PaymentsAuthorizationService
  ],
  exports: [
    PaymentsAuthorizationService
  ]
})
export class PaymentsAuthorizationModule {
}
