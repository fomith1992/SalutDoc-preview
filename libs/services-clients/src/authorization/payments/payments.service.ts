import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import {
  PaymentsAuthorizationServiceClient
} from '@salutdoc/protos/lib/gen/authorization/payments'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { PAYMENTS_AUTH_CLIENT } from './payments.constants'

@Injectable()
export class PaymentsAuthorizationService {
  private readonly payments: PaymentsAuthorizationServiceClient

  constructor (@Inject(PAYMENTS_AUTH_CLIENT) payments: ClientGrpc) {
    this.payments = payments.getService('PaymentsAuthorizationService')
  }

  async checkGetPaymentAccess (
    data: {
      payerId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.payments.checkGetPaymentAccess({
      payerId: data.payerId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkPromocodeAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.payments.checkPromocodeAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateConsultationPaymentAccess (
    data: {
      consultationId: string
      payerId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.payments.checkCreateConsultationPaymentAccess({
      consultationId: data.consultationId,
      userId: data.payerId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkRefundPaymentAccess (
    data: {},
    metadata: Metadata
  ): Promise<Access> {
    return await this.payments.checkRefundPaymentAccess(data, metadata).toPromise().then(transformAccess)
  }
}
