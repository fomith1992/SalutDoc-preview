import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import {
  NotificationsAuthorizationServiceClient, OutputNotificationConfig
} from '@salutdoc/protos/lib/gen/authorization/notifications'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { NOTIFICATIONS_AUTH_CLIENT } from './notifications.constants'

@Injectable()
export class NotificationsAuthorizationService {
  private readonly notifications: NotificationsAuthorizationServiceClient

  constructor (@Inject(NOTIFICATIONS_AUTH_CLIENT) messenger: ClientGrpc) {
    this.notifications = messenger.getService('NotificationsAuthorizationService')
  }

  async checkAuthUserAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.notifications.checkAuthUserAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkUpdateNotificationsSettingsAccess (
    data: {
      userId: string
      configs: OutputNotificationConfig[]
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.notifications.checkUpdateNotificationsSettingsAccess({
      userId: data.userId,
      configs: data.configs
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
