import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  NOTIFICATIONS_AUTHORIZATION_SERVICE_URI: t.string
})

export const notificationsConfig = registerAs('notifications-auth-config',
  () => t.transformOrThrow(configSchema)(process.env)
)
