import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { notificationsAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { notificationsConfig } from './notifications.config'
import { NOTIFICATIONS_AUTH_CLIENT } from './notifications.constants'
import { NotificationsAuthorizationService } from './notifications.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: NOTIFICATIONS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(notificationsConfig)
        ],
        inject: [notificationsConfig.KEY],
        useFactory: (config: ConfigType<typeof notificationsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.NOTIFICATIONS_AUTHORIZATION_SERVICE_URI,
            protoPath: notificationsAuthorizationServicePath,
            package: 'authorization.notifications',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    NotificationsAuthorizationService
  ],
  exports: [
    NotificationsAuthorizationService
  ]
})
export class NotificationsAuthorizationModule {
}
