import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { postsAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { postsConfig } from './posts.config'
import { POSTS_AUTH_CLIENT } from './posts.constants'
import { PostsAuthorizationService } from './posts.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: POSTS_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(postsConfig)
        ],
        inject: [postsConfig.KEY],
        useFactory: (config: ConfigType<typeof postsConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.POSTS_AUTHORIZATION_SERVICE_URI,
            protoPath: postsAuthorizationServicePath,
            package: 'authorization.posts',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    PostsAuthorizationService
  ],
  exports: [
    PostsAuthorizationService
  ]
})
export class PostsAuthorizationModule {
}
