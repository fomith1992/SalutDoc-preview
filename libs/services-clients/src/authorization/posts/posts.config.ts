import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  POSTS_AUTHORIZATION_SERVICE_URI: t.string
})

export const postsConfig = registerAs('posts-auth-config', () => t.transformOrThrow(configSchema)(process.env))
