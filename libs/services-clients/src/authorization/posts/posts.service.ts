import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import {
  OutputAuthorUser,
  OutputSiteUser,
  OutputSiteCommunity,
  OutputSiteService,
  PostsAuthorizationServiceClient
} from '@salutdoc/protos/lib/gen/authorization/posts'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { POSTS_AUTH_CLIENT } from './posts.constants'

@Injectable()
export class PostsAuthorizationService {
  private readonly posts: PostsAuthorizationServiceClient

  constructor (@Inject(POSTS_AUTH_CLIENT) posts: ClientGrpc) {
    this.posts = posts.getService('PostsAuthorizationService')
  }

  async checkGetPostAccess (
    data: {
      authorUser: OutputAuthorUser | null
      siteUser: OutputSiteUser | null
      siteCommunity: OutputSiteCommunity | null
      siteService: OutputSiteService | null
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.posts.checkGetPostAccess({
      authorUser: data.authorUser,
      siteUser: data.siteUser,
      siteCommunity: data.siteCommunity,
      siteService: data.siteService
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateUserPostAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.posts.checkCreatePostAccess({
      userId: data.userId,
      siteUser: {
        id: data.userId
      }
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateCommunityPostAccess (
    data: {
      userId: string
      communityId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.posts.checkCreatePostAccess({
      userId: data.userId,
      siteCommunity: {
        id: data.communityId
      }
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateServicePostAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.posts.checkCreatePostAccess({
      userId: data.userId,
      siteService: {
        id: 'service-id'
      }
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkEditPostAccess (
    data: {
      postId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.posts.checkEditPostAccess({
      postId: data.postId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkArchivePostAccess (
    data: {
      postId: string
      editorId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.posts.checkEditPostAccess({
      postId: data.postId,
      editorId: data.editorId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
