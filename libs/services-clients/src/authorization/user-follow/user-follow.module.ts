import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { protoLoaderConfig, userFollowAuthorizationServicePath } from '@salutdoc/protos'
import { userFollowConfig } from './user-follow.config'
import { USER_FOLLOW_AUTH_CLIENT } from './user-follow.constants'
import { UserFollowAuthorizationService } from './user-follow.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: USER_FOLLOW_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(userFollowConfig)
        ],
        inject: [userFollowConfig.KEY],
        useFactory: (config: ConfigType<typeof userFollowConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.USER_FOLLOW_AUTHORIZATION_SERVICE_URI,
            protoPath: userFollowAuthorizationServicePath,
            package: 'authorization.user_follow',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    UserFollowAuthorizationService
  ],
  exports: [
    UserFollowAuthorizationService
  ]
})
export class UserFollowAuthorizationModule {
}
