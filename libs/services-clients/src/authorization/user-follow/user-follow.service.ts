import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { USER_FOLLOW_AUTH_CLIENT } from './user-follow.constants'
import { UserFollowAuthorizationServiceClient } from '@salutdoc/protos/lib/gen/authorization/user-follow'

@Injectable()
export class UserFollowAuthorizationService {
  private readonly userFollow: UserFollowAuthorizationServiceClient

  constructor (@Inject(USER_FOLLOW_AUTH_CLIENT) FollowAccess: ClientGrpc) {
    this.userFollow = FollowAccess.getService('UserFollowAuthorizationService')
  }

  async checkFollowAccess (
    data: {
      followerId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.userFollow.checkFollowAccess({
      followerId: data.followerId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkUnfollowAccess (
    data: {
      followerId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.userFollow.checkUnfollowAccess({
      followerId: data.followerId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
