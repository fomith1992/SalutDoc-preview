import { Decision } from '@salutdoc/protos/lib/gen/authorization/access'
import * as t from '@salutdoc/utils/lib/transform'
import { pipe } from 'fp-ts/function'

export interface AccessAllowed {
  decision: 'ALLOWED'
}

export interface AccessDenied {
  decision: 'DENIED'
  reason: string
}

export type Access = AccessAllowed | AccessDenied

export const transformAccess = pipe(
  t.type({
    decision: t.literal(Decision.ALLOW, Decision.DENY),
    reason: t.string
  }),
  t.map((access): Access => {
    switch (access.decision) {
      case Decision.ALLOW:
        return {
          decision: 'ALLOWED'
        }
      case Decision.DENY:
        return {
          decision: 'DENIED',
          reason: access.reason
        }
    }
  }),
  t.transformOrThrow
)
