import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { filesAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { filesConfig } from './files.config'
import { FILES_AUTH_CLIENT } from './files.constants'
import { FilesAuthorizationService } from './files.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: FILES_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(filesConfig)
        ],
        inject: [filesConfig.KEY],
        useFactory: (config: ConfigType<typeof filesConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.FILES_AUTHORIZATION_SERVICE_URI,
            protoPath: filesAuthorizationServicePath,
            package: 'authorization.files',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    FilesAuthorizationService
  ],
  exports: [
    FilesAuthorizationService
  ]
})
export class FilesAuthorizationModule {
}
