import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import {
  FilesAuthorizationServiceClient
} from '@salutdoc/protos/lib/gen/authorization/files'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { FILES_AUTH_CLIENT } from './files.constants'

@Injectable()
export class FilesAuthorizationService {
  private readonly files: FilesAuthorizationServiceClient

  constructor (@Inject(FILES_AUTH_CLIENT) files: ClientGrpc) {
    this.files = files.getService('FilesAuthorizationService')
  }

  async canViewFile (
    data: {
      chatId?: string
      consultationId?: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.files.canViewFile({
      chatId: data.chatId,
      consultationId: data.consultationId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkCreateFileAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.files.checkCreateFileAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
