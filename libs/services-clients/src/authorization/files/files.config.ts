import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  FILES_AUTHORIZATION_SERVICE_URI: t.string
})

export const filesConfig = registerAs('files-auth-config', () => t.transformOrThrow(configSchema)(process.env))
