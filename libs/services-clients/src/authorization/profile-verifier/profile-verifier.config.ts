import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  PROFILE_VERIFIER_AUTHORIZATION_SERVICE_URI: t.string
})

export const profileVerifierConfig = registerAs('profile-verifier-config',
  () => t.transformOrThrow(configSchema)(process.env))
