import { Module } from '@nestjs/common'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { protoLoaderConfig, profileVerifierAuthorizationServicePath } from '@salutdoc/protos'
import { profileVerifierConfig } from './profile-verifier.config'
import { PROFILE_VERIFIER_AUTH_CLIENT } from './profile-verifier.constants'
import { ProfileVerifierAuthorizationService } from './profile-verifier.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: PROFILE_VERIFIER_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(profileVerifierConfig)
        ],
        inject: [profileVerifierConfig.KEY],
        useFactory: (config: ConfigType<typeof profileVerifierConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.PROFILE_VERIFIER_AUTHORIZATION_SERVICE_URI,
            protoPath: profileVerifierAuthorizationServicePath,
            package: 'authorization.profile_verifier',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    ProfileVerifierAuthorizationService
  ],
  exports: [
    ProfileVerifierAuthorizationService
  ]
})
export class ProfileVerifierAuthorizationModule {
}
