import { Inject, Injectable } from '@nestjs/common'
import { ProfileVerifierAuthorizationServiceClient } from '@salutdoc/protos/lib/gen/authorization/profile-verifier'
import { ClientGrpc } from '@nestjs/microservices'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { PROFILE_VERIFIER_AUTH_CLIENT } from './profile-verifier.constants'

@Injectable()
export class ProfileVerifierAuthorizationService {
  private readonly profileVerifier: ProfileVerifierAuthorizationServiceClient

  constructor (@Inject(PROFILE_VERIFIER_AUTH_CLIENT) profileVerifier: ClientGrpc) {
    this.profileVerifier = profileVerifier.getService('ProfileVerifierAuthorizationService')
  }

  async checkVerifyApplicationAccess (metadata: Metadata): Promise<Access> {
    return await this.profileVerifier.checkVerifyApplicationAccess({}, metadata).toPromise()
      .then(transformAccess)
  }
}
