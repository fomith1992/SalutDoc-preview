import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  MESSENGER_AUTHORIZATION_SERVICE_URI: t.string
})

export const messengerConfig = registerAs('messenger-auth-config', () => t.transformOrThrow(configSchema)(process.env))
