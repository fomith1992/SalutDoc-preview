import { Inject, Injectable } from '@nestjs/common'
import { ClientGrpc } from '@nestjs/microservices'
import {
  MessengerAuthorizationServiceClient
} from '@salutdoc/protos/lib/gen/authorization/messenger'
import { Metadata } from 'grpc'
import { Access, transformAccess } from '../access.schema'
import { MESSENGER_AUTH_CLIENT } from './messenger.constants'

@Injectable()
export class MessengerAuthorizationService {
  private readonly messenger: MessengerAuthorizationServiceClient

  constructor (@Inject(MESSENGER_AUTH_CLIENT) messenger: ClientGrpc) {
    this.messenger = messenger.getService('MessengerAuthorizationService')
  }

  async checkCreateDialogAccess (
    data: {
      userIds: string[]
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkCreateDialogAccess({
      userIds: data.userIds
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetDialogAccess (
    data: {
      userIds: string[]
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkGetDialogAccess({
      userIds: data.userIds
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetChatAccess (
    data: {
      chatId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkGetChatAccess({
      chatId: data.chatId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkSendMessageAccess (
    data: {
      chatId: string
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkSendMessageAccess({
      chatId: data.chatId,
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkIsMemberOfChatAccess (
    data: {
      chatId: string
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkIsMemberOfChatAccess({
      chatId: data.chatId,
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkAuthUserAccess (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkAuthUserAccess({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }

  async checkGetConsultationsWithUnreadMessagesCount (
    data: {
      userId: string
    },
    metadata: Metadata
  ): Promise<Access> {
    return await this.messenger.checkGetConsultationsWithUnreadMessagesCount({
      userId: data.userId
    }, metadata).toPromise()
      .then(transformAccess)
  }
}
