import { Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { messengerAuthorizationServicePath, protoLoaderConfig } from '@salutdoc/protos'
import { messengerConfig } from './messenger.config'
import { MESSENGER_AUTH_CLIENT } from './messenger.constants'
import { MessengerAuthorizationService } from './messenger.service'

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: MESSENGER_AUTH_CLIENT,
        imports: [
          ConfigModule.forFeature(messengerConfig)
        ],
        inject: [messengerConfig.KEY],
        useFactory: (config: ConfigType<typeof messengerConfig>) => ({
          transport: Transport.GRPC,
          options: {
            url: config.MESSENGER_AUTHORIZATION_SERVICE_URI,
            protoPath: messengerAuthorizationServicePath,
            package: 'authorization.messenger',
            loader: protoLoaderConfig
          }
        })
      }
    ])
  ],
  providers: [
    MessengerAuthorizationService
  ],
  exports: [
    MessengerAuthorizationService
  ]
})
export class MessengerAuthorizationModule {
}
