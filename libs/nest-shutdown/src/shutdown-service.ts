import { Injectable } from '@nestjs/common'
import { EventEmitter } from 'events'

@Injectable()
export class ShutdownService {
  private readonly emitter = new EventEmitter()

  onceShutdownRequested (handler: (reason: Error) => void): void {
    this.emitter.once('shutdown-requested', handler)
  }

  requestShutdown (reason: Error): void {
    this.emitter.emit('shutdown-requested', reason)
  }
}
