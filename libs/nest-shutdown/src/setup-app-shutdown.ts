import { INestApplicationContext } from '@nestjs/common'
import { Logger } from '@salutdoc/nest-logging'
import { ShutdownService } from './shutdown-service'

export async function setupAppShutdown (app: INestApplicationContext): Promise<void> {
  const logger = await app.resolve(Logger)
  logger.setContext('nest-shutdown')
  app.get(ShutdownService).onceShutdownRequested(reason => {
    logger.error('Shutting down due to internal error', reason)
    app.close().catch(error => {
      logger.error('Failed to shutdown the app', error)
    }).then(() => {
      setTimeout(() => {
        try {
          logger.error('Application still running after 10 seconds after shutdown, exiting the process')
        } finally {
          process.exit(1)
        }
      }, 10000).unref()
    }).catch(() => {
      process.exit(1)
    })
  })
}
