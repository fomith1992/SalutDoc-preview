import { LoggerService } from '@nestjs/common'
import { Logger } from 'winston'
import { createRootLogger } from './logger.root'

export class WinstonService implements LoggerService {
  constructor (private readonly logger: Logger = createRootLogger()) {
  }

  debug (message: any, context?: string): any {
    this.logger.debug({ message, context })
  }

  error (message: any, trace?: string, context?: string): any {
    this.logger.error({ message, trace, context })
  }

  log (message: any, context?: string): any {
    this.logger.info({ message, context })
  }

  verbose (message: any, context?: string): any {
    this.logger.verbose({ message, context })
  }

  warn (message: any, context?: string): any {
    this.logger.warn({ message, context })
  }
}
