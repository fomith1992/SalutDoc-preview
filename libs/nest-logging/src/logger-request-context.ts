import { AsyncLocalStorage } from 'async_hooks'

export const loggerRequestContext = new AsyncLocalStorage<LoggerRequestContext>()

export interface LoggerRequestContext {
  protocol?: string
  endpoint?: string
  requestId?: string
}
