import { DynamicModule, Module, Scope } from '@nestjs/common'
import { APP_INTERCEPTOR } from '@nestjs/core'
import * as winston from 'winston'
import { Logger } from './logger'
import { createRootLogger, ROOT_LOGGER } from './logger.root'
import { LoggingInterceptor } from './logging-interceptor'
import { LoggingModuleOptions } from './logging.options'

@Module({})
export class LoggingModule {
  static forRoot (options?: LoggingModuleOptions): DynamicModule {
    return {
      module: LoggingModule,
      global: true,
      providers: [
        {
          provide: ROOT_LOGGER,
          useValue: createRootLogger(options?.transports)
        },
        {
          provide: Logger,
          scope: Scope.TRANSIENT,
          inject: [ROOT_LOGGER],
          useFactory (logger: winston.Logger): Logger {
            return new Logger(logger)
          }
        },
        {
          provide: APP_INTERCEPTOR,
          useClass: LoggingInterceptor
        }
      ],
      exports: [
        Logger
      ]
    }
  }
}
