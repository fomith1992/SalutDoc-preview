import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common'
import type { Request } from 'express'
import { Observable } from 'rxjs'
import { finalize, tap } from 'rxjs/operators'
import { Logger } from './logger'
import { LoggerRequestContext, loggerRequestContext } from './logger-request-context'

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor (
    private readonly logger: Logger
  ) {
    logger.setContext('LoggingInterceptor')
  }

  private static async buildContext (context: ExecutionContext): Promise<LoggerRequestContext> {
    switch (context.getType()) {
      case 'http': {
        const request = context.switchToHttp().getRequest<Request>()

        return {
          protocol: 'http',
          requestId: request.header('X-Request-ID'),
          endpoint: request.path
        }
      }
      case 'rpc': {
        const ctx = context.switchToRpc().getContext()
        try {
          const { Metadata } = await import('grpc')
          if (ctx instanceof Metadata) {
            const requestId = ctx.get('X-Request-ID')[0]

            return {
              protocol: 'grpc',
              requestId: Buffer.isBuffer(requestId) ? requestId.toString('utf8') : requestId
            }
          }
        } catch {
        }
        try {
          const { KafkaContext } = await import('@nestjs/microservices')
          if (ctx instanceof KafkaContext) {
            const requestId = ctx.getMessage().headers?.['X-Request-ID']

            return {
              protocol: 'kafka',
              requestId: Buffer.isBuffer(requestId) ? requestId.toString('utf8') : requestId
            }
          }
        } catch {
        }
        break
      }
    }
    return {}
  }

  async intercept (context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const loggerContext = await LoggingInterceptor.buildContext(context)

    return await new Promise(resolve => loggerRequestContext.run(loggerContext, () => {
      this.logger.debug('init')

      resolve(next.handle().pipe(
        obs => obs.lift({
          call: (subscriber, source: Observable<any>) => source.subscribe(
            value => loggerRequestContext.run(loggerContext, () => subscriber.next(value)),
            error => loggerRequestContext.run(loggerContext, () => subscriber.error(error)),
            () => loggerRequestContext.run(loggerContext, () => subscriber.complete())
          )
        }),
        tap(
          () => this.logger.debug('response sent'),
          error => this.logger.error('error', error)
        ),
        finalize(() => {
          this.logger.debug('complete')
        })
      ))
    }))
  }
}
