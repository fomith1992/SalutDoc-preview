import { array } from 'fp-ts'
import winston from 'winston'
import { loggerRequestContext } from './logger-request-context'

export class Logger {
  private contextMeta: {
    context?: string
    [key: string]: any
  } = {}

  constructor (private readonly logger: winston.Logger) {
  }

  setContext (context: string): void {
    this.contextMeta = {
      ...this.contextMeta,
      context
    }
  }

  setContextMeta (meta: Record<string, any>): void {
    this.contextMeta = {
      ...this.contextMeta,
      ...meta
    }
  }

  info (message: string, ...meta: Array<object | Error>): void {
    this.log('info', message, ...meta)
  }

  debug (message: string, ...meta: Array<object | Error>): void {
    this.log('debug', message, ...meta)
  }

  warn (message: string, ...meta: Array<object | Error>): void {
    this.log('warn', message, ...meta)
  }

  error (message: string, ...meta: Array<object | Error>): void {
    this.log('error', message, ...meta)
  }

  log (level: string, message: string, ...meta: Array<object | Error>): void {
    const parts = array.partition((x: object | Error): x is Error => x instanceof Error)(meta)
    this.logger.log(level, message, Object.assign({},
      loggerRequestContext.getStore(),
      this.contextMeta,
      ...parts.left,
      parts.right.length === 0 ? {} : {
        errors: parts.right.map(error => ({
          // @ts-expect-error
          name: error.name,
          // @ts-expect-error
          message: error.message,
          stack: error.stack,
          ...error
        }))
      })
    )
  }
}
