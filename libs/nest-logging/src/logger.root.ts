import winston, { Logger } from 'winston'

export const ROOT_LOGGER = 'ROOT_LOGGER'

export function createRootLogger (transports?: Logger['transports']): Logger {
  return winston.createLogger({
    level: 'debug',
    format: winston.format.combine(
      winston.format.timestamp(),
      process.env.NODE_ENV === 'development'
        ? winston.format.prettyPrint({
          colorize: true
        })
        : winston.format.json()
    ),
    transports: transports ?? [
      new winston.transports.Stream({
        stream: process.stdout
      })
    ]
  })
}
