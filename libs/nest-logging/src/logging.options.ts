import winston from 'winston'

export interface LoggingModuleOptions {
  transports?: winston.Logger['transports']
}
