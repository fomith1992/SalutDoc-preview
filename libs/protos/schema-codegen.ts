import * as fs from 'fs'
import * as pb from 'protobufjs'

const generationRoot = 'src/gen'

const scalarTypesSchema: Record<string, string> = {
  double: 't.number',
  float: 't.number',
  int32: 't.number',
  int64: 't.number',
  uint32: 't.number',
  uint64: 't.number',
  sint32: 't.number',
  sint64: 't.number',
  fixed32: 't.number',
  fixed64: 't.number',
  sfixed32: 't.number',
  sfixed64: 't.number',
  bool: 't.boolean',
  string: 't.string',
  bytes: 't.instanceOf(Uint8Array)'
}

const schemaOverrides: Record<string, string> = {
  '.google.protobuf.Timestamp': 'timestampCodec',
  '.time.Date': 'dateCodec',
  '.time.Year': 'yearCodec'
}

const imports = [
  'import Long from \'long\'',
  'import * as t from \'@salutdoc/utils/lib/transform\'',
  'import { pipe } from \'fp-ts/lib/function\'',
  'import { dateCodec, timestampCodec, yearCodec } from \'../time\''
]

const root = pb.loadSync(`${generationRoot}/api.json`).resolveAll()

const nameMap: Record<string, string> = {}

function getTypeSchemaName (type: pb.ReflectionObject | null): string {
  let name
  if (type == null || (name = nameMap[type.fullName]) == null) {
    throw new Error('unresolved type')
  }
  return name
}

function generateSchemaField (field: pb.Field, nullable = true): string {
  let type
  if (field.type in scalarTypesSchema) {
    const scalarSchema = scalarTypesSchema[field.type]
    type = field.long ? `t.union(${scalarSchema}, t.instanceOf(Long))` : scalarSchema
  } else {
    type = getTypeSchemaName(field.resolvedType)
  }
  if (field.repeated) {
    type = `t.array(${type})`
  } else if (nullable && field.typeDefault == null) {
    type = `t.nullable(${type})`
  }
  return `${field.name}: ${type}`
}

function generateTypeSchema (name: string, type: pb.Type): string {
  if (type.fullName in schemaOverrides) {
    return `const ${name} = ${schemaOverrides[type.fullName]}`
  }

  const oneofs = type.oneofsArray.map(oneof => {
    const variants = oneof.fieldsArray.map(field => {
      const code = generateSchemaField(field, false)
      return `      ${code}`
    })
    return `    t.oneof('${oneof.name}', {\n${variants.join(',\n')}\n    })`
  })
  const fields = type.fieldsArray
    .filter(field => field.partOf == null)
    .map(field => generateSchemaField(field))
  const bodySchema = `  t.type({\n${fields.map(f => `    ${f}`).join(',\n')}\n  })`
  const body = 'pipe(\n' + [
    bodySchema,
    ...oneofs.map(x => `  t.and(\n${x}\n  )`)
  ].join(',\n') + '\n)'

  return `const ${name} = ${body}`
}

function generateEnumSchema (name: string, enumType: pb.Enum): string {
  const values = enumType.values
  const body = `t.literal(${Object.values(values).join(', ')})`
  return `const ${name} = ${body}`
}

function topSort (obj: pb.ReflectionObject): pb.ReflectionObject[] {
  const sorted: pb.ReflectionObject[] = []
  const stack: pb.ReflectionObject[] = []

  function dfs (obj: pb.ReflectionObject | null): void {
    if (obj == null || sorted.includes(obj)) {
      return
    }
    if (stack.includes(obj)) {
      throw new Error(`type cycle detected: ${[...stack, obj].map(obj => obj.fullName).join('->')}`)
    }
    stack.push(obj)
    if (obj instanceof pb.Service) {
      for (const method of obj.methodsArray) {
        dfs(method.resolvedRequestType)
        dfs(method.resolvedResponseType)
      }
    }
    if (obj instanceof pb.Type) {
      for (const field of obj.fieldsArray) {
        dfs(field.resolvedType)
      }
    }
    if (obj instanceof pb.Namespace) {
      for (const nested of obj.nestedArray) {
        dfs(nested)
      }
    }
    stack.pop()
    sorted.push(obj)
  }

  dfs(obj)

  return sorted
}

try {
  const objs = topSort(root)
  const schemas: Record<string, string> = {}
  let idx = 0
  for (const obj of objs) {
    const name = `${obj.fullName.replace(/[.]/g, '')}_${idx++}`
    nameMap[obj.fullName] = name
    if (obj instanceof pb.Type) {
      schemas[obj.fullName] = generateTypeSchema(name, obj)
    }
    if (obj instanceof pb.Enum) {
      schemas[obj.fullName] = generateEnumSchema(name, obj)
    }
  }

  const schemaFields = objs
    .filter(obj => obj.fullName in schemas)
    .map(obj => `  '${obj.fullName}': ${getTypeSchemaName(obj)}`)
    .join(',\n')
  const schema = `export const schema = {\n${schemaFields}\n}\n`

  fs.mkdirSync(generationRoot, { recursive: true })
  fs.writeFileSync([generationRoot, 'schema.ts'].join('/'),
    [
      ...imports,
      '',
      ...objs
        .filter(obj => obj.fullName in schemas)
        .map(obj => schemas[obj.fullName]),
      '',
      schema
    ].join('\n')
  )
} catch (e) {
  console.error(e)
  process.exit(1)
}
