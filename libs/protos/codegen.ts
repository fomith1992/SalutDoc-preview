import { ordString } from 'fp-ts/lib/Ord'
import * as fs from 'fs'
import * as pb from 'protobufjs'

const generationRoot = 'src/gen'

const scalarTypes: Record<string, string> = {
  double: 'number',
  float: 'number',
  int32: 'number',
  int64: 'number',
  uint32: 'number',
  uint64: 'number',
  sint32: 'number',
  sint64: 'number',
  fixed32: 'number',
  fixed64: 'number',
  sfixed32: 'number',
  sfixed64: 'number',
  bool: 'boolean',
  string: 'string',
  bytes: 'Uint8Array'
}

const defaultImports = [
  '// @ts-nocheck',
  'import type * as grpc from \'grpc\'',
  'import type Long from \'long\'',
  'import type * as pb from \'protobufjs\'',
  'import type { Observable } from \'rxjs\''
]

const root = pb.loadSync(`${generationRoot}/api.json`).resolveAll()

function merge (a?: string[], b?: string[]): string[] {
  const c = [...(a ?? []), ...(b ?? [])]
  c.sort(ordString.compare)
  const res: string[] = []
  for (const s of c) {
    if (res[res.length - 1] !== s) {
      res.push(s)
    }
  }
  return res
}

type Imports = Record<string, string[]>

function mergeImports (a: Imports, b: Imports): Imports {
  return Object.keys({ ...a, ...b }).reduce((imports: Imports, module) => {
    imports[module] = merge(a[module], b[module])
    return imports
  }, {})
}

class CodeBlock {
  lines: string[] = []
  imports: Imports = {}

  addLine (line: string): void {
    this.lines.push(line)
  }

  append (block: CodeBlock): void {
    this.lines.push(...block.lines)
    this.addImports(block.imports)
  }

  addImports (...imports: Imports[]): void {
    for (const i of imports) {
      this.imports = mergeImports(this.imports, i)
    }
  }
}

interface CodeInline {
  code: string
  imports: Imports
}

function modulePath (module: string): string[] {
  return module.substr(1).replace(/_/, '-').split(/\./)
}

function writeCode (namespace: pb.Namespace, code: string): void {
  const mp = modulePath(namespace.fullName)
  const dir = [generationRoot, ...mp.slice(0, mp.length - 1)].join('/')
  fs.mkdirSync(dir, { recursive: true })
  fs.writeFileSync([dir, mp[mp.length - 1] + '.ts'].join('/'), code)
}

function processNamespace (namespace: pb.Namespace): void {
  const block = new CodeBlock()

  for (const obj of namespace.nestedArray) {
    if (obj instanceof pb.Service) {
      block.append(generateServer(obj))
      block.append(generateClient(obj))
    } else if (obj instanceof pb.Type) {
      block.append(generateInputType(obj))
      block.append(generateOutputType(obj))
    } else if (obj instanceof pb.Enum) {
      block.append(processEnum(obj))
    }
    if (obj instanceof pb.Namespace) {
      processNamespace(obj)
    }
  }

  if (block.lines.length === 0) {
    return
  }

  const pathToRoot = modulePath(namespace.fullName).slice(1).map(() => '..')
  const imports = Object.entries(block.imports)
    .map(([module, members]) => {
      const mp = [...pathToRoot, ...modulePath(module)].join('/')
      return `import { ${members.join(', ')} } from '${mp}'`
    })

  const code = [
    ...defaultImports,
    ...imports,
    '',
    ...block.lines
  ].join('\n')

  writeCode(namespace, code)
}

function useType (kind: 'Input' | 'Output', type: pb.Type | pb.Enum | null, ns?: pb.Namespace | null): CodeInline {
  if (type == null) {
    throw new Error('unresolved type')
  }
  const typeName = type instanceof pb.Enum ? type.name : `${kind}${type.name}`
  const imports: Imports = {}
  if (type.parent != null && type.parent !== ns) {
    imports[type.parent.fullName] = [typeName]
  }
  return {
    code: typeName,
    imports
  }
}

function generateServer (service: pb.Service): CodeBlock {
  function requestType (method: pb.Method): CodeInline {
    const type = useType('Input', method.resolvedRequestType, method.parent?.parent)
    if (method.requestStream ?? false) {
      return {
        ...type,
        code: `Observable<${type.code}>`
      }
    } else {
      return type
    }
  }

  function responseType (method: pb.Method): CodeInline {
    const type = useType('Output', method.resolvedResponseType, method.parent?.parent)
    return {
      ...type,
      code: `Observable<${type.code}> | Promise<${type.code}> | Promise<Observable<${type.code}>>`
    }
  }

  const block = new CodeBlock()

  block.addLine(`export interface ${service.name}Server {`)
  for (const method of service.methodsArray) {
    const name = (method.name[0]?.toLowerCase() ?? '') + method.name.substr(1)
    const request = requestType(method)
    const response = responseType(method)
    const line = `  ${name} (request: ${request.code}, metadata: grpc.Metadata): ${response.code}`
    block.addLine(line)
    block.addImports(request.imports, response.imports)
  }
  block.addLine('}')

  return block
}

function generateClient (service: pb.Service): CodeBlock {
  function requestType (method: pb.Method): CodeInline {
    const type = useType('Output', method.resolvedRequestType, method.parent?.parent)
    if (method.requestStream ?? false) {
      return {
        ...type,
        code: `Observable<${type.code}>`
      }
    } else {
      return type
    }
  }

  function responseType (method: pb.Method): CodeInline {
    const type = useType('Input', method.resolvedResponseType, method.parent?.parent)
    return {
      ...type,
      code: `Observable<${type.code}>`
    }
  }

  const block = new CodeBlock()

  block.addLine(`export interface ${service.name}Client {`)
  for (const method of service.methodsArray) {
    const name = (method.name[0]?.toLowerCase() ?? '') + method.name.substr(1)
    const request = requestType(method)
    const response = responseType(method)
    const line = `  ${name} (request: ${request.code}, metadata?: grpc.Metadata): ${response.code}`
    block.addLine(line)
    block.addImports(request.imports, response.imports)
  }
  block.addLine('}')

  return block
}

function processField (kind: 'Input' | 'Output', field: pb.Field, nullable = true): CodeInline {
  let type: CodeInline
  if (field.type in scalarTypes) {
    type = {
      code: scalarTypes[field.type] + (field.long ? ' | Long' : ''),
      imports: {}
    }
  } else {
    type = useType(kind, field.resolvedType, field.parent?.parent)
  }
  const finalType = field.repeated
    ? `Array<${type.code}>`
    : nullable && field.typeDefault == null
      ? `${type.code} | null`
      : type.code
  return {
    code: `${field.name}${kind === 'Output' ? '?' : ''}: ${finalType}`,
    imports: type.imports
  }
}

function generateInputType (type: pb.Type): CodeBlock {
  const oneofs = type.oneofsArray.map(oneof => {
    const variants = oneof.fieldsArray.map(field => {
      const f = processField('Input', field, false)
      return {
        code: `{
  ${oneof.name}: '${field.name}'
  ${f.code}
}`,
        imports: f.imports
      }
    })
    const code = [
      `{ ${oneof.name}: null }`,
      ...variants.map(x => x.code)
    ].join(' | ')
    return {
      code,
      imports: variants.map(x => x.imports).reduce(mergeImports, {})
    }
  })
  const fields = type.fieldsArray
    .filter(field => field.partOf == null)
    .map(field => processField('Input', field))
  const body = [
    ...oneofs.map(x => `(${x.code})`),
    `{\n${fields.map(f => `  ${f.code}`).join('\n')}\n}`
  ].join(' & ')

  const block = new CodeBlock()
  block.addLine(`export type Input${type.name} = ${body}`)
  block.addImports(...oneofs.map(f => f.imports))
  block.addImports(...fields.map(f => f.imports))
  return block
}

function generateOutputType (type: pb.Type): CodeBlock {
  const fields = type.fieldsArray
    .map(field => processField('Output', field))
  const body = `{
${fields.map(f => `  ${f.code}`).join('\n')}
}`

  const block = new CodeBlock()
  block.addLine(`export type Output${type.name} = ${body}`)
  block.addImports(...fields.map(f => f.imports))
  return block
}

function processEnum (enumType: pb.Enum): CodeBlock {
  const values = enumType.values
  const body = `{
${Object.keys(values).map(key => `  ${key} = ${values[key]}`).join(',\n')}
}`
  const block = new CodeBlock()
  block.addLine(`export enum ${enumType.name} ${body}`)
  return block
}

try {
  processNamespace(root)
} catch (e) {
  console.error(e)
  process.exit(1)
}
