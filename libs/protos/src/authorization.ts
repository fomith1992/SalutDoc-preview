import { join } from 'path'

function createPath (service: string, file = 'service'): string {
  return join(__dirname, '..', 'proto', 'authorization', service, `${file}.proto`)
}

export const postsAuthorizationServicePath = createPath('posts')
export const communitiesAuthorizationServicePath = createPath('communities')
export const worksAuthorizationServicePath = createPath('works')
export const questionsAuthorizationServicePath = createPath('questions')
export const commentsAuthorizationServicePath = createPath('comments')
export const profileVerifierAuthorizationServicePath = createPath('profile_verifier')
export const communityMembershipAuthorizationServicePath = createPath('community_membership')
export const userFollowAuthorizationServicePath = createPath('user_follow')
export const feedAuthorizationServicePath = createPath('feed')
export const messengerAuthorizationServicePath = createPath('messenger')
export const consultationsAuthorizationServicePath = createPath('consultations')
export const paymentsAuthorizationServicePath = createPath('payments')
export const notificationsAuthorizationServicePath = createPath('notifications')
export const filesAuthorizationServicePath = createPath('files')
