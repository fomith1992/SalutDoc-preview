import { join } from 'path'

function createPath (service: string, file = 'service'): string {
  return join(__dirname, '..', 'proto', 'api', service, `${file}.proto`)
}

export const includePath = join(__dirname, '..', 'proto') // move to index
export const phoneVerifierPath = createPath('phone_verifier')
export const postsServicePath = createPath('posts')
export const postsEventsPath = createPath('posts', 'events')
export const userProfilesServicePath = createPath('user_profiles')
export const userProfilesEventsPath = createPath('user_profiles', 'events')
export const accountsServicePath = createPath('accounts')
export const communitiesServicePath = createPath('communities')
export const communitiesEventsPath = createPath('communities', 'events')
export const portfolioServicePath = createPath('portfolio')
export const portfolioEventsPath = createPath('portfolio', 'events')
export const likesServicePath = createPath('likes')
export const likesEventsPath = createPath('likes', 'events')
export const commentsServicePath = createPath('comments')
export const questionsServicePath = createPath('questions')
export const questionsEventsPath = createPath('questions', 'events')
export const profileVerifierPath = createPath('profile_verifier')
export const userFollowServicePath = createPath('user_follow')
export const userFollowEventsPath = createPath('user_follow', 'events')
export const communityMembershipServicePath = createPath('community_membership')
export const communityMembershipEventsPath = createPath('community_membership', 'events')
export const profileVerifierEventsPath = createPath('profile_verifier', 'events')
export const messengerServicePath = createPath('messenger')
export const messengerEventsPath = createPath('messenger', 'events')
export const feedServicePath = createPath('feed')
export const notificationsServicePath = createPath('notifications')
export const consultationsServicePath = createPath('consultations')
export const consultationsEventsPath = createPath('consultations', 'events')
export const filesServicePath = createPath('files')
export const paymentsServicePath = createPath('payments')

export const protoLoaderConfig = {
  // Set default values for all fields for better interoperability with other platforms (Java, Go, etc.)
  defaults: true,
  oneofs: true, // Set virtual oneof properties to the present field's name
  includeDirs: [includePath]
}
