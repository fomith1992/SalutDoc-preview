import { Instant, LocalDate, Year, ZoneId, ZoneOffset } from '@js-joda/core'
import { pipe } from 'fp-ts/lib/function'
import Long from 'long'
import * as t from '@salutdoc/utils/lib/transform'
import { InputTimestamp, OutputTimestamp } from './gen/google/protobuf'
import { InputDate, InputYear, OutputDate, OutputYear } from './gen/time'

export const timestampCodec = pipe(
  t.type({
    seconds: t.union(t.number, t.fromGuard(Long.isLong, 'Long')),
    nanos: t.number
  }),
  t.map(timestamp => {
    const seconds = (Long.isLong(timestamp.seconds) ? timestamp.seconds.toNumber() : timestamp.seconds)
    return Instant.ofEpochSecond(seconds, timestamp.nanos)
  })
)

export const dateCodec = pipe(
  t.type({
    epochDays: t.number
  }),
  t.map(
    ({ epochDays }) => LocalDate.ofEpochDay(epochDays)
  )
)

export const yearCodec = pipe(
  t.type({
    year: t.number
  }),
  t.map(
    ({ year }) => Year.of(year)
  )
)

export function encodeTimestamp (date: Date): OutputTimestamp {
  return {
    seconds: (date.getTime() / 1000) | 0,
    nanos: (date.getTime() % 1000) * 1_000_000
  }
}

export function decodeTimestamp (timestamp: InputTimestamp | null): Date {
  if (timestamp == null) {
    return new Date(0)
  }
  const seconds = (timestamp.seconds instanceof Long ? timestamp.seconds.toNumber() : timestamp.seconds)
  return new Date(seconds * 1000 + timestamp.nanos / 1_000_000)
}

export function encodeDate (date: Date): OutputDate {
  return {
    epochDays: Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.UTC).toLocalDate().toEpochDay()
  }
}

export function decodeDate (date: InputDate): Date {
  return new Date(LocalDate.ofEpochDay(date.epochDays).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli())
}

export function encodeYear (year: number): OutputYear {
  return { year }
}

export function decodeYear (year: InputYear): number {
  return year.year
}
