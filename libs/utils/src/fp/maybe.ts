export type Maybe<T extends {}> = T | null | undefined

export function map<T extends {}, R extends {}> (
  mapper: (value: T) => Maybe<R>
): (maybe: Maybe<T>) => Maybe<R> {
  return maybe => maybe == null ? maybe : mapper(maybe)
}

export function throwIfNil (onNil: () => Error): <T extends {}>(maybe: Maybe<T>) => T {
  return value => {
    if (value == null) {
      throw onNil()
    } else {
      return value
    }
  }
}

export function expectNonNil<T extends {}> (maybe: Maybe<T>): T {
  if (maybe == null) {
    throw new Error(`non-nil expected, but ${String(maybe)} found`)
  } else {
    return maybe
  }
}

export function assertNonNil<T extends {}> (maybe: Maybe<T>): asserts maybe is T {
  expectNonNil(maybe)
}

export function isNonNil<T> (maybe: T): maybe is NonNullable<T> {
  return maybe != null
}

export const filter: {
  <T extends {}, U extends T> (
    refinement: (value: T) => value is U
  ): (maybe: Maybe<T>) => Maybe<U>
  <T extends {}> (predicate: (value: T) => boolean): (maybe: Maybe<T>) => Maybe<T>
} = <T extends {}> (
  predicate: (value: T) => boolean
) => (value: Maybe<T>) => value == null || predicate(value) ? value : undefined

export function fold<T extends {}, R> (
  onNone: () => R,
  onSome: (value: T) => R
): (maybe: Maybe<T>) => R {
  return maybe => maybe == null ? onNone() : onSome(maybe)
}

export function getOrElseW<T extends {}, U> (fn: () => U): (maybe: Maybe<T>) => T | U {
  return maybe => maybe ?? fn()
}
