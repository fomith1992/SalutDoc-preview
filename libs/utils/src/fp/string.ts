export function ifEmpty<T> (defaultValue: T): (value: string) => string | T {
  return value => value === '' ? defaultValue : value
}
