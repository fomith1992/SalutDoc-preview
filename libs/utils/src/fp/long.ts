import * as t from '../transform'
import Long from 'long'

export const long = t.fromGuard(
  (value: unknown): value is Long => value instanceof Long,
  'Long'
)
