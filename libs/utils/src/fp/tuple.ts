import { Ord } from 'fp-ts/Ord'

export function map<T extends any[], B> (
  mapper: (value: T[number]) => B
): (tuple: T) => { [K in keyof T]: B } {
  return tuple => tuple.map(mapper) as { [K in keyof T]: B }
}

export function sort<T extends any[]> (
  order: Ord<T[number]>
): (tuple: T) => T {
  return tuple => (tuple.slice() as typeof tuple).sort(order.compare)
}
