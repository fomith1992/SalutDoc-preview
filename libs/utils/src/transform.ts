import * as ar from 'fp-ts/lib/Array'
import * as either from 'fp-ts/lib/Either'
import { Either } from 'fp-ts/lib/Either'
import { flow } from 'fp-ts/lib/function'
import { Monoid } from 'fp-ts/lib/Monoid'
import * as nea from 'fp-ts/lib/NonEmptyArray'
import { NonEmptyArray } from 'fp-ts/lib/NonEmptyArray'
import { pipe } from 'fp-ts/lib/pipeable'
import * as rcd from 'fp-ts/lib/Record'
import { getFirstSemigroup, getObjectSemigroup, Semigroup, semigroupVoid } from 'fp-ts/lib/Semigroup'
import * as tree from 'fp-ts/lib/Tree'
import { Tree } from 'fp-ts/lib/Tree'

export interface Transformer<I, O> {
  guard: (input: unknown) => Either<TextForest, I>

  transform: (input: I) => Either<TextForest, O>
}

export type TextForest = NonEmptyArray<Tree<string>>
const errorForestSemigroup = nea.getSemigroup<Tree<string>>()
const validation = either.getValidation(errorForestSemigroup)

const objectMonoid: Monoid<object> = {
  ...getObjectSemigroup(),
  empty: {}
}

export type InputOf<T extends Transformer<any, any>> = T extends Transformer<infer I, any> ? I : never
export type OutputOf<T extends Transformer<any, any>> = T extends Transformer<any, infer O> ? O : never

export function make<I, O> (
  guard: (input: unknown) => Either<TextForest, I>,
  transform: (encoded: I) => Either<TextForest, O>
): Transformer<I, O> {
  return {
    guard,
    transform
  }
}

export function fromGuard<T> (
  guard: (input: unknown) => input is T,
  expected: string
): Transformer<T, T> {
  return make(
    input => guard(input) ? success(input) : failure(`expected input to be ${expected}`),
    success
  )
}

export function instanceOf<T> (constructor: new(...args: any[]) => T): Transformer<T, T> {
  return make(
    input => input instanceof constructor
      ? success(input)
      : failure(`expected input to be instance of ${constructor.name}`),
    success
  )
}

export function error (message: string, forest?: Array<Tree<string>>): TextForest {
  return [tree.make(message, forest)]
}

export function failure<R = never> (message: string, forest?: Array<Tree<string>>): Either<TextForest, R> {
  return either.left(error(message, forest))
}

export function success<R> (value: R): Either<TextForest, R> {
  return either.right(value)
}

export const string = make(
  input => typeof input === 'string'
    ? either.right(input)
    : failure(`expected typeof input === 'string', but was '${typeof input}'`),
  success
)

export const number = make(
  input => typeof input === 'number'
    ? either.right(input)
    : failure(`expected typeof input === 'number', but was '${typeof input}'`),
  success
)

export const boolean = make(
  input => typeof input === 'boolean'
    ? either.right(input)
    : failure(`expected typeof input === 'boolean', but was '${typeof input}'`),
  success
)

export const unknownRecord = make(
  input => Object.prototype.toString.call(input) === '[object Object]'
    ? success(input as Record<string, unknown>)
    : failure(`expected input to be '[object Object]', but was '${Object.prototype.toString.call(input)}'`),
  success
)

export const unknownArray = make(
  input => Array.isArray(input)
    ? success(input as unknown[])
    : failure(`expected input to be an array, but was '${Object.prototype.toString.call(input)}'`),
  success
)

type OptionalKeys<P extends {
  [key: string]: Transformer<any, any>
}> = {
  [K in keyof P]: undefined extends InputOf<P[K]> ? K : never
}[keyof P]

export function type<P extends {
  [key: string]: Transformer<any, any>
}> (properties: P): Transformer<{
  [K in OptionalKeys<P>]?: InputOf<P[K]>
} & {
  [K in Exclude<keyof P, OptionalKeys<P>>]: InputOf<P[K]>
}, {
  [K in keyof P]: OutputOf<P[K]>
}> {
  return make(
    flow(
      unknownRecord.guard,
      either.chainFirst(input => pipe(
        properties,
        rcd.traverseWithIndex(validation)((key, { guard }) => prop(guard)(key, input[key]))
      )),
      either.map(x => x as any) // trust me, I'm an engineer
    ),
    input => pipe(
      properties,
      rcd.traverseWithIndex(validation)((key, { transform }) => prop(transform)(key, input[key])),
      either.map(x => x as any) // trust me, I'm an engineer
    )
  )
}

export function partial<P extends {
  [key: string]: Transformer<any, any>
}> (properties: P): Transformer<{
  [K in keyof P]?: InputOf<P[K]>
}, {
  [K in keyof P]?: OutputOf<P[K]>
}> {
  return make(
    flow(
      unknownRecord.guard,
      either.chainFirst(input => pipe(
        properties,
        rcd.filterWithIndex(key => key in input),
        rcd.traverseWithIndex(validation)((key, { guard }) => prop(guard)(key, input[key]))
      )),
      either.map(x => x as {
        [K in keyof P]?: InputOf<P[K]>
      })
    ),
    input => pipe(
      properties,
      rcd.filterWithIndex(key => key in input),
      rcd.traverseWithIndex(validation)((key, { transform }) => prop(transform)(key, input[key])),
      either.map(x => x as {
        [K in keyof P]?: OutputOf<P[K]>
      })
    )
  )
}

export function nullable<I, O> (transformer: Transformer<I, O>): Transformer<I | null, O | null> {
  return make(
    (input: unknown) => input === null ? either.right(null) : transformer.guard(input),
    input => input === null ? either.right(null) : transformer.transform(input)
  )
}

export function optional<I, O> (transformer: Transformer<I, O>): Transformer<I | undefined, O | undefined> {
  return make(
    (input: unknown) => input === undefined ? either.right(undefined) : transformer.guard(input),
    input => input === undefined ? either.right(undefined) : transformer.transform(input)
  )
}

export function oneof<N extends string, V extends { [name: string]: Transformer<any, any> }> (
  name: N,
  variants: V
): Transformer<{ [Q in N]: null } | { [P in keyof V]: { [Q in N]: P } & { [Q in P]: InputOf<V[P]> } }[keyof V],
  { [Q in N]: null } | { [P in keyof V]: { [Q in N]: P } & { [Q in P]: OutputOf<V[P]> } }[keyof V]> {
  const variantType = partial({
    [name]: nullable(string)
  })
  return make(
    flow(
      variantType.guard,
      either.chainFirst(x => {
        const variantName = x[name]
        if (variantName == null) {
          return success(x)
        }
        const variant = variants[variantName]
        if (variant == null) {
          return failure(`unexpected oneof variant ${variantName}`)
        }
        const derivedType = type({
          [variantName]: variant
        })
        return derivedType.guard(x)
      }),
      either.map(x => x as any) // TODO any?
    ),
    input => pipe(
      variantType.transform(input),
      either.chain(x => {
        const variantName = x[name]
        if (variantName == null) {
          return either.right({
            [name]: null
          })
        }
        const variant = variants[variantName]
        if (variant == null) {
          return failure(`unexpected oneof variant ${variantName}`)
        }
        const derivedType = type({
          [variantName]: variant
        })
        return pipe(
          derivedType.transform(input),
          either.map(obj => ({
            ...obj,
            [name]: variantName
          })),
          either.map(x => x as any) // TODO any?
        )
      })
    )
  )
}

export function chain<O, A> (
  transform: (input: O) => Either<TextForest, A>
): <I>(codec: Transformer<I, O>) => Transformer<I, A> {
  return transformer => make(
    transformer.guard,
    flow(
      transformer.transform,
      either.chain(transform)
    )
  )
}

export function composeShape<O, S extends { [K in keyof O]?: Transformer<O[K], any>}> (
  shape: S
): <I>(codec: Transformer<I, O>) =>
  Transformer<I, { [K in keyof O]: S[K] extends NonNullable<S[K]> ? OutputOf<S[K]> : O[K] }> {
  return chain(rcd.traverseWithIndex(either.getApplicativeValidation(errorForestSemigroup))((key, value) => {
    const transformer = (shape as any)[key] as Transformer<typeof value, any>
    return transformer == null ? success(value) : pipe(value,
      transformer.guard,
      either.chain(transformer.transform)
    )
  }) as any)
}

export function map<O, A> (
  transform: (input: O) => A
): <I>(codec: Transformer<I, O>) => Transformer<I, A> {
  return transformer => make(
    transformer.guard,
    flow(
      transformer.transform,
      either.map(transform)
    )
  )
}

function getUnionSemigroup<L, R> (semigroup: Semigroup<L>): Semigroup<Either<L, R>> {
  return {
    concat: (a, b) => pipe(
      a,
      either.fold(
        aLeft => pipe(
          b,
          either.fold(
            bLeft => either.left(semigroup.concat(aLeft, bLeft)),
            bRight => either.right(bRight)
          )
        ),
        aRight => either.right(aRight)
      )
    )
  }
}

export function union<T extends NonEmptyArray<Transformer<any, any>>> (
  ...transformers: T
): Transformer<InputOf<T[number]>, OutputOf<T[number]>> {
  return make(
    input => pipe(
      transformers,
      nea.map(transformer => transformer.guard(input)),
      nea.fold(getUnionSemigroup(errorForestSemigroup))
    ),
    input => pipe(
      transformers,
      nea.map(transformer => pipe(
        transformer.guard(input),
        either.chain(transformer.transform)
      )),
      nea.fold(getUnionSemigroup(errorForestSemigroup))
    )
  )
}

export function array<I, O> ({ guard, transform }: Transformer<I, O>): Transformer<I[], O[]> {
  return make(
    flow(
      unknownArray.guard,
      either.chainFirst(ar.traverseWithIndex(either.getValidation(errorForestSemigroup))(prop(guard))),
      either.map(x => x as I[])
    ),
    ar.traverseWithIndex(either.getValidation(errorForestSemigroup))(prop(transform))
  )
}

export function literal<T extends NonEmptyArray<string | number | boolean | null>> (
  ...variants: T
): Transformer<T[number], T[number]> {
  return make(
    input => (variants as unknown[]).includes(input)
      ? success(input as T[number])
      : failure(`expected ${variants.join(' | ')}`),
    success
  )
}

export function and<I1, O1 extends object> (
  second: Transformer<I1, O1>
): <I2, O2 extends object>(first: Transformer<I2, O2>) => Transformer<I1 & I2, O1 & O2> {
  return <I2, O2 extends object> (first: Transformer<I2, O2>): Transformer<I1 & I2, O1 & O2> => make(
    input => pipe(
      [first.guard(input), second.guard(input)],
      nea.map(either.map<unknown, undefined>(() => undefined)),
      nea.fold(either.getValidationSemigroup(errorForestSemigroup, semigroupVoid)),
      either.map(() => input as I1 & I2)
    ),
    input => pipe(
      [first.transform(input), second.transform(input)],
      nea.fold(either.getValidationSemigroup(errorForestSemigroup, objectMonoid)),
      either.map(output => output as O1 & O2)
    )
  )
}

export function or<I1, O1> (
  second: Transformer<I1, O1>
): <I2, O2>(first: Transformer<I2, O2>) => Transformer<I1 | I2, O1 | O2> {
  return first => make(
    input => pipe(
      [
        first.guard(input),
        second.guard(input)
      ],
      nea.fold(either.getValidationSemigroup(errorForestSemigroup, getFirstSemigroup()))
    ),
    input => pipe(
      [
        pipe(first.guard(input), either.chain(first.transform)),
        pipe(second.guard(input), either.chain(second.transform))
      ],
      nea.fold(either.getValidationSemigroup(errorForestSemigroup, getFirstSemigroup()))
    )
  )
}

export function lazy<I, O> (factory: () => Transformer<I, O>): Transformer<I, O> {
  return make(
    input => factory().guard(input),
    input => factory().transform(input)
  )
}

export function record<I, O> (
  { guard, transform }: Transformer<I, O>
): Transformer<Record<string, I>, Record<string, O>> {
  return make(
    flow(
      unknownRecord.guard,
      either.chainFirst(rcd.traverseWithIndex(validation)(prop(guard))),
      either.map(x => x as Record<string, I>)
    ),
    rcd.traverseWithIndex(validation)(prop(transform))
  )
}

function prop<I, O> (
  mapper: (input: I) => Either<TextForest, O>
): (key: string | number, value: I) => Either<TextForest, O> {
  return (key, value) => pipe(
    mapper(value),
    either.orElse(errors => failure(`property ${key}:`, errors))
  )
}

export function defaulted<O> (
  factory: () => O
): <I>(transformer: Transformer<I, O>) => Transformer<I | null | undefined, O> {
  return <I> (transformer: Transformer<I, O>) => make<I | null | undefined, O>(
    input => input == null ? success(input as null | undefined) : transformer.guard(input),
    input => input == null ? success(factory()) : transformer.transform(input)
  )
}

export function transformOrThrow<I, O> (transformer: Transformer<I, O>): (input: unknown) => O {
  return flow(
    transformer.guard,
    either.chain(transformer.transform),
    either.getOrElseW(errors => {
      throw new Error(tree.drawForest(errors))
    })
  )
}
