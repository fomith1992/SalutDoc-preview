import * as t from '../transform'
import { TextForest, Transformer, chain } from '../transform'
import * as either from 'fp-ts/lib/Either'
import { Either } from 'fp-ts/lib/Either'
import * as rec from 'fp-ts/lib/Record'
import { pipe } from 'fp-ts/lib/function'
import * as nea from 'fp-ts/lib/NonEmptyArray'
import { NonEmptyArray } from 'fp-ts/lib/NonEmptyArray'
import { getLastSemigroup, Semigroup } from 'fp-ts/lib/Semigroup'
import * as tree from 'fp-ts/lib/Tree'
import { Forest } from 'fp-ts/lib/Tree'

export interface Validator<T, R extends T = T> {
  description: TextForest

  validate: (value: T) => Either<TextForest, R>
}

export type ResultOf<V extends Validator<any, any>> = V extends Validator<any, infer R> ? R : never

export function make<T, R extends T = T> (
  description: string | TextForest,
  validate: (value: T) => Either<TextForest, R>
): Validator<T, R> {
  return {
    description: typeof description === 'string' ? [tree.make(description)] : description,
    validate
  }
}

const textForestSemigroup = nea.getSemigroup<TextForest[0]>()
export const validation = either.getValidation(textForestSemigroup)

function getValidationSemigroup<T> (): Semigroup<Either<TextForest, T>> {
  return either.getValidationSemigroup(textForestSemigroup, getLastSemigroup<T>())
}

export function validated<D, V extends D> (
  ...validations: [Validator<D, V>, ...Array<Validator<D, V>>]
): <E>(codec: Transformer<E, D>) => Transformer<E, V> {
  const combined = all(...validations)
  return t.chain(combined.validate)
}

export function prop<T, K extends keyof T, R extends T[K]> (
  key: K,
  ...validators: [Validator<T[K], R>, ...Array<Validator<T[K], R>>]
): Validator<T, T & { [Q in K]: R }> {
  const validator = all(...validators)
  const { description } = validator
  return make(
    [tree.make(String(key), description)],
    value => pipe(
      validator.validate(value[key]),
      either.orElse(errors => failure(`property ${String(key)} validation failed`, errors)),
      either.map(() => value as T & { [Q in K]: R })
    )
  )
}

export function all<A, B extends A> (
  v0: Validator<A, B>
): Validator<A, B>
export function all<A, B extends A, C extends B> (
  v0: Validator<A, B>,
  v1: Validator<B, C>
): Validator<A, C>
export function all<A, B extends A, C extends B, D extends C> (
  v0: Validator<A, B>,
  v1: Validator<B, C>,
  v2: Validator<C, D>
): Validator<A, D>
export function all<A, B extends A, C extends B, D extends C, E extends D> (
  v0: Validator<A, B>,
  v1: Validator<B, C>,
  v2: Validator<C, D>,
  v3: Validator<D, E>
): Validator<A, E>
export function all<A, B extends A, C extends B, D extends C, E extends D, F extends E> (
  v0: Validator<A, B>,
  v1: Validator<B, C>,
  v2: Validator<C, D>,
  v3: Validator<D, E>,
  v4: Validator<E, F>
): Validator<A, F>
export function all<T, R extends T> (...validators: NonEmptyArray<Validator<T, R>>): Validator<T, R>
export function all<T, R extends T> (...validators: NonEmptyArray<Validator<T, R>>): Validator<T, R> {
  return make(
    pipe(
      validators,
      nea.chain(({ description }) => description)
    ),
    value => pipe(
      validators,
      nea.map(validator => validator.validate(value)),
      nea.fold(getValidationSemigroup())
    )
  )
}

export function success<R> (value: R): Either<TextForest, R> {
  return either.right(value)
}

export function failure<R = never> (message: string, forest?: Forest<string>): Either<TextForest, R> {
  return either.left([tree.make(message, forest)])
}

export function equals<T> (expectedValue: T): Validator<T> {
  return make(
    `equals ${String(expectedValue)}`,
    value => value === expectedValue ? success(value) : failure('unexpected value')
  )
}

export function lazy<T> (description: string | TextForest, factory: () => Validator<T>): Validator<T> {
  return make(description, value => factory().validate(value))
}

export function nonnull<T> (): Validator<T | null, T> {
  return make(
    'nonnull',
    value => value !== null ? success(value) : failure('unexpected null value')
  )
}

export function required<T> (): Validator<T | null | undefined, T> {
  return make(
    'required',
    value => value != null ? success(value) : failure(`unexpected ${String(value)} value`)
  )
}

export function defined<T> (): Validator<T | undefined, T> {
  return make(
    'defined',
    value => value !== undefined ? success(value) : failure('unexpected undefined value')
  )
}

type ExplicitKeys<T> = {
  [K in keyof T]: T[K] extends NonNullable<T[K]> ? K : never
}[keyof T]

export function validatedShape<O, S extends {
  [K in keyof O]?: Validator<O[K], any>
}> (shape: S): <I>(transformer: Transformer<I, O>) => Transformer<I, O & {
  [K in ExplicitKeys<S>]: ResultOf<S[K]>
}> {
  return chain(rec.traverseWithIndex(either.getApplicativeValidation(textForestSemigroup))((key, value) => {
    const validator = (shape as Record<string, Validator<any>>)[key]
    return validator == null ? success(value) : validator.validate(value)
  }) as any)
}
