import { LocalDate } from '@js-joda/core'
import { failure, make, success, Validator } from './base'

export function min (minDate: LocalDate): Validator<LocalDate> {
  return make(
    `date not before ${minDate.toString()}`,
    date => !date.isBefore(minDate)
      ? success(date)
      : failure(`expected date to be not before ${minDate.toString()}, but was ${date.toString()}`)
  )
}

export function max (maxDate: LocalDate): Validator<LocalDate> {
  return make(
    `date not after ${maxDate.toString()}`,
    date => !date.isAfter(maxDate)
      ? success(date)
      : failure(`expected date to be at most ${maxDate.toString()}, but was ${date.toString()}`)
  )
}
