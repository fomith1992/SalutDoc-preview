import { failure, make, success, Validator } from './base'
import { Year } from '@js-joda/core'

export function min (minYear: Year | number): Validator<Year> {
  return make(
    `year not before ${String(minYear)}`,
    year => (typeof minYear === 'number' ? year.value() >= minYear : !year.isBefore(minYear))
      ? success(year)
      : failure(`expected year to be not before ${String(minYear)}, but was ${year.value()}`)
  )
}

export function max (maxYear: Year | number): Validator<Year> {
  return make(
    `year not after ${String(maxYear)}`,
    year => (typeof maxYear === 'number' ? year.value() <= maxYear : !year.isAfter(maxYear))
      ? success(year)
      : failure(`expected year to be not after ${String(maxYear)}, but was ${year.value()}`)
  )
}
