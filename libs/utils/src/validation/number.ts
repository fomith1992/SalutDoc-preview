import { failure, make, success, Validator } from './base'

export function min (minValue: number): Validator<number> {
  return make(
    `number at least ${minValue}`,
    value => value >= minValue
      ? success(value)
      : failure(`expected number to be at least ${minValue}, but was ${value}`)
  )
}

export function max (maxValue: number): Validator<number> {
  return make(
    `number at most ${maxValue}`,
    value => value <= maxValue
      ? success(value)
      : failure(`expected number to be at most ${maxValue}, but was ${value}`)
  )
}
