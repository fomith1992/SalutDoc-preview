import { failure, make, success, Validator } from './base'

export function min (minLength: number): Validator<string> {
  return make(
    `string at least ${minLength} characters long`,
    x => x.length >= minLength
      ? success(x)
      : failure(`expected string to be at least ${minLength} characters long, but was ${x.length}`)
  )
}

export function max (maxLength: number): Validator<string> {
  return make(
    `string at most ${maxLength} characters long`,
    x => x.length <= maxLength
      ? success(x)
      : failure(`expected string to be at most ${maxLength} characters long, but was ${x.length}`)
  )
}

export function matches (regex: RegExp): Validator<string> {
  return make(
    `string matching ${regex.source} regexp`,
    x => regex.test(x)
      ? success(x)
      : failure(`expected string matching ${regex.source}, but was "${x}"`)
  )
}
