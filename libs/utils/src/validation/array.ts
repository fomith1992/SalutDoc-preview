import { validation, failure, make, success, Validator } from './base'
import * as tree from 'fp-ts/lib/Tree'
import * as ar from 'fp-ts/lib/Array'

export function min<T extends any[]> (minLength: number): Validator<T> {
  return make(
    `array at least ${minLength} elements long`,
    value => value.length >= minLength
      ? success(value)
      : failure(`expected array to be at least ${minLength} elements long, but was ${value.length}`)
  )
}

export function max<T extends any[]> (maxLength: number): Validator<T> {
  return make(
    `array at most ${maxLength} elements long`,
    value => value.length <= maxLength
      ? success(value)
      : failure(`expected array to be at most ${maxLength} elements long, but was ${value.length}`)
  )
}

export function each<T, R extends T> (validator: Validator<T, R>): Validator<T[], R[]> {
  return make(
    [tree.make('each element', validator.description)],
    ar.traverse(validation)(validator.validate)
  )
}
