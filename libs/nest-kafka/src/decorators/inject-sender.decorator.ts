import { Inject } from '@nestjs/common'
import { getSenderToken } from '../utils'

export const InjectSender = (name: string): ParameterDecorator => Inject(getSenderToken(name))
