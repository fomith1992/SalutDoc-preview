import { Inject } from '@nestjs/common'
import { getProducerToken } from '../utils'

export const InjectProducer = (name?: string): ParameterDecorator => Inject(getProducerToken(name))
