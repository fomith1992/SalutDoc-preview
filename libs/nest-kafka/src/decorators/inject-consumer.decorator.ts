import { Inject } from '@nestjs/common'
import { getConsumerToken } from '../utils'

export const InjectConsumer = (name?: string): ParameterDecorator => Inject(getConsumerToken(name))
