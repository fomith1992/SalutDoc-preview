import { Inject } from '@nestjs/common'
import { getCodecToken } from '../utils'

export const InjectCodec = (name: string): ParameterDecorator => Inject(getCodecToken(name))
