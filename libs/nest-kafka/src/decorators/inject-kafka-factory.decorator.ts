import { Inject } from '@nestjs/common'
import { getKafkaFactoryToken } from '../utils'

export const InjectKafkaFactory = (name?: string): ParameterDecorator => Inject(getKafkaFactoryToken(name))
