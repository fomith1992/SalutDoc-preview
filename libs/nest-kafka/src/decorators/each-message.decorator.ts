import { SetMetadata } from '@nestjs/common'
import { KAFKA_MODULE_CONSUMER_EACH_MESSAGE } from '../kafka.constants'

export interface EachMessageOptions {
  subscription: string
  topic?: string | RegExp
}

export function EachMessage (options?: EachMessageOptions): MethodDecorator {
  return SetMetadata(KAFKA_MODULE_CONSUMER_EACH_MESSAGE, options ?? {})
}
