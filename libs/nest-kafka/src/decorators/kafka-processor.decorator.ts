import { SetMetadata } from '@nestjs/common'
import { KAFKA_MODULE_PROCESSOR } from '../kafka.constants'

export const KafkaProcessor = (): ClassDecorator => SetMetadata(KAFKA_MODULE_PROCESSOR, true)
