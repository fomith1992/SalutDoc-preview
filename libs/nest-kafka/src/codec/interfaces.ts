import type { IConversionOptions } from 'protobufjs'
import { PayloadEncoder } from '../producer'

export interface ProtobufCodecOptions {
  name: string
  type: string
  protoPath: string
  includePath: string
  conversion?: IConversionOptions
}

export interface KafkaCodec<I, O = I> extends PayloadEncoder<I> {
  decode: (buffer: Buffer, length: number) => O
}
