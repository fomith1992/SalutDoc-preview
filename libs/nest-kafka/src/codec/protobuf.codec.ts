import { OnModuleInit } from '@nestjs/common'
import path from 'path'
import { Root, Type } from 'protobufjs'
import { KafkaCodec, ProtobufCodecOptions } from './interfaces'

export class ProtobufCodec<T> implements KafkaCodec<T>, OnModuleInit {
  private message!: Type

  constructor (private readonly options: ProtobufCodecOptions) {
  }

  async onModuleInit (): Promise<void> {
    const { type, protoPath, includePath } = this.options
    const root = new Root()
    root.resolvePath = (_origin, target) => {
      return path.isAbsolute(target) ? target : path.join(includePath, target)
    }
    await root.load(protoPath)
    this.message = root.lookupType(type)
  }

  encodePayload (value: T): Buffer {
    const error = this.message.verify(value)
    if (error != null) {
      throw new Error(`Invalid object for type ${this.options.type} passed to codec ${this.options.name}: ${error}`)
    }
    return Buffer.from(this.message.encode(this.message.create(value)).finish())
  }

  decode (buffer: Buffer, length: number): T {
    const converstion = this.options.conversion ?? {
      defaults: true,
      oneofs: true
    }
    return this.message.toObject(this.message.decode(buffer, length), converstion) as T
  }
}
