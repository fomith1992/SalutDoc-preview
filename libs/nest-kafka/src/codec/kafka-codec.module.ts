import { DynamicModule, Module, Provider } from '@nestjs/common'
import { getCodecToken } from '../utils'
import { ProtobufCodecOptions } from './interfaces'
import { ProtobufCodec } from './protobuf.codec'

@Module({})
export class KafkaCodecModule {
  static forProtobuf (options: ProtobufCodecOptions[]): DynamicModule {
    const providers = options.map<Provider>(option => ({
      provide: getCodecToken(option.name),
      useValue: new ProtobufCodec(option)
    }))
    return {
      module: KafkaCodecModule,
      providers,
      exports: providers
    }
  }
}
