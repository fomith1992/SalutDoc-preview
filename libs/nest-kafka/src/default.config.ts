import { GlobalConfig } from 'node-rdkafka'

export const defaultGlobalConfig: GlobalConfig = {
  'client.id': 'nest-kafka'
}
