import { FactoryProvider, ModuleMetadata } from '@nestjs/common'
import {
  ConsumerGlobalConfig,
  ConsumerTopicConfig,
  GlobalConfig,
  ProducerGlobalConfig,
  ProducerTopicConfig,
  SubscribeTopic
} from 'node-rdkafka'
import { PayloadEncoder } from './producer'

export interface KafkaAsyncConfiguration extends Pick<ModuleMetadata, 'imports'> {
  kafkaKey?: string

  useFactory: FactoryProvider<GlobalConfig>['useFactory']

  inject?: FactoryProvider<GlobalConfig>['inject']
}

export interface ConsumerOptions {
  globalConfig?: ConsumerGlobalConfig

  topicConfig?: ConsumerTopicConfig

  subscriptions?: string[]

  allSubscriptions?: boolean
}

export interface ConsumerAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  kafkaKey?: string

  name?: string

  useFactory: FactoryProvider<ConsumerOptions>['useFactory']

  inject?: FactoryProvider<ConsumerOptions>['inject']
}

export interface SubscriptionOptions {
  topic: SubscribeTopic
}

export interface SubscriptionAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  name: string

  useFactory: FactoryProvider<SubscriptionOptions>['useFactory']

  inject?: FactoryProvider<SubscriptionOptions>['inject']
}

export interface ProducerOptions {
  globalConfig?: ProducerGlobalConfig

  topicConfig?: ProducerTopicConfig

  senders?: string[]

  allSenders?: boolean
}

export interface ProducerAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  kafkaKey?: string

  name?: string

  useFactory: FactoryProvider<ProducerOptions>['useFactory']

  inject?: FactoryProvider<ProducerOptions>['inject']
}

export interface SenderOptions {
  payloadEncoder: PayloadEncoder

  defaultTopic?: string
}

export interface SenderAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  name: string

  useFactory: FactoryProvider<SenderOptions>['useFactory']

  inject?: FactoryProvider<SenderOptions>['inject']
}
