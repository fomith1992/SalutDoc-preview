export type BackoffStrategy = (retryNum: number) => number

export function expBackoff (minDelay: number, maxDelay: number): BackoffStrategy {
  const maxPower = Math.ceil(Math.log2(maxDelay / minDelay))
  return retryNum => Math.min(maxDelay, minDelay << Math.min(maxPower, retryNum))
}
