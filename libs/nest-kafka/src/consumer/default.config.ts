import { ConsumerGlobalConfig, ConsumerTopicConfig } from 'node-rdkafka'

export const defaultConsumerGlobalConfig: ConsumerGlobalConfig = {
  'enable.auto.offset.store': false,
  'auto.commit.interval.ms': 1000,
  offset_commit_cb: true
}

export const defaultConsumerTopicConfig: ConsumerTopicConfig = {
}
