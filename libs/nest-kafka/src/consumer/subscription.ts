import { SubscriptionOptions } from '../kafka.types'

export class Subscription {
  constructor (
    readonly name: string,
    readonly config: SubscriptionOptions
  ) {
  }
}
