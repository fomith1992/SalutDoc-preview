import * as ar from 'fp-ts/Array'
import { pipe } from 'fp-ts/function'
import * as rcd from 'fp-ts/Record'

export function foldHeaders<T> (headers: Array<Record<string, T>>): Record<string, T[]> {
  return pipe(
    headers,
    ar.foldMap(rcd.getMonoid(ar.getMonoid<T>()))(rcd.map(ar.of))
  )
}
