import { Injectable } from '@nestjs/common'
import { Logger } from '@salutdoc/nest-logging'
import { KafkaMessage, SubscriptionHandler } from './interfaces'

@Injectable()
export class SubscriptionDispatcher {
  private readonly handlers: SubscriptionHandler[] = []

  constructor (
    private readonly logger: Logger
  ) {
    this.logger.setContext('SubscriptionDispatcher')
  }

  registerHandler (handler: SubscriptionHandler): void {
    this.handlers.push(handler)
  }

  dispatch = async (message: KafkaMessage): Promise<void> => {
    const matchingHandlers = this.handlers
      .filter(handler => handler.matchTopic(message.topic))
    const handler = matchingHandlers[0]
    if (handler == null) {
      this.logger.error(`Handler not found for topic ${message.topic}. Skipping message.`)
      return
    }
    if (matchingHandlers.length > 1) {
      this.logger.error(`Multiple handlers found for topic ${message.topic}. Skipping message.`)
      return
    }
    await handler.handler(message)
  }
}
