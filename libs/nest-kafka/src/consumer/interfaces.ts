export interface SubscriptionHandler {
  matchTopic: (topic: string) => boolean

  handler: (message: KafkaMessage) => Promise<void>
}

export interface KafkaMessage {
  topic: string
  partition: number
  offset: number
  key: Buffer | string | null | undefined
  value: Buffer | null
  size: number
  timestamp?: number
  headers: KafkaMessageHeaders
}

export type KafkaMessageHeaders = Record<string, Array<string | Buffer>>
