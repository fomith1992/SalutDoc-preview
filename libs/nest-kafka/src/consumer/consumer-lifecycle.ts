import { OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common'
import { Logger } from '@salutdoc/nest-logging'
import { ShutdownService } from '@salutdoc/nest-shutdown'
import {
  ConsumerGlobalConfig,
  ConsumerTopicConfig,
  GlobalConfig,
  KafkaConsumer,
  TopicPartitionOffset
} from 'node-rdkafka'
import { promisify } from 'util'
import { ConsumerOptions, SubscriptionOptions } from '../kafka.types'
import { expBackoff } from './backoff-strategy'
import { defaultConsumerGlobalConfig, defaultConsumerTopicConfig } from './default.config'
import { SubscriptionHandler } from './interfaces'
import { ReadableConsumer } from './readable-consumer'
import { SubscriptionDispatcher } from './subscription-dispatcher'
import { foldHeaders } from './utils'

export class ConsumerLifecycle implements OnApplicationBootstrap, OnApplicationShutdown {
  private readonly subscriptions: SubscriptionOptions[] = []
  private readonly offsetStore = new Map<string, number>() // used for testing
  private started = false
  private stopped = false
  private consumingPromise: Promise<void> | null = null

  readonly globalConfig: Readonly<ConsumerGlobalConfig>
  readonly topicConfig: Readonly<ConsumerTopicConfig>
  readonly consumer: KafkaConsumer

  constructor (
    readonly name: string | undefined,
    readonly options: ConsumerOptions,
    kafkaGlobalConfig: GlobalConfig,
    private readonly dispatcher: SubscriptionDispatcher,
    private readonly logger: Logger,
    private readonly shutdownService: ShutdownService
  ) {
    this.globalConfig = {
      ...defaultConsumerGlobalConfig,
      ...kafkaGlobalConfig,
      ...options.globalConfig
    }
    this.topicConfig = {
      ...defaultConsumerTopicConfig,
      ...options.topicConfig
    }
    this.consumer = new KafkaConsumer(this.globalConfig, this.topicConfig)

    this.logger.setContext('ConsumerLifecycle')
    this.logger.setContextMeta({
      clientId: this.globalConfig['client.id'],
      groupId: this.globalConfig['group.id']
    })
  }

  registerSubscription (subscription: SubscriptionOptions): void {
    if (this.started) {
      throw new Error('Dynamic subscriptions not supported')
    }
    this.subscriptions.push(subscription)
  }

  registerHandler (handler: SubscriptionHandler): void {
    this.dispatcher.registerHandler(handler)
  }

  async onApplicationBootstrap (): Promise<void> {
    this.logger.info('Connecting')
    this.started = true

    await promisify(this.consumer.connect).call(this.consumer, {})

    const topics = this.subscriptions.map(subscription => subscription.topic)

    this.logger.info('Subscribing', { topics })
    this.consumer.subscribe(topics)

    this.logger.info('Starting')
    this.consumingPromise = this.consumeStream(new ReadableConsumer(this.consumer)).catch(error => {
      this.logger.error('Consumer loop crashed', error)
      this.shutdownService.requestShutdown(error)
    })
  }

  getConsumedOffset (topic: string, partition: number): number | undefined {
    return this.offsetStore.get(`${topic}:${partition}`)
  }

  private storeOffsets (tpo: TopicPartitionOffset): void {
    this.consumer.offsetsStore([tpo])
    this.offsetStore.set(`${tpo.topic}:${tpo.partition}`, tpo.offset)
  }

  private async consumeStream (stream: ReadableConsumer): Promise<void> {
    const backoffStrategy = expBackoff(10, 5000)
    for await (const message of stream) {
      const logMeta = {
        topic: message.topic,
        offset: message.offset,
        partition: message.partition
      }
      for (let retryNum = 0; ; retryNum++) {
        if (this.stopped) {
          return
        }
        try {
          this.logger.debug('Message process started', logMeta)
          await this.dispatcher.dispatch({
            topic: message.topic,
            partition: message.partition,
            offset: message.offset,
            value: message.value,
            key: message.key,
            size: message.size,
            timestamp: message.timestamp,
            headers: foldHeaders(message.headers ?? [])
          })
          this.logger.debug('Storing message offset to be committed', logMeta)
          this.storeOffsets({
            topic: message.topic,
            offset: message.offset + 1,
            partition: message.partition
          })
          this.logger.debug('Message processed', logMeta)
          break
        } catch (error) {
          const delay = backoffStrategy(retryNum)
          this.logger.error(`Failed to process message. Retrying in ${delay}ms.`, {
            topic: message.topic,
            offset: message.offset,
            partition: message.partition
          }, error)
          await new Promise(resolve => setTimeout(resolve, delay))
        }
      }
    }
  }

  async onApplicationShutdown (): Promise<void> {
    try {
      this.logger.info('Disconnecting')
      this.stopped = true
      await promisify(this.consumer.disconnect).call(this.consumer)
      await this.consumingPromise
      this.logger.info('Disconnected')
    } catch (e) {
      this.logger.error('Failed to disconnect', e)
    }
  }
}
