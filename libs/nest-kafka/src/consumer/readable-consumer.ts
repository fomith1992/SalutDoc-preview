import { KafkaConsumer, LibrdKafkaError, Message } from 'node-rdkafka'
import { Readable } from 'stream'
import { BackoffStrategy, expBackoff } from './backoff-strategy'

export interface ReadableConsumer extends Readable {
  read: (size?: number) => Message | null

  [Symbol.asyncIterator]: () => AsyncIterableIterator<Message>
}

export interface ReadableConsumerOptions {
  /**
   * Time for which consume operation waits for more messages before returning,
   * effectively increasing RTT of each request by same amount of time.
   */
  timeout?: number

  /**
   * Max number of messages to be buffered until consumed
   */
  highWaterMark?: number

  /**
   * Backoff strategy to be used after consume operation returns 0 messages
   */
  backoffStrategy?: BackoffStrategy
}

export class ReadableConsumer extends Readable {
  private readonly backoffStrategy: BackoffStrategy

  constructor (
    private readonly consumer: KafkaConsumer,
    options?: ReadableConsumerOptions
  ) {
    super({
      objectMode: true,
      highWaterMark: options?.highWaterMark
    })

    this.backoffStrategy = options?.backoffStrategy ?? expBackoff(8, 128)
    this.consumer.setDefaultConsumeTimeout(options?.timeout ?? 16)

    this.consumer.once('disconnected', () => {
      this.destroy()
    })
  }

  private fetch (fetchSize: number): void {
    let retryNum = 0
    type Maybe<T> = T | null | undefined
    const onBatch = (err: Maybe<LibrdKafkaError>, messages: Maybe<Message[]>): void => {
      if (err != null) {
        this.emit('error', err)
      }

      if (messages == null || messages.length === 0) {
        const delay = this.backoffStrategy(retryNum++)
        setTimeout(() => {
          if (!this.destroyed && this.consumer.isConnected()) {
            this.consumer.consume(fetchSize, onBatch)
          }
        }, delay).unref()
      } else {
        for (const message of messages) {
          this.push(message)
        }
      }
    }

    if (this.destroyed || !this.consumer.isConnected()) {
      return
    }

    this.consumer.consume(fetchSize, onBatch)
  }

  _read (): void {
    const fetchSize = 1

    if (!this.consumer.isConnected()) {
      this.consumer.once('ready', () => {
        this.fetch(fetchSize)
      })
      return
    }

    this.fetch(fetchSize)
  }
}
