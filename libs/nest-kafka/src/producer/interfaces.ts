export interface SentMessage<T> {
  topic?: string
  partition?: number | null
  key?: string | Buffer | null
  timestamp?: number | null
  headers?: Record<string, string | Buffer | Array<string | Buffer>>
  value: T
}

export interface SendResult {
  topic: string
  partition: number
  offset: number
}

export interface PayloadEncoder<T = any> {
  encodePayload: (payload: T) => Buffer | Promise<Buffer>
}
