import { OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common'
import { Logger } from '@salutdoc/nest-logging'
import {
  GlobalConfig,
  LibrdKafkaError,
  Metadata,
  MetadataOptions,
  Producer,
  ProducerGlobalConfig,
  ProducerTopicConfig,
  WatermarkOffsets
} from 'node-rdkafka'
import { promisify } from 'util'
import { ProducerOptions } from '../kafka.types'
import { defaultProducerGlobalConfig, defaultProducerTopicConfig } from './default.config'
import { SendResult, SentMessage } from './interfaces'
import { unfoldHeaders } from './utils'

export class KafkaProducer implements OnApplicationBootstrap, OnApplicationShutdown {
  private readonly producerSymbol = Symbol('KafkaProducer')
  private inflightRequests: number = 0
  private pollerIntervalId: ReturnType<typeof setInterval> | null = null

  readonly globalConfig: Readonly<ProducerGlobalConfig>
  readonly topicConfig: Readonly<ProducerTopicConfig>
  readonly producer: Producer

  constructor (
    kafkaConfig: GlobalConfig,
    readonly options: ProducerOptions,
    private readonly logger: Logger
  ) {
    this.globalConfig = {
      ...defaultProducerGlobalConfig,
      ...kafkaConfig,
      ...options.globalConfig
    }
    this.topicConfig = {
      ...defaultProducerTopicConfig,
      ...options.topicConfig
    }
    this.producer = new Producer(this.globalConfig, this.topicConfig)
    this.logger.setContext('KafkaProducer')
    this.logger.setContextMeta({
      clientId: this.globalConfig['client.id']
    })
  }

  async getMetadata (metadataOptions?: MetadataOptions): Promise<Metadata> {
    return await promisify(this.producer.getMetadata).call(this.producer, metadataOptions)
  }

  async queryWatermarkOffsets (topic: string, partition: number, timeout?: number): Promise<WatermarkOffsets> {
    return await new Promise((resolve, reject) => {
      const callback = (error: LibrdKafkaError, offsets: WatermarkOffsets): void => {
        if (error != null) {
          reject(error)
        } else {
          resolve(offsets)
        }
      }
      try {
        if (timeout == null) {
          this.producer.queryWatermarkOffsets(topic, partition, callback)
        } else {
          this.producer.queryWatermarkOffsets(topic, partition, timeout, callback)
        }
      } catch (e) {
        reject(e)
      }
    })
  }

  async send (message: SentMessage<Buffer | null> & { topic: string }): Promise<SendResult> {
    const headers = unfoldHeaders(message.headers ?? {})

    return await new Promise((resolve, reject) => {
      try {
        this.producer.produce(
          message.topic,
          message.partition,
          message.value,
          message.key,
          message.timestamp,
          {
            producer: this.producerSymbol,
            callback: (err: any, report: any): void => {
              this.stopPolling()
              if (err != null) {
                reject(err)
              } else {
                resolve(report)
              }
            }
          },
          headers
        )
        this.startPolling()
      } catch (e) {
        return reject(e)
      }
    })
  }

  private startPolling (): void {
    if (this.inflightRequests++ === 0 && this.pollerIntervalId == null) {
      this.pollerIntervalId = setInterval(() => {
        this.producer.poll()
      }, 16)
    }
  }

  private stopPolling (): void {
    if (--this.inflightRequests === 0 && this.pollerIntervalId != null) {
      clearInterval(this.pollerIntervalId)
      this.pollerIntervalId = null
    }
  }

  async onApplicationBootstrap (): Promise<void> {
    this.producer.on('delivery-report', (err, report) => {
      if (report.opaque?.producer === this.producerSymbol) {
        const result: SendResult | null = report == null ? null : {
          topic: report.topic,
          offset: report.offset,
          partition: report.partition
        }
        report.opaque.callback(err, result)
      }
    })
    this.producer.setPollInterval(1000)
    await promisify(this.producer.connect).call(this.producer, {})
  }

  async onApplicationShutdown (): Promise<void> {
    try {
      this.logger.info('Disconnecting')
      await promisify(this.producer.disconnect).call(this.producer)
      this.logger.info('Disconnected')
    } catch (e) {
      this.logger.error('Failed to disconnect', e)
    }
  }
}
