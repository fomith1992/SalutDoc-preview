import { ProducerGlobalConfig, ProducerTopicConfig } from 'node-rdkafka'

export const defaultProducerGlobalConfig: ProducerGlobalConfig = {
  dr_cb: true
}

export const defaultProducerTopicConfig: ProducerTopicConfig = {
  partitioner: 'murmur2_random'
}
