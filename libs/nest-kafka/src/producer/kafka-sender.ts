import { PayloadEncoder, SendResult, SentMessage } from './interfaces'
import { KafkaProducer } from './kafka-producer'

export class KafkaSender<T> {
  producer!: KafkaProducer

  constructor (
    readonly name: string,
    private readonly encoder: PayloadEncoder<T>,
    readonly defaultTopic?: string
  ) {
  }

  bindProducer (producer: KafkaProducer): void {
    if (this.producer != null) {
      throw new Error('Producer already bound')
    }
    this.producer = producer
  }

  async send (message: SentMessage<T>): Promise<SendResult> {
    const producer = this.producer
    if (producer == null) {
      throw new Error('Producer not bound')
    }
    const topic = message.topic ?? this.defaultTopic
    if (topic == null) {
      throw new Error('Topic not provided, nor default configured')
    }
    const value = await this.encoder.encodePayload(message.value)

    return await this.producer.send({ ...message, topic, value })
  }
}
