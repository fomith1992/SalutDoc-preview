export function unfoldHeaders<T> (headers: Record<string, T | T[]>): Array<Record<string, T>> {
  return Object.entries(headers)
    .flatMap(([key, values]) => Array.isArray(values)
      ? values.map(value => ({ [key]: value }))
      : [{ [key]: values }])
}
