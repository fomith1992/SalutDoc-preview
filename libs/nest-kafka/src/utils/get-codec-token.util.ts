export function getCodecToken (name: string): string {
  return `KAFKA_CODEC(${name})`
}
