export const KAFKA_CONSUMER_LIFECYCLE_DEFAULT_TOKEN = 'KAFKA_CONSUMER_LIFECYCLE(default)'

export function getConsumerLifecycleToken (name?: string): string {
  return name != null
    ? `KAFKA_CONSUMER_LIFECYCLE(${name})`
    : KAFKA_CONSUMER_LIFECYCLE_DEFAULT_TOKEN
}
