export const KAFKA_PRODUCER_CONFIG_DEFAULT_TOKEN = 'KAFKA_PRODUCER_CONFIG(default)'

export function getProducerConfigToken (name?: string): string {
  return name != null ? `KAFKA_PRODUCER_CONFIG(${name})` : KAFKA_PRODUCER_CONFIG_DEFAULT_TOKEN
}
