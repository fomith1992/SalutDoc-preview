export const KAFKA_SUBSCRIPTION_DISPATCHER_DEFAULT_TOKEN = 'KAFKA_SUBSCRIPTION_DISPATCHER(default)'

export function getSubscriptionDispatcherToken (consumerName?: string): string {
  return consumerName != null
    ? `KAFKA_SUBSCRIPTION_DISPATCHER(${consumerName})`
    : KAFKA_SUBSCRIPTION_DISPATCHER_DEFAULT_TOKEN
}
