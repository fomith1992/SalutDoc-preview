export const KAFKA_CONSUMER_CONFIG_DEFAULT_TOKEN = 'KAFKA_CONSUMER_CONFIG(default)'

export function getConsumerConfigToken (name?: string): string {
  return name != null ? `KAFKA_CONSUMER_CONFIG(${name})` : KAFKA_CONSUMER_CONFIG_DEFAULT_TOKEN
}
