export const KAFKA_PRODUCER_DEFAULT_TOKEN = 'KAFKA_PRODUCER(default)'

export function getProducerToken (name?: string): string {
  return name != null ? `KAFKA_PRODUCER(${name})` : KAFKA_PRODUCER_DEFAULT_TOKEN
}
