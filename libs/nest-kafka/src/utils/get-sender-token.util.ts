export function getSenderToken (name: string): string {
  return `KAFKA_SENDER(${name})`
}
