export const KAFKA_CONFIG_DEFAULT_TOKEN = 'KAFKA_CONFIG(default)'

export function getKafkaConfigToken (kafkaKey?: string): string {
  return kafkaKey != null ? `KAFKA_CONFIG(${kafkaKey})` : KAFKA_CONFIG_DEFAULT_TOKEN
}
