export const KAFKA_FACTORY_DEFAULT_TOKEN = 'KAFKA_FACTORY(default)'

export function getKafkaFactoryToken (kafkaKey?: string): string {
  return kafkaKey != null ? `KAFKA_FACTORY(${kafkaKey})` : KAFKA_FACTORY_DEFAULT_TOKEN
}
