export function getSubscriptionConfigToken (subscriptionName: string): string {
  return `KAFKA_SUBSCRIPTION_CONFIG(${subscriptionName})`
}
