export function matchTopicName (topic: string | RegExp, topicName: string): boolean {
  return typeof topic === 'string'
    ? topic === topicName
    : topic.test(topicName)
}
