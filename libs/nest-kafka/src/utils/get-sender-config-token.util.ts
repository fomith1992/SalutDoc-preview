export function getSenderConfigToken (name: string): string {
  return `KAFKA_SENDER_CONFIG(${name})`
}
