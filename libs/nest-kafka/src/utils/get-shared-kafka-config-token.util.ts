export const SHARED_KAFKA_CONFIG_DEFAULT_TOKEN = 'SHARED_KAFKA_CONFIG(default)'

export function getSharedKafkaConfigToken (kafkaKey?: string): string {
  return kafkaKey != null ? `SHARED_KAFKA_CONFIG(${kafkaKey})` : SHARED_KAFKA_CONFIG_DEFAULT_TOKEN
}
