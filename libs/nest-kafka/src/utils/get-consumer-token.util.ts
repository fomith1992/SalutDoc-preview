export const KAFKA_CONSUMER_DEFAULT_TOKEN = 'KAFKA_CONSUMER(default)'

export function getConsumerToken (name?: string): string {
  return name != null ? `KAFKA_CONSUMER(${name})` : KAFKA_CONSUMER_DEFAULT_TOKEN
}
