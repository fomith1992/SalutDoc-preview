export function getSubscriptionToken (subscriptionName: string): string {
  return `KAFKA_SUBSCRIPTION(${subscriptionName})`
}
