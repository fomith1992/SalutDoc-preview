import { Injectable, OnModuleInit } from '@nestjs/common'
import { DiscoveryService, MetadataScanner, ModuleRef } from '@nestjs/core'
import { InstanceWrapper } from '@nestjs/core/injector/instance-wrapper'
import { Logger } from '@salutdoc/nest-logging'
import { ConsumerLifecycle, Subscription } from './consumer'
import { KafkaMetadataAccessor } from './kafka-metadata.accessor'
import { KafkaProducer, KafkaSender } from './producer'
import { getSubscriptionToken, matchTopicName } from './utils'

@Injectable()
export class KafkaExplorer implements OnModuleInit {
  private readonly subscriptionNameToConsumer: Record<string, ConsumerLifecycle> = {}

  constructor (
    private readonly moduleRef: ModuleRef,
    private readonly discoveryService: DiscoveryService,
    private readonly metadataAccessor: KafkaMetadataAccessor,
    private readonly metadataScanner: MetadataScanner,
    private readonly logger: Logger
  ) {
    this.logger.setContext('KafkaExplorer')
  }

  onModuleInit (): void {
    this.explore()
  }

  explore (): void {
    this.logger.info('Exploring started')
    const consumers: Array<InstanceWrapper<ConsumerLifecycle>> = this.discoveryService
      .getProviders()
      .filter((wrapper: InstanceWrapper) =>
        wrapper.instance instanceof ConsumerLifecycle
      )

    this.logger.info(`Found ${consumers.length} consumers`)

    const subscriptions: Array<InstanceWrapper<Subscription>> = this.discoveryService
      .getProviders()
      .filter((wrapper: InstanceWrapper) =>
        wrapper.instance instanceof Subscription
      )

    this.logger.info(`Found ${subscriptions.length} subscriptions`)

    subscriptions.map<[ConsumerLifecycle, Subscription]>(({ instance: subscription }) => {
      const matchingConsumers = consumers.filter(({ instance: consumer }) =>
        consumer.options.subscriptions?.some(subscriptionName => subscriptionName === subscription.name) ?? false
      )
      if (matchingConsumers[0] != null && matchingConsumers.length === 1) {
        return [matchingConsumers[0].instance, subscription]
      }
      if (matchingConsumers.length > 1) {
        throw new Error(`Multiple matching consumers found for subscription '${subscription.name}'`)
      }
      const lastResortConsumers = consumers.filter(({ instance: consumer }) =>
        consumer.options.allSubscriptions === true
      )
      if (lastResortConsumers[0] != null && lastResortConsumers.length === 1) {
        return [lastResortConsumers[0].instance, subscription]
      }
      if (lastResortConsumers.length > 1) {
        throw new Error('Multiple consumers with allSubscription=true found')
      }
      throw new Error(`Consumer not found for subscription '${subscription.name}'`)
    }).forEach(([consumer, subscription]) => {
      this.logger.info(`Binding subscription '${subscription.name}'` +
        ` to consumer ${consumer.name == null ? 'default' : `'${consumer.name}'`}`)
      consumer.registerSubscription(subscription.config)
      this.subscriptionNameToConsumer[subscription.name] = consumer
    })

    const processors: InstanceWrapper[] = this.discoveryService
      .getProviders()
      .filter((wrapper: InstanceWrapper) =>
        this.metadataAccessor.isProcessorComponent(wrapper.metatype)
      )

    processors.forEach((wrapper: InstanceWrapper) => {
      const { instance } = wrapper
      if (!wrapper.isDependencyTreeStatic()) {
        throw new Error('Request-scoped processors not supported')
      }

      this.metadataScanner.scanFromPrototype(
        instance,
        Object.getPrototypeOf(instance),
        (key: string) => {
          if (this.metadataAccessor.isEachMessageMethod(instance[key])) {
            this.handleEachMessage(instance, key)
          }
        }
      )
    })

    const producers: Array<InstanceWrapper<KafkaProducer>> = this.discoveryService
      .getProviders()
      .filter((wrapper: InstanceWrapper) =>
        wrapper.instance instanceof KafkaProducer
      )

    const senders: Array<InstanceWrapper<KafkaSender<any>>> = this.discoveryService
      .getProviders()
      .filter((wrapper: InstanceWrapper) =>
        wrapper.instance instanceof KafkaSender
      )

    senders.forEach(({ instance: sender }) => {
      const matchingProducers = producers.filter(({ instance: producer }) =>
        producer.options.senders?.some(senderName => senderName === sender.name) ?? false
      )
      if (matchingProducers[0] != null && matchingProducers.length === 1) {
        sender.bindProducer(matchingProducers[0].instance)
        return
      }
      if (matchingProducers.length > 1) {
        throw new Error(`Multiple matching producers found for sender '${sender.name}'`)
      }
      const lastResortProducers = producers.filter(({ instance: producer }) =>
        producer.options.allSenders === true
      )
      if (lastResortProducers[0] != null && lastResortProducers.length === 1) {
        sender.bindProducer(lastResortProducers[0].instance)
        return
      }
      if (lastResortProducers.length > 1) {
        throw new Error('Multiple producers with allSenders=true found')
      }
      throw new Error(`Producer not found for sender '${sender.name}'`)
    })
  }

  getSubscription (subscriptionName: string): Subscription {
    try {
      return this.moduleRef.get<Subscription>(getSubscriptionToken(subscriptionName), {
        strict: false
      })
    } catch (err) {
      this.logger.error(`Subscription not found for name ${subscriptionName}`)
      throw err
    }
  }

  private handleEachMessage (instance: any, key: string): void {
    const {
      subscription,
      topic
    } = this.metadataAccessor.getEachMessageMetadata(instance[key])
    const subscriptionLifecycle = this.getSubscription(subscription)
    const consumerLifecycle = this.subscriptionNameToConsumer[subscription]
    if (consumerLifecycle == null) {
      throw new Error(`Consumer not found for subscription ${subscription}`)
    }
    consumerLifecycle.registerHandler({
      matchTopic: topicName => (topic == null || matchTopicName(topic, topicName)) &&
        matchTopicName(subscriptionLifecycle.config.topic, topicName),
      handler: instance[key].bind(instance) // todo apply contexts, pipes, etc
    })
  }
}
