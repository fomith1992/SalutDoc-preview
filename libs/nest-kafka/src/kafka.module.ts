import { DynamicModule, Module } from '@nestjs/common'
import { DiscoveryModule } from '@nestjs/core'
import { Logger } from '@salutdoc/nest-logging'
import { ShutdownService } from '@salutdoc/nest-shutdown'
import { GlobalConfig } from 'node-rdkafka'
import { ConsumerLifecycle, Subscription, SubscriptionDispatcher } from './consumer'
import { defaultGlobalConfig } from './default.config'
import { KafkaMetadataAccessor } from './kafka-metadata.accessor'
import { KafkaExplorer } from './kafka.explorer'
import { KafkaFactory } from './kafka.factory'
import {
  ConsumerAsyncOptions,
  ConsumerOptions,
  KafkaAsyncConfiguration,
  ProducerAsyncOptions,
  ProducerOptions,
  SenderAsyncOptions,
  SenderOptions,
  SubscriptionAsyncOptions,
  SubscriptionOptions
} from './kafka.types'
import { KafkaProducer, KafkaSender } from './producer'
import {
  getConsumerConfigToken,
  getConsumerLifecycleToken,
  getConsumerToken,
  getKafkaConfigToken,
  getKafkaFactoryToken,
  getProducerConfigToken,
  getProducerToken,
  getSenderConfigToken,
  getSenderToken,
  getSharedKafkaConfigToken,
  getSubscriptionConfigToken,
  getSubscriptionDispatcherToken,
  getSubscriptionToken
} from './utils'

@Module({})
export class KafkaModule {
  static registerKafkaAsync (asyncConfig: KafkaAsyncConfiguration): DynamicModule {
    const kafkaKey = asyncConfig.kafkaKey
    const imports = this.getUniqImports([asyncConfig])
    const providers = [
      {
        provide: getKafkaConfigToken(kafkaKey),
        useFactory: asyncConfig.useFactory,
        inject: asyncConfig.inject ?? []
      },
      {
        provide: getSharedKafkaConfigToken(kafkaKey),
        inject: [getKafkaConfigToken(kafkaKey)],
        useFactory: (config: GlobalConfig) => ({
          ...defaultGlobalConfig,
          ...config
        })
      },
      {
        provide: getKafkaFactoryToken(kafkaKey),
        inject: [getSharedKafkaConfigToken(kafkaKey)],
        useFactory: (config: GlobalConfig) => new KafkaFactory(config)
      }
    ]
    return {
      module: KafkaModule,
      global: true,
      imports,
      providers,
      exports: [
        getSharedKafkaConfigToken(kafkaKey),
        getKafkaFactoryToken(kafkaKey)
      ]
    }
  }

  static registerConsumersAsync (...options: ConsumerAsyncOptions[]): DynamicModule {
    const imports = this.getUniqImports(options)
    const providers = options.flatMap(option => [
      {
        provide: getConsumerConfigToken(option.name),
        useFactory: option.useFactory,
        inject: option.inject ?? []
      },
      {
        provide: getSubscriptionDispatcherToken(option.name),
        useClass: SubscriptionDispatcher
      },
      {
        provide: getConsumerLifecycleToken(option.name),
        inject: [
          getSharedKafkaConfigToken(option.kafkaKey),
          getConsumerConfigToken(option.name),
          getSubscriptionDispatcherToken(option.name),
          Logger,
          ShutdownService
        ],
        useFactory: (
          kafkaConfig: GlobalConfig,
          options: ConsumerOptions,
          dispatcher: SubscriptionDispatcher,
          logger: Logger,
          shutdownService: ShutdownService
        ) => {
          return new ConsumerLifecycle(
            option.name,
            options,
            kafkaConfig,
            dispatcher,
            logger,
            shutdownService
          )
        }
      },
      {
        provide: getConsumerToken(option.name),
        inject: [getConsumerLifecycleToken(option.name)],
        useFactory: (consumerLifecycle: ConsumerLifecycle) => {
          return consumerLifecycle.consumer
        }
      }
    ])
    const exports = options.flatMap(option => [
      getConsumerToken(option.name)
    ])

    return {
      module: KafkaModule,
      imports: [...imports, this.registerCore()],
      providers,
      exports
    }
  }

  static registerSubscriptionsAsync (...options: SubscriptionAsyncOptions[]): DynamicModule {
    const imports = this.getUniqImports(options)
    const providers = options.flatMap(option => [
      {
        provide: getSubscriptionConfigToken(option.name),
        useFactory: option.useFactory,
        inject: option.inject ?? []
      },
      {
        provide: getSubscriptionToken(option.name),
        inject: [getSubscriptionConfigToken(option.name)],
        useFactory: (config: SubscriptionOptions) => {
          return new Subscription(option.name, config)
        }
      }
    ])
    const exports = options.flatMap(option => [
      getSubscriptionToken(option.name)
    ])
    return {
      module: KafkaModule,
      imports: imports,
      providers,
      exports
    }
  }

  static registerProducersAsync (...options: ProducerAsyncOptions[]): DynamicModule {
    const imports = this.getUniqImports(options)
    const providers = options.flatMap(option => [
      {
        provide: getProducerConfigToken(option.name),
        useFactory: option.useFactory,
        inject: option.inject ?? []
      },
      {
        provide: getProducerToken(option.name),
        inject: [
          getSharedKafkaConfigToken(option.kafkaKey),
          getProducerConfigToken(option.name),
          Logger
        ],
        useFactory: (kafkaConfig: GlobalConfig, options: ProducerOptions, logger: Logger) => {
          return new KafkaProducer(kafkaConfig, options, logger)
        }
      }
    ])
    const exports = options.flatMap(option => [
      getProducerToken(option.name)
    ])

    return {
      module: KafkaModule,
      imports: [...imports, this.registerCore()],
      providers,
      exports
    }
  }

  static registerSendersAsync (...options: SenderAsyncOptions[]): DynamicModule {
    const imports = this.getUniqImports(options)
    const providers = options.flatMap(option => [
      {
        provide: getSenderConfigToken(option.name),
        useFactory: option.useFactory,
        inject: option.inject ?? []
      },
      {
        provide: getSenderToken(option.name),
        inject: [getSenderConfigToken(option.name)],
        useFactory: (options: SenderOptions) => {
          return new KafkaSender(option.name, options.payloadEncoder, options.defaultTopic)
        }
      }
    ])
    const exports = options.flatMap(option => [
      getSenderToken(option.name)
    ])

    return {
      module: KafkaModule,
      imports,
      providers,
      exports
    }
  }

  private static registerCore (): DynamicModule {
    return {
      global: true,
      module: KafkaModule,
      imports: [DiscoveryModule],
      providers: [KafkaExplorer, KafkaMetadataAccessor]
    }
  }

  private static getUniqImports (
    options: Array<{ imports?: any[] }>
  ): any {
    return options.map(o => o.imports ?? []).flat().filter((v, i, a) => a.indexOf(v) === i)
  }
}
