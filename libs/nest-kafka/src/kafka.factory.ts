import { ConsumerGlobalConfig, ConsumerTopicConfig, GlobalConfig, KafkaConsumer } from 'node-rdkafka'

export class KafkaFactory {
  constructor (readonly globalConfig: Readonly<GlobalConfig>) {
  }

  consumer (globalConfig: ConsumerGlobalConfig, topicConfig: ConsumerTopicConfig): KafkaConsumer {
    return new KafkaConsumer({ ...this.globalConfig, ...globalConfig }, topicConfig)
  }
}
