import { Injectable, Type } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { EachMessageOptions } from './decorators'
import { KAFKA_MODULE_CONSUMER_EACH_MESSAGE, KAFKA_MODULE_PROCESSOR } from './kafka.constants'

@Injectable()
export class KafkaMetadataAccessor {
  constructor (private readonly reflector: Reflector) {
  }

  isProcessorComponent (target: Type<any> | Function): boolean {
    if (target == null) {
      return false
    }
    return this.reflector.get(KAFKA_MODULE_PROCESSOR, target) === true
  }

  isEachMessageMethod (target: Type<any> | Function): boolean {
    if (target == null) {
      return false
    }
    return this.getEachMessageMetadata(target) != null
  }

  getEachMessageMetadata (target: Type<any> | Function): EachMessageOptions {
    return this.reflector.get(KAFKA_MODULE_CONSUMER_EACH_MESSAGE, target)
  }
}
