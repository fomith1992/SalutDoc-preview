import { ObjectId } from 'mongodb'

export function parseObjectIdOrNull (id: string | null): ObjectId | null {
  try {
    return id == null ? null : ObjectId.createFromHexString(id)
  } catch (error) {
    return null
  }
}
