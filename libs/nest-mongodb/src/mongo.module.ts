import { DynamicModule, Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { MongoModule as Mongo } from 'nest-mongodb'
import { mongoConfig } from './mongo.config'

@Module({})
export class MongoModule {
  static forRoot (dbName: string): DynamicModule {
    return {
      module: MongoModule,
      imports: [
        Mongo.forRootAsync({
          imports: [
            ConfigModule.forFeature(mongoConfig)
          ],
          inject: [mongoConfig.KEY],
          useFactory: (config: ConfigType<typeof mongoConfig>) => {
            const uri = config.MONGO_CONNECTION_URL
            return {
              uri,
              dbName,
              clientOptions: {
                useUnifiedTopology: true,
                useNewUrlParser: true,
                ignoreUndefined: true,
                tlsCAFile: config.MONGO_TLS_CA_FILE
              }
            }
          }
        })
      ]
    }
  }

  static forFeature (collections?: string[]): DynamicModule {
    return {
      module: MongoModule,
      imports: [
        Mongo.forFeature(collections)
      ],
      exports: [
        Mongo
      ]
    }
  }
}
