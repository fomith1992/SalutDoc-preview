import { pipe } from 'fp-ts/function'
import { MongoError } from 'mongodb'
import * as maybe from '@salutdoc/utils/lib/fp/maybe'

export interface MongoErrorHandler<T> {
  onDuplicate?: () => T
}

const codeMapping: Record<number, keyof MongoErrorHandler<any>> = {
  11000: 'onDuplicate'
}

export function handleMongoError<T> (error: any, handler: MongoErrorHandler<T>): T {
  return pipe(
    error,
    maybe.filter((e): e is MongoError => e instanceof MongoError),
    maybe.map(e => e.code),
    maybe.map(code => codeMapping[code]),
    maybe.map(method => handler[method]),
    maybe.fold(
      () => { throw error },
      fn => fn()
    )
  )
}
