import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'
import * as rt from 'runtypes'

export const PATH = Symbol('path')
export const SCHEMA = Symbol('schema')
const BRAND = Symbol('brand')

export type ObjectPath<TRoot, T, B extends 'path' | 'pattern' = 'path'> = {
  [PATH]: PropertyKey[]
  [SCHEMA]: rt.Runtype<T>
  [BRAND]: B
} & {
  [P in keyof T]: ObjectPath<TRoot, T[P], B>
} & (T extends ReadonlyArray<infer E> ? {
  [P in keyof E]: ObjectPath<TRoot, E[P], 'pattern'>
} : {}) & (T extends Record<string, infer V> ? {
  [P in keyof V]: ObjectPath<TRoot, V[P], 'pattern'>
} : {})

export type ObjectPathPattern<TRoot, T> = ObjectPath<TRoot, T, any>

export function createPathBuilder<T> (schema: rt.Runtype<T>): ObjectPath<T, T> {
  return createPathBuilderFrom(schema, [], 'path')
}

function childSchema (reflect: rt.Reflect, key: PropertyKey): rt.Runtype | null {
  switch (reflect.tag) {
    case 'partial':
    case 'record': {
      const field = reflect.fields[key as string]
      if (field == null) {
        break
      }
      return field
    }
    case 'dictionary':
      return reflect.value
    case 'union': {
      const cases = reflect.alternatives
        .map(alt => childSchema(alt, key))
        .filter(it => it != null)
        .filter((it, idx, ar) => ar.indexOf(it) === idx)
      switch (cases.length) {
        case 0:
          return null
        case 1:
          return expectNonNil(cases[0])
        default:
          return rt.Union.apply(null, cases as any)
      }
    }
    case 'array':
      return reflect.element
    case 'tuple': {
      const component = reflect.components[key as number]
      if (component == null) {
        break
      }
      return component
    }
    case 'intersect': {
      const cases = reflect.intersectees
        .map(int => childSchema(int, key))
        .filter(it => it != null)
      switch (cases.length) {
        case 0:
          return null
        case 1:
          return expectNonNil(cases[0])
        default:
          return rt.Intersect.apply(null, cases as any)
      }
    }
    case 'constraint':
      return childSchema(reflect.underlying, key)
  }
  return null
}

function createPathBuilderFrom<TRoot, T, B extends 'path' | 'pattern'> (
  schema: rt.Runtype<T>,
  path: PropertyKey[],
  brand: B
): ObjectPath<TRoot, T, B> {
  const proxy = new Proxy({
    [PATH]: path,
    [SCHEMA]: schema,
    [BRAND]: brand
  }, {
    get (target, key) {
      if (key === PATH) {
        return target[PATH]
      }
      if (key === SCHEMA) {
        return target[SCHEMA]
      }
      if (key === BRAND) {
        return target[BRAND]
      }
      if (typeof key === 'string') {
        const intKey = parseInt(key, 10)
        if (key === intKey.toString()) {
          key = intKey
        }
      }
      let cs = childSchema(schema.reflect, key)
      if (cs == null) {
        switch (schema.reflect.tag) {
          case 'array':
            cs = childSchema(schema.reflect.element, key)
            break
          case 'dictionary':
            cs = childSchema(schema.reflect.value, key)
            break
        }
        if (cs == null) {
          throw new Error(`runtype ${schema.reflect.tag} has no ${String(key)} member`)
        }
        return createPathBuilderFrom(cs, [...path, key], 'pattern')
      } else {
        return createPathBuilderFrom(cs, [...path, key], brand === 'path' ? 'path' : 'pattern')
      }
    }
  })
  return proxy as ObjectPath<TRoot, T, B>
}

export function mongoPath (proxy: ObjectPathPattern<unknown, unknown>): string {
  const path = proxy[PATH]
  if (path.length === 0) {
    throw new Error('Cannot convert empty path to MongoDB path')
  }
  return path.join('.')
}

export function get<TRoot, T> (object: TRoot, proxy: ObjectPath<TRoot, T>): T {
  return proxy[PATH].reduce<any>((o, key) => o?.[key], object) as T
}

export function fromEntries<T> (
  ...entries: Array<[ObjectPathPattern<unknown, unknown>, T | undefined]>
): Record<string, T> {
  return Object.fromEntries(
    entries
      .filter((entry): entry is [ObjectPathPattern<unknown, unknown>, T] => entry[1] !== undefined)
      .map<[string, T]>(([key, value]) => [mongoPath(key), value])
  )
}
