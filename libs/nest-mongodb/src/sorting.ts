import { FilterQuery, Long, ObjectId } from 'mongodb'
import * as rt from 'runtypes'
import { get, mongoPath, ObjectPath, PATH, SCHEMA } from './path'
import * as s from './schema'

export type Sortable = string | number | Date | ObjectId | Long | boolean

export type SortedField<T, F extends Sortable> = {
  path: ObjectPath<T, F>
  dir: 1 | -1
  leeway: number
}

type SortedFields<T> = Array<SortedField<T, Sortable>>

type CursorOfFields<S extends SortedFields<unknown>> = {
  [P in keyof S]: S[P] extends SortedField<unknown, infer F> ? F : never
}

type Encoded<T> = {
  [P in keyof T]: T[P] extends string ? string
    : T[P] extends number ? number
      : T[P] extends Date ? number
        : T[P] extends ObjectId ? string
          : T[P] extends Long | number ? string
            : T[P] extends boolean ? boolean
              : unknown
}

export class Sorting<T, S extends SortedFields<T> = SortedFields<T>> {
  readonly bson: Readonly<Record<string, 1 | -1>>

  private constructor (readonly fields: S) {
    this.bson = Object.fromEntries(fields.map(({ path, dir }) => [mongoPath(path), dir]))
  }

  // this is needed to be able to specify T and infer S
  static builder<T> (): <S extends SortedFields<T>> (...fields: S) => Sorting<T, S> {
    return (...fields) => new Sorting(fields)
  }

  decodeCursor (cursor: unknown | null): CursorOf<this> | null {
    if (cursor == null) {
      return null
    }
    if (!Array.isArray(cursor)) {
      throw new Error('cursor must be an array')
    }

    return this.fields.map((f, i) => {
      const value = cursor[i]
      if (value == null) {
        return null
      }
      const schema = f.path[SCHEMA]
      switch (schema) {
        case s.ObjectId:
          return ObjectId.createFromHexString(rt.String.check(value))
        case s.Date:
          return new Date(rt.Number.check(value))
        case s.Int64:
          return Long.fromString(rt.String.check(value))
        case s.Boolean:
        case s.Int32:
        case s.Double:
        case s.String:
          return schema.check(value)
        default:
          throw new Error(`unexpected runtype: ${schema.reflect.tag}`)
      }
    }) as any
  }

  encodeCursor (cursor: CursorOf<this> | null): Encoded<CursorOf<this>> | null {
    if (cursor == null) {
      return null
    }
    return cursor.map((value, i) => {
      if (value == null) {
        return null
      }
      const field = this.fields[i]
      if (field == null) {
        throw new Error(`Field not found for index ${i}`)
      }
      const schema = field.path[SCHEMA]
      try {
        switch (schema) {
          case s.ObjectId:
            return s.ObjectId.check(value).toHexString()
          case s.Date:
            return s.Date.check(value).getTime()
          case s.Int64:
            return s.Int64.check(value).toString()
          case s.Boolean:
          case s.Int32:
          case s.Double:
          case s.String:
            return schema.check(value)
          default:
            throw new Error(`unexpected runtype: ${schema.reflect.tag}`)
        }
      } catch (e) {
        if (e instanceof Error) {
          throw new Error(`${field.path[PATH].join('.')}: ${e.message}`)
        } else {
          throw e
        }
      }
    }) as any
  }

  buildCursor (entity: T | null | undefined): CursorOf<this> | null {
    if (entity == null) return null
    return this.fields.map(({ path }) => get(entity, path)) as any
  }

  buildFilter (cursor: CursorOf<this> | null): FilterQuery<T> {
    if (cursor == null) {
      return {}
    } else {
      const equalsCondition: FilterQuery<T> = {}
      const branches = []
      for (let i = 0; i < this.fields.length; i++) {
        const field = this.fields[i]
        if (field == null) {
          throw new Error('Field is null')
        }
        const { path, dir, leeway } = field
        const key = mongoPath(path)
        const value = cursor[i]
        let condition: FilterQuery<T> | null
        switch (dir) {
          case 1:
            if (leeway === 0 || typeof value !== 'number') {
              condition = {
                [key]: { $gt: value }
              }
            } else {
              condition = {
                [key]: { $gt: value + leeway }
              }
            }
            break
          case -1:
            if (value == null) {
              condition = null
            } else if (leeway === 0 || typeof value !== 'number') {
              condition = {
                $or: [
                  { [key]: { $lt: value } },
                  { [key]: null }
                ]
              }
            } else {
              condition = {
                $or: [
                  { [key]: { $lt: value - leeway } },
                  { [key]: null }
                ]
              }
            }
        }
        if (condition != null) {
          branches.push({
            ...equalsCondition,
            ...condition
          })
        }
        if (leeway === 0 || typeof value !== 'number') {
          equalsCondition[key] = value
        } else {
          equalsCondition[key] = {
            $lte: value + leeway,
            $gte: value - leeway
          }
        }
      }
      return { $or: branches }
    }
  }
}

export type CursorOf<S extends Sorting<any, any>> = S extends Sorting<any, infer F> ? CursorOfFields<F> : never

interface SortOptions {
  leeway?: number
}

const defaultSortOptions: Required<SortOptions> = {
  leeway: 0
}

export function asc<T, F extends Sortable> (path: ObjectPath<T, F>, options?: SortOptions): SortedField<T, F> {
  return {
    path,
    dir: 1,
    leeway: options?.leeway ?? defaultSortOptions.leeway
  }
}

export function desc<T, F extends Sortable> (path: ObjectPath<T, F>, options?: SortOptions): SortedField<T, F> {
  return {
    path,
    dir: -1,
    leeway: options?.leeway ?? defaultSortOptions.leeway
  }
}
