import { registerAs } from '@nestjs/config'
import * as yup from 'yup'

const configSchema = yup.object({
  MONGO_CONNECTION_URL: yup.string().required(),
  MONGO_TLS_CA_FILE: yup.string()
}).required()

export const mongoConfig = registerAs('mongo-config', async () => await configSchema.validate(process.env, {
  stripUnknown: true,
  abortEarly: false
}))
