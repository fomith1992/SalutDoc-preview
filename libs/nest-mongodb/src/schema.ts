import bson from 'bson'
import mongodb from 'mongodb'
import * as rt from 'runtypes'

// this file contains Runtypes feature subset which are expressible in BSON

export {
  Array, Boolean, Dictionary, Null, Number as Double, Partial, Record as Object, Static, String, Tuple, Union,
  Runtype, Unknown
} from 'runtypes'

// there are effectively 2 ObjectId functions:
// 1) one from mongodb which in fact exposes it from from bson@1,
// 2) one from bson@4 which is our direct dependency
// we get the first one when interact with mongodb by find, insert, etc.
// and we get the second one when parse bson/ext-json using bson library
// however we want use the same scheme for both cases, so either ObjectId must be accepted by the scheme
export const ObjectId = rt.Union(rt.InstanceOf(mongodb.ObjectId), rt.InstanceOf(bson.ObjectId))
export const Date = rt.InstanceOf(global.Date)

type EnumBase = string | number | boolean | null
export function Enum<T extends [EnumBase, ...EnumBase[]]> (...cases: T): rt.Runtype<T[number]> {
  if (cases.length === 1) {
    return rt.Literal(cases[0])
  } else {
    return rt.Union.apply(null, cases.map(rt.Literal) as any) as any
  }
}

export const Int32 = rt.Number.withConstraint(
  x => Number.isSafeInteger(x) && x >= (1 << 31) && x <= ~(1 << 31),
  { name: 'Int32' }
)

// same goes here but for Long function, see ObjectId
export const Int64 = rt.Union(
  rt.InstanceOf(mongodb.Long),
  rt.InstanceOf(bson.Long),
  rt.Number.withConstraint(
    x => Number.isSafeInteger(x),
    { name: 'Integer' }
  )
)

export function checkStrict<R extends rt.Partial<any>> (schema: R, x: unknown): rt.Static<R>
export function checkStrict<R extends rt.Record<any, any>> (schema: R, x: unknown): rt.Static<R> {
  schema.assert(x)
  for (const key of Object.keys(x)) {
    if (!(key in schema.fields)) {
      throw new rt.ValidationError('Unexpected key', key)
    }
  }
  return x
}
