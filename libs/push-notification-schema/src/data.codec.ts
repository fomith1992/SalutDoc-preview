import * as c from 'io-ts/Codec'

export const notificationData = c.sum('type')({
  NEW_CONSULTATION: c.type({
    type: c.literal('NEW_CONSULTATION'),
    consultationId: c.string,
    specialization: c.string
  }),
  START_CONSULTATION: c.type({
    type: c.literal('START_CONSULTATION'),
    consultationId: c.string
  }),
  NEW_MESSAGE: c.type({
    type: c.literal('NEW_MESSAGE'),
    consultationId: c.string
  }),
  ADD_REPORT_CONSULTATION: c.type({
    type: c.literal('ADD_REPORT_CONSULTATION'),
    consultationId: c.string
  }),
  FINISH_CONSULTATION: c.type({
    type: c.literal('FINISH_CONSULTATION'),
    consultationId: c.string
  })
})

export type NotificationDataJson = c.TypeOf<typeof notificationData>
