import { Global, Module } from '@nestjs/common'
import { APP_PIPE } from '@nestjs/core'
import { DtoValidationPipe } from './dto-validation-pipe'

@Global()
@Module({
  providers: [
    {
      provide: APP_PIPE,
      useClass: DtoValidationPipe
    }
  ]
})
export class RpcValidationModule {
}
