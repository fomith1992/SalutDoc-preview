import { SetMetadata } from '@nestjs/common'
import { Transformer } from '@salutdoc/utils/lib/transform'
import { TRANSFORMER_METADATA_KEY } from './transformer.decorator'

export function createDtoClass<I, O> (transformer: Transformer<I, O>): new() => O {
  @SetMetadata(TRANSFORMER_METADATA_KEY, transformer)
  class TransformerClass {
  }

  return TransformerClass as new() => O
}
