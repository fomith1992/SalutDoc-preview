import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { RpcException } from '@nestjs/microservices'
import * as t from '@salutdoc/utils/lib/transform'
import * as either from 'fp-ts/Either'
import { pipe } from 'fp-ts/function'
import { drawForest } from 'fp-ts/Tree'
import { status } from 'grpc'
import { TRANSFORMER_METADATA_KEY } from './transformer.decorator'

@Injectable()
export class DtoValidationPipe implements PipeTransform {
  constructor (
    private readonly reflector: Reflector
  ) {
  }

  transform (value: any, { metatype }: ArgumentMetadata): any {
    let transformer: t.Transformer<any, any>
    if (metatype == null || (transformer = this.reflector.get(TRANSFORMER_METADATA_KEY, metatype)) == null) {
      // not applicable
      return value
    }

    const { guard, transform } = transformer
    return pipe(
      guard(value),
      either.chain(transform),
      either.getOrElseW(errors => {
        throw new RpcException({
          code: status.INVALID_ARGUMENT,
          message: drawForest(errors)
        })
      })
    )
  }
}
