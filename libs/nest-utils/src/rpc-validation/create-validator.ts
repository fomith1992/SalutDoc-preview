import { RpcException } from '@nestjs/microservices'
import * as either from 'fp-ts/lib/Either'
import { flow } from 'fp-ts/lib/function'
import { status } from 'grpc'
import { drawForest } from 'fp-ts/lib/Tree'
import { Transformer } from '@salutdoc/utils/lib/transform'

export function createValidator<T> (): <I extends T, O> (transformer: Transformer<I, O>) => (data: T) => O {
  return ({ guard, transform }) => flow(
    guard,
    either.chain(transform),
    either.getOrElseW(errors => {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: drawForest(errors)
      })
    })
  )
}
