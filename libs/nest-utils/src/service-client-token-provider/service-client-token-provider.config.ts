import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'

const configSchema = t.type({
  SERVICE_CLIENT_TOKEN: t.string
})

export const serviceClientToken = registerAs('service-client-token',
  () => t.transformOrThrow(configSchema)(process.env))
