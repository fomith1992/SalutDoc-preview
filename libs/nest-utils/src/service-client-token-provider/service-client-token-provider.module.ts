import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ServiceClientTokenProvider } from './service-client-token-provider'
import { serviceClientToken } from './service-client-token-provider.config'

@Module({
  imports: [
    ConfigModule.forFeature(serviceClientToken)
  ],
  providers: [
    ServiceClientTokenProvider
  ],
  exports: [
    ServiceClientTokenProvider
  ]
})
export class ServiceClientTokenProviderModule {
}
