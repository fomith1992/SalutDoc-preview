import { Injectable, Inject } from '@nestjs/common'
import { ConfigType } from '@nestjs/config'
import { Metadata } from 'grpc'
import { serviceClientToken } from './service-client-token-provider.config'

@Injectable()
export class ServiceClientTokenProvider {
  private readonly serviceToken: string

  constructor (@Inject(serviceClientToken.KEY) config: ConfigType<typeof serviceClientToken>) {
    this.serviceToken = config.SERVICE_CLIENT_TOKEN
  }

  provideMetadata (): Metadata {
    const metadata = new Metadata()
    metadata.add('serviceToken', this.serviceToken)
    return metadata
  }
}
