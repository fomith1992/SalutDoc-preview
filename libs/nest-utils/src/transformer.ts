import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common'
import * as t from '@salutdoc/utils/lib/transform'
import * as either from 'fp-ts/lib/Either'
import { flow } from 'fp-ts/lib/function'
import { drawForest } from 'fp-ts/lib/Tree'

@Injectable()
export class Transformer<T> implements PipeTransform<unknown, T> {
  constructor (private readonly transformer: t.Transformer<any, T>) {
  }

  transform = flow(
    this.transformer.guard,
    either.chain(this.transformer.transform),
    either.getOrElseW(errors => {
      throw new BadRequestException(drawForest(errors))
    })
  )
}
