import { Module } from '@nestjs/common'
import { TokenCodec } from './token-codec'

@Module({
  providers: [TokenCodec],
  exports: [TokenCodec]
})
export class TokenCodecModule {
}
