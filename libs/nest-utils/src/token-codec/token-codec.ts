import { Injectable } from '@nestjs/common'
import * as t from '@salutdoc/utils/lib/transform'

@Injectable()
export class TokenCodec {
  // TODO sign / encrypt tokens

  encode (token: Json | null): string {
    return token == null ? '' : Buffer.from(JSON.stringify(token)).toString('base64')
  }

  decode (value: string): Json | null {
    return value === '' ? null : JSON.parse(Buffer.from(value, 'base64').toString())
  }
}

export type Json = string | number | boolean | Json[] | { [key: string]: Json } | null

export const json: t.Transformer<Json, Json> = t.union(
  t.string,
  t.number,
  t.boolean,
  t.array(t.lazy(() => json)),
  t.record(t.lazy(() => json)),
  t.literal(null)
)
