import { status } from 'grpc'
import { RpcException } from '@nestjs/microservices'
import { Access } from '@salutdoc/services-clients'

export function checkAccess (access: Access): void {
  if (access.decision !== 'ALLOWED') {
    throw new RpcException({
      code: status.PERMISSION_DENIED,
      message: access.reason
    })
  }
}
