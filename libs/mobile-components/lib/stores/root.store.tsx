import * as React from 'react'
import { types, Instance } from 'mobx-state-tree'
import { DomainsStore, initializeDomainsStore } from './domains/domains.store'
import useAsync from 'react-use/lib/useAsync'

const RootStore = types.model({
  domains: DomainsStore
})

export type RootStoreInstance = Instance<typeof RootStore>

export const initializeRootStore = async (): Promise<RootStoreInstance> => {
  try {
    const store = RootStore.create({
      domains: await initializeDomainsStore()
    })
    return store
  } catch (error) {
    console.log(error)
    throw error
  }
}

const RootStoreContext = React.createContext<RootStoreInstance | null>(null)

export const useRootStore = (): RootStoreInstance => {
  const store = React.useContext(RootStoreContext)
  if (store == null) {
    throw new Error('Root store is not initialized')
  } else {
    return store
  }
}

export interface RootStoreProviderProps {
  children: React.ReactNode
  fallback?: React.ReactNode
}

export const RootStoreProvider = ({ children, fallback }: RootStoreProviderProps): React.ReactElement => {
  const { loading, value: rootStore } = useAsync(initializeRootStore)
  if (loading) {
    return <>{fallback}</>
  }
  return (
    <RootStoreContext.Provider value={rootStore ?? null}>
      {children}
    </RootStoreContext.Provider>
  )
}
