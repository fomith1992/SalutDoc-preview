import { types, Instance, SnapshotIn } from 'mobx-state-tree'
import { v4 as uuid } from 'uuid'

const Id = types.model({
  blockId: types.optional(types.identifier, () => uuid())
})

const TextBlock = types.model({
  type: types.literal('text'),
  text: types.string,
  placeholder: types.maybe(types.string)
})

const HeadingBlock = types.model({
  type: types.literal('heading'),
  text: types.string
})

const ImageBlock = types.model({
  type: types.literal('image'),
  uri: types.string
})

const ArticleBlock = types.union(TextBlock, HeadingBlock, ImageBlock)

export type ArticleBlockInstance = Instance<typeof ArticleBlock>

const ArticleBlockWithId = types.compose(Id, types.model({
  block: ArticleBlock
}))

export type ArticleBlockWithIdInstance = Instance<typeof ArticleBlockWithId>

const Community = types.model({
  id: types.string,
  name: types.string
})

const Article = types.model({
  id: types.identifier,
  title: types.maybe(types.string),
  subject: types.maybe(types.string),
  community: types.maybeNull(Community),
  blocks: types.array(ArticleBlockWithId)
})
  .actions(self => ({
    setTitle: (newTitle: string) => {
      self.title = newTitle
    },
    setSubject: (newSubject: string | undefined) => {
      self.subject = newSubject
    },
    setCommunity: (newCommunity: CommunityInstance | null) => {
      self.community = newCommunity
    },
    setBlocks: (newBlocks: readonly ArticleBlockWithIdInstance[]) => {
      self.blocks.replace([...newBlocks])
    },
    addBlock: (block: ArticleBlockInstance) => {
      self.blocks.push(ArticleBlockWithId.create({ block }))
    },
    addBlockToBeginning: (block: ArticleBlockInstance) => {
      self.blocks.unshift(ArticleBlockWithId.create({ block }))
    },
    removeBlock: (blockId: string) => {
      self.blocks.replace(
        self.blocks.filter(block => block.blockId !== blockId)
      )
    },
    updateBlock: (blockId: string, newBlock: ArticleBlockInstance) => {
      self.blocks.replace(self.blocks.map((block: ArticleBlockWithIdInstance) => {
        return block.blockId === blockId
          ? { block: newBlock, blockId }
          : block
      }))
    }
  }))
  .actions(self => ({
    clearArticle: () => {
      self.blocks.replace([])
      self.community = null
      self.subject = undefined
      self.title = ''
    }
  }))

export type CommunityInstance = Instance<typeof Community>
export type ArticleInstance = Instance<typeof Article>

export const ArticleConstructorStore = types
  .model('ArticleConstructorStore', {
    newArticle: Article,
    newMaterial: Article,
    newAlbum: Article,
    editedArticles: types.map(Article)
  })
  .actions(self => ({
    getAtricle: (id: string): ArticleInstance => {
      const article = self.editedArticles.get(id)
      if (article == null) {
        const emptyArticle = {
          id,
          blocks: [],
          community: null,
          subject: undefined,
          title: ''
        }
        return self.editedArticles.put(emptyArticle)
      }
      return article
    }
  }))

export type NewArticleStoreInstance = Instance<typeof ArticleConstructorStore>

export const initializeNewArticleStore = (): SnapshotIn<typeof ArticleConstructorStore> => ({
  editedArticles: {},
  newArticle: {
    id: uuid(),
    blocks: [
      {
        blockId: uuid(),
        block: { text: '', type: 'text' }
      }
    ],
    community: null,
    subject: undefined,
    title: ''
  },
  newMaterial: {
    id: uuid(),
    blocks: [
      {
        blockId: uuid(),
        block: { text: 'Жалобы и осмотр', type: 'heading' }
      },
      {
        blockId: uuid(),
        block: { text: '', type: 'text', placeholder: 'Например: у пациентки после родов увеличилась и расползлась грудь.' }
      },
      {
        blockId: uuid(),
        block: { text: 'Диагноз и пожелания', type: 'heading' }
      },
      {
        blockId: uuid(),
        block: { text: '', type: 'text', placeholder: 'Например: необходимость увеличить грудные железы.' }
      },
      {
        blockId: uuid(),
        block: { text: 'Лечение', type: 'heading' }
      },
      {
        blockId: uuid(),
        block: { text: '', type: 'text', placeholder: 'Например: чтобы скорректировать это изменение в фигуре ей была проведена операция по вживлению имплантов анатомической формы' }
      },
      {
        blockId: uuid(),
        block: { text: 'Результат', type: 'heading' }
      },
      {
        blockId: uuid(),
        block: { text: '', type: 'text', placeholder: 'Например: фигура вернулась к состоянию до беременности. Имплант отлично прижился в организме. ' }
      }
    ],
    community: null,
    subject: undefined,
    title: ''
  },
  newAlbum: {
    id: uuid(),
    blocks: [],
    community: null,
    subject: undefined,
    title: ''
  }
})
