import { Instance, SnapshotIn, types } from 'mobx-state-tree'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { persist } from 'mst-persist'

export const NotificationsSettingsStore = types
  .model('NotificationsSettingsStore', {
    notifyOnNewFollower: types.boolean,
    notifyOnLikeReceived: types.boolean,
    notifyOnDislikeReceived: types.boolean,
    notifyOnCommentReplyReceived: types.boolean,
    notifyOnCommunityInvitationReceived: types.boolean
  })
  .actions(self => ({
    toggleNotifyOnNewFollower: (value: boolean): void => {
      self.notifyOnNewFollower = value
    },
    toggleNotifyOnLikeReceived: (value: boolean): void => {
      self.notifyOnLikeReceived = value
    },
    toggleNotifyOnDislikeReceived: (value: boolean): void => {
      self.notifyOnDislikeReceived = value
    },
    toggleNotifyOnCommentReplyReceived: (value: boolean): void => {
      self.notifyOnCommentReplyReceived = value
    },
    toggleNotifyOnCommunityInvitationReceived: (value: boolean): void => {
      self.notifyOnCommunityInvitationReceived = value
    }
  }))

export type NotificationsSettingsStoreInstance = Instance<typeof NotificationsSettingsStore>

export const initializeNotificationsSettingsStore = async (): Promise<SnapshotIn<typeof NotificationsSettingsStore>> => {
  try {
    const store = NotificationsSettingsStore.create({
      notifyOnNewFollower: true,
      notifyOnLikeReceived: true,
      notifyOnDislikeReceived: true,
      notifyOnCommentReplyReceived: true,
      notifyOnCommunityInvitationReceived: true
    })

    await persist('NotificationsSettingsStore', store, {
      storage: AsyncStorage,
      jsonify: true,
      whitelist: [
        'notifyOnNewFollower',
        'notifyOnLikeReceived',
        'notifyOnDislikeReceived',
        'notifyOnCommentReplyReceived',
        'notifyOnCommunityInvitationReceived'
      ]
    }).then(() => console.log('NotificationsSettingsStore hydrated'))

    return store
  } catch (error) {
    console.log(error)
    throw error
  }
}
