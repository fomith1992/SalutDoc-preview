import { ArticleConstructorStore, initializeNewArticleStore } from './new-article.store'
import { Instance, SnapshotIn, types } from 'mobx-state-tree'
import { NotificationsSettingsStore, initializeNotificationsSettingsStore } from './notifications-settings.store'
import { UserProfileStore, initializeUserProfileStore } from './user-profile.store'
import { MedicalReportStore, initializeMedicalReportStore } from './new-medical-report.store'
import { Register, initializeRegisterStore } from './register.store'

export const DomainsStore = types.model('DomainsStore', {
  articleData: ArticleConstructorStore,
  userProfile: UserProfileStore,
  notificationsSettings: NotificationsSettingsStore,
  medicalReport: MedicalReportStore,
  registerReport: Register
})

export type DomainsStoreInstance = Instance<typeof DomainsStore>

export const initializeDomainsStore = async (): Promise<SnapshotIn<typeof DomainsStore>> => ({
  articleData: initializeNewArticleStore(),
  userProfile: await initializeUserProfileStore(),
  notificationsSettings: await initializeNotificationsSettingsStore(),
  medicalReport: initializeMedicalReportStore(),
  registerReport: initializeRegisterStore()
})
