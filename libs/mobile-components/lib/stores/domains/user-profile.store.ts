import { types, Instance, SnapshotIn } from 'mobx-state-tree'
import { persist } from 'mst-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'

const AppModule = types.union(types.literal('DOCTOR'), types.literal('PATIENT'))
export type AppModuleInstance = Instance<typeof AppModule>

export const UserProfileStore = types
  .model('UserProfileStore', {
    verificationOptionDismissed: types.boolean,
    appMode: AppModule
  })
  .actions(self => ({
    dismissVerification: (): void => {
      self.verificationOptionDismissed = true
    },
    setAppMode: (value: AppModuleInstance): void => {
      self.appMode = value
    }
  }))

export type UserProfileStoreInstance = Instance<typeof UserProfileStore>

export const initializeUserProfileStore = async (): Promise<SnapshotIn<typeof UserProfileStore>> => {
  try {
    const store = UserProfileStore.create({
      verificationOptionDismissed: false,
      appMode: 'PATIENT'
    })

    await persist('UserProfileStore', store, {
      storage: AsyncStorage,
      jsonify: true,
      whitelist: [
        'verificationOptionDismissed',
        'appMode'
      ]
    }).then(() => console.log('UserProfileStorage hydrated'))

    return store
  } catch (error) {
    console.log(error)
    throw error
  }
}
