import { types, Instance, SnapshotIn } from 'mobx-state-tree'

export const Register = types.model({
  phoneNumber: types.maybe(types.string),
  rawPhoneNumber: types.maybe(types.string),
  token: types.maybe(types.string),
  specialization: types.maybe(types.string),
  firstName: types.maybe(types.string),
  lastName: types.maybe(types.string),
  birthday: types.maybe(types.string),
  gender: types.union(types.literal('MALE'), types.literal('FEMALE')),
  mode: types.union(types.literal('DEFAULT'), types.literal('IN_CONSULTATION'))
})
  .actions(self => ({
    setPhoneNumber: (phoneNumber: string) => {
      self.phoneNumber = phoneNumber
    },
    setRawPhoneNumber: (rawPhoneNumber: string) => {
      self.rawPhoneNumber = rawPhoneNumber
    },
    setFirstName: (firstName: string) => {
      self.firstName = firstName
    },
    setLastName: (lastName: string) => {
      self.lastName = lastName
    },
    setBirthday: (birthday: string) => {
      self.birthday = birthday
    },
    setGender: (gender: 'MALE' | 'FEMALE') => {
      self.gender = gender
    },
    setToken: (token: string) => {
      self.token = token
    },
    setSpecialization: (specialization: string) => {
      self.specialization = specialization
    },
    setMode: (mode: 'DEFAULT' | 'IN_CONSULTATION') => {
      self.mode = mode
    }
  }))

export type RegisterInstance = Instance<typeof Register>

export const initializeRegisterStore = (): SnapshotIn<typeof Register> => ({
  gender: 'MALE',
  birthday: '',
  firstName: '',
  lastName: '',
  phoneNumber: '',
  specialization: '',
  mode: 'DEFAULT'
})
