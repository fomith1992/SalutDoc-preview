import { types, Instance, SnapshotIn } from 'mobx-state-tree'
import { v4 as uuid } from 'uuid'

const ReportBlock = types.model({
  blockId: types.optional(types.identifier, () => uuid()),
  title: types.string,
  name: types.string,
  value: types.string,
  placeholder: types.maybe(types.string)
})

export type MedicalReportBlockInstance = Instance<typeof ReportBlock>

const MedicalReportBlockWithId = types.model({
  block: ReportBlock
})

export type MedicalReportBlockWithIdInstance = Instance<typeof MedicalReportBlockWithId>

const MedicalReport = types.model({
  blocks: types.array(MedicalReportBlockWithId)
})
  .actions(self => ({
    updateBlock: (blockId: string, newBlock: MedicalReportBlockInstance) => {
      self.blocks.replace(self.blocks.map((block: MedicalReportBlockWithIdInstance) => {
        return block.block.blockId === blockId
          ? { block: newBlock, blockId }
          : block
      }))
    },
    clearMedicalReport: () => {
      self.blocks.replace(
        self.blocks.map((block: MedicalReportBlockWithIdInstance): MedicalReportBlockWithIdInstance => ({
          block: {
            ...block.block,
            value: ''
          }
        }))
      )
    },
    getBlockByName: (name: string): MedicalReportBlockWithIdInstance => {
      const block = self.blocks.filter(b => b.block.name === name)[0] ?? null
      if (block == null) {
        throw new Error(`Block with name '${name}' not found in MedicalReportStore`)
      }
      return block
    }
  }))

export type MedicalReportInstance = Instance<typeof MedicalReport>

export const MedicalReportStore = types
  .model('MedicalReportStore', {
    newMedicalReport: MedicalReport
  })

export type NewMedicalReportStoreInstance = Instance<typeof MedicalReportStore>

export const initializeMedicalReportStore = (): SnapshotIn<typeof MedicalReportStore> => ({
  newMedicalReport: {
    blocks: [
      {
        blockId: uuid(),
        block: {
          title: 'Наиболее вероятное заболевание',
          name: 'diagnosis',
          value: '',
          placeholder: 'ОРВИ'
        }
      },
      {
        blockId: uuid(),
        block: {
          title: 'Рекомендуемая диагностика',
          name: 'diagnostics',
          value: '',
          placeholder: 'Общий анализ крови'
        }
      },
      {
        blockId: uuid(),
        block: {
          title: 'Рекомендуемые врачи',
          name: 'doctors',
          value: '',
          placeholder: 'Рекомендую посетить очный прием ЛОРа'
        }
      },
      {
        blockId: uuid(),
        block: {
          title: 'Рекомендации по лекарствам',
          name: 'medicines',
          value: '',
          placeholder: 'Ринза'
        }
      },
      {
        blockId: uuid(),
        block: {
          title: 'Общие рекомендации',
          name: 'recommendation',
          value: '',
          placeholder: 'Постельный режим'
        }
      }
    ]
  }
})
