import React from 'react'
import { useNetInfo } from '@react-native-community/netinfo'

interface ConnectionStateGateProps {
  children: React.ReactElement
  failedConnectionScreen: React.ReactElement
}

export function ConnectionStateGate (
  { children, failedConnectionScreen }: ConnectionStateGateProps
): React.ReactElement {
  const netInfo = useNetInfo()
  return netInfo.isConnected ? children : failedConnectionScreen
}
