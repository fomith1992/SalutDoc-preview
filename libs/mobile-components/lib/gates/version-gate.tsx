import React from 'react'
import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'
import semver from 'semver'
import DeviceInfo from 'react-native-device-info'
import useAsync from 'react-use/lib/useAsync'

const mobileVersionUrl = expectNonNil(process.env.MOBILE_VERSION_URL)

interface VersionGateProps {
  fallback?: React.ReactNode
  children: React.ReactElement
  invalidScreen: React.ReactElement
  errorConnectServerScreen: React.ReactElement
}

async function getActualRange (): Promise<string> {
  try {
    const result = await fetch(mobileVersionUrl)
    if (result.ok) {
      return await result.text()
    } else {
      throw new Error('Failed connect to server')
    }
  } catch (error) {
    console.error(error)
    throw error
  }
}

export function VersionGate (
  { fallback, invalidScreen, children, errorConnectServerScreen }: VersionGateProps
): React.ReactElement {
  const { loading, value: actualRange } = useAsync(getActualRange)
  const currentVersion = DeviceInfo.getVersion()
  if (loading) {
    return <>{fallback}</>
  }
  if (actualRange === undefined || semver.validRange(actualRange) == null) {
    return errorConnectServerScreen
  }
  return semver.satisfies(currentVersion, actualRange) ? children : invalidScreen
}
