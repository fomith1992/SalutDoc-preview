import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { Platform } from 'react-native'
import { yup } from '../../modules/form-kit'
import { TextInput } from '../../modules/form-kit/text-input'
import { Modal, SubmitTextButton } from '../../modules/ui-kit'
import {
  ModalWindowActivity,
  ModalWindowContainer,
  Overlay,
  ButtonContainer
} from './request-modal.style'

export interface RequestModalViewProps {
  visible: boolean
  displaySuccessScreen?: boolean
  onCancel: () => void
  onSubmit: SubmissionHandler<ReportForm>
  modalTitle: string
  placeholder: string
}

const formSchema = yup.object({
  reportText: yup.string()
    .min(1).max(1000)
    .required()
}).required()

export type ReportForm = yup.InferType<typeof formSchema>
const { Form, Field, Submit } = createFormDescriptor(formSchema)

export function RequestModalView (
  {
    visible,
    onCancel,
    onSubmit,
    modalTitle,
    placeholder
  }: RequestModalViewProps
): React.ReactElement {
  return (
    <Modal visible={visible}>
      <Overlay behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <Form
          onSubmit={onSubmit}
        >
          <ModalWindowContainer>
            <Field name='reportText'>
              {props => (
                <TextInput
                  {...props}
                  placeholder={placeholder}
                  label={modalTitle}
                  numberOfLines={3}
                />
              )}
            </Field>
            <ModalWindowActivity>
              <ButtonContainer>
                <SubmitTextButton
                  valid
                  type='link'
                  size='L'
                  onSubmit={onCancel}
                >
                  Отмена
                </SubmitTextButton>
              </ButtonContainer>
              <Submit>
                {props =>
                  <ButtonContainer>
                    <SubmitTextButton
                      type='link'
                      size='L'
                      {...props}
                    >
                      Отправить
                    </SubmitTextButton>
                  </ButtonContainer>}
              </Submit>
            </ModalWindowActivity>
          </ModalWindowContainer>
        </Form>
      </Overlay>
    </Modal>
  )
}
