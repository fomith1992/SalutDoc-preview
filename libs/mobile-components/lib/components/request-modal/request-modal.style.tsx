import { Text, TouchableOpacity, View, KeyboardAvoidingView, Platform } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { sp } from '../../style/size'
import { TickIcon24 } from '../../images/tick.icon-24'

export const Overlay = styled(KeyboardAvoidingView, {
  backgroundColor: 'rgba(148, 148, 148, 0.4)',
  flex: 1,
  justifyContent: 'center'
})

export const ModalWindowActivity = styled(View, {
  flexDirection: 'row',
  justifyContent: 'flex-end',
  marginTop: sp(4)
})

export const ModalWindowContainer = styled(View, {
  ...container(),
  backgroundColor: color('surface'),
  paddingTop: sp(20),
  paddingHorizontal: sp(20),
  paddingBottom: sp(8),
  borderRadius: 4,
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const ButtonContainer = styled(TouchableOpacity, {
  marginTop: sp(4),
  paddingHorizontal: sp(16)
})

export const ClosingContainer = styled(View, {
  flex: 1,
  justifyContent: 'center'
})

export const SuccessContainer = styled(View, {
  marginVertical: sp(8),
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
})

export const ImgContainer = styled(View, {
  width: 64,
  height: 64,
  borderRadius: 32,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('secondaryLight')
})

export const Tick = styled(TickIcon24, {
  width: sp(20),
  height: sp(20),
  color: color('accent')
})

export const SuccessText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  marginTop: sp(12)
})
