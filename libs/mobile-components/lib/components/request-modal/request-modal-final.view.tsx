import React from 'react'
import { DismissibleModal, SubmitTextButton } from '../../modules/ui-kit'
import {
  ModalWindowContainer,
  SuccessContainer,
  ImgContainer,
  Tick,
  SuccessText,
  ClosingContainer
} from './request-modal.style'

export interface RequestModalViewProps {
  onClose: () => void
}

export function RequestModalFinalView ({ onClose }: RequestModalViewProps
): React.ReactElement {
  return (
    <DismissibleModal
      onDismiss={onClose}
      background='gray'
      visible
    >
      <ClosingContainer>
        <ModalWindowContainer>
          <SuccessContainer>
            <ImgContainer>
              <Tick />
            </ImgContainer>
            <SuccessText>
              Спасибо за отзыв!
            </SuccessText>
            <SubmitTextButton
              type='link'
              onSubmit={onClose}
              valid
            >
              Закрыть
            </SubmitTextButton>
          </SuccessContainer>
        </ModalWindowContainer>
      </ClosingContainer>
    </DismissibleModal>
  )
}
