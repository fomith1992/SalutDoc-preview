import React from 'react'
import { RequestModalView } from './request-modal.view'
import { Base64 } from 'js-base64'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'

interface RequestModalProps {
  isVisible: boolean
  onCancel: () => void
  modalTitle: string
  placeholder: string
  contentType: 'post' | 'community' | 'profile' | 'work' | 'question' | 'comment' | 'answer'
  contentId: string
}

export function RequestModal ({
  isVisible,
  onCancel,
  modalTitle,
  placeholder,
  contentType,
  contentId
}: RequestModalProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const dict = {
    post: 'пост',
    community: 'сообщество',
    profile: 'профиль',
    work: 'работу',
    question: 'вопрос',
    comment: 'комментарий',
    answer: 'ответ на вопрос'
  }
  return (
    <RequestModalView
      visible={isVisible}
      onCancel={onCancel}
      modalTitle={modalTitle}
      placeholder={placeholder}
      onSubmit={data => {
        fetch('https://salutdoc.atlassian.net/rest/servicedeskapi/request', {
          method: 'post',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Basic ${Base64.encode('salutdoc@yandex.ru:GWbdktXn6AEuXw3BEfcu9AF7')}`
          },
          body: JSON.stringify({
            serviceDeskId: '1',
            requestTypeId: '18',
            requestFieldValues: {
              summary: `Жалоба на ${dict[contentType]}`,
              labels: [
                `userId:${user == null ? 'anonymous' : user.id}`,
                `contentType:${contentType}`,
                `contentId:${contentId}`
              ],
              description: data.reportText
            }
          })
        }).then(response => {
          if (!response.ok) {
            console.log(response.text)
            throw new Error('Error when sending a complaint')
          }
        }).catch(error => console.log(error))
        onCancel()
        return {}
      }}
    />
  )
}
