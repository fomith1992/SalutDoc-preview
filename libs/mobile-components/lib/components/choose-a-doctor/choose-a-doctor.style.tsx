import { View, TextInput, Platform, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { lift, styled } from '../../style/styled'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { SearchIcon24 } from '../../images/search.icon-24'
import { inputFrameStyle } from '../../modules/form-kit/form-input.style'
import { SettingsSlidersIcon20 } from '../../images/settings-sliders.icon-20'

export const SearchHeader = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  ...Platform.select({
    android: {
      elevation: 5
    },
    ios: {
      borderBottomColor: 'rgb(242, 242, 242)',
      borderBottomWidth: 1
    }
  }),
  paddingVertical: sp(16)
})

export const Container = styled(View, {
  flex: 1,
  backgroundColor: color('surface')
})

export const InputFrame = styled(View, inputFrameStyle)

export const InputContainer = styled(InputFrame, {
  padding: sp(12)
})

export const SearchImg = styled(SearchIcon24, {
  width: sp(16),
  height: sp(16),
  color: color('inactive')
})

export const SettingsImg = styled(SettingsSlidersIcon20, {
  color: color('accent')
})

export const HeaderInputButton = TouchableOpacity

export const Input = styled(TextInput, {
  ...font({ type: 'text1' }),
  color: color('text'),
  paddingVertical: 0,
  marginHorizontal: sp(12),
  height: sp(20),
  flex: 1
})

export const RowContainer = styled(TouchableOpacity, {
  ...container(),
  paddingVertical: sp(12),
  flexDirection: 'row',
  alignItems: 'center'
})

export const UserInfoContainer = styled(View, {
  marginLeft: sp(12)
})

export const Name = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginBottom: sp(4)
})

export const RatingContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarContainer = styled(View, {
  width: sp(64)
})

export const RatingNum = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext')
})

export const Description = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  marginTop: sp(4)
})

export const HeaderContainer = styled(View, {
  ...container(),
  marginTop: sp(16),
  marginBottom: sp(12)
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  marginTop: sp(8)
})

export const FilterItem = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('surface'),
  padding: sp(8),
  backgroundColor: color('accent'),
  marginRight: sp(8)
})

export const BackgroundOpacityStyleSheet = lift({
  ...StyleSheet.absoluteFillObject,
  backgroundColor: color('shadow')
})

export const ContentContainerStyleSheet = lift({
  backgroundColor: color('surface')
})

export const BottomSheetHeaderContainer = styled(View, {
  paddingVertical: sp(12),
  borderTopRightRadius: sp(16),
  borderTopLeftRadius: sp(16),
  backgroundColor: color('surface'),
  justifyContent: 'center',
  alignItems: 'center'
})

export const HeaderHandler = styled(View, {
  width: sp(40),
  height: sp(4),
  borderRadius: 2,
  backgroundColor: color('inactive')
})

export const TitleContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between'
})

export const BottomSheetTitle = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('text')
})

export const ClearButtonText = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('accent')
})

export const ClearButton = TouchableOpacity

export const ButtonContainer = styled(View, {
  ...container(),
  marginTop: sp(24)
})
