import React, { useState } from 'react'
import { ChooseDoctorView } from './choose-a-doctor.view'

const data = [
  {
    firstName: 'Александра',
    lastName: 'Гафизова',
    rating: 5.0,
    feedbacksCounts: 23,
    specialization: 'therapist',
    followersCount: 12,
    avatar: 'https://test.k8s.linksdoc.com/assets/images/5fca83260d0e1a0012b34a0b'
  },
  {
    firstName: 'Александра',
    lastName: 'Гафизова',
    rating: 3.5,
    feedbacksCounts: 91,
    specialization: 'therapist',
    followersCount: 122
  },
  {
    firstName: 'Александра',
    lastName: 'Гафизова',
    rating: 4.7,
    feedbacksCounts: 27,
    specialization: 'therapist',
    followersCount: 1342,
    avatar: 'https://test.k8s.linksdoc.com/assets/images/5fca83260d0e1a0012b34a0b'
  },
  {
    firstName: 'Александра',
    lastName: 'Гафизова',
    rating: 0.1,
    feedbacksCounts: 1,
    specialization: 'therapist',
    followersCount: 1342
  },
  {
    firstName: 'Александра',
    lastName: 'Гафизова',
    rating: 0,
    feedbacksCounts: null,
    specialization: 'therapist',
    followersCount: 1342
  }
]

export function ChooseDoctor (): React.ReactElement {
  const [query, setQuery] = useState<string>('')
  const [filter, setFilter] = useState<{specialization: string, city?: string}>({ specialization: '' })
  return (
    <ChooseDoctorView
      data={data}
      query={query}
      setQuery={setQuery}
      onSubmitFilter={setFilter}
      filter={filter}
    />
  )
}
