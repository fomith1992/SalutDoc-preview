import React, { useState } from 'react'
import { specializationsData, TextButton } from '../../modules/ui-kit'
import BottomSheet from 'reanimated-bottom-sheet'
import Animated from 'react-native-reanimated'
import { useMaterialized } from '../../style/styled'
import { SelectSearch } from '../../modules/form-kit/select-search/select-search.view'
import { SelectInput } from '../../modules/form-kit'
import {
  BackgroundOpacityStyleSheet,
  ContentContainerStyleSheet,
  HeaderHandler,
  BottomSheetHeaderContainer,
  TitleContainer,
  BottomSheetTitle,
  ClearButtonText,
  ClearButton,
  ButtonContainer
} from './choose-a-doctor.style'

interface ChooseDoctorBottomSheetViewProps {
  onSubmitFilter: (data: {specialization: string, city?: string}) => void
  sheetRef: React.RefObject<BottomSheet>
  fall: Animated.Value<number>
}

const AnimatedView = Animated.View

const headerHeight = 28

const snapPoints = [
  headerHeight + 24 + 24 + 72 + 24 + 72 + 24 + 48 + 24, // header + title + margin + input + margin + input + margin + button + margin
  0
]

export function ChooseDoctorBottomSheetView ({
  onSubmitFilter,
  sheetRef,
  fall
}: ChooseDoctorBottomSheetViewProps): React.ReactElement {
  const [searchedSpecialization, setSearchedSpecialization] = useState<string>('')
  const [searchedCity, setSearchedCity] = useState<string | undefined>(undefined)
  const contentContainerStyle = useMaterialized(ContentContainerStyleSheet)

  function RenderAnumatedShadowView (): React.ReactElement {
    const backgroundOpacityStyle = useMaterialized(BackgroundOpacityStyleSheet)

    const animatedShadowOpacity = Animated.interpolate(fall, {
      inputRange: [0, 1],
      outputRange: [0.5, 0]
    })

    return (
      <AnimatedView
        pointerEvents='none'
        style={{
          ...backgroundOpacityStyle,
          opacity: animatedShadowOpacity
        }}
      />
    )
  }

  const renderContent = (): React.ReactElement => {
    return (
      <AnimatedView
        style={{
          ...contentContainerStyle,
          height: snapPoints[0] ?? 250 - headerHeight * 2
        }}
      >
        <TitleContainer>
          <BottomSheetTitle>
            Фильтры
          </BottomSheetTitle>
          <ClearButton
            onPress={() => {
              setSearchedCity(undefined)
              setSearchedSpecialization('')
              onSubmitFilter({ specialization: '', city: undefined })
              sheetRef.current?.snapTo(1)
            }}
          >
            <ClearButtonText>
              Очистить
            </ClearButtonText>
          </ClearButton>
        </TitleContainer>
        <ButtonContainer>
          <SelectSearch
            onChange={value => setSearchedSpecialization(value ?? '')}
            value={searchedSpecialization}
            options={specializationsData}
            label='Специальность'
            type='input'
          />
        </ButtonContainer>
        <ButtonContainer>
          <SelectInput
            label='Город'
            value={searchedCity}
            onChange={item => setSearchedCity(item)}
            options={[
              { label: 'Москва', value: 'Москва' },
              { label: 'Минск', value: 'Минск' }
            ]}
          />
        </ButtonContainer>
        <ButtonContainer>
          <TextButton
            size='XL'
            onPress={() => {
              onSubmitFilter({ specialization: searchedSpecialization, city: searchedCity })
              sheetRef.current?.snapTo(1)
            }}
            disabled={searchedSpecialization === '' && searchedCity == null}
          >
            Применить
          </TextButton>
        </ButtonContainer>
      </AnimatedView>
    )
  }

  return (
    <>
      <BottomSheet
        ref={sheetRef}
        enabledInnerScrolling={false}
        enabledContentTapInteraction={false}
        snapPoints={snapPoints}
        initialSnap={1}
        callbackNode={fall}
        renderHeader={() => (
          <BottomSheetHeaderContainer>
            <HeaderHandler />
          </BottomSheetHeaderContainer>
        )}
        renderContent={renderContent}
      />
      <RenderAnumatedShadowView />
    </>
  )
}
