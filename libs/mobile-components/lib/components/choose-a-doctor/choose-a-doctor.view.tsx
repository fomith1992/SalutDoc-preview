import React, { useMemo, useRef } from 'react'
import { FlatList } from 'react-native'
import { toCaseCount } from '../../i18n/utils'
import { specializationName } from '../../modules/consultations/i18n/specialization'
import { hitSlopParams } from '../../modules/ui-kit'
import { RatingStarsRow } from '../../modules/ui-kit/rating-stars-row/rating-stars-row.view'
import { Avatar } from '../avatar/avatar.view'
import BottomSheet from 'reanimated-bottom-sheet'
import Animated from 'react-native-reanimated'
import {
  SearchHeader,
  SearchImg,
  Input,
  HeaderInputButton,
  InputContainer,
  SettingsImg,
  RowContainer,
  UserInfoContainer,
  Name,
  RatingContainer,
  RatingNum,
  Description,
  AvatarContainer,
  Title,
  FilterItem,
  Container,
  HeaderContainer
} from './choose-a-doctor.style'
import { ChooseDoctorBottomSheetView } from './choose-a-doctor-bottom-sheet.view'

interface UserData {
  firstName: string
  lastName: string
  rating: number
  feedbacksCounts: number | null
  specialization: string
  followersCount: number
  avatar?: string
}

interface ChooseDoctorViewProps {
  query: string
  setQuery: (value: string) => void
  onSubmitFilter: (data: {specialization: string, city?: string}) => void
  data: UserData[]
  filter: {
    specialization: string
    city?: string
  }
}

function renderItem (item: UserData): React.ReactElement {
  return (
    <RowContainer
      onPress={() => {}}
    >
      <AvatarContainer>
        <Avatar
          url={item.avatar}
          shape='circle-border'
        />
      </AvatarContainer>
      <UserInfoContainer>
        <Name>
          {`${item.firstName} ${item.lastName}`}
        </Name>
        {item.feedbacksCounts != null
          ? (
            <RatingContainer>
              <RatingStarsRow rating={item.rating} />
              <RatingNum>
                {`${item.feedbacksCounts} ${toCaseCount(['отзыв', 'отзыва', 'отзывов'], item.feedbacksCounts)}`}
              </RatingNum>
            </RatingContainer>
          ) : (
            <RatingNum>
              Нет отзывов
            </RatingNum>
          )}
        <Description>
          {`${specializationName[item.specialization] ?? item.specialization}, ${item.followersCount} ${toCaseCount(['подписчик', 'подписчика', 'подписчиков'], item.followersCount)}`}
        </Description>
      </UserInfoContainer>
    </RowContainer>
  )
}

export function ChooseDoctorView ({
  query,
  setQuery,
  onSubmitFilter,
  data,
  filter
}: ChooseDoctorViewProps): React.ReactElement {
  const sheetRef = useRef<BottomSheet>(null)
  const fall = useMemo(() => new Animated.Value(1), [])

  return (
    <>
      <Container>
        <SearchHeader>
          <InputContainer>
            <SearchImg />
            <Input
              placeholder='Введите фамилию врача'
              value={query}
              onChangeText={setQuery}
            />
            <HeaderInputButton
              hitSlop={hitSlopParams(12)}
              onPress={() => sheetRef.current?.snapTo(0)}
            >
              <SettingsImg />
            </HeaderInputButton>
          </InputContainer>
        </SearchHeader>
        <FlatList
          contentContainerStyle={{ flexGrow: 1 }}
          data={data}
          keyExtractor={(_, idx) => idx.toString()}
          renderItem={({ item }) => renderItem(item)}
          ListHeaderComponent={() => (
            <HeaderContainer>
              {query !== '' || filter.city != null || filter.specialization !== ''
                ? (
                  <RatingContainer>
                    {filter.specialization !== ''
                      ? <FilterItem>{specializationName[filter.specialization] ?? filter.specialization}</FilterItem>
                      : null}
                    {filter.city != null
                      ? <FilterItem>{filter.city}</FilterItem>
                      : null}
                  </RatingContainer>
                ) : (
                  <Title>
                    Популярные врачи
                  </Title>
                )}
            </HeaderContainer>
          )}
        />
      </Container>
      <ChooseDoctorBottomSheetView
        onSubmitFilter={onSubmitFilter}
        sheetRef={sheetRef}
        fall={fall}
      />
    </>
  )
}
