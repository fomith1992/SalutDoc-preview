import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'

export const EmptyCommunityContainer = styled(View, {
  marginTop: sp(32),
  paddingHorizontal: sp(48),
  flexDirection: 'column',
  justifyContent: 'center'
})
export const EmptyCommunityText = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('subtext'),
  textAlign: 'center'
})

export const CommunitiesFollowButton = styled(TouchableOpacity, {
  padding: sp(12)
})

export const CommunitiesFollowButtonText = styled(Text, {
  ...font({ type: 'h3' }),
  color: color('accent'),
  textAlign: 'center'
})
