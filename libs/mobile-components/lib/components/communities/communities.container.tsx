import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { useUserCommunitiesListQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'
import { CommunitiesView } from './communities.view'

export interface Community {
  id: string
  name: string
  description: string
  avatar: string | null
}

interface CommunitiesListProps {
  userId: string
  renderAction: (communityId: string) => React.ReactElement
  onCommunityPress: (community: Community) => void
}

export const CommunitiesList = ({
  userId,
  onCommunityPress,
  renderAction
}: CommunitiesListProps): React.ReactElement => {
  const user = useAuthenticatedUserOrNull()
  const navigation = useNavigation()
  const { loading, data, fetchMore, refetch } = useUserCommunitiesListQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      userId,
      after: null
    }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.userById?.communitiesMembership?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousCommunitiesMembership = expectDefined(
          previousResult.userById?.communitiesMembership)
        return {
          userById: {
            __typename: 'User',
            id: userId,
            communitiesMembership: appendEdges(
              previousCommunitiesMembership, fetchMoreResult?.userById?.communitiesMembership)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.userById?.communitiesMembership?.pageInfo.endCursor])

  return (
    <CommunitiesView
      items={
        data?.userById?.communitiesMembership?.edges.map(
          ({ node: { community } }) => ({
            id: community.id,
            avatar: community.avatar,
            title: community.name,
            description: community.description,
            action: renderAction(community.id),
            onPress: () => onCommunityPress(community)
          })) ?? []
      }
      loading={loading && !refreshing}
      hasMore={data?.userById?.communitiesMembership?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore()}
      refreshing={refreshing}
      refresh={refresh}
      isMy={user?.id === userId}
      onFollowClick={() => navigation.dispatch(StackActions.push('Search', { type: 'COMMUNITY' }))}
    />
  )
}
