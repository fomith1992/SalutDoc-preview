import React from 'react'
import { FlatList } from 'react-native'
import { ListItemProps, ListItem } from '../list/list.view'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import {
  EmptyCommunityContainer,
  EmptyCommunityText,
  CommunitiesFollowButton,
  CommunitiesFollowButtonText
} from './communities.style'

export interface CommunitiesViewProps {
  items: ListItemProps[]
  hasMore: boolean
  loading: boolean
  loadMore: () => void
  refreshing: boolean
  refresh: () => void
  onFollowClick: () => void
  isMy: boolean
}

export const CommunitiesView = ({
  items,
  hasMore,
  loading,
  loadMore,
  refreshing,
  refresh,
  onFollowClick,
  isMy
}: CommunitiesViewProps): React.ReactElement => {
  const renderItem = ({ item }: { item: ListItemProps }): React.ReactElement => (
    <ListItem {...item} type='community' />
  )
  return (
    <FlatList<ListItemProps>
      data={items}
      renderItem={renderItem}
      ListEmptyComponent={loading ? null
        : (
          <EmptyCommunityContainer>
            {isMy
              ? (
                <>
                  <EmptyCommunityText>
                    Тут отображаются сообщества на которые вы подписанны
                  </EmptyCommunityText>
                  <CommunitiesFollowButton
                    onPress={onFollowClick}
                  >
                    <CommunitiesFollowButtonText>
                      + Подписаться на группы
                    </CommunitiesFollowButtonText>
                  </CommunitiesFollowButton>
                </>
              )
              : (
                <EmptyCommunityText>
                  Пользователь пока не подписан на сообщества
                </EmptyCommunityText>
              )}
          </EmptyCommunityContainer>)}
      keyExtractor={item => item.id}
      onEndReached={hasMore ? loadMore : null}
      ListFooterComponent={
        <LoadingIndicator visible={loading} />
      }
      refreshing={refreshing}
      onRefresh={refresh}
    />
  )
}
