import { appendEdges, expectDefined } from '@salutdoc/react-components'
import * as React from 'react'
import { Text } from 'react-native'
import { useWorksByCommunityIdQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { FollowCommunityButton } from '../../follow-community-button/follow-community-button.container'
import { FollowCommunityButtonSmall } from '../../follow-community-button/follow-community-button.small.view'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { WorkPreview, WorkProps } from '../work-prewiew/work-prewiew.container'
import { PortfolioView } from './portfolio.view'

export interface CommunityPortfolioProps {
  communityId: string
}

export function CommunityPortfolio ({ communityId }: CommunityPortfolioProps): React.ReactElement {
  const { data, loading, fetchMore, refetch } = useWorksByCommunityIdQuery({
    variables: { communityId }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.communityById?.works?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousWorks = expectDefined(previousResult.communityById?.works)
        return {
          communityById: {
            ...expectDefined(previousResult.communityById),
            works: appendEdges(previousWorks, fetchMoreResult?.communityById?.works)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.communityById?.works?.pageInfo.endCursor])
  if (data?.communityById?.works == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <PortfolioView
      author={{
        __typename: 'community',
        name: data.communityById.name,
        avatar: data.communityById.avatar,
        membersCount: data.communityById.membersCount ?? 0
      }}
      works={data.communityById.works.edges.map(({ node }) => ({ id: node.id }))}
      isMy={false}
      followButton={<FollowCommunityButton communityId={communityId} view={FollowCommunityButtonSmall} />}
      renderWork={({ item }: { item: WorkProps }): React.ReactElement => (
        <WorkPreview {...item} />
      )}
      loading={loading && !refreshing}
      refreshing={refreshing}
      hasMore={data?.communityById?.works?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore()}
      refresh={refresh}
    />
  )
}
