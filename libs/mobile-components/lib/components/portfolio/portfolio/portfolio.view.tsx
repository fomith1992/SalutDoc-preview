import * as React from 'react'
import { FlatList, ListRenderItem } from 'react-native'
import { toCaseCount } from '../../../i18n/utils'
import { ScreenBlock } from '../../../modules/ui-kit'
import { Avatar } from '../../avatar/avatar.view'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { WorkProps } from '../work-prewiew/work-prewiew.container'
import {
  AuthorAvatarContainer,
  AuthorInfoContainer,
  AuthorName,
  EmptyWorksContainer,
  EmptyWorksText,
  FollowButtonContainer,
  FollowersCount,
  PortfolioAuthorContainer,
  PortfolioAuthorTitle,
  Spread
} from './portfolio.style'

export interface AuthorUser {
  __typename: 'user'
  firstName: string
  lastName: string
  avatar: string | null
  followersCount: number
}

export interface AuthorCommunity {
  __typename: 'community'
  name: string
  avatar: string | null
  membersCount: number
}

export interface PortfolioViewProps {
  author: AuthorUser | AuthorCommunity
  works: WorkProps[]
  isMy: boolean
  followButton: React.ReactElement
  renderWork: ListRenderItem<WorkProps>
  loading: boolean
  refreshing: boolean
  hasMore: boolean
  loadMore: () => void
  refresh: () => void
}

export function PortfolioView ({
  author,
  works,
  isMy,
  followButton,
  renderWork,
  loading,
  refreshing,
  hasMore,
  loadMore,
  refresh
}: PortfolioViewProps): React.ReactElement {
  let authorName, followers
  switch (author.__typename) {
    case 'community': {
      authorName = author.name
      followers = author.membersCount
      break
    }
    case 'user': {
      authorName = `${author.firstName} ${author.lastName}`
      followers = author.followersCount
      break
    }
  }
  return (
    <FlatList
      ListHeaderComponent={
        <ScreenBlock>
          <PortfolioAuthorContainer>
            <AuthorInfoContainer>
              <AuthorAvatarContainer>
                <Avatar
                  url={author.avatar}
                />
              </AuthorAvatarContainer>
              <PortfolioAuthorTitle>
                <AuthorName
                  numberOfLines={2}
                >
                  {authorName}
                </AuthorName>
                <FollowersCount>
                  {`${followers} ${toCaseCount(
                    ['подписчик', 'подписчика', 'подписчиков'], followers
                  )}`}
                </FollowersCount>
              </PortfolioAuthorTitle>
            </AuthorInfoContainer>
            <FollowButtonContainer>
              {isMy ? null : followButton}
            </FollowButtonContainer>
          </PortfolioAuthorContainer>
        </ScreenBlock>
      }
      ListEmptyComponent={loading ? null : (
        <EmptyWorksContainer>
          <EmptyWorksText>
            Работы отсутствуют
          </EmptyWorksText>
        </EmptyWorksContainer>
      )}
      data={works}
      keyExtractor={work => work.id}
      renderItem={renderWork}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListFooterComponent={
        <>
          <LoadingIndicator visible={loading} />
          <Spread />
        </>
      }
    />
  )
}
