import { appendEdges, expectDefined } from '@salutdoc/react-components'
import * as React from 'react'
import { Text } from 'react-native'
import { useWorksByUserIdQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { SmallUserFollowButton } from '../../../modules/user-follow'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { useAuthenticatedUserOrNull } from '../../user-context/user-context-provider'
import { WorkPreview, WorkProps } from '../work-prewiew/work-prewiew.container'
import { PortfolioView } from './portfolio.view'

export interface UserPortfolioProps {
  userId: string
}

export function UserPortfolio ({ userId }: UserPortfolioProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const { data, loading, fetchMore, refetch } = useWorksByUserIdQuery({
    variables: { userId }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.userById?.works?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousWorks = expectDefined(previousResult.userById?.works)
        return {
          userById: {
            ...expectDefined(previousResult.userById),
            works: appendEdges(previousWorks, fetchMoreResult?.userById?.works)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.userById?.works?.pageInfo.endCursor])

  if (data?.userById?.works == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <PortfolioView
      author={{
        __typename: 'user',
        firstName: data.userById.firstName,
        lastName: data.userById.lastName,
        avatar: data.userById.avatar,
        followersCount: data.userById.followersCount ?? 0
      }}
      works={data.userById.works.edges.map(({ node }) => ({ id: node.id }))}
      isMy={userId === user?.id}
      followButton={<SmallUserFollowButton followeeId={data.userById.id} />}
      renderWork={({ item }: { item: WorkProps }): React.ReactElement => (
        <WorkPreview {...item} />
      )}
      loading={loading && !refreshing}
      refreshing={refreshing}
      hasMore={data?.userById?.works?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore()}
      refresh={refresh}
    />
  )
}
