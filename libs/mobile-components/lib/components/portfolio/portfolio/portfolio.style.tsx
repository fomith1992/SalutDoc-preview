import { styled } from '../../../style/styled'
import { View, Text, TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { font } from '../../../style/text'
import { ElasticText } from '../../elastic-text/elastic-text'

export const PortfolioAuthorContainer = styled(View, {
  backgroundColor: color('surface'),
  paddingLeft: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const AuthorInfoContainer = styled(View, {
  flex: 1,
  alignItems: 'center',
  paddingVertical: sp(16),
  flexDirection: 'row'
})

export const AuthorAvatarContainer = styled(TouchableOpacity, {
  height: sp(32),
  width: sp(32),
  borderRadius: sp(4),
  overflow: 'hidden'
})

export const PortfolioAuthorTitle = styled(TouchableOpacity, {
  flex: 1,
  overflow: 'hidden',
  marginLeft: sp(8)
})

export const AuthorName = styled(ElasticText, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const FollowersCount = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('accent')
})

export const FollowButtonContainer = styled(View, {
  marginLeft: 'auto'
})

export const Spread = styled(View, {
  paddingBottom: sp(8)
})

export const EmptyWorksContainer = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
  paddingVertical: sp(32)
})

export const EmptyWorksText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('inactive')
})
