import Color from 'color'
import { pipe } from 'fp-ts/lib/function'
import { ImageBackground, Platform, Text, TouchableOpacity, View } from 'react-native'
import { ChatCloudIcon20 } from '../../../images/chat-cloud.icon-20'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { mapStyle, styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { ElasticText } from '../../elastic-text/elastic-text'

export const CardContainer = styled(View, {
  paddingTop: sp(16),
  backgroundColor: color('surface'),
  marginBottom: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const AuthorsContainer = styled(View, {
  ...container('padding'),
  flexDirection: 'row',
  alignItems: 'center'
})

export const MenuButton = styled(TouchableOpacity, {
  padding: sp(8)
})

export const PublicDate = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('inactive')
})

export const WorkTitleText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('surface'),
  marginTop: sp(4)
})

export const WorkButtonsContainer = styled(View, {
  ...container(),
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarsContainer = styled(View, {
  ...container('padding'),
  flexDirection: 'row-reverse'
})

export const AvatarContainer = styled(TouchableOpacity, {
  marginLeft: sp(-8),
  width: sp(32),
  height: sp(32),
  borderRadius: sp(16),
  borderColor: color('surface'),
  borderWidth: 1
})

export const EmptyAvatarContainer = styled(View, {
  marginLeft: sp(-8),
  width: sp(32),
  height: sp(32),
  borderRadius: sp(16),
  borderColor: color('surface'),
  backgroundColor: color('inactive'),
  borderWidth: 1,
  alignItems: 'center',
  justifyContent: 'center'

})

export const AvatarText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('accent')
})

export const AuthorsNameContainer = styled(TouchableOpacity, {
  marginLeft: sp(8)
})

export const AuthorsName = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent')
})

export const WorkButtonCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'h3', weight: 'light' }),
  color: color(props.isPressed ? 'accent' : 'inactive'),
  lineHeight: sp(16),
  textAlign: 'center',
  marginLeft: sp(4)
}) as const)

export const CommentButton = styled(TouchableOpacity, {
  marginLeft: sp(16),
  alignItems: 'center',
  flexDirection: 'row'
})

export const LikeButton = styled(TouchableOpacity, {
  alignItems: 'center',
  flexDirection: 'row'
})

export const WorkDataContainer = styled(View, {
  ...container('padding')
})

export const MoreSvg = styled(VerticalDotsIcon24, {
  color: color('inactive')
})

export const AuthorContainer = styled(TouchableOpacity, {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const PostTitleContainer = styled(View, {
  ...container(),
  flexShrink: 1,
  flexDirection: 'column'
})

export const AuthorName = styled(ElasticText, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const SubjectContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-start'
})

export const AuthorAvatar = styled(View, {
  width: 40,
  height: 40
})

export const PostSubject = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('inactive')
})

export const Dot = styled(View, {
  width: 4,
  height: 4,
  borderRadius: 2,
  backgroundColor: color('inactive'),
  marginHorizontal: sp(8)
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  justifyContent: 'center'
})

export const BackgroundImage = styled(ImageBackground, {
  overflow: 'hidden',
  backgroundColor: color('inactive'),
  borderRadius: sp(8),
  marginTop: sp(12)
})

export const BackgroundImageContent = styled(View, {
  flex: 1,
  backgroundColor: pipe(color('shadow'), mapStyle(c => Color(c).alpha(0.5).string())),
  paddingVertical: sp(48),
  paddingHorizontal: sp(32),
  justifyContent: 'center',
  alignItems: 'center'
})

export const OpenWorkButton = styled(TouchableOpacity, {
  paddingHorizontal: sp(16),
  paddingVertical: sp(8),
  backgroundColor: color('surface'),
  marginTop: sp(16),
  borderRadius: sp(4)
})

export const OpenWorkText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('accent'),
  textAlign: 'center'
})

export const ChatCloudImg = styled(ChatCloudIcon20, {
  color: color('inactive')
})
