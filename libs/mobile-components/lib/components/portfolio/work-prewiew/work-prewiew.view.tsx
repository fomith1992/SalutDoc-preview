import React from 'react'
import { Avatar } from '../../avatar/avatar.view'
import { RenderLikeButtonView } from '../../likes/like.container'
import { LikeButtonView } from '../../likes/like.view'
import {
  AuthorsContainer,
  AvatarContainer,
  AvatarsContainer,
  AvatarText,
  CommentButton,
  EmptyAvatarContainer,
  WorkButtonCount,
  WorkButtonsContainer,
  WorkTitleText,
  PublicDate,
  WorkDataContainer,
  CardContainer,
  AuthorContainer,
  PostTitleContainer,
  AuthorName,
  SubjectContainer,
  PostSubject,
  Dot,
  MoreSvg,
  AuthorAvatar,
  MoreButton,
  BackgroundImage,
  OpenWorkButton,
  OpenWorkText,
  BackgroundImageContent,
  ChatCloudImg
} from './work-prewiew.style'
import { postDateFormatter } from '../../../modules/posts/post-date-formatter/post-date-formatter'
import { hitSlopParams } from '../../../modules/ui-kit'

export interface WorkPreviewAuthors {
  id: string
  avatar: string | null
  authorName: string
  handleAuthorPressed: () => void
}

export interface WorkPreviewProps {
  authors: WorkPreviewAuthors[]
  subject: string
  /* community: CommunityData | null */
  title: string
  commentsCount: number
  imageUrl?: string
  text?: string
  /* views?: number */
  publicationDate: Date
  handleCommentPressed: () => void
  onWorkPressed?: () => void
  wireLikes: (render: RenderLikeButtonView) => React.ReactElement
  onShowContextMenu: () => void
  contextMenu: React.ReactElement
}

interface RenderAuthorsProps {
  authors: WorkPreviewAuthors[]
  subject: string
  createdAt: Date
  onShowContextMenu: () => void
}

function RenderAuthors ({
  authors,
  subject,
  createdAt,
  onShowContextMenu
}: RenderAuthorsProps): React.ReactElement {
  return (
    authors.length === 1
      ? (
        <AuthorContainer onPress={authors[0]?.handleAuthorPressed}>
          <AuthorAvatar>
            <Avatar shape='circle' url={authors[0]?.avatar} />
          </AuthorAvatar>
          <PostTitleContainer>
            <AuthorName
              numberOfLines={2}
            >
              {authors[0]?.authorName ?? null}
            </AuthorName>
            <SubjectContainer>
              <PostSubject>
                {subject}
              </PostSubject>
              <Dot />
              <PublicDate>
                {postDateFormatter(createdAt)}
              </PublicDate>
            </SubjectContainer>
          </PostTitleContainer>
          <MoreButton
            hitSlop={hitSlopParams(16)}
            onPress={onShowContextMenu}
          >
            <MoreSvg />
          </MoreButton>
        </AuthorContainer>
      )
      : (
        <AvatarsContainer>
          {authors.length > 2
            ? (
              <EmptyAvatarContainer>
                <AvatarText>
                  {authors.length - 2}
                </AvatarText>
              </EmptyAvatarContainer>
            )
            : null}
          {authors.slice(0, 2).map((author, index) => {
            return (
              <AvatarContainer key={index} onPress={author.handleAuthorPressed}>
                <Avatar shape='circle' url={author.avatar} />
              </AvatarContainer>
            )
          })}
        </AvatarsContainer>
      )

  )
}

export function WorkPreviewView (
  {
    authors,
    title,
    imageUrl,
    subject,
    /* community,
    views, */
    commentsCount,
    publicationDate,
    onWorkPressed,
    wireLikes,
    handleCommentPressed,
    onShowContextMenu,
    contextMenu
  }: WorkPreviewProps
): React.ReactElement {
  return (
    <CardContainer>
      <AuthorsContainer>
        <RenderAuthors
          authors={authors}
          createdAt={publicationDate}
          onShowContextMenu={onShowContextMenu}
          subject={subject}
        />
        {/* {community != null
        ? (
          <AuthorsNameContainer onPress={community.handleCommunityPressed}>
            <AuthorsName>
              {community.communityName}
            </AuthorsName>
          </AuthorsNameContainer>
        ) : null} */}
      </AuthorsContainer>
      <WorkDataContainer>
        <BackgroundImage
          source={{ uri: imageUrl }}
        >
          <BackgroundImageContent>
            <WorkTitleText
              numberOfLines={3}
            >
              {title}
            </WorkTitleText>
            <OpenWorkButton
              onPress={onWorkPressed}
            >
              <OpenWorkText>
                Перейти к материалу
              </OpenWorkText>
            </OpenWorkButton>
          </BackgroundImageContent>
        </BackgroundImage>
      </WorkDataContainer>
      <WorkButtonsContainer>
        {wireLikes(props => (
          <LikeButtonView
            {...props}
          />
        ))}
        <CommentButton onPress={handleCommentPressed}>
          <ChatCloudImg />
          <WorkButtonCount isPressed={false}>{commentsCount === 0 ? null : commentsCount}</WorkButtonCount>
        </CommentButton>
      </WorkButtonsContainer>
      {contextMenu}
    </CardContainer>
  )
}
