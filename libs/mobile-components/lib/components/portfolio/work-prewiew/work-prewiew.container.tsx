import React, { useEffect } from 'react'
import { useNavigation, StackActions } from '@react-navigation/native'
import { useWorkPrewiewQuery } from '../../../gen/graphql'
import { Text } from 'react-native-svg'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { findFirstMap } from 'fp-ts/Array'
import { some, none, getOrElseW } from 'fp-ts/lib/Option'
import { pipe } from 'fp-ts/lib/function'
import { WorkPreviewProps, WorkPreviewView } from './work-prewiew.view'
import { LikeWork } from '../../likes/like.container'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { WorkContextMenu } from '../../work/work-context-menu/work-context-menu'

export type WorkProps = {
  id: string
  view?: (props: WorkPreviewProps) => React.ReactElement
}

type ImageBlock = {
  __typename: 'ArticleImageBlock'
  url: string
}
type VideoBlock = {
  __typename: 'ArticleYouTubeVideoBlock'
  videoId: string
}
type TextBlock = {
  __typename: 'ArticleTextBlock'
  text: string
}
type HeaderBlock = {
  __typename: 'ArticleHeaderBlock'
  text: string
}

type BlockType = ImageBlock | VideoBlock | HeaderBlock | TextBlock

export function WorkPreview ({
  id
}: WorkProps): React.ReactElement {
  const navigation = useNavigation()

  const { data, loading, error } = useWorkPrewiewQuery({
    variables: {
      workId: id
    }
  })

  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data == null || data.workById == null) {
    return (
      <Text>Произошла ошибка</Text>
    )
  }

  if (!data.workById.archived) {
    return (
      <WorkPreviewView
        wireLikes={renderLikes => <LikeWork id={id} render={renderLikes} />}
        authors={data.workById.authorUsers.map(author => {
          return {
            id: author.id,
            authorName: `${author.firstName} ${author.lastName}`,
            avatar: author.avatar,
            handleAuthorPressed: () => navigation.dispatch(
              StackActions.push('Profile', { userId: author.id }))
          }
        })}
        title={data.workById.title}
        subject={data.workById.subject}
        commentsCount={data.workById.commentsCount ?? 0}
        handleCommentPressed={() => navigation.dispatch(StackActions.push('Work', { workId: data.workById?.id }))}
        /* community={data.workById.community != null
        ? {
          communityName: data.workById.community.name,
          handleCommunityPressed: () => navigation.dispatch(
            StackActions.push('Community', { communityId: data.workById?.community?.id }))
        }
        : null} */
        imageUrl={pipe(
          data.workById.blocks,
          findFirstMap((p: BlockType) => p.__typename === 'ArticleImageBlock' ? some(p.url) : none),
          getOrElseW(() => undefined)
        )}
        text={pipe(
          data.workById.blocks,
          findFirstMap((p: BlockType) => p.__typename === 'ArticleTextBlock' ? some(p.text) : none),
          getOrElseW(() => undefined)
        )}
        publicationDate={new Date(data.workById.createdAt)}
        onWorkPressed={() => navigation.dispatch(StackActions.push('Work', { workId: data.workById?.id }))}
        contextMenu={
          <WorkContextMenu
            visible={bottomMenuVisible}
            workId={id}
            onHideMenu={hideBottomMenu}
          />
        }
        onShowContextMenu={showBottomMenu}
      />
    )
  } else return <></>
}
