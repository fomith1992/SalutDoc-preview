import { DataProxy } from 'apollo-cache'
import {
  WorkDataFragment,
  WorksByUserIdDocument,
  WorksByUserIdQuery,
  WorksByUserIdQueryVariables
} from '../../../gen/graphql'

export function portfolioCacheNewArticle (cache: DataProxy, article: WorkDataFragment): void {
  article.authorUsers.forEach(author => {
    try {
      const userWorks = cache.readQuery<WorksByUserIdQuery, WorksByUserIdQueryVariables>({
        query: WorksByUserIdDocument,
        variables: { userId: author.id }
      })
      if (userWorks?.userById?.works != null) {
        cache.writeQuery<WorksByUserIdQuery, WorksByUserIdQueryVariables>({
          query: WorksByUserIdDocument,
          variables: { userId: author.id },
          data: {
            ...userWorks,
            userById: {
              ...userWorks.userById,
              works: {
                ...userWorks.userById.works,
                edges: [
                  { node: article, __typename: 'WorkEdge' },
                  ...userWorks.userById.works.edges
                ]
              }
            }
          }
        })
      }
    } catch {
      console.warn('Failed to update portfolio cache')
    }
  })
}
