import React, { useState } from 'react'
import YoutubePlayer from 'react-native-youtube-iframe'
import {
  YoutubePlayerContainer
} from './youtube-player.style'
export function YoutubePlayerView (
  props: {
    videoId: string
    width: number
    play: boolean
    onPlayChange: (play: boolean) => void
  }
): React.ReactElement {
  const { videoId, width, play, onPlayChange } = props
  const [containerHeight, setContainerHeight] = useState(0)
  const recommendedHeight = width * (9 / 16) // Recommended 16:9
  const heightVideo = Math.min(recommendedHeight, containerHeight)
  return (
    <YoutubePlayerContainer
      onLayout={event => setContainerHeight(event.nativeEvent.layout.height)}
      width={width}
    >
      <YoutubePlayer
        height={heightVideo}
        width={heightVideo * 16 / 9}
        videoId={videoId}
        play={play}
        onChangeState={event => {
          console.log('event:', event)
          onPlayChange(event === 'playing')
        }}
        onReady={() => console.log('ready')}
        onError={e => console.log(e)}
        onPlaybackQualityChange={q => console.log('quality:', q)}
        initialPlayerParams={{
          cc_lang_pref: 'us',
          preventFullScreen: true // Hides open full screen button, otherwise video may be opens "under" the gallery
        }}
      />
    </YoutubePlayerContainer>
  )
}
