import { View } from 'react-native'
import { styled } from '../../style/styled'
import { color } from '../../style/color'

export const YoutubePlayerContainer = styled(View, (props: { width: number }) => ({
  flex: 1,
  width: props.width,
  backgroundColor: color('text'),
  justifyContent: 'center',
  alignItems: 'center'
}) as const)
