import React, { useMemo } from 'react'
import { LayoutChangeEvent } from 'react-native'
import { LongPressGestureHandler, State } from 'react-native-gesture-handler'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import Animated, { useCode, useValue } from 'react-native-reanimated'
import { color } from '../../../style/color'
import { useMaterialized } from '../../../style/styled'
import { elevationDuration, elevationRange, spring, timing, useClock } from './common'

export interface RowItemProps {
  children: React.ReactNode

  active: Animated.Adaptable<0 | 1>
  translateY: Animated.Adaptable<number>
  onLayout: (event: LayoutChangeEvent) => void
  onPressStateChange: (pressState: Animated.Value<State>) => Animated.Node<number>

  renderRightActions?: () => React.ReactNode
  renderLeftActions?: () => React.ReactNode

  simultaneousHandlers: React.Ref<any>
}

export function RowItem ({
  children,
  active,
  translateY,
  onPressStateChange,
  onLayout,
  renderLeftActions,
  renderRightActions,
  simultaneousHandlers
}: RowItemProps): React.ReactElement {
  const upClock = useClock()
  const downClock = useClock()
  const elevationValue = useValue(elevationRange[0])
  const pressState = useValue<State>(State.UNDETERMINED)

  const elevation = useMemo(() => {
    return Animated.cond(
      active,
      [
        Animated.stopClock(downClock),
        timing(upClock, elevationValue, elevationRange[1], elevationDuration)
      ],
      [
        Animated.stopClock(upClock),
        spring(downClock, elevationValue, elevationRange[0])
      ]
    )
  }, [active, upClock, downClock, elevationValue])

  useCode(() => Animated.onChange(pressState, onPressStateChange(pressState)), [pressState, onPressStateChange])

  const shadowColor = useMaterialized(color('shadow'))

  const style = useMemo(() => ({
    shadowColor: shadowColor,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: Animated.interpolate(elevation, {
      inputRange: elevationRange,
      outputRange: [0, 0.5]
    }),
    shadowRadius: Animated.interpolate(elevation, {
      inputRange: elevationRange,
      outputRange: [0, 5]
    }),

    elevation: Animated.interpolate(elevation, {
      inputRange: elevationRange,
      outputRange: [0, 6]
    }),

    transform: [{
      translateY
    }, {
      scale: Animated.interpolate(elevation, {
        inputRange: elevationRange,
        outputRange: [1, 1.05]
      })
    }],

    zIndex: Animated.cond(elevation, 1, 0)
  }), [elevation, translateY])

  const handleStateChange = useMemo(() => Animated.event([{
    nativeEvent: {
      state: pressState
    }
  }]), [pressState])

  return (
    <Animated.View
      onLayout={onLayout}
      style={style}
    >
      <Swipeable
        useNativeAnimations
        overshootRight={false}
        overshootLeft={false}
        overshootFriction={8}
        renderRightActions={renderRightActions}
        renderLeftActions={renderLeftActions}
      >
        <LongPressGestureHandler
          simultaneousHandlers={simultaneousHandlers}
          shouldCancelWhenOutside={false}
          maxDist={1e9} // some very big value to prevent gesture cancellation on android
          onHandlerStateChange={handleStateChange}
        >
          <Animated.View>
            {children}
          </Animated.View>
        </LongPressGestureHandler>
      </Swipeable>
    </Animated.View>
  )
}
