import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'
import React, { useCallback, useImperativeHandle, useMemo, useRef } from 'react'
import { PanGestureHandler, ScrollView, State } from 'react-native-gesture-handler'
import Animated, { useValue } from 'react-native-reanimated'
import { useToken } from '../../../hooks/use-token'
import { isEqualShallow } from '../../../hooks/utils'
import { spring, useMixedValue } from './common'
import { RowItem } from './row-item'

interface RenderItemProps<T> {
  item: T
  index: number
}

interface DraggableListProps<T> {
  data: readonly T[]
  keyExtractor: (item: T, index: number) => React.Key
  children: (props: RenderItemProps<T>) => React.ReactNode
  onReorder: (newData: readonly T[]) => void

  renderRightActions?: (props: RenderItemProps<T>) => React.ReactNode
  renderLeftActions?: (props: RenderItemProps<T>) => React.ReactNode

  handle?: React.Ref<DraggableListHandle>
}

interface NativeState {
  clock: Animated.Clock
  height: Animated.Value<number>
  physicalY: Animated.Value<number>
  currentY: Animated.Value<number>
}

export interface DraggableListHandle {
  scrollToEnd: () => void
}

export function DraggableList<T> ({
  data,
  children,
  keyExtractor,
  onReorder,
  renderLeftActions,
  renderRightActions,
  handle
}: DraggableListProps<T>): React.ReactElement {
  const scrollViewRef = useRef<ScrollView>(null)
  const panHandlerRef = useRef<PanGestureHandler>(null)

  useImperativeHandle(handle, () => ({
    scrollToEnd: () => scrollViewRef.current?.scrollToEnd()
  }))

  const [{ js: activeIdx, native: activeIdxNative }, setActiveIdx] = useMixedValue<number>(-1)
  const panState = useValue<State>(State.UNDETERMINED)
  const panTranslationY = useValue<number>(0)

  const activeTop = useValue<number>(0)
  const activeHeight = useValue<number>(0)

  const order = useMemo(() => data.map(keyExtractor), [data, keyExtractor])
  const orderToken = useToken(order, isEqualShallow)

  const stateByKey = useMemo(() => new Map<React.Key, NativeState>(), [])
  useMemo(() => {
    const keySet = new Set(order)
    stateByKey.forEach((_, itemKey) => {
      if (!keySet.has(itemKey)) {
        stateByKey.delete(itemKey)
      }
    })
    keySet.forEach(itemKey => {
      if (!stateByKey.has(itemKey)) {
        stateByKey.set(itemKey, {
          clock: new Animated.Clock(),
          height: new Animated.Value<number>(0),
          currentY: new Animated.Value<number>(0), // we keep currentY so that it is consistent between renders
          physicalY: new Animated.Value<number>(0)
        })
      }
    })
  }, [stateByKey, order])

  const state = useMemo(
    () => order.map(key => expectNonNil(stateByKey.get(key))),
    [orderToken, stateByKey]
  )

  const reorder = useCallback(([activeIdx, ...shifts]: readonly number[]): void => {
    if (activeIdx == null) {
      console.error('Active idx not provided')
      return
    }
    if (shifts.some(shift => shift !== 0)) {
      const newData = new Array(data.length)
      for (let i = 0; i < data.length; i++) {
        if (i !== activeIdx) {
          newData[i + (shifts[i] ?? 0)] = data[i]
        }
      }
      for (let i = 0; i < data.length; i++) {
        if (newData[i] == null) {
          newData[i] = data[activeIdx]
        }
      }
      if (newData.filter(x => x === data[activeIdx]).length !== 1) {
        console.error('Failed to reorder, corrupted shifts array: ', shifts, 'activeIdx: ', activeIdx)
      } else {
        console.log('reordering blocks',
          'shifts: ', shifts,
          'activeIdx: ', activeIdx,
          'oldData: ', data,
          'newData: ', newData)
        onReorder(newData)
      }
    }
    setActiveIdx(-1)
  }, [activeIdxNative, onReorder, data])

  const computed = useMemo(() => state.map(({ clock, currentY, height, physicalY }, idx) => {
    const midY = Animated.add(physicalY, Animated.divide(height, 2))
    const indexShift = Animated.cond(
      Animated.eq(activeIdxNative, -1),
      0,
      Animated.cond(
        Animated.lessThan(activeIdxNative, idx),
        Animated.cond(
          Animated.greaterOrEq(Animated.add(activeTop, activeHeight), midY),
          -1
        ),
        Animated.cond(
          Animated.greaterThan(activeIdxNative, idx),
          Animated.cond(
            Animated.lessOrEq(activeTop, midY),
            1
          )
        )
      )
    )

    const snapY = Animated.multiply(indexShift, activeHeight)

    return {
      indexShift,
      active: Animated.eq(activeIdxNative, idx),
      translationY: Animated.block([
        Animated.cond(
          Animated.eq(activeIdxNative, idx),
          [
            Animated.stopClock(clock),
            Animated.set(currentY, Animated.add(physicalY, panTranslationY)),
            Animated.set(activeTop, currentY)
          ],
          spring(clock, currentY, Animated.add(physicalY, snapY))
        ),
        Animated.sub(currentY, physicalY)
      ]),
      handleLayout: Animated.event([{
        nativeEvent: {
          layout: {
            y: physicalY,
            height: height
          }
        }
      }])
    }
  }), [activeTop, activeHeight, activeIdxNative, state])

  const handlePressStateChange = useMemo(() => state.map(({ currentY, height }, idx) => Animated.proc(
    (pressState: Animated.Node<State>) => Animated.block([
      Animated.call([pressState], ([state]) => console.log(state)),
      Animated.cond(
        Animated.eq(pressState, State.ACTIVE),
        Animated.cond(
          Animated.eq(activeIdxNative, -1),
          [
            Animated.set(panTranslationY, 0),
            Animated.set(activeTop, currentY),
            Animated.set(activeHeight, height),
            Animated.set(activeIdxNative, idx),
            Animated.call([], () => setActiveIdx(idx))
          ]
        ),
        Animated.cond(
          Animated.eq(activeIdxNative, idx),
          [
            Animated.call([activeIdxNative, ...computed.map(s => s.indexShift)], reorder)
          ]
        )
      )
    ])
  )), [reorder, panTranslationY, activeIdxNative, computed])

  // do not make event handlers complicated - it will make them extremely buggy
  const handlePanStateChange = useMemo(() => Animated.event([{
    nativeEvent: {
      state: panState
    }
  }]), [panState])

  const handlePanGesture = useMemo(() => Animated.event([{
    nativeEvent: {
      translationY: panTranslationY
    }
  }]), [panTranslationY])

  return (
    <PanGestureHandler
      ref={panHandlerRef}
      onHandlerStateChange={handlePanStateChange}
      onGestureEvent={handlePanGesture}
    >
      <Animated.View
        style={{ flex: 1 }}
      >
        <ScrollView
          ref={scrollViewRef}
          simultaneousHandlers={panHandlerRef}
          scrollEnabled={activeIdx === -1}
          style={{ flex: 1 }}
        >
          {data.map((item, index) => (
            <RowItem
              key={keyExtractor(item, index)}
              simultaneousHandlers={panHandlerRef}
              active={expectNonNil(computed[index]).active}
              onPressStateChange={expectNonNil(handlePressStateChange[index])}
              translateY={expectNonNil(computed[index]).translationY}
              onLayout={expectNonNil(computed[index]).handleLayout}
              renderRightActions={() => renderRightActions?.({ item, index })}
              renderLeftActions={() => renderLeftActions?.({ item, index })}
            >
              {children({ item, index })}
            </RowItem>
          ))}
        </ScrollView>
      </Animated.View>
    </PanGestureHandler>
  )
}
