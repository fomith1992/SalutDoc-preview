import { useMemo, useReducer } from 'react'
import Animated, { Easing } from 'react-native-reanimated'

export function useClock (): Animated.Clock {
  return useMemo(() => new Animated.Clock(), [])
}

export interface MixedValue<T extends number | string | boolean> {
  native: Animated.Value<T>
  js: T
}

export function useMixedValue<T extends number | string | boolean = number> (
  defaultValue: T
): [MixedValue<T>, (value: T) => void] {
  return useReducer(
    (state: MixedValue<T>, js: T) => {
      state.native.setValue(js)
      return {
        native: state.native,
        js
      }
    },
    defaultValue,
    js => ({ native: new Animated.Value<T>(js), js })
  )
}

export function timing (
  clock: Animated.Clock,
  value: Animated.Value<number>,
  toValue: Animated.Adaptable<number>,
  duration: Animated.Adaptable<number>
): Animated.Node<number> {
  const state = {
    finished: new Animated.Value(0),
    frameTime: new Animated.Value(0),
    position: value,
    time: new Animated.Value(0)
  }

  return Animated.block([
    Animated.cond(
      Animated.clockRunning(clock),
      0,
      [
        Animated.set(state.finished, 0),
        Animated.set(state.frameTime, 0),
        Animated.set(state.time, 0),
        Animated.startClock(clock)
      ]
    ),
    Animated.timing(clock, state, {
      toValue,
      duration,
      easing: Easing.in(Easing.back(4))
    }),
    value
  ])
}

export function spring (
  clock: Animated.Clock,
  value: Animated.Value<number>,
  toValue: Animated.Adaptable<number>
): Animated.Node<number> {
  const state = {
    finished: new Animated.Value(0),
    velocity: new Animated.Value(0),
    position: value,
    time: new Animated.Value(0)
  }

  return Animated.block([
    Animated.cond(
      Animated.clockRunning(clock),
      0,
      [
        Animated.set(state.finished, 0),
        Animated.set(state.velocity, 0),
        Animated.set(state.time, 0),
        Animated.startClock(clock)
      ]
    ),
    Animated.spring(clock, state, {
      damping: 10,
      mass: 0.1,
      stiffness: 100,
      overshootClamping: false,
      restSpeedThreshold: 0.001,
      restDisplacementThreshold: 0.001,
      toValue
    }),
    value
  ])
}

export const elevationRange = [0, 1] as const
export const elevationDuration = 400
