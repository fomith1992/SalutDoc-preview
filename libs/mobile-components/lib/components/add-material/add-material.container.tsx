import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { AddMaterialView } from './add-material.view'

export function AddMaterial (): React.ReactElement {
  const navigation = useNavigation()
  return (
    <AddMaterialView
      onAddAlbumPressed={() => navigation.dispatch(StackActions.push('AlbumConstructor', {}))}
      onAddCasePressed={() => navigation.dispatch(StackActions.push('MaterialConstructor', {}))}
      onAddWorkPressed={() => navigation.dispatch(StackActions.push('ArticleConstructor', {}))}
    />
  )
}
