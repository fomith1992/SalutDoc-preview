import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React from 'react'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useRootStore } from '../../../stores/root.store'
import { ArticleConstructorView } from './article-constructor.view'

export const ArticleConstructor = observer((): React.ReactElement => {
  const {
    blocks,
    addBlock,
    setBlocks,
    removeBlock,
    updateBlock
  } = useRootStore().domains.articleData.newArticle
  const navigation = useStackNavigation()
  return (
    <ArticleConstructorView
      blocks={toJS(blocks ?? [])}
      blocksLimitReached={blocks.length === 100}
      onAddBlock={block => addBlock(block)}
      onSetBlocks={blocks => setBlocks(blocks)}
      onRemoveBlock={blockId => removeBlock(blockId)}
      onUpdateBlock={(blockId, block) => updateBlock(blockId, block)}
      onPublishButtonPress={() => navigation.navigate('MaterialPublication', { materialType: 'newArticle' })}
    />
  )
})
