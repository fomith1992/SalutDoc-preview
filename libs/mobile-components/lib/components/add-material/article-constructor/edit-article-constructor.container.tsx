import React from 'react'
import { useRootStore } from '../../../stores/root.store'
import { useNavigation } from '@react-navigation/native'
import { ArticleConstructorView } from './article-constructor.view'
import { observer } from 'mobx-react'
import { toJS } from 'mobx'
import { useEditableWorkQuery } from '../../../gen/graphql'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { Text } from 'react-native'
import { v4 as uuid } from 'uuid'

interface ArticleConstructorContainerProps {
  workId: string
}

export const ArticleEditor = observer(
  ({ workId }: ArticleConstructorContainerProps): React.ReactElement => {
    const navigation = useNavigation()
    const { getAtricle } = useRootStore().domains.articleData
    const article = getAtricle(workId)

    const { data, loading } = useEditableWorkQuery({
      variables: {
        workId
      },
      onCompleted: (response) => {
        if (article?.blocks.length === 0) {
          const work = response.workById
          article.setTitle(work?.title ?? '')
          article.setSubject(work?.subject)
          article.setCommunity(work?.community != null
            ? {
              id: work?.community?.id,
              name: work?.community?.name
            } : null)
          article.setBlocks(
            dataBlocks.map(block => {
              switch (block.__typename) {
                case 'ArticleHeaderBlock':
                  return {
                    blockId: uuid(),
                    block: {
                      type: 'heading',
                      text: block.text
                    }
                  }
                case 'ArticleImageBlock':
                  return {
                    blockId: uuid(),
                    block: {
                      type: 'image',
                      uri: block.url
                    }
                  }
                case 'ArticleTextBlock':
                  return {
                    blockId: uuid(),
                    block: {
                      type: 'text',
                      text: block.text,
                      placeholder: ''
                    }
                  }
                default:
                  throw new Error('Unexpected type')
              }
            })
          )
        }
      }
    })

    const dataBlocks = data?.workById?.blocks ?? []

    if (loading) {
      return <LoadingIndicator visible />
    }

    if (data?.workById == null || article == null) {
      return <Text>Произошла ошибка</Text>
    }

    return (
      <ArticleConstructorView
        blocks={toJS(article.blocks ?? [])}
        blocksLimitReached={article.blocks.length === 100}
        onAddBlock={block => {
          article.addBlock(block)
        }}
        onSetBlocks={blocks => {
          article.setBlocks(blocks)
        }}
        onRemoveBlock={blockId => article.removeBlock(blockId)}
        onUpdateBlock={(blockId, block) => article.updateBlock(blockId, block)}
        onPublishButtonPress={() => navigation.navigate('ArticleEditPublication', { workId })}
      />
    )
  })
