import { View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { container } from '../../../style/view'

export const Container = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})

export const ConstructorContainer = styled(View, {
  flex: 1
})

export const FooterContainer = styled(View, {
  ...container('padding'),
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  borderTopWidth: 0.5,
  borderColor: color('inactive'),
  paddingVertical: sp(12)
})
