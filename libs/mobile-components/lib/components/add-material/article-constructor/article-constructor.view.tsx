import * as React from 'react'
import { ImagePicker, TextButton } from '../../../modules/ui-kit'
import { ArticleBlockInstance, ArticleBlockWithIdInstance } from '../../../stores/domains/new-article.store'
import { ConstructorContainer, Container, FooterContainer } from './article-constructor.style'
import { BlocksMenu } from '../blocks-menu/blocks-menu'
import { BlockView } from '../blocks/block.view'
import { DraggableList, DraggableListHandle } from '../draggable-list'
import { DeleteSwipeAction } from '../swipe-actions/delete-swipe-action'

export interface ArticleConstructorViewProps {
  blocks: ArticleBlockWithIdInstance[]
  blocksLimitReached: boolean
  onAddBlock: (block: ArticleBlockInstance) => void
  onSetBlocks: (newBlocks: readonly ArticleBlockWithIdInstance[]) => void
  onRemoveBlock: (id: string) => void
  onUpdateBlock: (id: string, newBlock: ArticleBlockInstance) => void
  onPublishButtonPress: () => void
}

export const ArticleConstructorView = ({
  blocks,
  blocksLimitReached,
  onAddBlock,
  onSetBlocks,
  onRemoveBlock,
  onUpdateBlock,
  onPublishButtonPress
}: ArticleConstructorViewProps): React.ReactElement => {
  const draggableListHandleRef = React.useRef<DraggableListHandle>(null)
  const handleAddBlock = (block: ArticleBlockInstance): void => {
    onAddBlock(block)
    requestAnimationFrame(() => draggableListHandleRef.current?.scrollToEnd())
  }
  return (
    <Container>
      <ConstructorContainer>
        <DraggableList
          handle={draggableListHandleRef}
          data={blocks}
          keyExtractor={item => item.blockId}
          onReorder={onSetBlocks}
          renderRightActions={({ item }) => (
            <DeleteSwipeAction
              onPress={() => onRemoveBlock(item.blockId)}
            />
          )}
        >
          {({ item }) => {
            return (
              <BlockView
                block={item.block}
                onChange={newItem => onUpdateBlock(item.blockId, newItem)}
              />
            )
          }}
        </DraggableList>
      </ConstructorContainer>
      <FooterContainer>
        <ImagePicker
          title='Choose image'
          onImagePicked={({ uri }) => {
            handleAddBlock({
              type: 'image',
              uri
            })
          }}
        >
          {({ pickImage }) => (
            <BlocksMenu
              onCreateHeading={() => handleAddBlock({
                type: 'heading',
                text: ''
              })}
              onCreateText={() => handleAddBlock({
                type: 'text',
                text: '',
                placeholder: ''
              })}
              onCreatePhoto={() => {
                requestAnimationFrame(() => {
                  pickImage()
                })
              }}
              blocksLimitReached={blocksLimitReached}
            />
          )}
        </ImagePicker>
        <TextButton
          size='L'
          type='primary'
          onPress={onPublishButtonPress}
        >
          Опубликовать
        </TextButton>
      </FooterContainer>
    </Container>
  )
}
