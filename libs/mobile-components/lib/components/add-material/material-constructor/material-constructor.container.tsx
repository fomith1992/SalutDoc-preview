import React from 'react'
import { useRootStore } from '../../../stores/root.store'
import { useNavigation } from '@react-navigation/native'
import { MaterialConstructorView } from './material-constructor.view'
import { observer } from 'mobx-react'
import { toJS } from 'mobx'

export const MaterialConstructor = observer((): React.ReactElement => {
  const {
    blocks,
    addBlock,
    setBlocks,
    removeBlock,
    updateBlock
  } = useRootStore().domains.articleData.newMaterial
  const navigation = useNavigation()
  return (
    <MaterialConstructorView
      blocks={toJS(blocks ?? [])}
      blocksLimitReached={blocks.length === 100}
      onAddBlock={block => addBlock(block)}
      onSetBlocks={blocks => setBlocks(blocks)}
      onRemoveBlock={blockId => removeBlock(blockId)}
      onUpdateBlock={(blockId, block) => updateBlock(blockId, block)}
      onPublishButtonPress={() => navigation.navigate('MaterialPublication', { materialType: 'newMaterial' })}
    />
  )
})
