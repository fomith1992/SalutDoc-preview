import { StyleSheet, TextInput, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const BlockContainer = styled(View, {
  backgroundColor: color('surface'),
  marginVertical: sp(4)
})

export const HeadingBlockInput = styled(TextInput, {
  ...container('padding'),
  ...font({ type: 'h2' }),
  color: color('text'),
  paddingVertical: sp(4),
  borderBottomWidth: 0.5,
  borderColor: color('inactive')
})

export const TextBlockInput = styled(TextInput, {
  ...container('padding'),
  ...font({ type: 'h2' }),
  color: color('text'),
  paddingVertical: sp(4),
  borderBottomWidth: 0.5,
  borderColor: color('inactive')
})

export const TextOverlay = styled(View, StyleSheet.absoluteFill)
