import React, { useCallback, useRef, useState } from 'react'
import { TextInput, TouchableWithoutFeedback } from 'react-native'
import { BlockContainer, HeadingBlockInput, TextOverlay } from './blocks.style'

export interface HeadingBlock {
  type: 'heading'
  text: string
}

export interface HeadingBlockViewProps {
  block: HeadingBlock
  onChange: (block: HeadingBlock) => void
}

export function HeadingBlockView ({
  block: { text },
  onChange
}: HeadingBlockViewProps): React.ReactElement {
  const ref = useRef<TextInput>(null)
  const [focused, setFocused] = useState(false)

  const handleFocus = useCallback((): void => {
    if (ref.current != null) {
      ref.current.focus()
      setFocused(true)
    }
  }, [ref, setFocused])
  const handleBlur = useCallback(() => setFocused(false), [setFocused])

  return (
    <BlockContainer>
      <HeadingBlockInput
        ref={ref}
        multiline
        placeholder='Введите заголовок...'
        onBlur={handleBlur}
        onFocus={handleFocus}
        value={text}
        onChangeText={text => onChange({ type: 'heading', text })}
      />
      {focused ? null : (
        <TouchableWithoutFeedback onPress={handleFocus}>
          <TextOverlay />
        </TouchableWithoutFeedback>
      )}
    </BlockContainer>
  )
}
