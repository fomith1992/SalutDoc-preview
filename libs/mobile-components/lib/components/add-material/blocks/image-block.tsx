import React from 'react'
import { TouchableOpacity } from 'react-native'
import { ImagePicker } from '../../../modules/ui-kit'
import { MainAttachmentView } from '../../attachments/main-attachment.view'
import { BlockContainer } from './blocks.style'

export interface ImageBlock {
  type: 'image'
  uri: string
}

export interface ImageBlockViewProps {
  block: ImageBlock
  onChange: (block: ImageBlock) => void
}

export function ImageBlockView (
  {
    block: { uri },
    onChange
  }: ImageBlockViewProps
): React.ReactElement {
  return (
    <ImagePicker
      title='Загрузить фото'
      onImagePicked={({ uri }) => onChange({
        type: 'image',
        uri
      })}
    >
      {({ pickImage }) => (
        <BlockContainer>
          <TouchableOpacity
            onPress={pickImage}
          >
            <MainAttachmentView
              uri={uri}
            />
          </TouchableOpacity>
        </BlockContainer>
      )}
    </ImagePicker>
  )
}
