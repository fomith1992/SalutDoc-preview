import React from 'react'
import { HeadingBlock, HeadingBlockView } from './heading-block'
import { ImageBlock, ImageBlockView } from './image-block'
import { TextBlock, TextBlockView } from './text-block'

export type ArticleBlock = TextBlock | HeadingBlock | ImageBlock

export interface BlockViewProps {
  block: ArticleBlock
  onChange: (newBlock: ArticleBlock) => void
}

export function BlockView ({ block, onChange }: BlockViewProps): React.ReactElement {
  switch (block.type) {
    case 'heading':
      return (
        <HeadingBlockView
          block={block}
          onChange={onChange}
        />
      )
    case 'text':
      return (
        <TextBlockView
          block={block}
          onChange={onChange}
        />
      )
    case 'image': {
      return (
        <ImageBlockView
          block={block}
          onChange={onChange}
        />
      )
    }
  }
}
