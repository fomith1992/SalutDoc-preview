import React, { useCallback, useRef, useState } from 'react'
import { TextInput, TouchableWithoutFeedback } from 'react-native'
import { BlockContainer, TextBlockInput, TextOverlay } from './blocks.style'

export interface TextBlock {
  type: 'text'
  text: string
  placeholder: string | undefined
}

export interface TextBlockViewProps {
  block: TextBlock
  onChange: (block: TextBlock) => void
}

export function TextBlockView ({
  block: { text, placeholder },
  onChange
}: TextBlockViewProps): React.ReactElement {
  const ref = useRef<TextInput>(null)
  const [focused, setFocused] = useState(false)

  const handleFocus = useCallback((): void => {
    if (ref.current != null) {
      ref.current.focus()
      setFocused(true)
    }
  }, [ref, setFocused])
  const handleBlur = useCallback(() => setFocused(false), [setFocused])

  return (
    <BlockContainer>
      <TextBlockInput
        ref={ref}
        multiline
        placeholder={placeholder != null && placeholder !== '' ? placeholder : 'Введите текст...'}
        onBlur={handleBlur}
        onFocus={handleFocus}
        value={text}
        onChangeText={text => onChange({ type: 'text', text, placeholder: placeholder ?? '' })}
      />
      {focused ? null : (
        <TouchableWithoutFeedback onPress={handleFocus}>
          <TextOverlay />
        </TouchableWithoutFeedback>
      )}
    </BlockContainer>
  )
}
