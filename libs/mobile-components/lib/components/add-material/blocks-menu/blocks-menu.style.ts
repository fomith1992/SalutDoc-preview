import { TouchableOpacity } from 'react-native'
import { HeadingIcon } from '../../../images/heading.icon'
import { PhotoIcon } from '../../../images/photo.icon'
import { PlusIcon16 } from '../../../images/plus.icon-16'
import { TextIcon } from '../../../images/text.icon'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const MenuConstructorButton = styled(TouchableOpacity, (props: { expand: boolean }) => ({
  backgroundColor: color(props.expand ? 'subtext' : 'accent'),
  padding: sp(8),
  borderRadius: sp(16),
  transform: [{ rotate: props.expand ? '45deg' : '0deg' }]
}))

const iconStyle = {
  color: color('subtext'),
  width: sp(16),
  height: sp(16)
}

export const HeadingImg = styled(HeadingIcon, iconStyle)

export const TextImg = styled(TextIcon, iconStyle)

export const PhotoImg = styled(PhotoIcon, iconStyle)

export const NewBlockIcon = styled(PlusIcon16, {
  width: sp(16),
  height: sp(16),
  color: color('surface')
})
