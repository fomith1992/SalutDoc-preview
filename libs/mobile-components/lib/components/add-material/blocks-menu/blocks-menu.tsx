import React, { useState } from 'react'
import { Alert } from 'react-native'
import { Modal } from '../../../modules/ui-kit'
import { DropDown } from '../../drop-down/drop-down.view'
import { HeadingImg, MenuConstructorButton, NewBlockIcon, PhotoImg, TextImg } from './blocks-menu.style'

export interface BlocksMenuProps {
  onCreateHeading?: () => void
  onCreateText?: () => void
  onCreatePhoto?: () => void
  blocksLimitReached: boolean
}

export function BlocksMenu ({
  onCreateHeading,
  onCreatePhoto,
  onCreateText,
  blocksLimitReached
}: BlocksMenuProps): React.ReactElement {
  const [expand, setExpand] = useState<boolean>(false)

  const andClose = (callback?: () => void) => () => {
    setExpand(false)
    callback?.()
  }

  return (
    <MenuConstructorButton
      onPress={() => {
        if (blocksLimitReached) {
          Alert.alert(
            'Ошибка',
            'Количество блоков в статье не должно превышать 100 штук'
          )
        } else {
          setExpand(!expand)
        }
      }}
      expand={expand}
    >
      <NewBlockIcon />
      <Modal visible={expand}>
        <DropDown
          data={[
            {
              icon: <HeadingImg />,
              title: 'Заголовок',
              onPress: andClose(onCreateHeading)
            },
            {
              icon: <TextImg />,
              title: 'Текст',
              onPress: andClose(onCreateText)
            },
            {
              icon: <PhotoImg />,
              title: 'Медиа',
              onPress: andClose(onCreatePhoto)
            }
          ]}
          onClose={() => setExpand(false)}
          bottom
        />
      </Modal>
    </MenuConstructorButton>
  )
}
