import { ReactNativeFile } from 'apollo-upload-client'
import { WorkArticleBlockInput } from '../../../gen/graphql'
import { ArticleBlockWithIdInstance } from '../../../stores/domains/new-article.store'

export const parseArticleBlocks = (blocks: ArticleBlockWithIdInstance[]): WorkArticleBlockInput[] => {
  return blocks.map(b => {
    switch (b.block.type) {
      case 'heading':
        return {
          header: b.block.text,
          text: null,
          image: null,
          youtube: null
        }
      case 'image': {
        const image = new ReactNativeFile({
          uri: b.block.uri,
          name: b.blockId,
          type: 'image/jpeg'
        })
        return {
          header: null,
          text: null,
          image,
          youtube: null
        }
      }
      case 'text':
        return {
          header: null,
          text: b.block.text,
          image: null,
          youtube: null
        }
    }
  })
}
