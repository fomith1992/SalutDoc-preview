import { TouchableOpacity, View, Text, ScrollView, ImageBackground } from 'react-native'
import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { container } from '../../../style/view'
import { sp } from '../../../style/size'
import { font } from '../../../style/text'

export const Container = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})

export const HeaderContainer = styled(View, {
  ...container('padding'),
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: sp(12)
})

export const ArticlePublicationContainer = styled(View, {
  paddingVertical: 8,
  paddingHorizontal: 12
})

export const BlockTitle = styled(Text, {
  ...font({ type: 'h3' }),
  marginBottom: 18
})

export const InputContainer = styled(View, {

  marginBottom: 18
})

export const ListContainer = styled(View, {
  backgroundColor: color('surface'),
  marginVertical: 'auto'
})

export const ListItemContainer = styled(TouchableOpacity, {
  paddingVertical: sp(12),
  backgroundColor: color('surface'),
  marginBottom: sp(4)
})

export const ListItem = styled(Text, {
  marginLeft: sp(8),
  ...font({ type: 'h3' }),
  color: color('text')
})

export const ScrollViewContainer = styled(ScrollView, {
  flex: 1
})

export const Image = styled(ImageBackground, {
  position: 'relative',
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  marginVertical: sp(24),
  paddingVertical: sp(64),
  borderRadius: sp(8),
  overflow: 'hidden'
})

export const AddImageContainer = styled(TouchableOpacity, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  marginVertical: sp(24),
  paddingVertical: sp(64),
  borderRadius: sp(8),
  borderWidth: 1,
  borderColor: color('inactive'),
  borderStyle: 'dashed'
})

export const ChangeImageButton = styled(TouchableOpacity, {
  position: 'absolute',
  width: sp(32),
  height: sp(32),
  justifyContent: 'center',
  alignItems: 'center',
  bottom: sp(12),
  right: sp(12)
})

export const ChangeImageContainer = styled(TouchableOpacity, {
  position: 'absolute',
  width: sp(32),
  height: sp(32),
  borderRadius: sp(16),
  backgroundColor: color('text'),
  opacity: 0.1
})

export const Description = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  marginTop: sp(8),
  textAlign: 'center'
})
