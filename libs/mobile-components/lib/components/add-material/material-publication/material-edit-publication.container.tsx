import { useNavigation } from '@react-navigation/native'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import * as React from 'react'
import { Alert, Text } from 'react-native'
import { useWorkEditArticleMutation } from '../../../gen/graphql'
import { useRootStore } from '../../../stores/root.store'
import { useAuthenticatedUser } from '../../user-context/user-context-provider'
import { MaterialPublicationView } from './material-publication.view'
import { parseArticleBlocks } from './parse-article-block'
import { pipe } from 'fp-ts/lib/function'
import { findFirstMap } from 'fp-ts/Array'
import { some, none, getOrElseW } from 'fp-ts/lib/Option'
import { ArticleBlockWithIdInstance } from '../../../stores/domains/new-article.store'

interface MaterialEditPublicationProps {
  workId: string
}

export const MaterialEditPublication = observer(
  ({ workId }: MaterialEditPublicationProps): React.ReactElement => {
    const navigation = useNavigation()
    const { id: userId } = useAuthenticatedUser()
    const { getAtricle } = useRootStore().domains.articleData
    const article = getAtricle(workId)

    const [workEditArticle, { loading: submittingArticle }] = useWorkEditArticleMutation({
      onCompleted (response) {
        const { work, message } = response.workEditArticle
        console.log('Материал отредактирован', message, work)
        if (article != null) {
          article.clearArticle()
        }
        navigation.navigate('UserPortfolio', { userId })
      },
      onError (response) {
        console.log('Не удалось отредактировать материал', response)
        Alert.alert('Ошибка', 'Не удалось отредактировать материал')
      }
    })
    if (article == null) {
      return <Text>Произошла ошибка</Text>
    }
    return (
      <MaterialPublicationView
        onSubmit={async () => {
          if (article.title !== '' && article.subject != null) {
            try {
              await workEditArticle({
                variables: {
                  workId,
                  content: {
                    blocks: parseArticleBlocks(article.blocks),
                    communityId: article.community?.id ?? null,
                    subject: article.subject,
                    title: article.title ?? ''
                  }
                }
              })
            } catch (e) {
              console.log(e)
              Alert.alert('Ошибка', 'Не удалось опубликовать работу')
            }
          } else {
            Alert.alert('Ошибка', 'Поля "Заголовок статьи" и "Категория работы" обязательны')
          }
          return {}
        }}
        skin={pipe(
          article.blocks,
          findFirstMap((p: ArticleBlockWithIdInstance) => p.block.type === 'image' ? some(p.block) : none),
          getOrElseW(() => undefined)
        )}
        title={toJS(article.title ?? '')}
        onSetTitle={title => article.setTitle(title)}
        subject={toJS(article.subject)}
        community={toJS(article.community)}
        onCommunityReset={() => article.setCommunity(null)}
        onSubjectChange={subject => article.setSubject(subject)}
        onCommunityButtonPress={() => navigation.navigate('ArticleEditPublicationCommunity', { workId })}
        submitting={submittingArticle}
        addBlockToBeginning={article.addBlockToBeginning}
        onUpdateBlock={(blockId, block) => article.updateBlock(blockId, block)}
      />
    )
  })
