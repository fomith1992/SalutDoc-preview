import { useNavigation } from '@react-navigation/native'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import * as React from 'react'
import { Alert } from 'react-native'
import { useWorkCreateArticleMutation } from '../../../gen/graphql'
import { feedCacheNewArticle } from '../../../modules/feed/cache'
import { portfolioCacheNewArticle } from '../../portfolio'
import { useAuthenticatedUser } from '../../user-context/user-context-provider'
import { MaterialPublicationView } from './material-publication.view'
import { parseArticleBlocks } from './parse-article-block'
import { pipe } from 'fp-ts/lib/function'
import { findFirstMap } from 'fp-ts/Array'
import { some, none, getOrElseW } from 'fp-ts/lib/Option'
import { ArticleBlockWithIdInstance } from '../../../stores/domains/new-article.store'
import { MaterialPublicationProps, storeData } from './store-data-parser'

export const MaterialPublication = observer(
  ({ materialType = 'newArticle' }: MaterialPublicationProps): React.ReactElement => {
    const { id: userId } = useAuthenticatedUser()
    const navigation = useNavigation()

    const {
      title,
      setTitle,
      community,
      setCommunity,
      subject,
      setSubject,
      blocks,
      clearArticle,
      addBlockToBeginning,
      updateBlock
    } = storeData({ materialType })

    const [workCreateArticle, { loading: submittingArticle }] = useWorkCreateArticleMutation({
      onCompleted (response) {
        const { work, message, errors } = response.workCreateArticle
        if (work == null) {
          console.log('Не удалось опубликовать материал', message, errors)
          Alert.alert('Ошибка', 'Не удалось опубликовать материал')
        } else {
          console.log('Материал опубликован', message, work)
          navigation.navigate('UserPortfolio', { userId })
          clearArticle()
        }
      },
      update (cache, { data }) {
        const work = data?.workCreateArticle.work
        if (work == null) {
          return
        }
        portfolioCacheNewArticle(cache, work)
        feedCacheNewArticle(cache, work)
      }
    })

    const validBlocks = blocks.every(block => {
      switch (block.block.type) {
        case 'heading':
          return block.block.text !== ''
        case 'image':
          return block.block.uri != null
        case 'text':
          return block.block.text !== ''
      }
    })
    return (
      <MaterialPublicationView
        skin={pipe(
          blocks,
          findFirstMap((p: ArticleBlockWithIdInstance) => p.block.type === 'image' ? some(p.block) : none),
          getOrElseW(() => undefined)
        )}
        onSubmit={async () => {
          if (title != null && title !== '' && subject != null && validBlocks) {
            try {
              await workCreateArticle({
                variables: {
                  content: {
                    blocks: parseArticleBlocks(blocks),
                    communityId: community?.id ?? null,
                    subject,
                    title
                  }
                }
              })
            } catch (e) {
              console.log(e)
              Alert.alert('Ошибка', 'Не удалось опубликовать работу')
            }
          } else {
            Alert.alert('Ошибка', 'Остались незаполненные поля')
          }
          return {}
        }}
        title={toJS(title ?? '')}
        onSetTitle={setTitle}
        subject={toJS(subject)}
        community={toJS(community)}
        onCommunityReset={() => setCommunity(null)}
        onSubjectChange={setSubject}
        onCommunityButtonPress={() => navigation.navigate('ArticlePublicationCommunity', { materialType })}
        submitting={submittingArticle}
        addBlockToBeginning={addBlockToBeginning}
        onUpdateBlock={(blockId, block) => updateBlock(blockId, block)}
      />
    )
  })
