import React from 'react'
import { HeaderSaveButton, TextInput } from '../../../modules/form-kit'
import { ArticleBlockInstance, CommunityInstance } from '../../../stores/domains/new-article.store'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'
import { SelectSearch } from '../../../modules/form-kit/select-search/select-search.view'
import { specializationsData, SelectButton, ImagePicker } from '../../../modules/ui-kit'
import {
  Container,
  ArticlePublicationContainer,
  BlockTitle,
  InputContainer,
  AddImageContainer,
  Description,
  Image,
  ChangeImageContainer,
  ChangeImageButton
} from './material-publication.style'
import { AddImageIcon } from '../../../images/add-image.icon'
import { PencilIcon } from '../../../images/pencil.icon'

export interface MaterialPublicationViewProps {
  onSubmit: () => void
  title: string
  onSetTitle: (value: string) => void
  subject: string | undefined
  community: CommunityInstance | null
  onCommunityReset: () => void
  onSubjectChange: (value: string | undefined) => void
  onCommunityButtonPress: () => void
  addBlockToBeginning: (block: ArticleBlockInstance) => void
  onUpdateBlock: (id: string, newBlock: ArticleBlockInstance) => void
  submitting: boolean
  skin?: { type: 'image', uri: string }
}

export const MaterialPublicationView = ({
  onSubmit,
  title,
  onSetTitle,
  subject,
  community,
  onCommunityReset,
  onCommunityButtonPress,
  onSubjectChange,
  addBlockToBeginning,
  submitting,
  skin
}: MaterialPublicationViewProps): React.ReactElement => {
  console.log(skin)
  return (
    <Container>
      <ArticlePublicationContainer>
        <HeaderSaveButton
          text='Опубликовать'
          onSubmit={onSubmit}
          valid
        />
        <ImagePicker
          title='Choose image'
          onImagePicked={({ uri }) => {
            addBlockToBeginning({
              type: 'image',
              uri
            })
          }}
        >
          {({ pickImage }) => (
            skin == null
              ? (
                <AddImageContainer onPress={pickImage}>
                  <AddImageIcon />
                  <Description
                    numberOfLines={2}
                  >
                    Загрузите изображение для обложки
                  </Description>
                </AddImageContainer>
              ) : (

                <Image
                  source={{ uri: skin.uri }}
                >
                  <ChangeImageButton onPress={pickImage}>
                    <ChangeImageContainer />
                    <PencilIcon />
                  </ChangeImageButton>
                </Image>)
          )}
        </ImagePicker>
        <BlockTitle>
          Название
        </BlockTitle>
        <InputContainer>
          <TextInput
            onChange={onSetTitle}
            value={title}
          />
        </InputContainer>
        <BlockTitle>
          Категория материала
        </BlockTitle>
        <InputContainer>
          <SelectSearch
            value={subject}
            onChange={onSubjectChange}
            options={specializationsData}
            type='button'
          />
        </InputContainer>
        <BlockTitle>
          Материал от сообщества
        </BlockTitle>
        <InputContainer>
          <SelectButton
            defaultText='Указать сообщество'
            value={community?.name}
            resetValue={onCommunityReset}
            onPress={onCommunityButtonPress}
          />
        </InputContainer>
      </ArticlePublicationContainer>
      <ModalLoadingIndicator visible={submitting} />
    </Container>
  )
}
