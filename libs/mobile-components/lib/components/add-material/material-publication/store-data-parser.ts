import { useRootStore } from '../../../stores/root.store'
import { ArticleInstance } from '../../../stores/domains/new-article.store'

export interface MaterialPublicationProps {
  materialType: 'newArticle' | 'newMaterial' | 'newAlbum'
}

export const storeData = ({ materialType }: MaterialPublicationProps): ArticleInstance => {
  switch (materialType) {
    case 'newAlbum':
      return useRootStore().domains.articleData.newAlbum
    case 'newArticle':
      return useRootStore().domains.articleData.newArticle
    case 'newMaterial':
      return useRootStore().domains.articleData.newMaterial
  }
}
