import React from 'react'
import { ImagePicker, TextButton } from '../../../modules/ui-kit'
import { ArticleBlockInstance, ArticleBlockWithIdInstance } from '../../../stores/domains/new-article.store'
import { ConstructorContainer, Container, FooterContainer } from './album-constructor.style'
import { BlocksMenu } from '../blocks-menu/blocks-menu'
import { BlockView } from '../blocks/block.view'
import { DraggableList, DraggableListHandle } from '../draggable-list'
import { DeleteSwipeAction } from '../swipe-actions/delete-swipe-action'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'

export interface AlbumConstructorViewProps {
  blocks: ArticleBlockWithIdInstance[]
  blocksLimitReached: boolean
  onAddBlock: (block: ArticleBlockInstance) => void
  onSetBlocks: (newBlocks: readonly ArticleBlockWithIdInstance[]) => void
  onRemoveBlock: (id: string) => void
  onUpdateBlock: (id: string, newBlock: ArticleBlockInstance) => void
  onPublishButtonPress: () => void
}

export const AlbumConstructorView = ({
  blocks,
  blocksLimitReached,
  onAddBlock,
  onSetBlocks,
  onRemoveBlock,
  onUpdateBlock,
  onPublishButtonPress
}: AlbumConstructorViewProps): React.ReactElement => {
  const draggableListHandleRef = React.useRef<DraggableListHandle>(null)
  const { state: modalVisible, setFalse: notShowModal } = useBooleanFlag(true)

  const handleAddBlock = (block: ArticleBlockInstance): void => {
    onAddBlock(block)
    requestAnimationFrame(() => draggableListHandleRef.current?.scrollToEnd())
  }

  return (
    <Container>
      <ConstructorContainer>
        <DraggableList
          handle={draggableListHandleRef}
          data={blocks}
          keyExtractor={item => item.blockId}
          onReorder={onSetBlocks}
          renderRightActions={({ item }) => (
            <DeleteSwipeAction
              onPress={() => onRemoveBlock(item.blockId)}
            />
          )}
        >
          {({ item }) => {
            return (
              <BlockView
                block={item.block}
                onChange={newItem => onUpdateBlock(item.blockId, newItem)}
              />
            )
          }}
        </DraggableList>
      </ConstructorContainer>
      <FooterContainer>
        <ImagePicker
          title='Choose image'
          onImagePicked={({ uri }) => {
            handleAddBlock({
              type: 'image',
              uri
            })
          }}
        >
          {({ pickImage }) => {
            if (blocks.length === 0 && modalVisible) {
              pickImage()
              notShowModal()
            }
            return (
              <BlocksMenu
                onCreateHeading={() => handleAddBlock({
                  type: 'heading',
                  text: ''
                })}
                onCreateText={() => handleAddBlock({
                  type: 'text',
                  text: '',
                  placeholder: ''
                })}
                onCreatePhoto={() => {
                  requestAnimationFrame(() => {
                    pickImage()
                  })
                }}
                blocksLimitReached={blocksLimitReached}
              />
            )
          }}
        </ImagePicker>
        <TextButton
          size='L'
          type='primary'
          onPress={onPublishButtonPress}
        >
          Опубликовать
        </TextButton>
      </FooterContainer>
    </Container>
  )
}
