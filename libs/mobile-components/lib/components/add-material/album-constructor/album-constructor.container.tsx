import React from 'react'
import { useRootStore } from '../../../stores/root.store'
import { useNavigation } from '@react-navigation/native'
import { AlbumConstructorView } from './album-constructor.view'
import { observer } from 'mobx-react'
import { toJS } from 'mobx'

export const AlbumConstructor = observer((): React.ReactElement => {
  const {
    blocks,
    addBlock,
    setBlocks,
    removeBlock,
    updateBlock
  } = useRootStore().domains.articleData.newAlbum
  const navigation = useNavigation()
  return (
    <AlbumConstructorView
      blocks={toJS(blocks ?? [])}
      blocksLimitReached={blocks.length === 100}
      onAddBlock={block => addBlock(block)}
      onSetBlocks={blocks => setBlocks(blocks)}
      onRemoveBlock={blockId => removeBlock(blockId)}
      onUpdateBlock={(blockId, block) => updateBlock(blockId, block)}
      onPublishButtonPress={() => navigation.navigate('MaterialPublication', { materialType: 'newAlbum' })}
    />
  )
})
