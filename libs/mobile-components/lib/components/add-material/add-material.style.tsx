import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { AlbumIcon } from '../../images/album.icon'
import { CaseIcon } from '../../images/case.icon'
import { ListIcon } from '../../images/list.icon'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const AddMaterialContainer = styled(ScrollView, {
  flex: 1,
  paddingVertical: sp(8),
  backgroundColor: color('surface')
})

export const AddMaterialRow = styled(TouchableOpacity, {
  ...container('padding'),
  paddingVertical: sp(8),
  flexDirection: 'row'
})

export const DescriptionContainer = styled(View, {
  flexDirection: 'column',
  marginLeft: sp(12)
})

export const Title = styled(Text, {
  ...font({ type: 'h3' }),
  color: color('text')
})

export const Description = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('inactive')
})

const ImageStyle = {
  color: color('accent'),
  width: sp(32),
  height: sp(32)
}

export const AlbumImg = styled(AlbumIcon, ImageStyle)

export const CaseImg = styled(CaseIcon, ImageStyle)

export const ListImg = styled(ListIcon, ImageStyle)
