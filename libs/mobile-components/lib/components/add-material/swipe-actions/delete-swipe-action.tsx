import React from 'react'
import { TouchableOpacity } from 'react-native'
import { DeleteIcon, DeleteSwipeActionText, DeleteSwipeButton } from './swipe-actions.style'

export interface DeleteSwipeActionProps {
  onPress?: () => void
}

export function DeleteSwipeAction ({
  onPress
}: DeleteSwipeActionProps): React.ReactElement {
  return (
    <TouchableOpacity onPress={onPress}>
      <DeleteSwipeButton>
        <DeleteIcon />
        <DeleteSwipeActionText>Удалить</DeleteSwipeActionText>
      </DeleteSwipeButton>
    </TouchableOpacity>
  )
}
