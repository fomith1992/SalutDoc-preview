import { Text, View } from 'react-native'
import { TrashBinIcon } from '../../../images/trash-bin.icon'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const DeleteSwipeButton = styled(View, {
  flex: 1,
  backgroundColor: color('error'),
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: sp(20),
  marginVertical: sp(4)
})

export const DeleteIcon = styled(TrashBinIcon, {
  color: color('surface'),
  height: sp(20),
  width: sp(20)
})

export const DeleteSwipeActionText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('surface'),
  marginTop: sp(8)
})
