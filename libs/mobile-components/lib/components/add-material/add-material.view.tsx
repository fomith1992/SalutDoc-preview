import React from 'react'
import {
  AddMaterialContainer,
  AddMaterialRow,
  DescriptionContainer,
  Title,
  Description,
  AlbumImg,
  CaseImg,
  ListImg
} from './add-material.style'

interface AddMaterialViewProps {
  onAddAlbumPressed: () => void
  onAddCasePressed: () => void
  onAddWorkPressed: () => void
}

export function AddMaterialView ({
  onAddAlbumPressed,
  onAddCasePressed,
  onAddWorkPressed
}: AddMaterialViewProps): React.ReactElement {
  return (
    <AddMaterialContainer>
      <AddMaterialRow
        onPress={onAddAlbumPressed}
      >
        <AlbumImg />
        <DescriptionContainer>
          <Title>
            Альбом
          </Title>
          <Description>
            Дипломы, сертификаты и другие изображения
          </Description>
        </DescriptionContainer>
      </AddMaterialRow>
      <AddMaterialRow
        onPress={onAddCasePressed}
      >
        <CaseImg />
        <DescriptionContainer>
          <Title>
            Кейс
          </Title>
          <Description>
            До и после или история болезней
          </Description>
        </DescriptionContainer>
      </AddMaterialRow>
      <AddMaterialRow
        onPress={onAddWorkPressed}
      >
        <ListImg />
        <DescriptionContainer>
          <Title>
            Другое
          </Title>
          <Description>
            Статьи или любой другой текст и изображения
          </Description>
        </DescriptionContainer>
      </AddMaterialRow>
    </AddMaterialContainer>
  )
}
