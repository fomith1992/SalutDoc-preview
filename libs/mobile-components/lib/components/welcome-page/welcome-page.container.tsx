import { StackActions } from '@react-navigation/native'
import React from 'react'
import { useLogoutFirebase } from '../../hooks/firebase/use-logout-firebase'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { WelcomePageView } from './welcome-page.view'

export function WelcomePage (): React.ReactElement {
  const navigation = useStackNavigation()
  useLogoutFirebase()
  return (
    <WelcomePageView
      onLogin={() => navigation.dispatch(StackActions.push('Login_Stack', {}))}
      onPressNewPatinentButton={() => navigation.dispatch(StackActions.push('AnonymousMain', {}))}
      onRegisterDoctor={() => navigation.dispatch(StackActions.push('PromoBannerDoctor', {}))}
    />
  )
}
