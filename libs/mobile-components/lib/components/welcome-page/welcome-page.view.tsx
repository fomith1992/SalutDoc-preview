import React from 'react'
import { TextButton } from '../../modules/ui-kit'
import { LogoView } from '../logo/logo.view'
import {
  LoginBtn,
  LoginContainer,
  LogoContainer,
  Title
} from './welcome-page.style'

export interface LoginViewProps {
  onLogin: () => void
  onPressNewPatinentButton: () => void
  onRegisterDoctor: () => void
}

export function WelcomePageView ({
  onLogin,
  onPressNewPatinentButton,
  onRegisterDoctor
}: LoginViewProps): React.ReactElement {
  return (
    <LoginContainer>
      <LogoContainer>
        <LogoView />
        <Title>
          Консультации с проверенными {'\n'} врачами по доступным ценам
        </Title>
      </LogoContainer>
      <LoginBtn>
        <TextButton
          type='primary'
          size='XL'
          onPress={onPressNewPatinentButton}
        >
          Я новый пациент
        </TextButton>
      </LoginBtn>
      <LoginBtn>
        <TextButton
          type='secondary'
          size='XL'
          onPress={onRegisterDoctor}
        >
          Я новый врач
        </TextButton>
      </LoginBtn>
      <LoginBtn>
        <TextButton
          type='link'
          size='XL'
          onPress={onLogin}
        >
          Я уже использовал сервис
        </TextButton>
      </LoginBtn>
    </LoginContainer>
  )
}
