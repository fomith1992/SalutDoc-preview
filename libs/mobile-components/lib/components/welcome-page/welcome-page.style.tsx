import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { sp } from '../../style/size'

export const LoginContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface'),
  paddingBottom: sp(24)
})

export const LogoContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
})

export const Title = styled(Text, {
  ...font({ type: 'h2', weight: 'light' }),
  color: color('text'),
  marginTop: sp(24)
})

export const LoginBtn = styled(View, {
  ...container(),
  marginTop: sp(16)
})
