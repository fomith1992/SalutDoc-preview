import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { ImageOrIconView } from '../image-or-icon/image-or-icon.view'

export const AvatarView = styled(ImageOrIconView, {
  backgroundColor: color('secondaryLight')
})
