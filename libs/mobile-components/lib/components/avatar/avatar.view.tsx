import React from 'react'
import { StyleProp, ViewStyle } from 'react-native'
import { IconComponent } from '../../images/icon-component'
import { ProfileIcon24 } from '../../images/profile.icon-24'
import { ColorType } from '../../style/color'
import { buildImageUrl } from '../../utils'
import { ShapeIconType } from '../image-or-icon/image-or-icon.view'
import { AvatarView } from './avatar.style'

type FilterProps = 'bw'

export interface AvatarProps {
  url?: string | null
  shape?: ShapeIconType
  placeholder?: IconComponent
  withLogo?: boolean
  filter?: FilterProps
  placeholderColor?: ColorType
  style?: StyleProp<ViewStyle>

  children?: React.ReactNode
}

export function Avatar (
  {
    url,
    shape = 'square',
    placeholder: Placeholder = ProfileIcon24,
    placeholderColor,
    withLogo,
    children,
    filter,
    style
  }: AvatarProps
): React.ReactElement {
  return (
    <AvatarView
      url={buildImageUrl(url, filter)}
      style={style}
      shape={shape}
      withLogo={withLogo}
      children={children}
      placeholder={Placeholder}
      placeholderColor={placeholderColor}
    />
  )
}
