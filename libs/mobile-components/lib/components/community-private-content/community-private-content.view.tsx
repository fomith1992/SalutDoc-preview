import React from 'react'
import { Container, Content, Lock } from './community-private-content.style'

export const CommunityEmptyView = (): React.ReactElement => {
  return (
    <Container>
      <Lock />
      <Content>
        Это закрытая группа.{'\n'}Информация доступна только подписчикам
      </Content>
    </Container>
  )
}
