import { Text, View } from 'react-native'
import { LockIcon24 } from '../../images/lock.icon-24'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'

export const Container = styled(View, {
  flex: 1,
  alignItems: 'center',
  marginTop: sp(64)
})

export const Content = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('inactive'),
  textAlign: 'center',
  marginTop: sp(16)
})

export const Lock = styled(LockIcon24, {
  width: sp(48),
  height: sp(64),
  color: color('inactive')
})
