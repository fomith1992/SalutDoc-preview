import { ScrollView, Text, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'

export const StoryContainer = styled(View, {
  flexDirection: 'column',
  alignItems: 'center',
  marginRight: 14,
  marginTop: 10,
  marginBottom: 16,
  width: 70
})

export const AvatarContainer = styled(View, {
  height: 32,
  width: 32,
  borderRadius: 4,
  overflow: 'hidden'
})

export const StoryTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  color: color('text')
})

export const StoriesSlider = styled(ScrollView, {
  flexDirection: 'row',
  paddingLeft: 10
})

export const StoriesSliderContainer = styled(View, {
  backgroundColor: color('surface')
})
