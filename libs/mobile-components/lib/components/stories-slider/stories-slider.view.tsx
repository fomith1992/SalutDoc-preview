import React from 'react'
import { TouchableOpacity } from 'react-native'
import { Avatar } from '../avatar/avatar.view'
import {
  StoryContainer,
  AvatarContainer,
  StoryTitle,
  StoriesSliderContainer,
  StoriesSlider
} from './stories-slider.style'

interface StoryProps {
  avatar?: string | null
  title: string
}

interface StoriesSliderProps {
  stories: StoryProps[]
}

function StoryView ({ avatar = null, title }: StoryProps): React.ReactElement {
  return (
    <TouchableOpacity>
      <StoryContainer>
        <AvatarContainer>
          <Avatar url={avatar} />
        </AvatarContainer>
        <StoryTitle numberOfLines={1}>
          {title}
        </StoryTitle>
      </StoryContainer>
    </TouchableOpacity>
  )
}

export function StoriesSliderView ({ stories }: StoriesSliderProps): React.ReactElement {
  return (
    <StoriesSliderContainer>
      <StoriesSlider horizontal showsHorizontalScrollIndicator={false}>
        {stories.map((item, idx) => <StoryView {...item} key={idx} />)}
      </StoriesSlider>
    </StoriesSliderContainer>
  )
}
