import { View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

const Skeleton = styled(View, {
  backgroundColor: color('background'),
  borderRadius: 4
})

export const Avatar = styled(Skeleton, {
  borderRadius: 99,
  aspectRatio: 1,
  width: sp(80 as any)
})

export const Name = styled(Skeleton, {
  width: '100%',
  height: sp(16),
  marginBottom: sp(4)
})

export const Specialization = styled(Skeleton, {
  width: '60%',
  height: sp(12),
  marginBottom: sp(4)
})

export const ActionButton = styled(Skeleton, {
  marginVertical: sp(4),
  height: sp(48),
  flex: 1
})
