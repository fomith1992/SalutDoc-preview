import { FlatList, Platform, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { ElasticText } from '../elastic-text/elastic-text'

export const ProfileContainer = styled(View, {
  backgroundColor: color('surface')
})

export const MainRow = styled(View, {
  ...container(),
  marginTop: sp(16),
  paddingVertical: sp(8),
  flexDirection: 'row'
})

export const AvatarTouchable = styled(TouchableOpacity, {
  width: sp(80)
})

export const InfoContainer = styled(View, (props: { isMy: boolean }) => ({
  flex: 1,
  marginLeft: sp(16),
  justifyContent: props.isMy ? 'center' : undefined
}) as const)

export const Name = styled(ElasticText, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const Specialization = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const ActionButtonRow = styled(View, {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-end',
  marginTop: sp(12)
})

export const ActionButton = styled(View, {
  marginRight: 'auto',
  marginBottom: sp(2 as any) // 2 on design
})

export const MakeAppointment = styled(View, {
  marginRight: sp(8),
  marginBottom: sp(2 as any) // 2 on design
})

export const FollowButtonsContainer = styled(View, (props: { isDoctor: boolean }) => ({
  ...(props.isDoctor ? {} : {
    ...container()
  }),
  flexDirection: 'row',
  paddingTop: sp(8),
  paddingBottom: sp(24)
}) as const)

export const ActionButtonsFlatList = FlatList

export const ActionButtonContainer = styled(TouchableOpacity, {
  justifyContent: 'center',
  backgroundColor: color('surface'),
  marginVertical: sp(4),
  marginHorizontal: sp(2 as any), // for shadow
  paddingHorizontal: sp(8),
  paddingVertical: sp(16),
  flex: 1,
  minWidth: sp(104 as any), // 104 on design
  borderRadius: sp(16),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const ActionButtonTitleContainer = styled(View, {
  flexDirection: 'column',
  alignItems: 'center'
})

export const ActionButtonTopText = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const ActionButtonBottomTitle = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const Spring = styled(View, (props: { isSeparator: boolean }) => ({
  width: sp(props.isSeparator ? 4 : 16)
}) as const)
