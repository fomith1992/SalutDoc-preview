import React, { useEffect } from 'react'
import { StackActions, useNavigation } from '@react-navigation/native'
import { useUserProfileInfoQuery } from '../../gen/graphql'
import { UserFollowButton } from '../../modules/user-follow'
import { AnimatedPlaceholder } from '../animated-placeholder/animated-placeholder'
import { UserProfileSkeletonView } from './user-profile-skeleton.view'
import { UserProfileView } from './user-profile.view'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'

export interface UserProfileProps {
  userId: string
}

export function UserProfile ({ userId }: UserProfileProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useNavigation()
  const { data, loading, error } = useUserProfileInfoQuery({
    variables: { userId }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  return (
    <AnimatedPlaceholder
      placeholder={<UserProfileSkeletonView isMy={user?.id === userId} />}
    >
      {data?.userById == null ||
      loading ? null
        : (
          <UserProfileView
            avatar={data.userById.avatar}
            firstName={data.userById.firstName}
            lastName={data.userById.lastName}
            followers={data.userById.followersCount ?? null}
            followees={data.userById.followeesCount ?? null}
            communities={data.userById.communitiesMembershipCount ?? null}
            specialization={data.userById.specialization ?? null}
            consultations={data.userById.consultationsCount ?? null}
            isMy={user?.id === userId}
            appointmentButtonVisible={false} // TODO add condition, if appMode === 'PATIENT' (made in SD-1350)
            isDoctor={data.userById.role === 'DOCTOR'}
            followBtn={user?.id === userId ? null : (
              <UserFollowButton
                followeeId={userId}
              />
            )}
            handlePressFollowers={() => navigation.dispatch(StackActions.push('Followers', { userId: userId }))}
            handlePressFollowees={() => navigation.dispatch(StackActions.push('Followees', { userId: userId }))}
            handlePressConsultation={() => navigation.dispatch(StackActions.push('PatientConsultation', {} /* { userId: userId } */))}
            handlePressCommunities={() => navigation.dispatch(StackActions.push('Communities', { userId: userId }))}
            handlePressContactInfo={() => navigation.dispatch(StackActions.push('ContactInfo', { userId }))}
            onRecordPagePressed={() => navigation.dispatch(StackActions.push('RecordPage', { userId }))}
          />
        )}
    </AnimatedPlaceholder>
  )
}
