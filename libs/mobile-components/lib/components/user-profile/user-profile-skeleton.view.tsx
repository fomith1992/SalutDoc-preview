import React from 'react'
import { ActionButton, Avatar, Name, Specialization } from './user-profile-skeleton.style'
import { FollowButtonsContainer, InfoContainer, MainRow, ProfileContainer, Spring } from './user-profile.style'

export function UserProfileSkeletonView (
  props: {
    isMy: boolean
  }
): React.ReactElement {
  const { isMy } = props
  return (
    <ProfileContainer>
      <MainRow>
        <Avatar />
        <InfoContainer isMy={isMy}>
          <Name />
          <Specialization />
        </InfoContainer>
      </MainRow>
      <FollowButtonsContainer
        isDoctor={false}
      >
        <ActionButton />
        <Spring isSeparator />
        <ActionButton />
        <Spring isSeparator />
        <ActionButton />
      </FollowButtonsContainer>
    </ProfileContainer>
  )
}
