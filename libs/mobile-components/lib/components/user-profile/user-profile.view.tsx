import React, { useState } from 'react'
import { toCaseCount } from '../../i18n/utils'
import { ScreenBlock, TextButton } from '../../modules/ui-kit'
import { Avatar } from '../avatar/avatar.view'
import { Gallery } from '../gallery/gallery.container'
import {
  ActionButton,
  ActionButtonBottomTitle,
  ActionButtonContainer,
  ActionButtonRow,
  ActionButtonsFlatList,
  ActionButtonTitleContainer,
  ActionButtonTopText,
  AvatarTouchable,
  FollowButtonsContainer,
  InfoContainer,
  MainRow,
  Name,
  ProfileContainer,
  Specialization,
  MakeAppointment,
  Spring
} from './user-profile.style'

interface ProfileScreenProps {
  isMy: boolean
  isDoctor: boolean
  appointmentButtonVisible: boolean
  firstName: string
  lastName: string
  specialization: string | null
  avatar: string | null

  followBtn?: React.ReactNode

  followers: number | null
  followees: number | null
  communities: number | null
  consultations: number | null

  handlePressFollowers: () => void
  handlePressFollowees: () => void
  handlePressCommunities: () => void
  handlePressConsultation: () => void
  handlePressContactInfo: () => void
  onRecordPagePressed: () => void
}

interface ActionButtonProps {
  onPress: () => void
  topContent: React.ReactNode
  bottomContent: React.ReactNode
  loading?: boolean
}

function ActionButtonView (props: { params: ActionButtonProps }): React.ReactElement {
  const {
    topContent,
    bottomContent,
    loading = false,
    onPress
  } = props.params
  return (
    <ActionButtonContainer
      onPress={onPress}
    >
      {loading ? '' : (
        <ActionButtonTitleContainer>
          <ActionButtonTopText>
            {topContent}
          </ActionButtonTopText>
          <ActionButtonBottomTitle>
            {bottomContent}
          </ActionButtonBottomTitle>
        </ActionButtonTitleContainer>
      )}
    </ActionButtonContainer>
  )
}

export function UserProfileView (
  {
    isMy,
    isDoctor,
    appointmentButtonVisible,
    firstName,
    lastName,
    avatar,
    specialization,
    followees,
    followers,
    communities,
    consultations,
    followBtn,
    onRecordPagePressed,
    handlePressFollowers,
    handlePressFollowees,
    handlePressCommunities,
    handlePressConsultation,
    handlePressContactInfo
  }: ProfileScreenProps
): React.ReactElement {
  const [galleryVisible, setGalleryVisible] = useState(false)
  const [galleryShowIndex, setGalleryShowIndex] = useState(1)
  const actionButtonsData: ActionButtonProps[] = isDoctor ? [
    // consultation
    {
      onPress: isMy
        ? handlePressConsultation
        : () => {},
      topContent: consultations,
      bottomContent: toCaseCount(['консультация', 'консультации', 'консультаций'], consultations ?? 0),
      loading: consultations == null
    },

    // followees
    {
      onPress: handlePressFollowees,
      topContent: followees,
      bottomContent: toCaseCount(['подписка', 'подписки', 'подписок'], followees ?? 0),
      loading: followees == null
    },

    // contact info
    {
      onPress: handlePressContactInfo,
      topContent: '...', // TODO: ICON
      bottomContent: 'о враче'
    },

    // followers
    {
      onPress: handlePressFollowers,
      topContent: followers,
      bottomContent: toCaseCount(['подписчик', 'подписчика', 'подписчиков'], followers ?? 0),
      loading: followers == null
    },

    // communities
    {
      onPress: handlePressCommunities,
      topContent: communities,
      bottomContent: toCaseCount(['группа', 'группы', 'групп'], communities ?? 0),
      loading: communities == null
    }
  ] : [
    // followees
    {
      onPress: handlePressFollowees,
      topContent: followees,
      bottomContent: toCaseCount(['подписка', 'подписки', 'подписок'], followees ?? 0),
      loading: followees == null
    },

    // followers
    {
      onPress: handlePressFollowers,
      topContent: followers,
      bottomContent: toCaseCount(['подписчик', 'подписчика', 'подписчиков'], followers ?? 0),
      loading: followers == null
    }
  ]
  function onShowGallery (showIndex: number): void {
    setGalleryShowIndex(showIndex)
    setGalleryVisible(true)
  }
  return (
    <ScreenBlock>
      <ProfileContainer>
        <MainRow>
          <AvatarTouchable
            disabled={avatar == null}
            onPress={() => onShowGallery(0)}
          >
            <Avatar
              url={avatar}
              shape='circle'
            />
          </AvatarTouchable>
          <InfoContainer
            isMy={isMy}
          >
            <Name numberOfLines={2}>
              {`${firstName} ${lastName}`}
            </Name>
            <Specialization>
              {specialization}
            </Specialization>
            {
              isMy ? (
                <></>
              ) : (
                <ActionButtonRow>
                  {appointmentButtonVisible
                    ? (
                      <MakeAppointment>
                        <TextButton
                          onPress={onRecordPagePressed}
                          type='primary'
                          size='M'
                        >
                          Записаться
                        </TextButton>
                      </MakeAppointment>
                    ) : null}
                  <ActionButton>
                    {followBtn}
                  </ActionButton>
                </ActionButtonRow>
              )
            }
          </InfoContainer>
        </MainRow>
        <FollowButtonsContainer
          isDoctor={isDoctor}
        >
          {isDoctor ? (
            <ActionButtonsFlatList
              horizontal
              scrollEnabled={isDoctor}
              ListHeaderComponent={<Spring isSeparator={false} />}
              ListFooterComponent={<Spring isSeparator={false} />}
              showsHorizontalScrollIndicator={false}
              data={actionButtonsData}
              renderItem={({ item }) => (<ActionButtonView params={item} />)}
              ItemSeparatorComponent={() => <Spring isSeparator />}
              keyExtractor={(_item, index) => `action-buttons${index}`}
            />
          ) : (
            actionButtonsData.map((item, idx) => (
              <React.Fragment key={`action-buttons-user-${idx}`}>
                <ActionButtonView params={item} />
                {idx !== actionButtonsData.length - 1 ? (
                  <Spring isSeparator />
                ) : (
                  <></>
                )}
              </React.Fragment>
            ))
          )}
        </FollowButtonsContainer>
      </ProfileContainer>
      {avatar != null && (
        <Gallery
          attachments={[{ __typename: 'ImagePostAttachment', url: avatar }]}
          showIndex={galleryShowIndex}
          galleryVisible={galleryVisible}
          onClose={() => setGalleryVisible(false)}
        />
      )}
    </ScreenBlock>
  )
}
