import React, { useContext } from 'react'
import { Role, useAuthenticatedUserQuery } from '../../gen/graphql'

export interface AuthenticatedUser {
  id: string
  avatar: string | null
  role: Role | null
}

export interface AuthUserProviderProps {
  children: React.ReactNode
  fallback?: React.ReactNode
}

const AuthenticatedUserContext = React.createContext<AuthenticatedUser | null>(null)

export function useAuthenticatedUser (): AuthenticatedUser {
  const user = useContext(AuthenticatedUserContext)
  if (user == null) {
    throw new Error('User is not authenticated')
  }
  return user
}

export function useAuthenticatedUserOrNull (): AuthenticatedUser | null {
  return useContext(AuthenticatedUserContext)
}

export function AuthenticatedUserProvider ({
  // fallback,
  children
}: AuthUserProviderProps): React.ReactElement {
  const { data/* , loading */ } = useAuthenticatedUserQuery({
    fetchPolicy: 'network-only'
  })
  /* if (loading) {
    return <>{fallback}</>
  } */
  return (
    <AuthenticatedUserContext.Provider value={data?.userMyself ?? null}>
      {children}
    </AuthenticatedUserContext.Provider>
  )
}
