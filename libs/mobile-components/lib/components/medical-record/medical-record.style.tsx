import { Text, View } from 'react-native'
import { CardIcon56 } from '../../images/card.icon-56'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'

export const Container = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('background')
})
export const EmptyContainer = styled(View, {
  justifyContent: 'center',
  alignItems: 'center'
})

export const CardIcon = styled(CardIcon56, {
  color: color('accent')
})

export const EmptyTitle = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  marginTop: sp(20)
})

export const EmptyDescription = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(12)
})

export const ButtonsContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: sp(24)
})

export const Spring = styled(View, {
  width: sp(16),
  height: 1
})
