import React from 'react'
import { useNavigation, StackActions } from '@react-navigation/native'
import { useDefaultHeader } from '../../modules/ui-kit/header'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'
import { AuthMedicalRecord } from './auth-medical-record.container'
import { NotAuthMedicalRecord } from './not-auth-medical-record.container'

export function MedicalRecord (): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigator = useNavigation()
  useDefaultHeader({
    actions: [{
      icon: 'settings',
      onPress: () => navigator.dispatch(
        StackActions.push(user == null ? 'SettingsAnonymous' : 'Settings')
      )
    }]
  })
  if (user == null) return <NotAuthMedicalRecord />
  return <AuthMedicalRecord />
}
