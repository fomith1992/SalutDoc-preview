import React from 'react'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { MedicalRecordView } from './medical-record.view'
import { StackActions } from '@react-navigation/native'
import { useRootStore } from '../../stores/root.store'

export function NotAuthMedicalRecord (): React.ReactElement {
  const navigation = useStackNavigation()
  const {
    setMode
  } = useRootStore().domains.registerReport
  return (
    <MedicalRecordView
      onLogin={() => navigation.dispatch(StackActions.push('Login_Stack', {}))}
      onRegister={() => {
        setMode('DEFAULT')
        navigation.dispatch(StackActions.push('Register_PatientStack', {}))
      }}
    />
  )
}
