import React from 'react'
import { TextButton } from '../../modules/ui-kit'
import { CircleWithIcon } from '../../modules/ui-kit/circle-with-icon/circle-with-icon.view'
import {
  Container,
  EmptyContainer,
  CardIcon,
  EmptyTitle,
  EmptyDescription,
  ButtonsContainer,
  Spring
} from './medical-record.style'

export interface MedicalRecordViewProps {
  onLogin: () => void
  onRegister: () => void
}

export function MedicalRecordView ({ onLogin, onRegister }: MedicalRecordViewProps): React.ReactElement {
  return (
    <Container>
      <EmptyContainer>
        <CircleWithIcon filled>
          <CardIcon />
        </CircleWithIcon>
        <EmptyTitle>
          Здесь будет ваша медкарта
        </EmptyTitle>
        <EmptyDescription>
          Зарегистрируйтесь, чтобы удобно хранить {'\n'} медицинские файлы и профили врачей
        </EmptyDescription>
        <ButtonsContainer>
          <TextButton
            onPress={onRegister}
            size='L'
          >
            Зарегистрироваться
          </TextButton>
          <Spring />
          <TextButton
            onPress={onLogin}
            size='L'
            type='secondary'
          >
            Войти
          </TextButton>
        </ButtonsContainer>
      </EmptyContainer>
    </Container>
  )
}
