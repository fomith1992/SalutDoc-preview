import { createFormDescriptor } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { yup } from '../../../modules/form-kit'
import { MaskedTextInput } from '../../../modules/form-kit/text-input/masked-input'
import { ExternalLink, SubmitTextButton } from '../../../modules/ui-kit'
import { useMaterialized } from '../../../style/styled'
import {
  ContainerStyleSheet,
  LoginContainer,
  LoginDescription,
  ButtonContainer,
  InputContainer,
  BottomContainer,
  BottomText,
  BottomLink
} from './login-number.style'

const formSchema = yup.object({
  phoneNumber: yup.string()
    .matches(/\+7 \(\d{3}\) \d{3}-\d\d-\d\d/, 'Введите корректный номер телефона')
    .required()
}).required()

const { Form, Field, FormSpy } = createFormDescriptor(formSchema)

interface LoginNumberViewProps {
  phoneNumber: string
  privacyPolicyHref: string
  termsOfServiceHref: string
  setPhoneNumber: (phoneNumber: string) => void
  onSubmit: () => void
}

export function LoginNumberView ({
  phoneNumber,
  privacyPolicyHref,
  termsOfServiceHref,
  setPhoneNumber,
  onSubmit
}: LoginNumberViewProps): React.ReactElement {
  const containerStyle = useMaterialized(ContainerStyleSheet)

  const [validForm, setValidForm] = useState(false)

  return (
    <Form
      onSubmit={() => ({})}
      initialValues={{
        phoneNumber
      }}
    >
      <FormSpy
        subscription={{ valid: true, values: true }}
        onChange={({ valid, values }) => {
          setPhoneNumber(values.phoneNumber)
          setValidForm(valid)
        }}
      />
      <LoginContainer
        contentContainerStyle={containerStyle}
      >
        <LoginDescription>
          Введите номер телефона, на который вы регистрировали свой аккаунт
        </LoginDescription>
        <InputContainer>
          <Field name='phoneNumber'>
            {props => (
              <MaskedTextInput
                {...props}
                maxLength={18}
                type='cel-phone'
                placeholder='+7 (900) 000-00-00'
                mask='+7 (999) 999-99-99'
              />
            )}
          </Field>
        </InputContainer>
        <BottomContainer>
          <BottomText>
            Нажимая кнопку «Получить код» вы соглашаетесь с{' '}
            <ExternalLink href={privacyPolicyHref}>
              {({ onPress }) => (
                <BottomLink onPress={onPress}>
                  политикой конфиденциальности
                </BottomLink>
              )}
            </ExternalLink>
            {' '}и{' '}
            <ExternalLink href={termsOfServiceHref}>
              {({ onPress }) => (
                <BottomLink onPress={onPress}>
                  пользовательским соглашением
                </BottomLink>
              )}
            </ExternalLink>
          </BottomText>
        </BottomContainer>
        <ButtonContainer>
          <SubmitTextButton
            type='primary'
            size='XL'
            valid={validForm}
            onSubmit={onSubmit}
          >
            Получить код
          </SubmitTextButton>
        </ButtonContainer>
      </LoginContainer>
    </Form>
  )
}
