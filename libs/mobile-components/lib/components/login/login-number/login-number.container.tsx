import { useNavigation } from '@react-navigation/native'
import { Alert } from 'react-native'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React from 'react'
import { useRootStore } from '../../../stores/root.store'
import { LoginNumberView } from './login-number.view'
import { expectDefined } from '@salutdoc/react-components'
import ky from 'ky/umd'

const termsOfServiceUrl = 'https://about.salutdoc.com/terms-of-service'
const privacyPolicyUrl = 'https://about.salutdoc.com/privacy/'

export const LoginNumber = observer((): React.ReactElement => {
  const FRONT_URL = expectDefined(process.env.FRONT_URL)
  const {
    phoneNumber,
    setPhoneNumber
  } = useRootStore().domains.registerReport
  const navigation = useNavigation()

  function handleSubmit (): void {
    ky.post(new URL('/api/v1/registration/phone/verifications', FRONT_URL), {
      json: {
        phoneNumber: phoneNumber,
        existingAccount: false
      }
    })
      .then(() => {
        Alert.alert(
          'Ошибка',
          'Профиля с таким телефоном нет. Пройдите регистрацию.'
        )
      })
      .catch(e => {
        if (e instanceof ky.HTTPError && e.response.status === 400) {
          navigation.navigate('LoginVerification', {})
        } else {
          Alert.alert(
            'Ошибка',
            'Повторите позже'
          )
        }
      })
  }

  return (
    <LoginNumberView
      phoneNumber={toJS(phoneNumber ?? '')}
      setPhoneNumber={setPhoneNumber}
      onSubmit={handleSubmit}
      privacyPolicyHref={privacyPolicyUrl}
      termsOfServiceHref={termsOfServiceUrl}
    />
  )
})
