import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React, { useEffect, useState } from 'react'
import { Alert } from 'react-native'
import { StackNavigationProp } from '@react-navigation/stack'
import { FirebaseAuthTypes } from '@react-native-firebase/auth'
import { useRootStore } from '../../../stores/root.store'
import { LoginVerificationView } from './login-verification.view'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useAuthFirebase } from '../../../hooks/firebase/use-auth-firebase'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'
import { useOidcFirebaseAuthentication } from '../../../modules/oidc/oidc-firebase-authentication-context'

export const LoginVerification = observer((): React.ReactElement => {
  const {
    phoneNumber
  } = useRootStore().domains.registerReport
  const { login } = useOidcFirebaseAuthentication()
  const [timerRemaining, setTimerRemaining] = useState<number>(Date.now() + 60000)
  const navigation = useStackNavigation()
  const { loading, error, confirmCode, resend } = useAuthFirebase({
    phoneNumber: phoneNumber ?? null,
    onSuccess: (user: FirebaseAuthTypes.User) => {
      user.getIdToken(true)
        .then(
          async token => {
            await login(token)
            navigation.dangerouslyGetParent<StackNavigationProp<any>>()?.pop()
          })
        .catch(console.log)
    }
  })

  useEffect(() => {
    if (error != null) {
      switch (error) {
        case 'auth/invalid-phone-number':
          navigation.goBack()
          return Alert.alert(
            'Ошибка',
            'Невалидный номер телефона. Проверьте правильность введенного номера'
          )
        case 'auth/too-many-requests':
          navigation.goBack()
          return Alert.alert(
            'Ошибка',
            'Вы превысили допустимую квоту смс сообщений. Попробуйте позднее'
          )
        case 'auth/invalid-verification-code':
          return Alert.alert(
            'Ошибка',
            'Неверный код.'
          )
        default:
          navigation.goBack()
          return Alert.alert(
            'Ошибка',
            'Попробуйте позднее'
          )
      }
    }
  }, [error])

  async function saveUser (code: string): Promise<void> {
    try {
      confirmCode(code)
    } catch (error) {
      Alert.alert(
        'Ошибка',
        'Неверный код'
      )
    }
  }

  function handleSubmit (code: string): void {
    saveUser(code).catch(e => console.log('err: ', e))
  }

  function handleResend (): void {
    resend()
    setTimerRemaining(Date.now() + 60000)
  }

  return (
    <>
      <LoginVerificationView
        phoneNumber={toJS(phoneNumber ?? '')}
        onSubmit={handleSubmit}
        resendCodeAfter={timerRemaining}
        resendMessageCode={handleResend}
      />
      <ModalLoadingIndicator
        visible={loading}
      />
    </>
  )
})
