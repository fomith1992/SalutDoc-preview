import { ScrollView, Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { lift, styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const ContainerStyleSheet = lift({
  flexGrow: 1
})

export const CellStyleSheet = lift({
  borderWidth: 0.5,
  borderRadius: sp(4),
  borderColor: color('inactive')
})

export const CellFocusedStyleSheet = lift({
  borderWidth: 0.5,
  borderRadius: sp(4),
  borderColor: color('accent')
})

export const TextStyleSheet = lift({
  ...font({ type: 'text1' }),
  color: color('text')
})

export const LoginContainer = styled(ScrollView, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface')
})

export const LoginDescription = styled(Text, {
  marginTop: sp(24),
  ...font({ type: 'text1' }),
  color: color('subtext')
})

export const ButtonContainer = styled(View, {
  marginBottom: sp(24)
})

export const InputContainer = styled(View, {
  marginTop: sp(20),
  justifyContent: 'center',
  alignItems: 'center'
})

export const BottomContainer = styled(View, {
  marginTop: 'auto',
  marginBottom: sp(20)
})

export const BottomText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  textAlign: 'center'
})

export const BottomLink = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  textAlign: 'center',
  textDecorationLine: 'underline'
})
