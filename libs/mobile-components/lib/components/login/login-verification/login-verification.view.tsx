import React, { useState } from 'react'
import { toCaseCount } from '../../../i18n/utils'
import { TextButton } from '../../../modules/ui-kit'
import { useMaterialized } from '../../../style/styled'
import {
  ContainerStyleSheet,
  LoginContainer,
  LoginDescription,
  ButtonContainer,
  InputContainer,
  BottomContainer,
  BottomText,
  CellStyleSheet,
  CellFocusedStyleSheet,
  TextStyleSheet
} from './login-verification.style'
import useInterval from 'react-use/lib/useInterval'
import useUpdate from 'react-use/lib/useUpdate'
import { PinCodeInput } from '../../../modules/form-kit/pin-code-input/pin-code-input.view'

interface LoginVerificationViewProps {
  phoneNumber: string
  onSubmit: (value: string) => void
  resendCodeAfter: number
  resendMessageCode: () => void
}

const pinCodeLength = 6

export function LoginVerificationView ({
  phoneNumber,
  resendCodeAfter,
  onSubmit,
  resendMessageCode
}: LoginVerificationViewProps): React.ReactElement {
  const [verificationCode, setVerificationCode] = useState('')

  const containerStyle = useMaterialized(ContainerStyleSheet)

  const cellStyle = useMaterialized(CellStyleSheet)
  const cellFocusedStyle = useMaterialized(CellFocusedStyleSheet)
  const textStyle = useMaterialized(TextStyleSheet)

  const timerRemaining = Math.round((resendCodeAfter - Date.now()) / 1000)
  useInterval(
    useUpdate(),
    timerRemaining > 0 ? 1000 : null
  )

  return (
    <LoginContainer
      contentContainerStyle={containerStyle}
    >
      <LoginDescription>
        Смс с кодом подтверждения было отправлено на номер {phoneNumber}.
      </LoginDescription>
      <InputContainer>
        <PinCodeInput
          codeLength={pinCodeLength}
          cellStyle={cellStyle}
          cellStyleFocused={cellFocusedStyle}
          textStyle={textStyle}
          value={verificationCode}
          onTextChange={setVerificationCode}
          autoFocus
        />
      </InputContainer>
      <BottomContainer>
        {timerRemaining > 0
          ? (
            <BottomText>
              {`Отправить смс повторно можно через ${timerRemaining} ${toCaseCount(['секунду', 'секунды', 'секунд'], timerRemaining)}`}
            </BottomText>
          ) : (
            <TextButton
              type='link'
              onPress={resendMessageCode}
            >
              Отправить смс повторно
            </TextButton>
          )}
      </BottomContainer>
      <ButtonContainer>
        <TextButton
          disabled={pinCodeLength !== verificationCode.length}
          onPress={() => onSubmit(verificationCode)}
          type='primary'
          size='XL'
        >
          Подтвердить
        </TextButton>
      </ButtonContainer>
    </LoginContainer>
  )
}
