import { StackActions, useNavigation } from '@react-navigation/native'
import React, { useEffect } from 'react'
import {
  CommunityDataFragment,
  FollowedUserFragmentFragment,
  PostDataFragment,
  QuestionDataFragment,
  SearchedItemType,
  useComposerSearchQuery,
  UserDataFragment,
  WorkDataFragment
} from '../../gen/graphql'
import { useDebouncedValue } from '../../hooks/use-debounced-value'
import { useRefresh } from '../../hooks/use-refresh'
import { toCaseCount } from '../../i18n/utils'
import { useAuthenticatedUser } from '../user-context/user-context-provider'
import { PreviewSlider } from './preview-slider.view'
import { SearchItem, SearchView } from './search.view'

type SearchComposerProps = {
  renderItem: (item: SearchItem) => React.ReactElement
  setQuery: (query: string) => void
  setSearchedType: (searchedType: SearchedItemType | null) => void
  query: string
  searchedType: SearchedItemType | null
}

export function SearchComposer (
  {
    setQuery,
    query,
    renderItem,
    setSearchedType,
    searchedType
  }: SearchComposerProps
): React.ReactElement {
  const navigation = useNavigation()
  const user = useAuthenticatedUser()
  const effectiveQuery = useDebouncedValue(query, 200)
  const { data, loading, error, refetch } = useComposerSearchQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      userId: user.id,
      recommendationTypes: searchedType == null ? null : [searchedType],
      searchTypes: searchedType == null ? null : [searchedType],
      query: effectiveQuery,
      hasQuery: effectiveQuery !== '',
      includeUsersAndCommunities: searchedType == null
    }
  })

  const { refreshing, refresh } = useRefresh(refetch)

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  const fetchData: SearchResult | undefined = data?.userById ?? data
  const users = fetchData?.users?.edges != null && fetchData.users.edges.length > 0
    ? (
      <PreviewSlider
        items={fetchData.users.edges.map(({ node }) => {
          switch (node.__typename) {
            case 'User':
              return {
                avatar: node.avatar ?? undefined,
                firstName: node.firstName,
                lastName: node.lastName,
                description: node.specialization ?? '',
                onPress: () => navigation.dispatch(StackActions.push('Profile', { userId: node.id }))
              }
            default:
              throw new Error('Unexpected searched type')
          }
        })}
        title='Найденные пользователи'
      />
    )
    : <></>

  const communities = fetchData?.communities?.edges != null && fetchData.communities.edges.length > 0
    ? (
      <PreviewSlider
        items={fetchData.communities.edges.map(({ node }) => {
          switch (node.__typename) {
            case 'Community':
              return {
                avatar: node.avatar ?? undefined,
                firstName: node.name,
                description: `${node.membersCount ?? 0} ${toCaseCount(
                  ['подписчик', 'подписчика', 'подписчиков'], node.membersCount ?? 0)}`,
                onPress: () => navigation.dispatch(StackActions.push('Community', { communityId: node.id }))
              }
            default:
              throw new Error('Unexpected searched type')
          }
        })}
        title='Найденные сообщества'
      />
    )
    : <></>
  return (
    <SearchView
      query={query}
      onSubmit={setQuery}
      handleCancelPressed={navigation.goBack}
      renderItem={renderItem}
      setSearchedType={setSearchedType}
      searchedType={searchedType}
      loading={loading}
      usersSearchData={users}
      communitiesSearchData={communities}
      refresh={refresh}
      refreshing={refreshing}
      content={fetchData?.content?.edges != null && fetchData.content.edges.length > 0
        ? fetchData.content.edges.map(({ node }) => {
          switch (node.__typename) {
            case 'Post':
              return {
                id: node.id,
                type: 'POST'
              }
            case 'Article':
              return {
                id: node.id,
                type: 'ARTICLE'
              }
            case 'User':
              return {
                id: node.id,
                type: 'USER'
              }
            case 'Community':
              return {
                id: node.id,
                type: 'COMMUNITY'
              }
            case 'Question':
              return {
                id: node.id,
                type: 'QUESTION'
              }
            default:
              throw new Error('Unexpected search type')
          }
        }) : []}
    />
  )
}

type SearchNode =
  PostDataFragment |
  WorkDataFragment |
  QuestionDataFragment |
  UserDataFragment & FollowedUserFragmentFragment |
  CommunityDataFragment

interface SearchConnection {
  edges: Array<{
    recommendationId: string | null
    node: SearchNode
  }>
}

interface SearchResult {
  content: SearchConnection | null
  users: SearchConnection | null
  communities: SearchConnection | null
}
