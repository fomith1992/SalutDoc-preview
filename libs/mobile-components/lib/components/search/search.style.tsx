import { Platform, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { CrossIcon16 } from '../../images/cross.icon-16'
import { FeedIcon24 } from '../../images/feed.icon-24'
import { SearchIcon24 } from '../../images/search.icon-24'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const SearchListContainer = styled(View, {
  ...container('margin')
})

export const TittleContainer = styled(View, {
  marginRight: sp(12),
  paddingTop: sp(8),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between'
})
export const TittleListContainer = styled(View, {
  paddingTop: sp(8),
  ...container(),
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between'
})

export const CardSliderContainer = styled(View, {
  ...container(),
  paddingLeft: sp(12),
  marginBottom: sp(12),
  backgroundColor: color('surface'),
  borderRadius: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const CardListContainer = styled(View, {
  marginTop: sp(12),
  paddingBottom: sp(12),
  backgroundColor: color('surface'),
  borderRadius: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const AvatarText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('accent')
})

export const CategoryText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent')
})

export const PostText = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  color: color('text'),
  marginTop: sp(4)
})

export const SliderContainer = styled(View, {
  marginTop: sp(8),
  marginBottom: sp(16)
})

export const SliderView = styled(ScrollView, {
  flexDirection: 'row'
})

export const SlideAvatarContainer = styled(View, {
  borderRadius: sp(12),
  overflow: 'hidden',
  width: 106,
  height: 154,
  marginVertical: sp(8),
  marginRight: sp(8),
  position: 'relative'
})

export const SlideTouchable = TouchableOpacity

export const HeaderInputButton = styled(TouchableOpacity, {
  padding: sp(4)
})

export const FirstNameSlide = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('surface')
})

export const LastNameSlide = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('surface')
})

export const TittleSlide = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('surface')
})

export const InfoContainer = styled(View, {
  position: 'absolute',
  marginHorizontal: sp(4),
  paddingBottom: sp(12),
  bottom: 0
})

export const GradientContainer = styled(View, {
  position: 'absolute'
})

/* export const FollowPlusIcon = styled(PlusIcon, {
  color: color('accent'),
  width: sp(20),
  height: sp(20)
}) */

/* export const FollowContainer = styled(TouchableOpacity, {
  marginLeft: 'auto',
  paddingHorizontal: sp(12),
  paddingVertical: sp(16)
}) */

/* export const SubscribeButtonIcon = styled(SubscribeIcon, {
  width: sp(24),
  height: sp(24)
}) */

export const SearchContainer = ScrollView

export const SearchHeader = styled(View, {
  backgroundColor: color('surface'),
  ...Platform.select({
    android: {
      elevation: 5
    },
    ios: {
      borderBottomColor: 'rgb(242, 242, 242)',
      borderBottomWidth: 1
    }
  }),
  marginBottom: sp(12)
})

export const SearchHeaderContainer = styled(View, {
  marginLeft: sp(12),
  flexDirection: 'row',
  alignItems: 'center'
})

export const CancelButtonText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('subtext')
})

export const CancelButton = styled(TouchableOpacity, {
  paddingHorizontal: sp(8),
  paddingVertical: sp(16)
})

export const InputSearchContainer = styled(View, {
  ...container('padding'),
  flexDirection: 'row',
  alignItems: 'center',
  flex: 1,
  backgroundColor: color('background'),
  borderRadius: sp(24),
  height: sp(32)
})

export const Input = styled(TextInput, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('subtext'),
  paddingVertical: 0,
  marginHorizontal: sp(4),
  flex: 1
})

export const ClosePlusIcon = styled(CrossIcon16, {
  width: sp(12),
  height: sp(12),
  color: color('accent')
})

export const HeaderButtonContainer = styled(View, {
  ...container(),
  marginTop: sp(8),
  flexDirection: 'row'
})

export const HeaderButton = styled(TouchableOpacity, {
  paddingHorizontal: sp(8),
  paddingVertical: sp(8)
})

export const HeaderButtonTextActive = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('accent')
})

export const HeaderButtonText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('text')
})

export const EmptySearchContainer = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
  paddingVertical: sp(32)
})

export const EmptySearchText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('inactive')
})

export const FeedIconSvg = styled(FeedIcon24, {
  width: sp(32),
  height: sp(32),
  color: color('accent')
})

export const FooterIndent = styled(View, {
  marginBottom: sp(8)
})

export const EmptyAvatarIconContainer = styled(View, {
  position: 'absolute',
  flex: 1
})

export const UserPreviewRowContainer = styled(View, {
  backgroundColor: color('surface'),
  marginVertical: sp(4),
  paddingVertical: sp(4)
})

export const SearchImg = styled(SearchIcon24, {
  width: sp(16),
  height: sp(16),
  color: color('inactive')
})
