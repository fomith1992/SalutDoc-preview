
import React from 'react'
import { SliderGradient } from './slider-gradient'
import {
  CardSliderContainer,
  PostText,
  TittleContainer,
  CategoryText,
  SliderContainer,
  SlideTouchable,
  SliderView,
  SlideAvatarContainer,
  FirstNameSlide,
  LastNameSlide,
  InfoContainer,
  TittleSlide,
  GradientContainer
} from './search.style'
import { Avatar } from '../avatar/avatar.view'

export type PreviewSlideProps = {
  avatar?: string
  firstName: string
  lastName?: string
  description: string | null
  onPress?: () => void
}

export function PreviewSlide ({
  avatar,
  firstName,
  lastName,
  description,
  onPress
}: PreviewSlideProps
): React.ReactElement {
  return (
    <SlideTouchable onPress={onPress}>
      <SlideAvatarContainer>
        <Avatar url={avatar} shape='fill' />
        <GradientContainer>
          <SliderGradient />
        </GradientContainer>
        <InfoContainer>
          <FirstNameSlide>
            {firstName}
          </FirstNameSlide>
          {lastName != null
            ? (
              <LastNameSlide>
                {lastName}
              </LastNameSlide>
            )
            : null}
          <TittleSlide>
            {description}
          </TittleSlide>
        </InfoContainer>
      </SlideAvatarContainer>
    </SlideTouchable>
  )
}

export function PreviewSlider (props: {items: PreviewSlideProps[], title: string}): React.ReactElement {
  const { items, title } = props
  return (
    <CardSliderContainer>
      <TittleContainer>
        <CategoryText>
          Подборка
        </CategoryText>
      </TittleContainer>
      <PostText>
        {title}
      </PostText>
      <SliderContainer>
        <SliderView horizontal showsHorizontalScrollIndicator={false}>
          {items.map((item, index) => <PreviewSlide {...item} key={index} />)}
        </SliderView>
      </SliderContainer>
    </CardSliderContainer>
  )
}
