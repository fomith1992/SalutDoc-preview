import React from 'react'
import { LinearGradient, Stop, Svg, Rect } from 'react-native-svg'
import { color } from '../../style/color'
import { useMaterialized } from '../../style/styled'

export function SliderGradient (): React.ReactElement {
  const indicatorColor = useMaterialized(color('accent'))
  return (
    <Svg height='154' width='106'>
      <LinearGradient id='grad' x1='0' y1='0' x2='0' y2='1'>
        <Stop offset='0' stopColor={indicatorColor} stopOpacity='0' />
        <Stop offset='0.5' stopColor={indicatorColor} stopOpacity='0' />
        <Stop offset='0.64' stopColor={indicatorColor} stopOpacity='0.2' />
        <Stop offset='1' stopColor={indicatorColor} stopOpacity='1' />
      </LinearGradient>
      <Rect width='106' height='154' fill='url(#grad)' />
    </Svg>
  )
}
