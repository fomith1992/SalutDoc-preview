import React from 'react'
import {
  /* PostText,
  CardListContainer,
  TittleListContainer,
  SearchContainer,
  SearchListContainer, */
  HeaderButton,
  HeaderButtonTextActive,
  HeaderButtonText,
  HeaderButtonContainer,
  SearchHeader,
  Input,
  InputSearchContainer,
  ClosePlusIcon,
  SearchHeaderContainer,
  CancelButtonText,
  CancelButton,
  HeaderInputButton,
  EmptySearchContainer,
  EmptySearchText,
  FeedIconSvg,
  FooterIndent,
  SearchImg
} from './search.style'
/* import { List, ListItemProps } from '../list/list.view' */
import { SafeAreaView, FlatList } from 'react-native'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { SearchedItemType } from '../../gen/graphql'

export type SearchItem = PostItem | ArticleItem | UserItem | QuestionItem | CommunityItem

type PostItem = {
  id: string
  type: 'POST'
}
type ArticleItem = {
  id: string
  type: 'ARTICLE'
}
type UserItem = {
  id: string
  type: 'USER'
}
type QuestionItem = {
  id: string
  type: 'QUESTION'
}
type CommunityItem = {
  id: string
  type: 'COMMUNITY'
}

type SearchViewProps = {
  content: SearchItem[]
  loading: boolean
  refreshing: boolean
  refresh: () => void
  renderItem: (item: SearchItem) => React.ReactElement
  handleCancelPressed: () => void
  onSubmit: (query: string) => void
  usersSearchData: React.ReactElement
  communitiesSearchData: React.ReactElement
  query: string
  setSearchedType: (searchedType: SearchedItemType | null) => void
  searchedType: SearchedItemType | null
}

interface MenuTabsItems {
  name: string
  value: SearchedItemType | null
}

export function SearchView (props: SearchViewProps): React.ReactElement {
  const {
    query,
    content,
    loading,
    refreshing,
    refresh,
    setSearchedType,
    searchedType = null,
    onSubmit,
    renderItem,
    handleCancelPressed,
    usersSearchData,
    communitiesSearchData
  } = props

  const menuTabs: MenuTabsItems[] = [
    { name: 'Все', value: null },
    { name: 'Работы', value: 'ARTICLE' },
    { name: 'Посты', value: 'POST' },
    { name: 'Вопросы', value: 'QUESTION' },
    { name: 'Люди', value: 'USER' },
    { name: 'Группы', value: 'COMMUNITY' }
  ]

  return (
    <SafeAreaView
      style={{ flex: 1 }}
    >
      <SearchHeader>
        <SearchHeaderContainer>
          <InputSearchContainer>
            <SearchImg />
            <Input
              placeholder='Поиск'
              value={query}
              onChangeText={onSubmit}
            />
            {query == null || query === ''
              ? null
              : (
                <HeaderInputButton
                  onPress={() => onSubmit('')}
                >
                  <ClosePlusIcon />
                </HeaderInputButton>)}
          </InputSearchContainer>
          <CancelButton onPress={handleCancelPressed}>
            <CancelButtonText>
              Отмена
            </CancelButtonText>
          </CancelButton>
        </SearchHeaderContainer>
        <HeaderButtonContainer>
          {menuTabs.map((tab, index) => {
            return (
              searchedType === tab.value ? (
                <HeaderButton key={index}>
                  <HeaderButtonTextActive>
                    {tab.name}
                  </HeaderButtonTextActive>
                </HeaderButton>
              ) : (
                <HeaderButton
                  key={index}
                  onPress={() => {
                    setSearchedType(tab.value)
                  }}
                >
                  <HeaderButtonText>
                    {tab.name}
                  </HeaderButtonText>
                </HeaderButton>
              ))
          })}
        </HeaderButtonContainer>
      </SearchHeader>
      {/* <SearchContainer>
        <SearchListContainer>
          <CardListContainer>
            <TittleListContainer>
              <CategoryText>
                Группы
              </CategoryText>
            </TittleListContainer>
            <List items={listDataCommunities} />
          </CardListContainer>
          <CardListContainer>
            <TittleListContainer>
              <CategoryText>
                Люди
              </CategoryText>
            </TittleListContainer>
            <List items={listDataUsers} />
      </CardListContainer>
          <CardSliderContainer>
            <TittleContainer>
              <CategoryText>
                Подборка
              </CategoryText>
            </TittleContainer>
            <PostText>
              Подборка интересных сообществ
            </PostText>
            <PreviewSlider items={sliderDataCommunities} />
          </CardSliderContainer>
        </SearchListContainer>
      </SearchContainer> */}
      <FlatList
        data={content}
        renderItem={({ item }) => renderItem(item)}
        keyExtractor={item => item.id}
        ListHeaderComponent={
          <>
            {usersSearchData}
            {communitiesSearchData}
          </>
        }
        ListEmptyComponent={loading ? null : (
          <EmptySearchContainer>
            <FeedIconSvg />
            <EmptySearchText>
              {query === '' || query == null ? 'Что-то ищете?' : 'Поиск ничего не дал'}
            </EmptySearchText>
          </EmptySearchContainer>
        )}
        ListFooterComponent={
          loading
            ? <LoadingIndicator visible={loading} />
            : <FooterIndent />
        }
        refreshing={refreshing}
        onRefresh={refresh}
      />
    </SafeAreaView>
  )
}
