import React, { useState } from 'react'
import { SearchedItemType } from '../../gen/graphql'
import { SmallUserFollowButton } from '../../modules/user-follow'
import { FollowCommunityButton } from '../follow-community-button/follow-community-button.container'
import { FollowCommunityButtonSmall } from '../follow-community-button/follow-community-button.small.view'
import { WorkPreview } from '../portfolio/work-prewiew/work-prewiew.container'
import { PostCard } from '../../modules/posts'
import { Question } from '../question/question-card/question.container'
import { CommunityPreviewRow } from '../preview-row/community-preview-row.container'
import { UserPreviewRow } from '../preview-row/user-preview-row.container'
import { UserPreviewRowContainer } from './search.style'
import { SearchItem } from './search.view'
import { SearchComposer } from './search-composer.container'

export interface SearchProps {
  type: SearchedItemType | null
}

export function Search ({ type }: SearchProps): React.ReactElement {
  const [query, setQuery] = useState<string>('')
  const [searchedType, setSearchedType] = useState<SearchedItemType | null>(type)

  const renderItem = (props: SearchItem): React.ReactElement => {
    switch (props.type) {
      case 'POST': {
        return <PostCard id={props.id} />
      }
      case 'ARTICLE': {
        return <WorkPreview id={props.id} />
      }
      case 'QUESTION': {
        return <Question id={props.id} />
      }
      case 'USER': {
        return (
          <UserPreviewRowContainer>
            <UserPreviewRow userId={props.id} action={<SmallUserFollowButton followeeId={props.id} />} />
          </UserPreviewRowContainer>
        )
      }
      case 'COMMUNITY': {
        return (
          <UserPreviewRowContainer>
            <CommunityPreviewRow
              communityId={props.id}
              action={
                <FollowCommunityButton
                  communityId={props.id}
                  view={FollowCommunityButtonSmall}
                />
              }
            />
          </UserPreviewRowContainer>
        )
      }
      default: {
        return <></>
      }
    }
  }
  return (
    <SearchComposer
      query={query}
      searchedType={searchedType}
      setSearchedType={setSearchedType}
      setQuery={setQuery}
      renderItem={renderItem}
    />
  )
}
