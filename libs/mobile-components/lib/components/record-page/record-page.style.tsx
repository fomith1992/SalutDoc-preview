import { TouchableOpacity, View, Text, ScrollView } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { ElasticText } from '../elastic-text/elastic-text'

export const Container = styled(View, {
  flex: 1
})

export const MainRow = styled(View, {
  ...container(),
  marginTop: sp(24),
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarContainer = styled(TouchableOpacity, {
  width: sp(56)
})

export const InfoContainer = styled(View, {
  flex: 1,
  marginLeft: sp(16)
})

export const Name = styled(ElasticText, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const Specialization = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext')
})

export const ChoosingBlock = styled(View, {
  marginTop: sp(24)
})

export const Title = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginLeft: sp(16)
})

export const ScrollBlock = styled(ScrollView, {
  marginTop: sp(12),
  flexDirection: 'row'
})

export const Item = styled(TouchableOpacity, (props: {
  isSelected: boolean
}) => ({
  paddingVertical: sp(8),
  paddingHorizontal: sp(12),
  borderRadius: sp(4),
  borderWidth: 1,
  borderColor: color('accent'),
  marginHorizontal: sp(8),
  backgroundColor: props.isSelected ? color('accent') : undefined
}) as const)

export const Description = styled(Text, (props: {isSelected: boolean}) => ({
  ...font({ type: 'h4' }),
  color: color(props.isSelected ? 'surface' : 'text')
}) as const)

export const ButtonContainer = styled(View, {
  ...container(),
  marginBottom: sp(24),
  marginTop: 'auto'
})

export const Spring = styled(View, {
  width: 8,
  height: 1
})
