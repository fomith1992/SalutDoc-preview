import React from 'react'
import { useRecordPageDataQuery } from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { RecordPageView } from './record-page.view'

interface RecordPageProps {
  userId: string
}

const testTimeArray = ['18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00']

const testDateArray = [
  {
    title: '12.04, пн',
    includesTime: testTimeArray
  },
  {
    title: '13.04, вт',
    includesTime: testTimeArray
  },
  {
    title: '14.04, ср',
    includesTime: testTimeArray
  },
  {
    title: '15.04, чт',
    includesTime: testTimeArray
  },
  {
    title: '16.04, пт',
    includesTime: testTimeArray
  },
  {
    title: '17.04, пт',
    includesTime: testTimeArray
  },
  {
    title: '18.04, сб',
    includesTime: testTimeArray
  }
]

export function RecordPage ({ userId }: RecordPageProps): React.ReactElement {
  const { data, loading } = useRecordPageDataQuery({
    variables: {
      userId
    }
  })

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.userById == null) {
    return <></>
  }

  return (
    <RecordPageView
      userData={data.userById}
      doctorAppointmentDates={testDateArray}
    />
  )
}
