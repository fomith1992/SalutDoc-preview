import React, { useState } from 'react'
import { TextButton } from '../../modules/ui-kit'
import { Avatar } from '../avatar/avatar.view'
import { DividerView } from '../divider/divider.view'
import {
  Container,
  MainRow,
  AvatarContainer,
  InfoContainer,
  Name,
  Specialization,
  ChoosingBlock,
  Title,
  ScrollBlock,
  Item,
  Description,
  ButtonContainer,
  Spring
} from './record-page.style'

interface DoctorAppointmentDates {
  title: string
  includesTime: string[]
}

interface RecordPageViewProps {
  userData: {
    firstName: string
    lastName: string | null
    specialization: string | null
    avatar: string | null
  }
  doctorAppointmentDates: DoctorAppointmentDates[]
}

export function RecordPageView ({
  userData,
  doctorAppointmentDates
}: RecordPageViewProps): React.ReactElement {
  const [appointmentDate, setAppointmentDate] = useState<DoctorAppointmentDates | null>(null)
  const [appointmentTime, setAppointmentTime] = useState<string | null>(null)
  return (
    <Container>
      <MainRow>
        <AvatarContainer>
          <Avatar
            url={userData.avatar}
            shape='circle'
          />
        </AvatarContainer>
        <InfoContainer>
          <Name numberOfLines={2}>
            {`${userData.firstName} ${userData.lastName ?? ''}`}
          </Name>
          <Specialization>
            {userData.specialization}
          </Specialization>
        </InfoContainer>
      </MainRow>
      <DividerView marginTop={24} />
      <ChoosingBlock>
        <Title>
          Выберите день
        </Title>
        <ScrollBlock
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          <Spring />
          {doctorAppointmentDates.map((date, index) => {
            return (
              <Item
                isSelected={date.title === appointmentDate?.title}
                key={index}
                onPress={() => setAppointmentDate(date)}
              >
                <Description isSelected={date.title === appointmentDate?.title}>
                  {date.title}
                </Description>
              </Item>
            )
          })}
          <Spring />
        </ScrollBlock>
      </ChoosingBlock>
      {appointmentDate != null
        ? (
          <ChoosingBlock>
            <Title>
              Выберите время
            </Title>
            <ScrollBlock
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              <Spring />
              {appointmentDate.includesTime.map((time, index) => {
                return (
                  <Item
                    isSelected={time === appointmentTime}
                    key={index}
                    onPress={() => setAppointmentTime(time)}
                  >
                    <Description
                      isSelected={time === appointmentTime}
                    >
                      {time}
                    </Description>
                  </Item>
                )
              })}
              <Spring />
            </ScrollBlock>
          </ChoosingBlock>
        ) : null}
      <ButtonContainer>
        <TextButton
          disabled={appointmentTime == null}
          size='XL'
        >
          Далее
        </TextButton>
      </ButtonContainer>
    </Container>
  )
}
