import { Text, TouchableOpacity, View } from 'react-native'
import { DislikeFingerIcon } from '../../images/dislike-finger.icon'
import { color } from '../../style/color'
import { styled, variants } from '../../style/styled'
import { font } from '../../style/text'
import { LikeFilledIcon24 } from '../../images/like-filled.icon-24'
import { sp } from '../../style/size'
import { LikeFingerIcon } from '../../images/like-finger.icon'
import { LikeIcon20 } from '../../images/like.icon-20'

export const LikeButtonContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center'
})

export const LikeButton = styled(TouchableOpacity, {
  paddingRight: sp(8),
  paddingVertical: sp(12),
  alignItems: 'center',
  flexDirection: 'row'
})

const LikeButtonCountVariants = styled(Text, variants('type', {
  normal: font({ type: 'h3', weight: 'light' }),
  work: font({ type: 'h2', weight: 'light' }),
  titleWork: font({ type: 'h2', weight: 'light' })
}))

export const LikeButtonCount = styled(LikeButtonCountVariants, (props: { isPressed: boolean }) => ({
  color: color(props.isPressed ? 'accent' : 'inactive'),
  textAlign: 'center',
  marginLeft: sp(4)
}) as const)

export const LikeFilledSvg = styled(LikeFilledIcon24, variants('type', {
  normal: {
    width: sp(20),
    height: sp(20)
  },
  work: {
    width: sp(24),
    height: sp(24)
  },
  titleWork: {
    width: sp(24),
    height: sp(24)
  }
}), { color: color('accent') })

export const LikeSvg = styled(LikeIcon20, variants('type', {
  normal: {
    width: sp(20),
    height: sp(20)
  },
  work: {
    width: sp(24),
    height: sp(24)
  },
  titleWork: {
    width: sp(24),
    height: sp(24)
  }
}), { color: color('inactive') })

export const LikeFingerButtonSvg = styled(LikeFingerIcon, {
  width: sp(16),
  height: sp(16)
})

export const DislikeFingerButtonSvg = styled(DislikeFingerIcon, {
  width: sp(16),
  height: sp(16)
})

export const LikeFingerButton = styled(TouchableOpacity, {
  padding: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const LikeFingerButtonCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'h3', weight: 'light' }),
  color: props.isPressed ? color('accent') : color('subtext'),
  lineHeight: sp(16),
  textAlign: 'center',
  marginLeft: sp(4)
}) as const)
