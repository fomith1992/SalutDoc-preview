import React from 'react'
import { LikeAnswerButtonViewStateProps, LikeButtonViewStateProps } from './like.container'
import {
  DislikeFingerButtonSvg,
  LikeButton,
  LikeSvg,
  LikeFilledSvg,
  LikeButtonCount,
  LikeFingerButtonSvg,
  LikeFingerButtonCount,
  LikeFingerButton,
  LikeButtonContainer
} from './like.style'

export interface LikeButtonViewProps extends LikeButtonViewStateProps {
  type?: 'normal' | 'work' | 'titleWork'
  hideLikesCount?: boolean
  dislike?: boolean
}

export function LikeButtonView ({
  isLiked,
  likeCount,
  type = 'normal',
  hideLikesCount = false,
  handleLikePressed
}: LikeButtonViewProps): React.ReactElement {
  return (
    <LikeButton onPress={() => handleLikePressed(!isLiked)}>
      {isLiked
        ? <LikeFilledSvg type={type} />
        : <LikeSvg type={type} />}
      <LikeButtonCount
        isPressed={isLiked}
        type={type}
      >
        {!hideLikesCount && likeCount > 0 ? likeCount : ''}
      </LikeButtonCount>
    </LikeButton>
  )
}

export function LikeButtonAnswerView ({
  isLiked,
  likeCount,
  dislikeCount,
  handleLikePressed
}: LikeAnswerButtonViewStateProps): React.ReactElement {
  return (
    <LikeButtonContainer>
      <LikeFingerButton onPress={() => handleLikePressed(isLiked === 'LIKED' ? 'NEUTRAL' : 'LIKED')}>
        <LikeFingerButtonSvg
          isPressed={isLiked === 'LIKED'}
        />
        <LikeFingerButtonCount
          isPressed={isLiked === 'LIKED'}
        >
          {likeCount > 0 ? likeCount : ''}
        </LikeFingerButtonCount>
      </LikeFingerButton>
      <LikeFingerButton onPress={() => handleLikePressed(isLiked === 'DISLIKED' ? 'NEUTRAL' : 'DISLIKED')}>
        <DislikeFingerButtonSvg
          isPressed={isLiked === 'DISLIKED'}
        />
        <LikeFingerButtonCount
          isPressed={isLiked === 'DISLIKED'}
        >
          {dislikeCount > 0 ? dislikeCount : ''}
        </LikeFingerButtonCount>
      </LikeFingerButton>
    </LikeButtonContainer>
  )
}
