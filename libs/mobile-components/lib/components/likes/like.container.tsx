import React, { useEffect, useMemo } from 'react'
import { StackActions } from '@react-navigation/native'
import {
  usePostLikeInfoQuery,
  useSetPostLikedMutation,
  useSetWorkLikedMutation,
  useWorkLikeInfoQuery,
  useSetQuestionLikedMutation,
  useQuestionLikeInfoQuery,
  useSetAnswerLikedMutation,
  useAnswerLikeInfoQuery,
  LikeState
} from '../../gen/graphql'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'

export interface LikeProps {
  id: string
  render: RenderLikeButtonView
}

export type RenderLikeButtonView = (props: LikeButtonViewStateProps) => React.ReactElement

export interface LikeButtonViewStateProps {
  isLiked: boolean
  likeCount: number
  handleLikePressed: (isLiked: boolean) => void
}

export interface LikeAnswerProps {
  id: string
  render: RenderLikeAnswerButtonView
}

export type RenderLikeAnswerButtonView = (props: LikeAnswerButtonViewStateProps) => React.ReactElement

export interface LikeAnswerButtonViewStateProps {
  isLiked: LikeState
  likeCount: number
  dislikeCount: number
  handleLikePressed: (isLiked: LikeState) => void
}

export function LikePost ({
  id,
  render
}: LikeProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useStackNavigation()
  const [mutateLiked] = useSetPostLikedMutation()
  const { data, error } = usePostLikeInfoQuery({
    variables: { id }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  const likesCountWithoutMy = useMemo(
    () => {
      if (data?.postById != null) {
        return data.postById.likesCount - (data.postById.isLikedByUser === true ? 1 : 0)
      } else {
        return 0
      }
    },
    [data?.postById?.id]
  )
  const handleLikePressed = (liked: boolean): void => {
    if (user == null) {
      return navigation.dispatch(StackActions.push('CreateAccountCard', {}))
    }
    mutateLiked({
      variables: { postId: id, liked }
    })
      .then(res => {
        const errors = res.data?.postSetLiked.errors
        if (errors == null || errors.length > 0) {
          console.log(errors)
        }
      })
      .catch(console.log)
  }

  if (data?.postById != null) {
    const { postById: post } = data
    const likeCount = likesCountWithoutMy + (post.isLikedByUser === true ? 1 : 0)
    return render({
      isLiked: post.isLikedByUser ?? false,
      likeCount,
      handleLikePressed
    })
  } else {
    return <></>
  }
}

export function LikeWork (
  {
    id,
    render
  }: LikeProps
): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useStackNavigation()
  const [mutateLiked] = useSetWorkLikedMutation()
  const { data, error } = useWorkLikeInfoQuery({
    variables: { id }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  const likesCountWithoutMy = useMemo(
    () => {
      if (data?.workById != null) {
        return data.workById.likesCount - (data.workById.isLikedByUser === true ? 1 : 0)
      } else {
        return 0
      }
    },
    [data?.workById?.id]
  )
  const handleLikePressed = (liked: boolean): void => {
    if (user == null) {
      return navigation.dispatch(StackActions.push('CreateAccountCard', {}))
    }
    mutateLiked({
      variables: { workId: id, liked }
    })
      .then(res => {
        const errors = res.data?.workSetLiked.errors
        if (errors == null || errors.length > 0) {
          console.log(errors)
        }
      })
      .catch(console.log)
  }

  if (data?.workById != null) {
    const { workById: work } = data
    const likeCount = likesCountWithoutMy + (work.isLikedByUser === true ? 1 : 0)
    return render({
      isLiked: work.isLikedByUser ?? false,
      likeCount,
      handleLikePressed
    })
  } else {
    return <></>
  }
}

export function LikeQuestion (
  {
    id,
    render
  }: LikeProps
): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useStackNavigation()
  const [mutateLiked] = useSetQuestionLikedMutation()
  const { data, error } = useQuestionLikeInfoQuery({
    variables: { questionId: id }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  const likesCountWithoutMy = useMemo(
    () => {
      if (data?.questionById != null) {
        return data.questionById.likesCount - (data.questionById.isLikedByUser === true ? 1 : 0)
      } else {
        return 0
      }
    },
    [data?.questionById?.id]
  )
  const handleLikePressed = (liked: boolean): void => {
    if (user == null) {
      return navigation.dispatch(StackActions.push('CreateAccountCard', {}))
    }
    mutateLiked({
      variables: { questionId: id, liked }
    })
      .then(res => {
        const errors = res.data?.questionSetLiked.errors
        if (errors == null || errors.length > 0) {
          console.log(errors)
        }
      })
      .catch(console.log)
  }

  if (data?.questionById != null) {
    const { questionById: work } = data
    const likeCount = likesCountWithoutMy + (work.isLikedByUser === true ? 1 : 0)
    return render({
      isLiked: work.isLikedByUser ?? false,
      likeCount,
      handleLikePressed
    })
  } else {
    return <></>
  }
}
export function LikeAnswer (
  {
    id,
    render
  }: LikeAnswerProps
): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useStackNavigation()
  const [mutateLiked] = useSetAnswerLikedMutation()
  const { data, error } = useAnswerLikeInfoQuery({
    variables: { answerId: id }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  const handleLikePressed = (liked: LikeState = 'NEUTRAL'): void => {
    if (user == null) {
      return navigation.dispatch(StackActions.push('CreateAccountCard', {}))
    }
    mutateLiked({
      variables: { answerId: id, state: liked }
    })
      .then(res => {
        const errors = res.data?.answerSetLiked.errors
        if (errors == null || errors.length > 0) {
          console.log(errors)
        }
      })
      .catch(console.log)
  }

  if (data?.answerById != null) {
    const { answerById: answer } = data
    const likeCount = data.answerById.likesCount ?? 0
    const dislikeCount = data.answerById.dislikesCount ?? 0
    return render({
      isLiked: answer.likeStateByUser ?? 'NEUTRAL',
      dislikeCount,
      likeCount,
      handleLikePressed
    })
  } else {
    return <></>
  }
}
