import React from 'react'
import {
  ButtonsContaner,
  Centered,
  Container,
  InfoText,
  LeftButtonContainer,
  TitleText,
  UserIcon
} from './create-account-card.style'
import { TextButton } from '../../modules/ui-kit'

export interface CreateAccountCardViewProps {
  onRegister: () => void
  onLogin: () => void
}

export function CreateAccountCardView (
  {
    onRegister,
    onLogin
  }: CreateAccountCardViewProps
): React.ReactElement {
  return (
    <Container>
      <Centered>
        <UserIcon />
        <TitleText>
          Создайте аккаунт
        </TitleText>
        <InfoText>
          Для того, чтобы ставить лайки, получать {'\n'} консультации и удобно хранить документы
        </InfoText>
        <ButtonsContaner>
          <LeftButtonContainer>
            <TextButton
              size='L'
              type='primary'
              onPress={onRegister}
            >
              Создать аккаунт
            </TextButton>
          </LeftButtonContainer>
          <TextButton
            size='L'
            type='secondary'
            onPress={onLogin}
          >
            Войти
          </TextButton>
        </ButtonsContaner>
      </Centered>
    </Container>
  )
}
