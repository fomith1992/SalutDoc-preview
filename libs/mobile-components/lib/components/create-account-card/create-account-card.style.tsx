import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { ProfileIcon56 } from '../../images/profile.icon-56'

export const Container = styled(View, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface')
})

export const Centered = styled(View, {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center'
})

export const UserIcon = styled(ProfileIcon56, {
  color: color('accent')
})

export const TitleText = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('text'),
  marginTop: sp(20),
  textAlign: 'center'
})

export const InfoText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(12),
  textAlign: 'center'
})

export const ButtonsContaner = styled(View, {
  marginTop: sp(24),
  flexDirection: 'row'
})

export const LeftButtonContainer = styled(View, {
  marginRight: sp(16)
})
