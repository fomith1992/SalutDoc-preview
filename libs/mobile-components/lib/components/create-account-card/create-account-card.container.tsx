import React from 'react'
import { StackActions } from '@react-navigation/native'
import { CreateAccountCardView } from './create-account-card.view'
import { useRootStore } from '../../stores/root.store'
import { useStackNavigation } from '../../navigation/use-stack-navigation'

export function CreateAccountCard (): React.ReactElement {
  const navigation = useStackNavigation()
  const {
    setMode
  } = useRootStore().domains.registerReport
  return (
    <CreateAccountCardView
      onLogin={() => navigation.dispatch(StackActions.push('Login_Stack', {}))}
      onRegister={() => {
        setMode('DEFAULT')
        navigation.dispatch(StackActions.push('Register_PatientStack', {}))
      }}
    />
  )
}
