import React from 'react'
import { useCreateCommunityMutation } from '../../gen/graphql'
import { CreateCommunityView } from './create-community.view'
import { useNavigation, StackActions } from '@react-navigation/native'
import { Alert } from 'react-native'

export function CreateCommunity (): React.ReactElement {
  const navigation = useNavigation()
  const [createCommunity, { loading: submittingCreateCommunity }] = useCreateCommunityMutation({
    onCompleted ({ communityCreate: { errors, message: dbgMsg, community } }) {
      console.log(dbgMsg, errors, community)
      if (community == null) {
        Alert.alert('Ошибка при создании сообщетва')
      } else {
        console.log('Сообщество создано успешно!')
        navigation.dispatch(
          StackActions.replace('Community', {
            communityId: community.id
          }))
      }
    }
  })

  return (
    <CreateCommunityView
      submitting={submittingCreateCommunity}
      onSubmit={input => {
        createCommunity({ variables: { input } }).catch(() => {
          Alert.alert('Ошибка при создании сообщества')
        })
        return {}
      }}
    />
  )
}
