import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { SelectInput, TextInput, yup } from '../../modules/form-kit'
import { SubmitTextButton } from '../../modules/ui-kit'
import { ModalLoadingIndicator } from '../loading-indicator/modal-loading-indicator.view'
import { Container, FieldContainer, SubmitContainer, Spring } from './create-community.style'

const nameMaxLength = 100
const specializationMaxLength = 100
const descriptionMaxLength = 1000

const formSchema = yup.object({
  name: yup.string()
    .required()
    .max(nameMaxLength),

  specialization: yup.string()
    .required()
    .max(specializationMaxLength),

  description: yup.string()
    .required()
    .max(descriptionMaxLength),

  type: yup.mixed()
    .required()
    .oneOf(['PUBLIC', 'PRIVATE'] as const)
}).required()

export type EditCommunityFormData = yup.InferType<typeof formSchema>

export interface EditCommunityFormProps {
  onSubmit: SubmissionHandler<EditCommunityFormData>
  submitting: boolean
}

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export const CreateCommunityView = (props: EditCommunityFormProps): React.ReactElement => {
  return (
    <Form
      onSubmit={props.onSubmit}
    >
      <Container>
        <FieldContainer>
          <Field name='name'>
            {props => (
              <TextInput
                {...props}
                label='Название сообщества'
                maxLength={nameMaxLength}
              />
            )}
          </Field>
        </FieldContainer>
        <FieldContainer>
          <Field name='specialization'>
            {props => (
              <TextInput
                {...props}
                label='Специализация'
                maxLength={specializationMaxLength}
              />
            )}
          </Field>
        </FieldContainer>
        <FieldContainer>
          <Field name='description'>
            {props => (
              <TextInput
                {...props}
                label='Описание'
                maxLength={descriptionMaxLength}
                numberOfLines={[2, 4]}
              />
            )}
          </Field>
        </FieldContainer>
        <FieldContainer>
          <Field name='type'>
            {props => (
              <SelectInput
                {...props}
                label='Тип'
                options={[
                  { label: 'Публичное', value: 'PUBLIC' },
                  { label: 'Приватное', value: 'PRIVATE' }
                ]}
              />
            )}
          </Field>
        </FieldContainer>
        <Spring />
        <SubmitContainer>
          <Submit>
            {props =>
              <SubmitTextButton
                type='primary'
                size='XL'
                {...props}
              >
                Создать группу
              </SubmitTextButton>}
          </Submit>
        </SubmitContainer>
        <ModalLoadingIndicator visible={props.submitting} />
      </Container>
    </Form>
  )
}
