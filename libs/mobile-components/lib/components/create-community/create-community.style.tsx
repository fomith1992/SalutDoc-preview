import { View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { container } from '../../style/view'

export const Container = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  flex: 1,
  paddingBottom: sp(8)
})

export const FieldContainer = styled(View, {
  marginTop: sp(12)
})

export const SubmitContainer = styled(View, {
  marginTop: sp(12),
  marginBottom: sp(8)
})

export const Spring = styled(View, {
  flex: 1
})
