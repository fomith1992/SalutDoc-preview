import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { useQuestionsByUserIdQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { SmallUserFollowButton } from '../../../modules/user-follow'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { UserPreviewRow } from '../../preview-row/user-preview-row.container'
import { useAuthenticatedUserOrNull } from '../../user-context/user-context-provider'
import { Question } from '../question-card/question.container'
import { QuestionsView } from './questions.view'

interface QuestionsProps {
  userId: string
}
export function Questions ({ userId }: QuestionsProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const { data, loading, fetchMore, refetch } = useQuestionsByUserIdQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      userId: userId
    }
  })
  const { refreshing, refresh } = useRefresh(refetch)

  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.userById?.questions?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousQuestions = expectDefined(previousResult.userById?.questions)
        return {
          userById: {
            ...expectDefined(previousResult.userById),
            questions: appendEdges(previousQuestions, fetchMoreResult?.userById?.questions)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.userById?.questions?.pageInfo.endCursor])

  if (loading && data?.userById == null) {
    return <LoadingIndicator visible />
  }

  if (data?.userById == null) {
    return <>Произошла ошибка</>
  }
  return (
    <QuestionsView
      renderQuestion={(id) => <Question id={id} />}
      questionsData={data.userById.questions?.edges.map(({ node }) => {
        return { id: node.id }
      }) ?? []}
      author={
        <UserPreviewRow
          userId={data.userById.id}
          action={user?.id !== data.userById.id ? <SmallUserFollowButton followeeId={data.userById.id} /> : <></>}
        />
      }
      loading={loading && !refreshing}
      hasMore={data.userById.questions?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore()}
      refreshing={refreshing}
      refresh={refresh}
    />
  )
}
