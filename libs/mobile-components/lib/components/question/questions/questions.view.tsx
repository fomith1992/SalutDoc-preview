import React from 'react'
import { FlatList } from 'react-native'
import { ScreenBlock } from '../../../modules/ui-kit/screen-block'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { FooterContainer, QuestionsContainer } from './questions.style'

interface QuestionsViewProps {
  questionsData: Array<{
    id: string
  }>
  author: React.ReactElement
  loading: boolean
  hasMore: boolean
  loadMore: () => void
  refreshing: boolean
  refresh: () => void
  renderQuestion: (id: string) => React.ReactElement
}
export function QuestionsView ({
  renderQuestion,
  questionsData,
  author,
  loading,
  hasMore,
  loadMore,
  refreshing,
  refresh
}: QuestionsViewProps): React.ReactElement {
  return (
    <FlatList
      ListHeaderComponent={
        <ScreenBlock>
          <QuestionsContainer>
            {author}
          </QuestionsContainer>
        </ScreenBlock>
      }
      data={questionsData}
      renderItem={({ item }) => renderQuestion(item.id)}
      keyExtractor={item => item.id}
      onEndReached={hasMore ? loadMore : null}
      ListFooterComponent={
        <FooterContainer>
          <LoadingIndicator visible={loading} />
        </FooterContainer>
      }
      refreshing={refreshing}
      onRefresh={refresh}
    />
  )
}
