import { View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const QuestionsContainer = styled(View, {
  backgroundColor: color('surface'),
  paddingVertical: sp(8)
})

export const FooterContainer = styled(View, {
  marginBottom: sp(8)
})
