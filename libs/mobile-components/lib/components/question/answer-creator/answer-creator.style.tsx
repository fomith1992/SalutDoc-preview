import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { sp } from '../../../style/size'
import { View } from 'react-native'

export const AnswerCreatorContainer = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})

export const AnswerCreatorFooter = View

export const PublishButtonContainer = styled(View, {
  marginLeft: 'auto',
  marginRight: sp(8),
  marginBottom: sp(16)
})
