import React from 'react'
import { PostTextInput, yup } from '../../../modules/form-kit'
import { SubmitTextButton } from '../../../modules/ui-kit'
import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import {
  PublishButtonContainer,
  AnswerCreatorContainer,
  AnswerCreatorFooter
} from './answer-creator.style'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'

const formSchema = yup.object({
  text: yup.string()
    .required()
}).required()

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export type AnswerCreatorFormData = yup.InferType<typeof formSchema>

export interface AnswerCreatorViewProps {
  onSubmit: SubmissionHandler<AnswerCreatorFormData>
  submitting: boolean
}

export const AnswerCreatorView = ({ onSubmit, submitting }: AnswerCreatorViewProps): React.ReactElement => {
  return (
    <Form
      onSubmit={onSubmit}
    >
      <AnswerCreatorContainer>
        <Field name='text'>
          {propsText => (
            <PostTextInput {...propsText} placeholder='Добавьте ответ на вопрос' />
          )}
        </Field>
        <AnswerCreatorFooter>
          <PublishButtonContainer>
            <Submit>
              {props => (
                <SubmitTextButton
                  type='primary'
                  {...props}
                >
                  Отправить
                </SubmitTextButton>
              )}
            </Submit>
          </PublishButtonContainer>
        </AnswerCreatorFooter>
      </AnswerCreatorContainer>
      <ModalLoadingIndicator visible={submitting} />
    </Form>
  )
}
