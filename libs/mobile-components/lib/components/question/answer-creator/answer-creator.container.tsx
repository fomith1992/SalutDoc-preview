import React from 'react'
import { useNavigation } from '@react-navigation/native'
import {
  useAnswerCreateMutation,
  QuestionByIdQuery,
  QuestionByIdQueryVariables,
  QuestionByIdDocument
} from '../../../gen/graphql'
import { AnswerCreatorView } from './answer-creator.view'
import { Alert } from 'react-native'

interface AnswerCreatorProps {
  questionId: string
}
export function AnswerCreator ({ questionId }: AnswerCreatorProps): React.ReactElement {
  const navigation = useNavigation()

  const [answerCreateMutation, { loading: submittingAnswer }] = useAnswerCreateMutation({
    onCompleted: (response) => {
      const { answer, message, errors } = response.answerCreate
      if (answer == null) {
        console.log('Не удалось создать запись', message, errors)
        Alert.alert('Ошибка', 'Не удалось создать запись')
      } else {
        console.log('Запись создана', message, answer)
        navigation.goBack()
      }
    },
    update (cache, { data }) {
      const newAnswer = data?.answerCreate.answer
      if (newAnswer == null) {
        return
      }
      let question = null
      try {
        question = cache.readQuery<QuestionByIdQuery, QuestionByIdQueryVariables>({
          query: QuestionByIdDocument,
          variables: {
            id: questionId,
            after: null
          }
        })
      } catch {
      }
      if (question?.questionById?.answers != null) {
        cache.writeQuery<QuestionByIdQuery, QuestionByIdQueryVariables>({
          query: QuestionByIdDocument,
          variables: {
            id: questionId,
            after: null
          },
          data: {
            ...question,
            questionById: {
              ...question.questionById,
              answers: {
                ...question.questionById.answers,
                edges: [
                  { node: newAnswer, __typename: 'AnswerEdge' },
                  ...question.questionById.answers.edges
                ]
              }
            }
          }
        })
      }
    }
  })

  return (
    <AnswerCreatorView
      submitting={submittingAnswer}
      onSubmit={async content => {
        await answerCreateMutation({
          variables: {
            questionId,
            content
          }
        })
        return {}
      }}
    />
  )
}
