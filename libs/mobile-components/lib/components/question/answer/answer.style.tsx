import { Text, TouchableOpacity, View } from 'react-native'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { ElasticText } from '../../elastic-text/elastic-text'

export const QuestionMain = styled(View, {
  ...container()
})

export const QuestionReplyText = styled(ElasticText, {
  ...font({ type: 'text1' }),
  color: color('text')
})

export const QuestionButtonsContainer = styled(View, {
  marginLeft: sp(12),
  height: sp(48),
  flexDirection: 'row',
  alignItems: 'center'
})

export const QuestionContainer = styled(View, {
  paddingTop: sp(8)
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  marginRight: sp(8),
  padding: sp(8),
  justifyContent: 'center'
})

export const MoreSvg = styled(VerticalDotsIcon24, {
  color: color('inactive')
})

export const MoreButtonCancel = styled(View, {
  justifyContent: 'center'
})

export const TitleContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center'
})

export const CategoryText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent')
})

export const ApproveAnswerContainer = styled(View, {
  marginLeft: 'auto'
})

export const ApproveAnswerText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('accent'),
  marginLeft: 'auto'
})
