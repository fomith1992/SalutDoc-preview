import React, { useMemo } from 'react'
import { Alert, Text } from 'react-native'
import { RequestModal } from '../../../../components/request-modal/request-modal.container'
import {
  useAnswerApproveMutation,
  useAnswerContextMenuQuery,
  useAnswerDisapproveMutation,
  useArchiveAnswerMutation
} from '../../../../gen/graphql'
import { useBooleanFlag } from '../../../../hooks/use-boolean-flag'
import { ContextMenu, ContextMenuButton } from '../../../../modules/ui-kit'
import { useAuthenticatedUser } from '../../../user-context/user-context-provider'

export interface AnswerContextMenuProps {
  visible: boolean
  answerId: string
  onHideMenu: () => void
}

export function AnswerContextMenu (
  {
    visible,
    answerId,
    onHideMenu
  }: AnswerContextMenuProps
): React.ReactElement {
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)
  const user = useAuthenticatedUser()
  const { data } = useAnswerContextMenuQuery({
    variables: { answerId }
  })

  if (data?.answerById == null) {
    return <Text>Произошла ошибка</Text>
  }
  const questionId = data.answerById.question.id
  const isMyQuestion = user.id === data.answerById.question.author.id

  const [archiveAnswerMutation] = useArchiveAnswerMutation()
  const [answerApproveMutation] = useAnswerApproveMutation()
  const [answerDisapproveMutation] = useAnswerDisapproveMutation()

  const archiveAnswer = (): void => {
    try {
      archiveAnswerMutation({
        variables: {
          answerId
        }
      })
    } catch {
      Alert.alert('Error', 'Failed to delete answer')
    }
  }
  const approveAnswer = (): void => {
    try {
      answerApproveMutation({
        variables: {
          questionId,
          answerId
        }
      })
    } catch {
      Alert.alert('Error', 'Failed to approve answer')
    }
  }
  const disapproveAnswer = (): void => {
    try {
      answerDisapproveMutation({
        variables: {
          questionId,
          answerId
        }
      })
    } catch {
      Alert.alert('Error', 'Failed to disapprove answer')
    }
  }

  const buttons = useMemo(() => {
    const buttons: ContextMenuButton[] = []
    if (data.answerById?.id === data.answerById?.question.approvedAnswer?.id && isMyQuestion) {
      buttons.push({
        type: 'black',
        title: 'Отменить выбор',
        onPress: () => {
          Alert.alert(
            'Предупреждение!',
            'Вы уверены, что хотите отменить?',
            [
              {
                text: 'Отмена',
                onPress: () => { onHideMenu() }
              },
              {
                text: 'Да',
                onPress: () => {
                  disapproveAnswer()
                  onHideMenu()
                }
              }
            ],
            { cancelable: false }
          )
        }
      })
    }
    if (data.answerById?.id !== data.answerById?.question.approvedAnswer?.id && isMyQuestion) {
      buttons.push({
        type: 'black',
        title: 'Выбрать правильным',
        onPress: () => {
          Alert.alert(
            'Предупреждение!',
            'Вы уверены, что хотите выбрать этот ответ правильным?',
            [
              {
                text: 'Отмена',
                onPress: () => { onHideMenu() }
              },
              {
                text: 'Да',
                onPress: () => {
                  approveAnswer()
                  onHideMenu()
                }
              }
            ],
            { cancelable: false }
          )
        }
      })
    }
    /* if (data?.answerById?.canEdit === true) {
      buttons.push({
        type: 'black',
        title: 'Редактировать запись',
        onPress: () => {
          onHideMenu()
          navigation.push('AnswerEditor', { answerId })
        }
      })
    } */
    if (data?.answerById?.canArchive === true) {
      buttons.push({
        type: 'black',
        title: 'Удалить запись',
        onPress: () => {
          Alert.alert(
            'Предупреждение!',
            'Вы уверены, что хотите удалить запись?',
            [
              {
                text: 'Отмена',
                onPress: onHideMenu
              },
              {
                text: 'Да',
                onPress: () => {
                  onHideMenu()
                  archiveAnswer()
                }
              }
            ]
          )
        }
      })
    }
    buttons.push(
      {
        type: 'black',
        title: 'Пожаловаться',
        onPress: () => {
          onHideMenu()
          showReportModal()
        }
      },
      {
        type: 'red',
        title: 'Отмена',
        onPress: onHideMenu
      }
    )
    return buttons
  }, [data])

  return (
    <>
      <ContextMenu
        visible={visible}
        onClose={onHideMenu}
        buttons={buttons}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='answer'
        contentId={answerId}
      />
    </>
  )
}
