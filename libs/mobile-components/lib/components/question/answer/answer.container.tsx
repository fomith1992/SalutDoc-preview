import React, { useEffect } from 'react'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { useAnswerApproveMutation, useAnswerQuery } from '../../../gen/graphql'
import { Alert, Text } from 'react-native'
import { AnswersView } from './answer.view'
import { useAuthenticatedUser } from '../../user-context/user-context-provider'
import { LikeAnswer } from '../../likes/like.container'
import { UserPreviewRow } from '../../preview-row/user-preview-row.container'
import { AnswerContextMenu } from './answer-context-menu/answer-context-menu'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { MoreButton, MoreSvg } from './answer.style'

interface AnswerProps {
  answerId: string
}
export function Answer ({
  answerId
}: AnswerProps): React.ReactElement {
  const { state: contextMenuVisible, setTrue: showContextMenu, setFalse: hideContextMenu } = useBooleanFlag(false)
  const user = useAuthenticatedUser()
  const { data, loading, error } = useAnswerQuery({
    variables: {
      answerId
    }
  })

  const [answerApproveMutation] = useAnswerApproveMutation()

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.answerById == null) {
    return <Text>Произошла ошибка</Text>
  }

  const questionId = data.answerById.question.id

  if (!data.answerById.archived) {
    return (
      <AnswersView
        author={(
          <UserPreviewRow
            shape='circle'
            userId={data.answerById.author.id}
            action={(
              <MoreButton onPress={showContextMenu}>
                <MoreSvg />
                <AnswerContextMenu
                  answerId={answerId}
                  onHideMenu={hideContextMenu}
                  visible={contextMenuVisible}
                />
              </MoreButton>
            )}
          />
        )}
        text={data.answerById.text}
        wireLikes={renderLikes => <LikeAnswer id={answerId} render={renderLikes} />}
        correct={data.answerById.id === data.answerById.question.approvedAnswer?.id}
        isMyQuestion={user.id === data.answerById.question.author.id}
        resolvedQuestion={data.answerById.question.approvedAnswer !== null}
        handleAnswerApprove={() => {
          answerApproveMutation({
            variables: {
              questionId,
              answerId
            }
          }).catch(e => {
            console.log('Failed to approve answer', e)
            Alert.alert('Error', 'Failed to approve answer')
          })
        }}

      />
    )
  } else return <></>
}
