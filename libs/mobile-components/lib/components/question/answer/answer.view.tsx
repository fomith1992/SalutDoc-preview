import React from 'react'
import { Alert } from 'react-native'
import { CardShadow } from '../../../modules/ui-kit'
import { RenderLikeAnswerButtonView } from '../../likes/like.container'
import { LikeButtonAnswerView } from '../../likes/like.view'
import {
  ApproveAnswerContainer,
  ApproveAnswerText,
  CategoryText,
  MoreButton,
  MoreButtonCancel,
  QuestionButtonsContainer,
  QuestionContainer,
  QuestionMain,
  QuestionReplyText,
  TitleContainer
} from './answer.style'

export interface AnswersViewProps {
  author: React.ReactElement
  text: string
  wireLikes: (render: RenderLikeAnswerButtonView) => React.ReactElement
  correct: boolean
  isMyQuestion: boolean
  resolvedQuestion: boolean
  handleAnswerApprove: () => void
}

export function AnswersView ({
  author,
  text,
  wireLikes,
  correct,
  isMyQuestion,
  resolvedQuestion,
  handleAnswerApprove
}: AnswersViewProps): React.ReactElement {
  return (
    <CardShadow>
      <QuestionContainer>
        {correct
          ? (
            <TitleContainer>
              <MoreButtonCancel>
                <CategoryText>
                  Правильный ответ
                </CategoryText>
              </MoreButtonCancel>
            </TitleContainer>
          )
          : null}
        {author}
        <QuestionMain>
          <QuestionReplyText
            numberOfLines={0}
          >
            {text}
          </QuestionReplyText>
        </QuestionMain>
        <QuestionButtonsContainer>
          {wireLikes(props => (
            <LikeButtonAnswerView
              {...props}
            />
          ))}
          {!isMyQuestion || correct || resolvedQuestion ? null
            : (
              <ApproveAnswerContainer>
                <MoreButton onPress={() => {
                  Alert.alert(
                    'Предупреждение!',
                    'Вы уверены, что хотите выбрать этот ответ правильным?',
                    [
                      {
                        text: 'Отмена',
                        onPress: () => {}
                      },
                      {
                        text: 'Да',
                        onPress: handleAnswerApprove
                      }
                    ],
                    { cancelable: false }
                  )
                }}
                >
                  <ApproveAnswerText>
                    Выбрать правильным
                  </ApproveAnswerText>
                </MoreButton>
              </ApproveAnswerContainer>
            )}
        </QuestionButtonsContainer>
      </QuestionContainer>
    </CardShadow>
  )
}
