import React from 'react'
import { Avatar } from '../../avatar/avatar.view'
import { RenderLikeButtonView } from '../../likes/like.container'
import { LikeButtonView } from '../../likes/like.view'
import { QuestionCardIcon } from '../../../images/question-card.icon'
import {
  AuthorContainer,
  QuestionButtonsContainer,
  QuestionText,
  OpenQuestionButton,
  OpenQuestionText,
  QuestionTitleContainer,
  CardContainer,
  CommentButton,
  QuestionButtonCount,
  AvatarContainer,
  PostTitleContainer,
  AuthorName,
  SubjectContainer,
  PostSubject,
  Dot,
  PublicDate,
  MoreButton,
  QuestionIcon,
  MoreSvg,
  ChatCloudImg
} from './question.style'
import { postDateFormatter } from '../../../modules/posts/post-date-formatter/post-date-formatter'
import { hitSlopParams } from '../../../modules/ui-kit'

export interface QuestionViewProps {
  title: string
  authorName: string
  answersCount: number
  subject: string
  resolved: boolean
  createdAt: string
  authorImg: string | null
  contextMenu: React.ReactElement
  handleQuestionPressed: () => void
  handleCommentsPressed: () => void
  wireLikes: (render: RenderLikeButtonView) => React.ReactElement
  onAuthorPressed: () => void
  onShowContextMenu: () => void
}

export function QuestionView ({
  handleQuestionPressed,
  title,
  answersCount,
  wireLikes,
  handleCommentsPressed,
  onAuthorPressed,
  authorImg,
  authorName,
  subject,
  createdAt,
  onShowContextMenu,
  contextMenu
}: QuestionViewProps): React.ReactElement {
  return (
    <CardContainer>
      <AuthorContainer onPress={onAuthorPressed}>
        <AvatarContainer>
          <Avatar shape='circle' url={authorImg} />
        </AvatarContainer>
        <PostTitleContainer>
          <AuthorName
            numberOfLines={2}
          >
            {authorName}
          </AuthorName>
          <SubjectContainer>
            <PostSubject>
              {subject}
            </PostSubject>
            <Dot />
            <PublicDate>
              {postDateFormatter(createdAt)}
            </PublicDate>
          </SubjectContainer>
        </PostTitleContainer>
        <MoreButton
          hitSlop={hitSlopParams(16)}
          onPress={onShowContextMenu}
        >
          <MoreSvg />
          {contextMenu}
        </MoreButton>
      </AuthorContainer>
      <QuestionTitleContainer>
        <QuestionText
          numberOfLines={3}
        >
          {title}
        </QuestionText>
        <QuestionIcon>
          <QuestionCardIcon />
        </QuestionIcon>
        <OpenQuestionButton
          onPress={handleQuestionPressed}
        >
          <OpenQuestionText>
            Перейти к вопросу
          </OpenQuestionText>
        </OpenQuestionButton>
      </QuestionTitleContainer>
      <QuestionButtonsContainer>
        {wireLikes(props => (
          <LikeButtonView
            {...props}
          />
        ))}
        <CommentButton onPress={handleCommentsPressed}>
          <ChatCloudImg />
          <QuestionButtonCount isPressed={false}>{answersCount > 0 ? answersCount : ''}</QuestionButtonCount>
        </CommentButton>
        {/* <ApproveQuestionText>
          {resolved
            ? 'Вопрос решен'
            : `${answersCount} ${toCaseCount(['ответ', 'ответа', 'ответов'], answersCount)}`}
        </ApproveQuestionText> */}
      </QuestionButtonsContainer>
      {contextMenu}
    </CardContainer>
  )
}
