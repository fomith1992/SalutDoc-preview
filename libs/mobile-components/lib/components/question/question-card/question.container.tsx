import { StackActions, useNavigation } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { Text } from 'react-native'
import { useQuestionQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { LikeQuestion } from '../../likes/like.container'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { QuestionContextMenu } from '../question-context-menu/question-context-menu'
import { QuestionView } from './question.view'

export interface QuestionProps {
  id: string
}

export function Question ({ id }: QuestionProps): React.ReactElement {
  const navigation = useNavigation()
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const { data, loading, error } = useQuestionQuery({
    variables: {
      id
    }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.questionById == null) {
    return <Text>Произошла ошибка</Text>
  }

  if (!data.questionById.archived) {
    return (
      <QuestionView
        title={data.questionById.title}
        answersCount={data.questionById.answersCount}
        resolved={data.questionById.approvedAnswer !== null}
        onAuthorPressed={() => navigation.dispatch(StackActions.push('Profile', { userId: data.questionById?.author.id }))}
        onShowContextMenu={showBottomMenu}
        createdAt={data.questionById.createdAt}
        subject={data.questionById.subject}
        authorName={`${data.questionById.author.firstName} ${data.questionById.author.lastName}`}
        authorImg={data.questionById.author.avatar}
        contextMenu={
          <QuestionContextMenu
            visible={bottomMenuVisible}
            questionId={id}
            onHideMenu={hideBottomMenu}
          />
        }
        handleQuestionPressed={() => navigation.dispatch(StackActions.push('QuestionFull', { id }))}
        handleCommentsPressed={() => navigation.dispatch(StackActions.push('QuestionFull', { id }))}
        wireLikes={renderLikes => <LikeQuestion id={id} render={renderLikes} />}
      />
    )
  } else return <></>
}
