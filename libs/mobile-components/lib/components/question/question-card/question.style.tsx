import { Platform, Text, TouchableOpacity, View } from 'react-native'
import { ChatCloudIcon20 } from '../../../images/chat-cloud.icon-20'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { ElasticText } from '../../elastic-text/elastic-text'

export const CardContainer = styled(View, {
  paddingTop: sp(16),
  backgroundColor: color('surface'),
  marginBottom: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const MenuButton = styled(TouchableOpacity, {
  padding: sp(8)
})

export const QuestionTitleContainer = styled(View, {
  ...container(),
  flex: 1,
  paddingVertical: sp(48),
  backgroundColor: color('accent'),
  borderRadius: sp(8),
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: sp(32),
  marginTop: sp(12)
})

export const QuestionText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('surface'),
  marginTop: sp(4)
})

export const QuestionButtonsContainer = styled(View, {
  ...container(),
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const Dot = styled(View, {
  width: 4,
  height: 4,
  borderRadius: 2,
  backgroundColor: color('inactive'),
  marginHorizontal: sp(8)
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  justifyContent: 'center'
})

export const MoreSvg = styled(VerticalDotsIcon24, {
  color: color('inactive')
})

export const PublicDate = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('inactive')
})

export const AuthorContainer = styled(TouchableOpacity, {
  ...container(),
  flex: 1,
  flexDirection: 'row'
})

export const AvatarContainer = styled(View, {
  width: 40,
  height: 40
})

export const AuthorName = styled(ElasticText, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const SubjectContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-start'
})

export const PostSubject = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('inactive')
})

export const PostTitleContainer = styled(View, {
  ...container(),
  flexShrink: 1,
  flexDirection: 'column'
})

export const OpenQuestionButton = styled(TouchableOpacity, {
  paddingHorizontal: sp(16),
  paddingVertical: sp(8),
  backgroundColor: color('surface'),
  marginTop: sp(16),
  borderRadius: sp(4)
})

export const OpenQuestionText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('accent'),
  textAlign: 'center'
})

export const CommentButton = styled(TouchableOpacity, {
  padding: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const QuestionButtonCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'h3', weight: 'light' }),
  color: color(props.isPressed ? 'accent' : 'inactive'),
  lineHeight: 16,
  textAlign: 'center',
  marginLeft: 4
}) as const)

export const QuestionIcon = styled(View, {
  opacity: 0.1,
  position: 'absolute',
  left: 0,
  bottom: 0
})

export const ChatCloudImg = styled(ChatCloudIcon20, {
  color: color('inactive')
})
