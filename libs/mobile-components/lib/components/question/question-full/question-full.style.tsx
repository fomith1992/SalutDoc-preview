import { Platform, Text, TouchableOpacity, View } from 'react-native'
import { ElasticText } from '../../elastic-text/elastic-text'
import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { sp } from '../../../style/size'

export const QuestionFullContainer = styled(View, {
  backgroundColor: color('surface'),
  paddingBottom: sp(8),
  marginBottom: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const QuestionReplyText = styled(ElasticText, {
  ...font({ type: 'text1' }),
  color: color('text')
})

export const QuestionMain = styled(View, {
  ...container()
})

export const QuestionTitle = styled(Text, {
  ...font({ type: 'h2', weight: 'light' }),
  color: color('text'),
  marginBottom: sp(8)
})

export const QuestionText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const QuestionButtonsContainer = styled(View, {
  ...container(),
  height: sp(48),
  flexDirection: 'row',
  alignItems: 'center'
})

export const ApproveAnswerText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('accent'),
  marginLeft: 'auto'
})

export const CardShadowContainer = styled(View, {
  ...container('margin'),
  marginBottom: sp(12),
  backgroundColor: color('surface'),
  borderRadius: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const QuestionContainer = styled(View, {
  paddingTop: sp(8)
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  padding: sp(8),
  justifyContent: 'center'
})

export const MoreButtonCancel = styled(TouchableOpacity, {
  justifyContent: 'center'
})

export const TitleContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center'
})

export const QuestionsContainer = styled(View, {
  flex: 1
})

export const CategoryText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent')
})

export const ApproveQuestionText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('inactive'),
  marginLeft: 'auto'
})

export const ApproveAnswerContainer = styled(View, {
  marginLeft: 'auto'
})

export const FooterContainer = styled(View, {
  marginBottom: sp(8)
})
