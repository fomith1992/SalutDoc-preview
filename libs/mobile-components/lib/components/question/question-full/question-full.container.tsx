import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { Text } from 'react-native'
import { useQuestionByIdQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { MoreSvg } from '../../../modules/posts/post-card/post-card.style'
import { LikeQuestion } from '../../likes/like.container'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { UserPreviewRow } from '../../preview-row/user-preview-row.container'
import { Answer } from '../answer/answer.container'
import { QuestionProps } from '../question-card/question.container'
import { QuestionContextMenu } from '../question-context-menu/question-context-menu'
import { MoreButton } from './question-full.style'
import { QuestionFullView } from './question-full.view'

export function QuestionFull ({ id }: QuestionProps): React.ReactElement {
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const navigation = useNavigation()
  const { data, loading, fetchMore, refetch } = useQuestionByIdQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      id
    }
  })

  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.questionById?.answers?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousAnswers = expectDefined(previousResult.questionById?.answers)
        return {
          questionById: {
            ...expectDefined(previousResult.questionById),
            answers: appendEdges(previousAnswers, fetchMoreResult?.questionById?.answers)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.questionById?.answers?.pageInfo.endCursor])

  if (loading && data?.questionById == null) {
    return <LoadingIndicator visible />
  }

  if (data?.questionById == null) {
    return <Text>Произошла ошибка</Text>
  }

  return (
    <QuestionFullView
      questionData={{
        id: data.questionById.id,
        author: (
          <UserPreviewRow
            userId={data.questionById.author.id}
            action={
              <MoreButton onPress={showBottomMenu}>
                <MoreSvg />
                <QuestionContextMenu
                  visible={bottomMenuVisible}
                  questionId={id}
                  onHideMenu={hideBottomMenu}
                />
              </MoreButton>
            }
          />
        ),
        title: data.questionById.title,
        description: data.questionById.description,
        answerCount: data.questionById.answersCount,
        canCreateAnswer: data.questionById.canCreateAnswer,
        approved: data.questionById.approvedAnswer !== null,
        wireLikes: renderLikes => <LikeQuestion id={id} render={renderLikes} />,
        onAddAnswer: () => navigation.dispatch(
          StackActions.push('AnswerCreate', { questionId: data.questionById?.id }))
      }}
      approvedAnswer={data.questionById.approvedAnswer !== null
        ? (
          <Answer
            answerId={data.questionById.approvedAnswer.id}
            key={data.questionById.approvedAnswer.id}
          />
        ) : null}
      replies={data.questionById.answers?.edges.filter(({ node }) => {
        return node.id !== node.question.approvedAnswer?.id
      }).map(({ node }) => {
        return <Answer answerId={node.id} key={node.id} />
      }) ?? []}
      loading={loading && !refreshing}
      refreshing={refreshing}
      hasMore={data.questionById.answers?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore()}
      refresh={refresh}
    />
  )
}
