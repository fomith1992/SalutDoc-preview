import React from 'react'
import { TextButton } from '../../../modules/ui-kit'
import { toCaseCount } from '../../../i18n/utils'
import {
  QuestionFullContainer,
  QuestionMain,
  QuestionTitle,
  QuestionText,
  QuestionButtonsContainer,
  ApproveQuestionText,
  QuestionsContainer,
  FooterContainer
} from './question-full.style'
import { FlatList, View } from 'react-native'
import { LikeButtonView } from '../../likes/like.view'
import { RenderLikeButtonView } from '../../likes/like.container'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'

interface QuestionFullViewProps {
  questionData: QuestionNoteProps
  approvedAnswer: React.ReactElement | null
  replies: React.ReactElement[]
  refreshing: boolean
  hasMore: boolean
  loading: boolean
  loadMore: () => void
  refresh: () => void
}

interface QuestionNoteProps {
  id: string
  author: React.ReactElement
  title: string
  description: string
  answerCount: number
  canCreateAnswer: boolean
  approved: boolean
  onAddAnswer: () => void
  wireLikes: (render: RenderLikeButtonView) => React.ReactElement
}

function QuestionNote ({
  author,
  title,
  description,
  answerCount,
  canCreateAnswer,
  approved,
  onAddAnswer,
  wireLikes
}: QuestionNoteProps): React.ReactElement {
  return (
    <QuestionFullContainer>
      {author}
      <QuestionMain>
        <QuestionTitle>
          {title}
        </QuestionTitle>
        <QuestionText>
          {description}
        </QuestionText>
      </QuestionMain>
      <QuestionButtonsContainer>
        {wireLikes(props => (
          <LikeButtonView
            {...props}
          />
        ))}

        <ApproveQuestionText>
          {approved
            ? 'Вопрос решен'
            : `${answerCount} ${toCaseCount(['ответ', 'ответа', 'ответов'], answerCount)} на вопрос`}
        </ApproveQuestionText>
      </QuestionButtonsContainer>
      {canCreateAnswer
        ? (
          <QuestionMain>
            <TextButton
              size='XL'
              type='primary'
              onPress={onAddAnswer}
            >
              Дать ответ на вопрос
            </TextButton>
          </QuestionMain>
        ) : null}
    </QuestionFullContainer>
  )
}

export function QuestionFullView ({
  questionData,
  replies,
  approvedAnswer,
  loading,
  refreshing,
  hasMore,
  loadMore,
  refresh
}: QuestionFullViewProps): React.ReactElement {
  return (
    <QuestionsContainer>
      <FlatList
        ListHeaderComponent={
          <View>
            <QuestionNote {...questionData} />
            {approvedAnswer}
          </View>
        }
        renderItem={({ item }) => item}
        data={replies}
        refreshing={refreshing}
        onRefresh={refresh}
        onEndReached={hasMore ? loadMore : null}
        ListFooterComponent={
          <FooterContainer>
            <LoadingIndicator visible={loading} />
          </FooterContainer>
        }
      />
    </QuestionsContainer>
  )
}
