import React, { useMemo } from 'react'
import { Alert } from 'react-native'
import { RequestModal } from '../../request-modal/request-modal.container'
import { useArchiveQuestionMutation, useQuestionContextMenuQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { ContextMenu, ContextMenuButton } from '../../../modules/ui-kit'

export interface QuestionContextMenuProps {
  visible: boolean
  questionId: string
  onHideMenu: () => void
}

export function QuestionContextMenu ({
  visible,
  questionId,
  onHideMenu
}: QuestionContextMenuProps
): React.ReactElement {
  const navigation = useStackNavigation()
  const { data } = useQuestionContextMenuQuery({
    variables: { questionId }
  })

  const [archiveQuestionMutation] = useArchiveQuestionMutation()
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)

  const archiveQuestion = (): void => {
    try {
      archiveQuestionMutation({
        variables: { questionId }
      })
      navigation.goBack()
    } catch {
      Alert.alert('Error', 'Failed to delete question')
    }
  }

  const buttons = useMemo(() => {
    const buttons: ContextMenuButton[] = []
    if (data?.questionById?.canEdit === true) {
      buttons.push({
        type: 'black',
        title: 'Редактировать запись',
        onPress: () => {
          onHideMenu()
          navigation.push('QuestionEditor', { questionId })
        }
      })
    }
    if (data?.questionById?.canArchive === true) {
      buttons.push({
        type: 'black',
        title: 'Удалить запись',
        onPress: () => {
          Alert.alert(
            'Предупреждение!',
            'Вы уверены, что хотите удалить запись?',
            [
              {
                text: 'Отмена',
                onPress: onHideMenu
              },
              {
                text: 'Да',
                onPress: () => {
                  onHideMenu()
                  archiveQuestion()
                }
              }
            ]
          )
        }
      })
    }
    buttons.push(
      {
        type: 'black',
        title: 'Пожаловаться',
        onPress: () => {
          onHideMenu()
          showReportModal()
        }
      },
      {
        type: 'red',
        title: 'Отмена',
        onPress: onHideMenu
      }
    )
    return buttons
  }, [data])

  return (
    <>
      <ContextMenu
        visible={visible}
        onClose={onHideMenu}
        buttons={buttons}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='question'
        contentId={questionId}
      />
    </>
  )
}
