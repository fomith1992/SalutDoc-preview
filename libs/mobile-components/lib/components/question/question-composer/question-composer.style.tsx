import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { container } from '../../../style/view'
import { sp } from '../../../style/size'
import { View } from 'react-native'

export const SubjectInputContainer = styled(View, {
  ...container('padding'),
  paddingTop: sp(8),
  backgroundColor: color('surface')
})

export const QuestionCreatorContainer = styled(View, {
  backgroundColor: color('surface'),
  paddingTop: sp(16),
  flex: 1
})

export const QuestionCreatorFooter = View

export const PublishButtonContainer = styled(View, {
  marginLeft: 'auto',
  marginRight: sp(8),
  marginBottom: sp(16)
})
