import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { PostTextInput, QuestionTextInput, yup } from '../../../modules/form-kit'
import { specializationsData, SubmitTextButton } from '../../../modules/ui-kit'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'
import { SelectSearch } from '../../../modules/form-kit/select-search/select-search.view'
import {
  PublishButtonContainer,
  QuestionCreatorContainer,
  QuestionCreatorFooter,
  SubjectInputContainer
} from './question-composer.style'

const formSchema = yup.object({
  subject: yup.string()
    .required(),
  title: yup.string()
    .required(),
  description: yup.string()
    .required()
}).required()

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export type QuestionComposerFormData = yup.InferType<typeof formSchema>

export interface QuestionComposerViewProps {
  initialValues?: Partial<QuestionComposerFormData>
  onSubmit: SubmissionHandler<QuestionComposerFormData>
  submitting: boolean
}

export const QuestionComposerView = ({
  onSubmit,
  submitting,
  initialValues
}: QuestionComposerViewProps): React.ReactElement => {
  return (
    <Form
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      <SubjectInputContainer>
        <Field name='subject'>
          {props => (
            <SelectSearch
              {...props}
              options={specializationsData}
              text='Тема вопроса'
              type='button'
            />)}
        </Field>
      </SubjectInputContainer>
      <QuestionCreatorContainer>
        <Field name='title'>
          {propsText => (
            <QuestionTextInput {...propsText} placeholder='Задайте свой вопрос' />
          )}
        </Field>
        <Field name='description'>
          {propsText => (
            <PostTextInput {...propsText} placeholder='Добавьте описание' />
          )}
        </Field>
        <QuestionCreatorFooter>
          <PublishButtonContainer>
            <Submit>
              {props => (
                <SubmitTextButton
                  type='primary'
                  {...props}
                >
                  Опубликовать
                </SubmitTextButton>
              )}
            </Submit>
          </PublishButtonContainer>
        </QuestionCreatorFooter>
      </QuestionCreatorContainer>
      <ModalLoadingIndicator visible={submitting} />
    </Form>
  )
}
