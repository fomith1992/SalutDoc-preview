import { useCreateUserQuestionMutation } from '../../../gen/graphql'
import React from 'react'
import { feedCacheNewQuestion } from '../../../modules/feed/cache/feed-cache-new-question'
import { useNavigation } from '@react-navigation/native'
import { Alert } from 'react-native'
import { QuestionComposerView } from '../question-composer/question-composer.view'

export function QuestionCreator (): React.ReactElement {
  const navigation = useNavigation()
  const [createUserQuestionMutation, { loading: submittingQuestion }] = useCreateUserQuestionMutation({
    update: (cache, { data }) => {
      const question = data?.questionCreate.question
      if (question == null) {
        return
      }
      feedCacheNewQuestion(cache, question)
    }
  })

  return (
    <QuestionComposerView
      onSubmit={content => {
        createUserQuestionMutation({
          variables: {
            content
          }
        }).catch(e => {
          console.log('Не удалось задать вопрос', e)
          Alert.alert('Не удалось задать вопрос')
        })
        navigation.goBack()
        return {}
      }}
      submitting={submittingQuestion}
    />
  )
}
