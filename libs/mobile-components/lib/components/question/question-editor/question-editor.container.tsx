import { useEditableQuestionQuery, useEditQuestionMutation } from '../../../gen/graphql'
import React, { useEffect } from 'react'
import { QuestionComposerView } from '../question-composer/question-composer.view'
import { useNavigation } from '@react-navigation/native'
import { Alert } from 'react-native'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'

interface QuestionEditorProps {
  questionId: string
}

export function QuestionEditor ({ questionId }: QuestionEditorProps): React.ReactElement {
  const navigation = useNavigation()

  const { data, loading, error } = useEditableQuestionQuery({
    variables: { questionId }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  const [editQuestionMutation, { loading: submittingQuestion }] = useEditQuestionMutation({
    onCompleted: result => {
      const { question, message, errors } = result.questionEdit
      if (question != null) {
        console.log('Запись отредактирована', message, question)
        navigation.goBack()
      } else {
        console.log('Не удалось отредактировать запись', message, errors)
        Alert.alert('Ошибка', 'Не удалось отредактировать запись')
      }
    }
  })

  return loading
    ? <LoadingIndicator visible />
    : (
      <QuestionComposerView
        initialValues={{
          title: data?.questionById?.title,
          description: data?.questionById?.description,
          subject: data?.questionById?.subject
        }}
        onSubmit={content => {
          editQuestionMutation({
            variables: {
              questionId,
              content
            }
          })
          return {}
        }}
        submitting={submittingQuestion}
      />
    )
}
