import React from 'react'
import { SearchContainer, InputPrefixContainer, SearchInput } from './inline-search.style'
import { SearchIcon24 } from '../../images/search.icon-24'

interface InlineSearchViewProps {
  placeholder?: string
}

export const InlineSearchView = ({ placeholder = '' }: InlineSearchViewProps): React.ReactElement => {
  return (
    <SearchContainer>
      <InputPrefixContainer>
        <SearchIcon24 />
      </InputPrefixContainer>
      <SearchInput
        placeholder={placeholder}
      />
    </SearchContainer>
  )
}
