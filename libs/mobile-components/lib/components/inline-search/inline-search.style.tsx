import { TextInput, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'

export const SearchContainer = styled(View, {
  backgroundColor: color('surface'),
  flexDirection: 'row'
})

export const InputPrefixContainer = styled(View, {
  padding: 12
})

export const SearchInput = styled(TextInput, {
  flex: 1
})
