import { View, Text, TouchableOpacity, Platform } from 'react-native'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { color } from '../../style/color'
import { ArrowIcon16 } from '../../images/arrow.icon-16'
import { sp } from '../../style/size'

export const HeaderContainer = styled(View, {
  alignItems: 'center',
  flexDirection: 'row',
  backgroundColor: color('surface'),
  ...Platform.select({
    android: {
      elevation: 5
    },
    ios: {
      borderBottomColor: 'rgb(242, 242, 242)',
      borderBottomWidth: 1
    }
  })
})

export const Title = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('text')
})

export const HeaderButton = styled(TouchableOpacity, {
  paddingVertical: sp(8),
  paddingHorizontal: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const HeaderBackImage = styled(ArrowIcon16, {
  marginHorizontal: sp(8),
  marginVertical: sp(16),
  color: color('accent')
})
