import React from 'react'
import {
  HeaderContainer,
  HeaderButton,
  Title,
  HeaderBackImage
} from './header-light.style'

type HeaderLightProps = {
  title: string
  onClose: () => void
}

export function HeaderLight ({ title, onClose }: HeaderLightProps): React.ReactElement {
  return (
    <HeaderContainer>
      <HeaderButton onPress={onClose}>
        <HeaderBackImage />
      </HeaderButton>
      <Title>
        {title}
      </Title>
    </HeaderContainer>
  )
}
