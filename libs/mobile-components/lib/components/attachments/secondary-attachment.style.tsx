import { Text, View, Image, TouchableOpacity } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { PlayIcon24 } from '../../images/play.icon-24'

export const AttachmentsConainer = styled(View, {
  flex: 1
})

export const SecondaryAttachment = styled(View, {
  flexDirection: 'row',
  marginTop: sp(8)
})

export const NodeAttachment = styled(Image, (props: { part: number, height: number }) => ({
  flex: props.part,
  height: props.height,
  borderRadius: sp(8)
}))

export const MainSecondaryAttachmentContainer = styled(TouchableOpacity, {
  flex: 1,
  justifyContent: 'center'
})

export const AttachmentContainer = styled(TouchableOpacity, {
  flex: 1,
  marginLeft: sp(8),
  justifyContent: 'center'
})

export const CountTailAttachmentsContainer = styled(View, {
  position: 'absolute',
  height: '100%',
  width: '100%',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: sp(8),
  backgroundColor: 'rgba(0, 0, 0, 0.5)'
})

export const CountTailAttachments = styled(Text, {
  ...font({ type: 'h2', weight: 'strong' }),
  color: color('surface')
})

export const PlayVideoContainer = styled(View, {
  position: 'absolute',
  paddingLeft: sp(12),
  paddingBottom: sp(4),
  alignSelf: 'center'
})

export const PlayIconContainer = styled(View, {
  height: sp(32),
  flexDirection: 'row',
  backgroundColor: 'rgba(18, 18, 18, 0.7)',
  borderRadius: sp(16),
  alignSelf: 'center',
  alignItems: 'center',
  paddingLeft: sp(12),
  paddingRight: sp(12)
})

export const AttachmentOverlay = TouchableOpacity

export const PlayImg = styled(PlayIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('background')
})
