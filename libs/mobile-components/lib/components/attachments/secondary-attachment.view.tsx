import React, { useState } from 'react'
import {
  AttachmentsConainer,
  SecondaryAttachment,
  NodeAttachment,
  CountTailAttachmentsContainer,
  AttachmentContainer,
  CountTailAttachments,
  PlayVideoContainer,
  PlayIconContainer,
  MainSecondaryAttachmentContainer,
  AttachmentOverlay,
  PlayImg
} from './secondary-attachment.style'
import { useTheme } from '../../style/theme'
import { sp } from '../../style/size'
import { materialize } from '../../style/styled'
import { MainAttachmentView } from './main-attachment.view'
import { Gallery } from '../gallery/gallery.container'

interface YouTubeVideoPostAttachment {
  __typename: 'YouTubeVideoPostAttachment'
  videoId: string
}

interface ImagePostAttachment {
  __typename: 'ImagePostAttachment'
  url: string
}

export type AttachmentProps = YouTubeVideoPostAttachment | ImagePostAttachment

export function getValidUrl (attachment: AttachmentProps): string {
  switch (attachment.__typename) {
    case 'ImagePostAttachment':
      return attachment.url
    case 'YouTubeVideoPostAttachment':
      return `https://img.youtube.com/vi/${attachment.videoId}/hqdefault.jpg`
  }
}

function isYoutubeAttachment (attachment: AttachmentProps): boolean {
  return attachment.__typename === 'YouTubeVideoPostAttachment'
}

export function PlayVideoIcon (): React.ReactElement {
  return (
    <PlayVideoContainer>
      <PlayIconContainer>
        <PlayImg />
        {/* TODO add video duration */}
      </PlayIconContainer>
    </PlayVideoContainer>
  )
}

function SecondaryAttachmentView (
  props: {
    attachments: AttachmentProps[]
    onPressAttachment: (index: number) => void
  }
): React.ReactElement {
  const theme = useTheme()
  const [width, setWidth] = useState<number>(0)
  const { attachments, onPressAttachment } = props
  const partFirstAttachment = attachments.length === 2 ? 2 : 1
  const height = (width - materialize(theme, sp(8))) / 3
  if (attachments.length === 0) {
    return <></>
  }
  return (
    <SecondaryAttachment
      onLayout={(event) => {
        const { width } = event.nativeEvent.layout
        setWidth(width)
      }}
    >
      {attachments.slice(0, 3).map((attachment, index) => {
        const attachmentUrl = getValidUrl(attachment)
        const isVideo = isYoutubeAttachment(attachment)
        switch (index) {
          case 0:
            return (
              <MainSecondaryAttachmentContainer
                key={attachmentUrl}
                onPress={() => onPressAttachment(index + 1)}
              >
                <NodeAttachment
                  source={{
                    uri: attachmentUrl
                  }}
                  part={partFirstAttachment}
                  height={height}
                />
                {isVideo ? <PlayVideoIcon /> : <></>}
              </MainSecondaryAttachmentContainer>
            )
          case 1:
            return (
              <AttachmentContainer
                key={attachmentUrl}
                onPress={() => onPressAttachment(index + 1)}
              >
                <NodeAttachment
                  source={{
                    uri: attachmentUrl
                  }}
                  part={1}
                  height={height}
                />
                {isVideo ? <PlayVideoIcon /> : <></>}
              </AttachmentContainer>
            )
          case 2:
            return (
              <AttachmentContainer
                key={attachmentUrl}
                onPress={() => onPressAttachment(index + 1)}
              >
                <NodeAttachment
                  source={{
                    uri: attachmentUrl
                  }}
                  part={1}
                  height={height}
                />
                {attachments.length > 3 ? (
                  <CountTailAttachmentsContainer>
                    <CountTailAttachments>
                      {`+${attachments.length - 3}`}
                    </CountTailAttachments>
                  </CountTailAttachmentsContainer>
                ) : isVideo ? <PlayVideoIcon /> : <></>}
              </AttachmentContainer>
            )
          default:
            return <></>
        }
      })}
    </SecondaryAttachment>
  )
}

export function Attachments (
  props: {
    attachments: AttachmentProps[]
  }
): React.ReactElement {
  const [galleryVisible, setGalleryVisible] = useState(false)
  const [galleryShowIndex, setGalleryShowIndex] = useState(1)
  const [mainAttachment, ...secondaryAttachments] = props.attachments
  if (mainAttachment === undefined) return <></>
  const mainAttachmentUri = getValidUrl(mainAttachment)
  const isVideo = isYoutubeAttachment(mainAttachment)
  function onShowGallery (showIndex: number): void {
    setGalleryShowIndex(showIndex)
    setGalleryVisible(true)
  }
  return (
    <AttachmentsConainer>
      <AttachmentOverlay
        onPress={() => onShowGallery(0)}
      >
        <MainAttachmentView
          uri={mainAttachmentUri}
          overlay={
            isVideo ? <PlayVideoIcon /> : <></>
          }
        />
      </AttachmentOverlay>
      <SecondaryAttachmentView
        attachments={secondaryAttachments}
        onPressAttachment={onShowGallery}
      />
      <Gallery
        attachments={props.attachments}
        showIndex={galleryShowIndex}
        galleryVisible={galleryVisible}
        onClose={() => setGalleryVisible(false)}
      />
    </AttachmentsConainer>
  )
}
