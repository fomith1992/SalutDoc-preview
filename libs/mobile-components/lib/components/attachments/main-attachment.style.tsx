import { View } from 'react-native'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

export const MainAttachmentContainer = styled(View, {
  borderRadius: sp(8),
  overflow: 'hidden'
})
