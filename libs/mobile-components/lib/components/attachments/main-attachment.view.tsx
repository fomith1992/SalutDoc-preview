import React from 'react'
import { ImageResizeView } from '../image-resize/image-resize.view'
import {
  MainAttachmentContainer
} from './main-attachment.style'

interface MainAttachmentViewProps {
  uri: string
  overlay?: React.ReactElement
  onLoadingState?: (value: boolean) => void
}

export function MainAttachmentView ({
  uri,
  overlay,
  onLoadingState
}: MainAttachmentViewProps): React.ReactElement {
  return (
    <MainAttachmentContainer>
      <ImageResizeView
        uri={uri}
        overlay={overlay}
        onLoadingState={onLoadingState}
      />
    </MainAttachmentContainer>
  )
}
