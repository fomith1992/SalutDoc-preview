import { styled } from '../../style/styled'
import { View, Text, TouchableOpacity, Platform } from 'react-native'
import { container } from '../../style/view'
import { sp } from '../../style/size'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { FeedIcon24 } from '../../images/feed.icon-24'

export const RecommendationsContainer = styled(View, {
})

export const CardContainer = styled(View, {
  ...container('margin'),
  ...container('padding'),
  marginTop: sp(12),
  backgroundColor: color('surface'),
  borderRadius: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const AuthorsName = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent')
})

export const PostButtonsContainer = styled(View, {
  height: 38,
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const PublicDate = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext'),
  marginLeft: 'auto'
})

export const MenuButton = styled(TouchableOpacity, {
  padding: sp(8)
})

export const BackgroundImage = styled(View, {
  aspectRatio: 1
})

export const Spring = styled(View, {
  flex: 1
})

export const EmptyRecommendationsContainer = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
  paddingVertical: sp(32)
})

export const EmptyRecommendationsText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('inactive')
})

export const FeedIconSvg = styled(FeedIcon24, {
  width: sp(32),
  height: sp(32),
  color: color('accent')
})

export const FooterIndent = styled(View, {
  marginBottom: sp(8)
})
