
import React from 'react'
/* import {
  CardContainer,
  PostText,
  PostButtonsContainer,
  PublicDate,
  MenuButton,
  Spring
} from './recommendations.style'
import { MoreIcon } from '../../images/more.icon' */
import { FlatList } from 'react-native'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import {
  EmptyRecommendationsContainer,
  EmptyRecommendationsText,
  FeedIconSvg,
  FooterIndent
} from './recommendations.style'
/* const attachments: AttachmentProps[] = [
  { __typename: 'ImagePostAttachment',
  url: 'https://medvestnik.ru/apps/mv/assets/files/content/news/771/77152/front.jpg' }
] */

export type RecommendationItem = PostItem | ArticleItem | QuestionItem

type PostItem = {
  id: string
  type: 'POST'
}

type ArticleItem = {
  id: string
  type: 'ARTICLE'
}

type QuestionItem = {
  id: string
  type: 'QUESTION'
}

type RecommendationsViewProps = {
  items: RecommendationItem[]
  renderItem: (item: RecommendationItem) => React.ReactElement
  loading: boolean
  refreshing: boolean
  refresh: () => void
  userRecommendedData: React.ReactElement
  communityRecommendedData: React.ReactElement
}

export function RecommendationsView (props: RecommendationsViewProps): React.ReactElement {
  const { items, loading, renderItem, refreshing, refresh, userRecommendedData, communityRecommendedData } = props
  return (
    <FlatList
      data={items}
      renderItem={({ item }) => renderItem(item)}
      keyExtractor={item => item.id}
      ListHeaderComponent={
        <>
          {userRecommendedData}
          {communityRecommendedData}
        </>
      }
      ListEmptyComponent={loading ? null : (
        <EmptyRecommendationsContainer>
          <FeedIconSvg />
          <EmptyRecommendationsText>
            Пока нет рекомендаций
          </EmptyRecommendationsText>
        </EmptyRecommendationsContainer>
      )}
      ListFooterComponent={
        loading
          ? <LoadingIndicator visible={loading} />
          : <FooterIndent />
      }
      refreshing={refreshing}
      onRefresh={refresh}
    />
  )
}
