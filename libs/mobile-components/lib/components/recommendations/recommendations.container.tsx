import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { useUserRecommendationsPostsQuery } from '../../gen/graphql'
import { useRefresh } from '../../hooks/use-refresh'
import { toCaseCount } from '../../i18n/utils'
import { WorkPreview } from '../portfolio/work-prewiew/work-prewiew.container'
import { PostCard } from '../../modules/posts'
import { Question } from '../question/question-card/question.container'
import { PreviewSlider } from '../search/preview-slider.view'
import { useAuthenticatedUser } from '../user-context/user-context-provider'
import { RecommendationItem, RecommendationsView } from './recommendations.view'

export function Recommendations (): React.ReactElement {
  const navigation = useNavigation()
  const user = useAuthenticatedUser()
  const { data, loading, refetch } = useUserRecommendationsPostsQuery({
    variables: {
      userId: user.id
    }
  })
  const { refreshing, refresh } = useRefresh(refetch)

  const renderItem = (props: RecommendationItem): React.ReactElement => {
    switch (props.type) {
      case 'POST': {
        return <PostCard id={props.id} />
      }
      case 'ARTICLE': {
        return <WorkPreview id={props.id} />
      }
      case 'QUESTION': {
        return <Question id={props.id} />
      }
      default: {
        return <></>
      }
    }
  }

  return (
    <RecommendationsView
      loading={loading}
      refreshing={refreshing}
      refresh={refresh}
      renderItem={renderItem}
      items={
        data?.userById?.content?.edges.map(({ node }) => {
          switch (node.__typename) {
            case 'Post':
              return {
                id: node.id,
                type: 'POST'
              }
            case 'Question':
              return {
                id: node.id,
                type: 'QUESTION'
              }
            case 'Article':
              return {
                id: node.id,
                type: 'ARTICLE'
              }
            default:
              throw new Error('Unexpected recommended type')
          }
        }) ?? []
      }
      userRecommendedData={data?.userById?.users != null && data.userById.users.edges.length > 0
        ? (
          <PreviewSlider
            items={data.userById.users.edges?.map(({ node }) => {
              switch (node.__typename) {
                case 'User':
                  return {
                    avatar: node.avatar ?? undefined,
                    firstName: node.firstName,
                    lastName: node.lastName,
                    description: node.specialization,
                    onPress: () => navigation.dispatch(StackActions.push('Profile', { userId: node.id }))
                  }
                default:
                  throw new Error('Unexpected recommended type')
              }
            })}
            title='Популярные пользователи'
          />
        ) : <></>}
      communityRecommendedData={data?.userById?.communities != null && data.userById.communities.edges.length > 0
        ? (
          <PreviewSlider
            items={data.userById.communities.edges?.map(({ node }) => {
              switch (node.__typename) {
                case 'Community':
                  return {
                    avatar: node.avatar ?? undefined,
                    firstName: node.name,
                    description: `${node.membersCount ?? 0} ${toCaseCount(
                      ['подписчик', 'подписчика', 'подписчиков'], node.membersCount ?? 0)}`,
                    onPress: () => navigation.dispatch(StackActions.push('Community', { communityId: node.id }))
                  }
                default:
                  throw new Error('Unexpected recommended type')
              }
            })}
            title='Популярные сообщества'
          />
        ) : <></>}
    />
  )
}
