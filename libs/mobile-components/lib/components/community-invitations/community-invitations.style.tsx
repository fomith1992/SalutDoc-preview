import { Text, TouchableOpacity, View } from 'react-native'
import { TickIcon24 } from '../../images/tick.icon-24'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { ArrowIcon16 } from '../../images/arrow.icon-16'

export const CancelInvitationButton = styled(TouchableOpacity, {
  justifyContent: 'center',
  marginLeft: 'auto',
  padding: sp(8)
})

export const CancelInvitationIconSvg = styled(ArrowIcon16, {
  color: color('accent'),
  width: sp(16),
  height: sp(16)
})

export const InvitationCancelledIconSvg = styled(TickIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('subtext')
})

export const EmptyInvitationsContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: sp(48)
})

export const EmptyInvitationsTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  textAlign: 'center'
})

export const EmptyInvitationsText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(16)
})
