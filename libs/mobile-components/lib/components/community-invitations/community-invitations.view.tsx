import {
  CancelInvitationButton,
  CancelInvitationIconSvg,
  EmptyInvitationsContainer,
  EmptyInvitationsText,
  EmptyInvitationsTitle,
  InvitationCancelledIconSvg
} from './community-invitations.style'
import { FlatList } from 'react-native-gesture-handler'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import React from 'react'
import { UserRow } from '../user-row/user-row.view'

interface User {
  id: string
  avatar: string | null
  firstName: string
  lastName: string
  invitationCancelled: boolean
  handleProceedToProfile: () => void
  handleCancelInvitation: () => void
}

export interface CommunityInvitationsViewProps {
  data: User[]
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
}

export function CommunityInvitationsView (
  {
    data,
    refreshing,
    loading,
    hasMore,
    refresh,
    loadMore
  }: CommunityInvitationsViewProps
): React.ReactElement {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => (
        <UserRow
          id={item.id}
          avatar={item.avatar}
          userName={`${item.firstName} ${item.lastName}`}
          actionPerformed={item.invitationCancelled}
          actionPerformedText='Заявка отклонена'
          onProceedToProfile={item.handleProceedToProfile}
          actions={
            <CancelInvitationButton onPress={item.handleCancelInvitation}>
              {item.invitationCancelled
                ? <InvitationCancelledIconSvg />
                : <CancelInvitationIconSvg />}
            </CancelInvitationButton>
          }
        />
      )}
      keyExtractor={item => item.id}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListFooterComponent={<LoadingIndicator visible={loading} />}
      ListEmptyComponent={() => (
        <EmptyInvitationsContainer>
          <EmptyInvitationsTitle>
            Нет приглашений
          </EmptyInvitationsTitle>
          <EmptyInvitationsText>
            В данный момент активных приглашений в группу нет
          </EmptyInvitationsText>
        </EmptyInvitationsContainer>
      )}
      contentContainerStyle={{ flexGrow: 1, marginTop: 8 }}
    />
  )
}
