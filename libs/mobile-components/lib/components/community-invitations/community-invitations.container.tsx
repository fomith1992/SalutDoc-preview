import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { Text } from 'react-native'
import { useCancelUserInvitationMutation, useCommunityInvitationsListQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { CommunityInvitationsView } from './community-invitations.view'

export interface CommunityInvitationsProps {
  communityId: string
}

export function CommunityInvitations ({ communityId }: CommunityInvitationsProps): React.ReactElement {
  const navigation = useNavigation()
  const [cancelledWithinSession, setCancelledWithinSession] = useState<string[]>([])
  const { loading, data, fetchMore, refetch } = useCommunityInvitationsListQuery({
    variables: {
      communityId,
      after: null
    },
    fetchPolicy: 'network-only'
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: {
        communityId,
        after: expectDefined(data?.communityById?.outgoingMembershipInvites?.pageInfo.endCursor)
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousInvitations = expectDefined(previousResult.communityById?.outgoingMembershipInvites)
        return {
          ...expectDefined(previousResult),
          communityById: {
            ...expectDefined(previousResult.communityById),
            outgoingMembershipInvites: appendEdges(previousInvitations, fetchMoreResult?.communityById?.outgoingMembershipInvites)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.communityById?.outgoingMembershipInvites?.pageInfo.endCursor])
  const [cancelInvitation] = useCancelUserInvitationMutation()
  if (data?.communityById?.outgoingMembershipInvites == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <CommunityInvitationsView
      data={data.communityById.outgoingMembershipInvites.edges
        .filter(({ node: { status, user: { id } } }) =>
          status === 'INVITE' ||
          cancelledWithinSession.includes(id)
        )
        .map(({ node: { user: { id, avatar, firstName, lastName } } }) => ({
          id,
          avatar,
          firstName,
          lastName,
          invitationCancelled: cancelledWithinSession.includes(id),
          handleProceedToProfile: (): void => navigation.dispatch(StackActions.push('Profile', { userId: id })),
          handleCancelInvitation: (): void => {
            cancelInvitation({
              variables: {
                communityId,
                userId: id
              }
            }).catch(console.error)
            setCancelledWithinSession([...cancelledWithinSession, id])
          }
        })) ?? []}
      hasMore={data.communityById.outgoingMembershipInvites.pageInfo.hasNextPage ?? false}
      refreshing={refreshing}
      loading={loading}
      refresh={refresh}
      loadMore={() => loadMore()}
    />
  )
}
