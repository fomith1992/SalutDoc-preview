import { Text, View } from 'react-native'
import { ExclamationIcon24 } from '../../images/exclamation.icon-24'
import { LockIcon24 } from '../../images/lock.icon-24'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { ElasticText } from '../elastic-text/elastic-text'

export const MainContainer = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  flexDirection: 'row',
  paddingTop: sp(16),
  overflow: 'hidden'
})

export const InfoContainer = styled(View, {
  flex: 1,
  marginLeft: sp(12),
  marginTop: sp(16),
  marginBottom: sp(8),
  alignItems: 'flex-start'
})

export const Name = styled(ElasticText, {
  ...font({ type: 'h2' })
})

export const Lock = styled(LockIcon24, {
  height: sp(20),
  width: sp(20)
})

export const ActionsContainer = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  paddingTop: sp(16),
  paddingBottom: sp(8)
})

export const ButtonsRow = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between'
})

export const ButtonContainer = styled(View, {
  flex: 1
})

export const Spring = styled(View, {
  paddingHorizontal: sp(8)
})

export const MoreInfoContainer = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  paddingVertical: sp(8),
  alignItems: 'flex-start'
})

export const Description = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('inactive')
})

export const ExclamationImg = styled(ExclamationIcon24, {
  width: sp(16),
  height: sp(16),
  color: color('inactive')
})
