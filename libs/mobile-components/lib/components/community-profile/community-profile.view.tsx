import React from 'react'
import { toCaseCount } from '../../i18n/utils'
import { Collapsible, ScreenBlock, TextButton } from '../../modules/ui-kit'
import { Avatar } from '../avatar/avatar.view'
import {
  ActionsContainer,
  ButtonsRow,
  ButtonContainer,
  Spring,
  Description,
  InfoContainer,
  Lock,
  MainContainer,
  MoreInfoContainer,
  Name,
  ExclamationImg
} from './community-profile.style'

interface CommunityProfileProps {
  communityName: string
  avatar?: string | null
  followersCount: number
  codex: string | null
  isPrivate?: boolean
  isMy: boolean
  handleMembers: () => void
  handleEdit: () => void
  handleInviteUsers: () => void
  followCommunityButton: React.ReactElement
}

export const CommunityProfileView = (
  {
    communityName,
    avatar,
    codex,
    followersCount,
    isPrivate = false,
    isMy,
    handleEdit,
    handleInviteUsers,
    handleMembers,
    followCommunityButton
  }: CommunityProfileProps
): React.ReactElement => {
  return (
    <ScreenBlock>
      <MainContainer>
        <Avatar url={avatar} />
        <InfoContainer>
          <Name numberOfLines={3}>
            {communityName}
          </Name>
          {isPrivate && !isMy ? <Lock /> : null}
          <TextButton
            size='L'
            type='link'
            onPress={handleMembers}
          >
            {`${followersCount} ${toCaseCount(['подписчик', 'подписчика', 'подписчиков'], followersCount)}`}
          </TextButton>
        </InfoContainer>
      </MainContainer>
      <ActionsContainer>
        {isMy ? (
          <ButtonsRow>
            <ButtonContainer>
              <TextButton
                size='L'
                type='primary'
                onPress={handleEdit}
              >
                Изменить
              </TextButton>
            </ButtonContainer>
            {isPrivate && (
              <>
                <Spring />
                <ButtonContainer>
                  <TextButton
                    size='L'
                    type='primary'
                    onPress={handleInviteUsers}
                  >
                    Пригласить
                  </TextButton>
                </ButtonContainer>
              </>
            )}
          </ButtonsRow>
        ) : (
          followCommunityButton
        )}
      </ActionsContainer>
      {(codex ?? '').trim() === '' ? null : (
        <MoreInfoContainer>
          <Collapsible
            threshold={80}
            collapsedHeight={45}
            renderFooter={({ collapsed, toggle }) => (
              <TextButton
                type='link'
                icon={<ExclamationImg />}
                onPress={toggle}
              >
                {!collapsed ? 'Скрыть информацию' : 'Показать информацию'}
              </TextButton>
            )}
          >
            <Description>{codex}</Description>
          </Collapsible>
        </MoreInfoContainer>
      )}
    </ScreenBlock>
  )
}
