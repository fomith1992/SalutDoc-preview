import { StackActions, useNavigation } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { useCommunityInfoQuery } from '../../gen/graphql'
import { AnimatedPlaceholder } from '../animated-placeholder/animated-placeholder'
import { FollowCommunityButton } from '../follow-community-button/follow-community-button.container'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { CommunityProfileView } from './community-profile.view'

interface CommunityProfileProps {
  communityId: string
}

export const CommunityProfile = ({ communityId }: CommunityProfileProps): React.ReactElement => {
  const navigation = useNavigation()

  const { data, loading, error } = useCommunityInfoQuery({
    variables: {
      communityId
    }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  return (
    <AnimatedPlaceholder
      placeholder={<LoadingIndicator visible />}
    >
      {data?.communityById == null || loading ? null
        : (
          <CommunityProfileView
            isMy={data.userMyself?.isCommunityAdmin ?? false}
            avatar={data.communityById.avatar ?? null}
            communityName={data.communityById.name}
            codex={data.communityById.codex}
            isPrivate={data.communityById.type === 'PRIVATE'}
            followersCount={data.communityById.membersCount ?? 0}
            handleMembers={() => navigation.dispatch(StackActions.push('Members', { communityId }))}
            handleEdit={() => navigation.dispatch(StackActions.push('EditCommunity', { communityId }))}
            handleInviteUsers={() => navigation.dispatch(StackActions.push('InviteUsers', { communityId }))}
            followCommunityButton={<FollowCommunityButton communityId={communityId} />}
          />
        )}
    </AnimatedPlaceholder>
  )
}
