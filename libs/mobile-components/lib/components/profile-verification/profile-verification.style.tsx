import { Text, View, ScrollView } from 'react-native'
import { styled } from '../../style/styled'
import { sp } from '../../style/size'
import { font } from '../../style/text'
import { color } from '../../style/color'

export const ScrollContainer = ScrollView

export const Spring = styled(View, {
  flex: 1
})

export const Body = styled(View, {
  paddingHorizontal: sp(8)
})

export const HeroInfoText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(24)
})

export const ApplicationInfoText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(16)
})

export const UploadButton = styled(View, {
  marginVertical: sp(16)
})

export const SendApplicationButton = styled(View, {
  marginVertical: sp(16)
})

export const Footer = styled(View, {
  paddingHorizontal: sp(8),
  marginTop: sp(12)
})

export const NeedHelpText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text'),
  textAlign: 'center'
})

export const Link = styled(Text, {
  ...font({ type: 'h3' }),
  color: color('accent'),
  padding: sp(12),
  textAlign: 'center'
})
