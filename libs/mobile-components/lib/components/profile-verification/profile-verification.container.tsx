import { ReactNativeFile } from 'apollo-upload-client'
import _ from 'lodash'
import React, { useState } from 'react'
import { Text } from 'react-native'
import { useCreateApplicationMutation, useProfileVerificationQuery } from '../../gen/graphql'
import { useRefresh } from '../../hooks/use-refresh'
import { Image, ImagePicker } from '../../modules/ui-kit'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { useAuthenticatedUser } from '../user-context/user-context-provider'
import { ProfileVerificationView } from './profile-verification.view'

const supportUrl = 'https://salutdoc.atlassian.net/servicedesk/customer/portal/1'

export function ProfileVerification (): React.ReactElement {
  const user = useAuthenticatedUser()

  const [diplomaImage, setDiplomaImage] = useState<Image>()

  const { data, loading, refetch } = useProfileVerificationQuery({
    variables: { userId: user.id },
    fetchPolicy: 'network-only'
  })

  const { refreshing, refresh } = useRefresh(refetch)

  const [createApplication, { loading: submitting }] = useCreateApplicationMutation()

  const onSendApplicationButtonPress = (): void => {
    if (diplomaImage != null) {
      createApplication({
        variables: {
          content: {
            userId: user.id,
            diplomaImage: new ReactNativeFile({
              uri: diplomaImage.uri,
              name: diplomaImage.fileName ?? 'diploma.jpeg',
              type: diplomaImage.mimeType
            })
          }
        }
      })
    }
  }

  if (data?.userById == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }

  const currentApplication = data.userById.lastVerificationApplication
  const diplomaImageEditable = currentApplication?.diplomaImage == null

  return (
    <ImagePicker
      title='Select diploma Image'
      onImagePicked={setDiplomaImage}
    >
      {({ pickImage }) => (
        <ProfileVerificationView
          submitting={submitting}
          refreshing={refreshing}
          refresh={refresh}
          currentApplication={currentApplication}
          diplomaImage={diplomaImage}
          supportHref={supportUrl}
          onReadImage={pickImage}
          onDiplomaImagePress={diplomaImageEditable ? pickImage : _.noop}
          onSendApplicationButtonPress={onSendApplicationButtonPress}
        />
      )}
    </ImagePicker>
  )
}
