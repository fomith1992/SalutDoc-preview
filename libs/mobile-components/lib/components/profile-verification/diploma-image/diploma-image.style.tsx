import { TouchableOpacity, Image } from 'react-native'
import { styled } from '../../../style/styled'
import { sp } from '../../../style/size'

interface AdjustableContainerProps {
  aspectRatio: number
}

export const DiplomaImageContainer = styled(TouchableOpacity, ({ aspectRatio }: AdjustableContainerProps) => ({
  marginTop: sp(12),
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  aspectRatio
} as const))

export const Img = styled(Image, ({ aspectRatio }: AdjustableContainerProps) => ({
  height: '100%',
  aspectRatio
} as const))
