import * as React from 'react'
import { DiplomaImageContainer, Img } from './diploma-image.style'
import useAsync from 'react-use/lib/useAsync'
import { Image } from 'react-native'

export interface DiplomaImageProps {
  uri: string
  onPress: () => void
}

export function DiplomaImage ({ uri, onPress }: DiplomaImageProps): React.ReactElement {
  const { value: size } = useAsync(async () => {
    return await new Promise<{ width: number, height: number }>((resolve, reject) => Image.getSize(
      uri,
      (width, height) => resolve({ width, height }),
      reject
    ))
  }, [uri])

  const imageAspectRatio = size == null ? 2 : Math.max(0.5, Math.min(2, size.width / size.height))
  const containerAspectRatio = Math.max(1, imageAspectRatio)

  return (
    <DiplomaImageContainer
      aspectRatio={containerAspectRatio}
      onPress={onPress}
    >
      <Img
        source={{ uri }}
        aspectRatio={imageAspectRatio}
      />
    </DiplomaImageContainer>
  )
}
