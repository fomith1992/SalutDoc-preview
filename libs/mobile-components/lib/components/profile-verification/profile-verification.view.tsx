import * as React from 'react'
import { RefreshControl } from 'react-native'
import { ExternalLinkText, Image, TextButton } from '../../modules/ui-kit'
import { ModalLoadingIndicator } from '../loading-indicator/modal-loading-indicator.view'
import { DiplomaImage } from './diploma-image/diploma-image.view'
import {
  ApplicationInfoText,
  Body,
  Spring,
  Footer,
  HeroInfoText,
  Link,
  NeedHelpText,
  SendApplicationButton,
  UploadButton,
  ScrollContainer
} from './profile-verification.style'

type Resolution = 'PENDING' | 'CONFIRMED' | 'REJECTED' | null

interface Application {
  id: string
  diplomaImage: string | null
  resolution: Resolution
  rejectReason: string | null
}

export interface ProfileVerificationViewProps {
  submitting: boolean
  refreshing: boolean
  refresh: () => void
  currentApplication: Application | null
  diplomaImage: Image | undefined
  supportHref: string
  onReadImage: () => void
  onDiplomaImagePress: () => void
  onSendApplicationButtonPress: () => void
}

interface Button {
  text: string
}

export function ProfileVerificationView (
  {
    submitting,
    refreshing,
    refresh,
    currentApplication,
    diplomaImage,
    supportHref,
    onReadImage,
    onDiplomaImagePress,
    onSendApplicationButtonPress
  }: ProfileVerificationViewProps
): React.ReactElement {
  let heroInfoText: string
  let applicationInfoText: string
  let uploadButton: Button | null
  let sendApplicationButton: Button | null

  if (currentApplication == null) {
    if (diplomaImage?.uri == null) {
      heroInfoText = 'Загрузите документ, подтверждающий Ваше образование'
      applicationInfoText = 'Загрузите фотографию диплома или сертификата в формате JPEG. Максимальный размер фотографии не должен превышать 2МБ'
      uploadButton = { text: 'Загрузить фотографию' }
      sendApplicationButton = null
    } else {
      heroInfoText = 'Документы загружены'
      applicationInfoText = 'Все верно?'
      uploadButton = null
      sendApplicationButton = { text: 'Отправить заявку' }
    }
  } else {
    switch (currentApplication.resolution) {
      case 'PENDING': {
        heroInfoText = 'Ваша заявка находится на рассмотрении'
        applicationInfoText = 'Пожалуйста, ожидайте подтверждения Ваших данных. При возникновении вопросов обратитесь в службу поддержки'
        uploadButton = null
        sendApplicationButton = null
        break
      }
      case 'REJECTED': {
        heroInfoText = 'Ваша заявка отклонена'
        applicationInfoText = currentApplication.rejectReason ?? ''
        if (diplomaImage == null) {
          uploadButton = { text: 'Отправить документы снова' }
          sendApplicationButton = null
        } else {
          uploadButton = null
          sendApplicationButton = { text: 'Отправить заявку' }
        }
        break
      }
      case 'CONFIRMED':
      case null:
        return <></>
    }
  }

  const diplomaImageSource = diplomaImage?.uri != null
    ? diplomaImage.uri
    : currentApplication?.diplomaImage != null
      ? currentApplication?.diplomaImage
      : null

  return (
    <ScrollContainer
      contentContainerStyle={{ minHeight: '100%' }}
      centerContent
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={refresh}
        />
      }
    >
      <Body>
        <HeroInfoText>
          {heroInfoText}
        </HeroInfoText>
        <ApplicationInfoText>
          {applicationInfoText}
        </ApplicationInfoText>
        {diplomaImageSource != null && (
          <DiplomaImage
            uri={diplomaImageSource}
            onPress={onDiplomaImagePress}
          />
        )}
        {uploadButton != null && (
          <UploadButton>
            <TextButton
              type='primary'
              size='XL'
              onPress={onReadImage}
            >
              {uploadButton.text}
            </TextButton>
          </UploadButton>
        )}
        {sendApplicationButton != null && (
          <SendApplicationButton>
            <TextButton
              type='primary'
              size='XL'
              onPress={onSendApplicationButtonPress}
            >
              {sendApplicationButton.text}
            </TextButton>
          </SendApplicationButton>
        )}
      </Body>
      <Spring />
      <Footer>
        <NeedHelpText>
          Если у Вас возникли вопросы, пожалуйста, напишите в службу поддержки
        </NeedHelpText>
        <Link>
          <ExternalLinkText href={supportHref}>
            Нужна помощь
          </ExternalLinkText>
        </Link>
      </Footer>
      <ModalLoadingIndicator visible={submitting} />
    </ScrollContainer>
  )
}
