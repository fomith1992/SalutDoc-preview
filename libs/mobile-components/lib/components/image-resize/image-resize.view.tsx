import React from 'react'
import { Image } from 'react-native'
import useAsync from 'react-use/lib/useAsync'
import { ImageResize, ImageResizeContainer, OverlayContainer } from './image-resize.style'

interface ImageResizeViewProps {
  uri: string
  overlay?: React.ReactElement
  onLoadingState?: (value: boolean) => void
}

export function ImageResizeView (
  {
    uri,
    overlay,
    onLoadingState
  }: ImageResizeViewProps
): React.ReactElement {
  const { value: size } = useAsync(async () => {
    return await new Promise<{ width: number, height: number }>((resolve, reject) => Image.getSize(
      uri,
      (width, height) => resolve({ width, height }),
      reject
    ))
  }, [uri])

  const origAspectRatio = size == null || !isFinite(size.height / size.width) ? 2 : size.height / size.width
  const imageAspectRatio = Math.max(1, Math.min(1, origAspectRatio))
  const containerAspectRatio = Math.max(1, imageAspectRatio)

  return (
    <ImageResizeContainer aspectRatio={containerAspectRatio}>
      <ImageResize
        onLoadEnd={() => onLoadingState != null ? onLoadingState(false) : {}}
        source={{
          uri
        }}
        aspectRatio={imageAspectRatio}
      />
      {overlay !== undefined ? (
        <OverlayContainer>
          {overlay}
        </OverlayContainer>) : <></>}
    </ImageResizeContainer>
  )
}
