import { View, Image } from 'react-native'
import { styled } from '../../style/styled'

interface ImageResizeContainerProps {
  aspectRatio: number
}

export const ImageResizeContainer = styled(View, ({ aspectRatio }: ImageResizeContainerProps) => ({
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  aspectRatio
}) as const)

interface ImageResizeProps {
  aspectRatio: number
}

export const ImageResize = styled(Image, ({ aspectRatio }: ImageResizeProps) => ({
  height: '100%',
  aspectRatio
}))

export const OverlayContainer = styled(View, {
  position: 'absolute',
  flex: 1
})
