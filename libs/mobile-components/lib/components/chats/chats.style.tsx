import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const Container = styled(View, {
  backgroundColor: color('surface')
})

export const Cover = styled(View, StyleSheet.absoluteFill, {
  backgroundColor: color('surface')
})

export const ChatContainer = styled(TouchableOpacity, {
  ...container(),
  flexDirection: 'row',
  marginTop: 16
})

export const AvatarContainer = styled(View, {
  width: 45,
  borderRadius: 4,
  overflow: 'hidden'
})

export const ChatInfo = styled(View, {
  flex: 1,
  paddingLeft: 12
})

export const ChatTitle = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between'
})

export const Name = styled(Text, {
  ...font({ type: 'text1', weight: 'strong' }),
  color: color('text')
})

export const Time = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const TextChatContainer = styled(View, {
  marginTop: 4,
  flexDirection: 'row'
})

export const AvatarMessageContainer = styled(View, {
  minWidth: 14,
  marginRight: 4,
  borderRadius: 4,
  overflow: 'hidden'
})

export const ContentMessage = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text')
})

export const TextMessage = styled(ContentMessage, (props: { isMy: boolean }) => ({
  paddingRight: props.isMy ? 55 : 42
}))

export const UnreadMessageContainer = styled(View, {
  backgroundColor: color('accent'),
  marginLeft: 'auto',
  width: 28,
  borderRadius: 3
})

export const UnreadMessage = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('surface'),
  textAlign: 'center'
})
