import React from 'react'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { Avatar } from '../avatar/avatar.view'

import {
  AvatarContainer,
  AvatarMessageContainer,
  ChatContainer,
  ChatInfo,
  ChatTitle,
  Container,
  ContentMessage,
  Name,
  TextChatContainer,
  TextMessage,
  Time,
  UnreadMessage,
  UnreadMessageContainer
} from './chats.style'

interface ChatsGroupProps {
  items: ChatItemProps[]
}

interface ChatItemProps {
  name: string
  time: string
  avatar?: string | null
  text: string
  isMy?: boolean
  unreadCount: number
}

interface ChatContentProps extends ChatItemProps {
  isGroup?: boolean
  onPress?: () => void
}

const ChatContent = (
  { name, time, avatar = null, text, unreadCount, isMy = false, isGroup = false, onPress }: ChatContentProps
): React.ReactElement => {
  return (
    <ChatContainer
      onPress={onPress}
    >
      <AvatarContainer>
        <Avatar url={avatar} />
      </AvatarContainer>
      <ChatInfo>
        <ChatTitle>
          <Name>
            {name}
          </Name>
          <Time>
            {time}
          </Time>
        </ChatTitle>
        <TextChatContainer>
          {isGroup ? (
            <AvatarMessageContainer>
              {isMy ? (
                <ContentMessage>Вы: </ContentMessage>
              ) : <Avatar url={avatar} />}
            </AvatarMessageContainer>
          ) : (
            isMy ? <ContentMessage>Вы: </ContentMessage> : <></>
          )}
          <TextMessage
            numberOfLines={1}
            isMy={isMy}
          >
            {text}
          </TextMessage>
          {
            isMy ? <></> : (
              <UnreadMessageContainer>
                <UnreadMessage>
                  {unreadCount}
                </UnreadMessage>
              </UnreadMessageContainer>
            )
          }
        </TextChatContainer>
      </ChatInfo>
    </ChatContainer>
  )
}

export const Chats = (
  { items }: ChatsGroupProps
): React.ReactElement => {
  const navigation = useStackNavigation()
  const chats = items.map((item, idx) => (
    <ChatContent
      {...item}
      key={idx}
      onPress={() => navigation.push('Chat', { chatId: '6020863b3407f754a0976664' })}
    />
  ))
  return (
    <Container>
      {chats}
    </Container>
  )
}
