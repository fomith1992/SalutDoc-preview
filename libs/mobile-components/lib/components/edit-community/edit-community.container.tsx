import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { Alert } from 'react-native'
import {
  useBasicCommunitySettingsQuery,
  useListCommunityAdminsQuery,
  useUpdateBasicCommunitySettingsMutation
} from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { AdminTitle } from './edit-community.style'
import { EditCommunityView } from './edit-community.view'

interface CommunityProfileProps {
  communityId: string
}

export function EditCommunity ({ communityId }: CommunityProfileProps): React.ReactElement {
  const navigation = useNavigation()
  const { data, loading } = useBasicCommunitySettingsQuery({
    variables: {
      communityId
    }
  })

  const listCommunityAdmins = useListCommunityAdminsQuery({
    variables: {
      communityId
    }
  })

  const [updateBasicCommunitySettings, { loading: submittingInfo }] = useUpdateBasicCommunitySettingsMutation({
    onCompleted () {
      navigation.navigate('Community', { communityId })
    }
  })

  const admins = listCommunityAdmins.data?.communityById?.admins?.map(admin => {
    return {
      id: admin.user.id,
      avatar: admin.user.avatar,
      title: `${admin.user.firstName} ${admin.user.lastName}`,
      description: admin.about,
      action: <AdminTitle>{admin.position}</AdminTitle>
    }
  })

  if (loading || listCommunityAdmins.loading) {
    return <LoadingIndicator visible />
  } else {
    return (
      <EditCommunityView
        initialValues={{
          codex: data?.communityById?.codex ?? undefined,
          network: data?.communityById?.network ?? undefined,
          name: data?.communityById?.name,
          description: data?.communityById?.description,
          specialization: data?.communityById?.specialization
        }}
        avatar={data?.communityById?.avatar ?? null}
        admins={admins}
        openAdminsScreen={() => navigation?.dispatch(StackActions.push('AddAdmins', { communityId }))}
        openFollowersScreen={() => navigation?.dispatch(StackActions.push('AddAdmins', { communityId }))}
        communityId={communityId}
        onSubmit={input => {
          updateBasicCommunitySettings({
            variables: { communityId, input }
          }).catch(() => {
            Alert.alert('Ошибка сохранения данных')
          })
          return {}
        }}
        submitting={submittingInfo}
      />
    )
  }
}
