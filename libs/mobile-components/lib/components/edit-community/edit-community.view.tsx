import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { HeaderSaveButton, TextInput, yup } from '../../modules/form-kit'
import { AvatarCommunityUpload } from '../avatar-upload/avatar-community-upload.container'
import { TextButton } from '../../modules/ui-kit'
import { List, ListItemProps } from '../list/list.view'
import { ModalLoadingIndicator } from '../loading-indicator/modal-loading-indicator.view'
import {
  AdminListContainer,
  AdminsTitleContainer,
  AdminTitle,
  AvatarContainer,
  Container,
  InfoContainer,
  MainContainer,
  Name,
  NameContainer
} from './edit-community.style'

const nameMaxLength = 100
const specializationMaxLength = 100
const descriptionMaxLength = 1000
const codexMaxLength = 1000
const networkMaxLength = 100

const formSchema = yup.object({
  name: yup.string()
    .required()
    .max(nameMaxLength),
  specialization: yup.string()
    .required()
    .max(specializationMaxLength),
  description: yup.string()
    .required()
    .max(descriptionMaxLength),
  codex: yup.string()
    .max(codexMaxLength)
    .ensure().defined(),
  network: yup.string()
    .max(networkMaxLength)
    .ensure().defined()
}).required()

export type EditCommunityFormData = yup.InferType<typeof formSchema>

const { Form, Field, Submit } = createFormDescriptor(formSchema)

interface EditCommunityViewProps {
  initialValues: Partial<EditCommunityFormData>
  openAdminsScreen: () => void
  openFollowersScreen: () => void
  avatar: string | null
  admins?: ListItemProps[]
  communityId: string
  onSubmit: SubmissionHandler<EditCommunityFormData>
  submitting: boolean
}

interface AdminsEditViewProps {
  admins: ListItemProps[] | undefined
  openAdminsScreen: () => void
  openFollowersScreen: () => void
}

const AdminsEdit = ({ admins, openAdminsScreen, openFollowersScreen }: AdminsEditViewProps): React.ReactElement => {
  return (
    <>
      <AdminsTitleContainer>
        <AdminTitle>
          Администраторы
        </AdminTitle>
        <TextButton
          type='link'
          onPress={openAdminsScreen}
        >
          править
        </TextButton>
      </AdminsTitleContainer>
      <AdminListContainer>
        <List items={admins ?? []} />
        <TextButton
          type='link'
          onPress={openFollowersScreen}
        >
          Добавить администратора
        </TextButton>
      </AdminListContainer>
    </>
  )
}

interface AvatarViewProps {
  avatar: string | null
  communityId: string
}

const Avatar = ({ avatar, communityId }: AvatarViewProps): React.ReactElement => {
  return (
    <AvatarContainer>
      <AvatarCommunityUpload
        avatarUrl={avatar}
        communityId={communityId}
      />
    </AvatarContainer>
  )
}

export const EditCommunityView = (
  {
    admins,
    initialValues,
    onSubmit,
    openFollowersScreen,
    openAdminsScreen,
    communityId,
    avatar,
    submitting
  }: EditCommunityViewProps
): React.ReactElement => {
  return (
    <Form
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      <Container>
        <Submit>
          {props => <HeaderSaveButton {...props} />}
        </Submit>
        <MainContainer>
          <Avatar avatar={avatar} communityId={communityId} />
          <NameContainer>
            <Field name='name'>
              {props => <Name>{props.value}</Name>}
            </Field>
          </NameContainer>
        </MainContainer>
        <InfoContainer>
          <Field name='description'>
            {props => (
              <TextInput
                {...props}
                label='Описание группы'
                maxLength={descriptionMaxLength}
                numberOfLines={[2, 4]}
              />
            )}
          </Field>
          <Field name='network'>
            {props => (
              <TextInput
                {...props}
                label='Сайт'
                maxLength={networkMaxLength}
              />
            )}
          </Field>
          <Field name='codex'>
            {props => (
              <TextInput
                {...props}
                label='Кодекс'
                maxLength={codexMaxLength}
                numberOfLines={[2, 6]}
              />
            )}
          </Field>
        </InfoContainer>
        <AdminsEdit
          admins={admins}
          openFollowersScreen={openFollowersScreen}
          openAdminsScreen={openAdminsScreen}
        />
      </Container>
      <ModalLoadingIndicator visible={submitting} />
    </Form>
  )
}
