import { ScrollView, Text, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const Container = styled(ScrollView, {
  flex: 1,
  backgroundColor: color('surface')
})

export const MainContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  marginTop: sp(16),
  marginBottom: sp(16)
})

export const InfoContainer = styled(View, {
  ...container()
})

export const AvatarContainer = styled(View, {
  height: sp(64),
  width: sp(64)
})

export const NameContainer = styled(View, {
  marginLeft: sp(24),
  flex: 1
})

export const Name = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const AdminsTitleContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: sp(12)
})

export const AdminTitle = styled(Text, {
  ...font({ type: 'h3' }),
  color: color('inactive')
})

export const AdminListContainer = styled(View, {
  marginBottom: sp(12)
})
