import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React, { useEffect } from 'react'
import { Alert, Text } from 'react-native'
import { useInsertAdminMutation, useListCommunityFollowersQuery, useRemoveAdminMutation } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { ActionTouchableContainer, AssignAdminIcon, UnassignAdminIcon } from './community-add-admins.style'
import { CommunityAddAdminsView } from './community-add-admins.view'

interface CommunityAddAdminsProps {
  communityId: string
}
export function CommunityAddAdmins (
  { communityId }: CommunityAddAdminsProps): React.ReactElement {
  const { data, loading, error, fetchMore, refetch } = useListCommunityFollowersQuery({
    variables: {
      communityId,
      first: 10
    }
  })

  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.communityById?.members?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousMembers = expectDefined(previousResult.communityById?.members)
        return {
          communityById: {
            ...expectDefined(previousResult.communityById),
            members: appendEdges(previousMembers, fetchMoreResult?.communityById?.members)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.communityById?.members?.pageInfo.endCursor])

  const [insertAdminMutation, { loading: submittingInserting }] = useInsertAdminMutation()
  const [removeAdminMutation, { loading: submittingRemoving }] = useRemoveAdminMutation()

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (data?.communityById?.members == null && !loading) {
    return <Text>Произошла ошибка</Text>
  }

  return (
    <CommunityAddAdminsView
      items={data?.communityById?.members?.edges.map(({ node }) => {
        return {
          id: node.user.id,
          avatar: node.user.avatar,
          title: `${node.user.firstName} ${node.user.lastName}`,
          description: node.user.specialization ?? '',
          action: node.user.isCommunityAdmin === true
            ? (
              <ActionTouchableContainer
                onPress={async () => {
                  await removeAdminMutation({
                    variables: {
                      communityId,
                      userId: node.user.id
                    }
                  }).catch(error => {
                    console.log(error)
                    Alert.alert('Error', 'Failed to remove')
                  })
                }}
              >
                <UnassignAdminIcon />
              </ActionTouchableContainer>
            ) : (
              <ActionTouchableContainer
                onPress={async () => {
                  await insertAdminMutation({
                    variables: {
                      communityId,
                      userId: node.user.id
                    }
                  }).catch(error => {
                    console.log(error)
                    Alert.alert('Error', 'Failed to insert')
                  })
                }}
              >
                <AssignAdminIcon />
              </ActionTouchableContainer>
            )
        }
      }) ?? []}
      refresh={refresh}
      loading={loading && !refreshing}
      hasMore={data?.communityById?.members?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore()}
      refreshing={refreshing}
      submitting={submittingInserting || submittingRemoving}
    />
  )
}
