import { TouchableOpacity } from 'react-native'
import { CrossIcon16 } from '../../../images/cross.icon-16'
import { PlusIcon16 } from '../../../images/plus.icon-16'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const AssignAdminIcon = styled(PlusIcon16, {
  color: color('accent'),
  width: sp(20),
  height: sp(20)
})

export const UnassignAdminIcon = styled(CrossIcon16, {
  color: color('subtext'),
  width: sp(20),
  height: sp(20)
})

export const ActionTouchableContainer = styled(TouchableOpacity, {
  padding: sp(8)
})
