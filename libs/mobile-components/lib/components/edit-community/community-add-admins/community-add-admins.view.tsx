import React from 'react'
import { InfiniteList, InfiniteListProps } from '../../list/list.view'

export function CommunityAddAdminsView (props: InfiniteListProps): React.ReactElement {
  return (
    <InfiniteList {...props} />
  )
}
