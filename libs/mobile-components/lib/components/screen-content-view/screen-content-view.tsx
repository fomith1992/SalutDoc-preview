import { useHeaderHeight } from '@react-navigation/stack'
import React from 'react'
import { KeyboardAvoidingView, Platform } from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import { color } from '../../style/color'
import { useMaterialized } from '../../style/styled'

export function ScreenContentView ({ children }: React.PropsWithChildren<{}>): React.ReactElement {
  const headerHeight = useHeaderHeight()
  const backgroundColor = useMaterialized(color('surface'))

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor }}>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        keyboardVerticalOffset={headerHeight}
      >
        {children}
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}
