import React from 'react'

export function ScreenContentView ({ children }: React.PropsWithChildren<{}>): React.ReactElement {
  return <>{children}</>
}
