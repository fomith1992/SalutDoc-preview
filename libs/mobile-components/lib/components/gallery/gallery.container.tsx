import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { Modal } from '../../modules/ui-kit'
import { Container } from './gallery.style'
import { GalleryItemProps, GalleryView } from './gallery.view'

export function Gallery (
  props: {
    attachments: GalleryItemProps[]
    showIndex: number
    galleryVisible: boolean
    onClose: () => void
  }
): React.ReactElement {
  const { attachments, galleryVisible, onClose, showIndex } = props
  return (
    <Modal
      animation='fade'
      visible={galleryVisible}
    >
      <Container>
        <SafeAreaView
          style={{ flex: 1 }}
        >
          <GalleryView
            data={attachments}
            showIndex={showIndex}
            onBackPress={onClose}
          />
        </SafeAreaView>
      </Container>
    </Modal>
  )
}
