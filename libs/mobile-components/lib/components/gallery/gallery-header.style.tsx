import { Text, View, TouchableOpacity } from 'react-native'
import { styled } from '../../style/styled'
import { ArrowIcon16 } from '../../images/arrow.icon-16'
import { font } from '../../style/text'
import { color } from '../../style/color'
import { sp } from '../../style/size'

export const HeaderContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: color('text')
})

export const HeaderBackImage = styled(ArrowIcon16, {
  color: color('surface')
})

export const HeaderLeftButton = styled(TouchableOpacity, {
  position: 'absolute',
  zIndex: 1,
  paddingLeft: sp(8),
  paddingRight: sp(8),
  height: '100%',
  paddingTop: sp(20)
})

export const HeaderTitle = styled(Text, {
  ...font({ type: 'h2' }),
  paddingTop: sp(16),
  paddingBottom: sp(16),
  color: color('surface'),
  flex: 1,
  textAlign: 'center'
})
