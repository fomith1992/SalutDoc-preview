import { ScrollView, Image, View } from 'react-native'
import { styled } from '../../style/styled'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { PlayIcon24 } from '../../images/play.icon-24'

export const Container = styled(View, {
  flex: 1,
  backgroundColor: color('backgroundDark')
})

export const GalleryContainer = styled(View, {
  flex: 1
})

export const GalleryScrollContainer = ScrollView

export const GalleryAttachmentContainer = styled(View, (props: { separatorSize: number }) => ({
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  marginRight: props.separatorSize
}) as const)

export const GalleryAttachment = styled(Image, (props: { width: number }) => ({
  width: props.width,
  height: '100%',
  resizeMode: 'center'
}) as const)

export const PlayVideoIconContainer = styled(View, {
  position: 'absolute',
  height: sp(96),
  width: sp(96),
  flexDirection: 'row',
  backgroundColor: 'rgba(165, 165, 165, 0.35)',
  borderRadius: sp(48),
  justifyContent: 'center',
  alignItems: 'center'
})

export const PlayIconSvg = styled(PlayIcon24, {
  width: sp(48),
  height: sp(48)
})

export const statusBarStyle = color('backgroundDark')
