import React from 'react'
import {
  HeaderContainer,
  HeaderLeftButton,
  HeaderBackImage,
  HeaderTitle
} from './gallery-header.style'

interface GalleryHeaderProps {
  onBackPressed: () => void
  title: string
}

export function GalleryHeaderView (
  {
    onBackPressed,
    title
  }: GalleryHeaderProps
): React.ReactElement {
  return (
    <HeaderContainer>
      <HeaderLeftButton
        onPress={onBackPressed}
      >
        <HeaderBackImage />
      </HeaderLeftButton>
      <HeaderTitle>
        {title}
      </HeaderTitle>
    </HeaderContainer>
  )
}
