import React, { useState, useRef, useEffect } from 'react'
import {
  GalleryContainer,
  GalleryScrollContainer,
  GalleryAttachmentContainer,
  GalleryAttachment,
  PlayVideoIconContainer,
  PlayIconSvg,
  statusBarStyle
} from './gallery.style'
import { GalleryHeaderView } from './gallery-header.view'
import { ScrollView, StatusBar } from 'react-native'
import { YoutubePlayerView } from '../youtube-player/youtube-player'
import { useMaterialized } from '../../style/styled'

interface YouTubeVideoPostAttachment {
  __typename: 'YouTubeVideoPostAttachment'
  videoId: string
}

interface ImagePostAttachment {
  __typename: 'ImagePostAttachment'
  url: string
}

export type GalleryItemProps = YouTubeVideoPostAttachment | ImagePostAttachment

export function PlayVideoIcon (): React.ReactElement {
  return (
    <PlayVideoIconContainer>
      <PlayIconSvg />
    </PlayVideoIconContainer>
  )
}

function GalleryItem (
  props: {
    url: string
    videoId?: string
    width: number
    separatorSize: number
    visible: boolean
  }
): React.ReactElement {
  const { url, videoId, separatorSize, width, visible } = props
  const [playVideo, setPlayVideo] = useState(false)
  useEffect(() => setPlayVideo(false), [visible])
  return (
    <GalleryAttachmentContainer
      separatorSize={separatorSize}
    >
      {videoId !== undefined ? (
        <YoutubePlayerView
          videoId={videoId}
          width={width}
          play={playVideo}
          onPlayChange={setPlayVideo}
        />
      ) : (
        <GalleryAttachment
          source={{
            uri: url ?? undefined
          }}
          width={width}
        />
      )}
    </GalleryAttachmentContainer>
  )
}

function getValidUrl (attachment: GalleryItemProps): string {
  switch (attachment.__typename) {
    case 'ImagePostAttachment':
      return attachment.url
    case 'YouTubeVideoPostAttachment':
      return `https://img.youtube.com/vi/${attachment.videoId}/hqdefault.jpg`
  }
}

export function GalleryView (
  props: {
    data: GalleryItemProps[]
    showIndex: number
    separatorSize?: number
    onBackPress: () => void
  }
): React.ReactElement {
  const { data, showIndex, separatorSize = 40, onBackPress } = props
  const [containerWidth, setContainerWidth] = useState<number>(0)
  const [indexShowAttachment, setIndexShowAttachment] = useState(showIndex)
  const scrollViewRef = useRef<ScrollView>(null)
  const statusBarColor = useMaterialized(statusBarStyle)
  useEffect(() => {
    scrollViewRef.current?.scrollTo({ x: (containerWidth + separatorSize) * (indexShowAttachment), animated: false })
  }, [scrollViewRef, containerWidth])

  return (
    <GalleryContainer
      onLayout={(event) => {
        const { width } = event.nativeEvent.layout
        setContainerWidth(width)
      }}
    >
      <StatusBar
        animated
        backgroundColor={statusBarColor}
        barStyle='light-content'
      />
      <GalleryHeaderView
        onBackPressed={onBackPress}
        title={`${indexShowAttachment + 1} из ${data.length}`}
      />
      <GalleryScrollContainer
        ref={scrollViewRef}
        decelerationRate='fast'
        horizontal
        snapToAlignment='center'
        snapToOffsets={
          data.length === 1 ? [containerWidth] : data.slice(1).map((_, i) => i * (containerWidth + separatorSize))
        }
        showsHorizontalScrollIndicator={false}
        disableIntervalMomentum
        onMomentumScrollEnd={e => {
          setIndexShowAttachment(Math.round(e.nativeEvent.contentOffset.x / containerWidth))
        }}
      >
        {data.map((item, index) => (
          <GalleryItem
            key={index}
            url={getValidUrl(item)}
            videoId={item.__typename === 'YouTubeVideoPostAttachment' ? item.videoId : undefined}
            width={containerWidth}
            separatorSize={data.length === index + 1 ? 0 : separatorSize}
            visible={indexShowAttachment === index}
          />
        ))}
      </GalleryScrollContainer>
    </GalleryContainer>
  )
}
