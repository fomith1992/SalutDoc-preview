import { createFormDescriptor } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { yup } from '../../../../modules/form-kit'
import { MaskedTextInput } from '../../../../modules/form-kit/text-input/masked-input'
import { ExternalLink, SubmitTextButton } from '../../../../modules/ui-kit'
import { useMaterialized } from '../../../../style/styled'
import {
  ContainerStyleSheet,
  RegisterContainer,
  RegisterDescription,
  ButtonContainer,
  InputContainer,
  BottomContainer,
  BottomText,
  BottomLink
} from './register-patient-number.style'

const formSchema = yup.object({
  phoneNumber: yup.string()
    .matches(/\+7 \(\d{3}\) \d{3}-\d\d-\d\d/, 'Введите корректный номер телефона')
    .required()
}).required()

const { Form, Field, FormSpy } = createFormDescriptor(formSchema)

interface RegisterPatientNumberViewProps {
  phoneNumber: string
  privacyPolicyHref: string
  termsOfServiceHref: string
  setPhoneNumber: (phoneNumber: string) => void
  setRawValue: (phoneNumber: string) => void
  onSubmit: () => void
}

export function RegisterPatientNumberView ({
  phoneNumber,
  privacyPolicyHref,
  termsOfServiceHref,
  setPhoneNumber,
  setRawValue,
  onSubmit
}: RegisterPatientNumberViewProps): React.ReactElement {
  const containerStyle = useMaterialized(ContainerStyleSheet)

  const [validForm, setValidForm] = useState(false)

  return (
    <Form
      onSubmit={() => ({})}
      initialValues={{
        phoneNumber
      }}
    >
      <FormSpy
        subscription={{ valid: true, values: true }}
        onChange={({ valid, values }) => {
          setPhoneNumber(values.phoneNumber)
          setValidForm(valid)
        }}
      />
      <RegisterContainer
        contentContainerStyle={containerStyle}
      >
        <RegisterDescription>
          Ваш номер телефона нужен для процедуры идентификации и безопасности ваших данных
        </RegisterDescription>
        <InputContainer>
          <Field name='phoneNumber'>
            {props => (
              <MaskedTextInput
                {...props}
                maxLength={18}
                type='cel-phone'
                placeholder='+7 (900) 000-00-00'
                mask='+7 (999) 999-99-99'
                getRawValue={setRawValue}
              />
            )}
          </Field>
        </InputContainer>
        <BottomContainer>
          <BottomText>
            Нажимая кнопку «Получить код» вы соглашаетесь с{' '}
            <ExternalLink href={privacyPolicyHref}>
              {({ onPress }) => (
                <BottomLink onPress={onPress}>
                  политикой конфиденциальности
                </BottomLink>
              )}
            </ExternalLink>
            {' '}и{' '}
            <ExternalLink href={termsOfServiceHref}>
              {({ onPress }) => (
                <BottomLink onPress={onPress}>
                  пользовательским соглашением
                </BottomLink>
              )}
            </ExternalLink>
          </BottomText>
        </BottomContainer>
        <ButtonContainer>
          <SubmitTextButton
            type='primary'
            size='XL'
            valid={validForm}
            onSubmit={onSubmit}
          >
            Получить код
          </SubmitTextButton>
        </ButtonContainer>
      </RegisterContainer>
    </Form>
  )
}
