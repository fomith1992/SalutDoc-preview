import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { TextInput, yup } from '../../../../modules/form-kit'
import { RadioButton } from '../../../../modules/form-kit/radio-button'
import { MaskedTextInput } from '../../../../modules/form-kit/text-input/masked-input'
import { SubmitTextButton } from '../../../../modules/ui-kit'
import {
  RadioButtonContainer,
  InputContainer,
  FormContainer,
  BottomContainer,
  RegisterDescription,
  Label
} from './register-patient-personal.style'

const firstNameMaxLength = 30

const formSchema = yup.object({
  birthday: yup.string()
    .min(10).max(10)
    .matches(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/, 'Введите корректную дату рождения')
    .required(),
  gender: yup.mixed()
    .required()
    .oneOf(['MALE', 'FEMALE'] as const),
  firstName: yup.string()
    .required()
    .max(firstNameMaxLength)
}).required()

export type EditProfileFormData = yup.InferType<typeof formSchema>

const { Form, Field, FormSpy } = createFormDescriptor(formSchema)

interface EditBasicInfoPatientViewProps {
  initialValues: Partial<EditProfileFormData>
  onChange: SubmissionHandler<EditProfileFormData>
  onSubmit: () => void
}

export function RegisterPatientPersonalView ({
  initialValues,
  onChange,
  onSubmit
}: EditBasicInfoPatientViewProps): React.ReactElement {
  const [validForm, setValidForm] = useState(false)

  return (
    <FormContainer>
      <RegisterDescription>
        Эта информация понадобится для создания вашей медкарты и проведения консультации
      </RegisterDescription>
      <Form
        initialValues={initialValues}
        onSubmit={() => ({})}
      >
        <FormSpy
          subscription={{ values: true, valid: true }}
          onChange={({ values, valid }) => {
            onChange(values)
            setValidForm(valid)
          }}
        />
        <InputContainer>
          <Field name='firstName'>
            {props => (
              <TextInput
                {...props}
                maxLength={firstNameMaxLength}
                label='Как к вам обращаться'
                placeholder='Например, Александра'
              />
            )}
          </Field>
        </InputContainer>
        <InputContainer>
          <Field name='birthday'>
            {props => (
              <MaskedTextInput
                {...props}
                maxLength={10}
                type='datetime'
                placeholder='ДД.ММ.ГГГГ'
                label='Дата рождения'
              />
            )}
          </Field>
        </InputContainer>
        <InputContainer>
          <Label>
            Пол
          </Label>
          <Field name='gender'>
            {props => (
              <RadioButtonContainer>
                <RadioButton
                  onPress={() => props.onChange('MALE')}
                  active={props.value === 'MALE'}
                  label='Мужчина'
                />
                <RadioButton
                  onPress={() => props.onChange('FEMALE')}
                  active={props.value === 'FEMALE'}
                  label='Женщина'
                />
              </RadioButtonContainer>
            )}
          </Field>
        </InputContainer>
        <BottomContainer>
          <SubmitTextButton
            type='primary'
            size='XL'
            valid={validForm}
            onSubmit={onSubmit}
          >
            Далее
          </SubmitTextButton>
        </BottomContainer>
      </Form>
    </FormContainer>
  )
}
