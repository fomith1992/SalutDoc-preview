import React from 'react'
import { StackNavigationProp } from '@react-navigation/stack'
import { useNavigation } from '@react-navigation/native'
import { useRootStore } from '../../../../stores/root.store'
import { RegisterPatientPersonalView } from './register-patient-personal.view'
import { toJS } from 'mobx'
import { ZonedDateTime } from '@js-joda/core'
import { expectDefined } from '@salutdoc/react-components'
import ky from 'ky/umd'
import moment from 'moment'
import { observer } from 'mobx-react'
import { Alert } from 'react-native'
import { useOidcFirebaseAuthentication } from '../../../../modules/oidc/oidc-firebase-authentication-context'

export const RegisterPatientPersonal = observer((): React.ReactElement => {
  const navigation = useNavigation()
  const { login } = useOidcFirebaseAuthentication()
  const FRONT_URL = expectDefined(process.env.FRONT_URL)
  const {
    token,
    firstName,
    birthday,
    rawPhoneNumber = '',
    gender,
    mode,
    setBirthday,
    setFirstName,
    setGender
  } = useRootStore().domains.registerReport

  function resolveActionMode (): void {
    switch (mode) {
      case 'DEFAULT':
        navigation.dangerouslyGetParent<StackNavigationProp<any>>()?.goBack()
        break
      case 'IN_CONSULTATION':
        navigation.dangerouslyGetParent<StackNavigationProp<any>>()?.goBack()
        navigation.navigate('NewConsultation', {})
        break
    }
  }

  const register = async (): Promise<boolean> => {
    try {
      await ky.post(new URL('api/v1/registration/users/register', FRONT_URL), {
        json: {
          role: 'USER',
          phoneNumber: `+${rawPhoneNumber}`,
          phoneNumberCertificate: token,
          firstName,
          lastName: ' ',
          gender,
          birthday: ZonedDateTime.parse(moment(birthday, 'DD.MM.YYYY').toISOString()).toLocalDate()
        }
      })
      return true
    } catch (err) {
      Alert.alert(
        'Ошибка',
        'Произошла ошибка'
      )
      console.log('err', err)
      return false
    }
  }

  function handleSubmit (): void {
    if (token == null) return console.log('token is null')
    register().then(async result => {
      await login(token)
      if (result) resolveActionMode()
    }).catch(console.log)
  }

  return (
    <RegisterPatientPersonalView
      initialValues={toJS({ firstName, birthday, gender })}
      onChange={values => {
        setBirthday(values.birthday)
        setFirstName(values.firstName)
        setGender(values.gender)
        return {}
      }}
      onSubmit={handleSubmit}
    />
  )
})
