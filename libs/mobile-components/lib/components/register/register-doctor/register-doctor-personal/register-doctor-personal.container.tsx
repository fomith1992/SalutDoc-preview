import React from 'react'
import { RegisterDoctorPersonalView } from './register-doctor-personal.view'
import { useRootStore } from '../../../../stores/root.store'
import { toJS } from 'mobx'
import { ZonedDateTime } from '@js-joda/core'
import { expectDefined } from '@salutdoc/react-components'
import ky from 'ky/umd'
import moment from 'moment'
import { observer } from 'mobx-react'
import { Alert } from 'react-native'
import { useOidcFirebaseAuthentication } from '../../../../modules/oidc/oidc-firebase-authentication-context'
import { useStackNavigation } from '../../../../navigation/use-stack-navigation'

export const RegisterDoctorPersonal = observer((): React.ReactElement => {
  const navigation = useStackNavigation()
  const { login } = useOidcFirebaseAuthentication()
  const FRONT_URL = expectDefined(process.env.FRONT_URL)
  const {
    token,
    rawPhoneNumber = '',
    firstName,
    birthday,
    gender,
    lastName,
    specialization,
    setLastName,
    setBirthday,
    setFirstName,
    setGender,
    setSpecialization
  } = useRootStore().domains.registerReport

  const register = async (): Promise<boolean> => {
    try {
      await ky.post(new URL('api/v1/registration/users/register', FRONT_URL), {
        json: {
          role: 'DOCTOR',
          phoneNumber: `+${rawPhoneNumber}`,
          phoneNumberCertificate: token,
          firstName,
          lastName,
          gender,
          birthday: ZonedDateTime.parse(moment(birthday, 'DD.MM.YYYY').toISOString()).toLocalDate()
        }
      })
      return true
    } catch (err) {
      Alert.alert(
        'Ошибка',
        'Произошла ошибка'
      )
      console.log('err', err)
      return false
    }
  }

  function handleSubmit (): void {
    if (token == null) return console.log('token is null')
    register().then(async result => {
      await login(token)
      if (result) {
        navigation.reset({
          index: 0,
          routes: [{ name: 'WelcomePage' }]
        })
      }
    }).catch(console.log)
  }

  return (
    <RegisterDoctorPersonalView
      initialValues={toJS({ firstName, birthday, gender, lastName, specialization })}
      onChange={values => {
        setBirthday(values.birthday)
        setFirstName(values.firstName)
        setGender(values.gender)
        setLastName(values.lastName)
        setSpecialization(values.specialization)
        return {}
      }}
      onSubmit={handleSubmit}
    />
  )
})
