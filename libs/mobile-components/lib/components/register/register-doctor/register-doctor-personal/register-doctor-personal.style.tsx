import { View, Text, ScrollView } from 'react-native'
import { color } from '../../../../style/color'
import { sp } from '../../../../style/size'
import { lift, styled } from '../../../../style/styled'
import { font } from '../../../../style/text'
import { container } from '../../../../style/view'

export const formContainerStyleSheet = lift({
  flexGrow: 1
})

export const InputContainer = styled(View, {
  marginTop: sp(24)
})

export const Spring = styled(View, {
  marginTop: sp(24)
})

export const BottomContainer = styled(View, {
  marginTop: 'auto',
  marginBottom: sp(16)
})

export const RadioButtonContainer = styled(View, {
  flexDirection: 'row'
})

export const Label = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const FormContainer = styled(ScrollView, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface'),
  paddingBottom: sp(16)
})

export const RegisterDescription = styled(Text, {
  marginTop: sp(24),
  ...font({ type: 'text1' }),
  color: color('subtext')
})
