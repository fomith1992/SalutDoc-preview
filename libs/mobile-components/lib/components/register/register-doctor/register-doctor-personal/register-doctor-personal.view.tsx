import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { TextInput, yup } from '../../../../modules/form-kit'
import { RadioButton } from '../../../../modules/form-kit/radio-button'
import { SelectSearch } from '../../../../modules/form-kit/select-search/select-search.view'
import { MaskedTextInput } from '../../../../modules/form-kit/text-input/masked-input'
import { specializationsData, SubmitTextButton } from '../../../../modules/ui-kit'
import { useMaterialized } from '../../../../style/styled'
import {
  RadioButtonContainer,
  InputContainer,
  FormContainer,
  BottomContainer,
  RegisterDescription,
  Label,
  Spring,
  formContainerStyleSheet
} from './register-doctor-personal.style'

const firstNameMaxLength = 30
const lastNameMaxLength = 30
const specializationMaxLength = 100

const formSchema = yup.object({
  birthday: yup.string()
    .min(10).max(10)
    .matches(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/, 'Введите корректную дату рождения')
    .required(),
  gender: yup.mixed()
    .required()
    .oneOf(['MALE', 'FEMALE'] as const),
  firstName: yup.string()
    .required()
    .max(firstNameMaxLength),
  lastName: yup.string()
    .required()
    .max(lastNameMaxLength),
  specialization: yup.string()
    .max(specializationMaxLength)
    .ensure().defined()
}).required()

export type EditProfileFormData = yup.InferType<typeof formSchema>

const { Form, Field, FormSpy } = createFormDescriptor(formSchema)

interface EditBasicInfoPatientViewProps {
  initialValues: Partial<EditProfileFormData>
  onChange: SubmissionHandler<EditProfileFormData>
  onSubmit: () => void
}

export function RegisterDoctorPersonalView ({
  initialValues,
  onSubmit,
  onChange
}: EditBasicInfoPatientViewProps): React.ReactElement {
  const formContainerStyle = useMaterialized(formContainerStyleSheet)
  const [validForm, setValidForm] = useState(false)

  return (
    <FormContainer
      contentContainerStyle={formContainerStyle}
    >
      <RegisterDescription>
        Эта информация нужна нам для того, чтобы пациенты могли больше доверять сервису.
      </RegisterDescription>
      <Form
        initialValues={initialValues}
        onSubmit={() => ({})}
      >
        <FormSpy
          subscription={{ values: true, valid: true }}
          onChange={({ values, valid }) => {
            onChange(values)
            setValidForm(valid)
          }}
        />
        <InputContainer>
          <Field name='firstName'>
            {props => (
              <TextInput
                {...props}
                maxLength={firstNameMaxLength}
                label='Имя'
                placeholder='Например, Александра'
              />
            )}
          </Field>
        </InputContainer>
        <InputContainer>
          <Field name='lastName'>
            {props => (
              <TextInput
                {...props}
                maxLength={firstNameMaxLength}
                label='Фамилия'
                placeholder='Например, Иванова'
              />
            )}
          </Field>
        </InputContainer>
        <InputContainer>
          <Field name='specialization'>
            {props => (
              <SelectSearch
                {...props}
                options={specializationsData}
                label='Специальность'
                type='input'
              />
            )}
          </Field>
        </InputContainer>
        <InputContainer>
          <Field name='birthday'>
            {props => (
              <MaskedTextInput
                {...props}
                maxLength={10}
                type='datetime'
                placeholder='ДД.ММ.ГГГГ'
                label='Дата рождения'
              />
            )}
          </Field>
        </InputContainer>
        <InputContainer>
          <Label>
            Пол
          </Label>
          <Field name='gender'>
            {props => (
              <RadioButtonContainer>
                <RadioButton
                  onPress={() => props.onChange('MALE')}
                  active={props.value === 'MALE'}
                  label='Мужчина'
                />
                <RadioButton
                  onPress={() => props.onChange('FEMALE')}
                  active={props.value === 'FEMALE'}
                  label='Женщина'
                />
              </RadioButtonContainer>
            )}
          </Field>
        </InputContainer>
        <Spring />
        <BottomContainer>
          <SubmitTextButton
            type='primary'
            size='XL'
            valid={validForm}
            onSubmit={onSubmit}
          >
            Далее
          </SubmitTextButton>
        </BottomContainer>
      </Form>
    </FormContainer>
  )
}
