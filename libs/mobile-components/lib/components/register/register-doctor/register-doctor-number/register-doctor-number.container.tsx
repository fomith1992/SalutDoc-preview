import { RegisterDoctorNumberView } from './register-doctor-number.view'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React from 'react'
import { useStackNavigation } from '../../../../navigation/use-stack-navigation'
import { useRootStore } from '../../../../stores/root.store'
import { expectDefined } from '@salutdoc/react-components'
import ky from 'ky/umd'
import { Alert } from 'react-native'
import { useLogoutFirebase } from '../../../../hooks/firebase/use-logout-firebase'

const termsOfServiceUrl = 'https://about.salutdoc.com/terms-of-service'
const privacyPolicyUrl = 'https://about.salutdoc.com/privacy/'

export const RegisterDoctorNumber = observer((): React.ReactElement => {
  const FRONT_URL = expectDefined(process.env.FRONT_URL)
  const {
    phoneNumber,
    setPhoneNumber,
    setRawPhoneNumber
  } = useRootStore().domains.registerReport
  const navigation = useStackNavigation()

  useLogoutFirebase()

  return (
    <RegisterDoctorNumberView
      phoneNumber={toJS(phoneNumber ?? '')}
      setPhoneNumber={setPhoneNumber}
      setRawValue={setRawPhoneNumber}
      privacyPolicyHref={privacyPolicyUrl}
      termsOfServiceHref={termsOfServiceUrl}
      onSubmit={async () => {
        try {
          await ky.post(new URL('/api/v1/registration/phone/verifications', FRONT_URL), {
            json: {
              phoneNumber: phoneNumber,
              existingAccount: false
            }
          })
          navigation.navigate('Register_DoctorVerification', {})
        } catch (e) {
          console.error(e)
          if (e instanceof ky.HTTPError && e.response.status === 400) {
            Alert.alert(
              'Ошибка',
              'Профиль с указанным мобильным телефоном уже существует'
            )
          } else {
            Alert.alert(
              'Ошибка',
              'Повторите позже'
            )
          }
        }
      }}
    />
  )
})
