import { RegisterDoctorVerificationView } from './register-doctor-verification.view'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React, { useEffect, useState } from 'react'
import { useStackNavigation } from '../../../../navigation/use-stack-navigation'
import { useRootStore } from '../../../../stores/root.store'
import { FirebaseAuthTypes } from '@react-native-firebase/auth'
import { Alert } from 'react-native'
import { ModalLoadingIndicator } from '../../../loading-indicator/modal-loading-indicator.view'
import { useAuthFirebase } from '../../../../hooks/firebase/use-auth-firebase'

export const RegisterDoctorVerification = observer((): React.ReactElement => {
  const {
    phoneNumber, setToken
  } = useRootStore().domains.registerReport
  const [timerRemaining, setTimerRemaining] = useState<number>(Date.now() + 60000)
  const navigation = useStackNavigation()
  const { loading, error, confirmCode, resend } = useAuthFirebase({
    phoneNumber: phoneNumber ?? null,
    onSuccess: (user: FirebaseAuthTypes.User) => {
      user.getIdToken(true)
        .then(
          token => {
            setToken(token)
            navigation.navigate('Register_DoctorPersonal', {})
          })
        .catch(console.log)
    }
  })

  useEffect(() => {
    if (error != null) {
      switch (error) {
        case 'auth/invalid-phone-number':
          navigation.goBack()
          return Alert.alert(
            'Ошибка',
            'Невалидный номер телефона. Проверьте правильность введенного номера'
          )
        case 'auth/too-many-requests':
          navigation.goBack()
          return Alert.alert(
            'Ошибка',
            'Вы превысили допустимую квоту смс сообщений. Попробуйте позднее'
          )
        case 'auth/invalid-verification-code':
          return Alert.alert(
            'Ошибка',
            'Неверный код.'
          )
        default:
          navigation.goBack()
          return Alert.alert(
            'Ошибка',
            'Попробуйте позднее'
          )
      }
    }
  }, [error])

  async function saveUser (code: string): Promise<void> {
    try {
      confirmCode(code)
    } catch (error) {
      Alert.alert(
        'Ошибка',
        'Неверный код'
      )
    }
  }

  function handleSubmit (code: string): void {
    saveUser(code).catch(e => console.log('err: ', e))
  }

  function handleResend (): void {
    resend()
    setTimerRemaining(Date.now() + 60000)
  }

  return (
    <>
      <RegisterDoctorVerificationView
        phoneNumber={toJS(phoneNumber ?? '')}
        resendCodeAfter={timerRemaining}
        onSubmit={handleSubmit}
        resendMessageCode={handleResend}
      />
      <ModalLoadingIndicator
        visible={loading}
      />
    </>
  )
})
