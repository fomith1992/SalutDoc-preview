import React, { useState } from 'react'
import { toCaseCount } from '../../../../i18n/utils'
import { TextButton } from '../../../../modules/ui-kit'
import { useMaterialized } from '../../../../style/styled'
import {
  ContainerStyleSheet,
  RegisterContainer,
  RegisterDescription,
  ButtonContainer,
  InputContainer,
  BottomContainer,
  BottomText,
  CellStyleSheet,
  CellFocusedStyleSheet,
  TextStyleSheet
} from './register-doctor-verification.style'
import useInterval from 'react-use/lib/useInterval'
import useUpdate from 'react-use/lib/useUpdate'
import { PinCodeInput } from '../../../../modules/form-kit/pin-code-input/pin-code-input.view'

interface RegisterDoctorVerificationViewProps {
  phoneNumber: string
  resendCodeAfter: number
  onSubmit: (value: string) => void
  resendMessageCode: () => void
}
const pinCodeLength = 6

export function RegisterDoctorVerificationView ({
  phoneNumber,
  resendCodeAfter,
  resendMessageCode,
  onSubmit
}: RegisterDoctorVerificationViewProps): React.ReactElement {
  const [verificationCode, setVerificationCode] = useState('')

  const containerStyle = useMaterialized(ContainerStyleSheet)

  const cellStyle = useMaterialized(CellStyleSheet)
  const cellFocusedStyle = useMaterialized(CellFocusedStyleSheet)
  const textStyle = useMaterialized(TextStyleSheet)

  let timerRemaining = Math.round((resendCodeAfter - Date.now()) / 1000)
  useInterval(useUpdate(), timerRemaining > 0 ? 1000 : null)

  return (
    <RegisterContainer
      contentContainerStyle={containerStyle}
    >
      <RegisterDescription>
        Смс с кодом подтверждения было отправлено на номер {phoneNumber}.
      </RegisterDescription>
      <InputContainer>
        <PinCodeInput
          codeLength={pinCodeLength}
          cellStyle={cellStyle}
          cellStyleFocused={cellFocusedStyle}
          textStyle={textStyle}
          value={verificationCode}
          onTextChange={setVerificationCode}
          autoFocus
        />
      </InputContainer>
      <BottomContainer>
        {timerRemaining > 0
          ? (
            <BottomText>
              {`Отправить смс повторно можно через ${timerRemaining} ${toCaseCount(['секунду', 'секунды', 'секунд'], timerRemaining)}`}
            </BottomText>
          ) : (
            <TextButton
              type='link'
              onPress={() => {
                resendMessageCode()
                timerRemaining = 60
                setVerificationCode('')
              }}
            >
              Отправить смс повторно
            </TextButton>
          )}
      </BottomContainer>
      <ButtonContainer>
        <TextButton
          disabled={pinCodeLength !== verificationCode.length}
          onPress={() => onSubmit(verificationCode)}
          type='primary'
          size='XL'
        >
          Подтвердить
        </TextButton>
      </ButtonContainer>
    </RegisterContainer>
  )
}
