import React from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import {
  BottomDropDownMenu,
  Option,
  Overlay,
  Title,
  TickIconSvg,
  TopDropDownMenu
} from './drop-down.style'

export interface DropDownItemProps {
  title?: string
  onPress?: () => void
  icon?: React.ReactElement
  active?: boolean
}

export interface DropDownProps {
  data: DropDownItemProps[]
  onClose: () => void
  bottom?: boolean
}

export function DropDown (
  {
    data,
    bottom = false,
    onClose
  }: DropDownProps
): React.ReactElement {
  const options = data.map(({ title, onPress, icon, active = false }) => (
    <Option key={title} onPress={onPress}>
      {icon}
      <Title withIcon={icon != null}>{title}</Title>
      {active && (<TickIconSvg />)}
    </Option>
  ))
  return (
    <TouchableWithoutFeedback onPress={onClose}>
      <Overlay>
        {bottom ? (
          <BottomDropDownMenu>
            {options}
          </BottomDropDownMenu>
        ) : (
          <TopDropDownMenu>
            {options}
          </TopDropDownMenu>
        )}
      </Overlay>
    </TouchableWithoutFeedback>
  )
}
