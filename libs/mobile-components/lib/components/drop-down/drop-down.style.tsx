import { SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { TickIcon24 } from '../../images/tick.icon-24'

const DropDownMenu = styled(View, {
  backgroundColor: color('surface'),
  borderRadius: 12,
  marginLeft: sp(16),
  width: '50%',
  shadowColor: color('shadow'),
  paddingVertical: sp(4),
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 3,
  elevation: 5
})

export const TopDropDownMenu = styled(DropDownMenu, {
  marginTop: sp(64)
})

export const BottomDropDownMenu = styled(DropDownMenu, {
  marginTop: 'auto',
  marginBottom: sp(64)
})

export const Option = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: sp(16),
  paddingVertical: sp(8)
})

export const Title = styled(Text, (props: { withIcon: boolean }) => ({
  ...font({ type: 'h4' }),
  color: color('text'),
  flex: 1,
  marginLeft: props.withIcon ? sp(8) : 0
}))

export const TickIconSvg = styled(TickIcon24, {
  width: sp(16),
  height: sp(12),
  color: color('accent')
})

export const Overlay = styled(SafeAreaView, StyleSheet.absoluteFill)
