import { Text, View, TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { CrossIcon24 } from '../../../images/cross.icon-24'
import { SpecializationIcon } from '../../../modules/consultations/specialization-icon'

export const Container = styled(View, {
  flex: 1,
  backgroundColor: color('background')
})

export const ConsultationCloseButton = styled(TouchableOpacity, {
  ...container('padding'),
  marginTop: sp(8),
  paddingVertical: sp(12),
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: color('surface')
})

export const CrossSvg = styled(CrossIcon24, {
  color: color('accent')
})

export const ConsultationCloseButtonText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginLeft: sp(16)
})

export const ConsultationInfoContainer = styled(View, {
  ...container('padding'),
  marginTop: sp(8),
  paddingVertical: sp(24),
  backgroundColor: color('surface')
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const PatientPreview = styled(TouchableOpacity, {
  marginTop: sp(16),
  flexDirection: 'row',
  alignItems: 'center'
})

export const InfoContainer = styled(View, {
  marginLeft: sp(16)
})

export const UserName = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const UserDescription = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext')
})

export const ComplaintsDescription = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(16)
})

export const IconContainer = styled(View, {
  width: sp(56),
  height: sp(56),
  borderRadius: sp(28),
  alignItems: 'center',
  justifyContent: 'center',
  borderWidth: 1,
  borderColor: color('inactiveLight')
})

export const StyledSpecializationIcon = styled(SpecializationIcon, {
  width: sp(28),
  height: sp(28),
  color: color('inactive')
})
