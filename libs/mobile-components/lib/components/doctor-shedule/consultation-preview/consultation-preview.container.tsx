import React, { useState } from 'react'
import { DoctorShedulePopUp } from '../doctor-shedule-pop-up/doctor-shedule-pop-up.view'
import { ScheduleConsultationPreviewView } from './consultation-preview.view'

const data = {
  date: '15 апреля 18:30',
  specialization: 'allergologist',
  userName: 'Екатерина',
  age: '29 лет',
  gender: 'FEMALE',
  complaints: 'Кашель, температуа и сильный насморк'
}

export function ScheduleConsultationPreview (): React.ReactElement {
  const [popUpVisible, setPopUpVisible] = useState(false)
  return (
    <>
      <ScheduleConsultationPreviewView
        data={data}
        onCancelConsultation={() => setPopUpVisible(true)}
      />
      <DoctorShedulePopUp
        visible={popUpVisible}
        title='Отменить консультацию?'
        description={'После этого действие вернуть \nневозможно'}
        cancelButton={{
          title: 'Нет, оставить',
          onCancel: () => setPopUpVisible(false)
        }}
        submitButton={{
          title: 'Все равно убрать',
          onSubmit: () => setPopUpVisible(false)
        }}
      />
    </>
  )
}
