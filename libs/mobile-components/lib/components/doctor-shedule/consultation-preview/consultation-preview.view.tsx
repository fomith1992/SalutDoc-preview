import React from 'react'
import {
  ConsultationCloseButton,
  Container,
  CrossSvg,
  ConsultationCloseButtonText,
  ConsultationInfoContainer,
  Title,
  PatientPreview,
  InfoContainer,
  UserName,
  UserDescription,
  ComplaintsDescription,
  IconContainer,
  StyledSpecializationIcon
} from './consultation-preview.style'

interface ScheduleConsultationPreviewViewProps {
  onCancelConsultation: () => void
  data: {
    date: string
    specialization: string
    userName: string
    age: string
    gender: string
    complaints: string
  }
}

export function ScheduleConsultationPreviewView ({
  data,
  onCancelConsultation
}: ScheduleConsultationPreviewViewProps): React.ReactElement {
  const { date, specialization, userName, age, gender, complaints } = data
  return (
    <Container>
      <ConsultationCloseButton
        onPress={onCancelConsultation}
      >
        <CrossSvg />
        <ConsultationCloseButtonText>
          Отменить консультацию
        </ConsultationCloseButtonText>
      </ConsultationCloseButton>
      <ConsultationInfoContainer>
        <Title>
          {date}
        </Title>
        <PatientPreview
          onPress={() => {}}
        >
          <IconContainer>
            <StyledSpecializationIcon
              specialization={specialization}
            />
          </IconContainer>
          <InfoContainer>
            <UserName>
              {userName}
            </UserName>
            <UserDescription>
              {`${age}, ${gender === 'MALE' ? 'мужчина' : 'женщина'}`}
            </UserDescription>
          </InfoContainer>
        </PatientPreview>
      </ConsultationInfoContainer>
      <ConsultationInfoContainer>
        <Title>
          Жалобы
        </Title>
        <ComplaintsDescription>
          {complaints}
        </ComplaintsDescription>
      </ConsultationInfoContainer>
    </Container>
  )
}
