import { createFormDescriptor } from '@salutdoc/react-components'
import moment from 'moment'
import React, { useState } from 'react'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { SelectInput, yup } from '../../../modules/form-kit'
import { CheckBox } from '../../../modules/form-kit/check-box/check-box'
import { RadioButton } from '../../../modules/form-kit/radio-button'
import { SubmitTextButton } from '../../../modules/ui-kit'
import {
  Container,
  FormContainer,
  InputContainer,
  CheckBoxContainer,
  CheckBoxText,
  SaveInfoTitle,
  Title,
  SaveInfoContainer,
  RadioButtonContainer,
  ButtonContainer
} from './consultation-edit-shedule.style'

const formSchema = yup.object({
  workFrom: yup.string()
    .required(),
  workTo: yup.string()
    .required()
}).required()

export type EditSheduleViewFormData = yup.InferType<typeof formSchema>

const { Form, Field, Submit } = createFormDescriptor(formSchema)

interface ConsultationEditSheduleViewProps {
  onSubmit: () => void
  onCloseAppointment: () => void
  startDate: Date
  endDate: Date
  consultationStep: number
}

export function ConsultationEditSheduleView ({
  onSubmit,
  onCloseAppointment,
  startDate,
  endDate,
  consultationStep
}: ConsultationEditSheduleViewProps): React.ReactElement {
  const [checkBoxState, setCheckBoxState] = useState(false)
  const { state: isThisWeekChoos, setTrue: chooseAllWeeks, setFalse: chooseThisWeek } = useBooleanFlag(false)

  const selectInputOptions = (start: Date, end: Date, step: number): Array<{label: string, value: string}> => {
    const data = []
    let i = moment(start)
    while (i < moment(end)) {
      data.push({ label: i.format('HH:mm'), value: i.format('YYYY-MM-DD HH:mm') })
      i = i.add(step, 'milliseconds')
    }
    return data
  }

  return (
    <Container>
      <Form
        onSubmit={() => {
          onSubmit()
          return {}
        }}
      >
        <FormContainer>
          <Title>
            16 апреля, вторник
          </Title>
          <InputContainer>
            <Field name='workFrom'>
              {props => (
                <SelectInput
                  {...props}
                  options={selectInputOptions(startDate, endDate, consultationStep)}
                  disabled={checkBoxState}
                  hideIcons
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='workTo'>
              {props => (
                <SelectInput
                  {...props}
                  options={selectInputOptions(startDate, endDate, consultationStep)}
                  disabled={checkBoxState}
                  hideIcons
                />
              )}
            </Field>
            <CheckBoxContainer>
              <CheckBox
                value={checkBoxState}
                onChange={() => setCheckBoxState(!checkBoxState)}
              />
              <CheckBoxText>
                Не принимать в этот день
              </CheckBoxText>
            </CheckBoxContainer>
          </InputContainer>
        </FormContainer>
        <SaveInfoContainer>
          <SaveInfoTitle>
            Как сохранить изменения?
          </SaveInfoTitle>
          <RadioButtonContainer>
            <RadioButton
              onPress={chooseThisWeek}
              active={!isThisWeekChoos}
              label='Только на эту неделю'
            />
          </RadioButtonContainer>
          <RadioButtonContainer>
            <RadioButton
              onPress={chooseAllWeeks}
              active={isThisWeekChoos}
              label='На эту и последующие недели'
            />
          </RadioButtonContainer>
        </SaveInfoContainer>
        <ButtonContainer>
          <Submit>
            {props => (
              <SubmitTextButton
                size='XL'
                type='primary'
                valid={props.valid || checkBoxState}
                onSubmit={() => {
                  checkBoxState
                    ? onCloseAppointment()
                    : props.onSubmit()
                }}
              >
                Сохранить
              </SubmitTextButton>
            )}
          </Submit>
        </ButtonContainer>
      </Form>
    </Container>
  )
}
