import React, { useState } from 'react'
import { DoctorShedulePopUp } from '../doctor-shedule-pop-up/doctor-shedule-pop-up.view'
import { ConsultationEditSheduleView } from './consultation-edit-shedule.view'
import moment from 'moment'

const CONSULTATION_STEP_MS = 3600000 // 1 hour

export function ConsultationEditShedule (): React.ReactElement {
  const [popUpVisible, setPopUpVisible] = useState(false)

  const date = moment('2021-05-20 0:00', 'YYYY-MM-DD HH:mm').toDate()
  return (
    <>
      <ConsultationEditSheduleView
        onSubmit={() => {}}
        onCloseAppointment={() => setPopUpVisible(true)}
        startDate={date}
        endDate={moment(date).add(1, 'd').toDate()}
        consultationStep={CONSULTATION_STEP_MS}
      />
      <DoctorShedulePopUp
        visible={popUpVisible}
        title='Убрать прием в этот день?'
        description='У вас забронирована консультация на это время и при изменении времени эта консультация выпадет'
        cancelButton={{
          title: 'Не убирать',
          onCancel: () => setPopUpVisible(false)
        }}
        submitButton={{
          title: 'Убрать',
          onSubmit: () => setPopUpVisible(false)
        }}
      />
    </>
  )
}
