import { Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const Container = styled(View, {
  flex: 1,
  backgroundColor: color('background')
})

export const InputContainer = styled(View, {
  marginTop: sp(20)
})

export const FormContainer = styled(View, {
  ...container('padding'),
  paddingVertical: sp(24),
  backgroundColor: color('surface')
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const UserName = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const CheckBoxText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginLeft: sp(16)
})

export const CheckBoxContainer = styled(View, {
  marginTop: sp(20),
  flexDirection: 'row',
  alignItems: 'center'
})

export const SaveInfoContainer = styled(View, {
  ...container('padding'),
  marginTop: sp(8),
  paddingVertical: sp(16),
  backgroundColor: color('surface')
})

export const RadioButtonContainer = styled(View, {
  flexDirection: 'row'
})

export const ButtonContainer = styled(View, {
  ...container(),
  marginTop: 'auto',
  marginBottom: sp(24)
})

export const SaveInfoTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginBottom: sp(4)
})
