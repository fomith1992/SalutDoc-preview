import React from 'react'
import { FlatList } from 'react-native'
import {
  ItemBlock,
  Day,
  WeekDay,
  MonthDay,
  WirkingHours,
  NotWorkingHoursText,
  DividerBlock,
  Date,
  LongLine,
  SmallLine,
  SubItemBlock,
  SubItemTextTime,
  SubItemText,
  InvisibleBlock,
  SubItemInfo,
  WorkingHoursText,
  Container,
  TitleBlock,
  Title,
  CloseSvg,
  CloseButon,
  Description
} from './doctor-shedule.style'
import moment from 'moment'
import { hitSlopParams } from '../../modules/ui-kit'
import { useBooleanFlag } from '../../hooks/use-boolean-flag'

interface DoctorSheduleViewProps {
  data: Array<{
    date: Date
    workFrom?: string
    workTo?: string
    handleShedulePressed: () => void
    isToday: boolean
    consultations: Array<{
      time: string
      userName: string
      handleItemPressed: () => void
    }>
  }>
}

export function DoctorSheduleView ({ data }: DoctorSheduleViewProps): React.ReactElement {
  const { state: cloudVisible, setFalse: setCloudVisibleFalse } = useBooleanFlag(true)
  return (
    <FlatList
      data={data}
      keyExtractor={item => item.date.toString()}
      ListHeaderComponent={() => {
        return data.every(x => x.workFrom == null && x.workTo == null) && cloudVisible
          ? (
            <Container>
              <TitleBlock>
                <Title>
                  Заполните расписание
                </Title>
                <CloseButon
                  hitSlop={hitSlopParams(20)}
                  onPress={setCloudVisibleFalse}
                >
                  <CloseSvg />
                </CloseButon>
              </TitleBlock>
              <Description>
                Расписание нужно для того, что пациенты смогли записываться именно к вам
              </Description>
            </Container>
          ) : <></>
      }}
      renderItem={({ item }) => {
        return (
          <>
            {moment(item.date).format('D') === '1'
              ? (
                <DividerBlock>
                  <SmallLine accent />
                  <Date accent>
                    {moment(item.date).format('MMMM')}
                  </Date>
                  <LongLine accent />
                </DividerBlock>
              )
              : null}
            {+moment(item.date).format('e') === 0
              ? (
                <DividerBlock>
                  <SmallLine accent={false} />
                  <Date accent={false}>
                    {moment(item.date).format('M') === moment(item.date).add(6, 'd').format('M')
                      ? `${moment(item.date).format('DD')}-${moment(item.date).add(6, 'd').format('DD MMMM')}`
                      : `${moment(item.date).format('DD MMMM')}-${moment(item.date).add(6, 'd').format('DD MMMM')}`}
                  </Date>
                  <LongLine accent={false} />
                </DividerBlock>
              )
              : null}
            <ItemBlock>
              <Day>
                <WeekDay isToday={item.isToday}>
                  {moment(item.date).format('dd')}
                </WeekDay>
                <MonthDay isToday={item.isToday}>
                  {moment(item.date).format('DD')}
                </MonthDay>
              </Day>
              <WirkingHours
                onPress={item.handleShedulePressed}
              >
                {item.workFrom != null && item.workTo != null
                  ? (
                    <WorkingHoursText>
                      {`с ${item.workFrom} до ${item.workTo}`}
                    </WorkingHoursText>
                  ) : (
                    <NotWorkingHoursText>
                      Приема нет
                    </NotWorkingHoursText>
                  )}
              </WirkingHours>
            </ItemBlock>
            {item.consultations.length > 0
              ? item.consultations.map((consultation, idx) => (
                <SubItemBlock key={`${consultation.userName} ${idx}`}>
                  <Day>
                    <InvisibleBlock>
                      {moment(item.date).format('dd')}
                    </InvisibleBlock>
                  </Day>
                  <SubItemInfo
                    onPress={consultation.handleItemPressed}
                  >
                    <SubItemTextTime>
                      {consultation.time}
                    </SubItemTextTime>
                    <SubItemText>
                      {consultation.userName}
                    </SubItemText>
                  </SubItemInfo>
                </SubItemBlock>
              ))
              : null}
          </>
        )
      }}
    />
  )
}
