import { Text, View, TouchableOpacity } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { CrossIcon16 } from '../../images/cross.icon-16'

export const ItemBlock = styled(View, {
  ...container(),
  marginVertical: sp(12),
  flexDirection: 'row',
  alignItems: 'center'
})

export const Day = styled(View, {
  paddingHorizontal: sp(16)
})

export const WeekDay = styled(Text, (props: {isToday: boolean}) => ({
  ...font({ type: 'h2' }),
  textTransform: 'uppercase',
  color: color(props.isToday ? 'accent' : 'text'),
  textAlign: 'center'
}) as const)

export const MonthDay = styled(Text, (props: {isToday: boolean}) => ({
  ...font({ type: 'h2' }),
  color: color(props.isToday ? 'accent' : 'text'),
  textAlign: 'center'
}) as const)

export const WirkingHours = styled(TouchableOpacity, {
  ...container('padding'),
  flex: 1,
  height: sp(48),
  borderRadius: sp(12),
  borderWidth: 1,
  borderColor: color('inactiveLight'),
  justifyContent: 'center'
})

export const NotWorkingHoursText = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('inactive')
})

export const WorkingHoursText = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const DividerBlock = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center'
})

export const Date = styled(Text, (props: {accent: boolean}) => ({
  ...container(),
  ...font({ type: 'text2' }),
  color: color(props.accent ? 'accent' : 'inactive'),
  textTransform: props.accent ? 'capitalize' : 'lowercase'
}) as const)

export const LongLine = styled(View, (props: {accent: boolean}) => ({
  flex: 1,
  height: 1,
  backgroundColor: color(props.accent ? 'accent' : 'inactive')
}) as const)

export const SmallLine = styled(View, (props: {accent: boolean}) => ({
  width: '20%',
  height: 1,
  backgroundColor: color(props.accent ? 'accent' : 'inactive')
}) as const)

export const SubItemBlock = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center'
})

export const SubItemInfo = styled(TouchableOpacity, {
  ...container('padding'),
  height: sp(48),
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  marginVertical: sp(4),
  borderRadius: sp(8),
  backgroundColor: color('secondaryLight')
})

export const SubItemTextTime = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginTop: 2
})

export const SubItemText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginLeft: sp(8)
})

export const InvisibleBlock = styled(Text, {
  ...font({ type: 'h2' }),
  textTransform: 'uppercase',
  opacity: 0
})

export const Container = styled(View, {
  ...container(),
  padding: sp(20),
  marginVertical: sp(24),
  borderRadius: sp(16),
  backgroundColor: color('accent')
})

export const TitleBlock = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between'
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('surface')
})

export const Description = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('surface'),
  marginTop: sp(12)
})

export const CloseSvg = styled(CrossIcon16, {
  color: color('inactive')
})

export const CloseButon = TouchableOpacity
