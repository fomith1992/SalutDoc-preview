import { View, Platform, Text } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const Container = styled(View, {
  justifyContent: 'center'
})

export const ButtonsContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-end'
})

export const ClosingContainer = styled(View, {
  flex: 1,
  justifyContent: 'center'
})

export const PopUpContainer = styled(View, {
  ...container(),
  paddingHorizontal: sp(20),
  paddingTop: sp(20),
  paddingBottom: sp(12),
  backgroundColor: color('surface'),
  borderRadius: sp(8),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const Description = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(12)
})

export const ButtonBlock = styled(View, {
  paddingHorizontal: sp(20)
})
