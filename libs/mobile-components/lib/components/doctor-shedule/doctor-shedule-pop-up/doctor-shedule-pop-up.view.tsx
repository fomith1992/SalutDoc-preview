import React from 'react'
import { DismissibleModal, TextButton } from '../../../modules/ui-kit'
import {
  Title,
  Description,
  PopUpContainer,
  ButtonsContainer,
  ClosingContainer,
  ButtonBlock
} from './doctor-shedule-pop-up.style'

export interface ModalLoadingIndicatorProps {
  visible: boolean
  title: string
  description: string
  submitButton: {
    title: string
    onSubmit: () => void
  }
  cancelButton: {
    title: string
    onCancel: () => void
  }
}

export function DoctorShedulePopUp ({
  visible,
  title,
  description,
  cancelButton,
  submitButton
}: ModalLoadingIndicatorProps): React.ReactElement {
  return (
    <DismissibleModal
      background='gray'
      visible={visible}
      onDismiss={() => cancelButton.onCancel()}
    >
      <ClosingContainer>
        <PopUpContainer>
          <Title>
            {title}
          </Title>
          <Description>
            {description}
          </Description>
          <ButtonsContainer>
            <ButtonBlock>
              <TextButton
                type='link'
                size='L'
                onPress={() => cancelButton.onCancel()}
              >
                {cancelButton.title}
              </TextButton>
            </ButtonBlock>
            <ButtonBlock>
              <TextButton
                type='link'
                size='L'
                onPress={() => submitButton.onSubmit()}
              >
                {submitButton.title}
              </TextButton>
            </ButtonBlock>
          </ButtonsContainer>
        </PopUpContainer>
      </ClosingContainer>
    </DismissibleModal>
  )
}
