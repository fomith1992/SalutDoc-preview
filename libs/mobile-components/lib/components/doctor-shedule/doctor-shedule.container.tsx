import React from 'react'
import { DoctorSheduleView } from './doctor-shedule.view'
import moment from 'moment'
import { StackActions, useNavigation } from '@react-navigation/native'

export function DoctorShedule (): React.ReactElement {
  const navigation = useNavigation()

  const data = [
    ...Array.from(Array(180).keys(), x => (
      {
        date: moment().add(x, 'd').toDate(),
        isToday: x === 0,
        handleShedulePressed: () => navigation.dispatch(StackActions.push('ConsultationEditShedule', {})),
        consultations: []
      }
    ))]

  const dataWithInfo = data.map(item => {
    if (moment(item.date).format('DD.MM.YYYY') === moment().format('DD.MM.YYYY')) {
      return {
        ...item,
        workFrom: '19.00',
        workTo: '21.00',
        consultations: [
          {
            time: '19.00',
            userName: 'Сергей',
            handleItemPressed: () => navigation.dispatch(StackActions.push('ConsultationPreviewScheduled', {}))
          },
          {
            time: '19.30',
            userName: 'Ольга',
            handleItemPressed: () => navigation.dispatch(StackActions.push('ConsultationPreviewScheduled', {}))
          },
          {
            time: '20.00',
            userName: 'Валерий',
            handleItemPressed: () => navigation.dispatch(StackActions.push('ConsultationPreviewScheduled', {}))
          }
        ]
      }
    } else if (moment(item.date).format('DD.MM.YYYY') === moment().add(2, 'd').format('DD.MM.YYYY')) {
      return {
        ...item,
        workFrom: '19.00',
        workTo: '21.00',
        consultations: [
          {
            time: '19.00',
            userName: 'Сергей',
            handleItemPressed: () => navigation.dispatch(StackActions.push('ConsultationPreviewScheduled', {}))
          }
        ]
      }
    } else return item
  })

  return (
    <DoctorSheduleView
      data={dataWithInfo}
    />
  )
}
