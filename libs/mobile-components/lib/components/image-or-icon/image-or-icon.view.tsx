import React from 'react'
import { StyleProp, ViewStyle } from 'react-native'
import { IconComponent } from '../../images/icon-component'
import { ColorType } from '../../style/color'
import { useMaterialized } from '../../style/styled'
import { EmptyAvatarContainer, Img, placeholderStyleSheetFn } from './image-or-icon.style'

export type ShapeIconType = 'circle' | 'square' | 'fill' | 'circle-border'

export interface ImageOrIconProps {
  url?: string | null
  shape?: ShapeIconType
  placeholder?: IconComponent
  withLogo?: boolean
  placeholderColor?: ColorType
  style?: StyleProp<ViewStyle>

  children?: React.ReactNode
}

export function ImageOrIconView (
  {
    url,
    shape = 'square',
    placeholder: Placeholder,
    withLogo = true,
    placeholderColor = 'accent',
    style,
    children
  }: ImageOrIconProps
): React.ReactElement {
  const placeholderStyleSheet = placeholderStyleSheetFn({
    color: placeholderColor
  })
  const placeholderStyle = useMaterialized(placeholderStyleSheet)
  return (
    <Img
      shape={shape}
      source={{
        uri: url ?? undefined
      }}
      style={style}
    >
      {url == null && withLogo && Placeholder != null ? (
        <EmptyAvatarContainer>
          <Placeholder style={placeholderStyle} />
        </EmptyAvatarContainer>
      ) : null}
      {children}
    </Img>
  )
}
