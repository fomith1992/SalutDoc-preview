import { ImageBackground, View } from 'react-native'
import { color, ColorType } from '../../style/color'
import { LazyStyleProp, lift, styled, variants } from '../../style/styled'

export const Img = styled(ImageBackground, variants('shape', {
  circle: {
    borderRadius: 99,
    aspectRatio: 1
  },
  'circle-border': {
    borderWidth: 1,
    borderColor: color('inactiveLight'),
    borderRadius: 99,
    aspectRatio: 1
  },
  square: {
    borderRadius: 4,
    aspectRatio: 1
  },
  fill: {
    flex: 1
  }
}), {
  overflow: 'hidden'
})

export const EmptyAvatarContainer = styled(View, {
  position: 'absolute',
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  alignItems: 'center',
  justifyContent: 'center'
})

export const placeholderStyleSheetFn = (props: { color: ColorType }): LazyStyleProp<{
  width: string
  height: string
  color: string
}> => lift({
  width: '50%',
  height: '50%',
  color: color(props.color)
})
