import { ReactNativeFile } from 'apollo-upload-client'
import React from 'react'
import { Alert } from 'react-native'
import { useUpdateCommunityAvatarMutation } from '../../gen/graphql'
import { AvatarUploadView } from './avatar-upload.view'

interface AvatarUploadProps {
  avatarUrl?: string | null
  communityId: string
}

export const AvatarCommunityUpload = (
  {
    avatarUrl,
    communityId
  }: AvatarUploadProps
): React.ReactElement => {
  const [updateCommunityAvatar] = useUpdateCommunityAvatarMutation({
    async onCompleted (data) {
      const { errors } = data.communityUpdateAvatar
      if (errors.length !== 0) {
        Alert.alert('Не удалось загрузить аватар')
      }
    }
  })

  const handleAvatarSave = (avatar: ReactNativeFile): void => {
    updateCommunityAvatar({
      variables: {
        communityId: communityId,
        avatar: avatar
      }
    }).catch(error => {
      console.log(error)
      Alert.alert('Не удалось загрузить аватар')
    })
  }

  return (
    <AvatarUploadView
      avatarUrl={avatarUrl}
      onAvatarSave={handleAvatarSave}
    />
  )
}
