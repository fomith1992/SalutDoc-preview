import { ReactNativeFile } from 'apollo-upload-client'
import React from 'react'
import { Alert } from 'react-native'
import { useUpdateUserAvatarMutation, useUserAvatarQuery } from '../../gen/graphql'
import { AvatarUploadView } from './avatar-upload.view'

interface AvatarUploadProps {
  userId: string
}

export const AvatarUpload = (
  {
    userId
  }: AvatarUploadProps
): React.ReactElement => {
  const { data } = useUserAvatarQuery({
    variables: { userId }
  })

  const [updateUserAvatar] = useUpdateUserAvatarMutation({
    async onCompleted (data) {
      const { errors, message, user } = data.userUpdateAvatar
      if (errors.length !== 0) {
        Alert.alert('Error', 'Failed to load avatar')
      } else {
        console.log(message, user)
      }
    }
  })

  const handleAvatarSave = (avatar: ReactNativeFile): void => {
    updateUserAvatar({
      variables: {
        userId: userId,
        avatar: avatar
      }
    }).catch(error => {
      console.log(error)
      Alert.alert('Error', 'Failed to load avatar')
    })
  }

  return (
    <AvatarUploadView
      avatarUrl={data?.userById?.avatar}
      onAvatarSave={handleAvatarSave}
    />
  )
}
