import { TouchableOpacity, View } from 'react-native'
import { WriteIcon20 } from '../../images/write.icon-20'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { Avatar } from '../avatar/avatar.view'

export const AvatarContainer = styled(View, {
  width: sp(80)
})

export const AvatarUploadContainer = styled(TouchableOpacity, {
  position: 'absolute',
  bottom: 0,
  right: 0
})

export const UpdateAvatarSvg = styled(WriteIcon20, {
  color: color('accent')
})

export const AvatarImg = styled(Avatar, {
  backgroundColor: 'transparent'
})
