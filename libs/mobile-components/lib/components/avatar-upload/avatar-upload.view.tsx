import { ReactNativeFile } from 'apollo-upload-client'
import React from 'react'
import { hitSlopParams, ImagePicker } from '../../modules/ui-kit'
import {
  AvatarUploadContainer,
  UpdateAvatarSvg,
  AvatarContainer,
  AvatarImg
} from './avatar-upload.style'

interface AvatarUploadViewProps {
  avatarUrl?: string | null
  onAvatarSave: (avatar: ReactNativeFile) => void
}

export function AvatarUploadView (
  { avatarUrl, onAvatarSave }: AvatarUploadViewProps
): React.ReactElement {
  return (
    <ImagePicker
      title='Select Avatar'
      onImagePicked={(image) => onAvatarSave(new ReactNativeFile({
        uri: image.uri,
        name: image.fileName ?? 'avatar.jpeg',
        type: image.mimeType
      }))}
    >
      {({ image, pickImage }) => (
        <AvatarContainer>
          <AvatarImg
            placeholderColor='inactive'
            shape='circle-border'
            url={image?.uri ?? avatarUrl}
          />
          <AvatarUploadContainer
            hitSlop={hitSlopParams(16)}
            onPress={pickImage}
          >
            <UpdateAvatarSvg />
          </AvatarUploadContainer>
        </AvatarContainer>
      )}
    </ImagePicker>
  )
}
