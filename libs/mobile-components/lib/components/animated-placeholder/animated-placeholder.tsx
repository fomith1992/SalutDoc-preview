import React, { useRef, useEffect, useState } from 'react'
import { Animated, View } from 'react-native'

export interface AnimatedPlaceholderProps {
  placeholder: React.ReactNode
  children?: React.ReactNode
}

export function AnimatedPlaceholder ({ placeholder, children }: AnimatedPlaceholderProps): React.ReactElement {
  const placeholderOpacity = useRef(new Animated.Value(1)).current
  const hasChildren = React.Children.count(children) !== 0
  const [animationEnded, setAnimationEnded] = useState(hasChildren)
  useEffect(() => {
    if (!animationEnded && hasChildren) {
      Animated.timing(placeholderOpacity, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false
      }).start(() => setAnimationEnded(true))
    }
  }, [animationEnded, hasChildren])

  if (animationEnded) {
    return <View>{children}</View>
  } else if (hasChildren) {
    return (
      <View>
        <Animated.View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            opacity: placeholderOpacity
          }}
        >
          {placeholder}
        </Animated.View>
        <Animated.View
          style={{
            opacity: Animated.subtract(new Animated.Value(1), placeholderOpacity)
          }}
        >
          {children}
        </Animated.View>
      </View>
    )
  } else {
    return <View>{placeholder}</View>
  }
}
