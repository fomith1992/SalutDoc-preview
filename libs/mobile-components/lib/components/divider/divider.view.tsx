import React from 'react'
import { Divider } from './divider.style'

interface DividerProps {
  marginTop?: number
  isFull?: boolean
}

export const DividerView = ({ marginTop = 0, isFull = false }: DividerProps): React.ReactElement => {
  return (
    <Divider marginTop={marginTop} isFull={isFull} />
  )
}
