import { styled } from '../../style/styled'
import { View } from 'react-native'
import { container } from '../../style/view'
import { color } from '../../style/color'

export const Divider = styled(View, (props: { marginTop: number, isFull: boolean }) => ({
  ...(props.isFull ? 0 : container()),
  height: 0.5,
  backgroundColor: color('inactiveLight'),
  marginTop: props.marginTop
}) as const)
