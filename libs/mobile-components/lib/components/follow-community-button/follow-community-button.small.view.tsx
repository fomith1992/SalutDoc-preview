import React, { useState } from 'react'
import { hitSlopParams } from '../../modules/ui-kit'
import { CoverLetterPopup } from './cover-letter-popup'
import { FollowContainer, FollowPlusIcon, UnfollowPlusIcon } from './follow-community-button.style'
import { CommunityMembership } from './follow-community-button.view'

export const FollowCommunityButtonSmall = (props: {
  isPrivate: boolean
  membership: CommunityMembership
}): React.ReactElement => {
  const { isPrivate, membership } = props
  const [popupVisible, setPopupVisible] = useState(false)

  switch (membership.status) {
    case 'INACTIVE': {
      if (isPrivate) {
        return membership.canSendMembershipApplication
          ? (
            <>
              <FollowContainer
                hitSlop={hitSlopParams(16)}
                onPress={() => setPopupVisible(true)}
              >
                <FollowPlusIcon />
              </FollowContainer>
              <CoverLetterPopup
                visible={popupVisible}
                onClose={() => setPopupVisible(false)}
                onSubmit={data => {
                  setPopupVisible(false)
                  return membership.handleSendRequest(data)
                }}
              />
            </>
          ) : (<></>)
      } else {
        return (
          <FollowContainer
            hitSlop={hitSlopParams(16)}
            onPress={membership.handleFollow}
          >
            <FollowPlusIcon />
          </FollowContainer>
        )
      }
    }
    case 'INVITE':
      return (
        <FollowContainer
          hitSlop={hitSlopParams(16)}
          onPress={membership.handleAcceptInvite}
        >
          <FollowPlusIcon />
        </FollowContainer>
      )
    case 'REQUEST':
      return (
        <FollowContainer
          hitSlop={hitSlopParams(16)}
          onPress={membership.handleCancelRequest}
        >
          <UnfollowPlusIcon />
        </FollowContainer>
      )
    case 'ACTIVE':
      return (
        <FollowContainer
          hitSlop={hitSlopParams(16)}
          onPress={membership.handleUnfollow}
        >
          <UnfollowPlusIcon />
        </FollowContainer>
      )
  }
}
