import { SubmissionHandler } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { TextButton } from '../../modules/ui-kit'
import { CoverLetterPopup, InviteCommunityForm } from './cover-letter-popup'
import { ButtonsRow, ButtonContainer, Spring } from './follow-community-button.style'

export type CommunityMembership = NoMembership | InvitedMembership | RequestedMembership | FollowerMembership

interface NoMembership {
  loading: boolean
  status: 'INACTIVE'
  canSendMembershipApplication: boolean
  handleFollow: () => void
  handleSendRequest: SubmissionHandler<InviteCommunityForm>
}

interface InvitedMembership {
  loading: boolean
  status: 'INVITE'
  handleAcceptInvite: () => void
  handleRejectInvite: () => void
}

interface RequestedMembership {
  loading: boolean
  status: 'REQUEST'
  handleCancelRequest: () => void
}

interface FollowerMembership {
  loading: boolean
  status: 'ACTIVE'
  handleUnfollow: () => void
}

export interface FollowCommunityButtonViewProps {
  isPrivate: boolean
  membership: CommunityMembership
}

export const FollowCommunityButtonView = ({
  isPrivate,
  membership
}: FollowCommunityButtonViewProps): React.ReactElement => {
  const [popupVisible, setPopupVisible] = useState<boolean>(false)
  switch (membership.status) {
    case 'INACTIVE': {
      if (isPrivate) {
        return membership.canSendMembershipApplication
          ? (
            <>
              <TextButton
                size='L'
                type='primary'
                onPress={() => setPopupVisible(true)}
                disabled={membership.loading}
              >
                Отправить заявку
              </TextButton>
              <CoverLetterPopup
                visible={popupVisible}
                onClose={() => setPopupVisible(false)}
                onSubmit={data => {
                  setPopupVisible(false)
                  return membership.handleSendRequest(data)
                }}
              />
            </>
          ) : (<></>)
      } else {
        return (
          <TextButton
            size='L'
            type='primary'
            onPress={membership.handleFollow}
            disabled={membership.loading}
          >
            Подписаться
          </TextButton>
        )
      }
    }
    case 'INVITE':
      return (
        <ButtonsRow>
          <ButtonContainer>
            <TextButton
              size='L'
              type='primary'
              onPress={membership.handleAcceptInvite}
              disabled={membership.loading}
            >
              Вступить
            </TextButton>
          </ButtonContainer>
          <Spring />
          <ButtonContainer>
            <TextButton
              size='L'
              type='primary'
              onPress={membership.handleRejectInvite}
              disabled={membership.loading}
            >
              Отклонить
            </TextButton>
          </ButtonContainer>
        </ButtonsRow>
      )
    case 'REQUEST':
      return (
        <TextButton
          size='L'
          type='secondary'
          onPress={membership.handleCancelRequest}
          disabled={membership.loading}
        >
          Заявка отправлена
        </TextButton>
      )
    case 'ACTIVE':
      return (
        <TextButton
          size='L'
          type='primary'
          onPress={membership.handleUnfollow}
          disabled={membership.loading}
        >
          Отписаться
        </TextButton>
      )
  }
}
