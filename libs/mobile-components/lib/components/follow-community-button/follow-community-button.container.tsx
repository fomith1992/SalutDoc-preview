import React from 'react'
import { Alert } from 'react-native'
import {
  useCancelRequestMutation,
  useCommunityFollowerInfoQuery,
  useConfirmInviteMutation,
  useFollowCommunityMutation,
  useRejectInviteMutation,
  useSendRequestMutation,
  useUnfollowCommunityMutation
} from '../../gen/graphql'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { AnimatedPlaceholder } from '../animated-placeholder/animated-placeholder'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'
import { CommunityMembership, FollowCommunityButtonView } from './follow-community-button.view'
import { StackActions } from '@react-navigation/native'

interface CommunityProfileProps {
  communityId: string
  view?: (props: {isPrivate: boolean, membership: CommunityMembership}) => React.ReactElement
}

export const FollowCommunityButton = ({
  communityId,
  view: FollowButtonView = FollowCommunityButtonView
}: CommunityProfileProps): React.ReactElement => {
  const user = useAuthenticatedUserOrNull()
  const navigation = useStackNavigation()
  const { data, loading } = useCommunityFollowerInfoQuery({
    variables: {
      communityId
    }
  })

  const [followCommunity, followData] = useFollowCommunityMutation()
  const [unfollowCommunity, unfollowData] = useUnfollowCommunityMutation()
  const [sendRequest, sendRequestData] = useSendRequestMutation()
  const [cancelRequest, cancelRequestData] = useCancelRequestMutation()
  const [confirmInvite, confirmInviteData] = useConfirmInviteMutation()
  const [rejectInvite, rejectInviteData] = useRejectInviteMutation()

  const status = data?.communityById?.membershipByUserId?.status
  const canSendMembershipApplication = data?.communityById?.canSendMembershipApplication ?? false
  if (user == null) {
    const viewMembership: CommunityMembership = {
      loading: false,
      status: 'INACTIVE',
      canSendMembershipApplication,
      handleFollow: () => {
        navigation.dispatch(StackActions.push('CreateAccountCard', {}))
        return {}
      },
      handleSendRequest: () => {
        navigation.dispatch(StackActions.push('CreateAccountCard', {}))
        return {}
      }
    }
    return (
      <AnimatedPlaceholder
        placeholder={<LoadingIndicator visible />}
      >
        {loading ? null
          : (
            <FollowButtonView
              membership={viewMembership}
              isPrivate={data?.communityById?.type === 'PRIVATE'}
            />)}
      </AnimatedPlaceholder>
    )
  }
  const viewMembership: CommunityMembership = status == null || status === 'INACTIVE'
    ? {
      loading: sendRequestData.loading || followData.loading,
      status: 'INACTIVE',
      canSendMembershipApplication,
      handleFollow: () => {
        followCommunity({
          variables: {
            userId: user.id,
            communityId
          }
        }).catch(() => {
          Alert.alert('Ошибка подписки на сообщество')
        })
      },
      handleSendRequest: (message) => {
        sendRequest({
          variables: {
            userId: user.id,
            communityId,
            message: message.coverLetter
          }
        }).catch(() => {
          Alert.alert('Попробуйте позднее')
        })
        return {}
      }
    }
    : {
      loading: unfollowData.loading || cancelRequestData.loading || confirmInviteData.loading || rejectInviteData.loading,
      status,
      handleCancelRequest: () => {
        cancelRequest({
          variables: {
            userId: user.id,
            communityId
          }
        }).catch(() => {
          Alert.alert('Попробуйте позднее')
        })
      },
      handleAcceptInvite: () => {
        confirmInvite({
          variables: {
            userId: user.id,
            communityId
          }
        }).catch(() => {
          Alert.alert('Попробуйте позднее')
        })
      },
      handleRejectInvite: () => {
        rejectInvite({
          variables: {
            userId: user.id,
            communityId
          }
        }).catch(() => {
          Alert.alert('Попробуйте позднее')
        })
      },
      handleUnfollow: () => {
        unfollowCommunity({
          variables: {
            userId: user.id,
            communityId
          }
        }).catch(() => {
          Alert.alert('Ошибка отписки от сообщества')
        })
      }
    }
  return (
    <AnimatedPlaceholder
      placeholder={<LoadingIndicator visible />}
    >
      {loading ? null
        : (
          <FollowButtonView
            membership={viewMembership}
            isPrivate={data?.communityById?.type === 'PRIVATE'}
          />)}
    </AnimatedPlaceholder>
  )
}
