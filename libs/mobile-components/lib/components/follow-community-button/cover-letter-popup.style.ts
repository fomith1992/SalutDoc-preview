import { KeyboardAvoidingView, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { container } from '../../style/view'

export const Overlay = styled(KeyboardAvoidingView, {
  backgroundColor: 'rgba(148, 148, 148, 0.4)',
  flex: 1,
  justifyContent: 'center'
})

export const Popup = styled(View, {
  ...container(),
  backgroundColor: color('surface'),
  paddingTop: sp(20),
  paddingLeft: sp(16),
  paddingRight: sp(16),
  paddingBottom: sp(16),
  borderRadius: 4
})

export const ActionsRow = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between'
})
