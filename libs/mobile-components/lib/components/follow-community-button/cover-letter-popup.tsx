import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { Platform } from 'react-native'
import { yup } from '../../modules/form-kit'
import { TextInput } from '../../modules/form-kit/text-input'
import { Modal, SubmitTextButton, TextButton } from '../../modules/ui-kit'
import { ActionsRow, Overlay, Popup } from './cover-letter-popup.style'

const coverLetterMaxLength = 300

const formSchema = yup.object({
  coverLetter: yup.string()
    .max(coverLetterMaxLength)
    .ensure().defined()
}).required()
export type InviteCommunityForm = yup.InferType<typeof formSchema>

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export interface CoverLetterPopupProps {
  visible: boolean
  onClose: () => void

  onSubmit: SubmissionHandler<InviteCommunityForm>
}

export function CoverLetterPopup (
  {
    visible,
    onSubmit,
    onClose
  }: CoverLetterPopupProps
): React.ReactElement {
  return (
    <Modal visible={visible} animation='fade'>
      <Overlay behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <Popup>
          <Form
            onSubmit={onSubmit}
          >
            <Field name='coverLetter'>
              {props => (
                <TextInput
                  {...props}
                  label='Сопроводительное письмо'
                  numberOfLines={8}
                  maxLength={coverLetterMaxLength}
                />
              )}
            </Field>
            <ActionsRow>
              <TextButton
                type='link'
                onPress={onClose}
              >
                Отменить
              </TextButton>
              <Submit>
                {props => (
                  <SubmitTextButton
                    type='link'
                    {...props}
                  >
                    Отправить
                  </SubmitTextButton>
                )}
              </Submit>
            </ActionsRow>
          </Form>
        </Popup>
      </Overlay>
    </Modal>
  )
}
