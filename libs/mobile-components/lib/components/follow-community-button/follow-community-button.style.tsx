import { View, TouchableOpacity } from 'react-native'
import { CrossIcon16 } from '../../images/cross.icon-16'
import { PlusIcon16 } from '../../images/plus.icon-16'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

export const FollowContainer = styled(TouchableOpacity, {
  marginLeft: 'auto'
})

export const FollowPlusIcon = styled(PlusIcon16, {
  color: color('accent'),
  width: sp(20),
  height: sp(20)
})

export const UnfollowPlusIcon = styled(CrossIcon16, {
  color: color('subtext'),
  width: sp(20),
  height: sp(20)
})

export const ButtonsRow = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between'
})

export const Spring = styled(View, {
  paddingHorizontal: sp(8)
})

export const ButtonContainer = styled(View, {
  flex: 1
})
