import { Text, View } from 'react-native'
import { BannerFreeConcultationIcon } from '../../images/banner-free-consultation.icon'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const BannerContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface')
})

export const Centered = styled(View, {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center'
})

export const Title = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('text'),
  marginTop: sp(32),
  textAlign: 'center'
})

export const Info = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(8),
  textAlign: 'center',
  paddingHorizontal: sp(48)
})

export const ButtonsContainer = styled(View, {
  ...container(),
  marginTop: 'auto',
  marginBottom: sp(8)
})

export const Spring = styled(View, {
  marginTop: sp(8)
})

export const BannerIcon = styled(BannerFreeConcultationIcon, {
  color: color('accent')
})
