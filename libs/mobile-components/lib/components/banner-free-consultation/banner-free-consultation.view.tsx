import React from 'react'
import { consultationDuration } from '../../modules/config'
import { TextButton } from '../../modules/ui-kit'
import {
  BannerContainer,
  Centered,
  Title,
  Info,
  ButtonsContainer,
  BannerIcon,
  Spring
} from './banner-free-consultation.style'

export function BannerFreeConsultationView (): React.ReactElement {
  return (
    <BannerContainer>
      <Centered>
        <BannerIcon />
        <Title>
          Консультация в подарок!
        </Title>
        <Info>
          {consultationDuration} минут помощи от опытного врача.
          Скидка 50% по промокоду SalutDoc.
        </Info>
      </Centered>
      <ButtonsContainer>
        <TextButton
          type='primary'
          size='L'
        >
          Начать консультацию
        </TextButton>
        <Spring />
        <TextButton
          type='link'
          size='L'
        >
          Не сейчас
        </TextButton>
      </ButtonsContainer>
    </BannerContainer>
  )
}
