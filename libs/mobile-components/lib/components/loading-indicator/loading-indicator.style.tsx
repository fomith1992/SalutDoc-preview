import { View, ActivityIndicator, Platform } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

export const Container = styled(View, {
  justifyContent: 'center'
})

export const Indicator = ActivityIndicator

export const OverlayContainer = styled(View, {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: 'rgba(148, 148, 148, 0.4)'
})

export const SpinnerContainer = styled(View, {
  padding: sp(16),
  backgroundColor: color('surface'),
  borderRadius: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})
