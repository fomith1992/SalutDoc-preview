import React, { useEffect, useMemo, useRef } from 'react'
import { Animated, Easing } from 'react-native'
import { LoadingIndicatorIcon24 } from '../../images/loading-indicator.icon-24'
import { color } from '../../style/color'
import { useMaterialized } from '../../style/styled'
import {
  Container,
  Indicator
} from './loading-indicator.style'

interface LoadingIndicatorProps {
  visible: boolean
  size?: 'enormous' | 'large' | 'normal' | 'small'
  colorType?: 'default' | 'inactive'
}

export const LoadingIndicator = ({
  visible,
  size = 'large',
  colorType = 'default'
}: LoadingIndicatorProps): React.ReactElement => {
  const rotateAnim = useRef(new Animated.Value(0)).current
  const spinner = useMemo(() => rotateAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  }), [rotateAnim])

  useEffect(() => {
    Animated.loop(
      Animated.timing(rotateAnim, {
        useNativeDriver: true,
        easing: Easing.linear,
        toValue: 1,
        duration: 1200
      })).start()
  }, [])

  const indicatorColor = {
    default: useMaterialized(color('accent')),
    inactive: useMaterialized(color('inactive'))
  }

  if (!visible) return <></>

  switch (size) {
    case 'enormous':
      return (
        <Animated.View style={{ width: 56, height: 56, transform: [{ rotate: spinner }] }}>
          <LoadingIndicatorIcon24 style={{ width: 56, height: 56, color: indicatorColor[colorType] }} />
        </Animated.View>
      )
    case 'large':
      return (
        <Container>
          <Indicator
            color={indicatorColor[colorType]}
            size={size}
          />
        </Container>
      )
    case 'normal':
      return (
        <Animated.View style={{ width: 20, height: 20, transform: [{ rotate: spinner }] }}>
          <LoadingIndicatorIcon24 style={{ width: 20, height: 20, color: indicatorColor[colorType] }} />
        </Animated.View>
      )
    case 'small':
      return (
        <Animated.View style={{ width: 10, height: 10, transform: [{ rotate: spinner }] }}>
          <LoadingIndicatorIcon24 style={{ width: 10, height: 10, color: indicatorColor[colorType] }} />
        </Animated.View>
      )
  }
}
