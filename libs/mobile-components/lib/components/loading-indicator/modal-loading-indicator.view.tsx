import React, { useEffect, useMemo, useRef } from 'react'
import { Animated, Easing } from 'react-native'
import { LoadingIndicatorBubblesIcon } from '../../images/loading-indicator-bubbles.icon'
import { Modal } from '../../modules/ui-kit'
import { OverlayContainer, SpinnerContainer } from './loading-indicator.style'

export interface ModalLoadingIndicatorProps {
  visible: boolean
}

export function ModalLoadingIndicator ({ visible }: ModalLoadingIndicatorProps): React.ReactElement {
  const rotateAnim = useRef(new Animated.Value(0)).current

  useEffect(() => {
    Animated.loop(
      Animated.timing(rotateAnim, {
        useNativeDriver: true,
        easing: Easing.linear,
        toValue: 1,
        duration: 1200
      })).start()
  }, [])

  const spinner = useMemo(() => rotateAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  }), [rotateAnim])

  return (
    <Modal
      visible={visible}
      animation='fade'
    >
      <OverlayContainer>
        <SpinnerContainer>
          <Animated.View style={{ width: 60, height: 60, transform: [{ rotate: spinner }] }}>
            <LoadingIndicatorBubblesIcon />
          </Animated.View>
        </SpinnerContainer>
      </OverlayContainer>
    </Modal>
  )
}
