import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { Text } from 'react-native'
import { useCommunityMembersListQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { CommunityMembersList } from './community-members.view'

interface CommunityMembersProps {
  communityId: string
}

export const CommunityMembers = ({ communityId }: CommunityMembersProps): React.ReactElement => {
  const navigation = useNavigation()
  const { loading, data, fetchMore, refetch } = useCommunityMembersListQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      communityId,
      after: null
    }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: {
        communityId,
        after: expectDefined(data?.communityById?.members?.pageInfo.endCursor)
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousCommunityMembers = expectDefined(previousResult.communityById?.members)
        return {
          ...expectDefined(previousResult),
          communityById: {
            ...expectDefined(previousResult.communityById),
            members: appendEdges(previousCommunityMembers, fetchMoreResult?.communityById?.members)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.communityById?.members?.pageInfo.endCursor])

  if (data?.communityById?.members == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <CommunityMembersList
      data={
        data?.communityById?.members?.edges.map(
          ({ node: { user: { id, avatar, firstName, lastName } } }) => ({
            id,
            avatar,
            firstName,
            lastName,
            handleProceedToProfile: () => navigation.dispatch(StackActions.push('Profile', { userId: id }))
          })) ?? []
      }
      hasMore={data?.communityById?.members?.pageInfo.hasNextPage ?? false}
      refreshing={loading && !refreshing}
      loading={loading}
      refresh={refresh}
      loadMore={() => loadMore()}
      isCommunityAdmin={data?.userMyself?.isCommunityAdmin ?? false}
      handleProceedToRequests={() => navigation?.dispatch(StackActions.push('CommunityRequests', { communityId }))}
    />
  )
}
