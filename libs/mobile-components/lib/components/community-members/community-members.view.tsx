import {
  EmptyFollowersContainer,
  EmptyFollowersTitle,
  RequestsAndInvitesContainer,
  RequestsAndInvitesIconSvg,
  RequestsAndInvitesText
} from './community-members.style'
import { FlatList } from 'react-native-gesture-handler'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import React from 'react'
import { UserRow } from '../user-row/user-row.view'

interface RequestsAndInvitesProps {
  onPress: () => void
}

interface User {
  id: string
  avatar: string | null
  firstName: string
  lastName: string
  handleProceedToProfile: () => void
}

export interface CommunityMembersViewProps {
  data: User[]
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
  handleProceedToRequests: () => void
  isCommunityAdmin: boolean
}

const RequestsAndInvites = ({ onPress }: RequestsAndInvitesProps): React.ReactElement => (
  <RequestsAndInvitesContainer onPress={onPress}>
    <RequestsAndInvitesIconSvg />
    <RequestsAndInvitesText>Заявки и приглашения</RequestsAndInvitesText>
  </RequestsAndInvitesContainer>
)

export const CommunityMembersList = (
  {
    data,
    refreshing,
    loading,
    hasMore,
    refresh,
    loadMore,
    handleProceedToRequests,
    isCommunityAdmin
  }: CommunityMembersViewProps): React.ReactElement => {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => (
        <UserRow
          id={item.id}
          avatar={item.avatar}
          userName={`${item.firstName} ${item.lastName}`}
          onProceedToProfile={item.handleProceedToProfile}
        />
      )}
      keyExtractor={item => item.id}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListHeaderComponent={isCommunityAdmin
        ? <RequestsAndInvites onPress={handleProceedToRequests} />
        : null}
      ListFooterComponent={<LoadingIndicator visible={loading} />}
      ListEmptyComponent={() => (
        <EmptyFollowersContainer>
          <EmptyFollowersTitle>
            Нет подписчиков
          </EmptyFollowersTitle>
        </EmptyFollowersContainer>
      )}
      contentContainerStyle={{ flexGrow: 1, marginTop: 8 }}
    />
  )
}
