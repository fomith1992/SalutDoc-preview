import { View, TouchableOpacity, Text } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { sp } from '../../style/size'
import { font } from '../../style/text'
import { RequestsListIcon } from '../../images/requests-list.icon'

export const RequestsAndInvitesContainer = styled(TouchableOpacity, {
  marginBottom: sp(8),
  paddingHorizontal: sp(16),
  paddingVertical: sp(12),
  backgroundColor: color('surface'),
  flexDirection: 'row'
})

export const RequestsAndInvitesIconSvg = styled(RequestsListIcon, {
  width: sp(16),
  height: sp(16),
  color: color('accent')
})

export const RequestsAndInvitesText = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('text'),
  paddingLeft: sp(12)
})

export const EmptyFollowersContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: sp(48)
})

export const EmptyFollowersTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  textAlign: 'center'
})
