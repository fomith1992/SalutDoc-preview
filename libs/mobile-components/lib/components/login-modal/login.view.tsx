import React from 'react'
import { TextButton } from '../../modules/ui-kit'
import {
  LoginBtn,
  LoginContainer,
  Title
} from './login.style'

export interface LoginViewProps {
  onLogin: () => void
  onRegister: () => void
}

export function LoginView ({ onLogin, onRegister }: LoginViewProps): React.ReactElement {
  return (
    <LoginContainer>
      <Title>
        Модальный логин
      </Title>
      <LoginBtn>
        <TextButton
          type='primary'
          size='XL'
          onPress={onRegister}
        >
          Зарегистрироваться
        </TextButton>
      </LoginBtn>
      <LoginBtn>
        <TextButton
          type='link'
          size='XL'
          onPress={onLogin}
        >
          Войти
        </TextButton>
      </LoginBtn>
    </LoginContainer>
  )
}
