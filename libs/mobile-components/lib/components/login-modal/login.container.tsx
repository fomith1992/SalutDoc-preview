import React from 'react'
import { useAuthentication } from '../../contexts/authentication-context'
import { LoginView } from './login.view'

export function Login (): React.ReactElement {
  const { login } = useAuthentication()

  return (
    <LoginView
      onLogin={() => login({ page: 'login' })}
      onRegister={() => login({ page: 'registration' })}
    />
  )
}
