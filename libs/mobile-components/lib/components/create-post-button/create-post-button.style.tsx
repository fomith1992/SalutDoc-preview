import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const CreatePostButtonContainer = styled(View, {
  backgroundColor: color('surface'),
  paddingVertical: sp(8),
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarContainer = styled(View, {
  height: 40,
  width: 40,
  marginLeft: sp(16)
})

export const WritePostTitleContainer = styled(View, {
  ...container(),
  backgroundColor: color('background'),
  borderRadius: 99,
  padding: sp(12),
  flex: 1
})

export const WritePostTitle = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})
