import { StackActions, useNavigation } from '@react-navigation/native'
import React, { useCallback } from 'react'
import { useCreatePostButtonCommunityAuthQuery } from '../../gen/graphql'
import { useAuthenticatedUserOrNull } from '../user-context/user-context-provider'
import { CreatePostButtonView } from './create-post-button.view'

interface CreateCommunityPostButtonProps {
  communityId: string
}

export function CreateCommunityPostButton ({ communityId }: CreateCommunityPostButtonProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useNavigation()
  const { data } = useCreatePostButtonCommunityAuthQuery({
    variables: { communityId }
  })

  const handleCreatePost = useCallback(() => {
    navigation.dispatch(StackActions.push('PostCreator', { siteId: communityId, siteType: 'COMMUNITY' }))
  }, [navigation, communityId])
  const handleCreateArticle = useCallback(() => {
    navigation.dispatch(StackActions.push('ArticleConstructor'))
  }, [navigation])

  if (data?.communityById == null) {
    return <></>
  } else {
    const { canCreatePost, canCreateArticle } = data.communityById
    if (canCreatePost || canCreateArticle) {
      return (
        <CreatePostButtonView
          avatar={user?.avatar}
          createPostVisible={canCreatePost}
          createArticleVisible={canCreateArticle}
          createQuestionVisible={false}
          onCreatePost={handleCreatePost}
          onCreateArticle={handleCreateArticle}
        />
      )
    } else {
      return <></>
    }
  }
}
