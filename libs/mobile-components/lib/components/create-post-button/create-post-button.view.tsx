import React from 'react'
import { TouchableOpacity } from 'react-native'
import { useBooleanFlag } from '../../hooks/use-boolean-flag'
import { ContextMenu, ContextMenuButton, ScreenBlock } from '../../modules/ui-kit'
import { Avatar } from '../avatar/avatar.view'
import {
  AvatarContainer,
  CreatePostButtonContainer,
  WritePostTitleContainer,
  WritePostTitle
} from './create-post-button.style'

export interface CreatePostButtonViewProps {
  avatar?: string | null

  createPostVisible: boolean
  createArticleVisible: boolean
  createQuestionVisible: boolean

  onCreatePost?: () => void
  onCreateArticle?: () => void
  onCreateQuestion?: () => void
}

export function CreatePostButtonView (
  {
    avatar,
    createPostVisible,
    createArticleVisible,
    createQuestionVisible,
    onCreatePost,
    onCreateArticle,
    onCreateQuestion
  }: CreatePostButtonViewProps
): React.ReactElement {
  const {
    state: writeBottomMenuVisible,
    setTrue: showWriteBottomMenu,
    setFalse: hideWriteBottomMenu
  } = useBooleanFlag(false)
  const writeMenuButtons: ContextMenuButton[] = []
  if (createPostVisible) {
    writeMenuButtons.push({
      type: 'black',
      title: 'Новая запись',
      onPress: () => {
        hideWriteBottomMenu()
        onCreatePost?.()
      }
    })
  }
  if (createArticleVisible) {
    writeMenuButtons.push({
      type: 'black',
      title: 'Написать статью',
      onPress: () => {
        hideWriteBottomMenu()
        onCreateArticle?.()
      }
    })
  }
  if (createQuestionVisible) {
    writeMenuButtons.push({
      type: 'black',
      title: 'Задать вопрос',
      onPress: () => {
        hideWriteBottomMenu()
        onCreateQuestion?.()
      }
    })
  }
  writeMenuButtons.push({
    type: 'red',
    title: 'Отмена',
    onPress: hideWriteBottomMenu
  })
  return (
    <ScreenBlock>
      <TouchableOpacity onPress={showWriteBottomMenu}>
        <CreatePostButtonContainer>
          <AvatarContainer>
            <Avatar url={avatar} shape='circle' />
          </AvatarContainer>
          <WritePostTitleContainer>
            <WritePostTitle>
              Напишите что-нибудь...
            </WritePostTitle>
          </WritePostTitleContainer>
          <ContextMenu
            visible={writeBottomMenuVisible}
            onClose={hideWriteBottomMenu}
            buttons={writeMenuButtons}
          />
        </CreatePostButtonContainer>
      </TouchableOpacity>
    </ScreenBlock>
  )
}
