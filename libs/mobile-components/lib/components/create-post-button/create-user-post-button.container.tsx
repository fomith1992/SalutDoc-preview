import { StackActions, useNavigation } from '@react-navigation/native'
import React, { useCallback } from 'react'
import { useCreatePostButtonUserAuthQuery } from '../../gen/graphql'
import { useAuthenticatedUser } from '../user-context/user-context-provider'
import { CreatePostButtonView } from './create-post-button.view'

interface CreateUserPostButtonProps {
  userId: string
}

export function CreateUserPostButton ({ userId }: CreateUserPostButtonProps): React.ReactElement {
  const user = useAuthenticatedUser()
  const navigation = useNavigation()
  const { data } = useCreatePostButtonUserAuthQuery({
    variables: { userId }
  })

  const handleCreatePost = useCallback(() => {
    navigation.dispatch(StackActions.push('PostCreator', { siteId: userId, siteType: 'USER' }))
  }, [navigation, userId])
  const handleCreateArticle = useCallback(() => {
    navigation.dispatch(StackActions.push('ArticleConstructor'))
  }, [navigation])
  const handleCreateQuestion = useCallback(() => {
    navigation.dispatch(StackActions.push('QuestionCreator'))
  }, [navigation])

  if (data?.userById == null) {
    return <></>
  } else {
    const { canCreatePost, canCreateArticle, canCreateQuestion } = data.userById
    if (canCreatePost || canCreateArticle || canCreateQuestion) {
      return (
        <CreatePostButtonView
          avatar={user.avatar}
          createPostVisible={canCreatePost}
          createArticleVisible={canCreateArticle}
          createQuestionVisible={canCreateQuestion}
          onCreatePost={handleCreatePost}
          onCreateArticle={handleCreateArticle}
          onCreateQuestion={handleCreateQuestion}
        />
      )
    } else {
      return <></>
    }
  }
}
