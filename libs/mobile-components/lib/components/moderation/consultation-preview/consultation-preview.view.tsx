import React from 'react'
import { ProfileIcon24 } from '../../../images/profile.icon-24'
import {
  InformationContainer,
  AnamnesisDescription,
  InformationTitle,
  Container,
  HeaderButtonContainer,
  HeaderButtonText,
  HeaderContainer,
  OpenChatSvg,
  ReturnMoneySvg,
  ReportTitle,
  ReportDescription,
  Divider,
  ReportItem,
  SectionContainer,
  ListItemContainer,
  AvatarOrIcon,
  TitleContainer,
  Itemtitle,
  ItemDescription
} from './consultation-preview.style'
import { ElasticText } from '../../elastic-text/elastic-text'
import { PaymentStatus } from '../../../gen/graphql'

interface User {
  id: string
  avatar: string | null
  firstName: string
  lastName: string
  description: string
  handleItemPressed: () => void
}
export interface Participants {
  patient: User
  doctor?: User
}

interface ConsultationPreviewViewProps {
  date: string
  participants: Participants
  anamnesis: string
  report: Array<{
    title: string
    description: string | null
  }>
  onOpenChat: () => void
  onRefundPayment: () => void
  paymentStatus: PaymentStatus
  loading: boolean
}

interface RenderItemProps {
  user: User
  isLast: boolean
}

const RenderUser = (
  {
    isLast,
    user
  }: RenderItemProps
): React.ReactElement => {
  const {
    id,
    avatar,
    firstName,
    lastName,
    description,
    handleItemPressed
  } = user
  return (
    <ListItemContainer
      key={id}
      isLastElement={isLast}
      onPress={handleItemPressed}
    >
      <AvatarOrIcon
        url={avatar}
        shape='circle'
        placeholder={ProfileIcon24}
        placeholderColor='inactive'
      />
      <TitleContainer>
        <ElasticText minimumFontScale={1}>
          <Itemtitle>
            {`${firstName} ${lastName}`}
          </Itemtitle>
        </ElasticText>
        <ItemDescription>
          {description}
        </ItemDescription>
      </TitleContainer>
    </ListItemContainer>
  )
}

function RefundPaymentButton (
  props: {
    loading: boolean
    paymentStatus: PaymentStatus
    onRefundPayment: () => void
  }
): React.ReactElement {
  const { loading, paymentStatus, onRefundPayment } = props

  function resolveStatusText (loading: boolean, paymentStatus: PaymentStatus): string {
    if (loading) return 'Происходит возврат денег'
    switch (paymentStatus) {
      case 'CANCELLED':
        return 'Платеж отменен'
      case 'PENDING':
        return 'Платеж ожидается'
      case 'REFUNDED':
        return 'Деньги возвращены'
      case 'ACCEPTED':
        return 'Вернуть деньги'
    }
  }

  return (
    <HeaderButtonContainer disabled={loading || paymentStatus !== 'ACCEPTED'} onPress={onRefundPayment}>
      <ReturnMoneySvg
        type={
          loading
            ? 'INACTIVE' : paymentStatus === 'REFUNDED'
              ? 'REFUNDED' : 'PRIMARY'
        }
      />
      <HeaderButtonText lodaing={loading}>
        {resolveStatusText(loading, paymentStatus)}
      </HeaderButtonText>
    </HeaderButtonContainer>
  )
}

export function ConsultationPreviewView ({
  date,
  participants,
  report,
  anamnesis,
  onOpenChat,
  onRefundPayment,
  paymentStatus,
  loading
}: ConsultationPreviewViewProps): React.ReactElement {
  return (
    <Container>
      <HeaderContainer>
        <HeaderButtonContainer onPress={onOpenChat}>
          <OpenChatSvg />
          <HeaderButtonText lodaing={false}>
            Открыть чат
          </HeaderButtonText>
        </HeaderButtonContainer>
        <RefundPaymentButton
          loading={loading}
          paymentStatus={paymentStatus}
          onRefundPayment={onRefundPayment}
        />
      </HeaderContainer>
      <SectionContainer>
        <InformationTitle>{date}</InformationTitle>
        {participants.doctor != null && (
          <RenderUser
            isLast={false}
            user={participants.doctor}
          />
        )}
        <RenderUser
          isLast
          user={participants.patient}
        />
      </SectionContainer>
      <InformationContainer>
        <InformationTitle>
          Жалобы
        </InformationTitle>
        <AnamnesisDescription>
          {anamnesis}
        </AnamnesisDescription>
      </InformationContainer>
      <InformationContainer>
        <InformationTitle>
          Медзаключение
        </InformationTitle>
        {report.length > 0
          ? report.map((item, index) => item.description != null
            ? (
              <ReportItem key={index}>
                <ReportTitle>
                  {item.title}
                </ReportTitle>
                <ReportDescription>
                  {item.description}
                </ReportDescription>
                {report.length !== index + 1 && (
                  <Divider />
                )}
              </ReportItem>
            ) : <></>
          ) : (
            <ReportTitle>
              Еще не создано
            </ReportTitle>
          )}
      </InformationContainer>
    </Container>
  )
}
