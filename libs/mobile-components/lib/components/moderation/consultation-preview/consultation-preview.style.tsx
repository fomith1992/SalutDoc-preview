import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { ChatWithPlusIcon24 } from '../../../images/chat-with-plus.icon-24'
import { ArrowBackIcon24 } from '../../../images/arrow-back.icon-24'
import { ImageOrIconView } from '../../image-or-icon/image-or-icon.view'

export const Container = styled(ScrollView, {
  flex: 1,
  backgroundColor: color('background')
})

export const ReportItem = View

export const HeaderContainer = styled(View, {
  marginTop: sp(8),
  backgroundColor: color('surface')
})

export const HeaderButtonContainer = styled(TouchableOpacity, {
  ...container('padding'),
  flexDirection: 'row',
  alignItems: 'center',
  paddingVertical: sp(12)
})

export const HeaderButtonText = styled(Text, (props: { lodaing: boolean }) => ({
  ...font({ type: 'text1' }),
  color: color(props.lodaing ? 'inactive' : 'text'),
  marginLeft: sp(16)
}) as const)

export const OpenChatSvg = styled(ChatWithPlusIcon24, {
  color: color('accent')
})

export const ReturnMoneySvg = styled(ArrowBackIcon24, (props: { type: 'PRIMARY' | 'INACTIVE' | 'REFUNDED' }) => ({
  color: color(props.type === 'PRIMARY' ? 'accent' : props.type === 'INACTIVE' ? 'inactive' : 'text'),
  width: sp(24),
  height: sp(24)
}))

export const InformationContainer = styled(View, {
  ...container('padding'),
  marginTop: sp(8),
  paddingTop: sp(24),
  paddingBottom: sp(16),
  backgroundColor: color('surface')
})

export const AnamnesisDescription = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginVertical: sp(8)
})

export const ReportTitle = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('subtext'),
  marginVertical: sp(8)
})

export const ReportDescription = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginBottom: sp(8)
})

export const Divider = styled(View, {
  height: 1,
  backgroundColor: color('inactiveLight'),
  marginVertical: sp(8)
})

export const SectionContainer = styled(View, {
  ...container('padding'),
  marginTop: sp(8),
  backgroundColor: color('surface'),
  paddingTop: sp(24)
})

export const ListItemContainer = styled(TouchableOpacity, (props: {isLastElement: boolean}) => ({
  flexDirection: 'row',
  alignItems: 'center',
  paddingTop: sp(8),
  paddingBottom: sp(props.isLastElement ? 24 : 8),
  backgroundColor: color('surface')
}) as const)

export const AvatarOrIcon = styled(ImageOrIconView, {
  borderWidth: 1,
  width: sp(56),
  borderColor: color('inactiveLight'),
  borderStyle: 'solid'
})

export const Itemtitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' })
})

export const ItemDescription = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  marginTop: sp(4)
})

export const TitleContainer = styled(View, {
  marginLeft: sp(16),
  overflow: 'hidden',
  flexDirection: 'column',
  flex: 1
})

export const InformationTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  paddingBottom: sp(8)
})
