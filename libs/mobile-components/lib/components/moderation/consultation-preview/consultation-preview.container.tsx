import React, { useEffect, useState } from 'react'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { ConsultationPreviewView, Participants } from './consultation-preview.view'
import {
  ConsultationPreviewDocument,
  ConsultationPreviewQuery,
  ConsultationPreviewQueryVariables,
  useConsultationPreviewQuery,
  usePaymentRefundMutation
} from '../../../gen/graphql'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { Alert, Text } from 'react-native'
import { expectDefined } from '@salutdoc/react-components'
import moment from 'moment'
import { specializationName } from '../../../modules/consultations/i18n/specialization'
import { NotificationActionsModalDialog } from '../../notifications/notification-actions/notification-actions-modal-dialog'

const formatDate = (date: Date | string): string => moment(date).format('DD MMMM HH:MM')

const formatAge = (date: Date | string | null): string | null =>
  date == null ? null : `${moment.duration(Date.now() - new Date(date).getTime()).years()}`

const formatGender = (gender: string | null): string | null => {
  switch (gender) {
    case 'MALE':
      return 'мужчина'
    case 'FEMALE':
      return 'женщина'
    case 'OTHER':
    default:
      return null
  }
}

const formatPatientDescription = (age: string | null, gender: string | null): string => {
  return age == null
    ? gender == null
      ? 'Пациент'
      : `${gender.slice(0, 1).toUpperCase()}${gender.slice(1)}`
    : gender == null
      ? `${age} лет`
      : `${age} лет, ${gender}`
}

export interface ConsultationPreviewProps {
  consultationId: string
}

export function ConsultationPreview ({ consultationId }: ConsultationPreviewProps): React.ReactElement {
  const navigator = useStackNavigation()
  const { data, loading, error } = useConsultationPreviewQuery({
    variables: {
      consultationId
    }
  })
  const [dialogVisible, setDialogVisible] = useState<boolean>(false)

  const [refundPayment, { loading: paymentRefunding }] = usePaymentRefundMutation({
    onCompleted (data) {
      const { errors } = data.paymentRefund
      if (errors.length !== 0) {
        Alert.alert('Не удалось вернуть платеж')
      }
    },
    update: (cache, { data: paymentData }) => {
      if (data == null || paymentData?.paymentRefund.payment?.status == null) return
      const consultation = cache.readQuery<ConsultationPreviewQuery, ConsultationPreviewQueryVariables>({
        query: ConsultationPreviewDocument,
        variables: { consultationId }
      })
      if (consultation?.consultationById?.payment != null) {
        cache.writeQuery<ConsultationPreviewQuery, ConsultationPreviewQueryVariables>({
          query: ConsultationPreviewDocument,
          variables: { consultationId },
          data: {
            ...consultation,
            consultationById: {
              ...consultation.consultationById,
              payment: {
                ...consultation.consultationById.payment,
                status: paymentData?.paymentRefund.payment?.status
              }
            }
          }
        })
      }
    },
    onError (err) {
      console.log(err)
      Alert.alert('Не удалось вернуть платеж')
    }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  const consultation = data?.consultationById
  if (consultation == null || consultation.payment == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  const patient: Participants['patient'] = {
    id: consultation.patient.id,
    avatar: consultation.patient.avatar,
    firstName: consultation.patient.firstName,
    lastName: consultation.patient.lastName,
    description: formatPatientDescription(formatAge(consultation.patient.birthday), formatGender(consultation.patient.gender)),
    handleItemPressed: () => navigator.push('Profile', { userId: consultation.patient.id })
  }

  return (
    <>
      <ConsultationPreviewView
        report={consultation.report == null
          ? []
          : [{
            title: 'Наиболее вероятное заболевание',
            description: consultation.report.diagnosis
          }, {
            title: 'Рекомендуемая диагностика',
            description: consultation.report.diagnostics
          }, {
            title: 'Рекомендуемые врачи',
            description: consultation.report.doctors
          }, {
            title: 'Рекомендации по лекарствам',
            description: consultation.report.medicines
          }, {
            title: 'Общие рекомендации',
            description: consultation.report.recommendation
          }]}
        anamnesis={expectDefined(consultation.anamnesis?.complaints)}
        date={formatDate(consultation.createdAt)}
        participants={consultation.doctor == null ? {
          patient
        } : {
          patient,
          doctor: {
            id: consultation.doctor.id,
            avatar: consultation.doctor.avatar,
            firstName: consultation.doctor.firstName,
            lastName: consultation.doctor.lastName,
            description: specializationName[consultation.specialization] ?? consultation.specialization,
            handleItemPressed: () => navigator.push('Profile', { userId: expectDefined(consultation.doctor?.id) })
          }
        }}
        onOpenChat={consultation.chat == null
          ? () => navigator.push('FreeConsultationChat', { consultationId })
          : () => navigator.push('DoctorConsultationChat', { consultationId })}
        paymentStatus={consultation.payment.status}
        onRefundPayment={() => setDialogVisible(true)}
        loading={paymentRefunding}
      />
      <NotificationActionsModalDialog
        visible={dialogVisible}
        setVisible={setDialogVisible}
        title='Вернуть деньги?'
        text='Отменить данное действие невозможно. Пожалуйста, подтвердите возврат.'
        applyButtonText='Вернуть'
        cancelButtonText='Отмена'
        onApplyButtonPress={() => {
          if (consultation.payment?.id != null) {
            refundPayment({
              variables: {
                paymentId: consultation.payment.id
              }
            })
          } else {
            Alert.alert('Консультация не оплачена')
          }
        }}
      />
    </>
  )
}
