import { ItemProps, ModerationView, SectionProps } from './moderation.view'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import React from 'react'
import { Text } from 'react-native'
import moment from 'moment'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useModerationQuery } from '../../gen/graphql'
import { useRefresh } from '../../hooks/use-refresh'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { specializationName } from '../../modules/consultations/i18n/specialization'

const formatDate = (date: Date | string): string => moment(date).format('D MMM')
const formatTime = (date: Date | string): string => moment(date).format('HH:MM')

const formatCreatedAtDate = (date: Date): string => {
  const now = new Date()
  const daysDifference = (now.getTime() - date.getTime()) / (1000 * 3600 * 24)
  switch (Math.floor(daysDifference)) {
    case 0:
      return 'Сегодня'
    case 1:
      return 'Вчера'
    default:
      return formatDate(date)
  }
}

export function Moderation (): React.ReactElement {
  const navigator = useStackNavigation()
  const { data, loading, refetch, fetchMore } = useModerationQuery({
    notifyOnNetworkStatusChange: true
  })
  const { refresh, refreshing } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: {
        after: expectDefined(data?.userMyself?.consultationsAsModerator?.pageInfo.endCursor)
      },
      updateQuery: (previousResult, { fetchMoreResult }) => ({
        ...expectDefined(previousResult),
        userMyself: {
          ...expectDefined(previousResult.userMyself),
          consultationsAsModerator: appendEdges(
            expectDefined(previousResult.userMyself?.consultationsAsModerator),
            fetchMoreResult?.userMyself?.consultationsAsModerator
          )
        }
      })
    }).catch(error => {
      console.log(error)
    })
  }, [data?.userMyself?.consultationsAsModerator?.pageInfo.endCursor])
  if (data?.userMyself?.consultationsAsModerator == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  const emptyList: SectionProps[] = []
  return (
    <ModerationView
      data={data.userMyself.consultationsAsModerator.edges.reduce((accumulator, { node }) => {
        const { id, doctor, specialization, createdAt, finished } = node
        if (doctor == null) {
          return accumulator
        }
        const { id: doctorId, avatar, firstName, lastName } = doctor
        const row: ItemProps = {
          id,
          userId: doctorId,
          avatar,
          firstName,
          lastName,
          specialization: specializationName[specialization] ?? specialization,
          createdAt: formatTime(createdAt),
          finished,
          handleItemPressed: () => navigator.push('ConsultationPreview', { consultationId: id })
        }
        const createdAtDate = formatCreatedAtDate(new Date(createdAt))
        const targetSection = accumulator.filter(section => section.title === createdAtDate)?.[0]
        if (targetSection != null) {
          targetSection.data.push(row)
        } else {
          accumulator.push({
            title: createdAtDate,
            data: [row]
          })
        }
        return accumulator
      }, emptyList)}
      hasMore={data.userMyself.consultationsAsModerator.pageInfo.hasNextPage ?? false}
      refreshing={loading && !refreshing}
      loading={loading}
      refresh={refresh}
      loadMore={() => loadMore()}
    />
  )
}
