import {
  AvatarOrIcon,
  ItemDescription,
  Itemtitle,
  ListItemContainer,
  ListTitleItem,
  ModerationContainer,
  TitleContainer,
  TimestampContainer,
  Timestamp
} from './moderation.style'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { ProfileIcon24 } from '../../images/profile.icon-24'
import React from 'react'
import { SectionList } from 'react-native'
import { buildImageUrl } from '../../utils'
import { ElasticText } from '../elastic-text/elastic-text'

export interface ItemProps {
  id: string
  userId: string
  avatar: string | null
  firstName: string
  lastName: string
  specialization: string
  createdAt: string
  finished: boolean
  handleItemPressed: () => void
}

export interface SectionProps {
  title: string
  data: ItemProps[]
}

export interface ModerationViewProps {
  data: SectionProps[]
  hasMore: boolean
  refreshing: boolean
  loading: boolean
  refresh: () => void
  loadMore: () => void
}

export function ModerationView (
  {
    data,
    hasMore,
    refreshing,
    loading,
    refresh,
    loadMore
  }: ModerationViewProps
): React.ReactElement {
  return (
    <ModerationContainer>
      <SectionList
        sections={data}
        renderSectionHeader={({ section: { title } }) => (
          title != null ? <ListTitleItem>{title}</ListTitleItem> : <></>
        )}
        renderItem={({ item, index, section }) => (
          <ListItemContainer
            isLastElement={section.data.length === index + 1}
            onPress={item.handleItemPressed}
          >
            <AvatarOrIcon
              url={buildImageUrl(item.avatar, item.finished ? 'bw' : undefined)}
              shape='circle'
              placeholder={ProfileIcon24}
              placeholderColor='inactive'
            />
            <TitleContainer>
              <ElasticText minimumFontScale={1}>
                <Itemtitle
                  isConsultationEnded={item.finished}
                >
                  {`${item.firstName} ${item.lastName}`}
                </Itemtitle>
              </ElasticText>
              <ItemDescription>
                {item.specialization}
              </ItemDescription>
            </TitleContainer>
            <TimestampContainer>
              <Timestamp>
                {item.createdAt}
              </Timestamp>
            </TimestampContainer>
          </ListItemContainer>
        )}
        keyExtractor={item => item.id}
        onEndReached={hasMore ? loadMore : null}
        refreshing={refreshing}
        onRefresh={refresh}
        ListFooterComponent={<LoadingIndicator visible={loading} />}
      />
    </ModerationContainer>
  )
}
