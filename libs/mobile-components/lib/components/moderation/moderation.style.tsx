import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { ImageOrIconView } from '../image-or-icon/image-or-icon.view'

export const ModerationContainer = styled(View, {
  flex: 1,
  backgroundColor: color('background')
})

export const SectionContainer = styled(View, {
  ...container('padding'),
  marginTop: sp(8),
  backgroundColor: color('surface'),
  paddingVertical: sp(12)
})

export const ListItemContainer = styled(TouchableOpacity, (props: {isLastElement: boolean}) => ({
  ...container('padding'),
  flexDirection: 'row',
  alignItems: 'center',
  paddingTop: sp(12),
  paddingBottom: sp(props.isLastElement ? 24 : 12),
  backgroundColor: color('surface')
}) as const)

export const ListTitleItem = styled(Text, {
  ...container('padding'),
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginTop: sp(8),
  backgroundColor: color('surface'),
  paddingTop: sp(24),
  paddingBottom: sp(12)
})

export const AvatarOrIcon = styled(ImageOrIconView, {
  borderWidth: 1,
  width: sp(56),
  borderColor: color('inactiveLight'),
  borderStyle: 'solid'
})

export const TitleContainer = styled(View, {
  marginLeft: sp(16),
  overflow: 'hidden',
  flexDirection: 'column',
  flex: 1
})

export const Itemtitle = styled(Text, (props: {isConsultationEnded: boolean}) => ({
  ...font({ type: 'h3', weight: 'strong' }),
  color: color(props.isConsultationEnded ? 'subtext' : 'text')
}) as const)

export const ItemDescription = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  marginTop: sp(4)
})

export const TimestampContainer = styled(View, {
  marginLeft: 'auto',
  flexDirection: 'column',
  alignItems: 'flex-start'
})

export const Timestamp = styled(Text, {
  ...font({ type: 'caption' }),
  flex: 1,
  color: color('inactive'),
  alignSelf: 'flex-start',
  paddingTop: sp(4)
})
