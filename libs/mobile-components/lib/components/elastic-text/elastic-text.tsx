import React from 'react'
import { Text, TextStyle } from 'react-native'

type ElasticTextProps = {
  children?: React.ReactNode
  numberOfLines?: number
  style?: TextStyle
  minimumFontScale?: number
}

export function ElasticText (
  {
    children,
    numberOfLines = 1,
    style,
    minimumFontScale = 0.7
  }: ElasticTextProps
): React.ReactElement {
  return (
    <Text
      adjustsFontSizeToFit={minimumFontScale < 1}
      minimumFontScale={minimumFontScale}
      numberOfLines={numberOfLines}
      ellipsizeMode='tail'
      style={style}
    >
      {children}
    </Text>
  )
}
