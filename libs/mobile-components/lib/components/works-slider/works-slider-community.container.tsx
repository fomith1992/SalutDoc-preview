import React, { useEffect } from 'react'
import { WorksSliderView } from './works-slider.view'
import { useNavigation, StackActions } from '@react-navigation/native'
import { useWorksSliderByCommunityIdQuery } from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { pipe } from 'fp-ts/lib/function'
import { findFirstMap } from 'fp-ts/Array'
import { some, none, getOrElseW } from 'fp-ts/lib/Option'
import { Text } from 'react-native'

export interface WorksSliderCommunityProps {
  communityId: string
}

export function WorksSliderCommunity ({ communityId }: WorksSliderCommunityProps): React.ReactElement {
  const navigation = useNavigation()
  const { data, loading, error } = useWorksSliderByCommunityIdQuery({
    variables: {
      communityId
    }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  if (data?.communityById == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Ошибка чтения работ</Text>
  }
  return (
    <WorksSliderView
      onSeeAllPressed={() => navigation.dispatch(StackActions.push('CommunityPortfolio', { communityId: communityId }))}
      onAddMaterialPressed={() => navigation.dispatch(StackActions.push('ArticleConstructor', {}))}
      data={data.communityById.works?.edges.map(({ node }) => ({
        id: node.id,
        image: pipe(
          node.blocks,
          findFirstMap(p => p.__typename === 'ArticleImageBlock' ? some(p.url) : none),
          getOrElseW(() => null)
        ),
        title: node.title,
        onPress: () => navigation.dispatch(StackActions.push('Work', { workId: node.id }))
      })) ?? []}
    />
  )
}
