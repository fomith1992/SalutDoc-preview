import React, { useEffect } from 'react'
import { WorksSliderView } from './works-slider.view'
import { useNavigation, StackActions } from '@react-navigation/native'
import { useWorksSliderByUserIdQuery } from '../../gen/graphql'
import { useAuthenticatedUserOrNull } from '../../components/user-context/user-context-provider'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { pipe } from 'fp-ts/lib/function'
import { findFirstMap } from 'fp-ts/Array'
import { some, none, getOrElseW } from 'fp-ts/lib/Option'
import { Text } from 'react-native'

export interface WorksSliderUserProps {
  userId: string
}

export function WorksSliderUser ({ userId }: WorksSliderUserProps): React.ReactElement {
  const navigation = useNavigation()
  const user = useAuthenticatedUserOrNull()
  const { data, loading, error } = useWorksSliderByUserIdQuery({
    variables: {
      userId
    }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  if (data?.userById == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Ошибка чтения работ</Text>
  }

  if (
    data?.userById.role !== 'DOCTOR' ||
    (data?.userById.id !== user?.id && data.userById.works?.edges.length === 0)
  ) {
    return <></>
  }

  return (
    <WorksSliderView
      onSeeAllPressed={() => navigation.dispatch(StackActions.push('UserPortfolio', { userId }))}
      onAddMaterialPressed={() => navigation.dispatch(StackActions.push('ArticleConstructor', {}))}
      data={data.userById.works?.edges.map(({ node }) => ({
        id: node.id,
        image: pipe(
          node.blocks,
          findFirstMap(p => p.__typename === 'ArticleImageBlock' ? some(p.url) : none),
          getOrElseW(() => null)
        ),
        title: node.title,
        onPress: () => navigation.dispatch(StackActions.push('Work', { workId: node.id }))
      })) ?? []}
    />
  )
}
