import { ImageBackground, Platform, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { font } from '../../../style/text'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { DocumentIcon20 } from '../../../images/document.icon-20'

export const Container = styled(View, {
  backgroundColor: color('surface'),
  paddingBottom: sp(16)
})

export const Slider = styled(ScrollView, {
  flexDirection: 'row'
})

export const SlideContainer = styled(View, (props: { isFirst: boolean, isLast: boolean }) => ({
  marginLeft: sp(props.isFirst ? 16 : 8),
  marginRight: sp(props.isLast ? 16 : 8),
  marginHorizontal: sp(8),
  marginBottom: sp(8),
  borderRadius: sp(8),
  overflow: 'hidden',
  backgroundColor: color('surface'),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
}) as const)

export const WorkPreviewSlideContent = styled(TouchableOpacity, {
  flexDirection: 'column',
  width: 196
})

export const SlideImageContainer = styled(ImageBackground, {
  flex: 1,
  position: 'relative',
  height: 100,
  justifyContent: 'center',
  alignItems: 'center',
  borderTopLeftRadius: sp(8),
  borderTopRightRadius: sp(8),
  overflow: 'hidden'
})

export const SlideEmptyImageContainer = styled(View, {
  flex: 1,
  position: 'relative',
  height: 100,
  justifyContent: 'center',
  alignItems: 'center',
  borderTopLeftRadius: sp(8),
  borderTopRightRadius: sp(8),
  backgroundColor: color('background'),
  overflow: 'hidden'
})

export const EmptyImageIcon = styled(DocumentIcon20, {
  width: sp(48),
  height: sp(48),
  color: color('inactive')
})

export const SlideTitle = styled(Text, {
  ...font({ type: 'caption' }),
  flex: 1,
  color: color('text'),
  padding: sp(12)
})
