import {
  Container,
  EmptyImageIcon,
  SlideContainer,
  SlideEmptyImageContainer,
  SlideImageContainer,
  SlideTitle,
  Slider,
  WorkPreviewSlideContent
} from './works-slider-cards.style'

import React from 'react'

export interface WorkPreviewSlideProps {
  id: string
  image: string | null
  title: string
  onPress: () => void
}

interface WorkPreviewSlideWithPositionProps {
  image: string | null
  title: string
  onPress: () => void
  isFirst?: boolean
  isLast?: boolean
}

export interface WorksSliderCardsViewProps {
  data: WorkPreviewSlideProps[]
}

function RenderItem (
  {
    image,
    title,
    onPress,
    isFirst = false,
    isLast = false
  }: WorkPreviewSlideWithPositionProps
): React.ReactElement {
  return (
    <SlideContainer isFirst={isFirst} isLast={isLast}>
      <WorkPreviewSlideContent onPress={onPress}>
        {image != null
          ? (
            <SlideImageContainer
              source={{ uri: image }}
            />
          ) : (
            <SlideEmptyImageContainer>
              <EmptyImageIcon />
            </SlideEmptyImageContainer>
          )}
        <SlideTitle numberOfLines={3}>
          {title}
        </SlideTitle>
      </WorkPreviewSlideContent>
    </SlideContainer>
  )
}

export function WorksSliderCardsView (
  {
    data
  }: WorksSliderCardsViewProps
): React.ReactElement {
  return (
    <Container>
      <Slider
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {data.map((item, index) => (
          <RenderItem
            {...item}
            key={item.id}
            isFirst={index === 0}
            isLast={index === data.length - 1}
          />
        )) ?? []}
      </Slider>
    </Container>
  )
}
