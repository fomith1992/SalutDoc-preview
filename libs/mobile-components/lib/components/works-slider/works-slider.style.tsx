import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'

export const Container = styled(View, {
  backgroundColor: color('surface')
})

export const WorksSliderHeader = styled(View, {
  paddingTop: sp(24),
  paddingHorizontal: sp(16),
  paddingBottom: sp(16),
  flexDirection: 'row',
  alignItems: 'center'
})

export const WorksSliderTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const SeeAllButtonContainer = styled(View, {
  marginLeft: 'auto'
})

export const EmptySliderContainer = styled(View, {
  paddingHorizontal: sp(32),
  paddingBottom: sp(24),
  justifyContent: 'center',
  alignItems: 'center'
})

export const EmptySliderText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center',
  paddingBottom: sp(16)
})
