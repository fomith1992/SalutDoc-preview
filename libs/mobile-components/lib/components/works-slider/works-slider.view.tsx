import {
  Container,
  SeeAllButtonContainer,
  EmptySliderContainer,
  WorksSliderHeader,
  EmptySliderText,
  WorksSliderTitle
} from './works-slider.style'
import { ScreenBlock, TextButton } from '../../modules/ui-kit'
import { WorkPreviewSlideProps, WorksSliderCardsView } from './works-slider-cards/works-slider-cards.view'
import React from 'react'

interface WorksSliderViewProps {
  data: WorkPreviewSlideProps[]
  onAddMaterialPressed: () => void
  onSeeAllPressed: () => void
}

export function WorksSliderView (
  {
    data,
    onAddMaterialPressed,
    onSeeAllPressed
  }: WorksSliderViewProps
): React.ReactElement {
  return (
    <ScreenBlock>
      <Container>
        <WorksSliderHeader>
          <WorksSliderTitle>
            Портфолио
          </WorksSliderTitle>
          <SeeAllButtonContainer>
            <TextButton
              type='link'
              onPress={onSeeAllPressed}
            >
              Смотреть все
            </TextButton>
          </SeeAllButtonContainer>
        </WorksSliderHeader>
        {data.length === 0
          ? (
            <EmptySliderContainer>
              <EmptySliderText
                numberOfLines={2}
              >
                Хотите показать свои достижения пациентам?{'\n'}Тогда добавьте свои работы в портфолио
              </EmptySliderText>
              <TextButton
                type='secondary'
                size='M'
                onPress={onAddMaterialPressed}
              >
                Добавить
              </TextButton>
            </EmptySliderContainer>
          ) : (
            <WorksSliderCardsView data={data} />
          )}
      </Container>
    </ScreenBlock>
  )
}
