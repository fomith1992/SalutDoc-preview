import React, { useEffect } from 'react'
import { Text } from 'react-native'
import { useUserInfoQuery } from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { EditProfileView, InfoCategoryProps } from './edit-profile-preview.view'
import _ from 'lodash'
import { StackActions, useNavigation } from '@react-navigation/native'
import { toCaseCount } from '../../i18n/utils'

interface EditProfileInfoProps {
  userId: string
}

export const EditProfileInfo = (
  { userId }: EditProfileInfoProps
): React.ReactElement => {
  const navigation = useNavigation()
  const { data, loading, error } = useUserInfoQuery({
    variables: { userId }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.userById == null) {
    return <Text>Not found</Text>
  }

  const categoriesdata: InfoCategoryProps[] = [
    {
      title: 'Основное образование',
      onPressEdit: () => navigation.dispatch(StackActions.push('EditUniversitiesInfo')),
      attributes: data.userById.universities.length === 0 ? []
        : data.userById.universities.map(university => ({
          specialization: university.specialization,
          data: _.compact([
            university.title === '' ? null : university.title,
            university.department === '' ? null : university.department,
            university.graduationYear == null ? null : university.graduationYear.toString()
          ])
        }))
    },
    {
      title: 'Интернатура/Ординатура',
      onPressEdit: () => navigation.dispatch(StackActions.push('EditIntershipsInfo')),
      attributes: data.userById.internships.map(internship => ({
        specialization: internship.specialization,
        data: _.compact([
          internship.title === '' ? null : internship.title,
          internship.graduationYear == null ? null : internship.graduationYear.toString()
        ])
      })
      )
    },
    {
      title: 'Повышение квалификации',
      onPressEdit: () => navigation.dispatch(StackActions.push('EditTrainingsInfo')),
      attributes: data.userById.trainings.map(training => ({
        specialization: training.specialization,
        data: _.compact([
          training.title === '' ? null : training.title,
          training.graduationYear == null ? null : training.graduationYear.toString(),
          training.hours === null ? null : `${training.hours} ${toCaseCount(['час', 'часа', 'часов'], training.hours)}`
        ])
      })
      )
    }]

  return (
    <EditProfileView
      userGeneralInfo={{
        specialization: data.userById.specialization,
        userId: userId,
        firstName: data.userById.firstName,
        lastName: data.userById.lastName,
        resolution: data.userById.lastVerificationApplication?.resolution ?? 'REJECTED',
        handleEditPressed: () => navigation.dispatch(StackActions.push('EditBasicInfoDoctor'))
      }}
      categories={categoriesdata}
    />
  )
}
