import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { PlusIcon24 } from '../../../images/plus.icon-24'
import { TrashIcon20 } from '../../../images/trash.icon-20'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const FormContainer = styled(View, {
  ...container('padding'),
  marginTop: sp(8),
  paddingTop: sp(12),
  backgroundColor: color('surface')
})

export const EditContainer = styled(ScrollView, {
  flex: 1,
  backgroundColor: color('background')
})

export const TitleText = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginVertical: sp(12)
})

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center'
})

export const InputContainer = styled(View, {
  marginBottom: sp(24)
})

export const DeleteDataButton = TouchableOpacity

export const DeleteDataText = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  color: color('accent')
})

export const AddEducationButton = styled(TouchableOpacity, {
  ...container('padding'),
  flexDirection: 'row',
  marginTop: sp(8),
  paddingVertical: sp(12),
  backgroundColor: color('surface')
})

export const PlusIcon = styled(PlusIcon24, {
  color: color('accent')
})

export const TrashIcon = styled(TrashIcon20, {
  color: color('error')
})

export const AddEducationText = styled(Text, (props: { disabledColor: boolean }) => ({
  ...font({ type: 'text1' }),
  color: color(props.disabledColor ? 'inactive' : 'text'),
  marginLeft: sp(16)
}) as const)

export const EmptyListContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('background')
})

export const EmptyListText = styled(Text, {
  ...font({ type: 'text2' }),
  textAlign: 'center',
  marginBottom: sp(16)
})
