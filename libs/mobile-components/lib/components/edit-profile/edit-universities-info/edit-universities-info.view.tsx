import { createFormDescriptor } from '@salutdoc/react-components'
import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import { SelectInput, TextInput, yup } from '../../../modules/form-kit'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'
import _ from 'lodash'
import { v4 as uuid } from 'uuid'
import {
  InputContainer,
  EditContainer,
  TitleText,
  TitleContainer,
  DeleteDataButton,
  AddEducationButton,
  AddEducationText,
  FormContainer,
  EmptyListContainer,
  EmptyListText,
  PlusIcon,
  TrashIcon
} from './edit-universities-info.style'
import { specializationsData } from '../../../modules/ui-kit/specializations-data/specializations-data'
import { SelectSearch } from '../../../modules/form-kit/select-search/select-search.view'
import { hitSlopParams, TextButton } from '../../../modules/ui-kit'

const universityTitleMaxLength = 100
const departmentMaxLength = 100
const specializationMaxLength = 100
const gradulationYearMinYear = 1900
const gradulationYearMaxYear = 2051
const universitiesMaxCount = 7

const formSchema = yup.object({
  title: yup.string()
    .max(universityTitleMaxLength)
    .ensure().required(),
  department: yup.string()
    .max(departmentMaxLength)
    .ensure().required(),
  specialization: yup.string()
    .max(specializationMaxLength)
    .ensure().required(),
  graduationYear: yup.number()
    .defined().notRequired()
    .min(gradulationYearMinYear)
    .max(gradulationYearMaxYear)
}).required()

export type EditProfileFormData = yup.InferType<typeof formSchema>

const { Form, FormSpy, Field } = createFormDescriptor(formSchema)

interface InternshipsInfoFormProps {
  initialValues: Partial<EditProfileFormData>
  onFormDataChange: (values: EditProfileFormData, valid: boolean) => void
  onDelete: () => void
}
interface EditInternshipsInfoViewProps {
  initialValues: EditProfileFormData[]
  submitting: boolean
  setValidForm: React.Dispatch<React.SetStateAction<{
    values: EditProfileFormData[]
    valid: boolean
  }>>
}

function UniversityInfoForm ({
  initialValues,
  onFormDataChange,
  onDelete
}: InternshipsInfoFormProps): React.ReactElement {
  return (
    <FormContainer>
      <Form
        initialValues={initialValues}
        onSubmit={() => ({})}
      >
        <FormSpy
          subscription={{ valid: true, values: true }}
          onChange={({ valid, values }) => {
            onFormDataChange(values, valid)
          }}
        />
        <TitleContainer>
          <TitleText>
            Университет
          </TitleText>
          <DeleteDataButton
            hitSlop={hitSlopParams(16)}
            onPress={onDelete}
          >
            <TrashIcon />
          </DeleteDataButton>
        </TitleContainer>
        <View>
          <InputContainer>
            <Field name='title'>
              {props => (
                <TextInput
                  {...props}
                  numberOfLines={3}
                  placeholder='Например, Россиийский национальный исследовательский медицинский университет имени Н. И. Пирогова'
                  maxLength={universityTitleMaxLength}
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='department'>
              {props => (
                <TextInput
                  {...props}
                  maxLength={departmentMaxLength}
                  placeholder='Например, Факультет Педиатрии'
                  label='Факультет'
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='specialization'>
              {props => (
                <SelectSearch
                  {...props}
                  options={specializationsData}
                  label='Специальность'
                  type='input'
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='graduationYear'>
              {props => (
                <SelectInput
                  {...props}
                  options={_.range(gradulationYearMinYear, gradulationYearMaxYear).map(
                    year => ({ value: year, label: String(year) }))}
                  label='Год окончания'
                />
              )}
            </Field>
          </InputContainer>
        </View>
      </Form>
    </FormContainer>
  )
}

export function EditUniversitiesInfoView ({
  initialValues,
  submitting,
  setValidForm
}: EditInternshipsInfoViewProps): React.ReactElement {
  const [formData, setFormData] = useState(initialValues.map(item => {
    return { ...item, id: uuid() }
  }))
  const [formValid, setFormValid] = useState([...formData.map(data => {
    if (
      data.title != null &&
      data.specialization != null &&
      data.department != null
    ) {
      return true
    } else return false
  })])

  useEffect(() => {
    setValidForm({
      values: formData.map(item => ({
        title: item.title,
        department: item.department,
        specialization: item.specialization,
        graduationYear: item.graduationYear
      })),
      valid: !formValid.some(x => !x)
    })
  }, [formValid, formData])

  return formData.length === 0
    ? (
      <EmptyListContainer>
        <EmptyListText>
          Добавьте информацию о вашем основном{'\n'}образовании, чтобы показать пациентам{'\n'}вашу компетентность
        </EmptyListText>
        <TextButton
          type='primary'
          size='L'
          onPress={() => {
            setFormData([
              {
                title: '',
                graduationYear: undefined,
                specialization: '',
                department: '',
                id: uuid()
              },
              ...formData])
            setFormValid([false, ...formValid])
          }}
        >
          Добавить
        </TextButton>
      </EmptyListContainer>
    ) : (
      <EditContainer>
        <AddEducationButton
          disabled={formData.length > universitiesMaxCount}
          onPress={() => {
            setFormData([
              {
                title: '',
                graduationYear: undefined,
                specialization: '',
                department: '',
                id: uuid()
              },
              ...formData])
            setFormValid([false, ...formValid])
          }}
        >
          <PlusIcon />
          <AddEducationText
            disabledColor={formData.length > universitiesMaxCount}
          >
            Добавить образование
          </AddEducationText>
        </AddEducationButton>
        {formData.map((data, idx) => {
          return (
            <UniversityInfoForm
              initialValues={data}
              onFormDataChange={(data, valid) => {
                const newData = formData
                newData[idx] = { ...data, id: formData[idx]?.id ?? uuid() }
                setFormData(newData)
                setFormValid([...formValid.map((x, index) => { return index === idx ? valid : x })])
              }}
              onDelete={() => {
                setFormData(formData.filter((_, index) => index !== idx))
                setFormValid([...formValid.filter((_, index) => index !== idx)])
              }}
              key={data.id}
            />
          )
        })}
        <ModalLoadingIndicator visible={submitting} />
      </EditContainer>
    )
}
