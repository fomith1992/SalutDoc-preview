import React, { useEffect, useState } from 'react'
import { useAuthenticatedUser } from '../../user-context/user-context-provider'
import {
  useUserEditIntershipsInfoMutation,
  useUserIntershipsInfoQuery
} from '../../../gen/graphql'
import { Alert, Text } from 'react-native'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { useNavigation } from '@react-navigation/native'
import { useDefaultHeader } from '../../../modules/ui-kit/header'
import { ContextMenu, ContextMenuButton } from '../../../modules/ui-kit'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import _ from 'lodash'
import { EditInternshipsInfoView, EditProfileFormData } from './edit-internships-info.view'

export function EditInternshipsInfo (): React.ReactElement {
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const [validForm, setValidForm] = useState<{values: EditProfileFormData[], valid: boolean}>({ values: [], valid: false })
  const user = useAuthenticatedUser()
  const navigation = useNavigation()

  const { data, loading } = useUserIntershipsInfoQuery()

  const initialValues = data?.userMyself?.internships.map(university => {
    return {
      title: university.title,
      specialization: university.specialization,
      graduationYear: university.graduationYear ?? undefined
    }
  })

  useDefaultHeader({
    actions: [
      {
        icon: !validForm.valid || _.isEqual(initialValues, validForm.values) ? 'tick-inactive' : 'tick',
        disabled: !validForm.valid || _.isEqual(initialValues, validForm.values),
        onPress: navigation.goBack
      }
    ]
  })

  useEffect(() =>
    navigation.addListener('beforeRemove', (e) => {
      e.preventDefault()
      if (_.isEqual(initialValues, validForm.values)) {
        navigation.dispatch(e.data.action)
      } else if (bottomMenuVisible) {
        hideBottomMenu()
        navigation.dispatch(e.data.action)
      } else {
        showBottomMenu()
      }
    }),
  [navigation, bottomMenuVisible, validForm]
  )

  const [userEditIntershipsInfoMutation, { loading: submitting }] = useUserEditIntershipsInfoMutation({
    onCompleted: (data) => {
      const { user, message, errors } = data.userEditInternshipsInfo
      if (user == null) {
        console.error(message, errors)
        Alert.alert('Не удалось обновить информацию')
      } else {
        navigation.goBack()
      }
    }
  })

  if (initialValues == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }

  const menuCancelButtons: ContextMenuButton[] = [
    {
      type: 'title',
      title: 'Одно или несколько обязательных полей были удалены. Выйти без сохранения?',
      onPress: () => {}
    },
    {
      type: 'black',
      title: 'Вернуться и заполнить',
      onPress: hideBottomMenu
    },
    {
      type: 'red',
      title: 'Выйти без сохранения изменений',
      onPress: navigation.goBack
    }
  ]

  const menuButtons: ContextMenuButton[] = [
    {
      type: 'title',
      title: 'Сохранить изменения?',
      onPress: () => {}
    },
    {
      type: 'black',
      title: 'Сохранить',
      onPress: () => {
        if (validForm.values != null) {
          userEditIntershipsInfoMutation({
            variables: {
              userId: user.id,
              intershipsInput: {
                internships: [
                  ...validForm.values.map(value => {
                    return {
                      title: value.title,
                      specialization: value.specialization,
                      graduationYear: value.graduationYear ?? null
                    }
                  })
                ]
              }
            }
          })
        }
      }
    },
    {
      type: 'red',
      title: 'Не сохранять',
      onPress: navigation.goBack
    }
  ]

  return (
    <>
      <EditInternshipsInfoView
        initialValues={initialValues}
        submitting={submitting}
        setValidForm={setValidForm}
      />
      <ContextMenu
        visible={bottomMenuVisible}
        onClose={hideBottomMenu}
        buttons={validForm.valid ? menuButtons : menuCancelButtons}
      />
    </>
  )
}
