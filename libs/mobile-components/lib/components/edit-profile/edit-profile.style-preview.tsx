import { Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { WriteIcon20 } from '../../images/write.icon-20'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { ElasticText } from '../elastic-text/elastic-text'

export const EditProfileContainer = styled(ScrollView, {
  flexGrow: 1,
  backgroundColor: color('background')
})

export const AvatarContainer = styled(View, {
  height: sp(80)
})

export const InfoItemsContainer = styled(View, (props: {separator: boolean}) => ({
  marginTop: props.separator ? sp(24) : 0
}))

export const GeneralInfoContainer = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  paddingVertical: sp(24),
  flexDirection: 'row',
  alignItems: 'center'
})

export const BlockTitle = styled(Text, {
  ...font({ type: 'h2', weight: 'light' }),
  marginBottom: sp(8)
})

export const WriteImg = styled(WriteIcon20, {
  color: color('accent')
})

export const Info = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('subtext')
})

export const EmptyText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('subtext'),
  marginTop: sp(16)
})

export const Description = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const EditProfileDataContainer = styled(View, {
  ...container('padding'),
  paddingVertical: sp(24),
  marginTop: sp(8),
  backgroundColor: color('surface')
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const UserName = styled(ElasticText, {
  ...font({ type: 'h2' }),
  color: color('text'),
  marginRight: sp(16)
})

export const UserNameContainer = styled(View, {
  marginLeft: sp(16),
  flexShrink: 1,
  flexDirection: 'column'
})

export const UserInfo = styled(View, {
})

export const TitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: sp(4)
})

export const AttributContainer = styled(View, {
  marginTop: sp(16)
})

export const EditDataButton = TouchableOpacity

export const UpdateGeneralInfoButton = styled(TouchableOpacity, {
  marginLeft: 'auto'
})

export const AttributSpecialization = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(8)
})
