import { createFormDescriptor } from '@salutdoc/react-components'
import React from 'react'
import { DateTimeInput, TextInput, yup } from '../../../modules/form-kit'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'
import {
  RadioButtonContainer,
  InputContainer,
  EditContainer,
  FormContainer,
  Label
} from './edit-basic-info-doctor.style'
import { RadioButton } from '../../../modules/form-kit/radio-button'
import { SelectSearch } from '../../../modules/form-kit/select-search/select-search.view'
import { specializationsData } from '../../../modules/ui-kit'

const firstNameMaxLength = 30
const lastNameMaxLength = 30
const specializationMaxLength = 100

const formSchema = yup.object({
  birthday: yup.date()
    .required()
    .min(new Date('1900-01-01'))
    .max(new Date()),
  gender: yup.mixed()
    .required()
    .oneOf(['MALE', 'FEMALE', 'OTHER'] as const),
  firstName: yup.string()
    .required()
    .max(firstNameMaxLength),
  lastName: yup.string()
    .required()
    .max(lastNameMaxLength),
  specialization: yup.string()
    .max(specializationMaxLength)
    .ensure().defined()
}).required()

export type EditProfileFormData = yup.InferType<typeof formSchema>

const { Form, Field, FormSpy } = createFormDescriptor(formSchema)

interface EditBasicInfoDoctorViewProps {
  initialValues: Partial<EditProfileFormData>
  submitting: boolean
  setValidForm: React.Dispatch<React.SetStateAction<{
    values: EditProfileFormData | null
    valid: boolean
  }>>
}

export function EditBasicInfoDoctorView ({
  setValidForm,
  initialValues,
  submitting
}: EditBasicInfoDoctorViewProps): React.ReactElement {
  return (
    <EditContainer>
      <FormContainer>
        <Form
          initialValues={initialValues}
          onSubmit={() => ({})}
        >
          <FormSpy
            subscription={{ valid: true, values: true }}
            onChange={({ valid, values }) => {
              setValidForm({ values, valid })
            }}
          />
          <InputContainer>
            <Field name='firstName'>
              {props => (
                <TextInput
                  {...props}
                  maxLength={firstNameMaxLength}
                  label='Ваше имя'
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='lastName'>
              {props => (
                <TextInput
                  {...props}
                  maxLength={lastNameMaxLength}
                  label='Ваша фамилия'
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='specialization'>
              {props => (
                <SelectSearch
                  {...props}
                  options={specializationsData}
                  label='Специальность'
                  type='input'
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Field name='birthday'>
              {props => (
                <DateTimeInput
                  {...props}
                  label='Дата рождения'
                />
              )}
            </Field>
          </InputContainer>
          <InputContainer>
            <Label>
              Пол
            </Label>
            <Field name='gender'>
              {props => (
                <RadioButtonContainer>
                  <RadioButton
                    onPress={() => props.onChange('MALE')}
                    active={props.value === 'MALE'}
                    label='Мужчина'
                  />
                  <RadioButton
                    onPress={() => props.onChange('FEMALE')}
                    active={props.value === 'FEMALE'}
                    label='Женщина'
                  />
                </RadioButtonContainer>
              )}
            </Field>
          </InputContainer>
          <ModalLoadingIndicator visible={submitting} />
        </Form>
      </FormContainer>
    </EditContainer>
  )
}
