import { View, ScrollView, Text } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const InputContainer = styled(View, {
  marginTop: sp(24)
})

export const RadioButtonContainer = styled(View, {
  flexDirection: 'row'
})

export const Label = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const EditContainer = styled(ScrollView, {
  flex: 1,
  backgroundColor: color('background')
})

export const FormContainer = styled(View, {
  ...container('padding'),
  backgroundColor: color('surface'),
  paddingBottom: sp(16)
})
