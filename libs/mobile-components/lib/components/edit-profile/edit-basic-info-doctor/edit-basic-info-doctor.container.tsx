import React, { useEffect, useState } from 'react'
import { useAuthenticatedUser } from '../../user-context/user-context-provider'
import { EditBasicInfoDoctorView, EditProfileFormData } from './edit-basic-info-doctor.view'
import { useDoctorBasicInfoQuery, useDoctorEditBasicInfoMutation } from '../../../gen/graphql'
import { Alert, Text } from 'react-native'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { useNavigation } from '@react-navigation/native'
import moment from 'moment'
import { ContextMenu, ContextMenuButton } from '../../../modules/ui-kit'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useDefaultHeader } from '../../../modules/ui-kit/header'
import _ from 'lodash'

export function EditBasicInfoDoctor (): React.ReactElement {
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const [validForm, setValidForm] = useState<{values: EditProfileFormData | null, valid: boolean}>({ values: null, valid: false })
  const user = useAuthenticatedUser()
  const navigation = useNavigation()

  const { data, loading } = useDoctorBasicInfoQuery()

  const initialValues = {
    firstName: data?.userMyself?.firstName,
    specialization: data?.userMyself?.specialization ?? '',
    lastName: data?.userMyself?.lastName,
    birthday: data?.userMyself?.birthday == null ? undefined : new Date(data?.userMyself.birthday),
    gender: data?.userMyself?.gender ?? 'MALE'
  }

  useDefaultHeader({
    actions: [
      {
        icon: !validForm.valid || _.isEqual(initialValues, validForm.values) ? 'tick-inactive' : 'tick',
        disabled: !validForm.valid || _.isEqual(initialValues, validForm.values),
        onPress: navigation.goBack
      }
    ]
  })

  useEffect(() =>
    navigation.addListener('beforeRemove', (e) => {
      e.preventDefault()
      if (_.isEqual(initialValues, validForm.values)) {
        navigation.dispatch(e.data.action)
      } else if (bottomMenuVisible) {
        hideBottomMenu()
        navigation.dispatch(e.data.action)
      } else {
        showBottomMenu()
      }
    }),
  [navigation, bottomMenuVisible, validForm]
  )

  const [doctorEditBasicInfoMutation, { loading: submitting }] = useDoctorEditBasicInfoMutation({
    onCompleted: (data) => {
      const { user, message, errors } = data.userEditBasicInfo
      if (user == null) {
        console.error(message, errors)
        Alert.alert('Не удалось обновить информацию')
      } else {
        navigation.goBack()
      }
    }
  })

  if (data?.userMyself == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }

  const menuCancelButtons: ContextMenuButton[] = [
    {
      type: 'title',
      title: 'Одно или несколько обязательных полей были удалены. Выйти без сохранения?',
      onPress: () => {}
    },
    {
      type: 'black',
      title: 'Вернуться и заполнить',
      onPress: hideBottomMenu
    },
    {
      type: 'red',
      title: 'Выйти без сохранения изменений',
      onPress: navigation.goBack
    }
  ]

  const menuButtons: ContextMenuButton[] = [
    {
      type: 'title',
      title: 'Сохранить изменения?',
      onPress: () => {}
    },
    {
      type: 'black',
      title: 'Сохранить',
      onPress: () => {
        if (validForm.values != null) {
          doctorEditBasicInfoMutation({
            variables: {
              userId: user.id,
              basicInput: {
                firstName: validForm.values.firstName,
                lastName: validForm.values.lastName,
                biography: null,
                city: null,
                birthday: moment(validForm.values.birthday).format('YYYY-MM-DD'),
                country: null,
                gender: validForm.values.gender,
                specialization: validForm.values.specialization
              }
            }
          })
        }
      }
    },
    {
      type: 'red',
      title: 'Не сохранять',
      onPress: navigation.goBack
    }
  ]

  return (
    <>
      <EditBasicInfoDoctorView
        setValidForm={setValidForm}
        initialValues={initialValues}
        submitting={submitting}
      />
      <ContextMenu
        visible={bottomMenuVisible}
        onClose={hideBottomMenu}
        buttons={validForm.valid ? menuButtons : menuCancelButtons}
      />
    </>
  )
}
