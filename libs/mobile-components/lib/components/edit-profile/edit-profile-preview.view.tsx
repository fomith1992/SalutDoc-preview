import React from 'react'
import { hitSlopParams } from '../../modules/ui-kit'
import { AvatarUpload } from '../avatar-upload/avatar-upload.container'

import {
  EditProfileContainer,
  GeneralInfoContainer,
  EditProfileDataContainer,
  Title,
  TitleContainer,
  EditDataButton,
  EmptyText,
  AvatarContainer,
  UserName,
  Description,
  UpdateGeneralInfoButton,
  UserNameContainer,
  AttributSpecialization,
  AttributContainer,
  Info,
  WriteImg
} from './edit-profile.style-preview'

export interface InfoCategoryProps {
  title?: string
  attributes: Array<{
    specialization: string
    data: string[]
  }>
  onPressEdit: () => void
}
export interface userGeneralInfoProps {
  userId: string
  firstName: string
  lastName: string
  specialization: string | null
  resolution: 'PENDING' | 'CONFIRMED' | 'REJECTED'
  handleEditPressed: () => void
}

interface EditProfileViewProps {
  categories: InfoCategoryProps[]
  userGeneralInfo: userGeneralInfoProps
}

export function EditProfileView (
  { categories, userGeneralInfo }: EditProfileViewProps
): React.ReactElement {
  return (
    <EditProfileContainer>
      <GeneralInfoContainer>
        <AvatarContainer>
          <AvatarUpload userId={userGeneralInfo.userId} />
        </AvatarContainer>
        <UserNameContainer>
          <UserName
            numberOfLines={2}
          >
            {`${userGeneralInfo.firstName} ${userGeneralInfo.lastName}`}
          </UserName>
          <Description>
            {userGeneralInfo.specialization}
          </Description>
        </UserNameContainer>
        <UpdateGeneralInfoButton
          hitSlop={hitSlopParams(16)}
          onPress={userGeneralInfo.handleEditPressed}
        >
          <WriteImg />
        </UpdateGeneralInfoButton>
      </GeneralInfoContainer>
      {categories.map(({ title, attributes, onPressEdit }, catIdx) => (
        <EditProfileDataContainer key={`title-${catIdx}`}>
          <TitleContainer>
            <Title>{title}</Title>
            <EditDataButton
              hitSlop={hitSlopParams(16)}
              onPress={onPressEdit}
            >
              <WriteImg />
            </EditDataButton>
          </TitleContainer>
          {attributes.length === 0
            ? <EmptyText key={`empty-${catIdx}`}>Нет информации</EmptyText>
            : attributes.map((attribut, index) => (
              <AttributContainer key={index}>
                <Info>
                  {attribut.data.join(', ')}
                </Info>
                <AttributSpecialization>
                  {attribut.specialization}
                </AttributSpecialization>
              </AttributContainer>
            ))}
        </EditProfileDataContainer>
      ))}
    </EditProfileContainer>
  )
}
