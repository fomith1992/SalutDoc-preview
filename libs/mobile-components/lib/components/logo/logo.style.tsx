import { styled } from '../../style/styled'
import { SalutDocLogoTextIcon } from '../../images/salutdoc-logo-text.icon'
import { color } from '../../style/color'

export const Logotype = styled(SalutDocLogoTextIcon, {
  color: color('accent')
})
