import React from 'react'
import { Logotype } from './logo.style'

export function LogoView (): React.ReactElement {
  return <Logotype />
}
