import {
  AcceptRequestIconSvg,
  DecisionMadeIconSvg,
  EmptyRequestsContainer,
  EmptyRequestsText,
  EmptyRequestsTitle,
  MembershipRequestButtonsRow,
  RejectRequestIconSvg,
  RequestActionButton
} from './community-requests.style'
import { FlatList } from 'react-native-gesture-handler'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import React from 'react'
import { UserRow } from '../user-row/user-row.view'

interface MembershipRequestButtonProps {
  confirmed: boolean
  rejected: boolean
  onAcceptRequest: () => void
  onRejectRequest: () => void
}

interface User {
  id: string
  avatar: string | null
  firstName: string
  lastName: string
  confirmed: boolean
  rejected: boolean
  handleProceedToProfile: () => void
  handleAcceptRequest: () => void
  handleRejectRequest: () => void
}

export interface CommunityRequestsViewProps {
  data: User[]
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
}

const MembershipRequestButton = (
  {
    confirmed,
    rejected,
    onAcceptRequest,
    onRejectRequest
  }: MembershipRequestButtonProps
): React.ReactElement => {
  return (
    <MembershipRequestButtonsRow>
      {confirmed || rejected
        ? (
          <RequestActionButton onPress={undefined}>
            <DecisionMadeIconSvg />
          </RequestActionButton>
        ) : (
          <>
            <RequestActionButton onPress={onAcceptRequest}>
              <AcceptRequestIconSvg />
            </RequestActionButton>
            <RequestActionButton onPress={onRejectRequest}>
              <RejectRequestIconSvg />
            </RequestActionButton>
          </>
        )}
    </MembershipRequestButtonsRow>
  )
}

export function CommunityRequestsView (
  {
    data,
    refreshing,
    loading,
    hasMore,
    refresh,
    loadMore
  }: CommunityRequestsViewProps
): React.ReactElement {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => (
        <UserRow
          id={item.id}
          avatar={item.avatar}
          userName={`${item.firstName} ${item.lastName}`}
          onProceedToProfile={item.handleProceedToProfile}
          actionPerformed={item.confirmed || item.rejected}
          actionPerformedText={item.confirmed
            ? 'Заявка одобрена'
            : item.rejected
              ? 'Заявка отклонена'
              : undefined}
          actions={
            <MembershipRequestButton
              onAcceptRequest={item.handleAcceptRequest}
              onRejectRequest={item.handleRejectRequest}
              confirmed={item.confirmed}
              rejected={item.rejected}
            />
          }
        />
      )}
      keyExtractor={item => item.id}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListFooterComponent={<LoadingIndicator visible={loading} />}
      ListEmptyComponent={() => (
        <EmptyRequestsContainer>
          <EmptyRequestsTitle>
            Нет заявок
          </EmptyRequestsTitle>
          <EmptyRequestsText>
            В данный момент активных заявок на вступление в группу нет
          </EmptyRequestsText>
        </EmptyRequestsContainer>
      )}
      contentContainerStyle={{ flexGrow: 1, marginTop: 8 }}
    />
  )
}
