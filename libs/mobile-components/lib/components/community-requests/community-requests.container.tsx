import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { Text } from 'react-native'
import {
  useCommunityMembershipRequestsListQuery,
  useConfirmMembershipRequestMutation,
  useRejectMembershipRequestMutation
} from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { CommunityRequestsView } from './community-requests.view'

interface CommunityRequestsProps {
  communityId: string
}

export function CommunityRequests ({ communityId }: CommunityRequestsProps): React.ReactElement {
  const navigation = useNavigation()
  const [confirmedWithinSession, setConfirmedWithinSession] = useState<string[]>([])
  const [rejectedWithinSession, setRejectedWithinSession] = useState<string[]>([])
  const { loading, data, fetchMore, refetch } = useCommunityMembershipRequestsListQuery({
    variables: {
      communityId,
      after: null
    },
    fetchPolicy: 'network-only'
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: {
        communityId,
        after: expectDefined(data?.communityById?.incomingMembershipRequests?.pageInfo.endCursor)
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousRequests = expectDefined(previousResult.communityById?.incomingMembershipRequests)
        return {
          ...expectDefined(previousResult),
          communityById: {
            ...expectDefined(previousResult.communityById),
            incomingMembershipRequests: appendEdges(previousRequests, fetchMoreResult?.communityById?.incomingMembershipRequests)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.communityById?.incomingMembershipRequests?.pageInfo.endCursor])
  const [confirmMembershipRequest] = useConfirmMembershipRequestMutation()
  const [rejectMembershipRequest] = useRejectMembershipRequestMutation()
  if (data?.communityById?.incomingMembershipRequests == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <CommunityRequestsView
      data={data.communityById.incomingMembershipRequests.edges
        .filter(({ node: { status, user: { id } } }) =>
          status === 'REQUEST' ||
          confirmedWithinSession.includes(id) ||
          rejectedWithinSession.includes(id)
        )
        .map(({ node: { user: { id, avatar, firstName, lastName } } }) => ({
          id,
          avatar,
          firstName,
          lastName,
          confirmed: confirmedWithinSession.includes(id),
          rejected: rejectedWithinSession.includes(id),
          handleProceedToProfile: (): void => navigation.dispatch(StackActions.push('Profile', { userId: id })),
          handleAcceptRequest: (): void => {
            confirmMembershipRequest({
              variables: {
                userId: id,
                communityId
              }
            }).catch(console.error)
            setConfirmedWithinSession([...confirmedWithinSession, id])
          },
          handleRejectRequest: (): void => {
            rejectMembershipRequest({
              variables: {
                userId: id,
                communityId
              }
            }).catch(console.error)
            setRejectedWithinSession([...rejectedWithinSession, id])
          }
        })) ?? []}
      hasMore={data.communityById.incomingMembershipRequests.pageInfo.hasNextPage ?? false}
      refreshing={refreshing}
      loading={loading}
      refresh={refresh}
      loadMore={() => loadMore()}
    />
  )
}
