import { Text, TouchableOpacity, View } from 'react-native'
import { PlusIcon16 } from '../../images/plus.icon-16'
import { TickIcon24 } from '../../images/tick.icon-24'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { CrossIcon16 } from '../../images/cross.icon-16'

export const MembershipRequestButtonsRow = styled(TouchableOpacity, {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-end',
  marginRight: 'auto'
})

export const RequestActionButton = styled(TouchableOpacity, {
  justifyContent: 'center',
  padding: sp(8)
})

export const AcceptRequestIconSvg = styled(PlusIcon16, {
  color: color('accent')
})

export const RejectRequestIconSvg = styled(CrossIcon16, {
  color: color('error')
})

export const DecisionMadeIconSvg = styled(TickIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('subtext')
})

export const EmptyRequestsContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: sp(48)
})

export const EmptyRequestsTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  textAlign: 'center'
})

export const EmptyRequestsText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(16)
})
