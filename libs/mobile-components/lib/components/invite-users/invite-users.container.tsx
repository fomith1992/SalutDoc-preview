import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React, { useState } from 'react'
import { Text } from 'react-native'
import { useInviteCommunityInfoQuery, useInviteUserMutation, useInviteUsersListQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { inviteToCommunity } from '../../modules/ui-kit'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { useAuthenticatedUser } from '../user-context/user-context-provider'
import { InviteUsersView } from './invite-users.view'

export interface InviteUsersProps {
  communityId: string
}

export function InviteUsers ({ communityId }: InviteUsersProps): React.ReactElement {
  const mobileHostName = expectDefined(process.env.MOBILE_HOST_NAME)
  const user = useAuthenticatedUser()
  const navigation = useNavigation()
  const [invitedWithinSession, setInvitedWithinSession] = useState<string[]>([])
  const [inviteUser] = useInviteUserMutation()
  const community = useInviteCommunityInfoQuery({
    variables: {
      communityId
    }
  })
  const { data, loading, fetchMore, refetch } = useInviteUsersListQuery({
    variables: {
      userId: user.id,
      communityId,
      after: null
    },
    fetchPolicy: 'network-only'
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: {
        userId: user.id,
        communityId,
        after: expectDefined(data?.userById?.followers?.pageInfo.endCursor)
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousFollowers = expectDefined(previousResult.userById?.followers)
        return {
          userById: {
            ...expectDefined(previousResult.userById),
            followers: {
              ...expectDefined(previousResult.userById?.followers),
              followers: appendEdges(previousFollowers, fetchMoreResult?.userById?.followers)
            }
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.userById?.followers?.pageInfo.endCursor])
  if (data?.userById?.followers == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <InviteUsersView
      data={data.userById.followers.edges
        .filter(({ node: { follower: { id, role, membershipByCommunityId } } }) =>
          id !== user.id &&
          (community.data?.communityById?.type === 'PRIVATE' ? role === 'DOCTOR' : 1) &&
          (membershipByCommunityId?.status === 'INACTIVE' || invitedWithinSession.includes(id))
        )
        .map(({ node: { follower: { id, avatar, firstName, lastName, membershipByCommunityId } } }) => ({
          id,
          avatar,
          firstName,
          lastName,
          handleProceedToProfile: (): void => navigation.dispatch(StackActions.push('Profile', { userId: id })),
          handleInviteUser: (): void => {
            inviteUser({
              variables: {
                userId: id,
                communityId,
                message: 'Вас пригласили в группу'
              }
            }).catch(console.error)
            setInvitedWithinSession([...invitedWithinSession, id])
          },
          invitationSent: membershipByCommunityId?.status === 'INVITE'
        })) ?? []}
      hasMore={data.userById.followers.pageInfo.hasNextPage ?? false}
      refreshing={loading && !refreshing}
      loading={loading}
      refresh={refresh}
      loadMore={() => loadMore()}
      handleInviteViaLinkPress={() => {
        inviteToCommunity(`https://${mobileHostName}/community/${communityId}`)
      }}
    />
  )
}
