import { Text, View, TouchableOpacity } from 'react-native'
import { styled } from '../../style/styled'
import { LinkIcon24 } from '../../images/link.icon-24'
import { PlusIcon16 } from '../../images/plus.icon-16'
import { TickIcon24 } from '../../images/tick.icon-24'
import { sp } from '../../style/size'
import { color } from '../../style/color'
import { font } from '../../style/text'

export const InviteViaLinkContainer = styled(TouchableOpacity, {
  marginBottom: sp(8),
  paddingHorizontal: sp(16),
  paddingVertical: sp(12),
  backgroundColor: color('surface'),
  flexDirection: 'row'
})

export const InviteViaLinkIconSvg = styled(LinkIcon24, {
  width: sp(16),
  height: sp(16),
  color: color('accent')
})

export const InviteViaLinkText = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('text'),
  paddingLeft: sp(12)
})

export const InviteUserButton = styled(TouchableOpacity, {
  justifyContent: 'center',
  marginLeft: 'auto',
  padding: sp(8)
})

export const InviteUserIconSvg = styled(PlusIcon16, {
  color: color('accent')
})

export const UserInvitedIconSvg = styled(TickIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('subtext')
})

export const EmptyChallengersContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: sp(48)
})

export const EmptyChallengersTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  textAlign: 'center'
})

export const EmptyChallengersText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(16)
})
