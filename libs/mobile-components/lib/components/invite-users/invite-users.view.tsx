import {
  EmptyChallengersContainer,
  EmptyChallengersText,
  EmptyChallengersTitle,
  InviteUserButton,
  InviteUserIconSvg,
  InviteViaLinkContainer,
  InviteViaLinkIconSvg,
  InviteViaLinkText,
  UserInvitedIconSvg
} from './invite-users.style'
import { FlatList } from 'react-native-gesture-handler'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import React from 'react'
import { UserRow } from '../user-row/user-row.view'

interface InviteViaLinkProps {
  onPress: () => void
}

const InviteViaLink = ({ onPress }: InviteViaLinkProps): React.ReactElement => (
  <InviteViaLinkContainer onPress={onPress}>
    <InviteViaLinkIconSvg />
    <InviteViaLinkText>Пригласить по ссылке</InviteViaLinkText>
  </InviteViaLinkContainer>
)

interface User {
  id: string
  avatar: string | null
  firstName: string
  lastName: string
  handleProceedToProfile: () => void
  handleInviteUser: () => void
  invitationSent: boolean
}

export interface InviteUsersViewProps {
  data: User[]
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
  handleInviteViaLinkPress: () => void
}

export function InviteUsersView (
  {
    data,
    refreshing,
    loading,
    hasMore,
    refresh,
    loadMore,
    handleInviteViaLinkPress
  }: InviteUsersViewProps
): React.ReactElement {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => (
        <UserRow
          id={item.id}
          avatar={item.avatar}
          userName={`${item.firstName} ${item.lastName}`}
          actionPerformed={item.invitationSent}
          actionPerformedText='Приглашение отправлено'
          onProceedToProfile={item.handleProceedToProfile}
          actions={
            <InviteUserButton onPress={item.handleInviteUser}>
              {item.invitationSent
                ? <UserInvitedIconSvg />
                : <InviteUserIconSvg />}
            </InviteUserButton>
          }
        />
      )}
      keyExtractor={item => item.id}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListHeaderComponent={() => (
        <InviteViaLink onPress={handleInviteViaLinkPress} />
      )}
      ListFooterComponent={<LoadingIndicator visible={loading} />}
      ListEmptyComponent={() => (
        <EmptyChallengersContainer>
          <EmptyChallengersTitle>
            Нет пользователей
          </EmptyChallengersTitle>
          <EmptyChallengersText>
            В данный момент нет пользователей, которых можно пригласить в группу
          </EmptyChallengersText>
        </EmptyChallengersContainer>
      )}
      contentContainerStyle={{ flexGrow: 1, marginTop: 8 }}
    />
  )
}
