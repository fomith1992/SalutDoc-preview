import { View } from 'react-native'
import { styled } from '../../style/styled'
import { Theme } from '../../style/theme'

const columns = 8

const chunk = 100 / columns

export const Row = styled(View, {
  flexDirection: 'row',
  marginLeft: ({ layout: { margin, gutter } }) => margin - gutter / 2,
  marginRight: ({ layout: { margin, gutter } }) => margin - gutter / 2
})

export interface ColProps {
  span?: number
  offset?: number
}

export const Col = styled(View, ({ span = 1, offset = 0 }: ColProps) => ({
  flexDirection: 'column',
  marginLeft: `${chunk * offset}%`,
  width: `${chunk * span}%`,
  paddingLeft: ({ layout: { gutter } }: Theme) => gutter / 2,
  paddingRight: ({ layout: { gutter } }: Theme) => gutter / 2
}) as const)
