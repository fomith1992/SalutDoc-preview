import React, { useState } from 'react'
import { TextButton } from '../../../modules/ui-kit'
import {
  ConsultationOverContainer,
  Title,
  Description,
  ButtonContainer,
  TextButtonContainer,
  ConsultationEndContainer,
  ConsultationEndText,
  ConsultationEndTextSecondary
} from './consultation-patient-over.style'
import { Rating } from './rating.view'

interface ConsultationPatientOverProps {
  status?: 'complete' | 'pending' | 'inProcess'
}

interface ConsultationComleteProps {
  onRatingChange: (rating: number) => void
  rating: number
}

function ConsultationPending (): React.ReactElement {
  return (
    <ConsultationOverContainer>
      <Title>
        Время консультации закончилось
      </Title>
      <Description>
        Вы можете продлить консультацию на 15 мин. за 60₽ (предложение доступно в течение 5 мин.).
      </Description>
      <ButtonContainer>
        <TextButtonContainer>
          <TextButton
            size='L'
            type='secondary'
          >
            Продлить
          </TextButton>
        </TextButtonContainer>
        <TextButtonContainer>
          <TextButton
            size='L'
            type='primary'
          >
            Завершить
          </TextButton>
        </TextButtonContainer>
      </ButtonContainer>
    </ConsultationOverContainer>
  )
}

function ConsultationComlete ({ rating, onRatingChange }: ConsultationComleteProps): React.ReactElement {
  return (
    <ConsultationEndContainer>
      <ConsultationEndText>
        Время консультации закончилось.
      </ConsultationEndText>
      <ConsultationEndText>
        Консультация завершена. <ConsultationEndTextSecondary>Врач готовит ваше медзаключение и скоро оно появится здесь.</ConsultationEndTextSecondary>
      </ConsultationEndText>
      <Rating
        rating={rating}
        onRatingChange={onRatingChange}
      />
    </ConsultationEndContainer>
  )
}

export function ConsultationPatientOver ({ status = 'complete' }: ConsultationPatientOverProps): React.ReactElement {
  //! test state
  const [rating, setRating] = useState(0)
  switch (status) {
    case 'pending':
      return (
        <ConsultationPending />
      )
    case 'complete':
      return (
        <ConsultationComlete rating={rating} onRatingChange={setRating} />
      )
    case 'inProcess':
      return (
        <></>
      )
  }
}
