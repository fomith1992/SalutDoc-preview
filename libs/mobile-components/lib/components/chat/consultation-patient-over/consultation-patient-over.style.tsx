import { Text, TouchableOpacity, View } from 'react-native'
import { StarFilledIcon24 } from '../../../images/star-filled.icon-24'
import { StarIcon24 } from '../../../images/star.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const ConsultationOverContainer = styled(View, {
  ...container(),
  marginVertical: sp(48),
  paddingVertical: sp(32),
  borderRadius: sp(16),
  borderWidth: 1,
  borderColor: color('inactiveLight'),
  alignItems: 'center'
})

export const ButtonContainer = styled(View, {
  marginTop: sp(16),
  flexDirection: 'row',
  justifyContent: 'center'
})

export const Title = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  textAlign: 'center'
})

export const Description = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('text'),
  marginTop: sp(8),
  textAlign: 'center',
  maxWidth: 255
})

export const TextButtonContainer = styled(View, {
  marginHorizontal: sp(8)
})

export const ConsultationEndContainer = styled(View, {
  marginVertical: sp(16),
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
})

export const ConsultationEndText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(16),
  marginHorizontal: sp(48)
})

export const ConsultationEndTextSecondary = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  textAlign: 'center'
})

export const RatingContainer = styled(View, {
  marginVertical: sp(16),
  flexDirection: 'column',
  alignItems: 'center'
})

export const StarsContainer = styled(View, {
  marginBottom: sp(16),
  flexDirection: 'row',
  justifyContent: 'center'
})

export const RatingEndTextInactive = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  textAlign: 'center',
  marginBottom: sp(16),
  marginHorizontal: sp(48)
})

export const RatingEndTextPrimary = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center',
  marginBottom: sp(16)
})

export const StarIconButton = styled(TouchableOpacity, {
  paddingHorizontal: sp(8)
})

export const StarImg = styled(StarIcon24, {
  color: color('accent')
})

export const StarFilledImg = styled(StarFilledIcon24, {
  color: color('accent')
})
