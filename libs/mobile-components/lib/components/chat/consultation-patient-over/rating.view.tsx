import React from 'react'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import _ from 'lodash'
import {
  RatingContainer,
  RatingEndTextInactive,
  RatingEndTextPrimary,
  StarIconButton,
  StarsContainer,
  StarImg,
  StarFilledImg
} from './consultation-patient-over.style'
import { TextButton } from '../../../modules/ui-kit'
import { RequestModalView } from '../../request-modal/request-modal.view'
import { RequestModalFinalView } from '../../request-modal/request-modal-final.view'

export interface RatingProps {
  rating: number
  onRatingChange: (rating: number) => void
}

export function Rating ({
  rating,
  onRatingChange
}: RatingProps): React.ReactElement {
  const { state: requestModalVisible, setTrue: showRequestModal, setFalse: hideRequestModal } = useBooleanFlag(false)
  const { state: successModalVisible, setTrue: showSuccessModal, setFalse: hideSuccessModal } = useBooleanFlag(false)
  return (
    <RatingContainer>
      {rating === 0
        ? (
          <RatingEndTextPrimary>
            Оцените консультацию:
          </RatingEndTextPrimary>
        )
        : (
          <RatingEndTextInactive>
            Спасибо за оценку.
          </RatingEndTextInactive>
        )}
      <StarsContainer>
        {_.range(1, 6).map(range => (
          <StarIconButton
            key={range}
            onPress={() => onRatingChange(range)}
          >
            {rating >= range ? <StarFilledImg /> : <StarImg />}
          </StarIconButton>
        ))}
      </StarsContainer>
      {rating === 0 ? null
        : (
          <>
            <RatingEndTextInactive>
              Также вы можете оставить отзыв о том, как прошла консультация.
            </RatingEndTextInactive>
            <TextButton
              onPress={showRequestModal}
              size='L'
              type='secondary'
            >
              Оставить отзыв
            </TextButton>
          </>
        )}
      {!successModalVisible
        ? (
          <RequestModalView
            displaySuccessScreen
            visible={requestModalVisible}
            onCancel={hideRequestModal}
            modalTitle='Отправить отзыв'
            placeholder='Напишите ваши впечатления о враче или о работе нашего приложения в целом.'
            onSubmit={request => {
              hideRequestModal()
              showSuccessModal()
              console.log(request)
              return {}
            }}
          />
        ) : <RequestModalFinalView onClose={hideSuccessModal} />}
    </RatingContainer>
  )
}
