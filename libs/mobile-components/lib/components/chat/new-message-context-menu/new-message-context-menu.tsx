import React, { useMemo } from 'react'
import { ContextMenu, ContextMenuButton } from '../../../modules/ui-kit'
import { filePickerFn } from '../../../modules/ui-kit/image-picker/file-picker-fn'

export interface NewMessageContextMenuProps {
  visible: boolean
  onHideMenu: () => void
}

export function NewMessageContextMenu (
  {
    visible,
    onHideMenu
  }: NewMessageContextMenuProps
): React.ReactElement {
  const buttons = useMemo(() => {
    const buttons: ContextMenuButton[] = []
    buttons.push({
      type: 'black',
      title: 'Сделать фото',
      onPress: () => {
        onHideMenu()
        filePickerFn({ type: 'camera' })
      }
    })
    buttons.push({
      type: 'black',
      title: 'Выбрать из галереи',
      onPress: () => {
        onHideMenu()
        filePickerFn({ type: 'imageLibrary' })
      }
    })
    buttons.push({
      type: 'black',
      title: 'Выбрать из файлов',
      onPress: () => {
        onHideMenu()
        filePickerFn({ type: 'document' })
      }
    })
    return buttons
  }, [])

  return (
    <ContextMenu
      visible={visible}
      onClose={onHideMenu}
      buttons={buttons}
    />
  )
}
