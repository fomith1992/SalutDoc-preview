import React from 'react'
import { ScrollView } from 'react-native'
/* import { toCaseCount } from '../../i18n/utils' */
import { Attachments } from '../../attachments/secondary-attachment.view'
import { RenderLikeButtonView } from '../../likes/like.container'
import { LikeButtonView } from '../../likes/like.view'
import {
  CommentButton,
  WorkAttachmentContainer,
  WorkBlocksContainer,
  WorkButtonCount,
  WorkButtonsContainer,
  WorkContainer,
  WorkContentText,
  WorkContentTitle,
  ChatCloudImg
} from './work.style'

interface WorkTextBlock {
  __typename: 'ArticleTextBlock'
  text: string
}
interface WorkImageBlock {
  __typename: 'ArticleImageBlock'
  url: string
}
interface WorkYouTubeBlock {
  __typename: 'ArticleYouTubeVideoBlock'
  videoId: string
}
interface WorkHeaderBlock {
  __typename: 'ArticleHeaderBlock'
  text: string
}

type WorkBlock = WorkTextBlock | WorkImageBlock | WorkYouTubeBlock | WorkHeaderBlock

interface WorkViewProps {
  blocks: WorkBlock[]
  commentsCount: number
  authorRow: React.ReactElement
  wireLikes: (render: RenderLikeButtonView) => React.ReactElement
  handleCommentsPressed?: () => void
}

export function WorkView ({
  authorRow,
  blocks,
  commentsCount,
  wireLikes,
  handleCommentsPressed
}: WorkViewProps): React.ReactElement {
  return (
    <WorkContainer>
      <ScrollView>
        {authorRow}
        <WorkBlocksContainer>
          {blocks.map((block, index) => {
            switch (block.__typename) {
              case 'ArticleHeaderBlock':
                return (
                  <WorkAttachmentContainer key={index}>
                    <WorkContentTitle>
                      {block.text}
                    </WorkContentTitle>
                  </WorkAttachmentContainer>
                )
              case 'ArticleImageBlock':
                return (
                  <WorkAttachmentContainer key={index}>
                    <Attachments
                      attachments={[{ __typename: 'ImagePostAttachment', url: block.url }]}
                    />
                  </WorkAttachmentContainer>
                )
              case 'ArticleTextBlock':
                return (
                  <WorkAttachmentContainer key={index}>
                    <WorkContentText>
                      {block.text}
                    </WorkContentText>
                  </WorkAttachmentContainer>
                )
              case 'ArticleYouTubeVideoBlock':
                return (
                  <WorkAttachmentContainer key={index}>
                    <Attachments
                      attachments={[{ __typename: 'YouTubeVideoPostAttachment', videoId: block.videoId }]}
                    />
                  </WorkAttachmentContainer>
                )
            }
          })}
        </WorkBlocksContainer>
        <WorkButtonsContainer>
          {wireLikes(props => (
            <LikeButtonView
              {...props}
            />
          ))}
          <CommentButton onPress={handleCommentsPressed}>
            <ChatCloudImg />
            <WorkButtonCount isPressed={false}>{commentsCount > 0 ? commentsCount : ''}</WorkButtonCount>
          </CommentButton>
          {/* <ViewContainer>
            <ViewContent>
              {`${views} ${toCaseCount(['просмотр', 'просмотра', 'просмотров'], views)}`}
            </ViewContent>
          </ViewContainer> */}
        </WorkButtonsContainer>
      </ScrollView>
    </WorkContainer>
  )
}
