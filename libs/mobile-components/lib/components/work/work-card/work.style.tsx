import { Text, View, TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { sp } from '../../../style/size'
import { container } from '../../../style/view'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { ChatCloudIcon20 } from '../../../images/chat-cloud.icon-20'

export const WorkContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface')
})

export const WorkBlocksContainer = styled(View, {
  ...container()
})

export const WorkContentTitle = styled(Text, {
  ...font({ type: 'h2', weight: 'light' })
})

export const WorkContentText = styled(Text, {
  ...font({ type: 'text1' })
})

export const WorkButtonsContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  paddingVertical: sp(8),
  paddingHorizontal: sp(16)
})

export const LikeButton = styled(TouchableOpacity, {
  alignItems: 'center',
  flexDirection: 'row'
})

export const CommentButton = styled(TouchableOpacity, {
  padding: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const WorkButtonCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'h2', weight: 'light' }),
  color: color(props.isPressed ? 'error' : 'inactive'),
  textAlign: 'center',
  marginLeft: sp(4)
}) as const)

export const WorkAttachmentContainer = styled(View, {
  marginBottom: sp(12)
})

export const ViewContainer = styled(View, {
  marginLeft: 'auto'
})

export const ViewContent = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('inactive')
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto'
})

export const MoreSvg = styled(VerticalDotsIcon24, {
  color: color('inactive')
})

export const ChatCloudImg = styled(ChatCloudIcon20, {
  color: color('inactive')
})
