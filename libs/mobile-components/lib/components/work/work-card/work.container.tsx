import React, { useEffect } from 'react'
import { WorkView } from './work.view'
import { useWorkQuery } from '../../../gen/graphql'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { Text } from 'react-native'
import { LikeWork } from '../../likes/like.container'
import { UserPreviewRow } from '../../preview-row/user-preview-row.container'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { WorkContextMenu } from '../work-context-menu/work-context-menu'
import { MoreButton, MoreSvg } from './work.style'
import { hitSlopParams } from '../../../modules/ui-kit'

type WorkProps = {
  workId: string
  handleCommentsPressed?: () => void
}

export function Work ({
  workId,
  handleCommentsPressed = (): void => {}
}: WorkProps): React.ReactElement {
  const { data, loading, error } = useWorkQuery({
    variables: {
      workId
    }
  })
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.workById == null) {
    return <Text>Работа не найдена</Text>
  }

  if (!data.workById.archived) {
    const author = data.workById.ownerUser

    return (
      <WorkView
        authorRow={(
          <UserPreviewRow
            userId={author.id}
            action={(
              <MoreButton
                hitSlop={hitSlopParams(16)}
                onPress={showBottomMenu}
              >
                <MoreSvg />
                <WorkContextMenu
                  goBackOnArchive
                  visible={bottomMenuVisible}
                  workId={workId}
                  onHideMenu={hideBottomMenu}
                />
              </MoreButton>
            )}
          />
        )}
        blocks={data.workById.blocks}
        handleCommentsPressed={handleCommentsPressed}
        commentsCount={data.workById.commentsCount}
        wireLikes={renderLikes => <LikeWork id={workId} render={renderLikes} />}
      />
    )
  } else return <></>
}
