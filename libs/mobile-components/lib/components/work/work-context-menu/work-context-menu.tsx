import React, { useMemo } from 'react'
import { Alert } from 'react-native'
import { RequestModal } from '../../request-modal/request-modal.container'
import { useArchiveWorkMutation, useWorkContextMenuQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { ContextMenu, ContextMenuButton } from '../../../modules/ui-kit'
import { StackActions } from '@react-navigation/native'

interface WorkContextMenuProps {
  visible: boolean
  workId: string
  onHideMenu: () => void
  goBackOnArchive?: boolean
}

export function WorkContextMenu ({
  visible,
  workId,
  onHideMenu,
  goBackOnArchive = false
}: WorkContextMenuProps
): React.ReactElement {
  const navigation = useStackNavigation()
  const { data } = useWorkContextMenuQuery({
    variables: { workId }
  })

  const [archiveWorkMutation] = useArchiveWorkMutation({
    onCompleted: () => {
      if (goBackOnArchive) {
        navigation.goBack()
      }
    },
    onError: (response) => {
      console.log('Failed to delete work', response)
      Alert.alert('Error', 'Failed to delete work')
    }
  })
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)

  const archiveWork = (): void => {
    archiveWorkMutation({
      variables: { workId }
    })
  }

  const buttons = useMemo(() => {
    const buttons: ContextMenuButton[] = []
    if (data?.workById?.canEdit === true) {
      buttons.push({
        type: 'black',
        title: 'Редактировать запись',
        onPress: () => {
          onHideMenu()
          navigation.dispatch(StackActions.push('ArticleEditor', { workId }))
        }
      })
    }
    if (data?.workById?.canArchive === true) {
      buttons.push({
        type: 'black',
        title: 'Удалить запись',
        onPress: () => {
          Alert.alert(
            'Предупреждение!',
            'Вы уверены, что хотите удалить запись?',
            [
              {
                text: 'Отмена',
                onPress: onHideMenu
              },
              {
                text: 'Да',
                onPress: () => {
                  onHideMenu()
                  archiveWork()
                }
              }
            ]
          )
        }
      })
    }
    buttons.push(
      {
        type: 'black',
        title: 'Пожаловаться',
        onPress: () => {
          onHideMenu()
          showReportModal()
        }
      },
      {
        type: 'red',
        title: 'Отмена',
        onPress: onHideMenu
      }
    )
    return buttons
  }, [data])

  return (
    <>
      <ContextMenu
        visible={visible}
        onClose={onHideMenu}
        buttons={buttons}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='work'
        contentId={workId}
      />
    </>
  )
}
