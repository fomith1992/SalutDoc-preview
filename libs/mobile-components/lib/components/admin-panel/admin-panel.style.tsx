import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'

export const Container = styled(View, {
  paddingTop: sp(8),
  flex: 1,
  backgroundColor: color('surface')
})

export const PanelButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  padding: sp(12)
})

export const PanelButtonText = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  marginLeft: sp(8),
  color: color('accent')
})
