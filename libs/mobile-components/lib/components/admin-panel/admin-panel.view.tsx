import React from 'react'
import { Container, PanelButton, PanelButtonText } from './admin-panel.style'

interface AdminPanelViewProps {
  onVerificationPanelPress: () => void
  onArchivePanelPress: () => void
}

export function AdminPanelView ({
  onVerificationPanelPress,
  onArchivePanelPress
}: AdminPanelViewProps): React.ReactElement {
  return (
    <Container>
      <PanelButton
        onPress={onVerificationPanelPress}
      >
        <PanelButtonText>
          Заявки на верификацию
        </PanelButtonText>
      </PanelButton>
      <PanelButton
        onPress={onArchivePanelPress}
      >
        <PanelButtonText>
          Архив
        </PanelButtonText>
      </PanelButton>
    </Container>
  )
}
