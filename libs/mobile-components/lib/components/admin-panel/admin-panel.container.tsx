import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { AdminPanelView } from './admin-panel.view'

export function AdminPanel (): React.ReactElement {
  const navigation = useNavigation()
  return (
    <AdminPanelView
      onVerificationPanelPress={() => navigation.dispatch(StackActions.push('VerificationPanel'))}
      onArchivePanelPress={() => navigation.dispatch(StackActions.push('ArchivePanel'))}
    />
  )
}
