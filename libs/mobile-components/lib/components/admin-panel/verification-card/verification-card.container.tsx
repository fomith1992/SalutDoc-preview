import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { Alert } from 'react-native'
import { useVerificationCardQuery, useConfirmApplicationMutation } from '../../../gen/graphql'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { UserPreviewRow } from '../../preview-row/user-preview-row.container'
import { VerificationCardView } from './verification-card.view'

interface VerificationCardProps {
  id: string
}

export function VerificationCard ({ id }: VerificationCardProps): React.ReactElement {
  const navigation = useNavigation()
  const { data, loading } = useVerificationCardQuery({
    variables: {
      id
    }
  })

  const [confirmApplicationMutation, { loading: submitting }] = useConfirmApplicationMutation({
    onCompleted: (response) => {
      const { verificationApplication, message, errors } = response.verificationApplicationConfirm
      if (verificationApplication == null) {
        console.log('Не удалось выполнить операцию', message, errors)
        Alert.alert('Ошибка', 'Не удалось выполнить операцию')
      } else {
        navigation.goBack()
      }
    },
    onError: (response) => {
      console.log('Ошибка загрузки данных', response)
      Alert.alert('Ошибка загрузки данных')
    }
  })

  if (loading && data?.verificationApplicationById == null) {
    return <LoadingIndicator visible />
  }

  if (data?.verificationApplicationById == null) {
    return <>Произошла ошибка</>
  }

  return (
    <VerificationCardView
      attachment={data.verificationApplicationById.diplomaImage}
      date={data.verificationApplicationById.createdAt}
      resolution={data.verificationApplicationById.resolution}
      handleCancelRequest={() => navigation.dispatch(
        StackActions.push('CancelRequest', { requestId: data.verificationApplicationById.id }))}
      handleSubmitConfirm={() => {
        confirmApplicationMutation({
          variables: {
            applicationId: id
          }
        })
      }}
      previewUser={<UserPreviewRow userId={data.verificationApplicationById.user.id} />}
      submitting={submitting}
    />
  )
}
