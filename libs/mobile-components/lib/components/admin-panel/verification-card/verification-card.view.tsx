import React, { useState } from 'react'
import moment from 'moment'
import { SubmitTextButton } from '../../../modules/ui-kit'
import {
  Container,
  DescriptionText,
  SectionContainer,
  ButtonContainer,
  AttachmentsContainer
} from './verification-card.style'
import { Alert } from 'react-native'
import { MainAttachmentView } from '../../attachments/main-attachment.view'
import { Gallery } from '../../gallery/gallery.container'
import { Resolution } from '../../../gen/graphql'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'

interface VerificationCardViewProps {
  date: string
  attachment: string
  submitting: boolean
  resolution: Resolution
  previewUser: React.ReactElement
  handleCancelRequest: () => void
  handleSubmitConfirm: () => void
}

export function VerificationCardView ({
  date,
  attachment,
  resolution,
  previewUser,
  submitting,
  handleSubmitConfirm,
  handleCancelRequest
}: VerificationCardViewProps): React.ReactElement {
  const [galleryShowIndex, setGalleryShowIndex] = useState(1)
  const [galleryVisible, setGalleryVisible] = useState(false)
  function onShowGallery (showIndex: number): void {
    setGalleryShowIndex(showIndex)
    setGalleryVisible(true)
  }

  return (
    <Container
      contentContainerStyle={{ flexGrow: 1 }}
    >
      {previewUser}
      <SectionContainer>
        <DescriptionText>
          {`Заявка на верификацию профиля от ${moment(date).format('Do MMMM YYYY')}`}
        </DescriptionText>
      </SectionContainer>
      <AttachmentsContainer onPress={() => onShowGallery(0)}>
        <MainAttachmentView
          uri={attachment}
        />
        <Gallery
          attachments={[{ __typename: 'ImagePostAttachment', url: attachment }]}
          showIndex={galleryShowIndex}
          galleryVisible={galleryVisible}
          onClose={() => setGalleryVisible(false)}
        />
      </AttachmentsContainer>
      {resolution !== 'PENDING' ? null
        : (
          <SectionContainer>
            <ButtonContainer>
              <SubmitTextButton
                valid
                onSubmit={() => {
                  Alert.alert(
                    'Предупреждение!',
                    'Вы уверены, что это врач?',
                    [
                      {
                        text: 'Отмена',
                        onPress: () => {}
                      },
                      {
                        text: 'Да',
                        onPress: handleSubmitConfirm
                      }
                    ]
                  )
                }}
                type='primary'
                size='L'
              >
                Подтвердить профиль
              </SubmitTextButton>
            </ButtonContainer>
            <ButtonContainer>
              <SubmitTextButton
                valid
                onSubmit={handleCancelRequest}
                type='cancel-button'
                size='L'
              >
                Отклонить заявку
              </SubmitTextButton>
            </ButtonContainer>
          </SectionContainer>
        )}
      <ModalLoadingIndicator visible={submitting} />
    </Container>
  )
}
