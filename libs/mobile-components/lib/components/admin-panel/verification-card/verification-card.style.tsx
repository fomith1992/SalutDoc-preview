import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const Container = styled(ScrollView, {
  flex: 1,
  backgroundColor: color('surface')
})

export const SectionContainer = styled(View, {
  ...container('padding'),
  marginVertical: sp(8)
})

export const AttachmentsContainer = styled(TouchableOpacity, {
  ...container('padding'),
  marginBottom: sp(32)
})

export const DescriptionText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text'),
  marginBottom: sp(8)
})

export const ButtonContainer = styled(View, {
  marginVertical: sp(8)
})
