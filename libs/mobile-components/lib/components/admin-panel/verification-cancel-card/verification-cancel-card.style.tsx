import { Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const Container = styled(View, {
  ...container('padding'),
  paddingTop: sp(16),
  flex: 1,
  backgroundColor: color('surface')
})

export const SectionContainer = styled(View, {
  marginVertical: sp(24)
})

export const TitleText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  marginBottom: sp(8)
})

export const ButtonContainer = styled(View, {
  marginVertical: sp(8)
})
