import React, { useState } from 'react'
import { TextInput } from '../../../modules/form-kit'
import { SubmitTextButton } from '../../../modules/ui-kit'
import { ModalLoadingIndicator } from '../../loading-indicator/modal-loading-indicator.view'
import {
  Container,
  TitleText,
  SectionContainer,
  ButtonContainer
} from './verification-cancel-card.style'

interface VerificationCancelCardViewProps {
  onSubmit: (message: string) => void
  submitting: boolean
}

export function VerificationCancelCardView ({
  onSubmit,
  submitting
}: VerificationCancelCardViewProps): React.ReactElement {
  const [requestText, setRequestText] = useState('')
  return (
    <Container>
      <TitleText>
        Отклонение заявки
      </TitleText>
      <TextInput
        value={requestText}
        onChange={setRequestText}
        placeholder='Опишите ситуацию'
        numberOfLines={8}
      />
      <SectionContainer>
        <ButtonContainer>
          <SubmitTextButton
            valid
            onSubmit={() => onSubmit(requestText)}
            type='primary'
            size='L'
          >
            Отправить
          </SubmitTextButton>
        </ButtonContainer>
      </SectionContainer>
      <ModalLoadingIndicator visible={submitting} />
    </Container>
  )
}
