import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { useRejectApplicationMutation } from '../../../gen/graphql'
import { Alert } from 'react-native'
import { VerificationCancelCardView } from './verification-cancel-card.view'

interface VerificationCancelCardProps {
  id: string
}

export function VerificationCancelCard ({ id }: VerificationCancelCardProps): React.ReactElement {
  const navigation = useNavigation()
  const [rejectApplicationMutation, { loading: submitting }] = useRejectApplicationMutation({
    onCompleted: (response) => {
      const { verificationApplication, message, errors } = response.verificationApplicationReject
      if (verificationApplication == null) {
        console.log('Не удалось выполнить операцию', message, errors)
        Alert.alert('Ошибка', 'Не удалось выполнить операцию')
      } else {
        navigation.goBack()
      }
    },
    onError: (response) => {
      console.log('Ошибка загрузки данных', response)
      Alert.alert('Ошибка загрузки данных')
    }
  })
  return (
    <VerificationCancelCardView
      onSubmit={message => {
        rejectApplicationMutation({
          variables: {
            applicationId: id,
            rejectReason: message
          }
        })
      }}
      submitting={submitting}
    />
  )
}
