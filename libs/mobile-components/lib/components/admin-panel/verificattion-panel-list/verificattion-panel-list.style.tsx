import { Platform, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const Container = styled(View, {
  flex: 1,
  backgroundColor: color('surface')
})

export const Indent = styled(View, {
  paddingTop: sp(8)
})

export const CardShadow = styled(TouchableOpacity, {
  paddingHorizontal: sp(8),
  paddingVertical: sp(12),
  marginBottom: sp(8),
  backgroundColor: color('background'),
  borderRadius: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const TitleText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('accent'),
  marginVertical: sp(8)
})

export const DescriptionText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text'),
  marginBottom: sp(8)
})
