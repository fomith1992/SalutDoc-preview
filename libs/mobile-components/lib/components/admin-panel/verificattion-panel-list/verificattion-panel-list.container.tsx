import { StackActions, useNavigation } from '@react-navigation/native'
import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { Text } from 'react-native'
import {
  useVerificationApplicationsListQuery
} from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'
import { VerificationPanelListView } from './verificattion-panel-list.view'

interface VerificationPanelListProps {
  type: 'verification' | 'archive'
}

export function VerificationPanelList ({ type }: VerificationPanelListProps): React.ReactElement {
  const navigation = useNavigation()
  const { data, loading, fetchMore, refetch } = useVerificationApplicationsListQuery({
    variables: {
      resolutions: type === 'archive'
        ? ['CONFIRMED', 'REJECTED']
        : ['PENDING']
    }
  })

  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.verificationApplications?.pageInfo.endCursor) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousRequests = expectDefined(previousResult.verificationApplications)
        return {
          verificationApplications: {
            ...expectDefined(previousResult.verificationApplications),
            verificationApplications: appendEdges(previousRequests, fetchMoreResult?.verificationApplications)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.verificationApplications?.pageInfo.endCursor])

  if (data?.verificationApplications == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }

  return (
    <VerificationPanelListView
      data={data.verificationApplications.edges
        .map(({ node }) => {
          return {
            requestId: node.id,
            date: node.createdAt,
            status: node.resolution,
            onRequestPress: () => navigation.dispatch(
              StackActions.push('VerificationCard', { requestId: node.id }))
          }
        })}
      hasMore={data?.verificationApplications.pageInfo.hasNextPage ?? false}
      refreshing={refreshing}
      loading={loading && !refreshing}
      refresh={refresh}
      loadMore={() => loadMore()}
    />
  )
}
