import React from 'react'
import moment from 'moment'
import { FlatList } from 'react-native'
import { CardShadow, TitleText, DescriptionText, Container, Indent } from './verificattion-panel-list.style'
import { LoadingIndicator } from '../../loading-indicator/loading-indicator.view'

interface RequestItem {
  requestId: string
  date: string
  status?: 'PENDING' | 'CONFIRMED' | 'REJECTED'
  onRequestPress: () => void
}

interface VerificationPanelListViewProps {
  data: RequestItem[]
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
}

const renderItem = ({ requestId, date, onRequestPress, status }: RequestItem): React.ReactElement => {
  return (
    <CardShadow onPress={onRequestPress}>
      <TitleText>{`Заявка #${requestId}`}</TitleText>
      {status !== 'PENDING'
        ? (status === 'CONFIRMED'
          ? <DescriptionText>Заявка подтверждена</DescriptionText>
          : <DescriptionText>Заявка отклонена</DescriptionText>)
        : (
          <DescriptionText>
            {`Заявка на верификацию профиля от ${moment(date).format('Do MMMM YYYY')}`}
          </DescriptionText>
        )}
    </CardShadow>
  )
}

export function VerificationPanelListView ({
  data,
  refreshing,
  hasMore,
  loading,
  refresh,
  loadMore
}: VerificationPanelListViewProps): React.ReactElement {
  return (
    <Container>
      <FlatList
        data={data}
        ListHeaderComponent={<Indent />}
        renderItem={({ item }) => renderItem(item)}
        keyExtractor={item => item.requestId}
        onEndReached={hasMore ? loadMore : null}
        refreshing={refreshing}
        onRefresh={refresh}
        ListFooterComponent={
          <LoadingIndicator visible={loading} />
        }
      />
    </Container>
  )
}
