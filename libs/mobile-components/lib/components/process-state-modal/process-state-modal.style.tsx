import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { container } from '../../style/view'
import { CrossIcon24 } from '../../images/cross.icon-24'
import { TickIcon56 } from '../../images/tick.icon-56'

export const Container = styled(View, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface')
})

export const Centered = styled(View, {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center'
})

export const LoadingIndicatorContainer = styled(View, {
  padding: sp(8)
})

export const Title = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('text'),
  marginTop: sp(32),
  textAlign: 'center'
})

export const Info = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(12),
  textAlign: 'center'
})

export const SuccessIconSvg = styled(TickIcon56, {
  width: sp(56),
  height: sp(56),
  color: color('accent')
})

export const FailureIconContainer = styled(View, {
  padding: sp(8)
})

export const FailureIconSvg = styled(CrossIcon24, {
  color: color('error'),
  width: sp(64),
  height: sp(64)
})

export const ButtonContainer = styled(View, {
  marginTop: 'auto',
  marginBottom: sp(24)
})
