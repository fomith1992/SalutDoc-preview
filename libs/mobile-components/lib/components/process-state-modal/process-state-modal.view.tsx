import React from 'react'
import {
  Container,
  Centered,
  Title,
  Info,
  SuccessIconSvg,
  FailureIconSvg,
  ButtonContainer,
  LoadingIndicatorContainer
} from './process-state-modal.style'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { Modal, TextButton } from '../../modules/ui-kit'
import { SafeAreaView } from 'react-native'
import { CircleWithIcon } from '../../modules/ui-kit/circle-with-icon/circle-with-icon.view'

interface ButtonProps {
  text: string
  handlePress: () => void
}

export interface ProcessState {
  active: boolean
  title: string
  info: string
  button?: ButtonProps
}

export interface ProcessStateModalProps {
  visible: boolean
  setVisible: (state: boolean) => void
  stateInProgress: ProcessState
  stateSuccess: ProcessState
  stateFailure: ProcessState
}

export function ProcessStateModalView (
  {
    visible,
    setVisible,
    stateInProgress,
    stateSuccess,
    stateFailure
  }: ProcessStateModalProps
): React.ReactElement {
  const closeAnd = (callback?: () => void) => () => {
    setVisible(false)
    callback?.()
  }
  const processing = stateInProgress.active
  const error = stateFailure.active
  const activeState = processing
    ? stateInProgress
    : error
      ? stateFailure
      : stateSuccess
  return (
    <Modal visible={visible}>
      <SafeAreaView
        style={{ flex: 1 }}
      >
        <Container>
          <Centered>
            {processing
              ? (
                <LoadingIndicatorContainer>
                  <LoadingIndicator visible size='enormous' />
                </LoadingIndicatorContainer>
              ) : error
                ? <FailureIconSvg />
                : <CircleWithIcon filled><SuccessIconSvg /></CircleWithIcon>}
            <Title>{activeState.title}</Title>
            <Info>{activeState.info}</Info>
          </Centered>
          {activeState.button == null
            ? null
            : (
              <ButtonContainer>
                <TextButton
                  onPress={closeAnd(activeState.button.handlePress)}
                  type='primary'
                  size='XL'
                >
                  {activeState.button.text}
                </TextButton>
              </ButtonContainer>
            )}
        </Container>
      </SafeAreaView>
    </Modal>
  )
}
