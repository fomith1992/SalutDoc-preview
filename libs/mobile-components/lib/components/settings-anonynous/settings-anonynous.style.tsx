import { Text, TouchableOpacity, View } from 'react-native'
import { LogoutIcon24 } from '../../images/logout.icon-24'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { SubscribeIcon24 } from '../../images/subscribe.icon-24'
import { HelpIcon24 } from '../../images/help.icon-24'
import { StarIcon24 } from '../../images/star.icon-24'

export const SettingsContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface'),
  paddingTop: sp(8)
})

export const SettingButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  paddingVertical: sp(12),
  paddingHorizontal: sp(16)
})

export const SettingTitle = styled(Text, {
  ...font({ type: 'text1' }),
  marginLeft: sp(16),
  color: color('text')
})

export const LoginSvg = styled(LogoutIcon24, {
  color: color('accent')
})

export const SubscribeIcon = styled(SubscribeIcon24, {
  color: color('accent')
})

export const HelpSvg = styled(HelpIcon24, {
  color: color('accent')
})

export const StarSvg = styled(StarIcon24, {
  color: color('accent')
})

export const BottomContainer = styled(View, {
  marginTop: 'auto',
  marginBottom: sp(24),
  paddingHorizontal: sp(16)
})

export const BottomText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  textAlign: 'center'
})

export const BottomLink = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center'
})
