import React from 'react'
import { ExternalLink } from '../../modules/ui-kit'
import {
  BottomContainer,
  BottomLink,
  BottomText,
  LoginSvg,
  StarSvg,
  SettingButton,
  SettingsContainer,
  SettingTitle,
  SubscribeIcon,
  HelpSvg
} from './settings-anonynous.style'

interface SettingsViewProps {
  privacyPolicyHref: string
  termsOfServiceHref: string
  feedbacktUrl: string
  supportHref: string
  onLogin: () => void
  onRegister: () => void
}

export function SettingsView (
  {
    privacyPolicyHref,
    termsOfServiceHref,
    supportHref,
    feedbacktUrl,
    onLogin,
    onRegister
  }: SettingsViewProps
): React.ReactElement {
  return (
    <SettingsContainer>
      <SettingButton onPress={onLogin}>
        <LoginSvg />
        <SettingTitle>Войти в свой аккаунт</SettingTitle>
      </SettingButton>
      <SettingButton onPress={onRegister}>
        <SubscribeIcon />
        <SettingTitle>Зарегистрироваться</SettingTitle>
      </SettingButton>
      <ExternalLink href={supportHref}>
        {({ onPress }) => (
          <SettingButton onPress={onPress}>
            <HelpSvg />
            <SettingTitle>Помощь</SettingTitle>
          </SettingButton>
        )}
      </ExternalLink>
      <ExternalLink href={feedbacktUrl}>
        {({ onPress }) => (
          <SettingButton onPress={onPress}>
            <StarSvg />
            <SettingTitle>Оставить отзыв или предложение</SettingTitle>
          </SettingButton>
        )}
      </ExternalLink>
      <BottomContainer>
        <BottomText>
          Пользуясь этим приложением, Вы соглашаетесь с{' '}
          <ExternalLink href={privacyPolicyHref}>
            {({ onPress }) => (
              <BottomLink onPress={onPress}>
                Политикой конфиденциальности
              </BottomLink>
            )}
          </ExternalLink>
          {' '}и{' '}
          <ExternalLink href={termsOfServiceHref}>
            {({ onPress }) => (
              <BottomLink onPress={onPress}>
                Пользовательским соглашением
              </BottomLink>
            )}
          </ExternalLink>
        </BottomText>
      </BottomContainer>
    </SettingsContainer>
  )
}
