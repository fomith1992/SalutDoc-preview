import React from 'react'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { SettingsView } from './settings-anonynous.view'
import { StackActions } from '@react-navigation/native'
import { useRootStore } from '../../stores/root.store'

const feedbacktUrl = 'https://salutdoc.atlassian.net/servicedesk/customer/portal/1'
const supportUrl = 'https://salutdoc.atlassian.net/servicedesk/customer/portal/1'
const termsOfServiceUrl = 'https://about.salutdoc.com/terms-of-service'
const privacyPolicyUrl = 'https://about.salutdoc.com/privacy/'

export function SettingsAnonymous (): React.ReactElement {
  const navigation = useStackNavigation()
  const {
    setMode
  } = useRootStore().domains.registerReport
  return (
    <SettingsView
      privacyPolicyHref={privacyPolicyUrl}
      termsOfServiceHref={termsOfServiceUrl}
      supportHref={supportUrl}
      feedbacktUrl={feedbacktUrl}
      onLogin={() => navigation.dispatch(StackActions.push('Login_Stack', {}))}
      onRegister={() => {
        setMode('DEFAULT')
        navigation.dispatch(StackActions.push('Register_PatientStack', {}))
      }}
    />
  )
}
