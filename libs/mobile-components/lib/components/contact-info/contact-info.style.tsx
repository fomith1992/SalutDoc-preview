import { ScrollView, Text, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const ContactInfoContainer = ScrollView

export const ContactInfoCategoryContainer = styled(View, {
  ...container('padding'),
  paddingVertical: sp(24),
  marginTop: sp(8),
  backgroundColor: color('surface')
})

export const TitleContainer = View
export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const InformationContainer = styled(View, {
  marginTop: sp(16)
})

export const InformationText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('subtext')
})
export const SpecializationText = styled(Text, {
  marginTop: sp(8),
  ...font({ type: 'text1' }),
  color: color('text')
})
