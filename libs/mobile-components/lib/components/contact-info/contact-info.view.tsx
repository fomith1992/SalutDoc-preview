import React from 'react'
import {
  ContactInfoContainer,
  Title,
  TitleContainer,
  ContactInfoCategoryContainer,
  InformationContainer,
  InformationText,
  SpecializationText
} from './contact-info.style'
import _ from 'lodash'

interface AttributesInfoProps {
  title: string
  specialization: string
  graduationYear: string
  department?: string
  hours?: string
}

interface CategoryInfoProps {
  title: string
  attributes: AttributesInfoProps[] | null
}

type CategoryTypeProps = 'education' | 'interships' | 'trainings'

export type ContactInfoViewProps = Record<CategoryTypeProps, CategoryInfoProps>

function ContactInfoCategoryView (
  props: {
    title: string
    information: AttributesInfoProps[] | null
  }
): React.ReactElement {
  const { title, information } = props
  return (
    <ContactInfoCategoryContainer>
      <TitleContainer>
        <Title>{title}</Title>
      </TitleContainer>
      {
        information == null ? (
          <InformationText>
            Нет информации
          </InformationText>
        ) : information.map((item, idx) => (
          <InformationContainer key={`${title}-${idx}`}>
            <InformationText>
              {Object.values(_.pickBy(_.omit(item, 'specialization'), _.identity)).join(', ')}
            </InformationText>
            <SpecializationText>
              {item.specialization}
            </SpecializationText>
          </InformationContainer>
        ))
      }
    </ContactInfoCategoryContainer>
  )
}

export const ContactInfoView = (
  props: {
    contactInfo: ContactInfoViewProps
  }
): React.ReactElement => {
  const { contactInfo } = props
  return (
    <ContactInfoContainer>
      {Object.entries(contactInfo).map(([category, information]) => {
        return (
          <ContactInfoCategoryView
            key={category}
            title={information.title}
            information={information.attributes}
          />
        )
      })}
    </ContactInfoContainer>
  )
}
