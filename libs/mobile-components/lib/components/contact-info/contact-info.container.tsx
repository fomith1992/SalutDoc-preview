import React, { useEffect } from 'react'
import { Text } from 'react-native'
import moment from 'moment'
import { ContactInfoView, ContactInfoViewProps } from './contact-info.view'
import { Gender, useUserContactInfoQuery } from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'

interface ContactInfoProps {
  userId: string
}

type GenderType = 'Мужской' | 'Женский' | 'Другое'

moment.locale('ru')

export const genderParse = (gender: Gender): GenderType => {
  switch (gender) {
    case 'MALE':
      return 'Мужской'
    case 'FEMALE':
      return 'Женский'
    default:
      return 'Другое'
  }
}

export const ContactInfo = ({ userId }: ContactInfoProps): React.ReactElement => {
  const { data, loading, error } = useUserContactInfoQuery({
    variables: { userId }
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible={loading} />
  }
  if (data?.userById == null) {
    return <Text>Not found</Text>
  }

  const categoriesData: ContactInfoViewProps = {
    education: {
      title: 'Образование',
      attributes: data.userById.universities.length === 0 ? null : data.userById.universities.map(item => ({
        title: item.title,
        specialization: item.specialization,
        graduationYear: item.graduationYear == null ? '' : item.graduationYear.toString(),
        department: item.department
      }))
    },
    interships: {
      title: 'Интернатура/Ординатура',
      attributes: data.userById.internships.length === 0 ? null : data.userById.internships.map(item => ({
        title: item.title,
        specialization: item.specialization,
        graduationYear: item.graduationYear == null ? '' : item.graduationYear.toString()
      }))
    },
    trainings: {
      title: 'Курсы повышения квалификации',
      attributes: data.userById.trainings.length === 0 ? null : data.userById.trainings.map(item => ({
        title: item.title,
        specialization: item.specialization,
        graduationYear: item.graduationYear == null ? '' : item.graduationYear.toString(),
        hours: `${item.hours.toString()} ч.`
      }))
    }
  }
  return <ContactInfoView contactInfo={categoriesData} />
}
