import {
  Container,
  AvatarContainer,
  TextContainer,
  UserNameText,
  ActionPerformedText
} from './user-row.style'
import { Avatar } from '../avatar/avatar.view'
import React from 'react'

export interface UserRowProps {
  id: string
  avatar: string | null
  userName: string
  onProceedToProfile: () => void
  actionPerformed?: boolean
  actionPerformedText?: string
  actions?: React.ReactNode
}

export function UserRow (
  {
    id,
    avatar,
    userName,
    onProceedToProfile,
    actionPerformed = false,
    actionPerformedText,
    actions
  }: UserRowProps
): React.ReactElement {
  return (
    <Container key={id}>
      <AvatarContainer onPress={onProceedToProfile}>
        <Avatar url={avatar} />
      </AvatarContainer>
      <TextContainer>
        <UserNameText onPress={onProceedToProfile}>{userName}</UserNameText>
        {actionPerformed && <ActionPerformedText>{actionPerformedText}</ActionPerformedText>}
      </TextContainer>
      {actions}
    </Container>
  )
}
