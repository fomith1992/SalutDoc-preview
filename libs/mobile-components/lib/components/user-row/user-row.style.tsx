import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

export const Container = styled(View, {
  flex: 1,
  flexDirection: 'row',
  backgroundColor: color('surface'),
  paddingLeft: sp(16),
  paddingRight: sp(8),
  paddingVertical: sp(8)
})

export const AvatarContainer = styled(TouchableOpacity, {
  width: sp(48),
  borderRadius: sp(24),
  borderColor: color('inactiveLight'),
  borderWidth: 1,
  overflow: 'hidden'
})

export const TextContainer = styled(View, {
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center',
  paddingLeft: sp(16)
})

export const UserNameText = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('text')
})

export const ActionPerformedText = styled(Text, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('subtext')
})
