import React from 'react'
import { FlatList } from 'react-native'
import { Avatar } from '../avatar/avatar.view'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { ModalLoadingIndicator } from '../loading-indicator/modal-loading-indicator.view'
import {
  AvatarContainer,
  InfiniteListContainer,
  ListContainer,
  ListItemActions,
  ListItemContainer,
  ListItemDescription,
  ListItemInfoContainer,
  ListItemTitle
} from './list.style'

export interface ListItemProps {
  id: string
  title: string
  description: string | null
  avatar?: string | null
  action?: React.ReactElement
  type?: 'profile' | 'community'
  onPress?: () => void
}

export interface ListProps {
  items: ListItemProps[]
}

export interface InfiniteListProps {
  items: ListItemProps[]
  hasMore: boolean
  loading: boolean
  loadMore: () => void
  refreshing: boolean
  refresh: () => void
  submitting?: boolean
}

const ListItemAvatar = (props: { url?: string | null }): React.ReactElement => {
  return (
    <AvatarContainer>
      <Avatar
        url={props.url}
      />
    </AvatarContainer>
  )
}

export const ListItem = ({
  avatar,
  title,
  description,
  action,
  onPress
}: ListItemProps): React.ReactElement => {
  return (
    <ListItemContainer onPress={onPress}>
      {avatar !== undefined ? <ListItemAvatar url={avatar} /> : null}
      <ListItemInfoContainer>
        <ListItemTitle
          numberOfLines={2}
        >
          {title}
        </ListItemTitle>
        <ListItemDescription
          numberOfLines={2}
        >
          {description}
        </ListItemDescription>
      </ListItemInfoContainer>
      <ListItemActions>
        {action}
      </ListItemActions>
    </ListItemContainer>
  )
}

export const InfiniteList = (
  {
    items,
    loading,
    hasMore,
    loadMore,
    refreshing,
    refresh,
    submitting = false
  }: InfiniteListProps
): React.ReactElement => {
  const renderItem = ({ item }: { item: ListItemProps }): React.ReactElement => (
    <ListItem {...item} />
  )
  const keyExtractor = (_item: unknown, index: number): string => index.toString()

  return (
    <InfiniteListContainer>
      <FlatList<ListItemProps>
        data={items}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        onEndReached={hasMore ? loadMore : null}
        ListFooterComponent={
          <LoadingIndicator visible={loading} />
        }
        refreshing={refreshing}
        onRefresh={refresh}
      />
      <ModalLoadingIndicator visible={submitting} />
    </InfiniteListContainer>
  )
}

export const List = ({ items }: ListProps): React.ReactElement => {
  return (
    <ListContainer>
      {items.map((item, idx) => <ListItem {...item} key={idx} />)}
    </ListContainer>
  )
}
