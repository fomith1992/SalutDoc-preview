import { TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'
import { sp } from '../../style/size'
import { ElasticText } from '../elastic-text/elastic-text'

export const InfiniteListContainer = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})

export const ListContainer = styled(View, {
  backgroundColor: color('surface')
})

export const ListItemContainer = styled(TouchableOpacity, {
  ...container('padding'),
  backgroundColor: color('surface'),
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarContainer = styled(View, {
  width: sp(40),
  height: sp(40),
  marginTop: sp(12),
  marginBottom: sp(12)
})

export const ListItemInfoContainer = styled(View, {
  marginHorizontal: sp(12),
  flex: 1
})

export const ListItemTitle = styled(ElasticText, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const ListItemDescription = styled(ElasticText, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext')
})

export const ListItemActions = View
