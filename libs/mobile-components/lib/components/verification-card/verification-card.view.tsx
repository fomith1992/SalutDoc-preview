import * as React from 'react'
import { ScreenBlock, TextButton } from '../../modules/ui-kit'
import {
  Body,
  BodyButton,
  BodyText,
  Container,
  DefaultHeaderText,
  Footer,
  Header,
  RejectedHeaderText
} from './verification-card.style'

interface Application {
  resolution: 'PENDING' | 'CONFIRMED' | 'REJECTED'
}

export interface VerificationCardViewProps {
  application: Application | null
  onCreateApplicationButtonPress: () => void
  onCancelVerificationButtonPress: () => void
}

interface CardHeader {
  text: string
  rejected: boolean
}

export function VerificationCardView ({
  application,
  onCreateApplicationButtonPress,
  onCancelVerificationButtonPress
}: VerificationCardViewProps): React.ReactElement {
  let header: CardHeader
  let buttonText: string
  let footerVisible: boolean
  if (application == null) {
    header = {
      text: 'Подтвердите свой аккаунт врача',
      rejected: false
    }
    buttonText = 'Пройти верификацию'
    footerVisible = true
  } else {
    switch (application.resolution) {
      case 'CONFIRMED':
        return <></>
      case 'PENDING': {
        header = {
          text: 'Ваша заявка на рассмотрении',
          rejected: false
        }
        buttonText = 'Документы на проверке'
        break
      }
      case 'REJECTED': {
        header = {
          text: 'Ваша заявка отклонена',
          rejected: true
        }
        buttonText = 'Отправить снова'
        break
      }
    }
    footerVisible = false
  }

  return (
    <ScreenBlock>
      <Container>
        <Header>
          {header.rejected
            ? (
              <RejectedHeaderText>
                {header.text}
              </RejectedHeaderText>
            ) : (
              <DefaultHeaderText>
                {header.text}
              </DefaultHeaderText>
            )}
        </Header>
        <Body>
          <BodyText>
            Вам будут недоступны некоторые
            возможности сервиса, пока Вы не
            подтвердите аккаунт врача
          </BodyText>
          <BodyButton>
            <TextButton
              size='L'
              type='primary'
              onPress={onCreateApplicationButtonPress}
            >
              {buttonText}
            </TextButton>
          </BodyButton>
        </Body>
        {footerVisible
          ? (
            <Footer>
              <TextButton
                type='link'
                onPress={onCancelVerificationButtonPress}
              >
                Не нужно, я не врач
              </TextButton>
            </Footer>
          ) : null}
      </Container>
    </ScreenBlock>
  )
}
