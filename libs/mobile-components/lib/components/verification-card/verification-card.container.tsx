import * as React from 'react'
import { VerificationCardView } from './verification-card.view'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { useLastUserVerificationApplicationQuery } from '../../gen/graphql'
import { Text } from 'react-native'
import { useNavigation, StackActions } from '@react-navigation/native'
import { useRootStore } from '../../stores/root.store'
import { useAuthenticatedUserOrNull } from '../../components/user-context/user-context-provider'
import { observer } from 'mobx-react'

export interface VerificationCardProps {
  userId: string
}

export const VerificationCard = observer(({ userId }: VerificationCardProps): React.ReactElement => {
  const navigation = useNavigation()
  const user = useAuthenticatedUserOrNull()

  const { verificationOptionDismissed, dismissVerification } = useRootStore().domains.userProfile

  const { data, loading } = useLastUserVerificationApplicationQuery({
    variables: { userId },
    fetchPolicy: 'network-only'
  })

  const onCreateApplicationButtonPress = (): void => navigation.dispatch(StackActions.push('ProfileVerification'))

  const onCancelVerificationButtonPress = (): void => dismissVerification()

  if (data?.userById == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }

  const application = data.userById.lastVerificationApplication
  const hasNonConfirmedApplication = application != null && application.resolution !== 'CONFIRMED'

  return userId === user?.id && (hasNonConfirmedApplication || !verificationOptionDismissed)
    ? (
      <VerificationCardView
        application={application}
        onCreateApplicationButtonPress={onCreateApplicationButtonPress}
        onCancelVerificationButtonPress={onCancelVerificationButtonPress}
      />
    ) : <></>
})
