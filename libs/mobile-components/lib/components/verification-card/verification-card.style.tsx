import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { container } from '../../style/view'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

export const Container = styled(View, {
  flex: 1,
  alignItems: 'center',
  padding: 0,
  backgroundColor: color('inactiveLight'),
  marginHorizontal: sp(8),
  borderRadius: 8
})

export const Header = styled(View, {
  ...container('margin'),
  alignItems: 'center'
})

const HeaderText = styled(Text, {
  ...font({ type: 'h2' }),
  textAlign: 'center',
  marginTop: sp(24)
})
export const DefaultHeaderText = styled(HeaderText, { color: color('text') })
export const RejectedHeaderText = styled(HeaderText, { color: color('error') })

export const Body = styled(View, {
  flex: 1,
  marginTop: sp(12)
})

export const BodyText = styled(Text, {
  ...font({ type: 'caption' }),
  textAlign: 'center',
  marginHorizontal: sp(12),
  paddingHorizontal: sp(12)
})

export const BodyButton = styled(View, {
  flex: 1,
  marginVertical: sp(12),
  paddingHorizontal: sp(8)
})

export const Footer = styled(View, {
  marginBottom: sp(12)
})
