import React, { useEffect } from 'react'
import { StackActions, useNavigation } from '@react-navigation/native'
import { useCommunityPreviewQuery } from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { PreviewRowView } from './preview-row.view'
import { toCaseCount } from '../../i18n/utils'

interface CommunityPreviewRowProps {
  communityId: string
  action?: React.ReactElement
  shape?: 'circle' | 'square'
}

export function CommunityPreviewRow ({
  communityId,
  action,
  shape = 'square'
}: CommunityPreviewRowProps): React.ReactElement {
  const navigation = useNavigation()
  const { data, loading, error } = useCommunityPreviewQuery({
    variables: {
      communityId
    }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }
  if (data?.communityById != null) {
    return (
      <PreviewRowView
        firstName={data.communityById.name}
        avatar={data.communityById.avatar}
        specialization={
          `${data.communityById.membersCount ?? 0} ${toCaseCount(
          ['подписчик', 'подписчика', 'подписчиков'], data.communityById.membersCount ?? 0)}`
        }
        handleAuthorPressed={() => navigation.dispatch(
          StackActions.push('Community', { communityId: data.communityById?.id }))}
        shape={shape}
        action={action}
      />
    )
  } else {
    return <> </>
  }
}
