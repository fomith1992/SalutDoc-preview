import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { font } from '../../style/text'
import { container } from '../../style/view'

export const AuthorContainer = styled(TouchableOpacity, {
  ...container('padding'),
  backgroundColor: color('surface'),
  paddingVertical: sp(8),
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarContainer = styled(View, {
  overflow: 'hidden',
  width: sp(48),
  height: sp(48)
})

export const AuthorTitleContainer = styled(View, {
  flex: 1,
  marginLeft: sp(12)
})

export const Author = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  fontSize: 14,
  lineHeight: sp(16)
})

export const Specialization = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('inactive')
})

export const ActionContainer = styled(View, {
  marginLeft: 'auto'
})
