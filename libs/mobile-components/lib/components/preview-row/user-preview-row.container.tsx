import React, { useEffect } from 'react'
import { StackActions, useNavigation } from '@react-navigation/native'
import { useUserPreviewQuery } from '../../gen/graphql'
import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import { PreviewRowView } from './preview-row.view'

interface UserPreviewRowProps {
  userId: string
  action?: React.ReactElement
  shape?: 'circle' | 'square'
}
export function UserPreviewRow ({
  userId,
  action,
  shape = 'square'
}: UserPreviewRowProps): React.ReactElement {
  const navigation = useNavigation()

  const { data, loading, error } = useUserPreviewQuery({
    variables: {
      userId
    }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  if (loading) {
    return <LoadingIndicator visible />
  }
  if (data?.userById != null) {
    return (
      <PreviewRowView
        firstName={data.userById.firstName}
        lastName={data.userById.lastName}
        avatar={data.userById.avatar}
        specialization={data.userById.specialization ?? ''}
        handleAuthorPressed={() => navigation.dispatch(
          StackActions.push('Profile', { userId: data.userById?.id }))}
        shape={shape}
        action={action}
      />
    )
  } else {
    return <></>
  }
}
