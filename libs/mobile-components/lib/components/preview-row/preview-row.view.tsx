import React from 'react'
import { IconComponent } from '../../images/icon-component'
import { Avatar } from '../avatar/avatar.view'
import {
  AuthorContainer,
  AvatarContainer,
  AuthorTitleContainer,
  Author,
  Specialization,
  ActionContainer
} from './preview-row.style'

export interface PreviewRowViewProps {
  firstName: string
  lastName?: string
  avatar: string | null
  avatarPlaceholder?: IconComponent
  specialization: string | null
  handleAuthorPressed: () => void
  action?: React.ReactElement | null
  shape?: 'circle' | 'square'
}
export function PreviewRowView ({
  firstName,
  lastName,
  avatar,
  specialization,
  handleAuthorPressed,
  action,
  shape = 'square'
}: PreviewRowViewProps): React.ReactElement {
  return (
    <AuthorContainer
      onPress={handleAuthorPressed}
    >
      <AvatarContainer>
        <Avatar url={avatar} shape={shape} />
      </AvatarContainer>
      <AuthorTitleContainer>
        <Author>
          {`${firstName} ${lastName ?? ''}`}
        </Author>
        <Specialization>
          {specialization}
        </Specialization>
      </AuthorTitleContainer>
      <ActionContainer>
        {action}
      </ActionContainer>
    </AuthorContainer>
  )
}
