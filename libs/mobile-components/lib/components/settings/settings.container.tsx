import { LoadingIndicator } from '../loading-indicator/loading-indicator.view'
import React from 'react'
import { SettingsView } from './settings.view'
import { Text } from 'react-native'
import { useAuthenticatedUser } from '../user-context/user-context-provider'
import { useProfileVerificationQuery } from '../../gen/graphql'
import { useStackNavigation } from '../../navigation/use-stack-navigation'
import { observer } from 'mobx-react'
import { useRootStore } from '../../stores/root.store'
import { useOidcFirebaseAuthentication } from '../../modules/oidc/oidc-firebase-authentication-context'

const supportUrl = 'https://salutdoc.atlassian.net/servicedesk/customer/portal/1'
const termsOfServiceUrl = 'https://about.salutdoc.com/terms-of-service'
const privacyPolicyUrl = 'https://about.salutdoc.com/privacy/'

export const Settings = observer((): React.ReactElement => {
  const user = useAuthenticatedUser()
  const { logout } = useOidcFirebaseAuthentication()
  const navigator = useStackNavigation()
  const { data, loading, error } = useProfileVerificationQuery({
    variables: { userId: user.id }
  })
  const {
    appMode,
    setAppMode
  } = useRootStore().domains.userProfile
  React.useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  if (loading) {
    return <LoadingIndicator visible />
  }
  if (data?.userById == null) {
    return <Text>Произошла ошибка</Text>
  }
  const currentApplication = data.userById.lastVerificationApplication
  const accountVerified = currentApplication != null && currentApplication.resolution === 'CONFIRMED'
  return (
    <SettingsView
      privacyPolicyHref={privacyPolicyUrl}
      termsOfServiceHref={termsOfServiceUrl}
      supportHref={supportUrl}
      accountVerified={accountVerified}
      onNotificationsSettingsPress={() => navigator.push('NotificationsSettings', {})}
      onVerifyAccountPress={() => navigator.push('ProfileVerification', {})}
      onAdminScreenPress={() => navigator.push('AdminPanel', {})}
      onModerationScreenPress={() => navigator.push('Moderation', {})}
      onCreatePostForServicePress={() => navigator.push('PostCreator', { siteType: 'SERVICE', siteId: user.id })}
      onLogoutPress={() => {
        navigator.reset({
          index: 0,
          routes: [{ name: 'AnonymousMain' }]
        })
        logout().catch(console.log)
      }}
      userRole={data.userById.role}
      appMode={appMode}
      onChangeAppModePress={() => setAppMode(appMode === 'DOCTOR' ? 'PATIENT' : 'DOCTOR')}
    />
  )
})
