import { Text, TouchableOpacity, View } from 'react-native'
import { ExclamationIcon24 } from '../../images/exclamation.icon-24'
import { LockIcon24 } from '../../images/lock.icon-24'
import { LogoutIcon24 } from '../../images/logout.icon-24'
import { NotificationsIcon24 } from '../../images/notifications.icon-24'
import { PlusIcon24 } from '../../images/plus.icon-24'
import { ProfileIcon24 } from '../../images/profile.icon-24'
import { SupportIcon24 } from '../../images/feedback.icon-24'
import { TickInCircleIcon24 } from '../../images/tick-in-circle.icon-24'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'
import { GeneralPractitionerIcon24 } from '../../images/general-practitioner.icon-24'

export const SettingsContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface'),
  paddingTop: sp(8)
})

export const SettingButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  paddingVertical: sp(12),
  paddingHorizontal: sp(16)
})

export const SettingTitle = styled(Text, {
  ...font({ type: 'text1' }),
  marginLeft: sp(16),
  color: color('text')
})

export const NotificationsSettingsSvg = styled(NotificationsIcon24, {
  color: color('accent')
})

export const VerifyProfileSvg = styled(TickInCircleIcon24, {
  color: color('accent')
})

export const LockSvg = styled(LockIcon24, {
  color: color('accent')
})

export const SupportSvg = styled(SupportIcon24, {
  color: color('accent')
})

export const LogoutSvg = styled(LogoutIcon24, {
  color: color('error')
})

export const ProfileSvg = styled(ProfileIcon24, {
  color: color('accent')
})

export const ExclamationSvg = styled(ExclamationIcon24, {
  color: color('accent')
})

export const PlusSvg = styled(PlusIcon24, {
  color: color('accent')
})

export const BottomContainer = styled(View, {
  marginTop: 'auto',
  marginBottom: sp(24),
  paddingHorizontal: sp(16)
})

export const BottomText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  textAlign: 'center'
})

export const BottomLink = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center'
})

export const GeneralPractitionerSvg = styled(GeneralPractitionerIcon24, {
  color: color('accent')
})
