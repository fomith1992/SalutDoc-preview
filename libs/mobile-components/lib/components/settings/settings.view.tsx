import {
  BottomContainer,
  BottomLink,
  BottomText,
  ExclamationSvg,
  GeneralPractitionerSvg,
  LogoutSvg,
  NotificationsSettingsSvg,
  ProfileSvg,
  PlusSvg,
  SettingButton,
  SettingTitle,
  SettingsContainer,
  SupportSvg,
  VerifyProfileSvg
} from './settings.style'
import { ExternalLink } from '../../modules/ui-kit'
import React from 'react'

interface SettingsViewProps {
  privacyPolicyHref: string
  termsOfServiceHref: string
  supportHref: string
  accountVerified: boolean
  userRole: 'USER' | 'DOCTOR' | 'MODERATOR' | null
  onNotificationsSettingsPress: () => void
  onVerifyAccountPress: () => void
  onLogoutPress: () => void
  onAdminScreenPress: () => void
  onModerationScreenPress: () => void

  onChangeAppModePress: () => void
  appMode: 'PATIENT' | 'DOCTOR'
  onCreatePostForServicePress: () => void
}

export function SettingsView (
  {
    privacyPolicyHref,
    termsOfServiceHref,
    supportHref,
    accountVerified,
    userRole,
    onNotificationsSettingsPress,
    onVerifyAccountPress,
    onLogoutPress,
    onAdminScreenPress,
    onModerationScreenPress,

    onChangeAppModePress,
    appMode,
    onCreatePostForServicePress
  }: SettingsViewProps
): React.ReactElement {
  return (
    <SettingsContainer>
      <SettingButton onPress={onNotificationsSettingsPress}>
        <NotificationsSettingsSvg />
        <SettingTitle>Уведомления</SettingTitle>
      </SettingButton>
      {!accountVerified && (
        <SettingButton onPress={onVerifyAccountPress}>
          <VerifyProfileSvg />
          <SettingTitle>Верификация аккаунта</SettingTitle>
        </SettingButton>
      )}
      {userRole === 'DOCTOR'
        ? appMode === 'DOCTOR'
          ? (
            <SettingButton onPress={onChangeAppModePress}>
              <ProfileSvg />
              <SettingTitle>Перейти в режим пациента</SettingTitle>
            </SettingButton>
          ) : (
            <SettingButton onPress={onChangeAppModePress}>
              <GeneralPractitionerSvg />
              <SettingTitle>Перейти в режим врача</SettingTitle>
            </SettingButton>
          )
        : <></>}
      <ExternalLink href={supportHref}>
        {({ onPress }) => (
          <SettingButton onPress={onPress}>
            <SupportSvg />
            <SettingTitle>Обратная связь</SettingTitle>
          </SettingButton>
        )}
      </ExternalLink>
      {userRole === 'MODERATOR'
        ? (
          <>
            <SettingButton onPress={onAdminScreenPress}>
              <ProfileSvg />
              <SettingTitle>Экран администратора</SettingTitle>
            </SettingButton>
            <SettingButton onPress={onModerationScreenPress}>
              <ExclamationSvg />
              <SettingTitle>Модерация</SettingTitle>
            </SettingButton>
            <SettingButton onPress={onCreatePostForServicePress}>
              <PlusSvg />
              <SettingTitle>Создать сервисный пост</SettingTitle>
            </SettingButton>
          </>
        ) : null}
      <SettingButton onPress={onLogoutPress}>
        <LogoutSvg />
        <SettingTitle>Выход</SettingTitle>
      </SettingButton>
      <BottomContainer>
        <BottomText>
          Пользуясь этим приложением, Вы соглашаетесь с{' '}
          <ExternalLink href={privacyPolicyHref}>
            {({ onPress }) => (
              <BottomLink onPress={onPress}>
                Политикой конфиденциальности
              </BottomLink>
            )}
          </ExternalLink>
          {' '}и{' '}
          <ExternalLink href={termsOfServiceHref}>
            {({ onPress }) => (
              <BottomLink onPress={onPress}>
                Пользовательским соглашением
              </BottomLink>
            )}
          </ExternalLink>
        </BottomText>
      </BottomContainer>
    </SettingsContainer>
  )
}

/*
      <SettingButton onPress={onProfileSettingsPress}>
        <LockSvg />
        <SettingTitle>Безопасность</SettingTitle>
      </SettingButton>
 */
