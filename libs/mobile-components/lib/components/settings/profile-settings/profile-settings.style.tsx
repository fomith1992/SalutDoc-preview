import { Text, TouchableOpacity } from 'react-native'
import { styled } from '../../../style/styled'
import { sp } from '../../../style/size'
import { font } from '../../../style/text'
import { color } from '../../../style/color'

export const ProfileSettingButton = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  padding: sp(8)
})

export const CategoryTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('inactive'),
  marginBottom: sp(12),
  marginTop: sp(12),
  paddingLeft: sp(8)
})

export const ChangePasswordTitle = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('accent')
})
