import React from 'react'
import { ProfileSettingsView } from './profile-settings.view'

export function ProfileSettings (): React.ReactElement {
  return (
    <ProfileSettingsView />
  )
}
