import React from 'react'
import { SettingsContainer } from '../settings.style'
import { ProfileSettingButton, ChangePasswordTitle, CategoryTitle } from './profile-settings.style'

export function ProfileSettingsView (): React.ReactElement {
  return (
    <SettingsContainer>
      <CategoryTitle>
        Личные данные
      </CategoryTitle>
      <ProfileSettingButton>
        <ChangePasswordTitle>
          Сменить пароль
        </ChangePasswordTitle>
      </ProfileSettingButton>
    </SettingsContainer>
  )
}
