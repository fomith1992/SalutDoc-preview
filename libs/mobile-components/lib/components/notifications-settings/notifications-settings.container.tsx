import { NotificationsSettingsView } from './notifications-settings.view'
import React from 'react'
import { observer } from 'mobx-react'
import { useRootStore } from '../../stores/root.store'

export const NotificationsSettings = observer((): React.ReactElement => {
  const {
    notifyOnNewFollower,
    notifyOnLikeReceived,
    notifyOnDislikeReceived,
    notifyOnCommentReplyReceived,
    notifyOnCommunityInvitationReceived,
    toggleNotifyOnNewFollower,
    toggleNotifyOnLikeReceived,
    toggleNotifyOnDislikeReceived,
    toggleNotifyOnCommentReplyReceived,
    toggleNotifyOnCommunityInvitationReceived
  } = useRootStore().domains.notificationsSettings
  return (
    <NotificationsSettingsView
      notifyOnNewFollower={notifyOnNewFollower}
      notifyOnLikeReceived={notifyOnLikeReceived}
      notifyOnDislikeReceived={notifyOnDislikeReceived}
      notifyOnCommentReplyReceived={notifyOnCommentReplyReceived}
      notifyOnCommunityInvitationReceived={notifyOnCommunityInvitationReceived}
      toggleNotifyOnNewFollower={toggleNotifyOnNewFollower}
      toggleNotifyOnLikeReceived={toggleNotifyOnLikeReceived}
      toggleNotifyOnDislikeReceived={toggleNotifyOnDislikeReceived}
      toggleNotifyOnCommentReplyReceived={toggleNotifyOnCommentReplyReceived}
      toggleNotifyOnCommunityInvitationReceived={toggleNotifyOnCommunityInvitationReceived}
    />
  )
})
