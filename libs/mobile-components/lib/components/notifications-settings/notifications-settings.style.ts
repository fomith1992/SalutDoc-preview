import { Text, View } from 'react-native'
import { color } from '../../style/color'
import { container } from '../../style/view'
import { font } from '../../style/text'
import { sp } from '../../style/size'
import { styled } from '../../style/styled'

export const Container = styled(View, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface')
})

export const Title = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('subtext'),
  marginTop: sp(24),
  marginBottom: sp(24)
})

export const SettingContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: sp(24)
})

export const SettingText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  flex: 1
})
