import {
  Container,
  SettingContainer,
  SettingText,
  Title
} from './notifications-settings.style'
import React from 'react'
import { ToggleSwitch } from '../../modules/form-kit/toggle-switch'

export interface NotificationsSettingsProps {
  notifyOnNewFollower: boolean
  notifyOnLikeReceived: boolean
  notifyOnDislikeReceived: boolean
  notifyOnCommentReplyReceived: boolean
  notifyOnCommunityInvitationReceived: boolean
  toggleNotifyOnNewFollower: (value: boolean) => void
  toggleNotifyOnLikeReceived: (value: boolean) => void
  toggleNotifyOnDislikeReceived: (value: boolean) => void
  toggleNotifyOnCommentReplyReceived: (value: boolean) => void
  toggleNotifyOnCommunityInvitationReceived: (value: boolean) => void
}

export interface NotificationSetting {
  text: string
  value: boolean
  toggleSwitch: (value: boolean) => void
}

export function NotificationsSettingsView (props: NotificationsSettingsProps): React.ReactElement {
  const settings: NotificationSetting[] = [
    {
      text: 'Новый подписчик',
      value: props.notifyOnNewFollower,
      toggleSwitch: props.toggleNotifyOnNewFollower
    },
    {
      text: 'Лайк',
      value: props.notifyOnLikeReceived,
      toggleSwitch: props.toggleNotifyOnLikeReceived
    },
    {
      text: 'Дизлайк',
      value: props.notifyOnDislikeReceived,
      toggleSwitch: props.toggleNotifyOnDislikeReceived
    },
    {
      text: 'Ответ на комментарий',
      value: props.notifyOnCommentReplyReceived,
      toggleSwitch: props.toggleNotifyOnCommentReplyReceived
    },
    {
      text: 'Приглашение в сообщество',
      value: props.notifyOnCommunityInvitationReceived,
      toggleSwitch: props.toggleNotifyOnCommunityInvitationReceived
    }
  ]
  return (
    <Container>
      <Title>PUSH-УВЕДОМЛЕНИЯ</Title>
      {settings.map(({ text, value, toggleSwitch }, index) => (
        <SettingContainer key={index}>
          <SettingText>{text}</SettingText>
          <ToggleSwitch
            value={value}
            onPress={toggleSwitch}
          />
        </SettingContainer>
      ))}
    </Container>
  )
}
