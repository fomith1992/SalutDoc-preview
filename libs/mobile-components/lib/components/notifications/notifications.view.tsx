import React from 'react'
import { NotificationItemProps, NotificationItem } from './notification-item.view'
import {
  ListContainer,
  EmptyListContainer,
  EmptyListHeader,
  EmptyListText,
  SectionHeader
} from './notifications.style'
import { SectionList } from 'react-native'

export interface NotificationsSectionProps {
  title: string
  data: NotificationItemProps[]
}

export interface NotificationsListProps {
  items: NotificationsSectionProps[]
}

export function NotificationsList ({ items }: NotificationsListProps): React.ReactElement {
  return items.length !== 0
    ? (
      <ListContainer>
        <SectionList
          sections={items}
          keyExtractor={item => item.id}
          renderSectionHeader={({ section: { title } }) => (
            title != null ? <SectionHeader>{title}</SectionHeader> : <></>
          )}
          renderItem={({ item: { id, avatar, onAvatarPress, content, createdAt, actions } }) => (
            <NotificationItem
              id={id}
              avatar={avatar}
              onAvatarPress={onAvatarPress}
              content={content}
              createdAt={createdAt}
              actions={actions}
            />
          )}
        />
      </ListContainer>
    ) : (
      <EmptyListContainer>
        <EmptyListHeader>
          Здесь будут уведомления
        </EmptyListHeader>
        <EmptyListText>
          Заходите сюда для промотра уведомлений о новых подписчиках, лайках и других событиях
        </EmptyListText>
      </EmptyListContainer>
    )
}
