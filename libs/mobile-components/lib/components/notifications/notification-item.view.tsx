import React from 'react'
import { Avatar } from '../avatar/avatar.view'
import {
  NotificationItemRow,
  InfoContainer,
  AvatarContainer,
  TextContainer,
  NotificationContent,
  Timestamp
} from './notifications.style'

export interface NotificationItemProps {
  id: string
  avatar: string | null
  onAvatarPress: () => void
  content: React.ReactNode
  createdAt: string
  actions?: React.ReactNode
}

export function NotificationItem (
  {
    avatar,
    onAvatarPress,
    content,
    createdAt,
    actions
  }: NotificationItemProps
): React.ReactElement {
  return (
    <NotificationItemRow>
      <InfoContainer>
        <AvatarContainer
          onPress={onAvatarPress}
        >
          <Avatar url={avatar} />
        </AvatarContainer>
        <TextContainer>
          <NotificationContent>{content}</NotificationContent>
          <Timestamp>{createdAt}</Timestamp>
          {actions}
        </TextContainer>
      </InfoContainer>
    </NotificationItemRow>
  )
}
