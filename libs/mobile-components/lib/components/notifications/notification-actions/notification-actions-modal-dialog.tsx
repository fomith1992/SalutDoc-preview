import React from 'react'
import { Platform, TouchableWithoutFeedback } from 'react-native'
import {
  Overlay,
  ModalDialogContainer,
  DialogTitle,
  DialogText,
  DialogActions,
  ButtonContainer
} from './notification-actions-modal-dialog.style'
import { Modal, TextButton } from '../../../modules/ui-kit'

export interface NotificationActionsModalDialogProps {
  visible: boolean
  setVisible: (state: boolean) => void
  title: string
  text?: string
  applyButtonText: string
  onApplyButtonPress: () => void
  cancelButtonText?: string
}

export function NotificationActionsModalDialog (
  {
    visible,
    setVisible,
    title,
    text,
    applyButtonText,
    onApplyButtonPress,
    cancelButtonText = 'Назад'
  }: NotificationActionsModalDialogProps
): React.ReactElement {
  return (
    <Modal visible={visible}>
      <TouchableWithoutFeedback onPress={() => setVisible(false)}>
        <Overlay behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
          <ModalDialogContainer>
            <DialogTitle>{title}</DialogTitle>
            <DialogText>{text}</DialogText>
            <DialogActions>
              <ButtonContainer isLeft>
                <TextButton
                  type='link'
                  size='S'
                  onPress={() => setVisible(false)}
                >
                  {cancelButtonText}
                </TextButton>
              </ButtonContainer>
              <ButtonContainer>
                <TextButton
                  type='link'
                  size='S'
                  onPress={() => {
                    onApplyButtonPress()
                    setVisible(false)
                  }}
                >
                  {applyButtonText}
                </TextButton>
              </ButtonContainer>
            </DialogActions>
          </ModalDialogContainer>
        </Overlay>
      </TouchableWithoutFeedback>
    </Modal>
  )
}
