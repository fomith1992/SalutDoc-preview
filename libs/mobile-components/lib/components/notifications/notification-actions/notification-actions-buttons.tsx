import React, { useState } from 'react'
import { TextButton } from '../../../modules/ui-kit'
import { Spring } from './notification-actions.style'
import {
  NotificationActionsModalDialog
} from './notification-actions-modal-dialog'

export interface NotificationActionButtonProps {
  text: string
  onPress: () => void
  withConfirmation?: boolean
}

export interface NotificationActionsButtonsProps {
  primaryButton: NotificationActionButtonProps
  defaultButton: NotificationActionButtonProps
  modalTitle: string
  modalText?: string
  modalApplyButtonText: string
}

export function NotificationActionsButtons (
  {
    primaryButton,
    defaultButton,
    modalTitle,
    modalText,
    modalApplyButtonText
  }: NotificationActionsButtonsProps
): React.ReactElement {
  const {
    text: primaryButtonText,
    onPress: onPrimaryButtonPress,
    withConfirmation: openModalOnPrimaryButtonPress = false
  } = primaryButton
  const {
    text: defaultButtonText,
    onPress: onDefaultButtonPress,
    withConfirmation: openModalOnDefaultButtonPress = false
  } = defaultButton
  const [dialogVisible, setDialogVisible] = useState<boolean>(false)
  return (
    <>
      <TextButton
        size='M'
        type='primary'
        onPress={() => {
          if (openModalOnPrimaryButtonPress) {
            setDialogVisible(true)
          } else {
            onPrimaryButtonPress()
          }
        }}
      >
        {primaryButtonText}
      </TextButton>
      <Spring />
      <TextButton
        size='M'
        type='secondary'
        onPress={() => {
          if (openModalOnDefaultButtonPress) {
            setDialogVisible(true)
          } else {
            onDefaultButtonPress()
          }
        }}
      >
        {defaultButtonText}
      </TextButton>
      <NotificationActionsModalDialog
        visible={dialogVisible}
        setVisible={setDialogVisible}
        title={modalTitle}
        text={modalText}
        applyButtonText={modalApplyButtonText}
        onApplyButtonPress={openModalOnPrimaryButtonPress ? onPrimaryButtonPress : onDefaultButtonPress}
      />
    </>
  )
}
