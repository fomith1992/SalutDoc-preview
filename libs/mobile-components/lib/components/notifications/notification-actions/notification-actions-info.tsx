import React from 'react'
import {
  TickIconSvg,
  ActionsInfoText
} from './notification-actions.style'

export interface NotificationActionsInfoProps {
  text: string
}

export function NotificationActionsInfo (
  {
    text
  }: NotificationActionsInfoProps
): React.ReactElement {
  return (
    <>
      <TickIconSvg />
      <ActionsInfoText>{text}</ActionsInfoText>
    </>
  )
}
