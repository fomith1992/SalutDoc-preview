import { KeyboardAvoidingView, Text, View } from 'react-native'
import { color } from '../../../style/color'
import { container } from '../../../style/view'
import { font } from '../../../style/text'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const Overlay = styled(KeyboardAvoidingView, {
  flex: 1,
  backgroundColor: 'rgba(148, 148, 148, 0.4)',
  justifyContent: 'center',
  paddingHorizontal: sp(32)
})

export const ModalDialogContainer = styled(View, {
  ...container(),
  backgroundColor: color('surface'),
  paddingHorizontal: sp(16),
  paddingVertical: sp(24),
  borderRadius: 8
})

export const DialogTitle = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const DialogText = styled(Text, {
  ...font({ type: 'caption' }),
  marginVertical: sp(16),
  color: color('text')
})

export const DialogActions = styled(View, {
  flexDirection: 'row',
  justifyContent: 'flex-end'
})

export const ButtonContainer = styled(View, (props: { isLeft?: boolean }) => ({
  marginRight: (props.isLeft ?? false) ? sp(24) : 0
}))

export const Spring = styled(View, {
  flex: 1,
  paddingHorizontal: sp(8)
})
