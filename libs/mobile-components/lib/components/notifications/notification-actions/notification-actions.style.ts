import { styled } from '../../../style/styled'
import { View, Text } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { font } from '../../../style/text'
import { TickIcon24 } from '../../../images/tick.icon-24'

export const Spring = styled(View, {
  width: sp(8)
})

export const TickIconSvg = styled(TickIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('subtext')
})

export const ActionsInfoText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext'),
  marginLeft: sp(8)
})
