import { styled } from '../../style/styled'
import { View, Text, TouchableOpacity } from 'react-native'
import { color } from '../../style/color'
import { font } from '../../style/text'
import { sp } from '../../style/size'

export const ListContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface'),
  paddingTop: sp(16)
})

export const EmptyListContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('surface')
})

export const EmptyListHeader = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  textAlign: 'center',
  color: color('text'),
  marginBottom: sp(16)
})

export const EmptyListText = styled(Text, {
  ...font({ type: 'caption' }),
  textAlign: 'center',
  color: color('text')
})

export const SectionHeader = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('subtext'),
  marginTop: sp(8),
  marginBottom: sp(24),
  paddingHorizontal: sp(16)
})

export const NotificationItemRow = styled(View, {
  flex: 1,
  marginBottom: sp(16),
  paddingHorizontal: sp(16),
  backgroundColor: color('surface')
})

export const InfoContainer = styled(View, {
  flex: 1,
  flexDirection: 'row'
})

export const AvatarContainer = styled(TouchableOpacity, {
  width: sp(48),
  height: sp(48),
  overflow: 'hidden',
  borderRadius: sp(24),
  borderColor: color('inactiveLight'),
  borderWidth: 1
})

export const TextContainer = styled(View, {
  flex: 1,
  flexDirection: 'column',
  marginLeft: sp(16)
})

export const NotificationContent = styled(View, {
  flex: 1,
  flexDirection: 'row',
  flexWrap: 'wrap'
})

export const NotificationText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const ClickableText = styled(Text, {
  ...font({ type: 'h4', weight: 'strong' }),
  color: color('accent')
})

export const Timestamp = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext')
})

export const NotificationActionsContainer = styled(View, {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: sp(8)
})
