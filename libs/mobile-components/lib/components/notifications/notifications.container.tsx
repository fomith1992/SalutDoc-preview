import React from 'react'
import { NotificationsList } from './notifications.view'
import {
  ClickableText,
  NotificationText,
  NotificationActionsContainer
} from './notifications.style'
import { useNavigation, StackActions } from '@react-navigation/native'
import { Text } from 'react-native'
import { NotificationActionsButtons } from './notification-actions/notification-actions-buttons'
import { NotificationActionsInfo } from './notification-actions/notification-actions-info'

type Notification = {
  id: string
  user: {
    id: string
    firstName: string
    lastName: string
    avatar: string | null
  }
  createdAt: string
  viewed: boolean
  content: {
    text: string
    entity: {
      id: string
      text: string
    }
  }
}

export function Notifications (): React.ReactElement {
  const navigation = useNavigation()
  const notifications: Notification[] = [
    {
      id: '1',
      user: {
        id: '5fe89d441e1ac90011be300e',
        firstName: 'Алексей',
        lastName: 'Гуськов',
        avatar: 'https://test.k8s.linksdoc.com/assets/images/5f970c9d8dc4c60011c490d4'
      },
      createdAt: '2 минуты назад',
      viewed: false,
      content: {
        entity: {
          id: '5edc0ef2907af1d05728a117',
          text: 'Как общаться со сложными пациентами'
        },
        text: 'поставил лайк Вашему посту'
      }
    },
    {
      id: '2',
      user: {
        id: '5fe89d441e1ac90011be300e',
        firstName: 'Петр',
        lastName: 'Федоров',
        avatar: 'https://test.k8s.linksdoc.com/assets/images/5f970c9d8dc4c60011c490d4'
      },
      createdAt: '1 час назад',
      viewed: false,
      content: {
        entity: {
          id: '5edc0ef2907af1d05728a117',
          text: 'от 14 декабря 2020'
        },
        text: 'поставил лайк Вашему посту'
      }
    },
    {
      id: '3',
      user: {
        id: '5fe89d441e1ac90011be300e',
        firstName: 'Виктория',
        lastName: 'Гутникова',
        avatar: 'https://test.k8s.linksdoc.com/assets/images/5f970c9d8dc4c60011c490d4'
      },
      createdAt: '14 декабря в 13:49',
      viewed: true,
      content: {
        entity: {
          id: '5edc0ef2907af1d05728a117',
          text: 'Коллега, вынужден с вами не согласиться и...'
        },
        text: 'поставила лайк Вашему комментарию'
      }
    },
    {
      id: '4',
      user: {
        id: '5fe89d441e1ac90011be300e',
        firstName: 'Евгений',
        lastName: 'Поляков',
        avatar: 'https://test.k8s.linksdoc.com/assets/images/5f970c9d8dc4c60011c490d4'
      },
      createdAt: '14 декабря в 13:45',
      viewed: true,
      content: {
        entity: {
          id: '5edc0ef2907af1d05728a117',
          text: 'Как общаться со сложными пациентами'
        },
        text: 'поставил дизлайк Вашему посту'
      }
    }
  ]
  const data = [
    {
      title: 'Новые',
      data: notifications.filter(n => !n.viewed)
    },
    {
      title: 'Просмотренные',
      data: notifications.filter(n => n.viewed)
    }
  ]
  return (
    <NotificationsList
      items={data.map(section => ({
        title: section.title,
        data: section.data.map(item => ({
          id: item.id,
          avatar: item.user.avatar,
          onAvatarPress: () => navigation.dispatch(StackActions.push('Profile', { userId: item.user.id })),
          content: (
            <Text>
              <ClickableText
                onPress={() => navigation.dispatch(StackActions.push('Profile', { userId: item.user.id }))}
              >
                {`${item.user.firstName} ${item.user.lastName}` + ' '}
              </ClickableText>
              <NotificationText>
                {item.content.text + ' '}
              </NotificationText>
              <ClickableText
                onPress={() => navigation.dispatch(StackActions.push('PostComments', { postId: item.content.entity.id }))}
              >
                {item.content.entity.text}
              </ClickableText>
            </Text>
          ),
          createdAt: item.createdAt,
          actions: (
            <NotificationActionsContainer>
              {parseInt(item.id) % 2 === 0
                ? (
                  <NotificationActionsButtons
                    primaryButton={{
                      text: 'Вступить',
                      onPress: (): void => {
                        console.log('Вступить')
                      },
                      withConfirmation: false
                    }}
                    defaultButton={{
                      text: 'Отклонить',
                      onPress: (): void => {
                        console.log('Отклонить')
                      },
                      withConfirmation: true
                    }}
                    modalTitle='Отклонить'
                    modalText='Если Вы отклоните приглашение в закрытое сообщество, то чтобы попасть туда в будущем, Вам нужно будет снова получить приглашение'
                    modalApplyButtonText='Отклонить'
                  />
                ) : (
                  <NotificationActionsInfo text='Заявка принята' />
                )}
            </NotificationActionsContainer>
          )
        }))
      }))}
    />
  )
}
