import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { Header } from '../../modules/ui-kit/header'
import { RegisterDoctorNumberScreen } from '../../screens/register-doctor-number.screen'
import { RegisterDoctorPersonalScreen } from '../../screens/register-doctor-personal.screen'
import { RegisterDoctorVerificationScreen } from '../../screens/register-doctor-verification.screen'

export type RegisterDoctorStackParamList = {
  Register_Doctor: {}
  Register_DoctorVerification: {}
  Register_DoctorPersonal: {}
}

const Stack = createStackNavigator<RegisterDoctorStackParamList>()

export function RegisterDoctorStack (): React.ReactElement {
  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} forceShowBack />
      }}
    >
      <Stack.Screen
        name='Register_Doctor'
        options={{
          title: 'Введите номер телефона'
        }}
      >
        {() => <RegisterDoctorNumberScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Register_DoctorVerification'
        options={{
          title: 'Введите код из смс'
        }}
      >
        {() => <RegisterDoctorVerificationScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Register_DoctorPersonal'
        options={{
          title: 'Заполните профиль'
        }}
      >
        {() => <RegisterDoctorPersonalScreen />}
      </Stack.Screen>
    </Stack.Navigator>
  )
}
