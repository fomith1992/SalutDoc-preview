import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { Header } from '../../modules/ui-kit/header'
import { LoginScreen } from '../../screens/login.screen'
import { LoginVerification } from '../../components/login/login-verification/login-verification.container'

export type LoginStackParamList = {
  Login: {}
  LoginVerification: {}
}

const Stack = createStackNavigator<LoginStackParamList>()

export function LoginStack (): React.ReactElement {
  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} forceShowBack />
      }}
    >
      <Stack.Screen
        name='Login'
        options={{
          title: 'Введите номер телефона'
        }}
      >
        {() => <LoginScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='LoginVerification'
        options={{
          title: 'Введите код из смс'
        }}
      >
        {() => <LoginVerification />}
      </Stack.Screen>
    </Stack.Navigator>
  )
}
