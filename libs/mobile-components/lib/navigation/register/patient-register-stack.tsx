import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { Header } from '../../modules/ui-kit/header'
import { RegisterPatientNumberScreen } from '../../screens/register-patient-number.screen'
import { RegisterPatientPersonalScreen } from '../../screens/register-patient-personal.screen'
import { RegisterPatientVerificationScreen } from '../../screens/register-patient-verification.screen'

export type RegisterPatientStackParamList = {
  Register_Patient: {}
  Register_PatientVerification: {}
  Register_PatientPersonal: {}
}

const Stack = createStackNavigator<RegisterPatientStackParamList>()

export function RegisterPatientStack (): React.ReactElement {
  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} forceShowBack />
      }}
    >
      <Stack.Screen
        name='Register_Patient'
        options={{
          title: 'Введите номер телефона'
        }}
      >
        {() => <RegisterPatientNumberScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Register_PatientVerification'
        options={{
          title: 'Введите код из смс'
        }}
      >
        {() => <RegisterPatientVerificationScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Register_PatientPersonal'
        options={{
          title: 'Создание медкарты'
        }}
      >
        {() => <RegisterPatientPersonalScreen />}
      </Stack.Screen>
    </Stack.Navigator>
  )
}
