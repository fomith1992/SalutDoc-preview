import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { SearchedItemType } from '../gen/graphql'
import { DoctorConsultationChatScreen, PatientConsultationChatScreen } from '../modules/messenger/screens'
import { FreeConsultationChatScreen } from '../modules/messenger/screens/free-consultation-chat.screen'
import { Header } from '../modules/ui-kit/header'
import { AddAdminsScreen } from '../screens/add-admins.screen'
import { AddMaterialScreen } from '../screens/add-material.screen'
import { AdminPanelScreen } from '../screens/admin-panel.screen'
import { ArchivePanelScreen } from '../screens/archive-panel.screen'
import { CancelRequestScreen } from '../screens/cancel-request.screen'
import { ChatScreen } from '../screens/chat.screen'
import { ChatsScreen } from '../screens/chats.screen'
import { CommunitiesScreen } from '../screens/communities.screen'
import { CommunityInvitationsScreen } from '../screens/community-invitations.screen'
import { CommunityMembersScreen } from '../screens/community-members.screen'
import { CommunityPortfolioScreen } from '../screens/community-portfolio.screen'
import { CommunityRequestsScreen } from '../screens/community-requests.screen'
import { CommunityScreen } from '../screens/community.screen'
import { ConsultationPreviewScreen } from '../screens/consultation-preview.screen'
import { ContactInfoScreen } from '../screens/contact-info.screen'
import { CreateCommunityScreen } from '../screens/create-community.screen'
import { DoctorSheduleScreen } from '../screens/doctor-shedule.screen'
import { EditbasicInfoDoctorScreen } from '../screens/edit-basic-info-doctor.screen'
import { EditbasicInfoPatientScreen } from '../screens/edit-basic-info-patient.screen'
import { EditCommunityScreen } from '../screens/edit-community.screen'
import { EditInternshipsInfoScreen } from '../screens/edit-interships-info.screen'
import { EditProfileScreen } from '../screens/edit-profile.screen'
import { EditTrainingsInfoScreen } from '../screens/edit-trainings-info.screen'
import { EditUniversitiesInfoScreen } from '../screens/edit-universities-info.screen'
import { FeedScreen } from '../screens/feed.screen'
import { FolloweesScreen } from '../screens/followees.screen'
import { FollowersScreen } from '../screens/followers.screen'
import { InviteUsersScreen } from '../screens/invite-users.screen'
import { MedicalRecordScreen } from '../screens/medical-record.screen'
import { ModerationScreen } from '../screens/moderation.screen'
import { NotificationsSettingsScreen } from '../screens/notifications-settings.screen'
import { NotificationsScreen } from '../screens/notifications.screen'
import { PatientConsultationsScreen } from '../screens/patient-consultations.screen'
import { PostCommentsScreen } from '../screens/post-comments.screen'
import { ProfileVerificationScreen } from '../screens/profile-verification.screen'
import { QuestionFullScreen } from '../screens/question-full.screen'
import { QuestionsScreen } from '../screens/questions.screen'
import { RecommendationsScreen } from '../screens/recommendations.screen'
import { SearchScreen } from '../screens/search.screen'
import { SettingsAnonymousScreen } from '../screens/settings-anonymous.screen'
import { SettingsScreen } from '../screens/settings.screen'
import { UserPortfolioScreen } from '../screens/user-portfolio.screen'
import { UserProfileScreen } from '../screens/user-profile.screen'
import { VerificationCardScreen } from '../screens/verification-card.screen'
import { VerificationPanelScreen } from '../screens/verification-panel.screen'
import { WorkScreen } from '../screens/work.screen'
import { DoctorConsultationsTabNavigator } from './doctor-consultations-tab-navigator'

export type MainStackParamList = {
  Feed: {}
  Search: { type?: SearchedItemType }
  Communities: { userId: string }
  Chats: {}
  Chat: { chatId: string }
  Community: { communityId: string }
  CommunityRequests: { communityId: string }
  CommunityInvitations: { communityId: string }
  EditCommunity: { communityId: string }
  AddAdmins: { communityId: string }
  CreateCommunity: {}
  Profile: { userId: string }
  ContactInfo: { userId: string }
  Followees: { userId: string }
  Followers: { userId: string }
  EditProfile: { userId: string }
  Members: { communityId: string }
  PostComments: { postId: string }
  QuestionFull: { id: string }
  Questions: { userId: string }
  AnswerCreate: { questionId: string }
  Portfolio: { userId: string }
  Work: { workId: string }
  UserPortfolio: { userId: string }
  CommunityPortfolio: { communityId: string }
  Settings: {}
  SettingsAnonymous: {}
  ProfileVerification: {}
  Recommendations: {}
  EditBasicInfoPatient: {}
  EditBasicInfoDoctor: {}
  EditTrainingsInfo: {}
  EditIntershipsInfo: {}
  EditUniversitiesInfo: {}
  AdminPanel: {}
  VerificationPanel: {}
  ArchivePanel: {}
  VerificationCard: { requestId: string }
  CancelRequest: { requestId: string }
  AddMaterial: {}
  Notifications: {}
  PatientConsultations: {}
  DoctorConsultations: {}
  NotificationsSettings: {}
  InviteUsers: { communityId: string }
  PatientConsultationChat: { consultationId: string }
  FreeConsultationChat: { consultationId: string }
  DoctorConsultationChat: { consultationId: string }
  Moderation: {}
  MedicalRecord: {}
  ConsultationPreview: { consultationId: string }
  DoctorShedule: {}
}

type MainStackKeyList = keyof MainStackParamList

const Stack = createStackNavigator<MainStackParamList>()

export function MainStack (
  props: { initialRouteName: MainStackKeyList }
): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const userId = user?.id

  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} />
      }}
      initialRouteName={props.initialRouteName}
    >
      <Stack.Screen
        name='Feed'
        options={{
          title: 'Новости'
        }}
      >
        {() => <FeedScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Work'
        options={{
          title: 'Материал'
        }}
      >
        {props => <WorkScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Search'
        options={{
          title: 'Поиск',
          headerShown: false
        }}
      >
        {() => <SearchScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Chats'
        options={{
          title: 'Все сообщения'
        }}
      >
        {() => <ChatsScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Chat'
      >
        {(props) => <ChatScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Communities'
        options={{
          title: 'Сообщества'
        }}
        initialParams={{ userId: userId }}
      >
        {props => <CommunitiesScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Community'
        options={{
          title: 'Сообщество'
        }}
      >
        {props => <CommunityScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Members'
        options={{
          title: 'Подписчики'
        }}
      >
        {props => <CommunityMembersScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='CommunityRequests'
        options={{
          title: 'Заявки'
        }}
      >
        {props => <CommunityRequestsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='CommunityInvitations'
        options={{
          title: 'Приглашения'
        }}
      >
        {props => <CommunityInvitationsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='EditCommunity'
      >
        {props => <EditCommunityScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='InviteUsers'
        options={{
          title: 'Пригласить'
        }}
      >
        {props => <InviteUsersScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='AddAdmins'
        options={{
          title: 'Добавить администратора'
        }}
      >
        {props => <AddAdminsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='CreateCommunity'
        options={{
          title: 'Создание сообщества'
        }}
      >
        {() => <CreateCommunityScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Profile'
        options={{
          title: 'Профиль'
        }}
        initialParams={{ userId: userId }}
      >
        {props => <UserProfileScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='ContactInfo'
        options={{
          title: 'Контактная информация'
        }}
      >
        {props => <ContactInfoScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Followers'
        options={{
          title: 'Подписчики'
        }}
        initialParams={{ userId: userId }}
      >
        {props => <FollowersScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Followees'
        options={{
          title: 'Подписки'
        }}
        initialParams={{ userId: userId }}
      >
        {props => <FolloweesScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='EditProfile'
        initialParams={{ userId: userId }}
        options={{
          title: 'Изменение профиля'
        }}
      >
        {props => <EditProfileScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='PostComments'
        options={{
          title: 'Новость'
        }}
      >
        {props => <PostCommentsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='UserPortfolio'
        options={{
          title: 'Портфолио'
        }}
      >
        {props => <UserPortfolioScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='CommunityPortfolio'
        options={{
          title: 'Работы сообщества'
        }}
      >
        {props => <CommunityPortfolioScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='SettingsAnonymous'
        options={{
          title: 'Настройки'
        }}
      >
        {() => <SettingsAnonymousScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Settings'
        options={{
          title: 'Настройки'
        }}
      >
        {() => <SettingsScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='NotificationsSettings'
        options={{
          title: 'Настройка уведомлений'
        }}
      >
        {() => <NotificationsSettingsScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='ProfileVerification'
        options={{
          title: 'Верификация'
        }}
      >
        {() => <ProfileVerificationScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Recommendations'
        options={{
          title: 'Рекомендации'
        }}
      >
        {(props) => <RecommendationsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='QuestionFull'
        options={{
          title: 'Вопрос'
        }}
      >
        {props => <QuestionFullScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Questions'
        options={{
          title: 'Вопросы'
        }}
      >
        {props => <QuestionsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='EditBasicInfoPatient'
        options={{
          title: 'Изменение профиля'
        }}
      >
        {() => <EditbasicInfoPatientScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='EditBasicInfoDoctor'
        options={{
          title: 'Общая информация'
        }}
      >
        {() => <EditbasicInfoDoctorScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='EditTrainingsInfo'
        options={{
          title: 'Изменение профиля'
        }}
      >
        {() => <EditTrainingsInfoScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='EditIntershipsInfo'
        options={{
          title: 'Изменение профиля'
        }}
      >
        {() => <EditInternshipsInfoScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='EditUniversitiesInfo'
        options={{
          title: 'Изменение профиля'
        }}
      >
        {() => <EditUniversitiesInfoScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='AdminPanel'
        options={{
          title: 'Экран администратора'
        }}
      >
        {() => <AdminPanelScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='VerificationPanel'
        options={{
          title: 'Верификация'
        }}
      >
        {() => <VerificationPanelScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='ArchivePanel'
        options={{
          title: 'Архив'
        }}
      >
        {() => <ArchivePanelScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='VerificationCard'
        options={{
          title: 'Заявка'
        }}
      >
        {props => <VerificationCardScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='CancelRequest'
        options={{
          title: 'Заявка'
        }}
      >
        {props => <CancelRequestScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='AddMaterial'
        options={{
          title: 'Добавить материал'
        }}
      >
        {() => <AddMaterialScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Notifications'
        options={{
          title: 'Уведомления'
        }}
      >
        {() => <NotificationsScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='PatientConsultations'
        options={{
          title: 'Консультации'
        }}
      >
        {() => <PatientConsultationsScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='DoctorConsultations'
        options={{
          title: 'Консультации'
        }}
      >
        {() => <DoctorConsultationsTabNavigator />}
      </Stack.Screen>
      <Stack.Screen
        name='FreeConsultationChat'
      >
        {props => <FreeConsultationChatScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='DoctorConsultationChat'
      >
        {props => <DoctorConsultationChatScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='PatientConsultationChat'
      >
        {props => <PatientConsultationChatScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='Moderation'
        options={{
          title: 'Модерация'
        }}
      >
        {() => <ModerationScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='ConsultationPreview'
        options={{
          title: 'Консультация'
        }}
      >
        {props => <ConsultationPreviewScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='MedicalRecord'
        options={{
          title: 'Медкарта'
        }}
      >
        {() => <MedicalRecordScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='DoctorShedule'
        options={{
          title: 'Расписание'
        }}
      >
        {() => <DoctorSheduleScreen />}
      </Stack.Screen>
    </Stack.Navigator>
  )
}
