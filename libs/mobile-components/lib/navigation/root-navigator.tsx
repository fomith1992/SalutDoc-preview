import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { Platform } from 'react-native'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { Header } from '../modules/ui-kit/header'
import { AlbumConstructorScreen } from '../screens/album-constructor.screen'
import { AnswerCreateScreen } from '../screens/answer-create.screen'
import { ArticleConstructorScreen } from '../screens/article-constructor.screen'
import { ArticleEditPublicationCommunityScreen } from '../screens/article-edit-publication-community.screen'
import { ArticleEditPublicationScreen } from '../screens/article-edit-publication.screen'
import { ArticleEditorScreen } from '../screens/article-editor.screen'
import { ArticlePublicationCommunityScreen } from '../screens/article-publication-community.screen'
import { BannerFreeConsultationScreen } from '../screens/banner-free-consultation.screen'
import { ConsultationPreviewScheduledScreen } from '../screens/consultation-preview-scheduled.screen'
import { ConsultationEditSheduleScreen } from '../screens/consultation-edit-schedule.screen'
import { MaterialConstructorScreen } from '../screens/material-constructor.screen'
import { MaterialPublicationScreen } from '../screens/material-publication.screen'
import { PostCreatorScreen } from '../screens/post-creator.screen'
import { PostEditorScreen } from '../screens/post-editor.screen'
import { QuestionCreatorScreen } from '../screens/question-creator.screen'
import { QuestionEditorScreen } from '../screens/question-editor.screen'
import { RecordPageScreen } from '../screens/record-page.screen'
import { useRootStore } from '../stores/root.store'
import PatientBottomNavigator from './bottom-navigators/patient-bottom-navigator'
import DoctorBottomNavigator from './bottom-navigators/doctor-bottom-navigator'
import { CreateMedialReportStack } from './create-medical-report-stack'
import { NewConsultationStack } from './new-consultation-stack'
import { observer } from 'mobx-react'
import { WelcomePageScreen } from '../screens/welcome-page.screen'
import { PromoBannerPatientScreen } from '../screens/promo-banner-patient.screen'
import { PromoBannerDoctorScreen } from '../screens/promo-banner-doctor.screen'
import { RegisterPatientStack } from './register/patient-register-stack'
import { RegisterDoctorStack } from './register/doctor-register-stack'
import { LoginStack } from './register/login-stack'
import AnonymousBottomNavigator from './bottom-navigators/anonymous-bottom-navigator'
import { CreateAccountCardScreen } from '../screens/create-account-card.screen'

export type RootStackParamList = {
  AnonymousMain: {}
  DoctorMain: {}
  PatientMain: {}
  PostCreator: { siteId: string, siteType: 'USER' | 'COMMUNITY' | 'SERVICE' }
  PostEditor: { postId: string }
  PostComments: { postId: string }
  AlbumConstructor: {}
  MaterialConstructor: {}
  ArticleConstructor: {}
  ArticleEditor: { workId: string }
  ArticleEditPublication: { workId: string }
  QuestionCreator: {}
  QuestionEditor: { questionId: string }
  MaterialPublication: { materialType: 'newArticle' | 'newMaterial' | 'newAlbum' }
  ArticlePublicationSubject: {}
  ArticlePublicationCommunity: { materialType: 'newArticle' | 'newMaterial' | 'newAlbum' }
  ArticleEditPublicationCommunity: { workId: string }
  AnswerCreate: { questionId: string }
  NewConsultation: {}
  Register: {}
  CreateMedicalReport: { consultationId: string }
  BannerFreeConsultation: {}
  ConsultationPreviewScheduled: {}
  ConsultationEditShedule: {}
  RecordPage: { userId: string }
  WelcomePage: {}
  PromoBannerPatient: {}
  PromoBannerDoctor: {}
  Login_Stack: {}
  Register_DoctorStack: {}
  Register_PatientStack: {}
  CreateAccountCard: {}
}

const Stack = createStackNavigator<RootStackParamList>()

export const RootNavigator = observer((): React.ReactElement => {
  const user = useAuthenticatedUserOrNull()

  const {
    appMode
  } = useRootStore().domains.userProfile
  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} />
      }}
      mode={Platform.OS === 'web' ? undefined : 'modal'}
    >
      {user == null ? (
        <Stack.Screen
          name='WelcomePage'
          options={{
            // animation is disabled to prevent re-render of main stack without authenticated user during animation
            animationEnabled: false,
            headerShown: false
          }}
        >
          {() => <WelcomePageScreen />}
        </Stack.Screen>
      ) : (<></>)}
      {user == null ? (
        <>
          <Stack.Screen
            name='AnonymousMain'
            options={{
              headerShown: false
            }}
          >
            {() => <AnonymousBottomNavigator />}
          </Stack.Screen>
          <Stack.Screen
            name='CreateAccountCard'
            options={{
              title: 'Зарегистрируйтесь'
            }}
          >
            {() => <CreateAccountCardScreen />}
          </Stack.Screen>
        </>
      ) : user.role === 'DOCTOR' && appMode === 'DOCTOR' ? (
        <Stack.Screen
          name='DoctorMain'
          options={{
            headerShown: false
          }}
        >
          {() => <DoctorBottomNavigator />}
        </Stack.Screen>
      ) : (
        <Stack.Screen
          name='PatientMain'
          options={{
            headerShown: false
          }}
        >
          {() => <PatientBottomNavigator />}
        </Stack.Screen>
      )}
      <Stack.Screen
        name='PostCreator'
        options={{
          title: 'Новая запись'
        }}
      >
        {props => <PostCreatorScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='QuestionCreator'
        options={{
          title: 'Задать вопрос'
        }}
      >
        {() => <QuestionCreatorScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='PostEditor'
        options={{
          title: 'Редактирование'
        }}
      >
        {props => <PostEditorScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='QuestionEditor'
        options={{
          title: 'Редактирование'
        }}
      >
        {props => <QuestionEditorScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='ArticleEditor'
        options={{
          title: 'Редактирование'
        }}
      >
        {props => <ArticleEditorScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='ArticleEditPublication'
        options={{
          title: ''
        }}
      >
        {props => <ArticleEditPublicationScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='ArticlePublicationCommunity'
        options={{
          title: 'Группы'
        }}
      >
        {props => <ArticlePublicationCommunityScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='ArticleEditPublicationCommunity'
        options={{
          title: 'Группы'
        }}
      >
        {props => <ArticleEditPublicationCommunityScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='AnswerCreate'
        options={{
          title: 'Ответ на вопрос'
        }}
      >
        {props => <AnswerCreateScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='AlbumConstructor'
        options={{
          title: 'Новый материал',
          gestureEnabled: false
        }}
      >
        {() => <AlbumConstructorScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='ArticleConstructor'
        options={{
          title: 'Новый материал',
          gestureEnabled: false
        }}
      >
        {() => <ArticleConstructorScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='MaterialConstructor'
        options={{
          title: 'Новый материал',
          gestureEnabled: false
        }}
      >
        {() => <MaterialConstructorScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='MaterialPublication'
        options={{
          title: ''
        }}
      >
        {props => <MaterialPublicationScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='NewConsultation'
        options={{
          headerShown: false
        }}
      >
        {() => <NewConsultationStack />}
      </Stack.Screen>
      <Stack.Screen
        name='CreateMedicalReport'
        options={{
          headerShown: false
        }}
      >
        {props => <CreateMedialReportStack {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='BannerFreeConsultation'
        options={{
          title: 'Бесплатная консультация'
        }}
      >
        {() => <BannerFreeConsultationScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='RecordPage'
        options={{
          title: 'Выбор времени'
        }}
      >
        {props => <RecordPageScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='ConsultationPreviewScheduled'
        options={{
          title: 'Консультация'
        }}
      >
        {() => <ConsultationPreviewScheduledScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='ConsultationEditShedule'
        options={{
          title: 'Изменить расписание'
        }}
      >
        {() => <ConsultationEditSheduleScreen />}
      </Stack.Screen>

      {/* Register */}
      <Stack.Screen
        name='PromoBannerPatient'
        options={{
          headerShown: false
        }}
      >
        {() => <PromoBannerPatientScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='PromoBannerDoctor'
        options={{
          headerShown: false
        }}
      >
        {() => <PromoBannerDoctorScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='Login_Stack'
        options={{
          headerShown: false
        }}
      >
        {() => <LoginStack />}
      </Stack.Screen>
      <Stack.Screen
        name='Register_DoctorStack'
        options={{
          headerShown: false
        }}
      >
        {() => <RegisterDoctorStack />}
      </Stack.Screen>
      <Stack.Screen
        name='Register_PatientStack'
        options={{
          headerShown: false
        }}
      >
        {() => <RegisterPatientStack />}
      </Stack.Screen>
    </Stack.Navigator>
  )
})
