import { CreateMedialReportStackParamList } from './create-medical-report-stack'
import { MainStackParamList } from './main-stack'
import { NewConsultationStackParamList } from './new-consultation-stack'
import { RegisterDoctorStackParamList } from './register/doctor-register-stack'
import { LoginStackParamList } from './register/login-stack'
import { RegisterPatientStackParamList } from './register/patient-register-stack'
import { RootStackParamList } from './root-navigator'

export type GlobalNavigatorParamList
  = RootStackParamList
  & MainStackParamList
  & NewConsultationStackParamList
  & RegisterDoctorStackParamList
  & RegisterPatientStackParamList
  & LoginStackParamList
  & CreateMedialReportStackParamList
