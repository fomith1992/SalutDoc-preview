import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import React from 'react'
import { useFreeConsultationsCountQuery } from '../gen/graphql'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { DoctorConsultationsFreeScreen } from '../screens/doctor-consultations-free.screen'
import { DoctorConsultationsMyScreen } from '../screens/doctor-consultations-my.screen'
import { useMaterialized } from '../style/styled'
import {
  tabBarStyleSheet,
  Container,
  LabelText,
  FreeConsultationsContainer,
  FreeConsultationsCount
} from './doctor-consultations-tab-navigator.style'

const Tab = createMaterialTopTabNavigator()

export function DoctorConsultationsTabNavigator (): React.ReactElement {
  useDefaultHeader({
    hasShadow: false
  })
  const tabBarStyles = useMaterialized(tabBarStyleSheet)

  const { data, loading } = useFreeConsultationsCountQuery({
    variables: {}
  })

  if (loading) {
    return (
      <></>
    )
  }

  return (
    <Tab.Navigator
      tabBarOptions={{
        ...tabBarStyles
      }}
    >
      <Tab.Screen
        name='DoctorConsultationsFree'
        options={{
          tabBarLabel: ({ focused }) => (
            <Container>
              <LabelText focused={focused}>
                Свободные
              </LabelText>
              {(data?.userMyself?.freeConsultationsCount ?? 0) > 0
                ? (
                  <FreeConsultationsContainer>
                    <FreeConsultationsCount>
                      {data?.userMyself?.freeConsultationsCount}
                    </FreeConsultationsCount>
                  </FreeConsultationsContainer>
                )
                : null}
            </Container>
          )
        }}
      >
        {() => <DoctorConsultationsFreeScreen />}
      </Tab.Screen>
      <Tab.Screen
        name='DoctorConsultationsMy'
        options={{
          tabBarLabel: 'Мои'
        }}
      >
        {() => <DoctorConsultationsMyScreen />}
      </Tab.Screen>
    </Tab.Navigator>
  )
}
