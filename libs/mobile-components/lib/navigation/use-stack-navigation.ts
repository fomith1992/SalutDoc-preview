import { useNavigation } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { GlobalNavigatorParamList } from './global-navigator-param-list'

export function useStackNavigation (): StackNavigationProp<GlobalNavigatorParamList> {
  return useNavigation()
}
