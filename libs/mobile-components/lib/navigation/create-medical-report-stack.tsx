import { createStackNavigator, StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { Header } from '../modules/ui-kit/header'
import { CreateMedicalReportComposeScreen } from '../screens/create-medical-report-compose.screen'
import { CreateMedicalReportPreviewScreen } from '../screens/create-medical-report-preview.screen'
import { RootStackParamList } from './root-navigator'

export type CreateMedialReportStackParamList = {
  CreateMedicalReport_Compose: { consultationId: string }
  CreateMedicalReport_Review: { consultationId: string }
}

const Stack = createStackNavigator<CreateMedialReportStackParamList>()

export function CreateMedialReportStack (
  { route }: StackScreenProps<RootStackParamList, 'CreateMedicalReport'>
): React.ReactElement {
  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} forceShowBack />
      }}
    >
      <Stack.Screen
        name='CreateMedicalReport_Compose'
        options={{
          title: 'Создание медзаключения'
        }}
        initialParams={route.params}
      >
        {props => <CreateMedicalReportComposeScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='CreateMedicalReport_Review'
        options={{
          title: 'Проверьте'
        }}
      >
        {props => <CreateMedicalReportPreviewScreen {...props} />}
      </Stack.Screen>
    </Stack.Navigator>
  )
}
