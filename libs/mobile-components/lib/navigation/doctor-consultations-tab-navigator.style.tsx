import { MaterialTopTabBarOptions } from '@react-navigation/material-top-tabs'
import { Platform, Text, View, ViewStyle } from 'react-native'
import { color } from '../style/color'
import { font } from '../style/text'
import { LazyStyleProp, lift, styled } from '../style/styled'
import { sp } from '../style/size'

export const tabBarStyleSheet = lift<MaterialTopTabBarOptions>({
  activeTintColor: color('text'),
  inactiveTintColor: color('subtext'),
  labelStyle: lift({
    ...font({ type: 'h3' }),
    textTransform: 'none'
  } as const),
  indicatorStyle: lift({
    backgroundColor: color('accent'),
    height: 2
  }),
  style: Platform.select<LazyStyleProp<ViewStyle>>({
    android: lift({
      elevation: 2
    }),
    ios: lift({
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    })
  })
} as const)

export const Container = styled(View, {
  flexDirection: 'row',
  alignItems: 'center'
})

export const FreeConsultationsContainer = styled(View, {
  marginLeft: sp(8),
  height: sp(20),
  paddingHorizontal: sp(4),
  minWidth: sp(20),
  borderRadius: sp(12),
  borderWidth: 1,
  borderColor: color('accent'),
  justifyContent: 'center',
  alignItems: 'center'
})

export const LabelText = styled(Text, (props: {focused: boolean}) => ({
  ...font({ type: 'h4' }),
  color: color(props.focused ? 'subtext' : 'inactive')
}) as const)

export const FreeConsultationsCount = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('accent')
})
