import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import {
  NewConsultationAnamnesisScreen,
  NewConsultationPaymentDetailsScreen,
  NewConsultationPaymentStateScreen,
  NewConsultationSpecializationScreen
} from '../modules/consultations/screens'
import { NewConsultationPaymentTerminalScreen } from '../modules/consultations/screens/new-consultation-payment-terminal.screen'
import { Header } from '../modules/ui-kit/header'

export type NewConsultationStackParamList = {
  NewConsultation_Specialization: {}
  NewConsultation_Anamnesis: { consultationId: string }
  NewConsultation_PaymentDetails: { consultationId: string }
  NewConsultation_PaymentTerminal: { consultationId: string }
  NewConsultation_PaymentState: { consultationId: string }
}

const Stack = createStackNavigator<NewConsultationStackParamList>()

export function NewConsultationStack (): React.ReactElement {
  return (
    <Stack.Navigator
      screenOptions={{
        header: props => <Header headerProps={props} forceShowBack />
      }}
    >
      <Stack.Screen
        name='NewConsultation_Specialization'
        options={{
          title: 'Выбор специальности'
        }}
      >
        {() => <NewConsultationSpecializationScreen />}
      </Stack.Screen>
      <Stack.Screen
        name='NewConsultation_Anamnesis'
        options={{
          title: 'Создание консультации'
        }}
      >
        {(props) => <NewConsultationAnamnesisScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='NewConsultation_PaymentDetails'
        options={{
          title: 'Оплата'
        }}
      >
        {(props) => <NewConsultationPaymentDetailsScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='NewConsultation_PaymentTerminal'
        options={{
          title: 'Оплата'
        }}
      >
        {(props) => <NewConsultationPaymentTerminalScreen {...props} />}
      </Stack.Screen>
      <Stack.Screen
        name='NewConsultation_PaymentState'
        options={{
          headerShown: false,
          gestureEnabled: false
        }}
      >
        {(props) => <NewConsultationPaymentStateScreen {...props} />}
      </Stack.Screen>
    </Stack.Navigator>
  )
}
