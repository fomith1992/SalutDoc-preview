import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React from 'react'
import { ChatWithPlusIcon24 } from '../../images/chat-with-plus.icon-24'
import { FeedIcon24 } from '../../images/feed.icon-24'
import { color } from '../../style/color'
import { MainStack } from '../main-stack'
import { useMaterialized } from '../../style/styled'
import { ProfileIcon24 } from '../../images/profile.icon-24'

export type PatientBottomTabParamLists = {
  PatientFeed: {}
  PatientConsultations: {}
  // MedicalRecord: {}
  Profile: {}
}

const Tab = createBottomTabNavigator<PatientBottomTabParamLists>()

function PatientBottomNavigator (): React.ReactElement {
  const indicatorColor = {
    active: useMaterialized(color('accent')),
    inactive: useMaterialized(color('subtext'))
  }
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {},
        tabStyle: { marginTop: 6 },
        labelStyle: { fontWeight: '600', lineHeight: 12, fontSize: 10, marginTop: 2, marginBottom: 4 },
        activeTintColor: indicatorColor.active,
        inactiveTintColor: indicatorColor.inactive,
        showLabel: true
      }}
      initialRouteName='PatientConsultations'
    >
      <Tab.Screen
        name='PatientFeed'
        options={{
          tabBarLabel: 'Статьи',
          tabBarIcon: (props) => (
            <FeedIcon24
              style={{ color: props.color }}
            />
          )
        }}
      >
        {() => <MainStack initialRouteName='Feed' />}
      </Tab.Screen>
      <Tab.Screen
        name='PatientConsultations'
        options={{
          tabBarLabel: 'Консультации',
          tabBarIcon: (props) => (
            <ChatWithPlusIcon24
              style={{ color: props.color, width: 26, height: 26 }}
            />
          )
        }}
      >
        {() => <MainStack initialRouteName='PatientConsultations' />}
      </Tab.Screen>
      {/* <Tab.Screen
        name='MedicalRecord'
        options={{
          tabBarLabel: 'Медкарта',
          tabBarIcon: (props) => <CardIcon24 style={{ color: props.color }} />
        }}
      >
        {() => <MainStack initialRouteName='MedicalRecord' />}
      </Tab.Screen> */}
      <Tab.Screen
        name='Profile'
        options={{
          tabBarLabel: 'Профиль',
          tabBarIcon: (props) => <ProfileIcon24 style={{ color: props.color }} />
        }}
      >
        {() => <MainStack initialRouteName='Profile' />}
      </Tab.Screen>
    </Tab.Navigator>
  )
}

export default PatientBottomNavigator
