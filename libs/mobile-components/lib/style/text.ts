import { Platform, TextStyle } from 'react-native'
import { SpScale, spUnsafe } from './size'
import { LazyStyle } from './styled'

type FontFamily = 'Montserrat' | 'Open Sans'

type FontType = 'h1' | 'h2' | 'h3' | 'h4' | 'text1' | 'text2' | 'caption'

type FontWeight = 'normal' | 'light' | 'strong'

interface FontProps {
  type: FontType
  weight?: FontWeight
}

const fontFamily = (type: FontType): FontFamily => {
  switch (type) {
    case 'h1':
    case 'h2':
    case 'h3':
    case 'h4':
      return 'Montserrat'
    case 'text1':
    case 'text2':
    case 'caption':
      return 'Open Sans'
  }
}

const fontLineHeight = (type: FontType): SpScale => {
  switch (type) {
    case 'h1':
      return 24
    case 'h2':
    case 'text1':
    case 'text2':
      return 20
    case 'h3':
    case 'h4':
    case 'caption':
      return 16
  }
}

const fontSize = (type: FontType): number => {
  switch (type) {
    case 'h1':
      return 18
    case 'h2':
      return 16
    case 'h3':
      return 14
    case 'text1':
      return 14
    case 'text2':
      return 13
    case 'h4':
    case 'caption':
      return 12
  }
}

const defaultFontWeight = (type: FontType): number => {
  switch (type) {
    case 'h1':
      return 700
    case 'h2':
      return 600
    case 'h3':
    case 'h4':
      return 500
    case 'text1':
    case 'text2':
    case 'caption':
      return 400
  }
}

const exactFontWeight = (type: FontType, relativeWeight: FontWeight): number => {
  const weight = defaultFontWeight(type)
  if (relativeWeight === 'normal') {
    return weight
  }
  switch (type) {
    case 'text1':
      switch (relativeWeight) {
        case 'light':
          return weight - 100
        case 'strong':
          return weight + 200
      }
      break // eslint ?
    case 'text2':
      switch (relativeWeight) {
        case 'light':
          return weight - 100
        case 'strong':
          return weight + 300
      }
      break
    case 'h2':
      switch (relativeWeight) {
        case 'light':
          return weight - 200
        case 'strong':
          return weight + 100
      }
      break
    case 'h4':
    case 'caption':
      switch (relativeWeight) {
        case 'light':
          return weight - 100
        case 'strong':
          return weight + 200
      }
      break
    case 'h1':
    case 'h3':
      switch (relativeWeight) {
        case 'light':
          return weight - 100
        case 'strong':
          return weight + 100
      }
      break
  }
}

const fontWeightToFileNameFontWeight = (weight: number): string => {
  switch (weight) {
    case 100:
      return 'Thin'
    case 200:
      return 'ExtraLight'
    case 300:
      return 'Light'
    case 400:
    default:
      return 'Normal'
    case 500:
      return 'Medium'
    case 600:
      return 'SemiBold'
    case 700:
      return 'Bold'
    case 800:
      return 'ExtraBold'
    case 900:
      return 'Black'
  }
}

const fontWeightTextStyleFontWeight = (weight: number): TextStyle['fontWeight'] => {
  switch (weight) {
    case 100:
      return '100'
    case 200:
      return '200'
    case 300:
      return '300'
    case 400:
    default:
      return '400'
    case 500:
      return '500'
    case 600:
      return '600'
    case 700:
      return '700'
    case 800:
      return '800'
    case 900:
      return '900'
  }
}

export function font (
  {
    type,
    weight = 'normal'
  }: FontProps
): LazyStyle<TextStyle> {
  const fontFamilyName = fontFamily(type)
  const fontWeight = exactFontWeight(type, weight)
  return Platform.OS === 'android'
    ? {
      fontFamily: `${fontFamilyName.replace(' ', '')}-${fontWeightToFileNameFontWeight(fontWeight)}`,
      fontSize: spUnsafe(fontSize(type)),
      lineHeight: fontLineHeight(type)
    } : {
      fontFamily: fontFamilyName,
      fontWeight: fontWeightTextStyleFontWeight(fontWeight),
      fontSize: spUnsafe(fontSize(type)),
      lineHeight: fontLineHeight(type)
    }
}
