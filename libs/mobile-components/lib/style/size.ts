import { LazyStyleProp } from './styled'

export type SpScale
  = -12 | -8 | -4
  | 4 | 8 | 12 | 16 | 20 | 24 | 28 | 32
  | 36 | 40 | 44 | 48 | 52 | 56 | 60 | 64
  | 68 | 72 | 76 | 80 | 84 | 88 | 92 | 96
  | 100 | 104 | 108 | 112 | 116 | 120 | 124 | 128
  | 132 | 136 | 140 | 144 | 148 | 152 | 156 | 160
  | 164 | 168 | 172 | 176 | 180 | 184 | 188 | 192

export function sp (units: SpScale): LazyStyleProp<number> {
  return spUnsafe(units)
}

export function spUnsafe (units: number): LazyStyleProp<number> {
  return theme => theme.size.scale * units
}
