import { createContext, useContext } from 'react'

export type Theme = typeof defaultTheme

export const defaultTheme = {
  colors: {
    accent: '#006EFD',
    accentDark: '#0056C5',

    secondary: '#2A557B',
    secondaryLight: '#EBF4FF',

    text: '#2D2D2D',
    subtext: '#818C99',

    inactive: '#C4C4C4',
    inactiveLight: '#E3E3E3',

    background: '#F6F6F6',
    backgroundDark: '#2D2D2D',

    surface: '#FFFFFF',

    error: '#FF3347',

    shadow: '#000000'
  },

  layout: {
    margin: 16,
    gutter: 8
  },

  size: {
    scale: 1 // assuming baseline=16
  }
}

const context = createContext(defaultTheme)

export const ThemeProvider = context.Provider
export const useTheme = (): Theme => useContext(context)
