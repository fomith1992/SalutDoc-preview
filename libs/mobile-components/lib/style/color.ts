import { LazyStyleProp } from './styled'
import { Theme } from './theme'

export type ColorType = keyof Theme['colors']

export function color (name: ColorType): LazyStyleProp<string> {
  return ({ colors }) => colors[name]
}
