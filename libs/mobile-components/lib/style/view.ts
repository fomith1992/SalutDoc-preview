import { ViewStyle } from 'react-native'
import { LazyStyle } from './styled'

export function container (type: 'margin' | 'padding' = 'margin'): LazyStyle<ViewStyle> {
  switch (type) {
    case 'margin':
      return {
        marginHorizontal: ({ layout }) => layout.margin
      }
    case 'padding':
      return {
        paddingHorizontal: ({ layout }) => layout.margin
      }
  }
}
