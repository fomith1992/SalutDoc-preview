import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { QuestionCreator } from '../components/question/question-creator/question-creator.container'

export const QuestionCreatorScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <QuestionCreator />
    </ScreenContentView>
  )
}
