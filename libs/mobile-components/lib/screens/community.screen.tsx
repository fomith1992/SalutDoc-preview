import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { useCommunityViewed } from '../analytics/community'
import { CommunityProfile } from '../components/community-profile/community-profile.container'
import { CreateCommunityPostButton } from '../components/create-post-button'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { RequestModal } from '../components/request-modal/request-modal.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { WorksSliderCommunity } from '../components/works-slider/works-slider-community.container'
import { useBooleanFlag } from '../hooks/use-boolean-flag'
import { CommunityFeed } from '../modules/feed'
import { ContextMenu, ContextMenuButton } from '../modules/ui-kit'
import { MainStackParamList } from '../navigation/main-stack'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'

export const CommunityScreen = (
  { route }: StackScreenProps<MainStackParamList, 'Community'>
): React.ReactElement => {
  const { communityId } = route.params
  const user = useAuthenticatedUserOrNull()
  useCommunityViewed(communityId)
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)
  useDefaultHeader({
    actions: [
      {
        icon: 'more',
        onPress: showBottomMenu
      }
    ]
  })
  const menuButtons: ContextMenuButton[] = [
    {
      type: 'black',
      title: 'Пожаловаться',
      onPress: () => {
        hideBottomMenu()
        showReportModal()
      }
    },
    {
      type: 'red',
      title: 'Отмена',
      onPress: hideBottomMenu
    }
  ]
  return (
    <ScreenContentView>
      <CommunityFeed
        communityId={communityId}
        header={
          <>
            <CommunityProfile communityId={communityId} />
            {user == null ? <></> : <WorksSliderCommunity communityId={communityId} />}
            <CreateCommunityPostButton communityId={communityId} />
          </>
        }
      />

      <ContextMenu
        visible={bottomMenuVisible}
        onClose={hideBottomMenu}
        buttons={menuButtons}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='community'
        contentId={communityId}
      />
    </ScreenContentView>
  )
}
