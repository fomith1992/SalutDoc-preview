import React from 'react'
import {
  VerificationPanelList
} from '../components/admin-panel/verificattion-panel-list/verificattion-panel-list.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const VerificationPanelScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <VerificationPanelList type='verification' />
    </ScreenContentView>
  )
}
