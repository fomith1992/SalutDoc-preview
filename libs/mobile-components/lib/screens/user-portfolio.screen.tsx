import { StackActions } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { UserPortfolio } from '../components/portfolio/portfolio/user-portfolio.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useUserPortfolioAuthQuery } from '../gen/graphql'
import { MainStackParamList } from '../navigation/main-stack'

export const UserPortfolioScreen = (
  { route, navigation }: StackScreenProps<MainStackParamList, 'UserPortfolio'>
): React.ReactElement => {
  const { userId } = route.params
  const user = useAuthenticatedUserOrNull()

  const { data } = useUserPortfolioAuthQuery({
    variables: { userId }
  })

  const dropDownItems = [
    {
      title: 'Работы',
      onPress: () => navigation.dispatch(StackActions.replace('UserPortfolio', { userId })),
      active: true
    },
    {
      title: 'Вопросы',
      onPress: () => navigation.dispatch(StackActions.replace('Questions', { userId })),
      active: false
    }
  ]

  useDefaultHeader(user?.id === userId ? {
    title: 'Мои работы',
    dropDownItems,
    actions: data?.userById?.canCreateArticle === true ? [{
      icon: 'plus-drop',
      onPress: () => navigation.navigate('AddMaterial', {})
    }] : []
  } : {
    title: 'Работы пользователя',
    dropDownItems
  })

  return (
    <ScreenContentView>
      <UserPortfolio userId={userId} />
    </ScreenContentView>
  )
}
