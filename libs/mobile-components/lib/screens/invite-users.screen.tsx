import { DividerView } from '../components/divider/divider.view'
import { InviteUsers } from '../components/invite-users/invite-users.container'
import { MainStackParamList } from '../navigation/main-stack'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'

export function InviteUsersScreen (
  { route }: StackScreenProps<MainStackParamList, 'InviteUsers'>
): React.ReactElement {
  const { communityId } = route.params
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск' /> */}
      <DividerView />
      <InviteUsers communityId={communityId} />
    </ScreenContentView>
  )
}
