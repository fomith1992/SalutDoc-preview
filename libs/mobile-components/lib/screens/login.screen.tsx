import React from 'react'
import { LoginNumber } from '../components/login/login-number/login-number.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function LoginScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <LoginNumber />
    </ScreenContentView>
  )
}
