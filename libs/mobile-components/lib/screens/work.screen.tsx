import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { WorkComments } from '../modules/comments/work/work-comments.container'
import { MainStackParamList } from '../navigation/main-stack'
import { useWorkViewed } from '../analytics/work'

export const WorkScreen = (
  { route }: StackScreenProps<MainStackParamList, 'Work'>
): React.ReactElement => {
  const { workId } = route.params
  useWorkViewed(workId)
  return (
    <ScreenContentView>
      <WorkComments workId={workId} />
    </ScreenContentView>
  )
}
