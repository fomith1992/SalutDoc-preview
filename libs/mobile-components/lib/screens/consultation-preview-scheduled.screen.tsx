import React from 'react'
import { ScheduleConsultationPreview } from '../components/doctor-shedule/consultation-preview/consultation-preview.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const ConsultationPreviewScheduledScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <ScheduleConsultationPreview />
    </ScreenContentView>
  )
}
