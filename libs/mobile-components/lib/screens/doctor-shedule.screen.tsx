import React from 'react'
import { DoctorShedule } from '../components/doctor-shedule/doctor-shedule.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const DoctorSheduleScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <DoctorShedule />
    </ScreenContentView>
  )
}
