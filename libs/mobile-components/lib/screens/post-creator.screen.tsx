import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { PostCreator } from '../modules/posts'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '../navigation/root-navigator'

export const PostCreatorScreen = (
  { route }: StackScreenProps<RootStackParamList, 'PostCreator'>
): React.ReactElement => {
  const { siteId, siteType } = route.params
  return (
    <ScreenContentView>
      <PostCreator siteId={siteId} siteType={siteType} />
    </ScreenContentView>
  )
}
