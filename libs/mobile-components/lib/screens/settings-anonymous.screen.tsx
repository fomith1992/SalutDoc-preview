import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { SettingsAnonymous } from '../components/settings-anonynous/settings-anonynous.container'

export function SettingsAnonymousScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <SettingsAnonymous />
    </ScreenContentView>
  )
}
