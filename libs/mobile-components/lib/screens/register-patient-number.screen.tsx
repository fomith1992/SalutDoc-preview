import React from 'react'
import { RegisterPatientNumber } from '../components/register/register-patient/register-patient-number/register-patient-number.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useDefaultHeader } from '../modules/ui-kit/header'

export function RegisterPatientNumberScreen (): React.ReactElement {
  useDefaultHeader({
    actions: [{
      icon: 'help',
      onPress: () => {}
    }]
  })

  return (
    <ScreenContentView>
      <RegisterPatientNumber />
    </ScreenContentView>
  )
}
