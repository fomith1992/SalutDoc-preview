import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '../navigation/root-navigator'
import { ArticleEditor } from '../components/add-material/article-constructor/edit-article-constructor.container'

export const ArticleEditorScreen = (
  { route }: StackScreenProps<RootStackParamList, 'ArticleEditor'>
): React.ReactElement => {
  const { workId } = route.params
  return (
    <ScreenContentView>
      <ArticleEditor workId={workId} />
    </ScreenContentView>
  )
}
