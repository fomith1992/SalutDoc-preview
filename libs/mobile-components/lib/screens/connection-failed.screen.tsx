import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { ProgressState } from '../modules/ui-kit/progress-state'

export const ConnectionFailedScreen = (): React.ReactElement => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ProgressState
        status='FAILURE'
        title='Ошибка загрузки'
        description={`Пожалуйста, проверьте соединение ${'\n'} с интернетом`}
      />
    </SafeAreaView>
  )
}
