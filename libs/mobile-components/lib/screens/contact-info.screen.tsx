import React from 'react'
import { ContactInfo } from '../components/contact-info/contact-info.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'
import { StackScreenProps } from '@react-navigation/stack'

export function ContactInfoScreen (
  { route }: StackScreenProps<MainStackParamList, 'ContactInfo'>
): React.ReactElement {
  return (
    <ScreenContentView>
      <ContactInfo userId={route.params.userId} />
    </ScreenContentView>
  )
}
