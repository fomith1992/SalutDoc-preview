import React from 'react'
import { DoctorConsultationsMy } from '../modules/consultations/doctor-consultations/doctor-consultations-my.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function DoctorConsultationsMyScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <DoctorConsultationsMy />
    </ScreenContentView>
  )
}
