import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MedicalReportCreate } from '../modules/consultations/medical-report-create/medical-report-create.container'
import { GlobalNavigatorParamList } from '../navigation/global-navigator-param-list'

export const CreateMedicalReportComposeScreen = (
  { route }: StackScreenProps<GlobalNavigatorParamList, 'CreateMedicalReport_Compose'>
): React.ReactElement => {
  const { consultationId } = route.params
  return (
    <ScreenContentView>
      <MedicalReportCreate consultationId={consultationId} />
    </ScreenContentView>
  )
}
