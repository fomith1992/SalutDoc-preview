import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { usePostViewed } from '../analytics/post'
import { PostComments } from '../modules/comments/post/post-comments.container'
import { MainStackParamList } from '../navigation/main-stack'

export function PostCommentsScreen (
  { route }: StackScreenProps<MainStackParamList, 'PostComments'>
): React.ReactElement {
  const { postId } = route.params

  usePostViewed(postId)

  return (
    <PostComments postId={postId} />
  )
}
