import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { RecordPage } from '../components/record-page/record-page.container'
import { RootStackParamList } from '../navigation/root-navigator'

export function RecordPageScreen (
  { route }: StackScreenProps<RootStackParamList, 'RecordPage'>
): React.ReactElement {
  return (
    <ScreenContentView>
      <RecordPage userId={route.params.userId} />
    </ScreenContentView>
  )
}
