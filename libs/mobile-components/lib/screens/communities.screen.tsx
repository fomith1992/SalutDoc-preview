import { StackActions } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { CommunitiesList, Community } from '../components/communities/communities.container'
import { FollowCommunityButton } from '../components/follow-community-button/follow-community-button.container'
import { FollowCommunityButtonSmall } from '../components/follow-community-button/follow-community-button.small.view'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useCreateCommunityAuthQuery } from '../gen/graphql'
import { MainStackParamList } from '../navigation/main-stack'

export const CommunitiesScreen = (
  { route, navigation }: StackScreenProps<MainStackParamList, 'Communities'>
): React.ReactElement => {
  const user = useAuthenticatedUserOrNull()
  const { userId } = route.params

  const { data } = useCreateCommunityAuthQuery()

  const showCreateCommunityButton = user?.id === userId &&
    data != null && (data.canCreatePrivateCommunity || data.canCreatePublicCommunity)

  useDefaultHeader({
    actions: showCreateCommunityButton ? [{
      icon: 'plus',
      onPress: () => navigation.push('CreateCommunity', {})
    }] : []
  })

  const handleCommunityPress = (community: Community): void => {
    navigation.dispatch(StackActions.push('Community', { communityId: community.id }))
  }

  const renderAction = (communityId: string): React.ReactElement => (
    <FollowCommunityButton
      communityId={communityId}
      view={FollowCommunityButtonSmall}
    />
  )

  return (
    <ScreenContentView>
      <CommunitiesList
        userId={userId}
        onCommunityPress={handleCommunityPress}
        renderAction={renderAction}
      />
    </ScreenContentView>
  )
}
