import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MedicalRecord } from '../components/medical-record/medical-record.container'

export const MedicalRecordScreen = (
/*   {
    navigation
  }: StackScreenProps<MainStackParamList, 'MedicalRecord'> */
): React.ReactElement => {
  return (
    <ScreenContentView>
      <MedicalRecord />
    </ScreenContentView>
  )
}
