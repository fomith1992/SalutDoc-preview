import { StackActions } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { Recommendations } from '../components/recommendations/recommendations.container'
import { MainStackParamList } from '../navigation/main-stack'

export function RecommendationsScreen (
  {
    navigation
  }: StackScreenProps<MainStackParamList, 'Recommendations'>
): React.ReactElement {
  useDefaultHeader({
    actions: [
      {
        icon: 'search',
        onPress: () => navigation.dispatch(StackActions.push('Search'))
      }
    ]
  })
  return (
    <Recommendations />
  )
}
