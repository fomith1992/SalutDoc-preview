import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { MainStackParamList } from '../navigation/main-stack'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { QuestionFull } from '../components/question/question-full/question-full.container'
import { useQuestionViewed } from '../analytics/question'

export const QuestionFullScreen = (
  { route }: StackScreenProps<MainStackParamList, 'QuestionFull'>
): React.ReactElement => {
  const { id } = route.params
  useQuestionViewed(id)
  return (
    <ScreenContentView>
      <QuestionFull id={id} />
    </ScreenContentView>
  )
}
