import React from 'react'
import { ConsultationPreview } from '../components/moderation/consultation-preview/consultation-preview.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { MainStackParamList } from '../navigation/main-stack'

export const ConsultationPreviewScreen = (
  { route }: StackScreenProps<MainStackParamList, 'ConsultationPreview'>
): React.ReactElement => {
  const { consultationId } = route.params
  return (
    <ScreenContentView>
      <ConsultationPreview consultationId={consultationId} />
    </ScreenContentView>
  )
}
