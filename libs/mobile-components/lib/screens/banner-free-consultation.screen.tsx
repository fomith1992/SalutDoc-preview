import React from 'react'
import { BannerFreeConsultationView } from '../components/banner-free-consultation/banner-free-consultation.view'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const BannerFreeConsultationScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <BannerFreeConsultationView />
    </ScreenContentView>
  )
}
