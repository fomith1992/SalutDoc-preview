import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import {
  EditBasicInfoPatient
} from '../components/edit-profile/edit-basic-info-patient/edit-basic-info-patient.container'

export const EditbasicInfoPatientScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <EditBasicInfoPatient />
    </ScreenContentView>
  )
}
