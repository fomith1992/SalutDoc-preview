import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useEditHeader } from '../modules/ui-kit/header'
import { EditBasicInfoDoctor } from '../components/edit-profile/edit-basic-info-doctor/edit-basic-info-doctor.container'

export const EditbasicInfoDoctorScreen = (): React.ReactElement => {
  useEditHeader({})
  return (
    <ScreenContentView>
      <EditBasicInfoDoctor />
    </ScreenContentView>
  )
}
