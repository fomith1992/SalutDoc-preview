import React from 'react'
import { RegisterDoctorNumber } from '../components/register/register-doctor/register-doctor-number/register-doctor-number.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function RegisterDoctorNumberScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <RegisterDoctorNumber />
    </ScreenContentView>
  )
}
