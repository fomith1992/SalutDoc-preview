import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { CreateCommunity } from '../components/create-community/create-community.container'
import { ScrollView } from 'react-native'

export const CreateCommunityScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        <CreateCommunity />
      </ScrollView>
    </ScreenContentView>
  )
}
