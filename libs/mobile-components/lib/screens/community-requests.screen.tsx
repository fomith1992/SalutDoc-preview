import React from 'react'
import { DividerView } from '../components/divider/divider.view'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { CommunityRequests } from '../components/community-requests/community-requests.container'
import { StackScreenProps } from '@react-navigation/stack'
import { MainStackParamList } from '../navigation/main-stack'
import { StackActions } from '@react-navigation/native'
import { useDefaultHeader } from '../modules/ui-kit/header/use-default-header'

export const CommunityRequestsScreen = (
  { navigation, route }: StackScreenProps<MainStackParamList, 'CommunityRequests'>
): React.ReactElement => {
  const { communityId } = route.params
  const dropDownItems = [
    {
      title: 'Заявки',
      onPress: () => navigation.dispatch(StackActions.replace('CommunityRequests', { communityId })),
      active: true
    },
    {
      title: 'Приглашения',
      onPress: () => navigation.dispatch(StackActions.replace('CommunityInvitations', { communityId })),
      active: false
    }
  ]
  useDefaultHeader({ dropDownItems })
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск' /> */}
      <DividerView />
      <CommunityRequests communityId={communityId} />
    </ScreenContentView>
  )
}
