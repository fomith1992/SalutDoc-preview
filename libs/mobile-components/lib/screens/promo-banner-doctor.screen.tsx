import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { BannerPromoDoctorIcon1 } from '../images/banner-promo-doctor.icon-1'
import { BannerPromoDoctorIcon2 } from '../images/banner-promo-doctor.icon-2'
import { BannerPromoDoctorIcon3 } from '../images/banner-promo-doctor.icon-3'
import { AnimatedBanner } from '../modules/ui-kit/animated-banner/animated-banner.view'

export function PromoBannerDoctorScreen (): React.ReactElement {
  const navigation = useNavigation()
  const data = [{
    Icon: BannerPromoDoctorIcon1,
    title: 'Будьте в тренде',
    description: 'Присоединяйтесь к быстрорастущей \n медицинской экосистеме'
  },
  {
    Icon: BannerPromoDoctorIcon2,
    title: 'Консультируйте',
    description: 'И увеличивайте свой доход (после \n подтверждения диплома)'
  },
  {
    Icon: BannerPromoDoctorIcon3,
    title: 'Создайте портфолио',
    description: 'Объедините фото работ, публикации, \n дипломы и сертификаты в одном месте'
  }]

  return (
    <ScreenContentView>
      <AnimatedBanner
        data={data}
        onEndAnimated={() => navigation.dispatch(StackActions.push('Register_DoctorStack'))}
      />
    </ScreenContentView>
  )
}
