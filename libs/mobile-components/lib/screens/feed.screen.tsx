import React from 'react'
import { CreateUserPostButton } from '../components/create-post-button'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { UserFeed } from '../modules/feed'
import { ScreenBlock } from '../modules/ui-kit'

// TODO Stories disabled for beta version app

export function FeedScreen (): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  // const navigation = useNavigation()
  // const hasNewNotifications = true
  // useDefaultHeader({
  //   actions: [
  //     {
  //       icon: 'notifications',
  //       iconProps: {
  //         withDot: hasNewNotifications
  //       },
  //       onPress: () => navigation.dispatch(StackActions.push('Notifications'))
  //     }
  //   ]
  // })

  return (
    <UserFeed
      userId={user == null ? null : user.id}
      header={user == null ? (<></>) : (
        <>
          {/* <StoriesSliderView stories={storiesData} /> */}
          <ScreenBlock />
          <CreateUserPostButton userId={user.id} />
        </>
      )}
    />
  )
}
