import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '../navigation/root-navigator'
import { MaterialEditPublication } from '../components/add-material/material-publication/material-edit-publication.container'
import { useEditHeader } from '../modules/ui-kit/header'

export const ArticleEditPublicationScreen = (
  { route }: StackScreenProps<RootStackParamList, 'ArticleEditPublication'>
): React.ReactElement => {
  const { workId } = route.params
  useEditHeader({})
  return (
    <ScreenContentView>
      <MaterialEditPublication workId={workId} />
    </ScreenContentView>
  )
}
