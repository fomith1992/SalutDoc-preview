import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { ProgressState } from '../modules/ui-kit/progress-state'
import { Linking, Platform } from 'react-native'

export const InvalidVersionScreen = (): React.ReactElement => {
  function buildUpdateUrl (): string {
    return Platform.OS === 'ios'
      ? 'itms-apps://itunes.apple.com/us/app/id1513387170?mt=8'
      : 'market://details?id=com.salutdoc.mobile.android'
  }

  function openAppByUrl (url: string): void {
    Linking.openURL(url)
      .catch(console.log)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ProgressState
        status='REFRESH'
        title='Обновите приложение'
        description={`Эта версия приложения устарела. Обновите ${'\n'} приложение, чтобы продолжить ${'\n'} пользоваться сервисом.`}
        button={{
          text: 'Обновить приложение',
          onPress: () => openAppByUrl(buildUpdateUrl())
        }}
      />
    </SafeAreaView>
  )
}
