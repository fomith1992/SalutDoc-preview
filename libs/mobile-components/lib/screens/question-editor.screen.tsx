import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '../navigation/root-navigator'
import { QuestionEditor } from '../components/question/question-editor/question-editor.container'

export const QuestionEditorScreen = (
  { route }: StackScreenProps<RootStackParamList, 'QuestionEditor'>
): React.ReactElement => {
  const { questionId } = route.params
  return (
    <ScreenContentView>
      <QuestionEditor questionId={questionId} />
    </ScreenContentView>
  )
}
