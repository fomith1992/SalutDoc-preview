import React from 'react'
import { MaterialConstructor } from '../components/add-material/material-constructor/material-constructor.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const MaterialConstructorScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <MaterialConstructor />
    </ScreenContentView>
  )
}
