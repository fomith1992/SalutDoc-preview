import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { AnswerCreator } from '../components/question/answer-creator/answer-creator.container'
import { RootStackParamList } from '../navigation/root-navigator'

export const AnswerCreateScreen = (
  { route }: StackScreenProps<RootStackParamList, 'AnswerCreate'>
): React.ReactElement => {
  const { questionId } = route.params
  return (
    <ScreenContentView>
      <AnswerCreator questionId={questionId} />
    </ScreenContentView>
  )
}
