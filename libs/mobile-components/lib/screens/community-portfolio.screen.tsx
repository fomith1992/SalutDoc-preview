import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { CommunityPortfolio } from '../components/portfolio/portfolio/community-portfolio.container'
import { StackScreenProps } from '@react-navigation/stack'
import { MainStackParamList } from '../navigation/main-stack'

export const CommunityPortfolioScreen = (
  { route }: StackScreenProps<MainStackParamList, 'CommunityPortfolio'>
): React.ReactElement => {
  const { communityId } = route.params
  return (
    <ScreenContentView>
      <CommunityPortfolio communityId={communityId} />
    </ScreenContentView>
  )
}
