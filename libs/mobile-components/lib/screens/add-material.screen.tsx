import React from 'react'
import { AddMaterial } from '../components/add-material/add-material.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const AddMaterialScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <AddMaterial />
    </ScreenContentView>
  )
}
