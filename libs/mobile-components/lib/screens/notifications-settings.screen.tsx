import { NotificationsSettings } from '../components/notifications-settings/notifications-settings.container'
import React from 'react'

export const NotificationsSettingsScreen = (): React.ReactElement => (<NotificationsSettings />)
