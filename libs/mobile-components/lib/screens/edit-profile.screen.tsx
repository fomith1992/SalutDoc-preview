import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { EditProfileInfo } from '../components/edit-profile/edit-profile-preview.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'

export const EditProfileScreen = (
  { route }: StackScreenProps<MainStackParamList, 'EditProfile'>
): React.ReactElement => {
  return (
    <ScreenContentView>
      <EditProfileInfo userId={route.params.userId} />
    </ScreenContentView>
  )
}
