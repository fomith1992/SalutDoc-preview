import { StackActions } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { Questions } from '../components/question/questions/questions.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useQuestionsAuthQuery } from '../gen/graphql'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { GlobalNavigatorParamList } from '../navigation/global-navigator-param-list'

export const QuestionsScreen = (
  { route, navigation }: StackScreenProps<GlobalNavigatorParamList, 'Questions'>
): React.ReactElement => {
  const { userId } = route.params
  const user = useAuthenticatedUserOrNull()
  const dropDownItems = [
    {
      title: 'Работы',
      onPress: () => navigation.dispatch(StackActions.replace('UserPortfolio', { userId })),
      active: false
    },
    {
      title: 'Вопросы',
      onPress: () => navigation.dispatch(StackActions.replace('Questions', { userId })),
      active: true
    }
  ]

  const { data } = useQuestionsAuthQuery({
    variables: { userId }
  })

  useDefaultHeader(user?.id === userId ? {
    title: 'Мои вопросы',
    dropDownItems,
    actions: data?.userById?.canCreateQuestion === true ? [{
      icon: 'plus-drop',
      onPress: () => navigation.navigate('QuestionCreator', {})
    }] : []
  } : {
    title: 'Вопросы пользователя',
    dropDownItems
  })

  return (
    <ScreenContentView>
      <Questions userId={userId} />
    </ScreenContentView>
  )
}
