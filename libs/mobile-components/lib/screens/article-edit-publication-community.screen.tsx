import { StackScreenProps } from '@react-navigation/stack'
import * as React from 'react'
import { Text } from 'react-native'
import { CommunitiesList, Community } from '../components/communities/communities.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useAuthenticatedUser } from '../components/user-context/user-context-provider'
import { RootStackParamList } from '../navigation/root-navigator'
import { useRootStore } from '../stores/root.store'

export function ArticleEditPublicationCommunityScreen (
  { navigation, route }: StackScreenProps<RootStackParamList, 'ArticleEditPublicationCommunity'>
): React.ReactElement {
  const { workId } = route.params
  const { getAtricle } = useRootStore().domains.articleData
  const article = getAtricle(workId)

  if (article == null) {
    return <Text>Произошла ошибка</Text>
  }

  const { id: userId } = useAuthenticatedUser()
  return (
    <ScreenContentView>
      <CommunitiesList
        userId={userId}
        onCommunityPress={({ id, name }: Community): void => {
          article.setCommunity({ id, name })
          navigation.navigate('ArticleEditPublication', { workId })
        }}
        renderAction={() => <></>}
      />
    </ScreenContentView>
  )
}
