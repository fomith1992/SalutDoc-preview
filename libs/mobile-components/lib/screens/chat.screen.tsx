import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { StackScreenProps } from '@react-navigation/stack'
import React, { useCallback } from 'react'
import { useChatHeader } from '../modules/ui-kit/header'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { Chat } from '../modules/messenger'
import { MainStackParamList } from '../navigation/main-stack'
import { useFocusEffect } from '@react-navigation/native'

export function ChatScreen (
  {
    route,
    navigation
  }: StackScreenProps<MainStackParamList, 'Chat'>
): React.ReactElement {
  // TODO It works but with some animation glitches :(
  const bottomTabNavigation = navigation.dangerouslyGetParent<BottomTabNavigationProp<any>>()

  useFocusEffect(
    useCallback(() => {
      bottomTabNavigation.setOptions({ tabBarVisible: false })
      return () => bottomTabNavigation.setOptions({ tabBarVisible: true })
    }, [bottomTabNavigation])
  )

  useChatHeader({
    actions: [
      {
        icon: 'more'
      }
    ],
    chatData: {
      avatar: null,
      name: 'Алексей Петров',
      specialization: 'Терапевт'
    }
  })

  return (
    <ScreenContentView>
      <Chat id={route.params.chatId} />
    </ScreenContentView>
  )
}
