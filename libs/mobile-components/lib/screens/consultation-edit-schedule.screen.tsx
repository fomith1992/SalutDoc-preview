import React from 'react'
import { ConsultationEditShedule } from '../components/doctor-shedule/consultation-edit-shedule/consultation-edit-shedule.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const ConsultationEditSheduleScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <ConsultationEditShedule />
    </ScreenContentView>
  )
}
