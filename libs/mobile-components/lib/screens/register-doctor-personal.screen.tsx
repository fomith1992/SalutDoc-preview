import React from 'react'
import { RegisterDoctorPersonal } from '../components/register/register-doctor/register-doctor-personal/register-doctor-personal.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function RegisterDoctorPersonalScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <RegisterDoctorPersonal />
    </ScreenContentView>
  )
}
