import React from 'react'
import { ChooseDoctor } from '../components/choose-a-doctor/choose-a-doctor.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const SearchScreen = (
): React.ReactElement => {
  return (
    <ScreenContentView>
      <ChooseDoctor />
    </ScreenContentView>
  )
}
