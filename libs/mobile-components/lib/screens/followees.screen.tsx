import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { DividerView } from '../components/divider/divider.view'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { UserFollowees } from '../modules/user-follow'
import { MainStackParamList } from '../navigation/main-stack'

export const FolloweesScreen = (
  {
    route
  }: StackScreenProps<MainStackParamList, 'Followees'>
): React.ReactElement => {
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск по подписчикам...' /> */}
      <DividerView />
      <UserFollowees userId={route.params.userId} />
    </ScreenContentView>
  )
}
