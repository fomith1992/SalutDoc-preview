import React from 'react'
import { DoctorConsultationsFree } from '../modules/consultations/doctor-consultations/doctor-consultations-free.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function DoctorConsultationsFreeScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <DoctorConsultationsFree />
    </ScreenContentView>
  )
}
