import React from 'react'
import { Moderation } from '../components/moderation/moderation.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const ModerationScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <Moderation />
    </ScreenContentView>
  )
}
