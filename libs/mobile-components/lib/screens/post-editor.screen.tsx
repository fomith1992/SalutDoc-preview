import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParamList } from '../navigation/root-navigator'
import { PostEditor } from '../modules/posts'

export const PostEditorScreen = (
  { route }: StackScreenProps<RootStackParamList, 'PostEditor'>
): React.ReactElement => {
  const { postId } = route.params
  return (
    <ScreenContentView>
      <PostEditor postId={postId} />
    </ScreenContentView>
  )
}
