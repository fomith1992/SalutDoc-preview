import React from 'react'
import { ArticleConstructor } from '../components/add-material/article-constructor/article-constructor.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const ArticleConstructorScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <ArticleConstructor />
    </ScreenContentView>
  )
}
