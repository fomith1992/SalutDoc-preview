import React from 'react'
import { LoginVerification } from '../components/login/login-verification/login-verification.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function LoginVerificationScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <LoginVerification />
    </ScreenContentView>
  )
}
