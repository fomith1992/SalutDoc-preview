import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { EditCommunity } from '../components/edit-community/edit-community.container'
import { useEditHeader } from '../modules/ui-kit/header'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'

export const EditCommunityScreen = (
  { route }: StackScreenProps<MainStackParamList, 'EditCommunity'>
): React.ReactElement => {
  const { communityId } = route.params
  useEditHeader({})

  return (
    <ScreenContentView>
      <EditCommunity communityId={communityId} />
    </ScreenContentView>
  )
}
