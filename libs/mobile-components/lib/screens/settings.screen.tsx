import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { Settings } from '../components/settings/settings.container'

export function SettingsScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <Settings />
    </ScreenContentView>
  )
}
