import { MedicalReportPreview } from '../modules/consultations/medical-report-preview/medical-report-preview.container'
import { GlobalNavigatorParamList } from '../navigation/global-navigator-param-list'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { StackScreenProps } from '@react-navigation/stack'

export const CreateMedicalReportPreviewScreen = (
  { route }: StackScreenProps<GlobalNavigatorParamList, 'CreateMedicalReport_Review'>
): React.ReactElement => {
  const { consultationId } = route.params
  return (
    <ScreenContentView>
      <MedicalReportPreview consultationId={consultationId} />
    </ScreenContentView>
  )
}
