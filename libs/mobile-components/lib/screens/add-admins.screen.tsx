import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { DividerView } from '../components/divider/divider.view'
import { CommunityAddAdmins } from '../components/edit-community/community-add-admins/community-add-admins.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'

export const AddAdminsScreen = (
  { route }: StackScreenProps<MainStackParamList, 'AddAdmins'>
): React.ReactElement => {
  const { communityId } = route.params
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск по подписчикам...' /> */}
      <DividerView />
      <CommunityAddAdmins communityId={communityId} />
    </ScreenContentView>
  )
}
