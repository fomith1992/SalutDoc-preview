import { StackActions, useNavigation } from '@react-navigation/native'
import { Notifications } from '../components/notifications/notifications.container'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useDefaultHeader } from '../modules/ui-kit/header'

export const NotificationsScreen = (): React.ReactElement => {
  const navigation = useNavigation()
  useDefaultHeader({
    actions: [{
      icon: 'settings',
      onPress: () => navigation.dispatch(StackActions.push('NotificationsSettings'))
    }]
  })
  return (
    <ScreenContentView>
      <Notifications />
    </ScreenContentView>
  )
}
