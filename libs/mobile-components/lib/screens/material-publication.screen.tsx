import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { MaterialPublication } from '../components/add-material/material-publication/material-publication.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useEditHeader } from '../modules/ui-kit/header'
import { RootStackParamList } from '../navigation/root-navigator'

export const MaterialPublicationScreen = (
  { route }: StackScreenProps<RootStackParamList, 'MaterialPublication'>
): React.ReactElement => {
  const { materialType } = route.params
  useEditHeader({})
  return (
    <ScreenContentView>
      <MaterialPublication materialType={materialType} />
    </ScreenContentView>
  )
}
