import React from 'react'
import { DividerView } from '../components/divider/divider.view'
import { CommunityMembers } from '../components/community-members/community-members.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'
import { StackScreenProps } from '@react-navigation/stack'

export const CommunityMembersScreen = (
  { route }: StackScreenProps<MainStackParamList, 'Members'>
): React.ReactElement => {
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск по подписчикам...' /> */}
      <DividerView />
      <CommunityMembers communityId={route.params.communityId} />
    </ScreenContentView>
  )
}
