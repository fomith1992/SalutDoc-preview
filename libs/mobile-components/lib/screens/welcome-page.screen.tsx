import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { WelcomePage } from '../components/welcome-page/welcome-page.container'

export function WelcomePageScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <WelcomePage />
    </ScreenContentView>
  )
}
