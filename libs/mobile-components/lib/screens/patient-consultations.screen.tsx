import React from 'react'
import { PatientConsultations } from '../modules/consultations/patient-consultations/patient-consultations.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const PatientConsultationsScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <PatientConsultations />
    </ScreenContentView>
  )
}
