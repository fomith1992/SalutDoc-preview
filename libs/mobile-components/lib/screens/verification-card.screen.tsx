
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { VerificationCard } from '../components/admin-panel/verification-card/verification-card.container'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'

export const VerificationCardScreen = (
  { route }: StackScreenProps<MainStackParamList, 'VerificationCard'>
): React.ReactElement => {
  const { requestId } = route.params

  useDefaultHeader({
    title: `Заявка #${requestId}`
  })

  return (
    <ScreenContentView>
      <VerificationCard id={requestId} />
    </ScreenContentView>
  )
}
