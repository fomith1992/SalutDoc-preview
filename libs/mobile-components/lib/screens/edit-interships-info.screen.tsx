import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { EditInternshipsInfo } from '../components/edit-profile/edit-internships-info/edit-internships-info.container'
import { useEditHeader } from '../modules/ui-kit/header'

export const EditInternshipsInfoScreen = (): React.ReactElement => {
  useEditHeader({})
  return (
    <ScreenContentView>
      <EditInternshipsInfo />
    </ScreenContentView>
  )
}
