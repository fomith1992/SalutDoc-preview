
import React from 'react'
import { AdminPanel } from '../components/admin-panel/admin-panel.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const AdminPanelScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <AdminPanel />
    </ScreenContentView>
  )
}
