import { StackScreenProps } from '@react-navigation/stack'
import * as React from 'react'
import { storeData } from '../components/add-material/material-publication/store-data-parser'
import { CommunitiesList, Community } from '../components/communities/communities.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useAuthenticatedUser } from '../components/user-context/user-context-provider'
import { RootStackParamList } from '../navigation/root-navigator'

export function ArticlePublicationCommunityScreen (
  {
    navigation,
    route
  }: StackScreenProps<RootStackParamList, 'ArticlePublicationCommunity'>
): React.ReactElement {
  const { materialType } = route.params
  const { setCommunity } = storeData({ materialType })
  const { id: userId } = useAuthenticatedUser()
  return (
    <ScreenContentView>
      <CommunitiesList
        userId={userId}
        onCommunityPress={({ id, name }: Community): void => {
          setCommunity({ id, name })
          navigation.navigate('MaterialPublication', { materialType })
        }}
        renderAction={() => <></>}
      />
    </ScreenContentView>
  )
}
