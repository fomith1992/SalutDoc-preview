import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useEditHeader } from '../modules/ui-kit/header'
import {
  EditUniversitiesInfo
} from '../components/edit-profile/edit-universities-info/edit-universities-info.container'

export const EditUniversitiesInfoScreen = (): React.ReactElement => {
  useEditHeader({})
  return (
    <ScreenContentView>
      <EditUniversitiesInfo />
    </ScreenContentView>
  )
}
