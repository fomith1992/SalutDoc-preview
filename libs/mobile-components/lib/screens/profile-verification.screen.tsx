import * as React from 'react'
import { ProfileVerification } from '../components/profile-verification/profile-verification.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function ProfileVerificationScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <ProfileVerification />
    </ScreenContentView>

  )
}
