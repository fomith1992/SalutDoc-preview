import React from 'react'
import { RegisterDoctorVerification } from '../components/register/register-doctor/register-doctor-verification/register-doctor-verification.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function RegisterDoctorVerificationScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <RegisterDoctorVerification />
    </ScreenContentView>
  )
}
