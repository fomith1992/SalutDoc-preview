import React from 'react'
import { Login } from '../components/login-modal/login.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function LoginModalScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <Login />
    </ScreenContentView>
  )
}
