
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import {
  VerificationCancelCard
} from '../components/admin-panel/verification-cancel-card/verification-cancel-card.container'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { MainStackParamList } from '../navigation/main-stack'

export const CancelRequestScreen = (
  { route }: StackScreenProps<MainStackParamList, 'CancelRequest'>
): React.ReactElement => {
  const { requestId } = route.params

  useDefaultHeader({
    title: `Заявка #${requestId}`
  })

  return (
    <ScreenContentView>
      <VerificationCancelCard id={requestId} />
    </ScreenContentView>
  )
}
