import React from 'react'
import { RegisterPatientPersonal } from '../components/register/register-patient/register-patient-personal/register-patient-personal.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useDefaultHeader } from '../modules/ui-kit/header'

export function RegisterPatientPersonalScreen (): React.ReactElement {
  useDefaultHeader({
    actions: [{
      icon: 'help',
      onPress: () => {}
    }]
  })

  return (
    <ScreenContentView>
      <RegisterPatientPersonal />
    </ScreenContentView>
  )
}
