import React from 'react'
import { CreateAccountCard } from '../components/create-account-card/create-account-card.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export function CreateAccountCardScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <CreateAccountCard />
    </ScreenContentView>
  )
}
