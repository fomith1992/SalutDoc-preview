import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { ScrollView } from 'react-native'
import { DividerView } from '../components/divider/divider.view'
import { Chats } from '../components/chats/chats.view'
import { Cover } from '../components/chats/chats.style'

const chatsData = [
  {
    name: 'Name Surname',
    time: '12:51',
    avatar: 'https://lh3.googleusercontent.com/proxy/eO4Yy8yIKPMtMoZ2rmWDWUsIv6cft-kDlTOuxA3BNqwFFov3vRWDnIDxL584zaYBEZGtW7rwV67Jyl1Dtinnp1YGt099W_VPJzXsthMjd13aqk_pNZ0',
    text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et temporibus blanditiis dignissimos voluptatibus, magni neque voluptas ducimus commodi facilis sunt libero est asperiores, ipsum perferendis recusandae qui consectetur in delectus.',
    unreadCount: 12
  },
  {
    name: 'Name Surname',
    time: '12:51',
    text: 'Lorem ipsum dolor si',
    unreadCount: 1
  },
  {
    name: 'Name Surname',
    time: '12:51',
    text: 'Lorem ipsum dolor si',
    isMy: true,
    unreadCount: 0
  },
  {
    name: 'Name Surname',
    time: '12:51',
    avatar: 'https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png',
    text: 'Lorem ipsum dolor si',
    unreadCount: 1
  },
  {
    name: 'Name Surname',
    time: '12:51',
    text: 'Lorem ipsum dolor si',
    isMy: true,
    unreadCount: 0
  },
  {
    name: 'Name Surname',
    time: '12:51',
    text: 'Lorem ipsum dolor si',
    unreadCount: 1
  },
  {
    name: 'Name Surname',
    time: '12:51',
    text: 'Lorem ipsum dolor si',
    isMy: true,
    unreadCount: 0
  }
]

export const ChatsScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <Cover>
        {/* <InlineSearchView placeholder='Поиск' /> */}
        <DividerView />
        <ScrollView>
          <Chats
            items={chatsData}
          />
        </ScrollView>
      </Cover>
    </ScreenContentView>
  )
}
