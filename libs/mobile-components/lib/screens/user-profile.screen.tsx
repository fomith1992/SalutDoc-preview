import { StackActions } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { useUserProfileViewed } from '../analytics/profile'
import { CreateUserPostButton } from '../components/create-post-button'
import { useDefaultHeader } from '../modules/ui-kit/header'
import { RequestModal } from '../components/request-modal/request-modal.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { UserProfile } from '../components/user-profile/user-profile.container'
import { VerificationCard } from '../components/verification-card/verification-card.container'
import { WorksSliderUser } from '../components/works-slider/works-slider-user.container'
import { useBooleanFlag } from '../hooks/use-boolean-flag'
import { UserPersonalFeed } from '../modules/feed'
import { ContextMenu, ContextMenuButton } from '../modules/ui-kit'
import { MainStackParamList } from '../navigation/main-stack'

export const UserProfileScreen = (
  {
    route,
    navigation
  }: StackScreenProps<MainStackParamList, 'Profile'>
): React.ReactElement => {
  const { userId } = route.params
  useUserProfileViewed(userId)
  const authUser = useAuthenticatedUserOrNull()
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)

  useDefaultHeader({
    actions: userId === authUser?.id ? [{
      icon: 'write',
      onPress: () => authUser?.role === 'USER'
        ? navigation.dispatch(StackActions.push('EditBasicInfoPatient'))
        : navigation.dispatch(StackActions.push('EditProfile'))
    }, {
      icon: 'settings',
      onPress: () => navigation.dispatch(StackActions.push('Settings'))
    }] : [{
      icon: 'more',
      onPress: showBottomMenu
    }]
  })

  const menuButtons: ContextMenuButton[] = [
    {
      type: 'black',
      title: 'Пожаловаться',
      onPress: () => {
        hideBottomMenu()
        showReportModal()
      }
    },
    {
      type: 'red',
      title: 'Отмена',
      onPress: hideBottomMenu
    }
  ]
  return (
    <ScreenContentView>
      <UserPersonalFeed
        userId={userId}
        header={
          <>
            <UserProfile userId={userId} />
            <VerificationCard userId={userId} />
            <WorksSliderUser userId={userId} />
            {authUser?.id === userId && authUser.role !== 'USER'
              ? (
                <CreateUserPostButton userId={userId} />
              ) : null}
          </>
        }
      />
      <ContextMenu
        visible={bottomMenuVisible}
        onClose={hideBottomMenu}
        buttons={menuButtons}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='profile'
        contentId={userId}
      />
    </ScreenContentView>
  )
}
