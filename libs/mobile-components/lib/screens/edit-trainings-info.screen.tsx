import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { EditTrainingsInfo } from '../components/edit-profile/edit-trainings-info/edit-trainings-info.container'
import { useEditHeader } from '../modules/ui-kit/header'

export const EditTrainingsInfoScreen = (): React.ReactElement => {
  useEditHeader({})
  return (
    <ScreenContentView>
      <EditTrainingsInfo />
    </ScreenContentView>
  )
}
