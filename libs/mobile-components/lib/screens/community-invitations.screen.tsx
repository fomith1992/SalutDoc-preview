import React from 'react'
import { DividerView } from '../components/divider/divider.view'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { CommunityInvitations } from '../components/community-invitations/community-invitations.container'
import { StackScreenProps } from '@react-navigation/stack'
import { MainStackParamList } from '../navigation/main-stack'
import { StackActions } from '@react-navigation/native'
import { useDefaultHeader } from '../modules/ui-kit/header/use-default-header'

export const CommunityInvitationsScreen = (
  { navigation, route }: StackScreenProps<MainStackParamList, 'CommunityInvitations'>
): React.ReactElement => {
  const { communityId } = route.params
  const dropDownItems = [
    {
      title: 'Заявки',
      onPress: () => navigation.dispatch(StackActions.replace('CommunityRequests', { communityId })),
      active: false
    },
    {
      title: 'Приглашения',
      onPress: () => navigation.dispatch(StackActions.replace('CommunityInvitations', { communityId })),
      active: true
    }
  ]
  useDefaultHeader({ dropDownItems })
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск' /> */}
      <DividerView />
      <CommunityInvitations communityId={communityId} />
    </ScreenContentView>
  )
}
