import React from 'react'
import { AlbumConstructor } from '../components/add-material/album-constructor/album-constructor.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'

export const AlbumConstructorScreen = (): React.ReactElement => {
  return (
    <ScreenContentView>
      <AlbumConstructor />
    </ScreenContentView>
  )
}
