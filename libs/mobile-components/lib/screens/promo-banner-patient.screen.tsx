import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { BannerPromoPatientIcon1 } from '../images/banner-promo-patient.icon-1'
import { BannerPromoPatientIcon2 } from '../images/banner-promo-patient.icon-2'
import { BannerPromoPatientIcon3 } from '../images/banner-promo-patient.icon-3'
import { consultationDuration } from '../modules/config'
import { AnimatedBanner, AnimatedBannerData } from '../modules/ui-kit/animated-banner/animated-banner.view'

export function PromoBannerPatientScreen (): React.ReactElement {
  const navigation = useNavigation()
  const data: AnimatedBannerData[] = [{
    Icon: BannerPromoPatientIcon1,
    title: 'Высокая экспертиза',
    description: 'Наши врачи — это специалисты \n с многолетним опытом'
  },
  {
    Icon: BannerPromoPatientIcon2,
    title: 'Круглосуточная помощь',
    description: 'Найдем врача за 15 минут \n даже если на часах уже ночь '
  },
  {
    Icon: BannerPromoPatientIcon3,
    title: 'Выгодная цена',
    description: `${consultationDuration}-минутная консультация с любым \n специалистом всего за `,
    descriptionStrong: '490 рублей'
  }]

  return (
    <ScreenContentView>
      <AnimatedBanner
        data={data}
        onEndAnimated={() => navigation.dispatch(StackActions.push('Register_Patient'))}
      />
    </ScreenContentView>
  )
}
