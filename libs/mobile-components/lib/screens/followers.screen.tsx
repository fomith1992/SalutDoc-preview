import React from 'react'
import { DividerView } from '../components/divider/divider.view'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { UserFollowers } from '../modules/user-follow'
import { MainStackParamList } from '../navigation/main-stack'
import { StackScreenProps } from '@react-navigation/stack'

export const FollowersScreen = (
  {
    route
  }: StackScreenProps<MainStackParamList, 'Followers'>
): React.ReactElement => {
  return (
    <ScreenContentView>
      {/* <InlineSearchView placeholder='Поиск по подписчикам...' /> */}
      <DividerView />
      <UserFollowers userId={route.params.userId} />
    </ScreenContentView>
  )
}
