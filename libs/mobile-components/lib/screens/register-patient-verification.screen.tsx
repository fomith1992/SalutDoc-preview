import React from 'react'
import { RegisterPatientVerification } from '../components/register/register-patient/register-patient-verification/register-patient-verification.container'
import { ScreenContentView } from '../components/screen-content-view/screen-content-view'
import { useDefaultHeader } from '../modules/ui-kit/header'

export function RegisterPatientVerificationScreen (): React.ReactElement {
  useDefaultHeader({
    actions: [{
      icon: 'help',
      onPress: () => {}
    }]
  })

  return (
    <ScreenContentView>
      <RegisterPatientVerification />
    </ScreenContentView>
  )
}
