import { createContext, useContext } from 'react'

export interface LoginProps {
  page: 'registration' | 'recovery' | 'login'
}

export interface AuthenticationContext {
  login: ({ page }: LoginProps) => void
  logout: () => void
}

const authenticationContext = createContext<AuthenticationContext | null>(null)

export const AuthenticationProvider = authenticationContext.Provider

export function useAuthentication (): AuthenticationContext {
  const context = useContext(authenticationContext)
  if (context == null) {
    throw new Error('Authentication context not initialized')
  }
  return context
}
