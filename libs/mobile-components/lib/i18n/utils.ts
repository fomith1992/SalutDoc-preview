import moment from 'moment'

export function toCaseCount (titles: [string, string, string], count: number): string {
  if (count % 100 > 4 && count % 100 < 20) {
    return titles[2]
  } else {
    switch (Math.min(count % 10, 5)) {
      case 1:
        return titles[0]
      case 2:
      case 3:
      case 4:
        return titles[1]
      case 0:
      case 5:
      default:
        return titles[2]
    }
  }
}

export function formatDate (date: Date | string): string {
  const dt = moment(date)
  if (dt.hours() === 0 && dt.minutes() === 0 && dt.seconds() === 0) {
    return dt.format('DD.MM.YYYY')
  } else {
    return dt.format('DD.MM.YYYY HH:mm:ss')
  }
}
