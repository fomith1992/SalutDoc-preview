import { useEffect } from 'react'
import { AddDetailView } from 'recombee-js-api-client'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useRecombeeClient } from './analytics-context'

export function useCommunityViewed (communityId: string): void {
  const user = useAuthenticatedUserOrNull()
  const client = useRecombeeClient()

  useEffect(() => {
    if (user != null) {
      const request = new AddDetailView(user.id, `community:${communityId}`, {
        cascadeCreate: true
      })
      client.send(request, (error) => {
        if (error != null) {
          console.error('Failed to send community viewed event to recombee', error)
        }
      })
    }
  }, [user?.id, communityId])
}
