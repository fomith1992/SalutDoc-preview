import analytics from '@react-native-firebase/analytics'
import { AppEventsLogger } from 'react-native-fbsdk-next'

export function sendScreenChangeEvent (
  previousRouteName: string | undefined,
  currentRouteName: string | undefined
): void {
  if (currentRouteName !== undefined && currentRouteName !== previousRouteName) {
    AppEventsLogger.logEvent('ScreenChange', { screen: currentRouteName })
    analytics().logScreenView({
      screen_class: currentRouteName,
      screen_name: currentRouteName
    }).catch(err => {
      console.error(`[Firebase Analytics] Failed to log screen view event for ${currentRouteName}`, err)
    })
  }
}
