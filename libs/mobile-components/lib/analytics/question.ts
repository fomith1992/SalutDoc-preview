import { useEffect } from 'react'
import { AddDetailView } from 'recombee-js-api-client'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useRecombeeClient } from './analytics-context'
import { AppEventsLogger } from 'react-native-fbsdk-next'

export function useQuestionViewed (questionId: string): void {
  const user = useAuthenticatedUserOrNull()
  const client = useRecombeeClient()

  useEffect(() => {
    if (user != null) {
      const request = new AddDetailView(user.id, `question:${questionId}`, {
        cascadeCreate: true
      })
      client.send(request, (error) => {
        if (error != null) {
          console.error('Failed to send question viewed event to recombee', error)
        }
      })
    }
  }, [user?.id, questionId])

  useEffect(() => {
    const eventName = 'fb_mobile_content_view'
    const eventData = {
      fb_content_type: 'Question',
      fb_content_id: questionId,
      fb_currency: 'RUB'
    }
    AppEventsLogger.logEvent(eventName, eventData)
  }, [questionId])
}
