import React from 'react'

// TODO recombee-js-api-client does not support SSR
// TODO SD-658
export function AnalyticsContextProvider ({ children }: React.PropsWithChildren<{}>): React.ReactElement {
  return <>{children}</>
}
