import { useEffect } from 'react'
import { AddDetailView } from 'recombee-js-api-client'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useRecombeeClient } from './analytics-context'
import { AppEventsLogger } from 'react-native-fbsdk-next'

export function usePostViewed (postId: string): void {
  const user = useAuthenticatedUserOrNull()
  const client = useRecombeeClient()

  useEffect(() => {
    if (user != null) {
      const request = new AddDetailView(user.id, `post:${postId}`, {
        cascadeCreate: true
      })
      client.send(request, (error) => {
        if (error != null) {
          console.error('Failed to send post viewed event to recombee', error)
        }
      })
    }
  }, [user?.id, postId])

  useEffect(() => {
    const eventName = 'fb_mobile_content_view'
    const eventData = {
      fb_content_type: 'Post',
      fb_content_id: postId,
      fb_currency: 'RUB'
    }
    AppEventsLogger.logEvent(eventName, eventData)
  }, [postId])
}
