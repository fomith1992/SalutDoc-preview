import analytics from '@react-native-firebase/analytics'

export function firebaseLogEvent (eventName: string, params?: { [key: string]: any }): void {
  analytics().logEvent(eventName, params).catch(err => {
    console.error(`[Firebase Analytics] Failed to log ${eventName} with params:`, params, err)
  })
}
