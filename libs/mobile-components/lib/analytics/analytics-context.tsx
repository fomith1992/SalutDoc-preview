import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'
import React, { useContext, useMemo } from 'react'
import { ApiClient } from 'recombee-js-api-client'

const databaseId = expectNonNil(process.env.RECOMBEE_DATABASE_ID)
const publicToken = expectNonNil(process.env.RECOMBEE_PUBLIC_TOKEN)

interface AnalyticsContext {
  recombeeClient: ApiClient
}

const analyticsContext = React.createContext<AnalyticsContext | null>(null)

export function useRecombeeClient (): ApiClient {
  const ctx = useContext(analyticsContext)
  if (ctx == null) {
    throw new Error('AnalyticsContext not initialized')
  }
  return ctx.recombeeClient
}

export interface AnalyticsContextProviderProps {
  children?: React.ReactNode
}

export function AnalyticsContextProvider ({ children }: AnalyticsContextProviderProps): React.ReactElement {
  const context = useMemo(() => ({
    recombeeClient: new ApiClient(databaseId, publicToken)
  }), [databaseId, publicToken])

  return (
    <analyticsContext.Provider
      value={context}
    >
      {children}
    </analyticsContext.Provider>
  )
}
