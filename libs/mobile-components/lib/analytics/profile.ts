import { useEffect } from 'react'
import { AddDetailView } from 'recombee-js-api-client'
import { useAuthenticatedUserOrNull } from '../components/user-context/user-context-provider'
import { useRecombeeClient } from './analytics-context'

export function useUserProfileViewed (userId: string): void {
  const user = useAuthenticatedUserOrNull()
  const client = useRecombeeClient()

  useEffect(() => {
    if (user != null && user.id !== userId) {
      const request = new AddDetailView(user.id, `user:${userId}`, {
        cascadeCreate: true
      })
      client.send(request, (error) => {
        if (error != null) {
          console.error('Failed to send user profile viewed event to recombee', error)
        }
      })
    }
  }, [user?.id, userId])
}
