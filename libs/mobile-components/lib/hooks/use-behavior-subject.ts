import { useMemo, useRef } from 'react'
import { BehaviorSubject, Observable } from 'rxjs'
import * as $ from 'rxjs/operators'

export function useBehaviorSubject<T> (value: T): Observable<T> {
  const subject = useRef(new BehaviorSubject(value)).current
  subject.next(value)
  return useMemo(() => subject.pipe($.distinct()), [])
}
