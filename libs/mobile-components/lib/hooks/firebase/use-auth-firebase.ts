import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth'
import { useState, useCallback, useEffect } from 'react'

export interface AuthFirebaseHook {
  loading: boolean
  error: string | null
  confirmCode: (code: string) => void
  resend: () => void
}

interface AuthFirebaseProps {
  phoneNumber: string | null
  onSuccess: (user: FirebaseAuthTypes.User) => void
}

export function useAuthFirebase ({
  phoneNumber,
  onSuccess
}: AuthFirebaseProps): AuthFirebaseHook {
  const [confirmation, setConfirmation] = useState<FirebaseAuthTypes.ConfirmationResult | null>(null)
  const [error, setError] = useState<string | null>(null)
  const [loading, setLoading] = useState<boolean>(false)

  async function sendMessageCode (resend: boolean): Promise<void> {
    if (phoneNumber != null) {
      try {
        setLoading(true)
        const confirmation = await auth().signInWithPhoneNumber(phoneNumber, resend)
        setConfirmation(confirmation)
        setLoading(false)
      } catch (error) {
        setError(error.code)
      }
    }
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(user => {
      if (user != null) {
        onSuccess(user)
      } else {
        console.log('sms code should have been sent')
      }
    })
    return subscriber
  }, [confirmation])

  useEffect(() => {
    sendMessageCode(false).catch(e => console.log('err: ', e))
  }, [phoneNumber])

  return {
    loading,
    error,
    confirmCode: useCallback(async (code) => {
      try {
        setLoading(true)
        const user = (await confirmation?.confirm(code))?.user
        setLoading(false)
        if (user != null) {
          onSuccess(user)
        } else {
          // TODO: add onError
          console.log('Confirm error')
        }
      } catch (error) {
        setError(error.code)
        setLoading(false)
      }
    }, [confirmation]),
    resend: useCallback(async () => await sendMessageCode(true), [])
  }
}
