import auth from '@react-native-firebase/auth'
import { useCallback } from 'react'

export function useLogoutFirebase (): () => void {
  return useCallback(() => {
    const user = auth().currentUser
    if (user != null) {
      auth().signOut().catch(e => console.log('Firebase signout error: ', e))
    }
  }, [])
}
