import { useState, useCallback } from 'react'

export interface BooleanFlagHook {
  state: boolean
  setState: (value: boolean) => void
  setTrue: () => void
  setFalse: () => void
  toggle: () => void
}

export function useBooleanFlag (initialValue: boolean): BooleanFlagHook {
  const [state, setState] = useState(initialValue)
  const setTrue = useCallback(() => setState(true), [])
  const setFalse = useCallback(() => setState(false), [])
  const toggle = useCallback(() => setState(state => !state), [])
  return { state, setState, setTrue, setFalse, toggle }
}
