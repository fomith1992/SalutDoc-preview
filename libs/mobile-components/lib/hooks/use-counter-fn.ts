import { useCallback, useRef } from 'react'

export type UseCounterFnHook = () => number

export function useCounterFn (firstValue = 0): UseCounterFnHook {
  const state = useRef(firstValue)
  return useCallback(() => state.current++, [state])
}
