import useAsyncFn from 'react-use/lib/useAsyncFn'

export interface RefreshHook {
  refreshing: boolean
  refresh: () => void
}

export function useRefresh (refetch: () => Promise<any>): RefreshHook {
  const [state, refresh] = useAsyncFn(async () => {
    return await refetch().catch((error) => {
      console.log(error)
    })
  })
  return { refreshing: state.loading, refresh }
}
