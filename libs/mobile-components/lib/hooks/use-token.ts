import { useEffect, useRef } from 'react'

/**
 * Allows custom comparison of complex values in dependencies of other hooks.
 * Example:
 * ```
 * function Component (props: { data: any[] }) {
 *   const dataToken = useToken(props.data, isEqualShallow)
 *   // will trigger re-computation only if contents of the props.data changed
 *   const transformedData = useMemo(() => props.data.map(transformation), [dataToken])
 * }
 * ```
 */
export function useToken<T> (value: T, isEqual: (prev: T, current: T) => boolean): object {
  const prevRef = useRef(value)
  const tokenRef = useRef({})
  let token = tokenRef.current
  if (prevRef.current !== value && !isEqual(prevRef.current, value)) {
    token = {}
  }
  useEffect(() => {
    prevRef.current = value
    tokenRef.current = token
  })
  return token
}
