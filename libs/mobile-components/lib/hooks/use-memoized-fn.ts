import { DependencyList, useMemo } from 'react'
import { isEqualShallow } from './utils'

type MemoizedState<T extends (...args: any) => any> = {
  args: Parameters<T>
  value: ReturnType<T>
}

type MemoizedFn<T extends (...args: any) => any> = (...args: Parameters<T>) => ReturnType<T>

export function useMemoizedFn<T extends (...args: any[]) => any> (fn: T, deps?: DependencyList): MemoizedFn<T> {
  return useMemo(() => {
    let state: MemoizedState<T> | null = null
    return (...args: Parameters<T>): ReturnType<T> => {
      if (state != null && isEqualShallow(state.args, args)) {
        return state.value
      } else {
        const value = fn(...args)
        state = { args, value }
        return value
      }
    }
  }, deps ?? [])
}
