import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function CaseIcon ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#5E8DB9'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg style={style} width={32} height={32} viewBox='0 0 32 32' fill='none' {...rest}>
      <Path
        d='M14.024 18.2561L11.2624 15.4945L9 17.7569L14.2385 22.9953L23.3617 12.0512L20.9009 10L14.024 18.2561Z'
        fill={color}
      />
      <Path
        d='M28.4444 0H3.55556C1.59467 0 0 1.59467 0 3.55556V28.4444C0 30.4053 1.59467 32 3.55556 32H28.4444C30.4053 32 32 30.4053 32 28.4444V3.55556C32 1.59467 30.4053 0 28.4444 0ZM3.55556 28.4444V3.55556H28.4444L28.448 28.4444H3.55556Z'
        fill={color}
      />
    </Svg>
  )
}
