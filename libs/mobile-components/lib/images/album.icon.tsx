import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function AlbumIcon ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#5E8DB9'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg style={style} width={32} height={32} viewBox='0 0 32 32' fill='none' {...rest}>
      <Path
        d='M28.4444 0H3.55556C1.59467 0 0 1.59467 0 3.55556V28.4444C0 30.4053 1.59467 32 3.55556 32H28.4444C30.4053 32 32 30.4053 32 28.4444V3.55556C32 1.59467 30.4053 0 28.4444 0ZM3.55556 28.4444V3.55556H28.4444L28.448 28.4444H3.55556Z'
        fill={color}
      />
      <Path
        d='M12.4446 19.5554L10.6668 17.7777L5.3335 24.8888H26.6668L17.7779 12.4443L12.4446 19.5554Z'
        fill={color}
      />
    </Svg>
  )
}
