import React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function ArrowIcon16 (
  {
    style,
    ...rest
  }: IconComponentProps
): React.ReactElement {
  const { width = 16, height = 16, color = '#000' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 16 16' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M9.04035 1.85861C9.42508 1.49221 9.43993 0.88329 9.07353 0.498562C8.70712 0.113834 8.0982 0.0989822 7.71347 0.46539L0.499538 7.3358C0.315042 7.51108 0.200012 7.75878 0.200012 8.03335C0.200012 8.30829 0.315355 8.55629 0.500292 8.73161L7.71347 15.6013C8.0982 15.9677 8.70712 15.9529 9.07353 15.5681C9.43993 15.1834 9.42508 14.5745 9.04035 14.2081L3.56697 8.99534H14.9047C15.436 8.99534 15.8667 8.56464 15.8667 8.03335C15.8667 7.50206 15.436 7.07136 14.9047 7.07136H3.56697L9.04035 1.85861Z'
        fill={color}
      />
    </Svg>
  )
}
