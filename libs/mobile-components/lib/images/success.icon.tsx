import * as React from 'react'
import { StyleProp, StyleSheet, ViewProps } from 'react-native'
import Svg, { Circle, Path } from 'react-native-svg'

interface SuccessIconStyleProps {
  width?: number
  height?: number
  color?: string
  fillColor?: string
}

export interface SuccessIconProps extends ViewProps {
  style?: StyleProp<SuccessIconStyleProps>
}

export function SuccessIcon ({
  style,
  ...rest
}: SuccessIconProps): React.ReactElement {
  const {
    width = 104,
    height = 104,
    color = '#006EFD',
    fillColor = '#EBF4FF'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 104 104' fill='none' {...rest}>
      <Circle cx='52' cy='52' r='52' fill={fillColor} />
      <Path
        d='M26 53.0833L48.2857 70.4167L78 35.75'
        stroke={color}
        strokeWidth='4.66667'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
