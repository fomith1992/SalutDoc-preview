import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function CheckMarkIcon20 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width,
    height
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 20 20' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M19.086 3.561c.397.34.443.937.103 1.334L9.318 16.411a.946.946 0 01-1.3.131L.616 10.785a.946.946 0 011.162-1.493l6.691 5.204 9.284-10.831a.946.946 0 011.334-.103z'
        fill={color}
        stroke={color}
        strokeWidth={0.3}
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
