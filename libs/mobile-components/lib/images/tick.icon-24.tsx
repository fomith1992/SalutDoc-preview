import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function TickIcon24 ({
  style,
  ...rest
}: IconComponentProps): React.ReactElement {
  const {
    width = 24,
    height = 24,
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none' {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M23.2949 4.27925C23.7813 4.69613 23.8376 5.42836 23.4207 5.91472L11.3178 20.0348C10.9149 20.5049 10.2138 20.5757 9.72505 20.1956L0.647829 13.1355C0.142187 12.7422 0.0510972 12.0135 0.444374 11.5079C0.837651 11.0022 1.56637 10.9111 2.07201 11.3044L10.2762 17.6855L21.6595 4.40506C22.0764 3.91869 22.8086 3.86237 23.2949 4.27925Z'
        fill={color}
        stroke={color}
        strokeWidth='0.3'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
