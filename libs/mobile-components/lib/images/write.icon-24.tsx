import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function WriteIcon24 ({
  style,
  ...rest
}: IconComponentProps): React.ReactElement {
  const {
    width = 24,
    height = 24,
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg
      width={width} height={height} viewBox='0 0 24 24' fill='none' {...rest}
    >
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M23.8 3.206a.815.815 0 00-.234-.572v-.001L21.49.537a.798.798 0 00-1.134 0L18.684 2.23l3.209 3.241 1.673-1.691a.815.815 0 00.234-.573zm-6.25.167l3.208 3.242v.001L9.828 17.66a.8.8 0 01-.314.195L5.64 19.159a.398.398 0 01-.517-.292.41.41 0 01.01-.22l1.291-3.913a.812.812 0 01.194-.318L17.55 3.373zM1.005 23.088A2.444 2.444 0 01.3 21.369V3.542c0-.645.254-1.263.705-1.719a2.394 2.394 0 011.701-.712h10.429c.212 0 .416.085.567.237a.814.814 0 010 1.146.798.798 0 01-.567.238H2.706a.798.798 0 00-.567.237.815.815 0 00-.235.573v17.827c0 .215.085.421.235.573.15.152.355.237.567.237h17.648a.798.798 0 00.567-.237.815.815 0 00.235-.573v-9.724c0-.215.085-.42.235-.573a.798.798 0 011.135 0c.15.152.235.358.235.573v9.724c0 .645-.254 1.263-.705 1.719a2.394 2.394 0 01-1.702.712H2.706c-.638 0-1.25-.256-1.701-.712z'
        fill={color}
      />
    </Svg>
  )
}
