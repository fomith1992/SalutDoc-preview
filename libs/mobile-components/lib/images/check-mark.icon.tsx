import * as React from 'react'
import { ViewProps, StyleProp, StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

interface CheckMarkIconStyleProps {
  width?: number
  height?: number
  color?: string
}

export interface CheckMarkIconProps extends ViewProps {
  style?: StyleProp<CheckMarkIconStyleProps>
}

export function CheckMarkIcon ({ style, ...rest }: CheckMarkIconProps): React.ReactElement {
  const { width = 10, height = 8, color = '#F6F6F6' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg
      width={width}
      height={height}
      viewBox='0 0 10 8'
      fill='none'
      {...rest}
    >
      <Path
        d='M1.66699 3.83325L4.88128 6.33325L9.16699 1.33325'
        fill='none'
        stroke={color}
        strokeWidth='1.58333'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
