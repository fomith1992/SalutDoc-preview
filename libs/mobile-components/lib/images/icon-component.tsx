import React from 'react'
import { StyleProp, ViewProps, ViewStyle } from 'react-native'

export interface IconComponentProps extends ViewProps {
  style?: StyleProp<ViewStyle & {
    color?: string
  }>
}

export type IconComponent = React.ComponentType<IconComponentProps>
