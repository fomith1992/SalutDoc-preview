import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function ListIcon20 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = '20',
    height = '20'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 20 20' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M5.895 15.221a.7.7 0 01.7-.7h12.589a.7.7 0 010 1.4H6.594a.7.7 0 01-.699-.7zm0-6.411a.7.7 0 01.7-.7h12.589a.7.7 0 010 1.4H6.594a.7.7 0 01-.699-.7zm0-6.411a.7.7 0 01.7-.7h12.589a.7.7 0 010 1.4H6.594a.7.7 0 01-.699-.7zM1.7 3.798a1.399 1.399 0 100-2.798 1.399 1.399 0 000 2.798zm0 6.41a1.399 1.399 0 100-2.796 1.399 1.399 0 000 2.797zm0 6.412a1.398 1.398 0 100-2.797 1.398 1.398 0 000 2.797z'
        fill={color}
        stroke={color}
        strokeWidth={0.1}
      />
    </Svg>
  )
}
