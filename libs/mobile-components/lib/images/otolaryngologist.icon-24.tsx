import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function OtolaryngologistIcon24 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = '24',
    height = '24'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M14.5417 23.5H14.4123C11.842 23.5 9.75 21.408 9.75 18.8377C9.75 16.4102 8.80508 14.1285 7.08871 12.4121L5.82754 11.1509C4.64879 9.97217 4 8.40625 4 6.73875C4 3.29833 6.79833 0.5 10.2388 0.5H12.625C16.3242 0.5 19.3333 3.50821 19.3333 7.20546V18.7103C19.3333 21.3514 17.1838 23.5 14.5417 23.5ZM10.2388 1.45833C7.32733 1.45833 4.95833 3.82733 4.95833 6.73875C4.95833 8.14942 5.50746 9.47575 6.50508 10.4724L7.76625 11.7336C9.66375 13.632 10.7083 16.1544 10.7083 18.8377C10.7083 20.8799 12.3701 22.5417 14.4123 22.5417H14.5417C16.6558 22.5417 18.375 20.8224 18.375 18.7093V7.20546C18.375 4.03625 15.7961 1.45833 12.625 1.45833H10.2388ZM14.5417 20.625C13.4846 20.625 12.625 19.7654 12.625 18.7083C12.625 18.4438 12.8397 18.2292 13.1042 18.2292C13.3687 18.2292 13.5833 18.4438 13.5833 18.7083C13.5833 19.2364 14.0136 19.6667 14.5417 19.6667C15.0697 19.6667 15.5 19.2364 15.5 18.7083C15.5 18.4438 15.7147 18.2292 15.9792 18.2292C16.2437 18.2292 16.4583 18.4438 16.4583 18.7083C16.4583 19.7654 15.5987 20.625 14.5417 20.625ZM15.8278 15.8084C15.8776 15.8257 15.9293 15.8333 15.9792 15.8333C16.1795 15.8333 16.3663 15.7068 16.4334 15.5056C16.5168 15.2555 16.3817 14.9833 16.1306 14.8999C16.1306 14.8999 15.6667 14.7447 15.1109 14.5674C14.1976 14.2751 13.5833 13.426 13.5833 12.4543V12.3239C13.5833 11.4327 14.1852 10.6497 15.0745 10.4111C15.9015 10.1351 16.4583 9.36267 16.4583 8.49058C16.4583 5.67021 14.1631 3.375 11.3428 3.375C8.87887 3.375 6.875 5.37888 6.875 7.84275V8.64583C6.875 8.91033 7.08967 9.125 7.35417 9.125C7.61867 9.125 7.83333 8.91033 7.83333 8.64583V7.84275C7.83333 5.90788 9.40788 4.33333 11.3428 4.33333C13.6351 4.33333 15.5 6.19825 15.5 8.49058C15.5 8.94963 15.2068 9.35692 14.7995 9.49396C13.5191 9.83608 12.625 11.0005 12.625 12.3249V12.4552C12.625 13.8448 13.5067 15.0609 14.8196 15.4807C15.2752 15.6254 15.6681 15.7556 15.7893 15.7957C15.8142 15.804 15.8276 15.8084 15.8278 15.8084Z'
        fill={color}
      />
      <Path
        d='M7.08871 12.4121L6.80587 12.6949L7.08871 12.4121ZM5.82754 11.1509L6.11038 10.8681L5.82754 11.1509ZM6.50508 10.4724L6.78793 10.1896L6.78779 10.1894L6.50508 10.4724ZM7.76625 11.7336L8.04916 11.4508L8.04909 11.4507L7.76625 11.7336ZM15.8278 15.8084L15.9586 15.4304L15.895 15.4084H15.8278V15.8084ZM16.1306 14.8999L16.0036 15.2792L16.0045 15.2795L16.1306 14.8999ZM15.1109 14.5674L14.989 14.9483L14.9894 14.9485L15.1109 14.5674ZM15.0745 10.4111L15.1782 10.7974L15.1897 10.7943L15.2011 10.7905L15.0745 10.4111ZM14.7995 9.49396L14.9027 9.8804L14.915 9.87712L14.927 9.87307L14.7995 9.49396ZM14.8196 15.4807L14.6978 15.8617L14.6985 15.8619L14.8196 15.4807ZM15.7893 15.7957L15.6635 16.1754L15.6635 16.1754L15.7893 15.7957ZM14.4123 23.9H14.5417V23.1H14.4123V23.9ZM9.35 18.8377C9.35 21.6289 11.6211 23.9 14.4123 23.9V23.1C12.063 23.1 10.15 21.187 10.15 18.8377H9.35ZM6.80587 12.6949C8.44721 14.3363 9.35 16.5163 9.35 18.8377H10.15C10.15 16.3042 9.16296 13.9207 7.37155 12.1292L6.80587 12.6949ZM5.5447 11.4338L6.80587 12.6949L7.37155 12.1292L6.11038 10.8681L5.5447 11.4338ZM3.6 6.73875C3.6 8.51235 4.29094 10.18 5.5447 11.4338L6.11038 10.8681C5.00664 9.76433 4.4 8.30015 4.4 6.73875H3.6ZM10.2388 0.1C6.57742 0.1 3.6 3.07742 3.6 6.73875H4.4C4.4 3.51925 7.01925 0.9 10.2388 0.9V0.1ZM12.625 0.1H10.2388V0.9H12.625V0.1ZM19.7333 7.20546C19.7333 3.28715 16.5449 0.1 12.625 0.1V0.9C16.1034 0.9 18.9333 3.72927 18.9333 7.20546H19.7333ZM19.7333 18.7103V7.20546H18.9333V18.7103H19.7333ZM14.5417 23.9C17.4045 23.9 19.7333 21.5725 19.7333 18.7103H18.9333C18.9333 21.1303 16.963 23.1 14.5417 23.1V23.9ZM5.35833 6.73875C5.35833 4.04825 7.54825 1.85833 10.2388 1.85833V1.05833C7.10642 1.05833 4.55833 3.60642 4.55833 6.73875H5.35833ZM6.78779 10.1894C5.86536 9.2679 5.35833 8.04341 5.35833 6.73875H4.55833C4.55833 8.25543 5.14955 9.6836 6.22238 10.7554L6.78779 10.1894ZM8.04909 11.4507L6.78793 10.1896L6.22224 10.7553L7.48341 12.0164L8.04909 11.4507ZM11.1083 18.8377C11.1083 16.0483 10.0216 13.4243 8.04916 11.4508L7.48334 12.0164C9.30589 13.8398 10.3083 16.2605 10.3083 18.8377H11.1083ZM14.4123 22.1417C12.591 22.1417 11.1083 20.659 11.1083 18.8377H10.3083C10.3083 21.1008 12.1492 22.9417 14.4123 22.9417V22.1417ZM14.5417 22.1417H14.4123V22.9417H14.5417V22.1417ZM17.975 18.7093C17.975 20.6014 16.4349 22.1417 14.5417 22.1417V22.9417C16.8766 22.9417 18.775 21.0434 18.775 18.7093H17.975ZM17.975 7.20546V18.7093H18.775V7.20546H17.975ZM12.625 1.85833C15.5754 1.85833 17.975 4.25734 17.975 7.20546H18.775C18.775 3.81516 16.0169 1.05833 12.625 1.05833V1.85833ZM10.2388 1.85833H12.625V1.05833H10.2388V1.85833ZM12.225 18.7083C12.225 19.9863 13.2637 21.025 14.5417 21.025V20.225C13.7055 20.225 13.025 19.5445 13.025 18.7083H12.225ZM13.1042 17.8292C12.6188 17.8292 12.225 18.2229 12.225 18.7083H13.025C13.025 18.6647 13.0606 18.6292 13.1042 18.6292V17.8292ZM13.9833 18.7083C13.9833 18.2229 13.5896 17.8292 13.1042 17.8292V18.6292C13.1478 18.6292 13.1833 18.6647 13.1833 18.7083H13.9833ZM14.5417 19.2667C14.2345 19.2667 13.9833 19.0155 13.9833 18.7083H13.1833C13.1833 19.4573 13.7927 20.0667 14.5417 20.0667V19.2667ZM15.1 18.7083C15.1 19.0155 14.8488 19.2667 14.5417 19.2667V20.0667C15.2906 20.0667 15.9 19.4573 15.9 18.7083H15.1ZM15.9792 17.8292C15.4938 17.8292 15.1 18.2229 15.1 18.7083H15.9C15.9 18.6647 15.9356 18.6292 15.9792 18.6292V17.8292ZM16.8583 18.7083C16.8583 18.2229 16.4646 17.8292 15.9792 17.8292V18.6292C16.0228 18.6292 16.0583 18.6647 16.0583 18.7083H16.8583ZM14.5417 21.025C15.8196 21.025 16.8583 19.9863 16.8583 18.7083H16.0583C16.0583 19.5445 15.3778 20.225 14.5417 20.225V21.025ZM15.9792 15.4333C15.9697 15.4333 15.9625 15.4318 15.9586 15.4304L15.6969 16.1864C15.7926 16.2195 15.889 16.2333 15.9792 16.2333V15.4333ZM16.0539 15.3791C16.043 15.412 16.0126 15.4333 15.9792 15.4333V16.2333C16.3463 16.2333 16.6897 16.0017 16.8129 15.6321L16.0539 15.3791ZM16.0045 15.2795C16.0447 15.2929 16.0677 15.3377 16.0539 15.3791L16.8129 15.6321C16.9658 15.1732 16.7186 14.6737 16.2566 14.5203L16.0045 15.2795ZM14.9894 14.9485C15.2658 15.0366 15.5196 15.1194 15.7042 15.1801C15.7965 15.2105 15.8715 15.2353 15.9234 15.2525C15.9493 15.2611 15.9694 15.2678 15.983 15.2724C15.9898 15.2746 15.995 15.2764 15.9985 15.2775C16.0002 15.2781 16.0015 15.2785 16.0024 15.2788C16.0028 15.279 16.0031 15.2791 16.0033 15.2791C16.0034 15.2792 16.0035 15.2792 16.0036 15.2792C16.0036 15.2792 16.0036 15.2792 16.0036 15.2792C16.0036 15.2792 16.0036 15.2792 16.0036 15.2792C16.0036 15.2792 16.0036 15.2792 16.1306 14.8999C16.2575 14.5206 16.2575 14.5206 16.2575 14.5206C16.2575 14.5206 16.2575 14.5206 16.2575 14.5206C16.2575 14.5206 16.2575 14.5206 16.2574 14.5206C16.2574 14.5205 16.2573 14.5205 16.2572 14.5205C16.2569 14.5204 16.2566 14.5203 16.2561 14.5201C16.2552 14.5198 16.2538 14.5193 16.252 14.5187C16.2484 14.5175 16.2431 14.5158 16.2361 14.5134C16.2222 14.5088 16.2017 14.502 16.1755 14.4933C16.123 14.4758 16.0473 14.4508 15.9541 14.4201C15.7679 14.3589 15.5118 14.2754 15.2325 14.1863L14.9894 14.9485ZM13.1833 12.4543C13.1833 13.597 13.9072 14.6021 14.989 14.9483L15.2328 14.1864C14.488 13.948 13.9833 13.255 13.9833 12.4543H13.1833ZM13.1833 12.3239V12.4543H13.9833V12.3239H13.1833ZM14.9708 10.0247C13.9105 10.3093 13.1833 11.2478 13.1833 12.3239H13.9833C13.9833 11.6176 14.4598 10.9902 15.1782 10.7974L14.9708 10.0247ZM16.0583 8.49058C16.0583 9.19048 15.6117 9.81012 14.9479 10.0317L15.2011 10.7905C16.1914 10.46 16.8583 9.53485 16.8583 8.49058H16.0583ZM11.3428 3.775C13.9422 3.775 16.0583 5.89112 16.0583 8.49058H16.8583C16.8583 5.4493 14.384 2.975 11.3428 2.975V3.775ZM7.275 7.84275C7.275 5.59979 9.09979 3.775 11.3428 3.775V2.975C8.65796 2.975 6.475 5.15796 6.475 7.84275H7.275ZM7.275 8.64583V7.84275H6.475V8.64583H7.275ZM7.35417 8.725C7.31058 8.725 7.275 8.68942 7.275 8.64583H6.475C6.475 9.13125 6.86875 9.525 7.35417 9.525V8.725ZM7.43333 8.64583C7.43333 8.68942 7.39775 8.725 7.35417 8.725V9.525C7.83958 9.525 8.23333 9.13125 8.23333 8.64583H7.43333ZM7.43333 7.84275V8.64583H8.23333V7.84275H7.43333ZM11.3428 3.93333C9.18696 3.93333 7.43333 5.68696 7.43333 7.84275H8.23333C8.23333 6.12879 9.62879 4.73333 11.3428 4.73333V3.93333ZM15.9 8.49058C15.9 5.97734 13.856 3.93333 11.3428 3.93333V4.73333C13.4142 4.73333 15.1 6.41916 15.1 8.49058H15.9ZM14.927 9.87307C15.5029 9.67931 15.9 9.11518 15.9 8.49058H15.1C15.1 8.78407 14.9106 9.03453 14.6719 9.11484L14.927 9.87307ZM13.025 12.3249C13.025 11.1817 13.7973 10.1758 14.9027 9.8804L14.6962 9.10752C13.2409 9.49639 12.225 10.8192 12.225 12.3249H13.025ZM13.025 12.4552V12.3249H12.225V12.4552H13.025ZM14.9414 15.0997C13.7962 14.7335 13.025 13.6729 13.025 12.4552H12.225C12.225 14.0167 13.2171 15.3883 14.6978 15.8617L14.9414 15.0997ZM15.9151 15.416C15.7936 15.3758 15.3987 15.2449 14.9407 15.0994L14.6985 15.8619C15.1516 16.0058 15.5427 16.1354 15.6635 16.1754L15.9151 15.416ZM15.8278 15.4084C15.8525 15.4084 15.8717 15.4108 15.8794 15.4118C15.8891 15.413 15.8968 15.4144 15.9015 15.4153C15.9108 15.417 15.918 15.4187 15.9213 15.4195C15.9283 15.4212 15.9338 15.4227 15.9356 15.4232C15.94 15.4245 15.9435 15.4255 15.9443 15.4258C15.9467 15.4265 15.9485 15.4271 15.9488 15.4272C15.9497 15.4275 15.9501 15.4276 15.9494 15.4274C15.9484 15.427 15.9464 15.4264 15.9432 15.4253C15.937 15.4233 15.9276 15.4202 15.9151 15.416L15.6635 16.1754C15.6758 16.1795 15.6856 16.1828 15.6923 16.185C15.6956 16.1861 15.6986 16.187 15.7009 16.1878C15.7019 16.1881 15.7036 16.1887 15.7054 16.1892C15.706 16.1895 15.7082 16.1901 15.7108 16.1909C15.7118 16.1912 15.7153 16.1923 15.7198 16.1936C15.7217 16.1941 15.7271 16.1956 15.7342 16.1973C15.7375 16.1981 15.7447 16.1998 15.754 16.2016C15.7586 16.2024 15.7664 16.2038 15.7761 16.2051C15.7837 16.2061 15.803 16.2084 15.8278 16.2084V15.4084Z'
        fill={color}
      />
    </Svg>
  )
}
