import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function TickFilledIcon24 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = 24,
    height = 24
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg style={style} width={width} height={height} viewBox='0 0 24 24' fill='none' {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M12.2032 23.5C18.6925 23.5 23.9532 18.2393 23.9532 11.75C23.9532 5.26065 18.6925 0 12.2032 0C5.71384 0 0.453186 5.26065 0.453186 11.75C0.453186 18.2393 5.71384 23.5 12.2032 23.5ZM17.7727 9.23811C18.0422 8.92361 18.0058 8.45014 17.6913 8.18057C17.3768 7.911 16.9033 7.94743 16.6338 8.26192L11.9563 13.7189L8.66368 11.158C8.33672 10.9037 7.86551 10.9626 7.61121 11.2896C7.3569 11.6165 7.4158 12.0877 7.74276 12.342L11.5999 15.342C11.9159 15.5878 12.3693 15.5421 12.6298 15.2381L17.7727 9.23811Z'
        fill={color}
      />

    </Svg>
  )
}
