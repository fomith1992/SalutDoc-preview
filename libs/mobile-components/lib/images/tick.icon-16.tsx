import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function TickIcon16 ({
  style,
  ...rest
}: IconComponentProps): React.ReactElement {
  const {
    width = 16,
    height = 16,
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 16 16' fill='none' {...rest}>
      <Path
        fillRule='evenodd' clipRule='evenodd' d='M15.269 2.84887C15.5864 3.12088 15.6231 3.59864 15.3511 3.91599L7.45416 13.1291C7.19126 13.4358 6.73381 13.482 6.41493 13.234L0.492206 8.62741C0.162284 8.37081 0.102849 7.89533 0.359455 7.56541C0.616062 7.23549 1.09154 7.17605 1.42146 7.43266L6.77458 11.5962L14.2019 2.93096C14.4739 2.61361 14.9517 2.57686 15.269 2.84887Z'
        fill={color}
        stroke={color}
        strokeWidth='0.3'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
