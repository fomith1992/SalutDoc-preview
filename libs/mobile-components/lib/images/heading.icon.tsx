import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function HeadingIcon ({
  style,
  ...rest
}: IconComponentProps): React.ReactElement {
  const {
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg {...rest} style={style} viewBox='0 0 17 16'>
      <Path
        d='M15.022 14.281L8.96.781h-.925l-5.812 13.5H.5v.938h5v-.938H3.243l1.494-3.469h5.512l1.558 3.47H10.25v.937h6.25v-.938h-1.478zM5.141 9.875l2.294-5.33 2.393 5.33H5.141zm7.694 4.406L7.938 3.378l.568-1.319 5.488 12.222h-1.16z'
        fill={color}
      />
    </Svg>
  )
}
