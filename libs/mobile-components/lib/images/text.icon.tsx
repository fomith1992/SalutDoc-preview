import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function TextIcon ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg {...rest} style={style} viewBox='0 0 16 16'>
      <Path
        d='M6 14.672a.5.5 0 01-.5-.5V1.813a.5.5 0 011 0v12.36a.5.5 0 01-.5.5z'
        fill={color}
      />
      <Path
        d='M11.5 3.983a.5.5 0 01-.5-.5v-1.17H1v1.17a.5.5 0 01-1 0v-1.67a.5.5 0 01.5-.5h11a.5.5 0 01.5.5v1.67a.5.5 0 01-.5.5zM7.5 14.672h-3a.5.5 0 010-1h3a.5.5 0 010 1zM9.833 9.495a.5.5 0 01-.5-.5v-1.17a.5.5 0 01.5-.5H15.5a.5.5 0 01.5.5v1.002a.5.5 0 01-1 0v-.502h-4.667v.67a.5.5 0 01-.5.5z'
        fill={color}
      />
      <Path
        d='M12.667 14.672a.5.5 0 01-.5-.5V7.825a.5.5 0 011 0v6.347a.5.5 0 01-.5.5z'
        fill={color}
      />
      <Path
        d='M13.5 14.672h-1.667a.5.5 0 010-1H13.5a.5.5 0 010 1z'
        fill={color}
      />
    </Svg>
  )
}
