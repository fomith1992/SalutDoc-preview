import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function LikeIcon20 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = '20',
    height = '20'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 20 20' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M10.0418 4.19694L9.16423 3.29486C7.10425 1.17735 3.327 1.90807 1.96346 4.57026C1.32332 5.82241 1.17888 7.63025 2.3478 9.93748C3.47388 12.159 5.8166 14.82 10.0418 17.7184C14.2671 14.82 16.6086 12.159 17.7359 9.93748C18.9048 7.62903 18.7616 5.82241 18.1202 4.57026C16.7567 1.90807 12.9794 1.17612 10.9194 3.29363L10.0418 4.19694ZM10.0418 19.1933C-8.72569 6.79181 4.26335 -2.88754 9.82641 2.23243C9.89985 2.29975 9.97207 2.36952 10.0418 2.44173C10.1109 2.36958 10.1828 2.30017 10.2573 2.23365C15.8191 -2.88999 28.8094 6.79059 10.0418 19.1933Z'
        fill={color}
        stroke={color}
        stroke-width='0.25'
      />
    </Svg>
  )
}
