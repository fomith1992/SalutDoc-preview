import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function LikeFilledIcon24 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = '24',
    height = '24'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd' clipRule='evenodd' d='M12.2449 2.6789C5.5692 -3.46507 -10.0177 8.15016 12.5034 23.032C35.0244 8.14869 19.4361 -3.468 12.7619 2.68037C12.6725 2.76019 12.5862 2.84349 12.5034 2.93007C12.4197 2.84341 12.333 2.75969 12.2449 2.6789Z'
        fill={color}
      />
    </Svg>
  )
}
