import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { ViewProps } from 'react-native'

export function PlusDropIcon ({ ...rest }: ViewProps): React.ReactElement {
  return (
    <Svg width={26} height={26} viewBox='0 0 26 26' fill='none' {...rest}>
      <Path
        d='M0 13C0 5.8203 5.8203 0 13 0C20.1797 0 26 5.8203 26 13C26 20.1797 20.1797 26 13 26H0V13Z'
        fill='#5E8DB9'
      />
      <Path
        d='M18.75 11.75H14.5C14.3619 11.75 14.25 11.6381 14.25 11.5V7.25C14.25 6.55969 13.6903 6 13 6C12.3097 6 11.75 6.55969 11.75 7.25V11.5C11.75 11.6381 11.6381 11.75 11.5 11.75H7.25C6.55969 11.75 6 12.3097 6 13C6 13.6903 6.55969 14.25 7.25 14.25H11.5C11.6381 14.25 11.75 14.3619 11.75 14.5V18.75C11.75 19.4403 12.3097 20 13 20C13.6903 20 14.25 19.4403 14.25 18.75V14.5C14.25 14.3619 14.3619 14.25 14.5 14.25H18.75C19.4403 14.25 20 13.6903 20 13C20 12.3097 19.4403 11.75 18.75 11.75Z'
        fill='#ffffff'
      />
    </Svg>
  )
}
