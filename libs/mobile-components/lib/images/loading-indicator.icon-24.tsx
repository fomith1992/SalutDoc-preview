import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function LoadingIndicatorIcon24 ({ style }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = 24,
    height = 24
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none'>
      <Path
        d='M12.05 23.8C18.4197 23.8 23.8 18.4197 23.8 12.05H21.45C21.45 17.146 17.146 21.45 12.05 21.45C6.95407 21.45 2.65005 17.146 2.65005 12.05C2.65005 6.95519 6.95407 2.64999 12.05 2.64999V0.299988C5.68037 0.299988 0.300049 5.68149 0.300049 12.05C0.300049 18.4197 5.68037 23.8 12.05 23.8Z'
        fill={color}
        stroke={color}
        strokeWidth='0.1'
      />
    </Svg>
  )
}
