import * as React from 'react'
import Svg, { Path, Rect } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function ChatWithPlusIcon24 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = '24',
    height = '24'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none' style={style} {...rest}>
      <Path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M4.23332 17.0006C4.38577 17.1536 4.50259 17.3384 4.57545 17.5417C4.64831 17.7451 4.67542 17.962 4.65485 18.177C4.55162 19.1722 4.35592 20.1556 4.07028 21.1145C6.11919 20.6401 7.37057 20.0908 7.93897 19.8029C8.26137 19.6396 8.63265 19.601 8.98178 19.6943C9.98264 19.9611 11.0142 20.0954 12.05 20.0938C17.9191 20.0938 22.3313 15.971 22.3313 11.2812C22.3313 6.593 17.9191 2.46875 12.05 2.46875C6.18088 2.46875 1.76875 6.593 1.76875 11.2812C1.76875 13.4374 2.67497 15.4378 4.23332 17.0006ZM3.50922 22.736C3.16123 22.805 2.81211 22.8682 2.462 22.9255C2.16825 22.9725 1.945 22.667 2.06103 22.3938C2.19145 22.0863 2.31098 21.7742 2.41941 21.4582L2.42382 21.4435C2.78807 20.386 3.08475 19.1699 3.19344 18.0375C1.39128 16.2309 0.300003 13.8663 0.300003 11.2812C0.300003 5.60306 5.56107 1 12.05 1C18.5389 1 23.8 5.60306 23.8 11.2812C23.8 16.9594 18.5389 21.5625 12.05 21.5625C10.8862 21.5641 9.7273 21.413 8.60285 21.1131C7.8391 21.4993 6.19557 22.2029 3.50922 22.736Z'
        fill={color}
        stroke={color}
        strokeWidth='0.2'
      />
      <Rect x='11' y='7' width='2' height='9' rx='1' fill={color} />
      <Rect x='7.5' y='12.5' width='2' height='9' rx='1' transform='rotate(-90 7.5 12.5)' fill={color} />
    </Svg>
  )
}
