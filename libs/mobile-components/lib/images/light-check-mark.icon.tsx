import * as React from 'react'
import { StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { IconComponentProps } from './icon-component'

export function LightCheckMarkIcon ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const { width = 10, height = 8, color = '#F6F6F6' } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg
      width={width}
      height={height}
      viewBox='0 0 14 10'
      fill='none'
      {...rest}
    >
      <Path
        d='M1 5l5.143 4L13 1'
        stroke={color}
        strokeWidth={1.5}
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
