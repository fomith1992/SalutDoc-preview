import * as React from 'react'
import { ViewProps, StyleProp, StyleSheet } from 'react-native'
import Svg, { Path } from 'react-native-svg'

interface QuestionCardStyleProps {
  width?: number
  height?: number
}

export interface QuestionCardIconProps extends ViewProps {
  color?: string
  style?: StyleProp<QuestionCardStyleProps>
}

export function QuestionCardIcon ({
  color = '#F6F6F6',
  style = { width: 86, height: 90 },
  ...rest
}: QuestionCardIconProps): React.ReactElement {
  const { width, height } = StyleSheet.flatten(style)
  return (
    <Svg width={width} height={height} viewBox='0 0 86 90' fill='none' {...rest}>
      <Path
        d='M30.9961 70.5312C27.9878 70.5312 25.5522 73.0383 25.5522 76.0468C25.5522 78.9835 27.9161 81.5623 30.9961 81.5623C34.0761 81.5623 36.5117 78.9837 36.5117 76.0468C36.5117 73.0383 34.0048 70.5312 30.9961 70.5312Z'
        fill={color}
      />
      <Path
        d='M31.9271 28.7701C22.2569 28.7701 17.8159 34.5005 17.8159 38.3685C17.8159 41.1623 20.1796 42.4516 22.1137 42.4516C25.9816 42.4516 24.4058 36.936 31.7121 36.936C35.2937 36.936 38.159 38.512 38.159 41.8069C38.159 45.6752 34.1477 47.8956 31.7838 49.9013C29.7067 51.692 26.9846 54.629 26.9846 60.7892C26.9846 64.5139 27.9873 65.5884 30.9242 65.5884C34.4341 65.5884 35.1503 64.0127 35.1503 62.6516C35.1503 58.9268 35.2222 56.7779 39.1618 53.6979C41.0958 52.1936 47.1845 47.3227 47.1845 40.5894C47.1845 33.856 41.0958 28.7701 31.9271 28.7701Z'
        fill={color}
      />
      <Path
        d='M32.6075 6.10352e-05C3.53282 6.10352e-05 -20 23.529 -20 52.6076V101.105C-20 103.375 -18.16 105.215 -15.89 105.215H32.6075C61.682 105.215 85.215 81.6862 85.215 52.6076C85.215 23.5329 61.6861 6.10352e-05 32.6075 6.10352e-05ZM32.6075 96.9952H-11.7801V52.6076C-11.7801 28.0758 8.07248 8.21999 32.6075 8.21999C57.1393 8.21999 76.9951 28.0725 76.9951 52.6076C76.9951 77.1393 57.1425 96.9952 32.6075 96.9952Z'
        fill={color}
      />
    </Svg>
  )
}
