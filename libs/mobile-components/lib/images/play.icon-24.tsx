import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function PlayIcon24 ({
  style,
  ...rest
}: IconComponentProps): React.ReactElement {
  const {
    width = 24,
    height = 24,
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none' {...rest}>
      <Path
        d='M21.8312 13.569L5.21905 23.2078C3.80925 24.025 2 23.0355 2 21.3881V2.11039C2 0.465621 3.80664 -0.526462 5.21905 0.293312L21.8312 9.93218C22.1519 10.1153 22.4185 10.3799 22.6039 10.6992C22.7893 11.0186 22.887 11.3813 22.887 11.7506C22.887 12.1198 22.7893 12.4825 22.6039 12.8019C22.4185 13.1212 22.1519 13.3859 21.8312 13.569Z'
        fill={color}
      />
    </Svg>
  )
}
