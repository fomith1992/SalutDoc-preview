import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleProp, ViewProps, ViewStyle } from 'react-native'

export interface PencilIconProps extends ViewProps {
  style?: StyleProp<ViewStyle>
}

export function PencilIcon ({ style, ...rest }: PencilIconProps): React.ReactElement {
  return (
    <Svg style={style} width={15} height={15} viewBox='0 0 15 15' fill='none' {...rest}>
      <Path
        d='M14 3.5L11.5 1L1.5 11L0.5 14.5L4 13.5L14 3.5ZM9.5 3L12 5.5L9.5 3ZM1.5 11L4 13.5L1.5 11Z'
        stroke='black'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </Svg>
  )
}
