import * as React from 'react'
import { StyleProp, StyleSheet, ViewProps, ViewStyle } from 'react-native'
import Svg, { Path } from 'react-native-svg'

export interface TickIcon56Props extends ViewProps {
  style?: StyleProp<ViewStyle & {
    color?: string
    width?: number
    height?: number
  }>
}

export function TickIcon56 ({
  style,
  ...rest
}: TickIcon56Props): React.ReactElement {
  const {
    width = 56,
    height = 56,
    color = '#000'
  } = StyleSheet.flatten(style) ?? {}
  return (
    <Svg width={width} height={height} viewBox='0 0 56 56' fill='none' {...rest}>
      <Path
        fill-rule='evenodd'
        clip-rule='evenodd'
        d='M54.368 9.99651C55.5231 10.9866 55.6569 12.7255 54.6668 13.8806L25.9234 47.4146C24.9665 48.531 23.3015 48.699 22.1408 47.7963L1.56356 31.0293C0.362707 30.0953 0.146377 28.3646 1.08037 27.1638C2.01437 25.9629 3.745 25.7466 4.94586 26.6806L23.4499 41.835L50.4839 10.2953C51.474 9.14022 53.213 9.00645 54.368 9.99651Z'
        fill={color}
      />
    </Svg>
  )
}
