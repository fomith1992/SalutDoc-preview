import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
import { StyleSheet } from 'react-native'
import { IconComponentProps } from './icon-component'

export function StarFilledIcon24 ({ style, ...rest }: IconComponentProps): React.ReactElement {
  const {
    color = '#000',
    width = '24',
    height = '24'
  } = StyleSheet.flatten(style) ?? {}

  return (
    <Svg width={width} height={height} viewBox='0 0 24 24' fill='none' {...rest}>
      <Path
        d='M6.05622 22.951C5.48929 23.2418 4.84599 22.7321 4.96055 22.0814L6.17959 15.1333L1.00528 10.2036C0.522075 9.74234 0.773227 8.89918 1.42093 8.8081L8.61473 7.78573L11.8224 1.42966C12.1118 0.856779 12.8946 0.856779 13.1839 1.42966L16.3916 7.78573L23.5854 8.8081C24.2331 8.89918 24.4843 9.74234 24.0011 10.2036L18.8268 15.1333L20.0458 22.0814C20.1604 22.7321 19.5171 23.2418 18.9501 22.951L12.501 19.6371L6.05475 22.951H6.05622Z'
        fill={color}
      />
    </Svg>
  )
}
