import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { FlatList } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { UserPreviewRow } from '../../../components/preview-row/user-preview-row.container'
import { useListUserFolloweesQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'

export interface UserFolloweesProps {
  userId: string
}

export function UserFollowees ({ userId }: UserFolloweesProps): React.ReactElement {
  const { loading, data, fetchMore, refetch } = useListUserFolloweesQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      userId
    }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousFollowees = expectDefined(previousResult.userById?.followees)
        return {
          userById: {
            __typename: 'User',
            id: userId,
            followees: appendEdges(previousFollowees, fetchMoreResult?.userById?.followees)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  })
  const followees = data?.userById?.followees
  const pageInfo = followees?.pageInfo
  return (
    <FlatList
      data={followees?.edges ?? []}
      keyExtractor={item => item.node.followee.id}
      renderItem={({ item }) => (
        <UserPreviewRow
          userId={item.node.followee.id}
        />
      )}
      onRefresh={refresh}
      refreshing={refreshing}
      onEndReached={() =>
        pageInfo?.hasNextPage === true && pageInfo?.endCursor != null ? loadMore(pageInfo?.endCursor) : null}
      ListFooterComponent={
        <LoadingIndicator visible={loading && !refreshing} />
      }
    />
  )
}
