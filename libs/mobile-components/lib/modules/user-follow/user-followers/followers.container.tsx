import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { FlatList } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { UserPreviewRow } from '../../../components/preview-row/user-preview-row.container'
import { useListUserFollowersQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { SmallUserFollowButton } from '../user-follow-button'

export interface UserFollowersProps {
  userId: string
}

export function UserFollowers ({ userId }: UserFollowersProps): React.ReactElement {
  const { data, loading, fetchMore, refetch } = useListUserFollowersQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      userId
    }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousFollowers = expectDefined(previousResult.userById?.followers)
        return {
          userById: {
            __typename: 'User',
            id: userId,
            followers: appendEdges(previousFollowers, fetchMoreResult?.userById?.followers)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  })

  const followers = data?.userById?.followers
  const pageInfo = followers?.pageInfo
  return (
    <FlatList
      data={followers?.edges ?? []}
      keyExtractor={item => item.node.follower.id}
      renderItem={({ item }) => (
        <UserPreviewRow
          userId={item.node.follower.id}
          action={<SmallUserFollowButton followeeId={item.node.follower.id} />}
        />
      )}
      onRefresh={refresh}
      refreshing={refreshing}
      onEndReached={() =>
        pageInfo?.hasNextPage === true && pageInfo?.endCursor != null ? loadMore(pageInfo?.endCursor) : null}
      ListFooterComponent={
        <LoadingIndicator visible={loading && !refreshing} />
      }
    />
  )
}
