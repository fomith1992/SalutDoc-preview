import React from 'react'
import { Alert } from 'react-native'
import { StackActions } from '@react-navigation/native'
import { useAuthenticatedUserOrNull } from '../../../components/user-context/user-context-provider'
import { useFollowUserMutation, useGetFollowStatusQuery, useUnfollowUserMutation } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { BigUserFollowButtonView } from './big-user-follow-button.view'
import { SmallUserFollowButtonView } from './small-user-follow-button.view'

export interface UserFollowButtonProps {
  followeeId: string
}

export type FollowState = 'followed' | 'unfollowed' | 'unknown'

export interface UserFollowButtonViewProps {
  state: FollowState
  loading: boolean
  onUnfollow: () => void
  onFollow: () => void
}

function AuthUserFollowButton (
  props: {
    followerId: string
    followeeId: string
    View: React.ComponentType<UserFollowButtonViewProps>
  }
): React.ReactElement {
  const { followerId, followeeId, View } = props
  const { data, loading } = useGetFollowStatusQuery({
    variables: { followeeId }
  })
  const followerByUserId = data?.userById?.followerByUserId
  const [follow, { loading: following }] = useFollowUserMutation({
    variables: {
      followerId,
      followeeId
    },
    onCompleted: (data) => {
      const { message, errors, follow } = data.userFollow
      if (follow == null) {
        console.error(message, errors)
        Alert.alert('Ошибка', 'Не удалось подписаться на пользователя')
      }
    },
    onError: (error) => {
      console.error(error)
      Alert.alert('Ошибка', 'Не удалось подписаться на пользователя')
    }
  })
  const [unfollow, { loading: unfollowing }] = useUnfollowUserMutation({
    variables: {
      followerId,
      followeeId
    },
    onCompleted: (data) => {
      const { message, errors, follow } = data.userUnfollow
      if (follow == null) {
        console.error(message, errors)
        Alert.alert('Ошибка', 'Не удалось отписаться от пользователя')
      }
    },
    onError: (error) => {
      console.error(error)
      Alert.alert('Ошибка', 'Не удалось отписаться от пользователя')
    }
  })

  let state: FollowState
  switch (followerByUserId?.status) {
    case 'ACTIVE':
      state = 'followed'
      break
    case 'INACTIVE':
      state = 'unfollowed'
      break
    default:
      state = 'unknown'
      break
  }
  return (
    <View
      state={state}
      loading={loading || following || unfollowing}
      onFollow={follow}
      onUnfollow={unfollow}
    />
  )
}

function withUserFollowButton (
  View: React.ComponentType<UserFollowButtonViewProps>
): (props: UserFollowButtonProps) => React.ReactElement {
  return ({ followeeId }) => {
    const navigation = useStackNavigation()
    const user = useAuthenticatedUserOrNull()
    if (user == null) {
      return (
        <View
          state='unfollowed'
          loading={false}
          onFollow={() => navigation.dispatch(StackActions.push('CreateAccountCard', {}))}
          onUnfollow={() => navigation.dispatch(StackActions.push('CreateAccountCard', {}))}
        />
      )
    }
    return (
      <AuthUserFollowButton
        View={View}
        followeeId={followeeId}
        followerId={user?.id}
      />
    )
  }
}

export const UserFollowButton = withUserFollowButton(BigUserFollowButtonView)
export const SmallUserFollowButton = withUserFollowButton(SmallUserFollowButtonView)
