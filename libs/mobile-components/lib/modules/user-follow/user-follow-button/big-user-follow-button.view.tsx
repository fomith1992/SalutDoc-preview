import React from 'react'
import { TextButton } from '../../../modules/ui-kit'
import { UserFollowButtonViewProps } from './user-follow-button.container'

export function BigUserFollowButtonView (
  {
    state,
    onFollow,
    onUnfollow
  }: UserFollowButtonViewProps
): React.ReactElement {
  switch (state) {
    case 'followed':
      return (
        <TextButton
          type='primary'
          size='M'
          onPress={onUnfollow}
        >
          Вы подписаны
        </TextButton>
      )
    case 'unfollowed':
      return (
        <TextButton
          type='secondary'
          size='M'
          onPress={onFollow}
        >
          Подписаться
        </TextButton>
      )
    case 'unknown':
      return <></>
  }
}
