import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { TouchableOpacity } from 'react-native'
import { SubscribeIcon } from '../../../images/subscribe.icon'

export const SubscribeButtonContainer = styled(TouchableOpacity, {
  paddingVertical: sp(4)
})

export const SubscribeButtonIcon = styled(SubscribeIcon, {
  width: sp(24),
  height: sp(24)
})
