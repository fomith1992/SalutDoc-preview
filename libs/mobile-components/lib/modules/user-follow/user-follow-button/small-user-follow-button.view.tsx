import React from 'react'
import { SubscribeButtonContainer, SubscribeButtonIcon } from './user-follow-button.style'
import { UserFollowButtonViewProps } from './user-follow-button.container'
import { hitSlopParams } from '../../ui-kit'

export function SmallUserFollowButtonView (
  {
    state,
    onFollow
  }: UserFollowButtonViewProps
): React.ReactElement {
  switch (state) {
    case 'followed':
      return <></>
    case 'unfollowed':
      return (
        <SubscribeButtonContainer
          hitSlop={hitSlopParams(16)}
          onPress={onFollow}
        >
          <SubscribeButtonIcon />
        </SubscribeButtonContainer>)
    case 'unknown':
      return <></>
  }
}
