import appsFlyer from 'react-native-appsflyer'
import { firebase } from '@react-native-firebase/analytics'
import { Settings } from 'react-native-fbsdk-next'
import { getTrackingStatus, TrackingStatus, requestTrackingPermission } from 'react-native-tracking-transparency'
import { Platform } from 'react-native'
import React from 'react'
import { LoadingIndicator } from '../../components/loading-indicator/loading-indicator.view'

async function analyticsEnable (enabled: boolean): Promise<void> {
  await firebase.analytics().setAnalyticsCollectionEnabled(enabled)
  appsFlyer.stop(enabled, res => {
    console.info(`[AppsFlyer] ${enabled ? 'started' : 'stoped'}`, res)
  })
  await Settings.setAdvertiserTrackingEnabled(enabled)
}

interface AnalyticsEnabledProviderProps {
  children: React.ReactNode
  fallback: React.ReactNode
}

export function AnalyticsEnabledProvider ({
  children, fallback
}: AnalyticsEnabledProviderProps): React.ReactElement {
  const [trackingStatus, setTrackingStatus] = React.useState<TrackingStatus>('not-determined')
  React.useEffect(() => {
    getTrackingStatus()
      .then((status) => {
        if (Platform.OS === 'android') {
          setTrackingStatus('authorized')
        }
        if (status === 'not-determined') {
          requestTrackingPermission().then((status) => {
            setTrackingStatus(status)
          }).catch(console.error)
        } else if (status === 'authorized') {
          analyticsEnable(true).catch(console.error)
        }
      })
      .catch(console.error)
  }, [trackingStatus])
  if (trackingStatus === 'not-determined') return <LoadingIndicator visible />
  if (trackingStatus !== 'authorized') return (<>{fallback}</>)
  return (<>{children}</>)
}
