import { DataProxy } from 'apollo-cache'
import { QuestionDataFragment } from '../../../gen/graphql'
import { newUserSiteEntry } from './new-user-site-entry'

export function feedCacheNewQuestion (cache: DataProxy, question: QuestionDataFragment): void {
  newUserSiteEntry(cache, question.author.id, question)
}
