import { DataProxy } from 'apollo-cache'
import {
  FeedEntryDataFragment,
  UserFeedDocument,
  UserFeedQuery,
  UserFeedQueryVariables,
  UserPersonalFeedDocument,
  UserPersonalFeedQuery,
  UserPersonalFeedQueryVariables
} from '../../../gen/graphql'

export function newUserSiteEntry (cache: DataProxy, userId: string, entry: FeedEntryDataFragment): void {
  try {
    const userFeedEntries = cache.readQuery<UserFeedQuery, UserFeedQueryVariables>({
      query: UserFeedDocument,
      variables: { userId }
    })
    if (userFeedEntries?.feedEntries != null) {
      cache.writeQuery<UserFeedQuery, UserFeedQueryVariables>({
        query: UserFeedDocument,
        variables: { userId },
        data: {
          ...userFeedEntries,
          feedEntries: {
            ...userFeedEntries.feedEntries,
            edges: [
              { node: entry, __typename: 'FeedEntryEdge' },
              ...userFeedEntries.feedEntries.edges
            ]
          }
        }
      })
    }
  } catch {
    console.warn('Failed to update user feed cache')
  }
  try {
    const userPersonalEntries = cache.readQuery<UserPersonalFeedQuery, UserPersonalFeedQueryVariables>({
      query: UserPersonalFeedDocument,
      variables: { userId }
    })
    if (userPersonalEntries?.userById?.personalEntries != null) {
      cache.writeQuery<UserPersonalFeedQuery, UserPersonalFeedQueryVariables>({
        query: UserPersonalFeedDocument,
        variables: { userId },
        data: {
          ...userPersonalEntries,
          userById: {
            ...userPersonalEntries.userById,
            personalEntries: {
              ...userPersonalEntries.userById.personalEntries,
              edges: [
                { node: entry, __typename: 'FeedEntryEdge' },
                ...userPersonalEntries.userById.personalEntries.edges
              ]
            }
          }
        }
      })
    }
  } catch {
    console.warn('Failed to update user personal feed cache')
  }
}
