import { DataProxy } from 'apollo-cache'
import { PostDataFragment } from '../../../gen/graphql'
import { newCommunitySiteEntry } from './new-community-site-entry'
import { newUserSiteEntry } from './new-user-site-entry'

export function feedCacheNewPost (cache: DataProxy, post: PostDataFragment): void {
  switch (post.site.__typename) {
    case 'User':
      return newUserSiteEntry(cache, post.site.id, post)
    case 'Community':
      return newCommunitySiteEntry(cache, post.site.id, post)
  }
}
