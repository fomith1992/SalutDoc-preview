import { DataProxy } from 'apollo-cache'
import {
  CommunityFeedDocument,
  CommunityFeedQuery,
  CommunityFeedQueryVariables,
  FeedEntryDataFragment
} from '../../../gen/graphql'

export function newCommunitySiteEntry (cache: DataProxy, communityId: string, entry: FeedEntryDataFragment): void {
  // pretty difficult to find out if user is subscribed to the community in order to add the entry to the user's feed
  try {
    const communityFeed = cache.readQuery<CommunityFeedQuery, CommunityFeedQueryVariables>({
      query: CommunityFeedDocument,
      variables: { communityId }
    })
    if (communityFeed?.communityById?.feedEntries != null) {
      cache.writeQuery<CommunityFeedQuery, CommunityFeedQueryVariables>({
        query: CommunityFeedDocument,
        variables: { communityId },
        data: {
          ...communityFeed,
          communityById: {
            ...communityFeed.communityById,
            feedEntries: {
              ...communityFeed.communityById.feedEntries,
              edges: [
                { node: entry, __typename: 'FeedEntryEdge' },
                ...communityFeed.communityById.feedEntries.edges
              ]
            }
          }
        }
      })
    }
  } catch {
    console.warn('Failed to update community feed cache')
  }
}
