import { DataProxy } from 'apollo-cache'
import { WorkDataFragment } from '../../../gen/graphql'
import { newCommunitySiteEntry } from './new-community-site-entry'
import { newUserSiteEntry } from './new-user-site-entry'

export function feedCacheNewArticle (cache: DataProxy, article: WorkDataFragment): void {
  article.authorUsers.forEach(author => {
    newUserSiteEntry(cache, author.id, article)
  })
  if (article.community != null) {
    newCommunitySiteEntry(cache, article.community.id, article)
  }
}
