import { View, Text } from 'react-native'
import { color } from '../../style/color'
import { lift, styled } from '../../style/styled'
import { sp } from '../../style/size'
import { font } from '../../style/text'
import { FeedIcon24 } from '../../images/feed.icon-24'

export const listStyleSheet = lift({ flexGrow: 1, backgroundColor: color('background') })

export const EmptyPostsContainer = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
  paddingVertical: sp(32)
})

export const EmptyPostsText = styled(Text, {
  ...font({ type: 'h4' }),
  color: color('inactive')
})

export const FeedIconSvg = styled(FeedIcon24, {
  width: sp(32),
  height: sp(32),
  color: color('accent')
})
