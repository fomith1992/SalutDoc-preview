import React from 'react'
import { FlatList } from 'react-native'
import { LoadingIndicator } from '../../components/loading-indicator/loading-indicator.view'
import { useMaterialized } from '../../style/styled'
import { EmptyPostsContainer, EmptyPostsText, FeedIconSvg, listStyleSheet } from './feed.style'

export interface FeedViewProps<T> {
  items: readonly T[]
  keyExtractor: (item: T) => string
  loading: boolean
  refreshing: boolean
  hasMore: boolean
  showEmptyList?: boolean
  loadMore: () => void
  refresh: () => void
  header: React.ReactElement
  emptyView?: React.ReactElement

  children?: (item: T) => React.ReactElement
}

export function FeedView<T> (
  {
    items,
    keyExtractor,
    hasMore,
    loading,
    refreshing,
    loadMore,
    refresh,
    header,
    children,
    showEmptyList = true
  }: FeedViewProps<T>
): React.ReactElement {
  const listStyle = useMaterialized(listStyleSheet)
  return (
    <FlatList<T>
      data={items}
      contentContainerStyle={listStyle}
      renderItem={({ item }) => children?.(item) ?? null}
      keyExtractor={keyExtractor}
      onEndReached={hasMore ? loadMore : null}
      ListHeaderComponent={header}
      ListEmptyComponent={loading ? null
        : showEmptyList
          ? (
            <EmptyPostsContainer>
              <FeedIconSvg />
              <EmptyPostsText>
                Пока нет постов
              </EmptyPostsText>
            </EmptyPostsContainer>
          ) : null}
      ListFooterComponent={
        <LoadingIndicator visible={loading} />
      }
      refreshing={refreshing}
      onRefresh={refresh}
    />
  )
}
