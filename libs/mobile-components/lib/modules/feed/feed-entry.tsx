import React from 'react'
import { WorkPreview } from '../../components/portfolio/work-prewiew/work-prewiew.container'
import { PostCard } from '../posts'
import { Question } from '../../components/question/question-card/question.container'

export interface FeedEntryProps {
  type: 'Post' | 'Article' | 'Question'
  id: string
}

export function FeedEntry (
  {
    type,
    id
  }: FeedEntryProps
): React.ReactElement {
  switch (type) {
    case 'Post':
      return <PostCard id={id} />
    case 'Article':
      return <WorkPreview id={id} />
    case 'Question':
      return <Question id={id} />
  }
}
