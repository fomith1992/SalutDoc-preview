import { appendEdges, assertDefined, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { useAuthenticatedUserOrNull } from '../../components/user-context/user-context-provider'
import { useUserPersonalFeedQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { FeedEntry } from './feed-entry'
import { FeedView } from './feed.view'

export interface UserPersonalFeedProps {
  userId: string
  header: React.ReactElement
}

export function UserPersonalFeed ({ userId, header }: UserPersonalFeedProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const { data, loading, fetchMore, refetch } = useUserPersonalFeedQuery({
    notifyOnNetworkStatusChange: true,
    variables: { userId }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        assertDefined(previousResult.userById?.personalEntries)
        return {
          ...previousResult,
          userById: {
            ...previousResult.userById,
            personalEntries: appendEdges(
              previousResult.userById.personalEntries,
              fetchMoreResult?.userById?.personalEntries
            )
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  })

  return (
    <FeedView
      items={data?.userById?.personalEntries?.edges ?? []}
      keyExtractor={edge => edge.node.id}
      loading={loading && !refreshing}
      refreshing={refreshing}
      hasMore={data?.userById?.personalEntries?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore(expectDefined(data?.userById?.personalEntries?.pageInfo.endCursor))}
      refresh={refresh}
      header={header}
      showEmptyList={user?.id === userId && user.role !== 'USER'}
    >
      {(edge) => <FeedEntry type={edge.node.__typename} id={edge.node.id} />}
    </FeedView>
  )
}
