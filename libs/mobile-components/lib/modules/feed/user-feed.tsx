import { appendEdges, assertDefined, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { useUserFeedQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { FeedEntry } from './feed-entry'
import { FeedView } from './feed.view'

export interface UserFeedProps {
  userId: string | null
  header: React.ReactElement
}

export function UserFeed ({ userId, header }: UserFeedProps): React.ReactElement {
  const { data, loading, fetchMore, refetch } = useUserFeedQuery({
    notifyOnNetworkStatusChange: true,
    variables: { userId }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { userId, after },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        assertDefined(previousResult.feedEntries)
        return {
          ...previousResult,
          feedEntries: appendEdges(
            previousResult.feedEntries,
            fetchMoreResult?.feedEntries
          )
        }
      }
    }).catch(error => {
      console.log(error)
    })
  })

  return (
    <FeedView
      items={data?.feedEntries?.edges ?? []}
      keyExtractor={edge => edge.node.id}
      loading={loading && !refreshing}
      refreshing={refreshing}
      hasMore={data?.feedEntries?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore(expectDefined(data?.feedEntries?.pageInfo.endCursor))}
      refresh={refresh}
      header={header}
    >
      {(edge) => <FeedEntry type={edge.node.__typename} id={edge.node.id} />}
    </FeedView>
  )
}
