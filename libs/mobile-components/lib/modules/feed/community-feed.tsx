import { appendEdges, assertDefined, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { CommunityEmptyView } from '../../components/community-private-content/community-private-content.view'
import { useCommunityFeedQuery } from '../../gen/graphql'
import { useMemoizedFn } from '../../hooks/use-memoized-fn'
import { useRefresh } from '../../hooks/use-refresh'
import { FeedEntry } from './feed-entry'
import { FeedView } from './feed.view'

interface CommunityFeedProps {
  communityId: string
  header: React.ReactElement
}

export function CommunityFeed ({ communityId, header }: CommunityFeedProps): React.ReactElement {
  const { data, loading, fetchMore, refetch } = useCommunityFeedQuery({
    notifyOnNetworkStatusChange: true,
    variables: { communityId }
  })
  const { refreshing, refresh } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        assertDefined(previousResult.communityById?.feedEntries)
        return {
          ...previousResult,
          communityById: {
            ...previousResult.communityById,
            feedEntries: appendEdges(
              previousResult.communityById?.feedEntries,
              fetchMoreResult?.communityById?.feedEntries
            )
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  })

  const emptyView =
    data?.communityById?.type === 'PRIVATE' && data.communityById.membershipByUserId?.status !== 'ACTIVE'
      ? <CommunityEmptyView />
      : undefined

  return (
    <FeedView
      items={data?.communityById?.feedEntries?.edges ?? []}
      keyExtractor={edge => edge.node.id}
      loading={loading && !refreshing}
      refreshing={refreshing}
      hasMore={data?.communityById?.feedEntries?.pageInfo.hasNextPage ?? false}
      loadMore={() => loadMore(expectDefined(data?.communityById?.feedEntries?.pageInfo.endCursor))}
      refresh={refresh}
      header={header}
      emptyView={emptyView}
    >
      {(edge) => <FeedEntry type={edge.node.__typename} id={edge.node.id} />}
    </FeedView>
  )
}
