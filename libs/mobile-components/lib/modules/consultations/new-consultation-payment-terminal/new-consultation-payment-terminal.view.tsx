
import React from 'react'
import { WebView } from 'react-native-webview'

interface NewConsultationWebViewProps {
  uri: string
  onPaymentComplete: () => void
}

export function NewConsultationWebView ({
  uri,
  onPaymentComplete
}: NewConsultationWebViewProps): React.ReactElement {
  return (
    <WebView
      source={{ uri }}
      onNavigationStateChange={(event) => {
        if (event.url.includes('/api/v1/payments/callback')) {
          onPaymentComplete()
        }
      }}
    />
  )
}
