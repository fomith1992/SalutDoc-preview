import { StackNavigationProp } from '@react-navigation/stack'
import React, { useEffect } from 'react'
import {
  useNewConsultationPaymentTerminalQuery
} from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useShowNotification } from '../../ui-kit/notifications'
import { NewConsultationWebView } from './new-consultation-payment-terminal.view'

export type NewConsultationPaymentTerminalProps = {
  consultationId: string
}

export function NewConsultationPaymentTerminal (
  {
    consultationId
  }: NewConsultationPaymentTerminalProps
): React.ReactElement {
  const navigator = useStackNavigation()
  const showNotification = useShowNotification()
  const { data, loading, error } = useNewConsultationPaymentTerminalQuery({
    variables: {
      consultationId
    }
  })
  useEffect(() => {
    if ((data != null && data.consultationById == null) || error != null) {
      showNotification({
        title: 'Ошибка',
        message: 'Консультация не найдена'
      })
      navigator.dangerouslyGetParent<StackNavigationProp<any>>()?.pop()
    }
  }, [data, error])

  if (loading || data?.consultationById?.payment?.paymentUrl == null) {
    return <></>
  }
  return (
    <NewConsultationWebView
      uri={data.consultationById.payment.paymentUrl}
      onPaymentComplete={() => navigator.navigate('NewConsultation_PaymentState', { consultationId })}
    />
  )
}
