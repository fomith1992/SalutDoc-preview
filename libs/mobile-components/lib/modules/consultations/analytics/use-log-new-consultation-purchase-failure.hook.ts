import { useCallback } from 'react'
import { AppEventsLogger } from 'react-native-fbsdk-next'
import { firebaseLogEvent } from '../../../analytics/firebase-log-event'

const newConsultationPurchaseFailureEventName = 'new_consultation_purchase_failure'

export function useLogNewConsultationPurchaseFailure (): () => void {
  return useCallback(() => {
    firebaseLogEvent(newConsultationPurchaseFailureEventName)
    AppEventsLogger.logEvent(newConsultationPurchaseFailureEventName)
  }, [])
}
