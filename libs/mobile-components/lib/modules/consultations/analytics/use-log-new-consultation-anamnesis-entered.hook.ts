import { useCallback } from 'react'
import { AppEventsLogger } from 'react-native-fbsdk-next'
import { firebaseLogEvent } from '../../../analytics/firebase-log-event'

const newConsultationAnamnesisEnteredEventName = 'new_consultation_anamnesis_entered'

export function useLogNewConsultationAnamnesisEntered (): () => void {
  return useCallback(() => {
    firebaseLogEvent(newConsultationAnamnesisEnteredEventName)
    AppEventsLogger.logEvent(newConsultationAnamnesisEnteredEventName)
  }, [])
}
