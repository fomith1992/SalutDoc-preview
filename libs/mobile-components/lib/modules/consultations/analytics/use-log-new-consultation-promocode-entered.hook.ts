import { useCallback } from 'react'
import { AppEventsLogger } from 'react-native-fbsdk-next'
import { firebaseLogEvent } from '../../../analytics/firebase-log-event'

const newConsultationPromocodeEnteredEventName = 'new_consultation_promocode_entered'

export function useLogNewConsultationPromocodeEntered (): (promocode: string) => void {
  return useCallback((promocode: string) => {
    firebaseLogEvent(newConsultationPromocodeEnteredEventName, { promocode })
    AppEventsLogger.logEvent(newConsultationPromocodeEnteredEventName, { promocode })
  }, [])
}
