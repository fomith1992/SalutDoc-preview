import { useCallback } from 'react'
import { AppEventsLogger } from 'react-native-fbsdk-next'
import { firebaseLogEvent } from '../../../analytics/firebase-log-event'

const newConsultationPurchaseBeginEventName = 'new_consultation_purchase_begin'

export function useLogNewConsultationPurchaseBegin (): () => void {
  return useCallback(() => {
    firebaseLogEvent(newConsultationPurchaseBeginEventName)
    AppEventsLogger.logEvent(newConsultationPurchaseBeginEventName)
  }, [])
}
