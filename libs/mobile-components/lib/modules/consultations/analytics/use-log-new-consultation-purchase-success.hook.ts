import { useCallback } from 'react'
import { AppEventsLogger } from 'react-native-fbsdk-next'
import { firebaseLogEvent } from '../../../analytics/firebase-log-event'

const newConsultationPurchaseSuccessEventName = 'new_consultation_purchase_success'

export function useLogNewConsultationPurchaseSuccess (): () => void {
  return useCallback(() => {
    firebaseLogEvent(newConsultationPurchaseSuccessEventName)
    AppEventsLogger.logEvent(newConsultationPurchaseSuccessEventName)
  }, [])
}
