import { useCallback } from 'react'
import { AppEventsLogger } from 'react-native-fbsdk-next'
import { firebaseLogEvent } from '../../../analytics/firebase-log-event'

const newConsultationSpecializationChosenEventName = 'new_consultation_specialization_chosen'

export function useLogNewConsultationSpecializationChosen (): (specialization: string) => void {
  return useCallback((specialization: string) => {
    firebaseLogEvent(newConsultationSpecializationChosenEventName, { specialization })
    AppEventsLogger.logEvent(newConsultationSpecializationChosenEventName, { specialization })
  }, [])
}
