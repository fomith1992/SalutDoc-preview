import { useApolloClient } from '@apollo/react-hooks'
import { StackNavigationProp } from '@react-navigation/stack'
import { CommonActions } from '@react-navigation/native'
import React, { useEffect, useRef } from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import {
  ListPatientConsultationsDocument,
  ListPatientConsultationsQuery,
  ListPatientConsultationsQueryVariables,
  useNewConsultationPaymentStateQuery
} from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useShowNotification } from '../../ui-kit/notifications'
import { useLogNewConsultationPurchaseFailure, useLogNewConsultationPurchaseSuccess } from '../analytics'
import { NewConsultationPaymentStateView } from './new-consultation-payment-state.view'
import { useRootStore } from '../../../stores/root.store'

export type NewConsultationPaymentStateProps = {
  consultationId: string
}

export function NewConsultationPaymentState (
  {
    consultationId
  }: NewConsultationPaymentStateProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const navigator = useStackNavigation()
  const showNotification = useShowNotification()
  const addedToTheListRef = useRef(false)
  const apollo = useApolloClient()
  const logNewConsultationPurchaseSuccess = useLogNewConsultationPurchaseSuccess()
  const logNewConsultationPurchaseFailure = useLogNewConsultationPurchaseFailure()
  const { data, error } = useNewConsultationPaymentStateQuery({
    variables: {
      consultationId,
      userId: user.id
    },
    pollInterval: 1000
  })
  const {
    mode,
    setMode
  } = useRootStore().domains.registerReport
  useEffect(() => {
    if ((data != null && data.consultationById == null) || error != null) {
      showNotification({
        title: 'Ошибка',
        message: 'Консультация не найдена'
      })
      navigator.dangerouslyGetParent<StackNavigationProp<any>>()?.pop()
    }
  }, [data, error])

  const consultation = data?.consultationById
  const success = consultation?.payment?.status === 'ACCEPTED' && consultation.chat != null
  useEffect(() => {
    if (consultation != null && success && !addedToTheListRef.current) {
      addedToTheListRef.current = true
      try {
        const prevData = apollo.readQuery<ListPatientConsultationsQuery, ListPatientConsultationsQueryVariables>({
          query: ListPatientConsultationsDocument,
          variables: {
            userId: user.id
          }
        })
        if (prevData?.userMyself?.consultationsAsPatient != null) {
          apollo.writeQuery<ListPatientConsultationsQuery, ListPatientConsultationsQueryVariables>({
            query: ListPatientConsultationsDocument,
            variables: {
              userId: user.id
            },
            data: {
              ...prevData,
              userMyself: {
                ...prevData.userMyself,
                consultationsAsPatient: {
                  ...prevData.userMyself.consultationsAsPatient,
                  edges: [
                    {
                      __typename: 'ConsultationEdge',
                      node: consultation
                    },
                    ...prevData.userMyself.consultationsAsPatient.edges
                  ]
                }
              }
            }
          })
        }
      } catch {
      }
    }
  }, [consultation, success, apollo, addedToTheListRef])

  useEffect(() => {
    switch (consultation?.payment?.status) {
      case 'ACCEPTED':
        logNewConsultationPurchaseSuccess()
        break
      case 'CANCELLED':
        logNewConsultationPurchaseFailure()
        break
    }
  }, [consultation?.payment?.status])

  useEffect(() => {
    return () => setMode('DEFAULT')
  }, [])

  function resolveRegisterMode (consultationId: string): void {
    switch (mode) {
      case 'DEFAULT':
        navigator.navigate('PatientConsultationChat', { consultationId })
        break
      case 'IN_CONSULTATION':
        navigator.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              { name: 'PatientMain' }
            ]
          })
        )
        break
    }
  }

  if (consultation != null && success) {
    const consultationId = consultation.id
    return (
      <NewConsultationPaymentStateView
        state='SUCCESS'
        onContinue={() => {
          navigator.dangerouslyGetParent<StackNavigationProp<any>>()?.goBack()
          resolveRegisterMode(consultationId)
        }}
      />
    )
  } else if (data?.consultationById?.payment?.status === 'CANCELLED') {
    return (
      <NewConsultationPaymentStateView
        state='FAILURE'
        onAbort={() => navigator.dangerouslyGetParent<StackNavigationProp<any>>()?.pop()}
      />
    )
  } else {
    return <NewConsultationPaymentStateView state='IN_PROGRESS' />
  }
}
