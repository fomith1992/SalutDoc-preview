import React from 'react'
import { ProgressState } from '../../ui-kit/progress-state'

export interface NewConsultationPaymentStateViewProps {
  state: 'IN_PROGRESS' | 'SUCCESS' | 'FAILURE'
  onContinue?: () => void
  onAbort?: () => void
}

export function NewConsultationPaymentStateView (
  {
    state,
    onContinue,
    onAbort
  }: NewConsultationPaymentStateViewProps
): React.ReactElement {
  switch (state) {
    case 'IN_PROGRESS':
      return (
        <ProgressState
          status='IN_PROGRESS'
          title='Обрабатываем платеж...'
          description='Обработка платежа обычно занимает несколько секунд'
        />
      )
    case 'SUCCESS':
      return (
        <ProgressState
          status='SUCCESS'
          title='Консультация оплачена!'
          description='Теперь вы можете перейти в чат с врачом'
          button={{
            text: 'Перейти в консультацию',
            onPress: onContinue
          }}
        />
      )
    case 'FAILURE':
      return (
        <ProgressState
          status='FAILURE'
          title='Оплата не удалась'
          description='Проверьте количество средств и корректность введенных данных'
          button={{
            text: 'Вернуться к консультациям',
            onPress: onAbort
          }}
        />
      )
  }
}
