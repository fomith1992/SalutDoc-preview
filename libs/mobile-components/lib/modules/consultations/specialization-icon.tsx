import React from 'react'
import { GeneralPractitionerIcon24 } from '../../images/general-practitioner.icon-24'
import { DermatologistIcon24 } from '../../images/dermatologist.icon-24'
import { PediatristIcon24 } from '../../images/pediatrist.icon-24'
import { GynecologistIcon24 } from '../../images/gynecologist.icon-24'
import { IconComponentProps } from '../../images/icon-component'
import { ToothIcon } from '../../images/tooth.icon'
import { VenereologistIcon24 } from '../../images/venereologist.icon-24'
import { SurgeonIcon24 } from '../../images/surgeon.icon-24'
import { CardiologistIcon24 } from '../../images/cardiologist.icon-24'
import { GastroenterologistIcon24 } from '../../images/gastroenterologist.icon-24'
import { OphthalmologistIcon24 } from '../../images/ophthalmologist.icon-24'
import { NeurologistIcon24 } from '../../images/neurologist.icon-24'
import { OtolaryngologistIcon24 } from '../../images/otolaryngologist.icon-24'
import { AllergologistIcon24 } from '../../images/allergologist.icon-24'

export type SpecializationIconProps = IconComponentProps & {
  specialization: string | null
}

export function SpecializationIcon (
  {
    specialization,
    ...rest
  }: SpecializationIconProps
): React.ReactElement {
  switch (specialization) {
    case 'dentist':
      return <ToothIcon {...rest} />
    case 'dermatologist':
      return <DermatologistIcon24 {...rest} />
    case 'pediatrist':
      return <PediatristIcon24 {...rest} />
    case 'gynecologist':
      return <GynecologistIcon24 {...rest} />
    case 'surgeon':
      return <SurgeonIcon24 {...rest} />
    case 'cardiologist':
      return <CardiologistIcon24 {...rest} />
    case 'gastroenterologist':
      return <GastroenterologistIcon24 {...rest} />
    case 'ophthalmologist':
      return <OphthalmologistIcon24 {...rest} />
    case 'neurologist':
      return <NeurologistIcon24 {...rest} />
    case 'otolaryngologist':
      return <OtolaryngologistIcon24 {...rest} />
    case 'allergologist':
      return <AllergologistIcon24 {...rest} />
    case 'venereologist':
      return <VenereologistIcon24 {...rest} />
    case 'therapist':
    default:
      return <GeneralPractitionerIcon24 {...rest} />
  }
}
