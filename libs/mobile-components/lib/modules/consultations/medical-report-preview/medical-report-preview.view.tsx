import { ProcessStateModalView } from '../../../components/process-state-modal/process-state-modal.view'
import {
  ButtonContainer,
  MedicalReportContainer,
  MedicalReportDescription,
  MedicalReportElement,
  MedicalReportTitle,
  Spring
} from './medical-report-preview.style'
import { FlatList } from 'react-native'
import {
  MedicalReportBlockWithIdInstance
} from '../../../stores/domains/new-medical-report.store'
import React from 'react'
import { TextButton } from '../../ui-kit/buttons'

interface MedicalReportPreviewViewProps {
  modalVisible: boolean
  setModalVisible: (state: boolean) => void
  sending: boolean
  error: boolean
  medicalReports: MedicalReportBlockWithIdInstance[]
  onSendMedicalReportPressed: () => void
  onFailureCallback: () => void
  onSuccessCallback: () => void
}

export function MedicalReportPreviewView ({
  modalVisible,
  setModalVisible,
  sending,
  error,
  medicalReports,
  onSendMedicalReportPressed,
  onFailureCallback,
  onSuccessCallback
}: MedicalReportPreviewViewProps): React.ReactElement {
  const modalStateInProgress = {
    active: sending && !error,
    title: 'Отправка медзаключения',
    info: 'Отправка медзаключения обычно занимает несколько секунд'
  }
  const modalStateFailure = {
    active: error,
    title: 'Медзаключение не отправлено',
    info: 'Возникла ошибка. Пожалуйста, попробуйте позже',
    button: {
      text: 'Вернуться в чат',
      handlePress: onFailureCallback
    }
  }
  const modalStateSuccess = {
    active: !sending && !error,
    title: 'Медзаключение отправлено',
    info: 'Теперь вы можете завершить консультацию',
    button: {
      text: 'Завершить консультацию',
      handlePress: onSuccessCallback
    }
  }
  return (
    <MedicalReportContainer>
      <FlatList
        contentContainerStyle={{ flexGrow: 1 }}
        data={medicalReports}
        keyExtractor={item => item.block.blockId}
        renderItem={medicalReport => {
          if (medicalReport.item.block.value !== '' && medicalReport.item.block.value != null) {
            return (
              <MedicalReportElement>
                <MedicalReportTitle>
                  {medicalReport.item.block.title}
                </MedicalReportTitle>
                <MedicalReportDescription>
                  {medicalReport.item.block.value}
                </MedicalReportDescription>
              </MedicalReportElement>
            )
          } else return null
        }}
        ListFooterComponent={<Spring />}
      />
      <ButtonContainer>
        <TextButton
          disabled={!medicalReports.some(report => report.block.value !== '')}
          onPress={onSendMedicalReportPressed}
          type='primary'
          size='XL'
        >
          Отправить медзаключение
        </TextButton>
      </ButtonContainer>
      <ProcessStateModalView
        visible={modalVisible}
        setVisible={setModalVisible}
        stateInProgress={modalStateInProgress}
        stateSuccess={modalStateSuccess}
        stateFailure={modalStateFailure}
      />
    </MedicalReportContainer>
  )
}
