import { StackNavigationProp } from '@react-navigation/stack'
import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React, { useState } from 'react'
import { useConsultationAddReportMutation } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useRootStore } from '../../../stores/root.store'
import { MedicalReportPreviewView } from './medical-report-preview.view'

export interface MedicalReportPreviewProps {
  consultationId: string
}

export const MedicalReportPreview = observer(
  ({ consultationId }: MedicalReportPreviewProps): React.ReactElement => {
    const navigation = useStackNavigation()
    const [modalVisible, setModalVisible] = useState<boolean>(false)
    const [consultationAddReport, { loading, error }] = useConsultationAddReportMutation()
    const {
      blocks,
      getBlockByName,
      clearMedicalReport
    } = useRootStore().domains.medicalReport.newMedicalReport
    const diagnosis = getBlockByName('diagnosis').block.value
    const diagnostics = getBlockByName('diagnostics').block.value
    const doctors = getBlockByName('doctors').block.value
    const medicines = getBlockByName('medicines').block.value
    const recommendation = getBlockByName('recommendation').block.value
    return (
      <MedicalReportPreviewView
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        sending={loading}
        error={error != null}
        medicalReports={toJS(blocks ?? [])}
        onSendMedicalReportPressed={async () => {
          setModalVisible(true)
          await consultationAddReport({
            variables: {
              consultationId,
              content: {
                diagnosis,
                diagnostics,
                doctors,
                medicines,
                recommendation
              }
            }
          }).catch(console.error)
          clearMedicalReport()
        }}
        onFailureCallback={() => {
          navigation.dangerouslyGetParent<StackNavigationProp<any>>().pop()
        }}
        onSuccessCallback={() => {
          navigation.dangerouslyGetParent<StackNavigationProp<any>>().pop()
        }}
      />
    )
  })
