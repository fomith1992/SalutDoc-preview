import { Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const MedicalReportContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface')
})

export const ButtonContainer = styled(View, {
  ...container(),
  marginTop: 'auto',
  marginBottom: sp(24)
})

export const Spring = styled(View, {
  height: sp(24)
})

export const MedicalReportElement = styled(View, {
  ...container()
})

export const MedicalReportDescription = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(4)
})

export const MedicalReportTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginTop: sp(24)
})
