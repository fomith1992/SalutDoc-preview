import React from 'react'
import { Avatar } from '../../../components/avatar/avatar.view'
import { ElasticText } from '../../../components/elastic-text/elastic-text'
import { formatDate } from '../i18n/format-date'
import {
  ActionContainer,
  AvatarContainer,
  EmptyIndicator,
  IconContainer,
  IndicatorContainer,
  InterlocutorContainer,
  InterlocutorName,
  NewMessageIndicator,
  NoNewMessageIndicator,
  RowContainer,
  Specialization,
  StyledSpecializationIcon,
  Timestamp
} from './consultation-preview-row.style'

type IndicatorType = 'unseen' | 'done' | 'none'

export type ConsultationPreviewRowViewProps = {
  avatar: string | null
  specialization: string
  title: string
  description: string
  createdAt: Date
  isInactive: boolean
  indicatorType: IndicatorType

  onPress?: () => void
}

export function ConsultationPreviewRowView (
  {
    avatar,
    specialization,
    title,
    description,
    createdAt,
    isInactive,
    indicatorType,
    onPress
  }: ConsultationPreviewRowViewProps
): React.ReactElement {
  return (
    <RowContainer onPress={onPress}>
      {avatar != null ? (
        <AvatarContainer>
          <Avatar
            url={avatar}
            shape='circle'
            filter={isInactive ? 'bw' : undefined}
          />
        </AvatarContainer>
      ) : (
        <IconContainer active={!isInactive}>
          <StyledSpecializationIcon
            specialization={specialization}
            active={!isInactive}
          />
        </IconContainer>
      )}
      <InterlocutorContainer>
        <ElasticText minimumFontScale={1}>
          <InterlocutorName finished={isInactive}>
            {title}
          </InterlocutorName>
        </ElasticText>
        <Specialization>
          {description}
        </Specialization>
      </InterlocutorContainer>
      <ActionContainer>
        <Timestamp>
          {formatDate(createdAt)}
        </Timestamp>
        <IndicatorContainer>
          <Indicator type={indicatorType} />
        </IndicatorContainer>
      </ActionContainer>
    </RowContainer>
  )
}

function Indicator (props: {
  type: IndicatorType
}): React.ReactElement {
  switch (props.type) {
    case 'unseen':
      return <NewMessageIndicator />
    case 'done':
      return <NoNewMessageIndicator />
    case 'none':
      return <EmptyIndicator />
  }
}
