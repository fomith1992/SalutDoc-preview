import React from 'react'
import { specializationName } from '../i18n/specialization'
import { ConsultationPreviewRowView } from './consultation-preview-row.view'

export type PatientConsultationPreviewRowViewProps = {
  doctor: {
    avatar: string | null
    firstName: string
    lastName: string
  } | null
  specialization: string
  createdAt: Date
  state: 'LOOKING_FOR_DOCTOR' | 'IN_PROGRESS' | 'DONE'
  hasNewMessages: boolean

  onPress?: () => void
}

export function PatientConsultationPreviewRowView (
  {
    doctor,
    specialization,
    createdAt,
    hasNewMessages,
    state,
    onPress
  }: PatientConsultationPreviewRowViewProps
): React.ReactElement {
  return (
    <ConsultationPreviewRowView
      avatar={doctor?.avatar ?? null}
      specialization={specialization}
      title={doctor == null
        ? 'Поиск врача'
        : `${doctor.firstName} ${doctor.lastName}`}
      description={specializationName[specialization] ?? specialization}
      createdAt={createdAt}
      isInactive={state === 'DONE' && !hasNewMessages}
      indicatorType={hasNewMessages ? 'unseen' : state === 'IN_PROGRESS' ? 'done' : 'none'}
      onPress={onPress}
    />
  )
}
