import { Text, TouchableOpacity, View } from 'react-native'
import { TickInCircleIcon12 } from '../../../images/tick-in-circle.icon-12'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { SpecializationIcon } from '../specialization-icon'

export const RowContainer = styled(TouchableOpacity, {
  ...container('padding'),
  backgroundColor: color('surface'),
  paddingVertical: sp(12),
  flexDirection: 'row',
  alignItems: 'center'
})

export const AvatarContainer = styled(View, {
  overflow: 'hidden',
  width: sp(56),
  height: sp(56)
})

export const IconContainer = styled(View, (props: { active: boolean }) => ({
  width: sp(56),
  height: sp(56),
  borderRadius: sp(28),
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: color(props.active ? 'secondaryLight' : 'background')
}) as const)

export const StyledSpecializationIcon = styled(SpecializationIcon, (props: { active: boolean }) => ({
  width: sp(32),
  height: sp(32),
  color: color(props.active ? 'accent' : 'subtext')
}))

export const InterlocutorContainer = styled(View, {
  marginLeft: sp(16),
  overflow: 'hidden',
  flexDirection: 'column',
  flex: 1
})

export const InterlocutorName = styled(Text, (props: { finished: boolean }) => ({
  ...font({ type: 'h3', weight: 'strong' }),
  color: color(props.finished ? 'subtext' : 'text')
}) as const)

export const Specialization = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext'),
  marginTop: sp(4)
})

export const Timestamp = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('inactive')
})

export const ActionContainer = styled(View, {
  marginLeft: 'auto',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center'
})

export const IndicatorContainer = styled(View, {
  marginTop: sp(8),
  alignSelf: 'flex-end'
})

export const NoNewMessageIndicator = styled(TickInCircleIcon12, {
  width: sp(12),
  height: sp(12),
  color: color('inactive')
})

export const NewMessageIndicator = styled(View, {
  width: sp(12),
  height: sp(12),
  borderRadius: sp(8),
  backgroundColor: color('accent')
})

export const EmptyIndicator = styled(View, {
  width: sp(12),
  height: sp(12)
})
