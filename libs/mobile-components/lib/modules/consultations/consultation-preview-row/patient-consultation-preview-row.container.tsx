import React from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { usePatientConsultationPreviewRowQuery } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { PatientConsultationPreviewRowView } from './patient-consultation-preview-row.view'

export type PatientConsultationPreviewRowProps = {
  consultationId: string
}

export function PatientConsultationPreviewRow (
  {
    consultationId
  }: PatientConsultationPreviewRowProps
): React.ReactElement {
  const navigator = useStackNavigation()
  const user = useAuthenticatedUser()
  const { data } = usePatientConsultationPreviewRowQuery({
    variables: {
      consultationId,
      userId: user.id
    }
  })
  const consultation = data?.consultationById
  if (consultation == null) {
    return <></>
  }

  return (
    <PatientConsultationPreviewRowView
      doctor={consultation.doctor}
      specialization={consultation.specialization}
      createdAt={new Date(consultation.createdAt)}
      hasNewMessages={(consultation.chat?.unreadMessagesCount ?? 0) > 0}
      state={consultation.doctor == null
        ? 'LOOKING_FOR_DOCTOR'
        : consultation.report == null
          ? 'IN_PROGRESS'
          : 'DONE'}
      onPress={() => navigator.push('PatientConsultationChat', { consultationId })}
    />
  )
}
