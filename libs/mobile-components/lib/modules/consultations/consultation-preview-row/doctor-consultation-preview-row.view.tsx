import React from 'react'
import { consultationToDoctor } from '../i18n/specialization'
import { ConsultationPreviewRowView } from './consultation-preview-row.view'

export type DoctorConsultationPreviewRowViewProps = {
  patient: {
    firstName: string
    lastName: string
  }
  specialization: string
  createdAt: Date
  hasNewMessages: boolean
  state: 'FREE' | 'IN_PROGRESS' | 'WAITING_FOR_REPORT' | 'DONE'

  onPress?: () => void
}

export function DoctorConsultationPreviewRowView (
  {
    patient,
    specialization,
    createdAt,
    hasNewMessages,
    state,
    onPress
  }: DoctorConsultationPreviewRowViewProps
): React.ReactElement {
  return (
    <ConsultationPreviewRowView
      avatar={null}
      specialization={specialization}
      title={`${patient.firstName} ${patient.lastName}`}
      description={consultationToDoctor[specialization] ?? specialization}
      createdAt={createdAt}
      isInactive={state === 'DONE' && !hasNewMessages}
      indicatorType={hasNewMessages || state === 'WAITING_FOR_REPORT'
        ? 'unseen'
        : state === 'IN_PROGRESS' ? 'done' : 'none'}
      onPress={onPress}
    />
  )
}
