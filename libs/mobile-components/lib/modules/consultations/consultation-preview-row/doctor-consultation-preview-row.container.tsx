import React from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useDoctorConsultationPreviewRowQuery } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { DoctorConsultationPreviewRowView } from './doctor-consultation-preview-row.view'

export type DoctorConsultationPreviewRowProps = {
  consultationId: string
}

export function DoctorConsultationPreviewRow (
  {
    consultationId
  }: DoctorConsultationPreviewRowProps
): React.ReactElement {
  const navigator = useStackNavigation()
  const user = useAuthenticatedUser()
  const { data } = useDoctorConsultationPreviewRowQuery({
    variables: {
      consultationId,
      userId: user.id
    }
  })
  const consultation = data?.consultationById
  if (consultation == null) {
    return <></>
  }

  return (
    <DoctorConsultationPreviewRowView
      patient={consultation.patient}
      specialization={consultation.specialization}
      createdAt={new Date(consultation.createdAt)}
      hasNewMessages={(consultation.chat?.unreadMessagesCount ?? 0) > 0}
      state={consultation.chat == null
        ? 'FREE'
        : !consultation.finished
          ? 'IN_PROGRESS'
          : consultation.report == null
            ? 'WAITING_FOR_REPORT'
            : 'DONE'}
      onPress={consultation.chat == null
        ? () => navigator.push('FreeConsultationChat', { consultationId })
        : () => navigator.push('DoctorConsultationChat', { consultationId })}
    />
  )
}
