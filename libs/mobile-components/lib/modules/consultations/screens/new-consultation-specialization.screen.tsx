import React from 'react'
import { NewConsultationSpecialization } from '../new-consultation-specialization'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'

export function NewConsultationSpecializationScreen (): React.ReactElement {
  return (
    <ScreenContentView>
      <NewConsultationSpecialization />
    </ScreenContentView>
  )
}
