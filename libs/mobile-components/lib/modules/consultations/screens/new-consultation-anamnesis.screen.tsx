import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { NewConsultationAnamnesis } from '../new-consultation-anamnesis'
import { NewConsultationStackParamList } from '../../../navigation/new-consultation-stack'

export function NewConsultationAnamnesisScreen (
  { route }: StackScreenProps<NewConsultationStackParamList, 'NewConsultation_Anamnesis'>
): React.ReactElement {
  return (
    <ScreenContentView>
      <NewConsultationAnamnesis consultationId={route.params.consultationId} />
    </ScreenContentView>
  )
}
