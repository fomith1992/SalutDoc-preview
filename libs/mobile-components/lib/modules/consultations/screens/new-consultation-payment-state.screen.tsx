import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { NewConsultationStackParamList } from '../../../navigation/new-consultation-stack'
import { NewConsultationPaymentState } from '../new-consultation-payment-state'

export function NewConsultationPaymentStateScreen (
  { route }: StackScreenProps<NewConsultationStackParamList, 'NewConsultation_PaymentState'>
): React.ReactElement {
  return (
    <ScreenContentView>
      <NewConsultationPaymentState consultationId={route.params.consultationId} />
    </ScreenContentView>
  )
}
