import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { NewConsultationPaymentTerminal } from '../new-consultation-payment-terminal'
import { NewConsultationStackParamList } from '../../../navigation/new-consultation-stack'

export function NewConsultationPaymentTerminalScreen (
  { route }: StackScreenProps<NewConsultationStackParamList, 'NewConsultation_PaymentTerminal'>
): React.ReactElement {
  return (
    <ScreenContentView>
      <NewConsultationPaymentTerminal consultationId={route.params.consultationId} />
    </ScreenContentView>
  )
}
