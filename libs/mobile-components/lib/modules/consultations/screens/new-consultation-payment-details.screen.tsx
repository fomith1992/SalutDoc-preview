import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { NewConsultationPaymentDetails } from '../new-consultation-payment-details'
import { NewConsultationStackParamList } from '../../../navigation/new-consultation-stack'
import { useDefaultHeader } from '../../ui-kit/header'

export function NewConsultationPaymentDetailsScreen (
  { route }: StackScreenProps<NewConsultationStackParamList, 'NewConsultation_PaymentDetails'>
): React.ReactElement {
  useDefaultHeader({
    actions: [{
      icon: 'help',
      onPress: () => {}
    }]
  })
  return (
    <ScreenContentView>
      <NewConsultationPaymentDetails consultationId={route.params.consultationId} />
    </ScreenContentView>
  )
}
