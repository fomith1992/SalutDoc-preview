export * from './new-consultation-anamnesis.screen'
export * from './new-consultation-payment-details.screen'
export * from './new-consultation-payment-state.screen'
export * from './new-consultation-specialization.screen'
