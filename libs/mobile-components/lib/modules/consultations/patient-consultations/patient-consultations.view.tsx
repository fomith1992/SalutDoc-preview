import React from 'react'
import { FlatList } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useMaterialized } from '../../../style/styled'
import { consultationDuration } from '../../config'
import { TextButton } from '../../ui-kit'
import {
  Description,
  DialogImage,
  EmptyListContainer,
  IconContainer,
  ImageContainer,
  listContentContainerStyleSheet,
  listStyleSheet,
  PlusIconAvatar,
  StartConsultationContainer,
  SubtitleStartConsultation,
  Title,
  TitleContainer,
  TitleStartConsultation,
  EmptyElement
} from './patient-consultations.style'

interface PatientConsultationsViewProps<T> {
  data: readonly T[]
  extractKey: (item: T) => string
  renderItem: (item: T) => React.ReactElement
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
  onStartConsultation: () => void
}

export function PatientConsultationsView<T> (
  {
    data,
    extractKey,
    renderItem,
    refreshing,
    loading,
    hasMore,
    refresh,
    loadMore,
    onStartConsultation
  }: PatientConsultationsViewProps<T>
): React.ReactElement {
  const listStyle = useMaterialized(listStyleSheet)
  const listContentContainerStyle = useMaterialized(listContentContainerStyleSheet)

  return (
    <FlatList
      style={listStyle}
      contentContainerStyle={listContentContainerStyle}
      ListHeaderComponent={
        data.length !== 0
          ? (
            <StartConsultationContainer
              onPress={onStartConsultation}
            >
              <IconContainer>
                <PlusIconAvatar />
              </IconContainer>
              <TitleContainer>
                <TitleStartConsultation>
                  Начать консультацию
                </TitleStartConsultation>
                <SubtitleStartConsultation>
                  490 руб. за {consultationDuration} мин. общения с врачом
                </SubtitleStartConsultation>
              </TitleContainer>
            </StartConsultationContainer>
          )
          : null
      }
      ListEmptyComponent={() => (
        <EmptyListContainer>
          <EmptyElement>
            <ImageContainer>
              <DialogImage />
            </ImageContainer>
            <Title>
              Проконсультироваться с врачом
            </Title>
            <Description>
              {consultationDuration} минут помощи от опытного врача. {'\n'}
              Скидка 50% по промокоду SalutDoc.
            </Description>
            <TextButton
              type='primary'
              size='L'
              onPress={onStartConsultation}
            >
              Начать консультацию
            </TextButton>
          </EmptyElement>
        </EmptyListContainer>
      )}
      data={data}
      keyExtractor={extractKey}
      renderItem={({ item }) => renderItem(item)}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListFooterComponent={<LoadingIndicator visible={loading} />}
    />
  )
}
