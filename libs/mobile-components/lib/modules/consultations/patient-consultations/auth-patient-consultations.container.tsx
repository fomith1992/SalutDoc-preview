import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { Text } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useListPatientConsultationsQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { PatientConsultationPreviewRow } from '../consultation-preview-row'
import { PatientConsultationsView } from './patient-consultations.view'

export function AuthPatientConsultations (
  props: { userId: string}
): React.ReactElement {
  const navigation = useStackNavigation()
  const { data, loading, fetchMore, refetch } = useListPatientConsultationsQuery({
    variables: {
      userId: props.userId
    },
    notifyOnNetworkStatusChange: true
  })
  const { refresh, refreshing } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => ({
        userMyself: {
          ...expectDefined(previousResult.userMyself),
          consultationsAsPatient: appendEdges(
            expectDefined(previousResult.userMyself?.consultationsAsPatient),
            fetchMoreResult?.userMyself?.consultationsAsPatient
          )
        }
      })
    }).catch(error => {
      console.log(error)
    })
  })
  const consultations = data?.userMyself?.consultationsAsPatient
  if (consultations == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <PatientConsultationsView
      data={consultations.edges}
      extractKey={item => item.node.id}
      renderItem={item => <PatientConsultationPreviewRow consultationId={item.node.id} />}
      refreshing={refreshing}
      loading={loading && !refreshing}
      hasMore={consultations.pageInfo.hasNextPage}
      refresh={refresh}
      loadMore={() => loadMore(consultations.pageInfo.endCursor)}
      onStartConsultation={() => navigation.push('NewConsultation', {})}
    />
  )
}
