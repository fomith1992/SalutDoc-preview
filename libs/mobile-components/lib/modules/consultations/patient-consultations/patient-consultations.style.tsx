import { Text, TouchableOpacity, View } from 'react-native'
import { PlusIcon16 } from '../../../images/plus.icon-16'
import { color } from '../../../style/color'
import { container } from '../../../style/view'
import { font } from '../../../style/text'
import { sp } from '../../../style/size'
import { lift, styled } from '../../../style/styled'
import { ChatWithPlusIcon56 } from '../../../images/chat-with-plus.icon-56'

export const listStyleSheet = lift({
  backgroundColor: color('surface')
})

export const listContentContainerStyleSheet = lift({
  paddingVertical: sp(12),
  flexGrow: 1
})

export const EmptyElement = styled(View, {
  justifyContent: 'center',
  alignItems: 'center'
})

export const EmptyListContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
})

export const ImageContainer = styled(View, {
  width: sp(104),
  height: sp(104),
  borderRadius: sp(64),
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: color('secondaryLight')
})

export const DialogImage = styled(ChatWithPlusIcon56, {
  width: 63,
  height: 59,
  color: color('accent')
})

export const Title = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(20)
})

export const Description = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(12),
  marginBottom: sp(24)
})

export const StartConsultationContainer = styled(TouchableOpacity, {
  ...container(),
  marginTop: 'auto',
  marginBottom: 'auto',
  backgroundColor: color('surface'),
  paddingVertical: sp(12),
  flexDirection: 'row',
  alignItems: 'center'
})

export const IconContainer = styled(View, {
  width: sp(56),
  height: sp(56),
  borderRadius: sp(28),
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: color('accent')
})

export const TitleContainer = styled(View, {
  marginLeft: sp(12)
})

export const TitleStartConsultation = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' })
})

export const SubtitleStartConsultation = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext'),
  marginTop: sp(4)
})

export const PlusIconAvatar = styled(PlusIcon16, {
  width: sp(32),
  height: sp(32),
  color: color('surface')
})
