import React from 'react'
import { StackActions } from '@react-navigation/native'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { PatientConsultationsView } from './patient-consultations.view'
import { useRootStore } from '../../../stores/root.store'

export function NotAuthPatientConsultations (): React.ReactElement {
  const navigation = useStackNavigation()
  const {
    setMode
  } = useRootStore().domains.registerReport
  return (
    <PatientConsultationsView
      data={[]}
      extractKey={_item => 'epmty-key'}
      renderItem={_item => <></>}
      refreshing={false}
      loading={false}
      hasMore={false}
      refresh={() => {}}
      loadMore={() => {}}
      onStartConsultation={() => {
        setMode('IN_CONSULTATION')
        navigation.dispatch(StackActions.push('Register_PatientStack', {}))
      }}
    />
  )
}
