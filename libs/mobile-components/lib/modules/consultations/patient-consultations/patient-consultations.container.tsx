import React from 'react'
import { useAuthenticatedUserOrNull } from '../../../components/user-context/user-context-provider'
import { AuthPatientConsultations } from './auth-patient-consultations.container'
import { NotAuthPatientConsultations } from './not-auth-patient-consultations.container'

export function PatientConsultations (): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  if (user == null) return <NotAuthPatientConsultations />
  return <AuthPatientConsultations userId={user.id} />
}
