import React from 'react'
import { TextInput } from '../../../modules/form-kit'
import { TextButton } from '../../ui-kit/buttons'
import {
  MedicalReportBlockInstance,
  MedicalReportBlockWithIdInstance
} from '../../../stores/domains/new-medical-report.store'
import {
  ButtonContainer,
  MedicalReportContainer,
  MedicalReportDescription,
  MedicalReportElement,
  Spring,
  reportContainerStyleSheet
} from './medical-report-create.style'
import { useMaterialized } from '../../../style/styled'

interface MedicalReportCreateViewProps {
  medicalReports: MedicalReportBlockWithIdInstance[]
  onUpdateBlock: (id: string, newBlock: MedicalReportBlockInstance) => void
  onOpenPreviewPressed: () => void
}

const medicalReportInputMaxLenght = 500

export function MedicalReportCreateView ({
  medicalReports,
  onUpdateBlock,
  onOpenPreviewPressed
}: MedicalReportCreateViewProps): React.ReactElement {
  const reportContainerStyle = useMaterialized(reportContainerStyleSheet)
  return (
    <MedicalReportContainer
      contentContainerStyle={reportContainerStyle}
    >
      <MedicalReportDescription>
        Если нет необходимости заполнять какое-либо поле, то вы можете оставить его пустым.
      </MedicalReportDescription>
      {medicalReports.map(item => (
        <MedicalReportElement key={item.block.blockId}>
          <TextInput
            label={item.block.title}
            maxLength={medicalReportInputMaxLenght}
            numberOfLines={[1, 3]}
            placeholder={item.block.placeholder}
            value={item.block.value}
            onChange={value => onUpdateBlock(
              item.block.blockId,
              {
                ...item.block,
                value
              }
            )}
          />
        </MedicalReportElement>
      ))}
      <Spring />
      <ButtonContainer>
        <TextButton
          disabled={!medicalReports.some(report => report.block.value !== '')}
          onPress={onOpenPreviewPressed}
          type='primary'
          size='XL'
        >
          Далее
        </TextButton>
      </ButtonContainer>
    </MedicalReportContainer>
  )
}
