import { Text, View, ScrollView } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { lift, styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const reportContainerStyleSheet = lift({
  flexGrow: 1
})

export const MedicalReportContainer = styled(ScrollView, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface')
})

export const ButtonContainer = styled(View, {
  marginTop: 'auto',
  marginBottom: sp(24)
})

export const Spring = styled(View, {
  height: sp(24)
})

export const MedicalReportElement = styled(View, {
  marginTop: sp(24)
})

export const MedicalReportDescription = styled(Text, {
  marginTop: sp(24),
  ...font({ type: 'text2' }),
  color: color('subtext')
})
