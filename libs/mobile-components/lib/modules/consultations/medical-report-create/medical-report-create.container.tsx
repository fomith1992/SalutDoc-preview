import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React from 'react'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useRootStore } from '../../../stores/root.store'
import { MedicalReportCreateView } from './medical-report-create.view'

export interface MedicalReportCreateProps {
  consultationId: string
}

export const MedicalReportCreate = observer(
  ({ consultationId }: MedicalReportCreateProps): React.ReactElement => {
    const navigation = useStackNavigation()
    const {
      blocks,
      updateBlock
    } = useRootStore().domains.medicalReport.newMedicalReport
    return (
      <MedicalReportCreateView
        medicalReports={toJS(blocks ?? [])}
        onUpdateBlock={(blockId, block) => updateBlock(blockId, block)}
        onOpenPreviewPressed={() => navigation.push('CreateMedicalReport_Review', { consultationId })}
      />
    )
  })
