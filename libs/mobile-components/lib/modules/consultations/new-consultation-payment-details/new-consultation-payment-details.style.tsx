import { View, Text, ScrollView } from 'react-native'
import { InfoCircleIcon24 } from '../../../images/info-circle.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { SpecializationIcon } from '../specialization-icon'

export const ScrollContainer = ScrollView

export const Container = styled(View, {
  flex: 1,
  backgroundColor: color('surface')
})

export const SpecializationIconContainer = styled(View, {
  width: sp(56),
  height: sp(56),
  borderRadius: sp(28),
  backgroundColor: color('secondaryLight'),
  justifyContent: 'center',
  alignItems: 'center'
})

export const ConsultationDescriptionContainer = styled(View, {
  ...container(),
  marginTop: sp(24),
  flexDirection: 'row',
  alignItems: 'center'
})

export const ConsultationDescriptionText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text'),
  marginLeft: sp(16),
  flex: 1
})

export const ConsultationPriceText = styled(Text, {
  ...container(),
  ...font({ type: 'text1' }),
  color: color('text'),
  marginTop: sp(16)
})

export const ButtonContainer = styled(View, {
  ...container(),
  marginVertical: sp(24),
  backgroundColor: color('surface')
})

export const PayContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: sp(24)
})

export const SumContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center'
})

export const DiscountText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('subtext'),
  textDecorationLine: 'line-through',
  marginRight: sp(8)
})

export const PayText = styled(Text, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const DescriptionDiscountContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  justifyContent: 'flex-end',
  marginTop: sp(4)
})

export const DescriptionDiscountText = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text')
})

export const PromocodeInputContainer = styled(View, {
  ...container('margin'),
  marginTop: sp(24),
  flexDirection: 'row'
})

export const PromocodeTextInputContainer = styled(View, {
  flex: (1),
  marginRight: sp(8)
})

export const PolicyConfirmationContainer = styled(View, {
  ...container(),
  marginTop: 'auto',
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: color('surface')
})

export const Spring = styled(View, {
  flex: 1,
  paddingVertical: sp(24),
  marginTop: 'auto'
})

export const PolicyConfirmationText = styled(Text, {
  ...font({ type: 'text2' }),
  marginHorizontal: sp(16),
  color: color('text')
})

export const PolicyConfirmationLink = styled(PolicyConfirmationText, {
  textDecorationLine: 'underline'
})

export const SpecializationImg = styled(SpecializationIcon, {
  color: color('accent'),
  width: sp(32),
  height: sp(32)
})

export const InfoCloud = styled(View, {
  ...container(),
  borderRadius: sp(16),
  marginTop: sp(16),
  padding: sp(16),
  backgroundColor: color('background'),
  flexDirection: 'row'
})

export const InfoCircleSvg = styled(InfoCircleIcon24, {
  color: color('text')
})

export const InfoCloudText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginLeft: sp(12),
  flex: 1
})
