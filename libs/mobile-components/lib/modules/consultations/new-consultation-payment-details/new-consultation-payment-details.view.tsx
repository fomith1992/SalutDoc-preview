import React, { useState } from 'react'
import { DividerView } from '../../../components/divider/divider.view'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { CheckBox } from '../../form-kit/check-box/check-box'
import { TextButton, ExternalLinkText, hitSlopParams } from '../../ui-kit'
import { consultationWithSpecialization } from '../i18n/specialization'
import {
  ButtonContainer,
  ConsultationDescriptionContainer,
  ConsultationDescriptionText,
  ConsultationPriceText,
  Container,
  DescriptionDiscountContainer,
  DescriptionDiscountText,
  DiscountText,
  PayContainer,
  PayText,
  PolicyConfirmationContainer,
  PolicyConfirmationLink,
  PolicyConfirmationText,
  PromocodeInputContainer,
  PromocodeTextInputContainer,
  ScrollContainer,
  SpecializationIconContainer,
  Spring,
  SumContainer,
  SpecializationImg,
  InfoCloud,
  InfoCircleSvg,
  InfoCloudText
} from './new-consultation-payment-details.style'
import { TextInput } from '../../form-kit/text-input'
import { consultationDuration } from '../../config'

export interface PromocodeInfo {
  code: string
  amountToPay: string
}

interface ConsultationPaymentViewProps {
  specialization?: string
  loading: boolean
  policyConfirmationHref: string
  promocodeInfo: PromocodeInfo | null
  onApplyPromocode: (code: string) => void
  onPay: () => void
}

export function NewConsultationPaymentDetailsView (
  {
    specialization,
    loading,
    policyConfirmationHref,
    promocodeInfo,
    onApplyPromocode,
    onPay
  }: ConsultationPaymentViewProps
): React.ReactElement {
  const [rulesAccepted, setRulesAccepted] = useState(false)
  const [promocode, setPromocode] = useState<string>('')
  return (
    <ScrollContainer
      contentContainerStyle={{ minHeight: '100%' }}
      centerContent
    >
      <Container>
        {specialization == null ? null : (
          <>
            <ConsultationDescriptionContainer>
              <SpecializationIconContainer>
                <SpecializationImg specialization={specialization} />
              </SpecializationIconContainer>
              <ConsultationDescriptionText
                numberOfLines={2}
              >
                {`Онлайн-консультация\nс ${consultationWithSpecialization[specialization] ?? specialization}`}
              </ConsultationDescriptionText>
            </ConsultationDescriptionContainer>
            <ConsultationPriceText>
              • текстовый чат с врачом{'\n'}
              • продолжительность {consultationDuration} минут{'\n'}
              • заключение и рекомендации от врача{'\n'}
            </ConsultationPriceText>
            <DividerView />
            <PayContainer>
              <PayText>
                К оплате
              </PayText>
              <SumContainer>
                {promocodeInfo == null
                  ? (
                    <PayText>
                      490 ₽
                    </PayText>
                  ) : (
                    <>
                      <DiscountText>
                        490 ₽
                      </DiscountText>
                      <PayText>
                        {promocodeInfo.amountToPay} ₽
                      </PayText>
                    </>
                  )}
              </SumContainer>
            </PayContainer>
            {promocodeInfo == null
              ? null
              : (
                <DescriptionDiscountContainer>
                  <DescriptionDiscountText>
                    по промокоду {promocodeInfo.code}
                  </DescriptionDiscountText>
                </DescriptionDiscountContainer>
              )}
            <InfoCloud>
              <InfoCircleSvg />
              <InfoCloudText>
                Если качество консультации вас не{'\n'}
                устроит или врач не выйдет на связь,{'\n'}
                то деньги будут возвращены вам
              </InfoCloudText>
            </InfoCloud>
            <DividerView marginTop={24} />
            <PromocodeInputContainer>
              <PromocodeTextInputContainer>
                <TextInput
                  value={promocode === '' ? undefined : promocode}
                  onChange={value => setPromocode(value)}
                  placeholder='Введите промокод'
                />
              </PromocodeTextInputContainer>
              <TextButton
                type='primary'
                size='M'
                disabled={promocode === ''}
                onPress={() => onApplyPromocode(promocode)}
              >
                Применить
              </TextButton>
            </PromocodeInputContainer>
            <Spring />
            <PolicyConfirmationContainer>
              <CheckBox
                value={rulesAccepted}
                onChange={setRulesAccepted}
                hitSlop={hitSlopParams(24)}
              />
              <PolicyConfirmationText>
                Я согласен на <ExternalLinkText href={policyConfirmationHref}><PolicyConfirmationLink>медицинское вмешательство</PolicyConfirmationLink></ExternalLinkText> во время
                онлайн-консультации
              </PolicyConfirmationText>
            </PolicyConfirmationContainer>
            <ButtonContainer>
              <TextButton
                onPress={onPay}
                type='primary'
                size='XL'
                disabled={!rulesAccepted}
              >
                {promocodeInfo?.amountToPay === '0' ? 'Оплатить промокодом' : 'Оплатить картой'}
              </TextButton>
            </ButtonContainer>
          </>
        )}
        <ModalLoadingIndicator visible={loading} />
      </Container>
    </ScrollContainer>
  )
}
