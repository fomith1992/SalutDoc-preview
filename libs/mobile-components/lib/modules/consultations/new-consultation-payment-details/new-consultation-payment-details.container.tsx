import React, { useEffect, useState } from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import {
  useNewConsultationApplyPromocodeMutation,
  useNewConsultationPaymentDetailsQuery,
  useNewConsultationPayMutation
} from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useShowNotification } from '../../ui-kit/notifications'
import { useLogNewConsultationPurchaseBegin } from '../analytics'
import { NewConsultationPaymentDetailsView, PromocodeInfo } from './new-consultation-payment-details.view'

const policyConfirmation = 'https://about.salutdoc.com/medical-services-agreement/'

export type NewConsultationPaymentDetailsProps = {
  consultationId: string
}

export function NewConsultationPaymentDetails (
  {
    consultationId
  }: NewConsultationPaymentDetailsProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const navigator = useStackNavigation()
  const showNotification = useShowNotification()
  const logNewConsultationPurchaseBegin = useLogNewConsultationPurchaseBegin()
  const [promocodeInfo, setPromocodeInfo] = useState<PromocodeInfo | null>(null)
  const { data, loading } = useNewConsultationPaymentDetailsQuery({
    variables: { consultationId }
  })
  const [consultationPay, { loading: paying }] = useNewConsultationPayMutation({
    variables: {
      consultationId,
      userId: user.id,
      promocode: promocodeInfo?.code ?? null
    },
    onCompleted: data => {
      if (data.consultationPay.consultation != null) {
        if (data.consultationPay.consultation.payment?.status === 'ACCEPTED') {
          navigator.push('NewConsultation_PaymentState', { consultationId })
        } else {
          navigator.push('NewConsultation_PaymentTerminal', { consultationId })
        }
      } else {
        console.error('Не удалось оплатить консультацию', data.consultationPay)
        showNotification({
          title: 'Ошибка',
          message: 'Не удалось оплатить консультацию'
        })
      }
    },
    onError: error => {
      console.error('Не удалось оплатить консультацию', error)
      showNotification({
        title: 'Ошибка',
        message: 'Не удалось оплатить консультацию'
      })
    }
  })
  const [applyPromocode, { loading: applyingPromocode }] = useNewConsultationApplyPromocodeMutation({
    onError: error => {
      console.error('Не удалось применить промокод', error)
      showNotification({
        title: 'Ошибка',
        message: 'Не удалось применить промокод'
      })
      setPromocodeInfo(null)
    }
  })
  useEffect(() => {
    if (!loading && data?.consultationById == null) {
      showNotification({
        title: 'Ошибка',
        message: 'Консультация не найдена'
      })
      navigator.goBack()
    }
  }, [loading, data?.consultationById, navigator])
  return (
    <NewConsultationPaymentDetailsView
      specialization={data?.consultationById?.specialization}
      loading={loading || paying || data?.consultationById == null || applyingPromocode}
      promocodeInfo={promocodeInfo}
      onApplyPromocode={(code: string) => {
        applyPromocode({
          variables: {
            userId: user.id,
            code
          }
        })
          .then(({ data }) => {
            if (data?.consultationApplyPromocode.amount == null) {
              console.error('Не удалось применить промокод', data?.consultationApplyPromocode)
              showNotification({
                title: 'Ошибка',
                message: 'Не удалось применить промокод'
              })
              setPromocodeInfo(null)
            } else {
              setPromocodeInfo({
                code,
                amountToPay: data.consultationApplyPromocode.amount
              })
            }
          })
          .catch(/* handled in hook */)
      }}
      onPay={() => {
        logNewConsultationPurchaseBegin()
        consultationPay().catch(/* handled in hook */)
      }}
      policyConfirmationHref={policyConfirmation}
    />
  )
}
