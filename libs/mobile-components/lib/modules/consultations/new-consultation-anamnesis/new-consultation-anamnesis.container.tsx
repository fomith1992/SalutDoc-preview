import { ReactNativeFile } from 'apollo-upload-client'
import React, { useState } from 'react'
import { Alert } from 'react-native'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import {
  useNewConsultationAnamnesisQuery,
  useConsultationAddAnamnesisMutation,
  useFileUploadMutation
} from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useShowNotification } from '../../ui-kit/notifications'
import { useLogNewConsultationAnamnesisEntered } from '../analytics'
import { Document } from '../../ui-kit/image-picker'
import { NewConsultationAnamnesisView, UploadedDocument } from './new-consultation-anamnesis.view'

export type NewConsultationAnamnesisProps = {
  consultationId: string
}

interface UploadedFile extends UploadedDocument {
  id: string | null
}

export function NewConsultationAnamnesis (
  {
    consultationId
  }: NewConsultationAnamnesisProps
): React.ReactElement {
  const showNotification = useShowNotification()
  const user = useAuthenticatedUser()
  const navigator = useStackNavigation()
  const logNewConsultationAnamnesisEntered = useLogNewConsultationAnamnesisEntered()
  const currentConsultation = useNewConsultationAnamnesisQuery({
    variables: {
      consultationId
    }
  })
  const [files, setFiles] = useState<UploadedFile[]>([])
  const [addAnamnesis] = useConsultationAddAnamnesisMutation({
    onCompleted: data => {
      if (data.consultationAddAnamnesis.consultation != null) {
        navigator.push('NewConsultation_PaymentDetails', { consultationId })
      } else {
        console.error('Failed to add anamnesis', data.consultationAddAnamnesis)
        showNotification({
          title: 'Ошибка',
          message: 'Не удалось сохранить данные'
        })
      }
    },
    onError: error => {
      console.error('Failed to add anamnesis', error)
      showNotification({
        title: 'Ошибка',
        message: 'Не удалось сохранить данные'
      })
    }
  })

  const [fileUpload, { loading: loadingFileUpload }] = useFileUploadMutation({
    onCompleted: data => {
      if (data.fileUpload?.file == null) {
        console.error('Failed to add anamnesis', data.fileUpload)
        showNotification({
          title: 'Ошибка',
          message: 'Не удалось сохранить файл'
        })
      }
    },
    onError: error => {
      console.error('Failed to add anamnesis', error)
      showNotification({
        title: 'Ошибка',
        message: 'Не удалось сохранить файл'
      })
    }
  })

  const finishedFileUpload = (initialFiles: UploadedFile[], uri: string, id: string): void => {
    setFiles(initialFiles.map(file => {
      if (file.uri === uri) {
        return {
          ...file,
          id,
          uploaded: true
        }
      } else {
        return file
      }
    }))
  }

  function handleAddFilePressed (file: Document): void {
    if (!files.some(item => item.uri === file.uri)) {
      const RNFile = new ReactNativeFile({
        uri: file.uri,
        name: file.uri ?? 'file-name',
        type: file.type
      })
      const initialFiles = [
        ...files,
        {
          ...file,
          id: null,
          uploaded: false
        }]
      setFiles(initialFiles)
      fileUpload({
        variables: {
          uploaderId: user.id,
          entityId: consultationId,
          type: 'CONSULTATION',
          file: RNFile
        }
      }).then(result => {
        if (result.data?.fileUpload?.file?.id != null) {
          finishedFileUpload(initialFiles, file.uri, result.data.fileUpload.file.id)
        } else {
          showNotification({
            title: 'Ошибка',
            message: 'Не удалось сохранить файл'
          })
        }
      })
    } else {
      Alert.alert('Файл уже добавлен')
    }
  }

  function handleDeleteFilePressed (fileUri: string): void {
    setFiles(files.filter(file => file.uri !== fileUri))
  }

  return (
    <NewConsultationAnamnesisView
      initialValues={{
        anamnesis: currentConsultation.data?.consultationById?.anamnesis?.complaints ?? ''
      }}
      files={files}
      loading={loadingFileUpload}
      onAddFilePressed={handleAddFilePressed}
      onDeleteFilePressed={handleDeleteFilePressed}
      onSubmit={async (data) => {
        logNewConsultationAnamnesisEntered()
        await addAnamnesis({
          variables: {
            consultationId,
            content: {
              complaints: data.anamnesis,
              fileIds: files.map(item => item.id ?? '')
            }
          }
        })
        return {}
      }}
    />
  )
}
