import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { ClipIcon24 } from '../../../images/clip.icon-24'
import { DocumentIcon20 } from '../../../images/document.icon-20'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const AddAnamnesisContainer = styled(View, {
  ...container('padding'),
  paddingBottom: sp(24),
  backgroundColor: color('surface'),
  flex: 1
})

export const ScrollContainer = styled(ScrollView, {
  flex: 1
})

export const FilesTitleText = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  marginTop: sp(24),
  color: color('text')
})

export const AddFileButtonText = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  color: color('subtext'),
  marginLeft: sp(8)
})

export const FileNameText = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  maxWidth: '70%',
  color: color('text'),
  marginLeft: sp(8)
})

export const InputContainer = styled(View, {
  marginTop: sp(24)
})

export const AddFileButton = styled(TouchableOpacity, {
  marginTop: sp(12),
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: sp(16),
  paddingVertical: sp(16),
  borderRadius: sp(4),
  borderWidth: 1,
  borderColor: color('inactive'),
  borderStyle: 'dashed'
})

export const FileDeleteButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  padding: sp(8)
})

export const File = styled(View, {
  paddingHorizontal: sp(8),
  paddingVertical: sp(8),
  backgroundColor: color('background'),
  flexDirection: 'row',
  alignItems: 'center',
  borderRadius: sp(4),
  marginBottom: sp(4)
})

export const FilesContainer = styled(View, {
  marginTop: sp(12)
})

export const ClipImg = styled(ClipIcon24, {
  color: color('subtext')
})

export const DocumentImg = styled(DocumentIcon20, {
  color: color('subtext')
})
