import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import * as yup from 'yup'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { TextInput } from '../../form-kit'
import { SubmitTextButton } from '../../ui-kit'
import { Document, filePickerFn } from '../../ui-kit/image-picker'
import {
  AddAnamnesisContainer,
  InputContainer,
  ScrollContainer,
  FilesTitleText,
  FilesContainer,
  File,
  FileNameText,
  FileDeleteButton,
  AddFileButton,
  AddFileButtonText
} from './new-consultation-anamnesis.style'
import { DocumentIcon20 } from '../../../images/document.icon-20'
import { ClipIcon24 } from '../../../images/clip.icon-24'
import { CloseIconSvg } from '../../comments/comment-creator/comment-creator.style'

export interface UploadedDocument extends Document {
  uploaded: boolean
}

const maxComplaintsLength = 10000

const ATTACHMENT_LIMIT_COUNT = 10

const formSchema = yup.object({
  anamnesis: yup.string().trim()
    .max(maxComplaintsLength)
    .required()
}).required()

const { Form, Submit, Field } = createFormDescriptor(formSchema)

export type AnamnesisData = yup.InferType<typeof formSchema>

interface NewConsultationAnamnesisProps {
  files: UploadedDocument[]
  loading: boolean
  onAddFilePressed: (file: Document) => void
  onDeleteFilePressed: (fileUri: string) => void
  onSubmit: SubmissionHandler<AnamnesisData>
  initialValues: Partial<AnamnesisData>
}

interface AnamnesisFileViewProps {
  fileName: string
  uploaded: boolean
  onDeleteFilePressed: () => void
}

function AnamnesisFileView ({
  fileName, uploaded, onDeleteFilePressed
}: AnamnesisFileViewProps): React.ReactElement {
  return (
    <File>
      {uploaded
        ? <DocumentIcon20 />
        : <LoadingIndicator size='normal' visible />}
      <FileNameText
        numberOfLines={1}
        ellipsizeMode='tail'
      >
        {fileName}
      </FileNameText>
      <FileDeleteButton
        onPress={onDeleteFilePressed}
      >
        <CloseIconSvg />
      </FileDeleteButton>
    </File>
  )
}

export function NewConsultationAnamnesisView (
  {
    initialValues,
    files,
    loading,
    onAddFilePressed,
    onDeleteFilePressed,
    onSubmit
  }: NewConsultationAnamnesisProps
): React.ReactElement {
  return (
    <Form
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      <AddAnamnesisContainer>
        <ScrollContainer>
          <InputContainer>
            <Field name='anamnesis'>
              {props => (
                <TextInput
                  {...props}
                  label='Жалобы'
                  placeholder='Например, температура тела 38,5ºС, насморк, кашель. Болела голова, было общее недомогание.'
                  numberOfLines={[3, 12]}
                  maxLength={maxComplaintsLength}
                />
              )}
            </Field>
          </InputContainer>
          <FilesTitleText>
            Файлы
          </FilesTitleText>
          {files.length > 0
            ? (
              <FilesContainer>
                {files.map(file => {
                  return (
                    <AnamnesisFileView
                      key={file.uri}
                      fileName={file.fileName ?? 'file-name'}
                      uploaded={file.uploaded}
                      onDeleteFilePressed={() => onDeleteFilePressed(file.uri)}
                    />
                  )
                })}
              </FilesContainer>
            )
            : null}
          {files.length >= ATTACHMENT_LIMIT_COUNT ? null : (
            <AddFileButton
              onPress={() => {
                filePickerFn({
                  type: 'document',
                  onDocumentPicked: data => onAddFilePressed(data)
                })
              }}
            >
              <ClipIcon24 />
              <AddFileButtonText>
                Добавить (например, результаты МРТ)
              </AddFileButtonText>
            </AddFileButton>
          )}
        </ScrollContainer>
        <Submit>
          {props => <SubmitTextButton {...props} valid={!loading && props.valid} size='XL' type='primary'>Далее</SubmitTextButton>}
        </Submit>
        <Submit>
          {props => <ModalLoadingIndicator visible={props.submitting} />}
        </Submit>
      </AddAnamnesisContainer>
    </Form>
  )
}
