import { DataProxy } from 'apollo-cache'
import {
  DoctorConsultationRowDataFragment,

  ListDoctorConsultationsQuery,
  ListDoctorConsultationsQueryVariables,
  ListDoctorConsultationsDocument,

  ListFreeConsultationsQuery,
  ListFreeConsultationsQueryVariables,
  ListFreeConsultationsDocument

} from '../../../gen/graphql'

export function newDoctorSiteConsultation (cache: DataProxy, userId: string, entry: DoctorConsultationRowDataFragment): void {
  try {
    const doctorConsultations = cache.readQuery<ListDoctorConsultationsQuery, ListDoctorConsultationsQueryVariables>({
      query: ListDoctorConsultationsDocument,
      variables: { userId }
    })
    if (doctorConsultations?.userMyself?.consultationsAsDoctor != null) {
      cache.writeQuery<ListDoctorConsultationsQuery, ListDoctorConsultationsQueryVariables>({
        query: ListDoctorConsultationsDocument,
        variables: { userId },
        data: {
          ...doctorConsultations,
          userMyself: {
            ...doctorConsultations.userMyself,
            consultationsAsDoctor: {
              ...doctorConsultations.userMyself.consultationsAsDoctor,
              edges: [
                { node: entry, __typename: 'ConsultationEdge' },
                ...doctorConsultations.userMyself.consultationsAsDoctor.edges
              ]
            }
          }
        }
      })
    }
  } catch {
    console.warn('Failed to update doctor consultations cache')
  }
  try {
    const freeConsultations = cache.readQuery<ListFreeConsultationsQuery, ListFreeConsultationsQueryVariables>({
      query: ListFreeConsultationsDocument,
      variables: { userId }
    })
    if (freeConsultations?.userMyself?.freeConsultations != null) {
      cache.writeQuery<ListFreeConsultationsQuery, ListFreeConsultationsQueryVariables>({
        query: ListFreeConsultationsDocument,
        variables: { userId },
        data: {
          ...freeConsultations,
          userMyself: {
            ...freeConsultations.userMyself,
            freeConsultationsCount: freeConsultations.userMyself.freeConsultationsCount != null &&
            freeConsultations.userMyself.freeConsultationsCount > 0
              ? freeConsultations.userMyself.freeConsultationsCount - 1 : 0,
            freeConsultations: {
              ...freeConsultations.userMyself.freeConsultations,
              edges: [
                ...freeConsultations.userMyself.freeConsultations.edges.filter(
                  ({ node: { id } }) => { return id !== entry.id })
              ]
            }
          }
        }
      })
    }
  } catch {
    console.warn('Failed to update free consultations cache')
  }
}
