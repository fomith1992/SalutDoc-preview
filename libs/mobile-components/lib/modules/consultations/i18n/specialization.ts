export const specializationName: Record<string, string> = {
  cardiologist: 'Кардиолог',
  dentist: 'Стоматолог',
  psychologist: 'Психолог',
  therapist: 'Терапевт',
  gynecologist: 'Гинеколог',
  pediatrist: 'Педиатр',
  dermatologist: 'Дерматолог',
  surgeon: 'Хирург',
  gastroenterologist: 'Гастроэнтеролог',
  ophthalmologist: 'Офтальмолог',
  neurologist: 'Невролог',
  otolaryngologist: 'Отоларинголог',
  allergologist: 'Аллерголог-иммунолог',
  venereologist: 'Дерматовенеролог'
}

export const consultationWithSpecialization: Record<string, string> = {
  cardiologist: 'кардиологом',
  dentist: 'стоматологом',
  psychologist: 'психологом',
  therapist: 'терапевтом',
  gynecologist: 'гинекологом',
  pediatrist: 'педиатром',
  dermatologist: 'дерматологом',
  surgeon: 'хирургом',
  gastroenterologist: 'гастроэнтерологом',
  ophthalmologist: 'офтальмологом',
  neurologist: 'неврологом',
  otolaryngologist: 'отоларингологом',
  allergologist: 'аллергологом-иммунологом',
  venereologist: 'дерматовенерологом'
}

export const consultationToDoctor: Record<string, string> = {
  cardiologist: 'К кардиологу',
  dentist: 'К стоматологу',
  psychologist: 'К психологу',
  therapist: 'К терапевту',
  gynecologist: 'К гинекологу',
  pediatrist: 'К педиатру',
  dermatologist: 'К дерматологу',
  surgeon: 'К хирургу',
  gastroenterologist: 'К гастроэнтерологу',
  ophthalmologist: 'К офтальмологу',
  neurologist: 'К неврологу',
  otolaryngologist: 'К отоларингологу',
  allergologist: 'К аллергологу-иммунологу',
  venereologist: 'К дерматовенерологу'
}
