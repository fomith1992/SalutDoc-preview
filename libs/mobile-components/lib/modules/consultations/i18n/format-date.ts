import moment from 'moment'

export const formatDate = (date: Date | string): string => moment(date).format('D MMM')
