import React from 'react'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useAuthenticatedUserOrNull } from '../../../components/user-context/user-context-provider'
import { useCreateConsultationMutation } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { useShowNotification } from '../../ui-kit/notifications'
import { useLogNewConsultationSpecializationChosen } from '../analytics'
import { NewConsultationSpecializationView } from './new-consultation-specialization.view'

export function NewConsultationSpecialization (): React.ReactElement {
  const user = useAuthenticatedUserOrNull()
  const navigation = useStackNavigation()
  const showNotification = useShowNotification()
  const logNewConsultationSpecializationChosen = useLogNewConsultationSpecializationChosen()
  const [createConsultation, { loading }] = useCreateConsultationMutation({
    onCompleted: data => {
      if (data.consultationCreate.consultation != null) {
        navigation.push('NewConsultation_Anamnesis', {
          consultationId: data.consultationCreate.consultation.id
        })
      } else {
        console.error('Failed to create consultation', data.consultationCreate)
        showNotification({
          title: 'Ошибка',
          message: 'Не удалось создать консультацию'
        })
      }
    },
    onError: error => {
      console.error('Failed to create consultation', error)
      showNotification({
        title: 'Ошибка',
        message: 'Не удалось создать консультацию'
      })
    }
  })

  // Todo: When you instantly switch to this screen (after login) the user may not yet be determined
  if (user == null) {
    return (
      <LoadingIndicator visible />
    )
  }

  return (
    <NewConsultationSpecializationView
      specializations={['therapist', 'pediatrist', 'dermatologist', 'gynecologist', 'cardiologist', 'neurologist', 'surgeon', 'venereologist', 'ophthalmologist', 'otolaryngologist', 'gastroenterologist', 'allergologist']}
      loading={loading}
      onSpecializationPressed={specialization => {
        logNewConsultationSpecializationChosen(specialization)
        createConsultation({
          variables: {
            userId: user.id,
            specialization
          }
        }).catch(/* handled in hook */)
      }}
    />
  )
}
