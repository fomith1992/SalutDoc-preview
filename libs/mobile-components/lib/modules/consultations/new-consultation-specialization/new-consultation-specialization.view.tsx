import React from 'react'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { specializationName } from '../i18n/specialization'
import {
  ChoosingSpecialityContainer,
  RecommendationText,
  SpecializationIconContainer,
  SpecializationName,
  SpecializationRow,
  SpecializationImg,
  SpecializationsContainer
} from './new-consultation-specialization.style'

interface NewConsultationSpecializationViewProps {
  specializations: string[]
  loading: boolean

  onSpecializationPressed: (specialization: string) => void
}

export function NewConsultationSpecializationView (
  {
    specializations,
    loading,
    onSpecializationPressed
  }: NewConsultationSpecializationViewProps
): React.ReactElement {
  return (
    <ChoosingSpecialityContainer>
      <SpecializationsContainer>
        {specializations.map(specialization => (
          <SpecializationRow
            key={specialization}
            onPress={() => onSpecializationPressed(specialization)}
          >
            <SpecializationIconContainer>
              <SpecializationImg specialization={specialization} />
            </SpecializationIconContainer>
            <SpecializationName>
              {specializationName[specialization] ?? specialization}
            </SpecializationName>
          </SpecializationRow>
        ))}
      </SpecializationsContainer>
      <RecommendationText>
        Если не знаете к какому врачу записаться, тогда запишитесь к терапевту. Он выслушает ваши жалобы и поможет выбрать подходящего врача.
      </RecommendationText>
      <ModalLoadingIndicator visible={loading} />
    </ChoosingSpecialityContainer>
  )
}
