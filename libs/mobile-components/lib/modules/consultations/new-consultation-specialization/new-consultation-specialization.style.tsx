import { Text, TouchableOpacity, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { SpecializationIcon } from '../specialization-icon'

export const ChoosingSpecialityContainer = styled(ScrollView, {
  ...container('padding'),
  flex: 1,
  backgroundColor: color('surface')
})

export const SpecializationRow = styled(TouchableOpacity, {
  flexDirection: 'row',
  paddingVertical: sp(8),
  alignItems: 'center'
})

export const SpecializationsContainer = styled(View, {
  marginTop: sp(12)
})

export const SpecializationIconContainer = styled(View, {
  width: sp(56),
  height: sp(56),
  borderRadius: sp(28),
  backgroundColor: color('secondaryLight'),
  justifyContent: 'center',
  alignItems: 'center'
})

export const SpecializationName = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginLeft: sp(16)
})

export const RecommendationText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('inactive'),
  marginVertical: sp(12)
})

export const SpecializationImg = styled(SpecializationIcon, {
  color: color('accent'),
  width: sp(32),
  height: sp(32)
})
