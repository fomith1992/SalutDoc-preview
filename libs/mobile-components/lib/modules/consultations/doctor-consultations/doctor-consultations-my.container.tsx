import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { Text } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useListDoctorConsultationsQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { DoctorConsultationPreviewRow } from '../consultation-preview-row'
import { DoctorConsultationsView } from './doctor-consultations.view'

export function DoctorConsultationsMy (): React.ReactElement {
  const user = useAuthenticatedUser()
  const { data, loading, fetchMore, refetch } = useListDoctorConsultationsQuery({
    variables: {
      userId: user.id
    },
    notifyOnNetworkStatusChange: true
  })
  const { refresh, refreshing } = useRefresh(refetch)

  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => ({
        userMyself: {
          ...expectDefined(previousResult.userMyself),
          consultationsAsDoctor: appendEdges(
            expectDefined(previousResult.userMyself?.consultationsAsDoctor),
            fetchMoreResult?.userMyself?.consultationsAsDoctor
          )
        }
      })
    }).catch(error => {
      console.log(error)
    })
  })

  const consultations = data?.userMyself?.consultationsAsDoctor
  if (consultations == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }

  return (
    <DoctorConsultationsView
      isMy
      data={data?.userMyself?.consultationsAsDoctor?.edges ?? []}
      extractKey={item => item.node.id}
      renderItem={item => <DoctorConsultationPreviewRow key={item.node.id} consultationId={item.node.id} />}
      refreshing={refreshing}
      loading={loading && !refreshing}
      hasMore={data?.userMyself?.consultationsAsDoctor?.pageInfo.hasNextPage ?? false}
      refresh={refresh}
      loadMore={() => loadMore(consultations.pageInfo.endCursor)}
    />
  )
}
