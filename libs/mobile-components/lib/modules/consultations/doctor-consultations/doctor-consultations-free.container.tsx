import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { Text } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useListFreeConsultationsQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { useRefresh } from '../../../hooks/use-refresh'
import { DoctorConsultationPreviewRow } from '../consultation-preview-row'
import { DoctorConsultationsView } from './doctor-consultations.view'

export function DoctorConsultationsFree (): React.ReactElement {
  const user = useAuthenticatedUser()
  const { data, loading, fetchMore, refetch } = useListFreeConsultationsQuery({
    variables: {
      userId: user.id
    },
    notifyOnNetworkStatusChange: true
  })
  const { refresh, refreshing } = useRefresh(refetch)
  const loadMore = useMemoizedFn((after: string) => {
    fetchMore({
      variables: { after },
      updateQuery: (previousResult, { fetchMoreResult }) => ({
        userMyself: {
          ...expectDefined(previousResult.userMyself),
          freeConsultations: appendEdges(
            expectDefined(previousResult.userMyself?.freeConsultations),
            fetchMoreResult?.userMyself?.freeConsultations
          )
        }
      })
    }).catch(error => {
      console.log(error)
    })
  })
  const consultations = data?.userMyself?.freeConsultations
  if (consultations == null) {
    return loading
      ? <LoadingIndicator visible />
      : <Text>Произошла ошибка</Text>
  }
  return (
    <DoctorConsultationsView
      isMy={false}
      data={data?.userMyself?.freeConsultations?.edges ?? []}
      extractKey={item => item.node.id}
      renderItem={item => <DoctorConsultationPreviewRow consultationId={item.node.id} />}
      refreshing={refreshing}
      loading={loading && !refreshing}
      hasMore={data?.userMyself?.freeConsultations?.pageInfo.hasNextPage ?? false}
      refresh={refresh}
      loadMore={() => loadMore(consultations.pageInfo.endCursor)}
    />
  )
}
