import { Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { lift, styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { SpecializationIcon } from '../specialization-icon'

export const listStyleSheet = lift({
  backgroundColor: color('surface')
})

export const listContentContainerStyleSheet = lift({
  paddingVertical: sp(12)
})

export const EmptyListContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
})

export const SpecializationImg = styled(SpecializationIcon, (props: {active?: boolean}) => ({
  color: color(props.active ?? false ? 'accent' : 'subtext')
}))

export const EmptyListTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(32)
})

export const EmptyListDescription = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(12),
  marginBottom: sp(24)
})
