import React from 'react'
import { FlatList } from 'react-native'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useMaterialized } from '../../../style/styled'
import {
  EmptyListContainer,
  EmptyListDescription,
  EmptyListTitle,
  listContentContainerStyleSheet,
  listStyleSheet
} from './doctor-consultations.style'

interface ConsultationDoctorViewProps<T> {
  data: T[]
  extractKey: (item: T) => string
  renderItem: (item: T) => React.ReactElement

  isMy: boolean
  refreshing: boolean
  loading: boolean
  hasMore: boolean
  refresh: () => void
  loadMore: () => void
}

export function DoctorConsultationsView<T> (
  {
    data,
    extractKey,
    renderItem,
    isMy,
    refreshing,
    loading,
    hasMore,
    refresh,
    loadMore
  }: ConsultationDoctorViewProps<T>
): React.ReactElement {
  const listStyle = useMaterialized(listStyleSheet)
  const listContentContainerStyle = useMaterialized(listContentContainerStyleSheet)
  const emptyListTitle = isMy
    ? 'Здесь будут ваши консультации'
    : 'Здесь будут все консультации'
  const emptyListDescription = isMy
    ? 'Заходите сюда для ответа пациентам'
    : 'Заходите сюда для выбора пациентов'

  return (
    <FlatList
      style={listStyle}
      contentContainerStyle={listContentContainerStyle}
      ListEmptyComponent={() => (
        <EmptyListContainer>
          <EmptyListTitle>
            {emptyListTitle}
          </EmptyListTitle>
          <EmptyListDescription>
            {emptyListDescription}
          </EmptyListDescription>
        </EmptyListContainer>
      )}
      data={data}
      keyExtractor={extractKey}
      renderItem={({ item }) => renderItem(item)}
      onEndReached={hasMore ? loadMore : null}
      refreshing={refreshing}
      onRefresh={refresh}
      ListFooterComponent={<LoadingIndicator visible={loading} />}
    />
  )
}
