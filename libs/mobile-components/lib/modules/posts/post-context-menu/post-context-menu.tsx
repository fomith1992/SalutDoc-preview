import React, { useMemo } from 'react'
import { Alert } from 'react-native'
import { RequestModal } from '../../../components/request-modal/request-modal.container'
import { useArchivePostMutation, usePostContextMenuQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { ContextMenu, ContextMenuButton, shareUrl } from '../../ui-kit'
import { expectDefined } from '@salutdoc/react-components'

export interface PostContextMenuProps {
  visible: boolean
  postId: string
  onHideMenu: () => void
}

export function PostContextMenu (
  {
    visible,
    postId,
    onHideMenu
  }: PostContextMenuProps
): React.ReactElement {
  const navigation = useStackNavigation()
  const { data } = usePostContextMenuQuery({
    variables: { postId }
  })
  const [archivePostMutation] = useArchivePostMutation()
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)
  const mobileHostName = expectDefined(process.env.MOBILE_HOST_NAME)

  const archivePost = (): void => {
    try {
      archivePostMutation({
        variables: {
          postId
        }
      })
      navigation.goBack()
    } catch {
      Alert.alert('Error', 'Failed to delete post')
    }
  }

  const buttons = useMemo(() => {
    const buttons: ContextMenuButton[] = []
    if (data?.postById?.canEdit === true) {
      buttons.push({
        type: 'black',
        title: 'Редактировать запись',
        onPress: () => {
          onHideMenu()
          navigation.push('PostEditor', { postId })
        }
      })
    }
    if (data?.postById?.canArchive === true) {
      buttons.push({
        type: 'black',
        title: 'Удалить запись',
        onPress: () => {
          Alert.alert(
            'Предупреждение!',
            'Вы уверены, что хотите удалить запись?',
            [
              {
                text: 'Отмена',
                onPress: onHideMenu
              },
              {
                text: 'Да',
                onPress: () => {
                  onHideMenu()
                  archivePost()
                }
              }
            ]
          )
        }
      })
    }
    buttons.push(
      {
        type: 'black',
        title: 'Поделиться',
        onPress: () => {
          onHideMenu()
          shareUrl(`https://${mobileHostName}/post/${postId}`)
        }
      },
      {
        type: 'black',
        title: 'Пожаловаться',
        onPress: () => {
          onHideMenu()
          showReportModal()
        }
      },
      {
        type: 'red',
        title: 'Отмена',
        onPress: onHideMenu
      }
    )
    return buttons
  }, [data])

  return (
    <>
      <ContextMenu
        visible={visible}
        onClose={onHideMenu}
        buttons={buttons}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='post'
        contentId={postId}
      />
    </>
  )
}
