import { Text, TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const PostContentText = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('text'),
  marginBottom: sp(12)
})

export const PostContentTextHashtag = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('accent')
})

export const ButtonShow = styled(TouchableOpacity, {
  height: sp(12)
})

export const ButtonShowText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('inactive')
})
