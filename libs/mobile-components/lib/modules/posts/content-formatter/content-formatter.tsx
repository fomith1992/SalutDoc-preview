import React, { useState } from 'react'
import {
  ButtonShowText,
  ButtonShow,
  PostContentText,
  PostContentTextHashtag
} from './content-formatter.style'

export interface ContentFormatterProps {
  children: string
  maxTextlimit?: number
}

export function ContentFormatter ({
  children,
  maxTextlimit = 99999
}: ContentFormatterProps): React.ReactElement {
  const [textLimit, setTextLimit] = useState(maxTextlimit)
  const text = children.length > textLimit
    ? children.substring(0, textLimit - 3) + '... '
    : children
  return (
    <PostContentText>
      {text
        .split(/(^|\s)(#[a-z][\w-]*)($|\s)/i)
        .filter(Boolean)
        .map((v, index) => {
          if (/^#[a-z][\w-]*$/i.test(v)) {
            return <PostContentTextHashtag key={index}>{v}</PostContentTextHashtag>
          } else {
            return v
          }
        })}
      {children.length > textLimit
        ? (
          <ButtonShow
            onPress={() => setTextLimit(children.length)}
          >
            <ButtonShowText>
              Ещё
            </ButtonShowText>
          </ButtonShow>
        )
        : null}
    </PostContentText>
  )
}
