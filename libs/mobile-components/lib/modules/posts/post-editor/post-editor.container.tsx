import { useNavigation } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { Alert } from 'react-native'
import { useEditablePostQuery, useEditPostMutation } from '../../../gen/graphql'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { PostComposerFormData, PostComposerView } from '../post-composer'
import { parseAttachments } from '../utils'

interface PostEditorProps {
  postId: string
}

export const PostEditor = ({ postId }: PostEditorProps): React.ReactElement => {
  const navigation = useNavigation()

  const { data, loading, error } = useEditablePostQuery({
    variables: { id: postId }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  const [editPost, { loading: submittingPost }] = useEditPostMutation({
    onCompleted: result => {
      const { post, message, errors } = result.postEdit
      if (post != null) {
        console.log('Запись отредактирована', message, post)
        navigation.goBack()
      } else {
        console.log('Не удалось отредактировать запись', message, errors)
        Alert.alert('Ошибка', 'Не удалось отредактировать запись')
      }
    }
  })

  const handleEditPost = async ({ text, subject, attachments }: PostComposerFormData): Promise<void> => {
    await editPost({
      variables: {
        postId: postId,
        content: {
          subject,
          tags: [],
          text,
          attachments: parseAttachments(attachments)
        }
      }
    })
  }

  return loading
    ? <LoadingIndicator visible />
    : (
      <PostComposerView
        submitting={submittingPost}
        initialValues={{
          text: data?.postById?.text,
          subject: data?.postById?.subject,
          attachments: data?.postById?.attachments.map(attachment => {
            switch (attachment.__typename) {
              case 'ImagePostAttachment':
                return {
                  url: attachment.url,
                  id: attachment.id,
                  type: 'IMAGE_REF'
                }
              case 'YouTubeVideoPostAttachment':
                return {
                  url: attachment.videoId,
                  type: 'YOUTUBE',
                  id: attachment.videoId
                }
            }
          })
        }}
        onSubmit={async values => {
          try {
            await handleEditPost(values)
          } catch (e) {
            console.log('Не удалось отредактировать запись', e)
            Alert.alert('Ошибка', 'Не удалось отредактировать запись')
          }
          return {}
        }}
      />
    )
}
