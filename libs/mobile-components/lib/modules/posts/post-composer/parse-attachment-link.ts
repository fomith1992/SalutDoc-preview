import URI from 'urijs'
import { Attachment } from './form-schema'

function parseYoutubeAttachmentFromParam (uri: URI): Attachment | null {
  if (
    (uri.hostname() === 'youtube.com' || uri.hostname() === 'www.youtube.com') &&
    uri.hasQuery('v') &&
    uri.pathname() === '/watch' &&
    uri.search(true).v?.length === 11
  ) {
    return {
      url: uri.search(true).v,
      id: uri.search(true).v,
      type: 'YOUTUBE'
    }
  }
  return null
}

function parseYoutubeAttachmentFromPath (uri: URI): Attachment | null {
  if (
    uri.hostname() === 'youtu.be' &&
    uri.pathname() !== ''
  ) {
    const segments = uri.segment()
    if (segments[0]?.length === 11 && segments.length === 1) {
      return {
        url: segments[0],
        id: segments[0],
        type: 'YOUTUBE'
      }
    }
  }
  return null
}

const parsers = [
  parseYoutubeAttachmentFromParam,
  parseYoutubeAttachmentFromPath
]

export function parseAttachmentLink (link: string): Attachment | null {
  const uri = new URI(link)
  for (const parse of parsers) {
    const result = parse(uri)
    if (result !== null) return result
  }
  return null
}
