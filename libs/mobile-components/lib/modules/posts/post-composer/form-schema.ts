import { createFormDescriptor } from '@salutdoc/react-components'
import { yup } from '../../form-kit/yup-errors-update'
import { ATTACHMENT_LIMIT_COUNT } from './post-composer.constants'

export const formSchema = yup.object({
  subject: yup.string()
    .required(),
  text: yup.string()
    .min(1).max(10000)
    .required(),
  attachments: yup.array()
    .max(ATTACHMENT_LIMIT_COUNT)
    .of(
      yup.object({
        url: yup.string()
          .required(),
        id: yup.string().required(),
        type: yup.mixed()
          .oneOf(['IMAGE', 'YOUTUBE', 'IMAGE_REF'] as const)
          .required()
      }).required()
    ).defined()
}).required()

export type Attachment = yup.InferType<typeof formSchema>['attachments'][number]

export type PostComposerFormData = yup.InferType<typeof formSchema>

export const { Form, Field, Submit } = createFormDescriptor(formSchema)
