import { SubmissionHandler } from '@salutdoc/react-components'
import anchorme from 'anchorme'
import _ from 'lodash'
import React, { useState } from 'react'
import { Alert } from 'react-native'
import { v4 as uuid } from 'uuid'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { SelectSearch } from '../../../modules/form-kit/select-search/select-search.view'
import { PostTextInput } from '../../form-kit'
import { ImagePicker } from '../../ui-kit'
import { specializationsData } from '../../ui-kit/specializations-data/specializations-data'
import { AttachmentsSliderView } from './attachment-slider.view'
import { Attachment, Field, Form, PostComposerFormData } from './form-schema'
import { parseAttachmentLink } from './parse-attachment-link'
import { PostCreatorFooterView } from './post-composer-footer.view'
import { PostCreatorContainer, ThemeInputContainer } from './post-composer.style'
import { filterNewAttachments } from './utils'
import { ATTACHMENT_LIMIT_COUNT } from './post-composer.constants'

export interface PostComposerViewProps {
  initialValues: Partial<PostComposerFormData>
  onSubmit: SubmissionHandler<PostComposerFormData>
  submitting: boolean
}

export function PostComposerView (
  {
    initialValues,
    onSubmit,
    submitting
  }: PostComposerViewProps
): React.ReactElement {
  const [attachmentsInText, setAttachmentsInText] = useState<Attachment[]>([])
  function addAttachmentFromText (
    text: string,
    attachments: Attachment[],
    setAttachments: (attachments: Attachment[]) => void
  ): void {
    const attachmentLinks = anchorme.list(text)
    const attachmentsInCurrentText: Attachment[] = attachmentLinks
      .map(link => parseAttachmentLink(link.string))
      .filter((x): x is Attachment => x != null)
    if (!_.isEqual(attachmentsInText, attachmentsInCurrentText)) {
      setAttachmentsInText(attachmentsInCurrentText)
      const newAttachmentsInCurrentText = filterNewAttachments(attachmentsInText, attachmentsInCurrentText)
      const newAttachments = filterNewAttachments(attachments, newAttachmentsInCurrentText)
      if (newAttachments.length !== 0) {
        setAttachments([...attachments, ...newAttachments])
      }
    }
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      <ThemeInputContainer>
        <Field name='subject'>
          {props => (
            <SelectSearch
              {...props}
              options={specializationsData}
              type='button'
            />)}
        </Field>
      </ThemeInputContainer>
      <PostCreatorContainer>
        <Field name='text'>
          {propsText => (
            <Field name='attachments'>
              {propsAttachments => {
                if (propsAttachments.value != null && propsAttachments.value?.length < ATTACHMENT_LIMIT_COUNT) {
                  addAttachmentFromText(propsText.value ?? '', propsAttachments.value, propsAttachments.onChange)
                }
                return <PostTextInput {...propsText} />
              }}
            </Field>
          )}
        </Field>
        <Field name='attachments'>
          {({ value, onChange }) => {
            return (
              <>
                <AttachmentsSliderView
                  attachments={value ?? []}
                  onDelete={(_, delIdx) => onChange(value?.filter((_, idx) => idx !== delIdx))}
                />
                <ImagePicker
                  title='Выберите вложение'
                  onImagePicked={image => onChange([...value ?? [], {
                    type: 'IMAGE',
                    id: uuid(),
                    url: image.uri
                  }])}
                >
                  {({ pickImage }) => (
                    <PostCreatorFooterView
                      onAddPhoto={() => {
                        if (value != null && value.length < ATTACHMENT_LIMIT_COUNT) {
                          pickImage()
                        } else {
                          Alert.alert(
                            'Ошибка',
                            `Количество вложений не должно превышать ${ATTACHMENT_LIMIT_COUNT} штук`
                          )
                        }
                      }}
                    />
                  )}
                </ImagePicker>
              </>
            )
          }}
        </Field>
      </PostCreatorContainer>
      <ModalLoadingIndicator visible={submitting} />
    </Form>
  )
}
