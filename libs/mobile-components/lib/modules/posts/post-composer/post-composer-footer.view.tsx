import React from 'react'
import { SubmitTextButton } from '../../ui-kit'
import { Submit } from './form-schema'
import { AddPhotoButton, AddPhotoIcon, PostCreatorFooter, PublishButtonContainer } from './post-composer.style'

interface PostCreatorFooterViewProps {
  onAddPhoto: () => void
}

export function PostCreatorFooterView ({ onAddPhoto }: PostCreatorFooterViewProps): React.ReactElement {
  return (
    <PostCreatorFooter>
      <AddPhotoButton
        onPress={onAddPhoto}
      >
        <AddPhotoIcon />
      </AddPhotoButton>
      <PublishButtonContainer>
        <Submit>
          {props => (
            <SubmitTextButton
              type='primary'
              {...props}
            >
              Опубликовать
            </SubmitTextButton>
          )}
        </Submit>
      </PublishButtonContainer>
    </PostCreatorFooter>
  )
}
