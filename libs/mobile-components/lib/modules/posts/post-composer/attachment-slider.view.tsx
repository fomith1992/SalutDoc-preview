import React from 'react'
import { AttachmentView } from './attachment.view'
import { Attachment } from './form-schema'
import { AttachmentsContainer, AttachmentsSlider } from './post-composer.style'

export interface AttachmentsSliderProps {
  attachments: Attachment[]
  onDelete: (attachment: Attachment, index: number) => void
}

export function AttachmentsSliderView (
  { attachments, onDelete }: AttachmentsSliderProps
): React.ReactElement {
  return (
    <AttachmentsContainer>
      <AttachmentsSlider
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {
          attachments.map(
            (attachment, index) =>
              <AttachmentView
                attachment={attachment}
                onDelete={() => onDelete(attachment, index)}
                key={index}
              />
          )
        }
      </AttachmentsSlider>
    </AttachmentsContainer>
  )
}
