import React from 'react'
import { Attachment } from './form-schema'
import {
  AttachmentNode,
  AttachmentNodeContainer,
  DeleteAttachmentContainer,
  DeleteIconContainer,
  PlayIconContainer,
  PlayVideoContainer,
  CrossImg,
  PlayImg
} from './post-composer.style'

export interface AttachmentViewProps {
  attachment: Attachment
  onDelete: () => void
}

export function AttachmentView (
  { attachment, onDelete }: AttachmentViewProps
): React.ReactElement {
  const uri = attachment.type === 'YOUTUBE'
    ? `https://img.youtube.com/vi/${attachment.url}/hqdefault.jpg` : attachment.url
  return (
    <AttachmentNodeContainer>
      <AttachmentNode
        source={{
          uri: uri ?? undefined
        }}
      />
      <DeleteAttachmentContainer>
        <DeleteIconContainer
          onPress={onDelete}
        >
          <CrossImg />
        </DeleteIconContainer>
      </DeleteAttachmentContainer>
      {attachment.type === 'YOUTUBE' ? (
        <PlayVideoContainer>
          <PlayIconContainer>
            <PlayImg />
            {/* TODO add video duration */}
          </PlayIconContainer>
        </PlayVideoContainer>
      ) : <></>}
    </AttachmentNodeContainer>
  )
}
