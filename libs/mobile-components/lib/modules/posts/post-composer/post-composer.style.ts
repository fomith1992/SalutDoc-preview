import { View, ScrollView, Image, Text, TouchableOpacity } from 'react-native'
import { PhotoIcon } from '../../../images/photo.icon'
import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { container } from '../../../style/view'
import { sp } from '../../../style/size'
import { font } from '../../../style/text'
import { PlayIcon24 } from '../../../images/play.icon-24'
import { CrossIcon16 } from '../../../images/cross.icon-16'

export const PostCreatorContainer = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})

export const PostCreatorFooter = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center',
  height: 46
})

export const PublishButtonContainer = styled(View, {
  marginLeft: 'auto'
})

export const AddPhotoButton = TouchableOpacity

export const AddPhotoIcon = styled(PhotoIcon, {
  width: sp(24),
  height: sp(24),
  color: color('accent')
})

export const AttachmentsContainer = styled(View, {
  backgroundColor: color('surface')
})

export const AttachmentsSlider = styled(ScrollView, {
  flexDirection: 'row',
  backgroundColor: color('surface')
})

export const AttachmentNodeContainer = View

export const AttachmentNode = styled(Image, {
  height: sp(96),
  width: sp(64),
  borderRadius: sp(8),
  marginLeft: sp(8)
})

export const DeleteAttachmentContainer = styled(View, {
  position: 'absolute',
  height: '100%',
  width: '100%',
  alignItems: 'flex-end',
  paddingRight: sp(4),
  paddingTop: sp(4)
})

export const DeleteIconContainer = styled(TouchableOpacity, {
  height: sp(16),
  width: sp(16),
  borderColor: color('surface'),
  borderWidth: 1,
  backgroundColor: color('surface'),
  borderRadius: sp(8),
  justifyContent: 'center',
  alignItems: 'center'
})

export const PlayVideoContainer = styled(View, {
  position: 'absolute',
  bottom: 0,
  paddingLeft: sp(12),
  paddingBottom: sp(4)
})

export const PlayIconContainer = styled(View, {
  height: sp(16),
  flexDirection: 'row',
  backgroundColor: 'rgba(165, 165, 165, 0.35)',
  borderRadius: sp(8),
  justifyContent: 'center',
  alignItems: 'center',
  paddingLeft: sp(4),
  paddingRight: sp(4)
})

export const DurationVideo = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('surface'),
  marginLeft: sp(4)
})

export const ThemeInputContainer = styled(View, {
  ...container('padding'),
  paddingTop: sp(8),
  backgroundColor: color('surface')
})

export const CrossImg = styled(CrossIcon16, {
  width: sp(12),
  height: sp(12),
  color: color('subtext')
})

export const PlayImg = styled(PlayIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('background')
})
