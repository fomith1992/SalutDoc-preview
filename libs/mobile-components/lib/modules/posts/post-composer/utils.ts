import { Attachment } from './form-schema'

export function filterNewAttachments (
  oldAttachment: Attachment[],
  newAttachment: Attachment[]
): Attachment[] {
  return newAttachment.filter(item =>
    oldAttachment.every(o => o.type !== item.type || o.id !== item.id)
  )
}
