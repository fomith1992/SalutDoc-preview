import { Platform, Text, TouchableOpacity, View } from 'react-native'
import { ElasticText } from '../../../components/elastic-text/elastic-text'
import { ChatCloudIcon20 } from '../../../images/chat-cloud.icon-20'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const AvatarContainer = styled(View, {
  width: 40,
  height: 40
})

export const PostTitleContainer = styled(View, {
  ...container(),
  flexShrink: 1,
  flexDirection: 'column'
})

export const SubjectContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-start'
})

export const PostContentAttachments = styled(View, {
  ...container()
})

export const Dot = styled(View, {
  width: 4,
  height: 4,
  borderRadius: 2,
  backgroundColor: color('inactive'),
  marginHorizontal: sp(8)
})

export const AuthorName = styled(ElasticText, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const PostSubject = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('inactive')
})

export const PublicDate = styled(Text, {
  ...font({ type: 'text2' }),
  color: color('inactive')
})

export const PostContentTouchableContainer = styled(TouchableOpacity, {
  ...container(),
  marginTop: sp(12),
  paddingBottom: 2
})

/* export const ButtonShow = TouchableOpacity

export const ButtonShowText = styled(Text, {
  ...font({ type: 'h3' }),
  color: color('inactive')
}) */

export const PostButtonsContainer = styled(View, {
  ...container(),
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const PostButtonCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'h3', weight: 'light' }),
  color: color(props.isPressed ? 'accent' : 'inactive'),
  lineHeight: 16,
  textAlign: 'center',
  marginLeft: 4
}) as const)

export const PostContainer = styled(View, {
  paddingTop: sp(16),
  backgroundColor: color('surface'),
  marginBottom: sp(12),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const AuthorContainer = styled(TouchableOpacity, {
  ...container(),
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  justifyContent: 'center'
})

export const CommentButton = styled(TouchableOpacity, {
  padding: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const AttachmentsContainer = styled(View, {
  flex: 1
})

export const AttachmentContainer = styled(View, {
  flex: 1,
  marginLeft: sp(8),
  justifyContent: 'center'
})

export const CountTailAttachments = styled(Text, {
  ...font({ type: 'h2', weight: 'light' }),
  color: color('surface')
})

export const MoreSvg = styled(VerticalDotsIcon24, {
  color: color('inactive')
})

export const ChatCloudImg = styled(ChatCloudIcon20, {
  color: color('inactive')
})
