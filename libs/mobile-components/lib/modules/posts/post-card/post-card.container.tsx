import React from 'react'
import { LikePost } from '../../../components/likes/like.container'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { usePostCardQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { PostContextMenu } from '../post-context-menu'
import { PostCardView } from './post-card.view'

export interface PostCardProps {
  id: string
}

export function PostCard (
  {
    id
  }: PostCardProps
): React.ReactElement {
  const navigator = useStackNavigation()
  const { data, loading } = usePostCardQuery({
    variables: { id }
  })
  const { state: contextMenuVisible, setTrue: showContextMenu, setFalse: hideContextMenu } = useBooleanFlag(false)

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.postById != null && !data.postById.archived) {
    const { postById: post } = data

    const authorName = post.author.__typename === 'User'
      ? `${post.author.firstName} ${post.author.lastName}`
      : post.author.name

    return (
      <PostCardView
        authorName={authorName}
        authorImg={post.author.avatar}
        createdAt={post.createdAt}
        text={post.text}
        subject={post.subject}
        attachments={post.attachments}
        commentCount={post.commentsCount}
        onPostPressed={() => navigator.push('PostComments', { postId: post.id })}
        onCommentsPressed={() => navigator.push('PostComments', { postId: id })}
        onAuthorPressed={() => navigator.push('Profile', { userId: post.author.id })}
        wireLikes={renderLikes => <LikePost id={id} render={renderLikes} />}
        onShowContextMenu={showContextMenu}
        contextMenu={
          <PostContextMenu
            visible={contextMenuVisible}
            postId={id}
            onHideMenu={hideContextMenu}
          />
        }
      />
    )
  } else {
    return <></>
  }
}
