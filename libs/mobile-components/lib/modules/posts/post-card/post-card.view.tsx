import React from 'react'
import { AttachmentProps, Attachments } from '../../../components/attachments/secondary-attachment.view'
import { Avatar } from '../../../components/avatar/avatar.view'
import { RenderLikeButtonView } from '../../../components/likes/like.container'
import { LikeButtonView } from '../../../components/likes/like.view'
import { ContentFormatter } from '../content-formatter'
import {
  AuthorContainer,
  AuthorName,
  AvatarContainer,
  CommentButton,
  MoreButton,
  PostButtonCount,
  PostButtonsContainer,
  PostContainer,
  PostContentTouchableContainer,
  PostSubject,
  PostTitleContainer,
  Dot,
  PublicDate,
  SubjectContainer,
  PostContentAttachments,
  MoreSvg,
  ChatCloudImg
} from './post-card.style'
import { postDateFormatter } from '../post-date-formatter/post-date-formatter'
import { hitSlopParams } from '../../ui-kit'

export interface PostViewProps {
  authorName: string
  commentCount: number
  text: string
  subject: string
  createdAt: string
  authorImg: string | null
  attachments: AttachmentProps[]
  wireLikes: (render: RenderLikeButtonView) => React.ReactElement
  contextMenu: React.ReactElement

  onPostPressed: () => void
  onCommentsPressed: () => void
  onAuthorPressed: () => void
  onShowContextMenu: () => void
}

const maxTextlimit = 150

export function PostCardView (
  {
    authorName,
    commentCount,
    text,
    subject,
    authorImg,
    contextMenu,
    attachments,
    createdAt,
    onPostPressed,
    onCommentsPressed,
    onAuthorPressed,
    onShowContextMenu,
    wireLikes
  }: PostViewProps
): React.ReactElement {
  return (
    <PostContainer>
      <AuthorContainer onPress={onAuthorPressed}>
        <AvatarContainer>
          <Avatar shape='circle' url={authorImg} />
        </AvatarContainer>
        <PostTitleContainer>
          <AuthorName
            numberOfLines={2}
          >
            {authorName}
          </AuthorName>
          <SubjectContainer>
            <PostSubject>
              {subject}
            </PostSubject>
            <Dot />
            <PublicDate>
              {postDateFormatter(createdAt)}
            </PublicDate>
          </SubjectContainer>
        </PostTitleContainer>
        <MoreButton
          hitSlop={hitSlopParams(16)}
          onPress={onShowContextMenu}
        >
          <MoreSvg />
          {contextMenu}
        </MoreButton>
      </AuthorContainer>
      <PostContentTouchableContainer onPress={onPostPressed}>
        <ContentFormatter
          maxTextlimit={maxTextlimit}
        >{text}
        </ContentFormatter>
      </PostContentTouchableContainer>
      <PostContentAttachments>
        {attachments.length === 0 ? <></> : <Attachments attachments={attachments} />}
      </PostContentAttachments>
      <PostButtonsContainer>
        {wireLikes(props => (
          <LikeButtonView
            {...props}
          />
        ))}
        <CommentButton onPress={onCommentsPressed}>
          <ChatCloudImg />
          <PostButtonCount isPressed={false}>{commentCount > 0 ? commentCount : ''}</PostButtonCount>
        </CommentButton>
      </PostButtonsContainer>
    </PostContainer>
  )
}
