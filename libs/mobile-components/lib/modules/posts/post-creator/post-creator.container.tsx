import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { Alert } from 'react-native'
import {
  useCreateCommunityPostMutation,
  useCreateServicePostMutation,
  useCreateUserPostMutation
} from '../../../gen/graphql'
import { feedCacheNewPost } from '../../feed/cache'
import { PostComposerFormData, PostComposerView } from '../post-composer'
import { parseAttachments } from '../utils'

export interface PostCreatorProps {
  siteType: 'USER' | 'COMMUNITY' | 'SERVICE'
  siteId: string
}

export function PostCreator ({ siteType, siteId }: PostCreatorProps): React.ReactElement {
  const navigation = useNavigation()

  const [createUserPost, { loading: submittingUserPost }] = useCreateUserPostMutation({
    onCompleted: (response) => {
      const { post, message, errors } = response.postCreateForUser
      if (post == null) {
        console.log('Не удалось создать запись', message, errors)
        Alert.alert('Ошибка', 'Не удалось создать запись')
      } else {
        console.log('Запись создана', message, post)
        navigation.goBack()
      }
    },
    update (cache, { data }) {
      const post = data?.postCreateForUser.post
      if (post == null) {
        return
      }
      feedCacheNewPost(cache, post)
    }
  })

  const [createCommunityPost, { loading: submittingCommunityPost }] = useCreateCommunityPostMutation({
    onCompleted: (response) => {
      const { post, message, errors } = response.postCreateForCommunity
      if (post == null) {
        console.log('Не удалось создать запись', message, errors)
        Alert.alert('Ошибка', 'Не удалось создать запись')
      } else {
        console.log('Запись создана', message, post)
        navigation.goBack()
      }
    },
    update (cache, { data }) {
      const post = data?.postCreateForCommunity.post
      if (post == null) {
        return
      }
      feedCacheNewPost(cache, post)
    }
  })

  const [createServicePost, { loading: submittingServicePost }] = useCreateServicePostMutation({
    onCompleted: (response) => {
      const { post, message, errors } = response.postCreateForService
      if (post == null) {
        console.log('Не удалось создать запись', message, errors)
        Alert.alert('Ошибка', 'Не удалось создать запись')
      } else {
        console.log('Запись создана', message, post)
        navigation.goBack()
      }
    },
    update (cache, { data }) {
      const post = data?.postCreateForService.post
      if (post == null) {
        return
      }
      feedCacheNewPost(cache, post)
    }
  })

  let handleCreatePost: (content: PostComposerFormData) => Promise<void>
  switch (siteType) {
    case 'USER':
      handleCreatePost = async ({ text, subject, attachments }: PostComposerFormData) => {
        await createUserPost({
          variables: {
            userId: siteId,
            content: {
              subject,
              tags: [],
              text,
              attachments: parseAttachments(attachments)
            }
          }
        })
      }
      break
    case 'COMMUNITY':
      handleCreatePost = async ({ text, subject, attachments }: PostComposerFormData) => {
        await createCommunityPost({
          variables: {
            communityId: siteId,
            content: {
              subject,
              tags: [],
              text,
              attachments: parseAttachments(attachments)
            }
          }
        })
      }
      break
    case 'SERVICE':
      handleCreatePost = async ({ text, subject, attachments }: PostComposerFormData) => {
        await createServicePost({
          variables: {
            userId: siteId,
            content: {
              subject,
              tags: [],
              text,
              attachments: parseAttachments(attachments)
            }
          }
        })
      }
      break
  }

  return (
    <PostComposerView
      initialValues={{
        text: '',
        attachments: []
      }}
      submitting={submittingUserPost || submittingCommunityPost || submittingServicePost}
      onSubmit={async values => {
        try {
          await handleCreatePost(values)
        } catch (e) {
          console.log('Не удалось создать запись', e)
          Alert.alert('Ошибка', 'Не удалось создать запись')
        }
        return {}
      }}
    />
  )
}
