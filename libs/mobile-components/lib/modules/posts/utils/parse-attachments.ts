import { ReactNativeFile } from 'apollo-upload-client'
import { PostAttachmentInput } from '../../../gen/graphql'
import { Attachment } from '../post-composer'

export function parseAttachments (attachments: Attachment[] | undefined): PostAttachmentInput[] {
  return attachments == null ? [] : attachments.map(attachment => {
    switch (attachment.type) {
      case 'IMAGE': {
        const image = new ReactNativeFile({
          uri: attachment.url,
          name: attachment.id,
          type: 'image/jpeg'
        })
        return {
          image,
          imageRef: null,
          youtube: null
        }
      }
      case 'IMAGE_REF':
        return {
          image: null,
          imageRef: attachment.id,
          youtube: null
        }
      case 'YOUTUBE':
        return {
          image: null,
          imageRef: null,
          youtube: attachment.url
        }
    }
  })
}
