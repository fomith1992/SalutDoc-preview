import React from 'react'
import { AttachmentProps, Attachments } from '../../../components/attachments/secondary-attachment.view'
import { RenderLikeButtonView } from '../../../components/likes/like.container'
import { LikeButtonView } from '../../../components/likes/like.view'
import { ContentFormatter } from '../content-formatter'
import {
  ChatCloudImg,
  CommentButton,
  MoreButton,
  MoreSvg,
  PostButtonCount,
  PostButtonsContainer,
  PostContainer,
  PostContentContainer
} from './full-post.style'
import { UserPreviewRow } from '../../../components/preview-row/user-preview-row.container'

export interface FullPostViewProps {
  userId: string
  commentCount: number
  text: string
  createdAt: string
  attachments: AttachmentProps[]
  wireLikes: (render: RenderLikeButtonView) => React.ReactElement
  contextMenu: React.ReactElement
  onCommentsPressed: () => void
  onShowContextMenu: () => void
}

export function FullPostView (
  {
    userId,
    commentCount,
    text,
    attachments,
    wireLikes,
    contextMenu,
    onCommentsPressed,
    onShowContextMenu
  }: FullPostViewProps
): React.ReactElement {
  return (
    <>
      <UserPreviewRow
        shape='circle'
        userId={userId}
        action={
          <MoreButton onPress={onShowContextMenu}>
            <MoreSvg />
            {contextMenu}
          </MoreButton>
        }
      />
      <PostContainer>
        <PostContentContainer>
          <ContentFormatter>{text}</ContentFormatter>
        </PostContentContainer>
        {attachments.length === 0 ? <></> : <Attachments attachments={attachments} />}
        <PostButtonsContainer>
          {wireLikes(props => (
            <LikeButtonView
              {...props}
            />
          ))}
          <CommentButton onPress={onCommentsPressed}>
            <ChatCloudImg />
            <PostButtonCount isPressed={false}>{commentCount > 0 ? commentCount : ''}</PostButtonCount>
          </CommentButton>
        </PostButtonsContainer>
      </PostContainer>
    </>
  )
}
