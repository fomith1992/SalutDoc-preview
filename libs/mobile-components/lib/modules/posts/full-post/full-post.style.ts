import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { sp } from '../../../style/size'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { ChatCloudIcon20 } from '../../../images/chat-cloud.icon-20'

export const Container = styled(View, {
  marginTop: sp(12)
})

export const PostContentContainer = styled(View, {
  ...container(),
  marginTop: 12,
  paddingBottom: 2
})

export const PostButtonsContainer = styled(View, {
  ...container(),
  height: 38,
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center'
})

export const PostButtonCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'h3', weight: 'light' }),
  color: color(props.isPressed ? 'accent' : 'inactive'),
  lineHeight: 16,
  textAlign: 'center',
  marginLeft: 4
}) as const)

export const PostContainer = styled(View, {
  ...container()
})

export const MoreButton = styled(TouchableOpacity, {
  marginLeft: 'auto',
  padding: sp(16),
  justifyContent: 'center'
})

export const CommentButton = styled(TouchableOpacity, {
  padding: sp(8),
  alignItems: 'center',
  flexDirection: 'row'
})

export const AttachmentsContainer = styled(View, {
  flex: 1
})

export const AttachmentContainer = styled(View, {
  flex: 1,
  marginLeft: sp(8),
  justifyContent: 'center'
})

export const CountTailAttachments = styled(Text, {
  ...font({ type: 'h2', weight: 'light' }),
  color: color('surface')
})

export const MoreSvg = styled(VerticalDotsIcon24, {
  color: color('inactive')
})

export const ChatCloudImg = styled(ChatCloudIcon20, {
  color: color('inactive')
})
