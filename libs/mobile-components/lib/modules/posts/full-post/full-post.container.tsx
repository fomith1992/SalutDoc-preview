import React from 'react'
import { Text } from 'react-native'
import { LikePost } from '../../../components/likes/like.container'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useFullPostQuery } from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { PostContextMenu } from '../post-context-menu'
import { FullPostView } from './full-post.view'

export interface FullPostProps {
  id: string
  onCommentsPressed: () => void
}

export function FullPost (
  {
    id,
    onCommentsPressed
  }: FullPostProps
): React.ReactElement {
  const { data, loading } = useFullPostQuery({
    variables: { id }
  })
  const { state: contextMenuVisible, setTrue: showContextMenu, setFalse: hideContextMenu } = useBooleanFlag(false)

  if (loading) {
    return <LoadingIndicator visible />
  }

  if (data?.postById == null) {
    return <Text>Запись не найдена</Text>
  }

  const { postById: post } = data

  return (
    <FullPostView
      userId={post.author.id}
      createdAt={post.createdAt}
      text={post.text}
      attachments={post.attachments}
      commentCount={post.commentsCount}
      onCommentsPressed={onCommentsPressed}
      wireLikes={renderLikes => <LikePost id={id} render={renderLikes} />}
      onShowContextMenu={showContextMenu}
      contextMenu={
        <PostContextMenu
          visible={contextMenuVisible}
          postId={id}
          onHideMenu={hideContextMenu}
        />
      }
    />
  )
}
