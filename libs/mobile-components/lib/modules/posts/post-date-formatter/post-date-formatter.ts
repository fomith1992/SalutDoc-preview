import moment from 'moment'

export const postDateFormatter = (date: Date | string): string => {
  const thisDay = moment().format('DD')
  const thisYear = moment().format('YYYY')
  const postDay = moment(date).format('DD')
  const postYear = moment(date).format('YYYY')
  if (postDay === thisDay) {
    return moment(date).format('hh:mm')
  } else if (postYear === thisYear) {
    return moment(date).format('DD MMM')
  } else {
    return moment(date).format('DD.MM.YY')
  }
}
