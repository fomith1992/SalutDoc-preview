import React from 'react'
import {
  FormInputLabel,
  FormInputError,
  FormInputSuccess,
  FormInputContainer
} from './form-input.style'

export interface FormInputProps {
  label?: string
  error?: string | null
  success?: string | null
  touched?: boolean

  children?: React.ReactNode
}

export function FormInput (
  {
    label,
    error,
    children,
    success,
    touched = true
  }: FormInputProps
): React.ReactElement {
  const labelEl = label == null ? null : <FormInputLabel>{label}</FormInputLabel>
  const errorEl = error == null || !touched ? null : <FormInputError>{error}</FormInputError>
  const successEl = success == null || !touched ? null : <FormInputSuccess>{success}</FormInputSuccess>
  return (
    <FormInputContainer>
      {labelEl}
      {children}
      {errorEl}
      {successEl}
    </FormInputContainer>
  )
}
