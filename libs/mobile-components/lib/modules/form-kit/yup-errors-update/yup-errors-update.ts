import * as yup from 'yup'
import { toCaseCount, formatDate } from '../../../i18n/utils'

yup.setLocale({
  mixed: {
    required: 'Поле обязательно для заполнения',
    oneOf: 'Недопустимое значение'
  },
  string: {
    url: 'Указан некорректный адрес',
    email: 'Указан некоррекнтый e-mail',
    max: ({ max }) =>
      `Длина строки не может быть более чем ${max} ${toCaseCount(['символ', 'символа', 'символов'], max)}`,
    min: ({ min }) =>
      `Длина строки не может быть менее чем ${min} ${toCaseCount(['символ', 'символа', 'символов'], min)}`
  },
  number: {
    max: ({ max }) => `Величина не может быть больше ${max}`,
    min: ({ min }) => `Величина не может быть меньше ${min}`
  },
  date: {
    min: ({ min }) => `Дата не может быть раньше ${formatDate(min)}`,
    max: ({ max }) => `Дата не может быть позже ${formatDate(max)}`
  }
})

export { yup }
