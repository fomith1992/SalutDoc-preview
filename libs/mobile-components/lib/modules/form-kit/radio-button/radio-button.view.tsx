import React from 'react'
import {
  RadioButtonContainer,
  RadioButtonLabel,
  Container,
  ActiveView
} from './radio-button.style'

interface RadioButtonProps {
  active?: boolean
  disabled?: boolean
  label?: string
  onPress: () => void
}

export function RadioButton ({
  active = false,
  disabled = false,
  label,
  onPress
}: RadioButtonProps): React.ReactElement {
  return (
    <Container
      disabled={disabled}
      onPress={onPress}
    >
      <RadioButtonContainer
        active={active}
        disabled={disabled}
      >
        {active && (
          <ActiveView
            active={active}
            disabled={disabled}
          />
        )}
      </RadioButtonContainer>
      {label != null
        ? (
          <RadioButtonLabel>
            {label}
          </RadioButtonLabel>
        ) : null}
    </Container>
  )
}
