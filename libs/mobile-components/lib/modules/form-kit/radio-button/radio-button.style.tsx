import Color from 'color'
import { pipe } from 'fp-ts/lib/function'
import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { mapStyle, styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const Container = styled(TouchableOpacity, {
  flex: 1,
  paddingVertical: sp(12),
  flexDirection: 'row',
  alignItems: 'center'
})

export const RadioButtonContainer = styled(View, (props: {
  active: boolean
  disabled: boolean
}) => ({
  width: sp(20),
  height: sp(20),
  borderRadius: sp(12),
  borderWidth: 2,
  borderColor: props.active
    ? pipe(color('accent'), mapStyle(c => Color(c).alpha(props.disabled ? 0.5 : 1).string()))
    : pipe(color('inactive'), mapStyle(c => Color(c).alpha(props.disabled ? 0.5 : 1).string())),
  justifyContent: 'center',
  alignItems: 'center'
}) as const)

export const ActiveView = styled(View, (props: {
  active: boolean
  disabled: boolean
}) => ({
  width: 10,
  height: 10,
  borderRadius: sp(8),
  backgroundColor: color(
    props.active
      ? props.disabled ? 'secondaryLight' : 'accent'
      : props.disabled ? 'inactive' : 'subtext'
  )
}) as const)

export const RadioButtonLabel = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  marginLeft: sp(12)
})
