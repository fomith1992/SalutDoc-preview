import React from 'react'
import { FormInput } from '../form-input'
import { MaskedInput } from './text-input.style'

export interface TextInputProps {
  value: string | undefined
  onChange: (value: string) => void
  getRawValue?: (value: string) => void
  onFocus?: () => void
  onBlur?: () => void
  error?: string | null
  success?: string | null
  active?: boolean
  touched?: boolean
  disabled?: boolean
  mask?: string
  type: 'cel-phone' | 'datetime'

  placeholder?: string
  maxLength?: number
  label?: string
  numberOfLines?: number | [number, number]
}

export function MaskedTextInput (
  {
    value,
    onChange,
    onFocus,
    onBlur,
    error,
    success,
    active,
    touched = true,
    disabled = false,

    placeholder,
    type,
    mask,
    maxLength,
    label,
    numberOfLines = 1,
    getRawValue
  }: TextInputProps
): React.ReactElement {
  const [minLinesNumber, maxLinesNumber] = typeof numberOfLines === 'number'
    ? [numberOfLines, numberOfLines]
    : numberOfLines

  const ref = React.useRef<any>(null)
  getRawValue?.(ref.current?.getRawValue())

  return (
    <FormInput label={label} error={error} touched={touched} success={success}>
      <MaskedInput
        type={type}
        options={type === 'cel-phone' ? {
          dddMask: mask
        } : {
          format: 'DD.MM.YYYY'
        }}
        value={value}
        onChangeText={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        active={active}
        editable={!disabled}
        hasError={error != null && touched}
        maxLength={maxLength}
        placeholder={placeholder}
        multiline={maxLinesNumber > 1}
        minLinesNumber={minLinesNumber}
        maxLinesNumber={maxLinesNumber}
        ref={ref}
      />
    </FormInput>
  )
}
