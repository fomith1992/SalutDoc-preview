import React from 'react'
import { FormInput } from '../form-input'
import { Input, PostInput, QuestionInput } from './text-input.style'

export interface TextInputProps {
  value: string | undefined
  onChange: (value: string) => void
  onFocus?: () => void
  onBlur?: () => void
  error?: string | null
  success?: string | null
  active?: boolean
  touched?: boolean
  disabled?: boolean

  placeholder?: string
  maxLength?: number
  label?: string
  numberOfLines?: number | [number, number]
}

export function TextInput (
  {
    value,
    onChange,
    onFocus,
    onBlur,
    error,
    success,
    active,
    touched = true,
    disabled = false,

    placeholder,
    maxLength,
    label,
    numberOfLines = 1
  }: TextInputProps
): React.ReactElement {
  const [minLinesNumber, maxLinesNumber] = typeof numberOfLines === 'number'
    ? [numberOfLines, numberOfLines]
    : numberOfLines

  return (
    <FormInput label={label} error={error} touched={touched} success={success}>
      <Input
        value={value}
        onChangeText={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        active={active}
        editable={!disabled}
        hasError={error != null && touched}
        maxLength={maxLength}
        placeholder={placeholder}
        multiline={maxLinesNumber > 1}
        minLinesNumber={minLinesNumber}
        maxLinesNumber={maxLinesNumber}
      />
    </FormInput>
  )
}

export function PostTextInput (
  {
    value,
    onChange,
    onFocus,
    onBlur,
    placeholder
  }: TextInputProps
): React.ReactElement {
  return (
    <PostInput
      value={value}
      onChangeText={onChange}
      onFocus={onFocus}
      maxLength={10000}
      onBlur={onBlur}
      placeholder={placeholder}
      multiline
    />
  )
}

export function QuestionTextInput (
  {
    value,
    onChange,
    onFocus,
    onBlur,
    placeholder
  }: TextInputProps
): React.ReactElement {
  return (
    <QuestionInput
      value={value}
      onChangeText={onChange}
      onFocus={onFocus}
      onBlur={onBlur}
      placeholder={placeholder}
    />
  )
}
