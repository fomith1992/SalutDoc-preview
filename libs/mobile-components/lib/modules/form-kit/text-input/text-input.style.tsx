import { TextInput } from 'react-native'
import { color } from '../../../style/color'
import { sp, SpScale } from '../../../style/size'
import { LazyStyleProp, styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { inputFrameStyle } from '../form-input.style'
import { TextInputMask } from 'react-native-masked-text'

const FramedInput = styled(TextInput, inputFrameStyle)
const FramedInputMasked = styled(TextInputMask, inputFrameStyle)

function linesHeight (linesNumber: number | undefined): LazyStyleProp<number | undefined> {
  return linesNumber == null ? undefined : sp(
    12 * 2 + // padding
    2 + // border
    linesNumber * 20 as SpScale // lines
  )
}

export const MaskedInput = styled(FramedInputMasked, (props: { minLinesNumber?: number, maxLinesNumber?: number }) => ({
  ...font({ type: 'text1' }),
  color: color('text'),
  // padding does not work with multiline inputs for some reason =/
  paddingHorizontal: sp(12),
  paddingTop: sp(12),
  paddingBottom: sp(12),
  minHeight: linesHeight(props.minLinesNumber),
  maxHeight: linesHeight(props.maxLinesNumber),
  textAlignVertical: props.minLinesNumber != null && props.minLinesNumber > 1 ? 'top' : 'center'
}) as const)

export const Input = styled(FramedInput, (props: { minLinesNumber?: number, maxLinesNumber?: number }) => ({
  ...font({ type: 'text1' }),
  color: color('text'),
  // padding does not work with multiline inputs for some reason =/
  paddingHorizontal: sp(12),
  paddingTop: sp(12),
  paddingBottom: sp(12),
  minHeight: linesHeight(props.minLinesNumber),
  maxHeight: linesHeight(props.maxLinesNumber),
  textAlignVertical: props.minLinesNumber != null && props.minLinesNumber > 1 ? 'top' : 'center'
}) as const)

export const PostInput = styled(TextInput, {
  ...container(),
  flex: 1,
  textAlignVertical: 'top'
})

export const QuestionInput = styled(TextInput, {
  ...container('margin'),
  textAlignVertical: 'top',
  borderBottomColor: color('inactive'),
  borderBottomWidth: 0.4,
  height: sp(32),
  paddingVertical: 0,
  ...font({ type: 'h2', weight: 'light' })
})
