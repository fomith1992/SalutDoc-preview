import { TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { mapStyle, styled } from '../../../style/styled'
import { LightCheckMarkIcon } from '../../../images/light-check-mark.icon'
import Color from 'color'
import { pipe } from 'fp-ts/lib/function'

export const CheckBoxContainer = styled(TouchableOpacity, (props: { value: boolean, disabled: boolean }) => ({
  borderWidth: 2,
  borderColor: color(props.value ? 'accent' : 'inactive'),
  backgroundColor: props.value
    ? pipe(color('accent'), mapStyle(c => Color(c).alpha(props.disabled ? 0.4 : 1).string()))
    : props.disabled ? color('background') : 'transparent',
  borderRadius: 2,
  height: sp(20),
  width: sp(20),
  alignItems: 'center',
  justifyContent: 'center'
}) as const)

const iconStyle = {
  color: color('surface'),
  width: sp(12),
  height: sp(12)
}

export const CheckMark = styled(LightCheckMarkIcon, iconStyle)
