import React from 'react'
import { Insets } from 'react-native'
import { CheckBoxContainer, CheckMark } from './check-box.style'

export interface CheckBoxProps {
  value: boolean
  disabled?: boolean
  onChange: (value: boolean) => void
  hitSlop?: Insets
}

export function CheckBox (
  {
    value,
    disabled = false,
    onChange,
    hitSlop
  }: CheckBoxProps
): React.ReactElement {
  return (
    <CheckBoxContainer
      value={value}
      disabled={disabled}
      onPress={() => onChange(!value)}
      hitSlop={hitSlop}
    >
      {value ? <CheckMark /> : null}
    </CheckBoxContainer>
  )
}
