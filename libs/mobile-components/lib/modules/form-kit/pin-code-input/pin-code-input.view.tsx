import React, { useRef, useState } from 'react'
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  I18nManager,
  ViewStyle,
  TextStyle,
  KeyboardTypeOptions,
  NativeSyntheticEvent,
  TextInputKeyPressEventData
} from 'react-native'

const styles = StyleSheet.create({
  containerDefault: {},
  cellDefault: {
    borderColor: 'gray',
    borderWidth: 1
  },
  cellFocusedDefault: {
    borderColor: 'black',
    borderWidth: 2
  },
  textStyleDefault: {
    color: 'gray',
    fontSize: 24
  },
  textStyleFocusedDefault: {
    color: 'black'
  }
})

interface PinCodeInputProps {
  value?: string
  codeLength?: number
  cellSize?: { width: number, height: number }
  cellSpacing?: number

  placeholder?: string
  mask?: string | React.ReactElement
  password?: boolean

  autoFocus?: boolean

  restrictToNumbers?: boolean

  containerStyle?: ViewStyle

  cellStyle?: ViewStyle
  cellStyleFocused?: ViewStyle
  cellStyleFilled?: ViewStyle

  textStyle?: TextStyle
  textStyleFocused?: TextStyle

  onFulfill?: (value: string) => void
  onChangeText?: (value: string) => void
  onBackspace?: () => void
  onTextChange?: (value: string) => void
  onFocus?: () => void
  onBlur?: () => void
  keyboardType?: KeyboardTypeOptions
  editable?: boolean
  inputProps?: TextStyle
  disableFullscreenUI?: boolean
}

export function PinCodeInput ({
  value = '',
  codeLength = 4,
  cellSize = { width: 44, height: 44 },
  cellSpacing = 8,
  placeholder = '',
  password = false,
  mask = '*',
  keyboardType = 'numeric',
  autoFocus = false,
  restrictToNumbers = false,
  containerStyle = styles.containerDefault,
  cellStyle = styles.cellDefault,
  cellStyleFocused = styles.cellFocusedDefault,
  textStyle = styles.textStyleDefault,
  cellStyleFilled,
  textStyleFocused = styles.textStyleFocusedDefault,
  editable = true,
  disableFullscreenUI = true,
  inputProps = {},
  onTextChange,
  onFulfill,
  onBackspace
}: PinCodeInputProps): React.ReactElement {
  const ref = useRef<View>(null)
  const inputRef = useRef<TextInput>(null)
  const [state, setState] = useState({ maskDelay: false, focused: false })

  const _keyPress = (event: NativeSyntheticEvent<TextInputKeyPressEventData>): void => {
    if (event.nativeEvent.key === 'Backspace') {
      if (value === '' && onBackspace != null) {
        onBackspace()
      }
    }
  }

  const _inputCode = (code: string): void => {
    if (restrictToNumbers) {
      code = (code.match(/[0-9]/g) ?? []).join('')
    }

    if (onTextChange != null) {
      onTextChange(code)
    }
    if (code.length === codeLength && onFulfill != null) {
      onFulfill(code)
    }

    const maskDelay = password &&
      code.length > value.length
    setState({ maskDelay, focused: state.focused })
  }

  return (
    <View
      ref={ref}
      style={[{
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'relative',
        width: cellSize.width * codeLength + cellSpacing * (codeLength - 1),
        height: cellSize.height
      },
      containerStyle
      ]}
    >
      <View style={{
        position: 'absolute',
        margin: 0,
        height: '100%',
        flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
        alignItems: 'center'
      }}
      >
        {
          Array.apply(null, Array(codeLength)).map((_, idx) => {
            const cellFocused = state.focused && idx === value.length
            const filled = idx < value.length
            const last = (idx === value.length - 1)
            const showMask = filled && (password && (!state.maskDelay || !last))
            const isPlaceholderText = typeof placeholder === 'string'
            const isMaskText = typeof mask === 'string'
            const pinCodeChar = value.charAt(idx)

            let cellText = null
            if (filled || placeholder !== null) {
              if (showMask && isMaskText) {
                cellText = mask
              } else if (!filled && isPlaceholderText) {
                cellText = placeholder
              } else if (pinCodeChar != null) {
                cellText = pinCodeChar
              }
            }

            const placeholderComponent = !isPlaceholderText ? placeholder : null
            const maskComponent = (showMask && !isMaskText) ? mask : null
            const isCellText = typeof cellText === 'string'

            return (
              <View
                key={idx}
                style={[
                  {
                    width: cellSize.width,
                    height: cellSize.height,
                    marginLeft: cellSpacing / 2,
                    marginRight: cellSpacing / 2,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  },
                  cellStyle,
                  cellFocused ? cellStyleFocused : {},
                  filled ? cellStyleFilled : {}
                ]}
              >
                {isCellText && maskComponent == null && (
                  <Text style={[textStyle, cellFocused ? textStyleFocused : {}]}>
                    {cellText}
                  </Text>
                )}

                {(!isCellText && maskComponent == null) && placeholderComponent}
                {isCellText && maskComponent}
              </View>
            )
          })
        }
      </View>
      <TextInput
        disableFullscreenUI={disableFullscreenUI}
        value={value}
        ref={inputRef}
        onChangeText={_inputCode}
        onKeyPress={_keyPress}
        onFocus={() => {
          setState({ focused: true, maskDelay: state.maskDelay })
          inputRef.current?.focus()
        }}
        onBlur={() => {
          setState({ focused: false, maskDelay: state.maskDelay })
          inputRef.current?.blur()
        }}
        spellCheck={false}
        autoFocus={autoFocus}
        keyboardType={keyboardType}
        numberOfLines={1}
        caretHidden
        maxLength={codeLength}
        selection={{
          start: value.length,
          end: value.length
        }}
        style={{
          flex: 1,
          opacity: 0,
          textAlign: 'center'
        }}
        editable={editable}
        {...inputProps}
      />
    </View>
  )
}
