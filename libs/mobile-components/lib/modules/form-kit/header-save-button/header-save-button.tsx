import React from 'react'
import { useEditHeader } from '../../ui-kit/header'

export function HeaderSaveButton (props: {
  text?: string
  valid: boolean
  onSubmit: () => void
}): React.ReactElement {
  const { valid, onSubmit, text } = props
  useEditHeader({
    saveButton: {
      text: text ?? 'Сохранить',
      enabled: valid,
      onPress: onSubmit
    }
  }, false)
  return <></>
}
