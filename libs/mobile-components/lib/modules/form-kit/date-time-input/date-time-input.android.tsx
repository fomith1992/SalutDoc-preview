import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import React, { useCallback, useState } from 'react'
import { FormInput } from '../form-input'
import { DateTimeInputProps } from './date-time-input'
import { InputFrame, StaticInputValue } from '../form-input.style'
import { CalendarIcon, Container } from './date-time-input.style'

export function DateTimeInput (
  {
    value,
    onChange,
    onBlur,
    onFocus,
    active,
    touched = true,
    error,
    label
  }: DateTimeInputProps
): React.ReactElement {
  const [visible, setVisible] = useState(false)
  const handleChange = useCallback((_: unknown, date?: Date) => {
    setVisible(false)
    if (date != null) {
      onChange?.(date)
    }
    onBlur?.()
  }, [onChange, onBlur])
  const handleShow = useCallback(() => {
    setVisible(true)
    onFocus?.()
  }, [onFocus])
  return (
    <FormInput
      error={error}
      label={label}
      touched={touched}
    >
      <InputFrame
        active={active}
        hasError={error != null && touched}
      >
        <Container onPress={handleShow}>
          <StaticInputValue>{moment(value).format('DD.MM.YYYY')}</StaticInputValue>
          <CalendarIcon />
          {!visible ? null : (
            <DateTimePicker
              value={value ?? new Date()}
              mode='date'
              display='default'
              onChange={handleChange}
            />
          )}
        </Container>
      </InputFrame>
    </FormInput>
  )
}
