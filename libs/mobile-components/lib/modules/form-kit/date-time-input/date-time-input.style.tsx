import { TouchableOpacity, View } from 'react-native'
import { CalendarIcon16 } from '../../../images/calendar.icon-16'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const CalendarIcon = styled(CalendarIcon16, {
  color: color('accent')
})

export const IconContainer = styled(View, {
  marginRight: sp(12)
})

export const Container = styled(TouchableOpacity, {
  flex: 1,
  paddingRight: sp(12),
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center'
})
