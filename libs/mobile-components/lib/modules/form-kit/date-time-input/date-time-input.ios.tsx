import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import React, { useCallback, useState } from 'react'
import { DismissibleModal } from '../../ui-kit'
import { FormInput } from '../form-input'
import { BottomMenuContainer, InputButton, StaticInputValue } from '../form-input.style'
import { DateTimeInputProps } from './date-time-input'
import { CalendarIcon, IconContainer } from './date-time-input.style'

export function DateTimeInput (
  {
    value,
    onChange,
    onBlur,
    onFocus,
    active,
    touched = true,
    error,
    label
  }: DateTimeInputProps
): React.ReactElement {
  const [modalVisible, setModalVisible] = useState(false)
  const handleChange = useCallback((_: unknown, date?: Date) => {
    if (date != null) {
      onChange?.(date)
    }
  }, [onChange])
  const showModal = useCallback(() => {
    setModalVisible(true)
    onFocus?.()
  }, [onFocus])
  const hideModal = useCallback(() => {
    onBlur?.()
    setModalVisible(false)
  }, [onBlur])
  return (
    <FormInput
      label={label}
      error={error}
      touched={touched}
    >
      <InputButton
        active={active}
        hasError={error != null && touched}
        onPress={showModal}
      >
        <StaticInputValue>{moment(value).format('DD.MM.YYYY')}</StaticInputValue>
        <IconContainer>
          <CalendarIcon />
        </IconContainer>
        <DismissibleModal visible={modalVisible} onDismiss={hideModal}>
          <BottomMenuContainer>
            <DateTimePicker
              value={value ?? new Date()}
              mode='date'
              onChange={handleChange}
            />
          </BottomMenuContainer>
        </DismissibleModal>
      </InputButton>
    </FormInput>
  )
}
