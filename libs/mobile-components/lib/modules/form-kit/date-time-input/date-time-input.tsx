import React from 'react'

export interface DateTimeInputProps {
  value: Date | undefined
  onChange?: (value: Date) => void
  onFocus?: () => void
  onBlur?: () => void
  active?: boolean
  touched?: boolean
  error?: string | null
  label?: string
}

// implemented per platform
export declare function DateTimeInput (props: DateTimeInputProps): React.ReactElement
