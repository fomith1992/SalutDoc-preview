import React from 'react'
import { OptionsProps } from './select-search.view'
import { SelectSearchModal } from './select-search.modal'
import { FormInput } from '../../form-kit/form-input'
import { InputButton, StaticInputValue } from '../../form-kit/form-input.style'
import { ShevronDownIcon, IconContainer } from './select-search.style'

interface SelectSearchInputProps {
  options: OptionsProps[]
  value: string | undefined
  onChange: (value: string | undefined) => void
  expanded: boolean
  onSetExpanded: (state: boolean) => void
  active?: boolean
  touched?: boolean
  error?: string | null
  label?: string
}

export function SelectSearchInput ({
  options,
  value,
  onChange,
  expanded,
  onSetExpanded,
  active,
  touched = true,
  label,
  error
}: SelectSearchInputProps): React.ReactElement {
  const matchingValues = options.flatMap(group =>
    group.data.filter(item =>
      item.specializationRu === value
    )
  )
  return (
    <FormInput
      label={label}
      error={error}
      touched={touched}
    >
      <InputButton
        active={active}
        hasError={error != null && touched}
        onPress={() => onSetExpanded(true)}
      >
        <StaticInputValue>
          {matchingValues[0]?.specializationRu ?? 'Не задана'}
        </StaticInputValue>
        <IconContainer>
          <ShevronDownIcon />
        </IconContainer>
        <SelectSearchModal
          options={options.map(group => ({
            title: group.title,
            data: group.data.map(item => ({
              value: item.specializationRu
            }))
          }))}
          expanded={expanded}
          onSetExpanded={onSetExpanded}
          onChange={onChange}
        />
      </InputButton>
    </FormInput>
  )
}
