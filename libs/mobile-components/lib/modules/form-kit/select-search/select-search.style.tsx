import { styled } from '../../../style/styled'
import { TouchableOpacity, Text, View, TextInput } from 'react-native'
import { PlusIcon16 } from '../../../images/plus.icon-16'
import { sp } from '../../../style/size'
import { color } from '../../../style/color'
import { font } from '../../../style/text'
import { container } from '../../../style/view'
import { SearchIcon24 } from '../../../images/search.icon-24'
import { CrossIcon16 } from '../../../images/cross.icon-16'
import { ShevronDownIcon16 } from '../../../images/shevron-down.icon-16'

export const InfoContainer = styled(TouchableOpacity, {
  marginRight: 'auto',
  padding: sp(12),
  flexDirection: 'row',
  alignItems: 'flex-start',
  borderRadius: sp(32),
  backgroundColor: color('surface')
})

export const SelectedInfoContainer = styled(TouchableOpacity, {
  marginRight: 'auto',
  padding: sp(12),
  flexDirection: 'row-reverse',
  alignItems: 'flex-start',
  borderRadius: sp(32),
  backgroundColor: color('surface')
})

export const InfoPlusIcon = styled(PlusIcon16, {
  color: color('subtext')
})

export const ActivePlusIcon = styled(CrossIcon16, {
  color: color('accent')
})

export const ShevronDownIcon = styled(ShevronDownIcon16, {
  color: color('accent')
})

export const IconContainer = styled(View, {
  marginRight: sp(12)
})

export const DefaultText = styled(Text, {
  marginLeft: sp(12),
  ...font({ type: 'h3', weight: 'light' }),
  color: color('text')
})

export const ActiveText = styled(Text, {
  marginRight: sp(4),
  ...font({ type: 'h3', weight: 'light' }),
  color: color('accent')
})

export const ListContainer = styled(View, {
  flex: 1,
  backgroundColor: color('surface'),
  marginVertical: 'auto'
})

export const ListItemContainer = styled(TouchableOpacity, {
  paddingVertical: sp(12),
  backgroundColor: color('surface')
})

export const ListItem = styled(Text, {
  marginLeft: sp(12),
  ...font({ type: 'h3', weight: 'light' }),
  color: color('text')
})
export const ListTitleItem = styled(Text, {
  paddingVertical: sp(4),
  paddingLeft: sp(12),
  backgroundColor: color('background'),
  ...font({ type: 'h1' }),
  color: color('subtext')
})

export const InputSearchContainer = styled(View, {
  ...container('padding'),
  marginHorizontal: sp(8),
  flexDirection: 'row',
  alignItems: 'center',
  flex: 1,
  backgroundColor: color('background'),
  borderRadius: sp(24),
  height: sp(32)
})

export const Input = styled(TextInput, {
  ...font({ type: 'caption', weight: 'strong' }),
  color: color('inactive'),
  paddingVertical: 0,
  marginHorizontal: sp(4),
  flex: 1
})

export const HeaderInputButton = styled(TouchableOpacity, {
  padding: sp(4)
})

export const ClosePlusIcon = styled(CrossIcon16, {
  width: sp(12),
  height: sp(12),
  color: color('accent')
})

export const IconSearch = styled(SearchIcon24, {
  color: color('accent')
})

export const Separator = styled(View, {
  ...container(),
  borderBottomWidth: 1,
  borderBottomColor: color('surface')
})

export const EmptyContainer = styled(View, {
  justifyContent: 'center',
  alignItems: 'center',
  paddingVertical: sp(64)
})
export const SearchContainer = styled(View, {
  paddingVertical: sp(8),
  backgroundColor: color('surface')
})

export const EmptyText = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  color: color('inactive')
})
