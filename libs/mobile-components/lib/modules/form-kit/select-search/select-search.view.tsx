import React, { useState } from 'react'
import { SelectSearchButton } from './select-search-button.view'
import { SelectSearchInput } from './select-search-input.view'

export interface OptionsProps {
  title: string | null
  data: Array<{
    value: string
    specializationRu: string
  }>
}

interface SelectSearchProps {
  options: OptionsProps[]
  value: string | undefined
  onChange: (value: string | undefined) => void
  text?: string
  type: 'button' | 'input'
  active?: boolean
  touched?: boolean
  error?: string | null
  label?: string
}

export function SelectSearch ({
  value,
  onChange,
  options,
  text,
  type = 'button',
  active,
  touched,
  error,
  label
}: SelectSearchProps): React.ReactElement {
  const [expanded, setExpanded] = useState<boolean>(false)
  switch (type) {
    case 'button':
      return (
        <SelectSearchButton
          options={options}
          value={value}
          onChange={onChange}
          text={text}
          expanded={expanded}
          onSetExpanded={(state: boolean) => setExpanded(state)}
        />
      )
    case 'input':
      return (
        <SelectSearchInput
          options={options}
          value={value}
          onChange={onChange}
          expanded={expanded}
          onSetExpanded={(state: boolean) => setExpanded(state)}
          active={active}
          touched={touched}
          error={error}
          label={label}
        />
      )
  }
}
