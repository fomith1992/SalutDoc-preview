import React from 'react'
import { OptionsProps } from './select-search.view'
import { SelectSearchModal } from './select-search.modal'
import {
  ActivePlusIcon,
  ActiveText,
  DefaultText,
  InfoContainer,
  InfoPlusIcon,
  SelectedInfoContainer
} from './select-search.style'

interface SelectSearchButtonProps {
  options: OptionsProps[]
  value: string | undefined
  onChange: (value: string | undefined) => void
  text?: string
  expanded: boolean
  onSetExpanded: (state: boolean) => void
}

export function SelectSearchButton ({
  options,
  value,
  onChange,
  text,
  expanded,
  onSetExpanded
}: SelectSearchButtonProps): React.ReactElement {
  return value !== undefined
    ? (
      <SelectedInfoContainer onPress={() => onChange(undefined)}>
        <ActivePlusIcon />
        <ActiveText>
          {value}
        </ActiveText>
      </SelectedInfoContainer>
    ) : (
      <InfoContainer onPress={() => onSetExpanded(true)}>
        <InfoPlusIcon />
        <DefaultText>
          {text ?? 'Не выбрана'}
        </DefaultText>
        <SelectSearchModal
          options={options.map(group => ({
            title: group.title,
            data: group.data.map(item => ({
              value: item.value
            }))
          }))}
          expanded={expanded}
          onSetExpanded={onSetExpanded}
          onChange={onChange}
        />
      </InfoContainer>
    )
}
