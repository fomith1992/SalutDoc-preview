import React, { useState } from 'react'
import { KeyboardAvoidingView, Platform, SafeAreaView, SectionList } from 'react-native'
import { Modal } from '../../ui-kit'
import { HeaderLight } from '../../../components/header-light/header-light.view'
import {
  ClosePlusIcon,
  EmptyContainer,
  EmptyText,
  HeaderInputButton,
  IconSearch,
  Input,
  InputSearchContainer,
  ListContainer,
  ListItem,
  ListItemContainer,
  ListTitleItem,
  SearchContainer,
  Separator
} from './select-search.style'

interface OptionsProps {
  title: string | null
  data: Array<{
    value: string
  }>
}

interface SelectSearchInfoProps {
  items: OptionsProps[]
  onChange: (value: string) => void
}

function SelectSearchInfo ({
  items,
  onChange
}: SelectSearchInfoProps): React.ReactElement {
  const [searchItem, setSearchItem] = useState('')
  const filteredOptions = searchItem === ''
    ? items
    : items.map(group => ({
      title: group.title,
      data: group.data.filter(({ value }) =>
        value.split(' ').filter(piece =>
          piece.toLowerCase().replace(/[()+]/g, '').startsWith(searchItem.toLowerCase())
        ).length !== 0
      )
    })).filter(result => result.data.length !== 0)
  return (
    <ListContainer>
      <SectionList
        ListHeaderComponent={
          <SearchContainer>
            <InputSearchContainer>
              <IconSearch />
              <Input
                placeholder='Поиск'
                value={searchItem}
                onChangeText={setSearchItem}
              />
              {searchItem == null || searchItem === ''
                ? null
                : (
                  <HeaderInputButton
                    onPress={() => setSearchItem('')}
                  >
                    <ClosePlusIcon />
                  </HeaderInputButton>)}
            </InputSearchContainer>
          </SearchContainer>
        }
        sections={filteredOptions}
        ItemSeparatorComponent={() => <Separator />}
        renderItem={({ item: { value } }) => {
          return (
            <ListItemContainer
              onPress={() => onChange(value)}
            >
              <ListItem>
                {value}
              </ListItem>
            </ListItemContainer>
          )
        }}
        keyExtractor={item => item.value}
        renderSectionHeader={({ section: { title } }) => (
          title != null ? <ListTitleItem>{title}</ListTitleItem> : <></>
        )}
        ListEmptyComponent={
          <EmptyContainer>
            <EmptyText>
              Ничего не найдено
            </EmptyText>
          </EmptyContainer>
        }
      />
    </ListContainer>
  )
}

interface SelectSearchModalProps {
  options: OptionsProps[]
  expanded: boolean
  onSetExpanded: (state: boolean) => void
  onChange: (value: string) => void
}

export function SelectSearchModal ({
  options,
  expanded,
  onSetExpanded,
  onChange
}: SelectSearchModalProps): React.ReactElement {
  return (
    <Modal
      animation='slide'
      visible={expanded}
    >
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      >
        <SafeAreaView style={{ flex: 1 }}>
          <HeaderLight onClose={() => onSetExpanded(false)} title='Выберите категорию' />
          <SelectSearchInfo
            items={options}
            onChange={value => {
              onChange(value)
              onSetExpanded(false)
            }}
          />
        </SafeAreaView>
      </KeyboardAvoidingView>
    </Modal>
  )
}
