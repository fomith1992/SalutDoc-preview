import Color from 'color'
import React, { useEffect, useRef } from 'react'
import { Animated, TouchableWithoutFeedback } from 'react-native'
import { Container } from './toggle-switch.style'

export interface ToggleSwitchProps {
  value: boolean
  onPress: (value: boolean) => void
  disabled?: boolean
}

export function ToggleSwitch (
  {
    disabled = false,
    value,
    onPress
  }: ToggleSwitchProps
): React.ReactElement {
  const colors = {
    track: {
      inactive: '#E3E3E3',
      active: '#E5F0FE'
    },
    thumb: {
      inactive: '#C4C4C4',
      active: '#006EFD'
    }
  }
  const dimensions = {
    track: {
      width: 34,
      height: 14,
      borderRadius: 7
    },
    thumb: {
      width: 20,
      height: 20,
      borderRadius: 10
    }
  }
  const translation = useRef(new Animated.Value(value ? 1 : 0)).current
  const enterAnimation = useRef(Animated.spring(translation, {
    toValue: 1,
    useNativeDriver: false
  })).current
  const leaveAnimation = useRef(Animated.spring(translation, {
    toValue: 0,
    useNativeDriver: false
  })).current
  useEffect(() => {
    if (value) {
      enterAnimation.start()
    } else {
      leaveAnimation.start()
    }
  }, [value])
  return (
    <Container>
      <TouchableWithoutFeedback
        onPress={() => !disabled && onPress(!value)}
      >
        <Animated.View
          // Track
          style={{
            width: dimensions.track.width,
            height: dimensions.track.height,
            borderRadius: dimensions.track.borderRadius,
            backgroundColor: translation.interpolate({
              inputRange: [0, 1],
              outputRange: [
                Color(colors.track.inactive).alpha(disabled ? 0.5 : 1).string(),
                Color(colors.track.active).alpha(disabled ? 0.5 : 1).string()
              ]
            })
          }}
        >
          <Animated.View
            // Thumb
            style={{
              position: 'absolute',
              top: (dimensions.track.height - dimensions.thumb.height) / 2,
              width: dimensions.thumb.width,
              height: dimensions.thumb.height,
              borderRadius: dimensions.thumb.borderRadius,
              backgroundColor: translation.interpolate({
                inputRange: [0, 1],
                outputRange: [
                  Color(colors.thumb.inactive).alpha(disabled ? 0.5 : 1).string(),
                  Color(colors.thumb.active).alpha(disabled ? 0.5 : 1).string()
                ]
              }),
              transform: [
                {
                  translateX: translation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, dimensions.track.width - dimensions.thumb.width]
                  })
                }
              ]
            }}
          />
        </Animated.View>
      </TouchableWithoutFeedback>
    </Container>
  )
}
