import { View } from 'react-native'
import { styled } from '../../../style/styled'

export const Container = styled(View, {
  alignItems: 'center',
  alignContent: 'center',
  marginRight: 'auto'
})
