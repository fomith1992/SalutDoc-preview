import React from 'react'

export interface SelectInputProps<T> {
  value: T | undefined
  onChange?: (value: T | undefined) => void
  options: Array<{
    label: string
    value: T
  }>
  onFocus?: () => void
  onBlur?: () => void
  active?: boolean
  touched?: boolean
  error?: string | null
  label?: string
  disabled?: boolean
  hideIcons?: boolean
}

// implemented per platform
export declare function SelectInput<T> (props: SelectInputProps<T>): React.ReactElement
