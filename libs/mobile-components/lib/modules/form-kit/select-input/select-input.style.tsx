import { Picker } from '@react-native-community/picker'
import { StyleSheet, View } from 'react-native'
import { ShevronDownIcon16 } from '../../../images/shevron-down.icon-16'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const AndroidPicker = styled(Picker, StyleSheet.absoluteFill, {
  color: 'transparent',
  height: 'auto'
})

export const ShevronDownIcon = styled(ShevronDownIcon16, {
  color: color('accent')
})

export const IconContainer = styled(View, {
  marginRight: sp(12)
})
