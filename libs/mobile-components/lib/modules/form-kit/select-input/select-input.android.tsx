import { Picker } from '@react-native-community/picker'
import React, { useMemo } from 'react'
import { useTheme } from '../../../style/theme'
import { FormInput } from '../form-input'
import { InputFrame, StaticInputValue } from '../form-input.style'
import { SelectInputProps } from './select-input'
import { AndroidPicker, IconContainer, ShevronDownIcon } from './select-input.style'

export function SelectInput<T> (
  {
    options,
    value,
    onChange,
    active,
    touched = true,
    label,
    error,
    disabled = false,
    hideIcons = false
  }: SelectInputProps<T>
): React.ReactElement {
  const { colors: { subtext, text } } = useTheme()
  const items = useMemo(
    () => [
      <Picker.Item key={-1} value={-1} label='Не задано' color={subtext} />,
      ...options.map(({ label }, idx) => <Picker.Item key={idx} value={idx} label={label} color={text} />)
    ],
    [options]
  )
  // TODO pass onFocus and onBlur
  return (
    <FormInput
      error={error}
      label={label}
      touched={touched}
    >
      <InputFrame
        editable={!disabled}
        active={active}
        hasError={error != null && touched}
      >
        <StaticInputValue disabled={disabled}>
          {options.find(opt => value === opt.value)?.label ?? 'Не задано'}
        </StaticInputValue>
        <AndroidPicker
          enabled={!disabled}
          selectedValue={options.findIndex(option => option.value === value)}
          onValueChange={value => {
            onChange?.(value === -1 ? undefined : options[value as number]?.value)
          }}
        >
          {items}
        </AndroidPicker>
        {!hideIcons
          ? (
            <IconContainer>
              <ShevronDownIcon />
            </IconContainer>
          ) : null}
      </InputFrame>
    </FormInput>
  )
}
