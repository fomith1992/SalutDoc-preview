import { Picker } from '@react-native-community/picker'
import React, { useCallback, useMemo, useState } from 'react'
import { useTheme } from '../../../style/theme'
import { DismissibleModal } from '../../ui-kit'
import { FormInput } from '../form-input'
import { BottomMenuContainer, InputButton, StaticInputValue } from '../form-input.style'
import { SelectInputProps } from './select-input'
import { IconContainer, ShevronDownIcon } from './select-input.style'

export function SelectInput<T> (
  {
    value,
    options,
    onChange,
    onBlur,
    onFocus,
    active,
    touched = true,
    label,
    error,
    disabled = false,
    hideIcons = false
  }: SelectInputProps<T>
): React.ReactElement {
  const [modalVisible, setModalVisible] = useState(false)
  const showModal = useCallback(() => {
    setModalVisible(true)
    onFocus?.()
  }, [onFocus])
  const hideModal = useCallback(() => {
    onBlur?.()
    setModalVisible(false)
  }, [onBlur])
  const { colors: { subtext } } = useTheme()
  const items = useMemo(
    () => [
      <Picker.Item key={-1} value={-1} label='Не задано' color={subtext} />,
      ...options.map(({ label }, idx) => <Picker.Item key={idx} value={idx} label={label} />)
    ],
    [options]
  )
  return (
    <FormInput
      label={label}
      error={error}
      touched={touched}
    >
      <InputButton
        editable={!disabled}
        active={active}
        hasError={error != null && touched}
        onPress={showModal}
      >
        <StaticInputValue disabled={disabled}>
          {options.find(opt => value === opt.value)?.label ?? 'Не задано'}
        </StaticInputValue>
        {!hideIcons
          ? (
            <IconContainer>
              <ShevronDownIcon />
            </IconContainer>
          ) : null}
        <DismissibleModal visible={modalVisible} onDismiss={hideModal}>
          <BottomMenuContainer>
            <Picker
              enabled={!disabled}
              selectedValue={options.findIndex(option => option.value === value)}
              onValueChange={value => {
                onChange?.(value === -1 ? undefined : options[value as number]?.value)
              }}
            >
              {items}
            </Picker>
          </BottomMenuContainer>
        </DismissibleModal>
      </InputButton>
    </FormInput>
  )
}
