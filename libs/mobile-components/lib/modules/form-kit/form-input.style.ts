import { Text, View, ViewStyle, TouchableOpacity } from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import { color } from '../../style/color'
import { sp } from '../../style/size'
import { LazyStyle, styled } from '../../style/styled'
import { font } from '../../style/text'

export interface InputFrameStyleProps {
  active?: boolean
  hasError?: boolean
  editable?: boolean
}

export const inputFrameStyle = ({
  editable = true,
  active = false,
  hasError = false
}: InputFrameStyleProps): LazyStyle<ViewStyle> => ({
  backgroundColor: color(editable ? 'surface' : 'background'),
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  borderRadius: 4,
  borderColor: color(
    active
      ? 'accent'
      : hasError
        ? 'error'
        : 'inactive'
  ),
  borderWidth: 1
} as const)

export const InputFrame = styled(View, inputFrameStyle)

export const InputButton = styled(TouchableOpacity, inputFrameStyle)

export const StaticInputValue = styled(Text, (props: {disabled?: boolean}) => ({
  ...font({ type: 'text1' }),
  color: color(props.disabled ?? false ? 'inactive' : 'text'),
  padding: sp(12)
}) as const)

export const FormInputContainer = View

export const FormInputLabel = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text'),
  marginBottom: sp(12)
})

export const FormInputError = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('error'),
  marginTop: sp(12)
})

export const FormInputSuccess = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent'),
  marginTop: sp(8)
})

export const BottomMenuContainer = styled(SafeAreaView, {
  marginTop: 'auto',
  backgroundColor: 'rgb(213, 215, 220)' // default iOS color
})
