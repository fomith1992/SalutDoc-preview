import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React, { useEffect, useRef, useState } from 'react'
import { useGetCommentsByPostIdQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { FullPost } from '../../posts'
import { CommentReplies } from '../comment-replies/comment-replies.container'
import { Comment } from '../comment/comment.container'
import { CommentsView, CommentsViewApi } from '../comments/comments.view'
import { PostCommentCreator } from './post-comment-creator.container'

interface PostCommentsProps {
  postId: string
}

export interface RenderHeaderProps {
  scrollToComments: () => void
}

export function PostComments ({ postId }: PostCommentsProps): React.ReactElement {
  const { data, loading, error, fetchMore } = useGetCommentsByPostIdQuery({
    variables: { postId }
  })
  const [replyToCommentId, setReplyToCommentId] = useState<string>()
  const fetchedComments = data?.postById?.comments
  const loadMoreComments = useMemoizedFn((after: string | undefined) => {
    fetchMore({
      variables: { after: expectDefined(after) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousComments = expectDefined(previousResult.postById?.comments)
        return {
          postById: {
            __typename: 'Post',
            id: postId,
            commentsCount: previousResult?.postById?.commentsCount ?? 0,
            comments: appendEdges(previousComments, fetchMoreResult?.postById?.comments),
            canCreateComment: fetchMoreResult?.postById?.canCreateComment ??
              previousResult.postById?.canCreateComment ??
              false
          }
        }
      }
    }).catch()
  })
  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])
  const commentsViewRef = useRef<CommentsViewApi>(null)

  const scrollToComment = (commentId: string): void => {
    const idx = fetchedComments?.edges.findIndex(({ node }) => node.id === commentId) ?? -1
    if (idx !== -1) {
      commentsViewRef.current?.scrollToCommentAt(idx)
    }
  }

  return (
    <CommentsView
      apiRef={commentsViewRef}
      header={
        <FullPost
          id={postId}
          onCommentsPressed={() => commentsViewRef.current?.scrollToComments()}
        />
      }
      comments={fetchedComments?.edges.map(({ node }) => node) ?? []}
      renderComment={({ id }) => (
        <Comment
          commentId={id}
          isRoot
          onReplyTo={() => setReplyToCommentId(id)}
          replies={
            <CommentReplies
              commentId={id}
              onReplyTo={setReplyToCommentId}
            />
          }
        />
      )}
      keyExtractor={comment => comment.id}
      hasMoreComments={fetchedComments?.pageInfo.hasNextPage ?? false}
      loadMoreComments={() => loadMoreComments(fetchedComments?.pageInfo.endCursor)}
      loadingMoreComments={loading}
      commentsCount={data?.postById?.commentsCount ?? 0}
      commentInput={data?.postById?.canCreateComment === true ? (
        <PostCommentCreator
          postId={postId}
          replyToCommentId={replyToCommentId}
          onCancelReply={() => setReplyToCommentId(undefined)}
          onCommentCreated={() => commentsViewRef.current?.scrollToComments()}
          onReplyCreated={() => scrollToComment(replyToCommentId ?? '')}
        />
      ) : null}
    />
  )
}
