import { FORM_ERROR, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import {
  GetCommentsByPostIdDocument,
  GetCommentsByPostIdQuery,
  GetCommentsByPostIdQueryVariables,
  usePostAddCommentMutation
} from '../../../gen/graphql'
import { AddCommentFormData, CommentCreatorView } from '../comment-creator/comment-creator.view'
import { useReplyCommentCreator } from '../reply-creator/reply-creator.hook'

export interface PostCommentCreatorProps {
  postId: string
  replyToCommentId?: string
  onCancelReply: () => void

  onCommentCreated?: (commentId: string) => void
  onReplyCreated?: (commentId: string) => void
}

export function PostCommentCreator (
  { postId, onCommentCreated, replyToCommentId, onReplyCreated, onCancelReply }: PostCommentCreatorProps
): React.ReactElement {
  const [addComment, { loading: submitting }] = usePostAddCommentMutation({
    update (cache, { data }) {
      const newComment = data?.postAddComment.comment
      if (newComment == null) {
        return
      }
      const existingComments = cache.readQuery<GetCommentsByPostIdQuery, GetCommentsByPostIdQueryVariables>({
        query: GetCommentsByPostIdDocument,
        variables: { postId }
      })
      if (existingComments?.postById?.comments != null) {
        cache.writeQuery<GetCommentsByPostIdQuery, GetCommentsByPostIdQueryVariables>({
          query: GetCommentsByPostIdDocument,
          variables: { postId },
          data: {
            postById: {
              ...existingComments.postById,
              commentsCount: (existingComments?.postById?.commentsCount ?? 0) + 1,
              comments: {
                ...existingComments.postById.comments,
                edges: [
                  { node: newComment, __typename: 'CommentEdge' },
                  ...existingComments.postById.comments.edges
                ]
              }
            }
          }
        })
      }
    }
  })

  const replyProps = useReplyCommentCreator({
    commentId: replyToCommentId,
    onCancelReply,
    onReplyCreated
  })

  const handleAddComment: SubmissionHandler<AddCommentFormData> = async (values) => {
    try {
      const { data } = await addComment({
        variables: {
          postId,
          content: { text: values.commentText }
        }
      })
      if (data?.postAddComment.comment == null) {
        return { [FORM_ERROR]: 'Failed to post a reply' }
      } else {
        onCommentCreated?.(data.postAddComment.comment.id)
        return {}
      }
    } catch (error) {
      console.log(error)
      return { [FORM_ERROR]: 'Failed to post a comment' }
    }
  }

  return replyProps == null ? (
    <CommentCreatorView
      onAddComment={handleAddComment}
      submitting={submitting}
    />
  ) : (
    <CommentCreatorView {...replyProps} />
  )
}
