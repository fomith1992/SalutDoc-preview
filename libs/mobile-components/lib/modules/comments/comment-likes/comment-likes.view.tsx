import React from 'react'
import {
  ActionBtn,
  ActionBtnContainer,
  ActionCount,
  DislikeButtonSvg,
  LikeFingerButtonSvg
} from './comment-likes.style'

export type LikeState = 'NEUTRAL' | 'LIKED' | 'DISLIKED'

export interface CommentLikesViewProps {
  likeState: LikeState
  likesCount: number
  dislikesCount: number

  onLikePressed: () => void
  onDislikePressed: () => void
}

export function CommentLikesView (
  {
    likeState,
    likesCount,
    dislikesCount,
    onLikePressed,
    onDislikePressed
  }: CommentLikesViewProps
): React.ReactElement {
  return (
    <ActionBtnContainer>
      <ActionBtn onPress={onLikePressed}>
        <LikeFingerButtonSvg isPressed={likeState === 'LIKED'} />
        <ActionCount isPressed={likeState === 'LIKED'}>
          {likesCount}
        </ActionCount>
      </ActionBtn>
      <ActionBtn onPress={onDislikePressed}>
        <DislikeButtonSvg isPressed={likeState === 'DISLIKED'} />
        <ActionCount isPressed={likeState === 'DISLIKED'}>
          {dislikesCount}
        </ActionCount>
      </ActionBtn>
    </ActionBtnContainer>
  )
}
