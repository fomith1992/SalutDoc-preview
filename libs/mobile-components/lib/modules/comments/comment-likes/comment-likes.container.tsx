import React, { useEffect, useMemo } from 'react'
import { Alert } from 'react-native'
import { LikeState, useGetCommentLikesQuery, useSetCommentLikeStateMutation } from '../../../gen/graphql'
import { CommentLikesView } from './comment-likes.view'

export interface CommentLikesProps {
  commentId: string
}

export function CommentLikes ({ commentId }: CommentLikesProps): React.ReactElement {
  const { data, error } = useGetCommentLikesQuery({
    variables: { commentId }
  })

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  const [mutateLiked] = useSetCommentLikeStateMutation()
  const likesCountWithoutMy = useMemo(
    () => {
      const comment = data?.commentById
      if (comment != null) {
        return comment.likesCount - (comment.likeStateByUser === 'LIKED' ? 1 : 0)
      } else {
        return 0
      }
    },
    [data?.commentById?.id]
  )

  const dislikesCountWithoutMy = useMemo(
    () => {
      const comment = data?.commentById
      if (comment != null) {
        return comment.dislikesCount - (comment.likeStateByUser === 'DISLIKED' ? 1 : 0)
      } else {
        return 0
      }
    },
    [data?.commentById?.id]
  )

  const setLikeState = (state: LikeState): void => {
    mutateLiked({
      variables: { commentId, state }
    }).catch(error => Alert.alert(error, 'Failed to set like'))
  }

  if (data?.commentById == null) {
    return <></>
  }

  const { likeStateByUser } = data.commentById

  return (
    <CommentLikesView
      likesCount={likesCountWithoutMy + (likeStateByUser === 'LIKED' ? 1 : 0)}
      dislikesCount={dislikesCountWithoutMy + (likeStateByUser === 'DISLIKED' ? 1 : 0)}
      likeState={likeStateByUser ?? 'NEUTRAL'}
      onLikePressed={() => setLikeState(likeStateByUser === 'LIKED' ? 'NEUTRAL' : 'LIKED')}
      onDislikePressed={() => setLikeState(likeStateByUser === 'DISLIKED' ? 'NEUTRAL' : 'DISLIKED')}
    />
  )
}
