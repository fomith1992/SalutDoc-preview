import { Text, TouchableOpacity, View } from 'react-native'
import { DislikeFingerIcon } from '../../../images/dislike-finger.icon'
import { LikeFingerIcon } from '../../../images/like-finger.icon'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const ActionBtnContainer = styled(View, {
  flexDirection: 'row',
  marginRight: sp(16)
})

export const ActionBtn = styled(TouchableOpacity, {
  flexDirection: 'row',
  alignItems: 'center',
  marginLeft: sp(8)
})

export const ActionCount = styled(Text, (props: { isPressed: boolean }) => ({
  ...font({ type: 'text1' }),
  marginLeft: sp(8),
  color: props.isPressed ? color('accent') : color('inactive')
}) as const)

export const DislikeButtonSvg = styled(DislikeFingerIcon, {
  width: sp(16),
  height: sp(16)
})

export const LikeFingerButtonSvg = styled(LikeFingerIcon, {
  width: sp(16),
  height: sp(16)
})
