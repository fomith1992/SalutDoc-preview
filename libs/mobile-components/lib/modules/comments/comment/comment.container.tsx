import { StackActions, useNavigation } from '@react-navigation/native'
import React from 'react'
import { Alert } from 'react-native'
import { RequestModal } from '../../../components/request-modal/request-modal.container'
import { useAuthenticatedUserOrNull } from '../../../components/user-context/user-context-provider'
import {
  useGetCommentByIdQuery,
  useArchiveCommentMutation
} from '../../../gen/graphql'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { CommentLikes } from '../comment-likes/comment-likes.container'
import { CommentView } from './comment.view'

export interface CommentProps {
  commentId: string
  onReplyTo: () => void
  isRoot: boolean
  replies?: React.ReactElement
}

export function Comment (
  {
    commentId,
    replies,
    isRoot,
    onReplyTo
  }: CommentProps
): React.ReactElement {
  const navigator = useNavigation()
  const user = useAuthenticatedUserOrNull()
  const [archiveCommentMutation] = useArchiveCommentMutation({
    onError: (response) => {
      console.log('Ошибка при удалении комментария', response)
      Alert.alert('Ошибка', 'Ошибка при удалении комментария')
    }
  })
  const { data } = useGetCommentByIdQuery({
    variables: {
      commentId
    }
  })
  const { state: reportModalVisible, setTrue: showReportModal, setFalse: hideReportModal } = useBooleanFlag(false)

  if (data?.commentById == null) {
    return <></>
  }
  const deleteComment = (): void => {
    archiveCommentMutation({
      variables: {
        commentId
      }
    })
  }
  const { author, text, canReply, archived } = data.commentById
  return (
    <>
      <CommentView
        author={author}
        text={text}
        isRoot={isRoot}
        archived={archived}
        handleAuthorClicked={() => navigator.dispatch(
          StackActions.push('Profile', { userId: data?.commentById?.author.id }))}
        replyToVisible={canReply}
        onReplyTo={onReplyTo}
        handleReport={showReportModal}
        handleCommentDeleted={deleteComment}
        isMy={user?.id === author.id}
        likes={<CommentLikes commentId={commentId} />}
        replies={replies}
      />
      <RequestModal
        isVisible={reportModalVisible}
        onCancel={hideReportModal}
        modalTitle='Отправить жалобу'
        placeholder='Опишите проблему'
        contentType='comment'
        contentId={data.commentById.id}
      />
    </>
  )
}
