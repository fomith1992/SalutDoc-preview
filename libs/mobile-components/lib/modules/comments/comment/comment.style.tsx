import { Text, TouchableHighlight, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const CommentContainer = styled(View, (props: { isRoot: boolean }) => ({
  flexDirection: 'row',
  marginTop: props.isRoot ? sp(8) : 0
}) as const)

export const CommentAvatarColumn = View

export const CommentAvatarContainer = styled(TouchableOpacity, (props: { isRoot: boolean }) => ({
  width: sp(props.isRoot ? 32 : 24),
  height: sp(props.isRoot ? 32 : 24),
  marginRight: sp(12)
}) as const)

export const CommentInfoContainer = styled(View, {
  flex: 1
})

export const CommentTitleContainer = styled(View, {
  flexDirection: 'row',
  justifyContent: 'space-between'
})

export const CommentAuthor = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' })
})

export const CommentText = styled(Text, (props: { isRoot: boolean }) => ({
  ...font({ type: 'text1' }),
  marginTop: sp(8),
  color: color(props.isRoot ? 'text' : 'inactive')
}) as const)

export const CommentActionsContainer = styled(View, {
  flexDirection: 'row',
  flex: 1,
  marginTop: sp(8)
})

export const ReplyBtn = styled(TouchableOpacity, {
  flex: 1
})

export const ReplyBtnText = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('inactive')
})

export const CommentLikesContainer = styled(View, {
  marginLeft: 'auto'
})

export const CommentReplyContainer = View

export const LoadMoreRepliesContainer = styled(View, {
  marginRight: 'auto',
  marginLeft: sp(48)
})

export const CommentTouchable = styled(View, {
  flexDirection: 'row'
})

export const CommentTouchableHighlight = styled(TouchableHighlight, (props: { isRoot: boolean }) => ({
  paddingLeft: sp(props.isRoot ? 8 : 48),
  paddingVertical: sp(8)
}) as const)

export const CommentContentContainer = styled(View, {
  flex: 1
})

export const CommentArchivedContentContainer = styled(View, {
  flex: 1,
  justifyContent: 'center'
})

export const CommentArchivedText = styled(Text, {
  opacity: 0.5
})

export const underlayColor = color('background')
