import React from 'react'
import { Alert, TouchableOpacity, View } from 'react-native'
import { Avatar } from '../../../components/avatar/avatar.view'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { useMaterialized } from '../../../style/styled'
import { ContextMenu, ContextMenuButton } from '../../ui-kit'
import {
  CommentActionsContainer,
  CommentAuthor,
  CommentAvatarColumn,
  CommentAvatarContainer,
  CommentArchivedContentContainer,
  CommentArchivedText,
  CommentContainer,
  CommentContentContainer,
  CommentInfoContainer,
  CommentLikesContainer,
  CommentReplyContainer,
  CommentText,
  CommentTitleContainer,
  CommentTouchable,
  CommentTouchableHighlight,
  ReplyBtn,
  ReplyBtnText,
  underlayColor
} from './comment.style'

export interface CommentViewProps {
  author: {
    firstName: string
    lastName: string
    avatar: string | null
  }
  text: string
  isRoot: boolean
  handleAuthorClicked?: () => void
  replyToVisible: boolean
  onReplyTo: () => void
  handleReport: () => void
  handleCommentDeleted: () => void
  isMy: boolean
  archived: boolean

  replies?: React.ReactElement
  likes?: React.ReactElement
}

function CommentContent (
  props: {
    text: string
    replyToVisible: boolean
    onReplyTo: () => void
    isRoot: boolean

    likes?: React.ReactElement
  }
): React.ReactElement {
  const { text, likes, replyToVisible, onReplyTo, isRoot } = props
  return (
    <View>
      <CommentText isRoot={isRoot}>
        {text}
      </CommentText>
      <CommentActionsContainer>
        {replyToVisible ? (
          <ReplyBtn onPress={onReplyTo}>
            <ReplyBtnText>
              Ответить
            </ReplyBtnText>
          </ReplyBtn>
        ) : null}
        <CommentLikesContainer>
          {likes}
        </CommentLikesContainer>
      </CommentActionsContainer>
    </View>
  )
}

export function CommentView (
  {
    author,
    text,
    isRoot,
    handleAuthorClicked,
    replyToVisible,
    onReplyTo,
    handleReport,
    handleCommentDeleted,
    isMy,
    replies,
    likes,
    archived
  }: CommentViewProps
): React.ReactElement {
  const { state: bottomMenuVisible, setTrue: showBottomMenu, setFalse: hideBottomMenu } = useBooleanFlag(false)
  const nonOwnMenuButtons: ContextMenuButton[] = []
  if (replyToVisible) {
    nonOwnMenuButtons.push({
      type: 'black',
      title: 'Ответить',
      onPress: () => {
        hideBottomMenu()
        onReplyTo()
      }
    })
  }
  nonOwnMenuButtons.push(
    {
      type: 'black',
      title: 'Пожаловаться',
      onPress: () => {
        hideBottomMenu()
        handleReport()
      }
    },
    {
      type: 'red',
      title: 'Отмена',
      onPress: hideBottomMenu
    }
  )

  const ownMenuButtons: ContextMenuButton[] = []
  if (replyToVisible) {
    ownMenuButtons.push({
      type: 'black',
      title: 'Ответить',
      onPress: () => {
        hideBottomMenu()
        onReplyTo()
      }
    })
  }
  ownMenuButtons.push(
    {
      type: 'black',
      title: 'Удалить комментарий',
      onPress: () => {
        Alert.alert(
          'Предупреждение!',
          'Вы уверены, что хотите удалить комментарий?',
          [
            {
              text: 'Отмена',
              onPress: () => {
                hideBottomMenu()
              }
            },
            {
              text: 'Да',
              onPress: () => {
                handleCommentDeleted()
                hideBottomMenu()
              }
            }
          ]
        )
      }
    },
    {
      type: 'red',
      title: 'Отмена',
      onPress: hideBottomMenu
    }
  )

  const touchableHighlightUnderlayColor = useMaterialized(underlayColor)

  return (
    <CommentContainer isRoot={isRoot}>
      <CommentInfoContainer>
        <CommentTouchableHighlight
          onPress={showBottomMenu}
          onLongPress={showBottomMenu}
          underlayColor={touchableHighlightUnderlayColor}
          isRoot={isRoot}
          disabled={archived}
        >
          {archived ? (
            <CommentTouchable>
              <CommentAvatarColumn>
                <CommentAvatarContainer onPress={handleAuthorClicked} isRoot={isRoot}>
                  <Avatar withLogo={false} />
                </CommentAvatarContainer>
              </CommentAvatarColumn>
              <CommentArchivedContentContainer>
                <CommentArchivedText>
                  Комментарий был удален
                </CommentArchivedText>
              </CommentArchivedContentContainer>
            </CommentTouchable>
          ) : (
            <CommentTouchable>
              <CommentAvatarColumn>
                <CommentAvatarContainer onPress={handleAuthorClicked} isRoot={isRoot}>
                  <Avatar url={author.avatar} />
                </CommentAvatarContainer>
              </CommentAvatarColumn>
              <CommentContentContainer>
                <CommentTitleContainer>
                  <TouchableOpacity onPress={handleAuthorClicked}>
                    <CommentAuthor>
                      {`${author.firstName} ${author.lastName}`}
                    </CommentAuthor>
                  </TouchableOpacity>
                  <ContextMenu
                    visible={bottomMenuVisible}
                    onClose={hideBottomMenu}
                    buttons={isMy ? ownMenuButtons : nonOwnMenuButtons}
                  />
                  {/* {isRoot ? <></> : (
                    <ReplyTitleText>
                      Профессору
                    </ReplyTitleText>
                  )} */}
                </CommentTitleContainer>
                <CommentContent
                  text={text}
                  likes={likes}
                  replyToVisible={replyToVisible}
                  onReplyTo={onReplyTo}
                  isRoot={isRoot}
                />
              </CommentContentContainer>
            </CommentTouchable>
          )}
        </CommentTouchableHighlight>
        <CommentReplyContainer>
          {replies}
        </CommentReplyContainer>
      </CommentInfoContainer>
    </CommentContainer>
  )
}
