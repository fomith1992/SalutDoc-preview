import { Text, TextInput, TouchableOpacity, View } from 'react-native'
import { CrossIcon16 } from '../../../images/cross.icon-16'
import { SendIcon24 } from '../../../images/send.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const ReplyUserContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: sp(8)
})

export const ReplyTextName = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('inactive'),
  marginRight: sp(4)
})

export const ReplyUserName = styled(Text, {
  ...font({ type: 'h4', weight: 'light' }),
  color: color('accent')
})

export const CommentInput = styled(TextInput, {
  borderRadius: 14,
  borderWidth: 1,
  borderColor: color('surface'),
  flex: 1,
  paddingTop: sp(4),
  paddingBottom: sp(4),
  paddingLeft: sp(16),
  marginRight: sp(12),
  ...font({ type: 'text1' })
})

export const ReplyCancelButton = styled(TouchableOpacity, {
  marginRight: sp(8)
})

export const CommentCreatorContainer = styled(View, {
  flexDirection: 'column',
  flex: 1
})

export const InputContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center'
})

export const CloseIconSvg = styled(CrossIcon16, {
  width: sp(12),
  height: sp(12),
  color: color('inactive')
})

export const SendImg = styled(SendIcon24, {
  color: color('subtext')
})
