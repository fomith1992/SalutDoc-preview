import { createFormDescriptor, FORM_ERROR, hasErrors, SubmissionHandler } from '@salutdoc/react-components'
import React, { useRef } from 'react'
import { Alert, TextInput, TouchableOpacity } from 'react-native'
import * as yup from 'yup'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import {
  CloseIconSvg,
  CommentCreatorContainer,
  CommentInput,
  InputContainer,
  ReplyCancelButton,
  ReplyTextName,
  ReplyUserContainer,
  ReplyUserName,
  SendImg
} from './comment-creator.style'

const formSchema = yup.object({
  commentText: yup.string().required()
}).required()

export type AddCommentFormData = yup.InferType<typeof formSchema>

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export interface CommentTextInputProps {
  value: string | undefined
  onChange: (value: string) => void
  onFocus: () => void
  onBlur: () => void
  placeholder?: string
}

const commentMaxLength = 1000

const CommentTextInput = React.forwardRef<TextInput, CommentTextInputProps>((
  {
    value,
    onChange,
    onFocus,
    onBlur,
    placeholder
  },
  ref
) => (
  <CommentInput
    maxLength={commentMaxLength}
    ref={ref}
    value={value}
    onChangeText={onChange}
    onFocus={onFocus}
    onBlur={onBlur}
    placeholder={placeholder}
    multiline
  />
))

function AddCommentButton (props: {
  valid: boolean
  onSubmit: () => void | Promise<void>
}): React.ReactElement {
  const { valid, onSubmit } = props
  return (
    <TouchableOpacity
      disabled={!valid}
      onPress={onSubmit}
    >
      <SendImg />
    </TouchableOpacity>
  )
}

export interface CommentCreatorViewProps {
  replyTo?: ReplyInfoProps
  onCancelReply?: () => void
  onAddComment: SubmissionHandler<AddCommentFormData>
  submitting: boolean
}

export interface ReplyInfoProps {
  author: {
    firstName: string
  }
}

interface ReplyUserProps {
  replyTo: ReplyInfoProps
  onCancel?: () => void
}

function ReplyUser ({ replyTo, onCancel }: ReplyUserProps): React.ReactElement {
  return (
    <ReplyUserContainer>
      <ReplyCancelButton onPress={onCancel}>
        <CloseIconSvg />
      </ReplyCancelButton>
      <ReplyTextName>
        ответ
      </ReplyTextName>
      <ReplyUserName>
        {replyTo.author.firstName}
      </ReplyUserName>
    </ReplyUserContainer>
  )
}

export function CommentCreatorView (
  {
    replyTo,
    onCancelReply,
    onAddComment,
    submitting
  }: CommentCreatorViewProps
): React.ReactElement {
  const textInputRef = useRef<TextInput>(null)
  return (
    <Form
      onSubmit={async values => {
        textInputRef.current?.blur()
        return await onAddComment(values)
      }}
      onSubmitted={(errors, formApi) => {
        if (hasErrors(errors)) {
          Alert.alert('Error', errors[FORM_ERROR] ?? 'Failed to post a comment')
        } else {
          formApi.reset()
        }
      }}
    >
      <CommentCreatorContainer>
        {replyTo !== undefined ? <ReplyUser replyTo={replyTo} onCancel={onCancelReply} /> : null}
        <InputContainer>
          <Field name='commentText'>
            {props => (
              <CommentTextInput
                {...props}
                ref={textInputRef}
                placeholder='Комментарий'
              />
            )}
          </Field>
          <Submit>
            {props => <AddCommentButton {...props} />}
          </Submit>
        </InputContainer>
      </CommentCreatorContainer>
      <ModalLoadingIndicator visible={submitting} />
    </Form>
  )
}
