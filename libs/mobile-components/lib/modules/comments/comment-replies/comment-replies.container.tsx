import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React from 'react'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useLoadRepliesQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { TextButton } from '../../ui-kit'
import { Comment } from '../comment/comment.container'
import { CommentReplyContainer, LoadMoreRepliesContainer } from '../comment/comment.style'

export interface CommentRepliesProps {
  commentId: string
  onReplyTo: (replyToCommentId: string) => void
}

export function CommentReplies (
  {
    commentId,
    onReplyTo
  }: CommentRepliesProps
): React.ReactElement {
  const { data, loading, fetchMore } = useLoadRepliesQuery({
    variables: { commentId }
  })

  const loadMoreReplies = useMemoizedFn(() => {
    fetchMore({
      variables: { after: expectDefined(data?.commentById?.repliesFlat?.pageInfo.endCursor), first: 10 },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousCommentState = expectDefined(previousResult.commentById)
        const previousComments = expectDefined(previousCommentState.repliesFlat)
        return {
          commentById: {
            ...previousCommentState,
            repliesFlat: appendEdges(previousComments, fetchMoreResult?.commentById?.repliesFlat)
          }
        }
      }
    }).catch(error => {
      console.log(error)
    })
  }, [data?.commentById?.repliesFlat?.pageInfo.endCursor])
  const hasMoreReplies = data?.commentById?.repliesFlat?.pageInfo.hasNextPage ?? false
  return (loading ? <LoadingIndicator visible />
    : (
      <CommentReplyContainer>
        {data?.commentById?.repliesFlat?.edges.map(({ node }) => (
          <Comment
            key={node.id}
            commentId={node.id}
            isRoot={false}
            onReplyTo={() => onReplyTo(node.id)}
          />
        ))}
        {hasMoreReplies ? (
          <LoadMoreRepliesContainer>
            <TextButton
              type='link'
              onPress={loadMoreReplies}
            >
              Показать еще
            </TextButton>
          </LoadMoreRepliesContainer>
        ) : null}
      </CommentReplyContainer>
    ))
}
