import { FORM_ERROR, SubmissionHandler } from '@salutdoc/react-components'
import {
  LoadRepliesDocument,
  LoadRepliesQuery,
  LoadRepliesQueryVariables,
  useAuthorByCommentIdQuery,
  useCommentAddReplyMutation
} from '../../../gen/graphql'
import { AddCommentFormData, CommentCreatorViewProps } from '../comment-creator/comment-creator.view'

export interface ReplyCommentCreatorProps {
  commentId?: string
  onCancelReply: () => void
  onReplyCreated?: (replyId: string) => void
}

export function useReplyCommentCreator (
  { commentId, onCancelReply, onReplyCreated }: ReplyCommentCreatorProps
): CommentCreatorViewProps | null {
  const [addReply, { loading: submittingReply }] = useCommentAddReplyMutation({
    update (cache, { data }) {
      const newReply = data?.commentAddReply.comment
      if (newReply == null) {
        return
      }
      const existingReplies = cache.readQuery<LoadRepliesQuery, LoadRepliesQueryVariables>({
        query: LoadRepliesDocument,
        variables: { commentId: newReply.root.id }
      })
      if (existingReplies?.commentById?.repliesFlat != null) {
        cache.writeQuery<LoadRepliesQuery, LoadRepliesQueryVariables>({
          query: LoadRepliesDocument,
          variables: { commentId: newReply.root.id },
          data: {
            commentById: {
              ...existingReplies.commentById,
              repliesFlat: {
                ...existingReplies.commentById.repliesFlat,
                edges: [
                  { node: newReply, __typename: 'CommentEdge' },
                  ...existingReplies.commentById.repliesFlat.edges
                ]
              }
            }
          }
        })
      }
    }
  })

  const { data } = useAuthorByCommentIdQuery({
    skip: commentId == null,
    variables: { commentId: commentId ?? '' }
  })

  if (commentId == null || data?.commentById == null) {
    return null
  } else {
    const handleAddComment: SubmissionHandler<AddCommentFormData> = async (values) => {
      try {
        const { data } = await addReply({
          variables: {
            commentId,
            content: { text: values.commentText }
          }
        })
        if (data?.commentAddReply.comment == null) {
          return { [FORM_ERROR]: 'Failed to post a reply' }
        } else {
          onReplyCreated?.(data.commentAddReply.comment.id)
          return {}
        }
      } catch (error) {
        console.log(error)
        return { [FORM_ERROR]: 'Failed to post a reply' }
      }
    }

    return {
      replyTo: data.commentById,
      onCancelReply: onCancelReply,
      onAddComment: handleAddComment,
      submitting: submittingReply
    }
  }
}
