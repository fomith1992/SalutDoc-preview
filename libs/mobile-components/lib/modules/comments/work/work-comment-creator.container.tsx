import { FORM_ERROR, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import {
  GetCommentsByWorkIdDocument,
  GetCommentsByWorkIdQuery,
  GetCommentsByWorkIdQueryVariables,
  useWorkAddCommentMutation
} from '../../../gen/graphql'
import { AddCommentFormData, CommentCreatorView } from '../comment-creator/comment-creator.view'
import { useReplyCommentCreator } from '../reply-creator/reply-creator.hook'

export interface WorkCommentCreatorProps {
  workId: string
  replyToCommentId?: string
  onCancelReply: () => void

  onCommentCreated?: (commentId: string) => void
  onReplyCreated?: (commentId: string) => void
}

export function WorkCommentCreator (
  { workId, onCommentCreated, replyToCommentId, onCancelReply, onReplyCreated }: WorkCommentCreatorProps
): React.ReactElement {
  const [addComment, { loading: submitting }] = useWorkAddCommentMutation({
    update (cache, { data }) {
      const newComment = data?.workAddComment.comment
      if (newComment == null) {
        return
      }
      const existingComments = cache.readQuery<GetCommentsByWorkIdQuery, GetCommentsByWorkIdQueryVariables>({
        query: GetCommentsByWorkIdDocument,
        variables: { workId }
      })
      if (existingComments?.workById?.comments != null) {
        cache.writeQuery<GetCommentsByWorkIdQuery, GetCommentsByWorkIdQueryVariables>({
          query: GetCommentsByWorkIdDocument,
          variables: { workId },
          data: {
            workById: {
              ...existingComments.workById,
              commentsCount: (existingComments?.workById?.commentsCount ?? 0) + 1,
              comments: {
                ...existingComments.workById.comments,
                edges: [
                  { node: newComment, __typename: 'CommentEdge' },
                  ...existingComments.workById.comments.edges
                ]
              }
            }
          }
        })
      }
    }
  })

  const replyProps = useReplyCommentCreator({
    commentId: replyToCommentId,
    onCancelReply,
    onReplyCreated
  })

  const handleAddComment: SubmissionHandler<AddCommentFormData> = async (values) => {
    try {
      const { data } = await addComment({
        variables: {
          workId,
          content: { text: values.commentText }
        }
      })
      if (data?.workAddComment.comment == null) {
        return { [FORM_ERROR]: 'Failed to post a reply' }
      } else {
        onCommentCreated?.(data.workAddComment.comment.id)
        return {}
      }
    } catch (error) {
      console.log(error)
      return { [FORM_ERROR]: 'Failed to post a comment' }
    }
  }

  return replyProps == null ? (
    <CommentCreatorView
      onAddComment={handleAddComment}
      submitting={submitting}
    />
  ) : (
    <CommentCreatorView {...replyProps} />
  )
}
