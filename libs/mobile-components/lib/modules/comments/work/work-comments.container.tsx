import { appendEdges, expectDefined } from '@salutdoc/react-components'
import React, { useEffect, useRef, useState } from 'react'
import { Work } from '../../../components/work/work-card/work.container'
import { useGetCommentsByWorkIdQuery } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { CommentReplies } from '../comment-replies/comment-replies.container'
import { Comment } from '../comment/comment.container'
import { CommentsView, CommentsViewApi } from '../comments/comments.view'
import { WorkCommentCreator } from './work-comment-creator.container'

interface WorkCommentsProps {
  workId: string
}

export function WorkComments ({ workId }: WorkCommentsProps): React.ReactElement {
  const { data, loading, error, fetchMore } = useGetCommentsByWorkIdQuery({
    variables: { workId }
  })
  const [replyToCommentId, setReplyToCommentId] = useState<string>()
  const fetchedComments = data?.workById?.comments
  const loadMoreComments = useMemoizedFn((after: string | undefined) => {
    fetchMore({
      variables: { after: expectDefined(after) },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        const previousComments = expectDefined(previousResult.workById?.comments)
        return {
          workById: {
            __typename: 'Article',
            id: workId,
            commentsCount: previousResult?.workById?.commentsCount ?? 0,
            comments: appendEdges(previousComments, fetchMoreResult?.workById?.comments),
            canCreateComment: fetchMoreResult?.workById?.canCreateComment ??
              previousResult.workById?.canCreateComment ??
              false
          }
        }
      }
    }).catch()
  })
  const commentsViewRef = useRef<CommentsViewApi>(null)

  const scrollToComment = (commentId: string): void => {
    const idx = fetchedComments?.edges.findIndex(({ node }) => node.id === commentId) ?? -1
    if (idx !== -1) {
      commentsViewRef.current?.scrollToCommentAt(idx)
    }
  }

  useEffect(() => {
    if (error != null) {
      console.log(error)
    }
  }, [error])

  return (
    <CommentsView
      apiRef={commentsViewRef}
      header={
        <Work
          workId={workId}
          handleCommentsPressed={() => commentsViewRef.current?.scrollToComments()}
        />
      }
      comments={fetchedComments?.edges.map(({ node }) => node) ?? []}
      renderComment={({ id }) => (
        <Comment
          commentId={id}
          isRoot
          onReplyTo={() => setReplyToCommentId(id)}
          replies={
            <CommentReplies
              commentId={id}
              onReplyTo={setReplyToCommentId}
            />
          }
        />
      )}
      keyExtractor={comment => comment.id}
      hasMoreComments={fetchedComments?.pageInfo.hasNextPage ?? false}
      loadMoreComments={() => loadMoreComments(fetchedComments?.pageInfo.endCursor)}
      loadingMoreComments={loading}
      commentsCount={data?.workById?.commentsCount ?? 0}
      commentInput={data?.workById?.canCreateComment === true ? (
        <WorkCommentCreator
          workId={workId}
          replyToCommentId={replyToCommentId}
          onCancelReply={() => setReplyToCommentId(undefined)}
          onCommentCreated={() => commentsViewRef.current?.scrollToComments()}
          onReplyCreated={() => scrollToComment(replyToCommentId ?? '')}
        />
      ) : null}
    />
  )
}
