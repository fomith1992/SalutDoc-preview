import React, { Ref, useImperativeHandle, useRef } from 'react'
import { FlatList } from 'react-native'
import { DividerView } from '../../../components/divider/divider.view'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { toCaseCount } from '../../../i18n/utils'
import { useMaterialized } from '../../../style/styled'
import {
  CommentInputContainer,
  CommentsContainer,
  CommentsCount,
  commentsListContentContainerStyleSheet
} from './comments.style'

interface CommentsViewProps<T> {
  apiRef?: Ref<CommentsViewApi>

  header: React.ReactNode
  commentInput: React.ReactNode

  commentsCount: number
  comments: T[]
  renderComment: (item: T) => React.ReactElement
  keyExtractor: (item: T) => string

  loadingMoreComments: boolean
  hasMoreComments: boolean
  loadMoreComments: () => void
}

export interface CommentsViewApi {
  scrollToComments: () => void

  scrollToCommentAt: (index: number) => void
}

export function CommentsView<T> (
  {
    apiRef,

    header,
    commentInput,

    commentsCount,
    comments,
    renderComment,
    keyExtractor,

    loadingMoreComments,
    hasMoreComments,
    loadMoreComments
  }: CommentsViewProps<T>
): React.ReactElement {
  const flatListRef = useRef<FlatList<T>>(null)
  useImperativeHandle(apiRef, () => ({
    scrollToComments: () => comments.length > 0 ? flatListRef.current?.scrollToIndex({ index: 0 }) : undefined,
    scrollToCommentAt: (index) => flatListRef.current?.scrollToIndex({ index })
  }), [flatListRef, comments])
  const commentsListContentContainerStyle = useMaterialized(commentsListContentContainerStyleSheet)

  return (
    <CommentsContainer>
      <FlatList
        ref={flatListRef}
        ListHeaderComponent={
          <>
            {header}
            <DividerView />
            <CommentsCount>
              {`${commentsCount} ${toCaseCount(['комментарий', 'комментария', 'комментариев'], commentsCount)}`}
            </CommentsCount>
          </>
        }
        contentContainerStyle={commentsListContentContainerStyle}
        renderItem={({ item }) => renderComment(item)}
        data={comments}
        ListFooterComponent={<LoadingIndicator visible={loadingMoreComments} />}
        onEndReached={hasMoreComments ? loadMoreComments : null}
        keyExtractor={keyExtractor}
      />
      <DividerView isFull />
      <CommentInputContainer>
        {commentInput}
      </CommentInputContainer>
    </CommentsContainer>
  )
}
