import { Text, View, ViewStyle } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { lift, styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const Container = View

export const CommentsContainer = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})

export const CommentInputContainer = styled(View, {
  ...container(),
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: 10,
  minHeight: 30,
  maxHeight: 66,
  marginBottom: 12
})

export const CommentsCount = styled(Text, {
  ...container(),
  ...font({ type: 'h4', weight: 'light' }),
  color: color('inactive'),
  marginTop: 12
})

export const commentsListContentContainerStyleSheet = lift<ViewStyle>({
  paddingBottom: sp(16)
})
