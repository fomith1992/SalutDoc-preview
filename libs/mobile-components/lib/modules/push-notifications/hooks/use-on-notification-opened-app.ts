import { either } from 'fp-ts'
import { pipe } from 'fp-ts/lib/function'
import { draw } from 'io-ts/Decoder'
import messaging from '@react-native-firebase/messaging'
import { useEffect, RefObject } from 'react'
import { notificationData } from '@salutdoc/push-notification-schema'
import { NavigationContainerRef } from '@react-navigation/native'
import { Role } from '../../../gen/graphql'

export function useOnNotificationOpenedApp (
  navigationRef: RefObject<NavigationContainerRef>,
  site: Role | null
): void {
  useEffect(() => messaging().onNotificationOpenedApp((message) => {
    if (message.data != null) {
      const data = pipe(
        message.data,
        notificationData.decode,
        either.getOrElseW(errors => {
          console.warn('Failed to parse push notification data: ', draw(errors))
          return null
        })
      )
      switch (data?.type) {
        case 'NEW_CONSULTATION': {
          navigationRef.current?.navigate('FreeConsultationChat', { consultationId: data.consultationId })
          break
        }
        case 'START_CONSULTATION':
        case 'NEW_MESSAGE':
        case 'ADD_REPORT_CONSULTATION':
        case 'FINISH_CONSULTATION': {
          navigationRef.current?.navigate(site === 'USER' ? 'PatientConsultationChat' : 'DoctorConsultationChat', { consultationId: data.consultationId })
          break
        }
      }
    }
  }), [])
}
