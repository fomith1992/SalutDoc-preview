import { either } from 'fp-ts'
import { pipe } from 'fp-ts/lib/function'
import { draw } from 'io-ts/Decoder'
import messaging from '@react-native-firebase/messaging'
import { useEffect, RefObject } from 'react'
import { notificationData } from '@salutdoc/push-notification-schema'
import { NavigationContainerRef } from '@react-navigation/native'
import { useShowNotification } from '../../ui-kit'
import { playLocalSoundFile } from '../../../utils/play-sounds/play-sound-file'
import { consultationToDoctor } from '../../consultations/i18n/specialization'
import { SpecializationIcon } from '../../consultations/specialization-icon'

export function useOnNotificationShowInAppMessage (
  navigationRef: RefObject<NavigationContainerRef>
): void {
  const showNotification = useShowNotification()
  useEffect(() => messaging().onMessage((message) => {
    if (message.data != null) {
      const data = pipe(
        message.data,
        notificationData.decode,
        either.getOrElseW(errors => {
          console.warn('Failed to parse push notification data: ', draw(errors))
          return null
        })
      )

      if (data?.type === 'NEW_CONSULTATION') {
        showNotification({
          icon: () => SpecializationIcon({ specialization: data.specialization }),
          title: 'Новая консультация',
          message: consultationToDoctor[data.specialization] ?? data.specialization,
          onDisplayed: () => playLocalSoundFile('notifications'),
          onPress: () => navigationRef.current?.navigate('FreeConsultationChat', { consultationId: data.consultationId })
        })
      }
    }
  }), [])
}
