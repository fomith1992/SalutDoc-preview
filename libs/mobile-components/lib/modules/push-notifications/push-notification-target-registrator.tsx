import messaging from '@react-native-firebase/messaging'
import React, { useMemo } from 'react'
import { Platform } from 'react-native'
import { getUniqueId } from 'react-native-device-info'
import useObservable from 'react-use/lib/useObservable'
import * as rx from 'rxjs'
import * as $ from 'rxjs/operators'
import { useAuthenticatedUserOrNull } from '../../components/user-context/user-context-provider'
import { usePushNotificationTargetRegistratorMutation } from '../../gen/graphql'
import { useBehaviorSubject } from '../../hooks/use-behavior-subject'

const os = Platform.select({
  android: 'ANDROID',
  ios: 'IOS'
} as const)

const deviceId = getUniqueId()

const initialToken$ = rx.defer(async () => await messaging().getToken()).pipe(
  $.retryWhen(errors => errors.pipe(
    $.tap(error => {
      console.error('[FCM] Failed to get FCM token, retrying in 5 seconds...', error)
    }),
    $.delay(5000)
  ))
)

const tokenRefreshed$ = rx.fromEventPattern<string>(
  handler => messaging().onTokenRefresh(handler),
  (_, off) => off()
)

const token$ = tokenRefreshed$.pipe(
  $.map(token => rx.of(token)),
  $.startWith(initialToken$),
  $.switchAll()
)

export function PushNotificationTargetRegistrator (): React.ReactElement {
  if (os == null || deviceId === 'unknown') {
    // works only for iOS and Android with known deviceId
    return <></>
  }
  const user$ = useBehaviorSubject(useAuthenticatedUserOrNull())
  const [registerPushNotificationTarget] = usePushNotificationTargetRegistratorMutation()

  const registration$ = useMemo(() => rx.zip(user$, token$).pipe(
    $.switchMap(([user, token]) => user == null ? [] : rx.defer(async () => await registerPushNotificationTarget({
      variables: {
        deviceId,
        os,
        token,
        userId: user.id
      }
    })).pipe(
      $.tap(value => {
        const result = value.data?.registerNotificationTarget
        if ((value.errors != null && value.errors.length !== 0) ||
          (result?.errors != null && result.errors.length !== 0)) {
          console.error('Notification target registration error', value)
          throw new Error('Notification target registration error')
        }
      }),
      $.retryWhen(errors => errors.pipe(
        $.tap(error => {
          console.error('Failed to register notification target, retrying in 5 seconds...', error)
        }),
        $.delay(5000)
      )),
      $.tap(value => {
        console.log('Notification target registered', value.data?.registerNotificationTarget.notificationsTarget)
      })
    ))
  ), [user$, token$])

  useObservable(registration$)

  return <></>
}
