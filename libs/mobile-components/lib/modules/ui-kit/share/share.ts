import { Share, Alert } from 'react-native'

export async function shareUrl (
  url: string = 'https://salutdoc.com/'
): Promise<void> {
  try {
    await Share.share({
      message: `SalutDoc\nПереходите по ссылке: ${url}`,
      url,
      title: 'SalutDoc'
    }, {
      dialogTitle: 'SalutDoc'
    })
  } catch (error) {
    console.error(error.message)
    Alert.alert('Не удалось поделиться')
  }
}

export async function inviteToCommunity (
  url: string = 'https://salutdoc.com'
): Promise<void> {
  try {
    await Share.share({
      message: `SalutDoc Community Invitation\nВас пригласили в сообщество: ${url}`,
      url,
      title: 'Send SalutDoc Community Invitation'
    }, {
      dialogTitle: 'SalutDoc'
    })
  } catch (error) {
    console.error(error.message)
    Alert.alert('Не удалось поделиться')
  }
}
