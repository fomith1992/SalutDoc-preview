
import { IconComponent } from '../../../images/icon-component'

export interface NotificationProps {
  avatar?: string | null
  title: string
  message: string
  icon?: IconComponent
  duration?: number
  onDisplayed?: () => void
  onPress?: () => void
}

export interface NotificationHandle {
  dismiss: (onDismissed?: () => void) => void
}

export type ShowNotificationFn = (props: NotificationProps) => NotificationHandle
