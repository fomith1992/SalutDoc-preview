import { Platform, Text, View, TouchableOpacity } from 'react-native'
import { ImageOrIconView } from '../../../components/image-or-icon/image-or-icon.view'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const NotificationBox = styled(TouchableOpacity, {
  backgroundColor: color('surface'),
  borderRadius: sp(8),
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  marginHorizontal: sp(32),
  marginTop: sp(8),
  paddingVertical: sp(8),
  paddingHorizontal: sp(16),
  ...Platform.select({
    android: {
      elevation: 6
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.06,
      shadowRadius: 12
    }
  })
})

export const NotificationIconContainer = styled(View, {
  marginRight: sp(16),
  overflow: 'hidden',
  width: sp(40),
  height: sp(40)
})

export const NotificationContentContainer = styled(View, {
  flex: 1
})

export const NotificationTitle = styled(Text, {
  ...font({ type: 'h3', weight: 'strong' }),
  color: color('text')
})

export const NotificationText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text')
})

export const NotificationIconView = styled(ImageOrIconView, {
  borderWidth: 1,
  borderColor: color('inactiveLight'),
  borderStyle: 'solid'
})
