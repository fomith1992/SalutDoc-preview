import React, { useEffect, useRef, useState } from 'react'
import { Animated, SafeAreaView, StyleSheet } from 'react-native'
import useLatest from 'react-use/lib/useLatest'
import { IconComponent } from '../../../images/icon-component'
import { Portal } from '../portal'
import {
  NotificationBox,
  NotificationContentContainer,
  NotificationIconContainer,
  NotificationText,
  NotificationTitle
} from './notification-banner.style'
import { NotificationIcon } from './notification-icon'

export interface NotificationBannerProps {
  avatar?: string | null
  title: React.ReactText
  message: React.ReactText
  icon?: IconComponent

  dismissRequested: boolean
  onDisplayed: () => void
  onDismissed: () => void
  onPress?: () => void
}

const transitionDuration = 400

export function NotificationBanner (
  {
    avatar,
    title,
    message,
    icon,
    dismissRequested,
    onPress,
    onDisplayed,
    onDismissed
  }: NotificationBannerProps
): React.ReactElement {
  const translation = useRef(new Animated.Value(0)).current
  const enterAnimation = useRef(Animated.timing(translation, {
    toValue: 1,
    useNativeDriver: true,
    duration: transitionDuration
  })).current
  const leaveAnimation = useRef(Animated.timing(translation, {
    toValue: 0,
    useNativeDriver: true,
    duration: transitionDuration
  })).current

  const handleDisplayedRef = useLatest(onDisplayed)
  const handleDismissedRef = useLatest(onDismissed)

  const [animating, setAnimating] = useState(false)
  const [displayed, setDisplayed] = useState(false)
  const [dismissed, setDismissed] = useState(false)

  useEffect(() => {
    if (animating) return
    if (!displayed) {
      setAnimating(true)
      enterAnimation.start(() => {
        setDisplayed(true)
        setAnimating(false)
        handleDisplayedRef.current()
      })
    } else if (dismissRequested && !dismissed) {
      setAnimating(true)
      leaveAnimation.start(() => {
        setDismissed(true)
        setAnimating(false)
        handleDismissedRef.current()
      })
    }
  }, [dismissRequested, dismissed, displayed, animating])

  return (
    <Portal>
      <SafeAreaView
        style={StyleSheet.absoluteFill}
        pointerEvents='box-none'
      >
        <Animated.View
          pointerEvents='box-none'
          style={{
            transform: [
              {
                translateY: translation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-100, 0]
                })
              }
            ]
          }}
        >
          <NotificationBox
            onPress={onPress}
          >
            <NotificationIconContainer>
              <NotificationIcon url={avatar} icon={icon} />
            </NotificationIconContainer>
            <NotificationContentContainer>
              <NotificationTitle
                numberOfLines={1}
                ellipsizeMode='tail'
              >
                {title}
              </NotificationTitle>
              <NotificationText
                numberOfLines={1}
                ellipsizeMode='tail'
              >
                {message}
              </NotificationText>
            </NotificationContentContainer>
          </NotificationBox>
        </Animated.View>
      </SafeAreaView>
    </Portal>
  )
}
