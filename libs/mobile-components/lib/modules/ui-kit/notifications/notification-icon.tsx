import React from 'react'
import { ExclamationIcon24 } from '../../../images/exclamation.icon-24'
import { NotificationIconView } from './notification-banner.style'
import { IconComponent } from '../../../images/icon-component'

export interface NotificationIconProps {
  url?: string | null
  icon?: IconComponent
}

export function NotificationIcon ({
  url,
  icon = ExclamationIcon24
}: NotificationIconProps): React.ReactElement {
  return (
    <NotificationIconView
      url={url}
      shape='circle'
      placeholder={icon}
      placeholderColor='text'
    />
  )
}
