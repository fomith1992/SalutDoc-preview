import React, { useCallback, useContext, useMemo, useState } from 'react'
import { useCounterFn } from '../../../hooks/use-counter-fn'
import { NotificationBanner } from './notification-banner'
import { NotificationProps, ShowNotificationFn } from './types'

export interface NotificationsContext {
  showNotification: ShowNotificationFn
}

const notificationsContext = React.createContext<NotificationsContext | null>(null)

export interface NotificationsHostProps {
  children?: React.ReactNode
}

interface NotificationState {
  readonly key: number
  readonly props: NotificationProps
  readonly onDisplayed: Array<() => void>
  readonly onDismissed: Array<() => void>
  dismissRequested: boolean
  dismissed: boolean
}

const defaultDuration = 5000 // 5 sec

export function NotificationsHost (
  {
    children
  }: NotificationsHostProps
): React.ReactElement {
  const nextKey = useCounterFn()
  const [queue, setQueue] = useState<NotificationState[]>([])

  const active = queue[0]
  if (active != null && queue.length > 1) {
    active.dismissRequested = true
  }

  const showNotification = useCallback<ShowNotificationFn>((props) => {
    const state: NotificationState = {
      key: nextKey(),
      props,
      dismissRequested: false,
      dismissed: false,
      onDisplayed: [],
      onDismissed: []
    }
    if (props.onDisplayed != null) {
      state.onDisplayed.push(props.onDisplayed)
    }
    setQueue(queue => [...queue, state])

    return {
      dismiss: onDismissed => {
        if (state.dismissed) {
          onDismissed?.()
        } else {
          if (onDismissed != null) {
            state.onDismissed.push(onDismissed)
          }
          if (!state.dismissRequested) {
            state.dismissRequested = true
            setQueue(queue => [...queue])
          }
        }
      }
    }
  }, [])

  const context = useMemo(() => ({
    showNotification
  }), [showNotification])

  return (
    <notificationsContext.Provider
      value={context}
    >
      {children}
      {active == null ? null : (
        <NotificationBanner
          avatar={active.props.avatar}
          title={active.props.title}
          key={active.key}
          icon={active.props.icon}
          message={active.props.message}
          dismissRequested={active.dismissRequested}
          onPress={() => {
            active.props.onPress?.()
            if (!active.dismissRequested) {
              active.dismissRequested = true
              setQueue(queue => [...queue])
            }
          }}
          onDisplayed={() => {
            setTimeout(() => {
              if (!active.dismissRequested) {
                active.dismissRequested = true
                setQueue(queue => [...queue])
              }
            }, active.props.duration ?? defaultDuration)
            active.onDisplayed.forEach(it => it())
          }}
          onDismissed={() => {
            active.dismissed = true
            setQueue(queue => queue.filter(n => n.key !== active.key))
            active.onDismissed.forEach(it => it())
          }}
        />
      )}
    </notificationsContext.Provider>
  )
}

export function useShowNotification (): ShowNotificationFn {
  const context = useContext(notificationsContext)
  if (context == null) {
    throw new Error(
      'Looks like you forgot to wrap your root component with `NotificationsHost` component.'
    )
  }
  return context.showNotification
}
