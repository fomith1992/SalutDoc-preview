import { Platform, SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const ModalBtn = styled(TouchableOpacity, {
  paddingVertical: sp(12)
})

export const ModalBtnText = styled(Text, (props: { type: 'red' | 'black' | 'title' }) => ({
  ...font(props.type === 'title'
    ? { type: 'text1' }
    : { type: 'h3', weight: 'strong' }
  ),
  paddingHorizontal: props.type === 'title' ? sp(12) : 0,
  textAlign: 'center',
  color: color(props.type === 'red' ? 'error' : 'text')
}) as const)

export const MenuContainer = styled(SafeAreaView, {
  marginTop: 'auto'
})

export const MenuFrame = styled(View, {
  ...container(),
  backgroundColor: color('surface'),
  borderRadius: sp(4),
  marginBottom: sp(24),
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})
