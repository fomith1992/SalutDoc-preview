import React from 'react'
import { DismissibleModal } from '../dismissible-modal'
import { MenuContainer, MenuFrame } from './context-menu.style'
import { MenuButton } from './menu-button'

export interface ContextMenuButton {
  type: 'red' | 'black' | 'title'
  title: string
  onPress: () => void
}

export interface ContextMenuProps {
  buttons: ContextMenuButton[]
  visible: boolean
  onClose: () => void
}

export function ContextMenu ({ buttons, visible, onClose }: ContextMenuProps): React.ReactElement {
  return (
    <DismissibleModal
      background='gray'
      visible={visible}
      onDismiss={onClose}
    >
      <MenuContainer>
        <MenuFrame>
          {buttons.map((item, idx) => <MenuButton {...item} key={idx} />)}
        </MenuFrame>
      </MenuContainer>
    </DismissibleModal>
  )
}
