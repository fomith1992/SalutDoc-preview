import React from 'react'
import { ModalBtn, ModalBtnText } from './context-menu.style'

interface MenuButtonProps {
  type: 'red' | 'black' | 'title'
  title: string
  onPress: () => void
}

export function MenuButton ({ type, title, onPress }: MenuButtonProps): React.ReactElement {
  return (
    <ModalBtn
      onPress={onPress}
      disabled={type === 'title'}
    >
      <ModalBtnText type={type}>{title}</ModalBtnText>
    </ModalBtn>
  )
}
