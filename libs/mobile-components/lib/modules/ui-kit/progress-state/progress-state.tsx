import React from 'react'
import { TextButton } from '../buttons'
import { CircleWithIcon } from '../circle-with-icon/circle-with-icon.view'
import { ProgressStateIndicator, ProgressStatus } from './progress-state-indicator'
import { ButtonContainer, Centered, Container, Info, Title } from './progress-state.style'

export interface ProgressStateProps {
  status: ProgressStatus
  title: React.ReactNode
  description: React.ReactNode
  button?: {
    text: React.ReactNode
    onPress?: () => void
  }
}

export function ProgressState (
  {
    status,
    title,
    description,
    button
  }: ProgressStateProps
): React.ReactElement {
  const handlePress = button?.onPress
  return (
    <Container>
      <Centered>
        <CircleWithIcon
          filled={status === 'SUCCESS'}
        >
          <ProgressStateIndicator status={status} />
        </CircleWithIcon>
        <Title>{title}</Title>
        <Info>{description}</Info>
      </Centered>
      {button == null ? null : (
        <ButtonContainer>
          <TextButton
            onPress={handlePress}
            type='primary'
            size='XL'
          >
            {button.text}
          </TextButton>
        </ButtonContainer>
      )}
    </Container>
  )
}
