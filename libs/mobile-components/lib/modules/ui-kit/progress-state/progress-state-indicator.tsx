import React from 'react'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { FailureIconSvg, SuccessIconSvg, RefreshIconSvg } from './progress-state.style'

export type ProgressStatus = 'IN_PROGRESS' | 'SUCCESS' | 'FAILURE' | 'REFRESH'

export type ProgressStateIndicatorProps = {
  status: ProgressStatus
}

export function ProgressStateIndicator (
  {
    status
  }: ProgressStateIndicatorProps
): React.ReactElement {
  switch (status) {
    case 'IN_PROGRESS': return <LoadingIndicator visible size='enormous' />
    case 'SUCCESS': return <SuccessIconSvg />
    case 'FAILURE': return <FailureIconSvg />
    case 'REFRESH': return <RefreshIconSvg />
  }
}
