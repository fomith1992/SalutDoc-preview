import { useNavigation } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React, { useLayoutEffect } from 'react'
import { DropDownItemProps } from '../../../components/drop-down/drop-down.view'
import { Header } from './header'
import { HeaderActionProps } from './header-actions.view'
import { TextHeaderTitleView } from './text-header-title.view'

export interface DefaultHeaderOptions {
  title?: string
  dropDownItems?: DropDownItemProps[]
  actions?: HeaderActionProps[]
  hasShadow?: boolean
}

export function useDefaultHeader (
  {
    title,
    dropDownItems,
    actions,
    hasShadow
  }: DefaultHeaderOptions
): void {
  const navigation = useNavigation<StackNavigationProp<any>>()
  useLayoutEffect(() => {
    navigation.setOptions({
      ...(title == null ? {} : { title }),
      header: (props) => (
        <Header
          headerProps={props}
          renderTitle={(title) => <TextHeaderTitleView title={title} dropdownItems={dropDownItems} />}
          actions={actions}
          hasShadow={hasShadow}
        />
      )
    })
  }, [navigation, title, dropDownItems, actions, hasShadow])
}
