import { Text, TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { DownIcon16 } from '../../../images/down.icon-16'

export const HeaderButton = styled(TouchableOpacity, {
  flex: 1,
  alignItems: 'center',
  flexDirection: 'row'
})

export const Title = styled(Text, {
  ...font({ type: 'h1' }),
  color: color('text'),
  marginRight: sp(8)
})

export const TitleDown = styled(DownIcon16, {
  width: sp(12),
  height: sp(12),
  color: color('accent')
})

export const ModalContainer = styled(TouchableOpacity, {
  flex: 1,
  backgroundColor: 'rgb(0, 0, 0, 0, 25)'
})
