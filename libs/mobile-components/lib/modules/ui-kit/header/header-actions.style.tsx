import { TouchableOpacity, View } from 'react-native'
import { VerticalDotsIcon24 } from '../../../images/vertical-dots.icon-24'
import { PlusIcon16 } from '../../../images/plus.icon-16'
import { SettingsIcon24 } from '../../../images/settings.icon-24'
import { NotificationsIcon24 } from '../../../images/notifications.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { SearchIcon24 } from '../../../images/search.icon-24'
import { WriteIcon24 } from '../../../images/write.icon-24'
import { TickIcon24 } from '../../../images/tick.icon-24'
import { HelpIcon24 } from '../../../images/help.icon-24'

export const ActionsContainer = styled(View, {
  marginLeft: 'auto',
  flexDirection: 'row'
})

export const ActionButton = styled(TouchableOpacity, {
  alignItems: 'center',
  flexDirection: 'row',
  padding: sp(16)
})

export const HeaderSettingsIcon = styled(SettingsIcon24, {
  color: color('accent')
})

export const HeaderMoreIcon = styled(VerticalDotsIcon24, {
  color: color('accent')
})

export const HeaderWriteIcon = styled(WriteIcon24, {
  color: color('accent')
})

export const TickInactiveIcon = styled(TickIcon24, {
  color: color('inactive')
})

export const TickIcon = styled(TickIcon24, {
  color: color('accent')
})

export const HelpIcon = styled(HelpIcon24, {
  color: color('accent')
})

export const HeaderPlusIcon = styled(PlusIcon16, {
  color: color('accent')
})

export const HeaderNotificationsIcon = styled(NotificationsIcon24, {
  color: color('text')
})

export const SearchIcon = styled(SearchIcon24, {
  color: color('text')
})
