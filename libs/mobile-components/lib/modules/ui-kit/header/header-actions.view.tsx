import {
  ActionButton,
  ActionsContainer,
  HeaderMoreIcon,
  HeaderNotificationsIcon,
  HeaderPlusIcon,
  HeaderSettingsIcon,
  HeaderWriteIcon,
  HelpIcon,
  SearchIcon,
  TickIcon,
  TickInactiveIcon
} from './header-actions.style'
import { PlusDropIcon } from '../../../images/plus-drop.icon'
import React from 'react'
import { TicketIcon } from '../../../images/ticket.icon'
import { StatisticsIcon } from '../../../images/statistics.icon'
import { Badge } from '../badge/badge'

export interface HeaderActionsViewProps {
  actions?: HeaderActionProps[]
}

export type HeaderIconType = 'settings' | 'more' | 'plus' | 'plus-drop' | 'search' | 'notifications' | 'ticket' | 'statistics' | 'write' | 'tick' | 'tick-inactive' | 'help'

export interface HeaderIconProps {
  withDot?: boolean
}

export interface HeaderActionProps {
  icon: HeaderIconType
  disabled?: boolean
  iconProps?: HeaderIconProps
  onPress?: () => void
}

function renderHeaderIcon (icon: HeaderIconType): React.ReactElement {
  switch (icon) {
    case 'settings':
      return <HeaderSettingsIcon />
    case 'more':
      return <HeaderMoreIcon />
    case 'plus':
      return <HeaderPlusIcon />
    case 'plus-drop':
      return <PlusDropIcon />
    case 'search':
      return <SearchIcon />
    case 'ticket':
      return <TicketIcon />
    case 'notifications':
      return <HeaderNotificationsIcon />
    case 'statistics':
      return <StatisticsIcon />
    case 'write':
      return <HeaderWriteIcon />
    case 'tick':
      return <TickIcon />
    case 'tick-inactive':
      return <TickInactiveIcon />
    case 'help':
      return <HelpIcon />
  }
}

export function HeaderActionsView (
  {
    actions
  }: HeaderActionsViewProps
): React.ReactElement {
  return (
    <ActionsContainer>
      {actions?.map(({ icon, iconProps, onPress, disabled }, index) => (
        <ActionButton
          disabled={disabled}
          key={index}
          onPress={onPress}
        >
          <Badge active={iconProps?.withDot}>
            {renderHeaderIcon(icon)}
          </Badge>
        </ActionButton>
      ))}
    </ActionsContainer>
  )
}
