import { Text, TouchableOpacity, View } from 'react-native'
import { ElasticText } from '../../../components/elastic-text/elastic-text'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const Container = styled(TouchableOpacity, {
  alignItems: 'center',
  flexDirection: 'row'
})

export const AvatarContainer = styled(View, {
  width: sp(48),
  borderRadius: sp(24),
  overflow: 'hidden'
})

export const ContentContainer = styled(View, {
  marginLeft: sp(12),
  marginRight: sp(20),
  flex: 1
})

export const Name = styled(ElasticText, {
  ...font({ type: 'h2' }),
  color: color('text')
})

export const Specialization = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext')
})
