import { useNavigation } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React, { useLayoutEffect } from 'react'
import { ChatHeaderProps, ChatHeaderTitleView } from './chat-header-title.view'
import { Header } from './header'
import { HeaderActionProps } from './header-actions.view'

export interface ChatHeaderOptions {
  chatData: ChatHeaderProps
  actions?: HeaderActionProps[]
}

export function useChatHeader (
  { chatData, actions }: ChatHeaderOptions
): void {
  const navigation = useNavigation<StackNavigationProp<any>>()
  useLayoutEffect(() => {
    navigation.setOptions({
      header: (props) => (
        <Header
          headerProps={props}
          renderTitle={() => <ChatHeaderTitleView {...chatData} />}
          actions={actions}
        />
      )
    })
  }, [navigation, chatData, actions])
}
