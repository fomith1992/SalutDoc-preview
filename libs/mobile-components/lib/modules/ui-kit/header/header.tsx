import { StackActions } from '@react-navigation/native'
import { StackHeaderProps } from '@react-navigation/stack'
import React, { useMemo } from 'react'
import { HeaderActionProps } from './header-actions.view'
import { HeaderView } from './header.view'

interface HeaderProps {
  headerProps: StackHeaderProps
  actions?: HeaderActionProps[]
  hasShadow?: boolean
  forceShowBack?: boolean

  renderTitle?: (title: string) => React.ReactNode
}

export function Header (
  {
    headerProps: {
      previous,
      navigation,
      scene,
      styleInterpolator,
      layout,
      insets
    },
    actions,
    hasShadow,
    forceShowBack = false,
    renderTitle
  }: HeaderProps
): React.ReactElement {
  const { options } = scene.descriptor
  const title = typeof options.headerTitle === 'string' ? options.headerTitle : (options.title ?? scene.route.name)

  const goBack = React.useCallback(
    () => {
      if (navigation.isFocused() && navigation.canGoBack()) {
        navigation.dispatch({
          ...StackActions.pop(),
          source: scene.route.key
        })
      }
    },
    [navigation, scene.route.key]
  )

  const interpolatedStyles = useMemo(() => styleInterpolator({
    current: {
      progress: scene.progress.current
    },
    next: scene.progress.next == null ? undefined : {
      progress: scene.progress.next
    },
    layouts: {
      header: {
        height: 0, // unused for now
        width: layout.width
      },
      screen: {
        width: layout.width,
        height: layout.height
      }
    }
  }), [scene.progress.current, scene.progress.next, layout.width, layout.height])

  return (
    <HeaderView
      title={title}
      renderTitle={renderTitle}
      hasShadow={hasShadow}
      actions={actions}
      insets={insets}
      interpolatedStyles={interpolatedStyles}
      goBack={previous == null && !forceShowBack ? undefined : goBack}
    />
  )
}
