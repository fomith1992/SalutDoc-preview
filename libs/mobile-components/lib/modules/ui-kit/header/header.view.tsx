import { StackHeaderInterpolatedStyle } from '@react-navigation/stack'
import React from 'react'
import { View } from 'react-native'
import { EdgeInsets } from 'react-native-safe-area-context'
import { HeaderActionProps, HeaderActionsView } from './header-actions.view'
import { HeaderBackImage } from './header-back'
import { Container, GoBackContainer, HeaderFirstLine, HeaderTitleContainer } from './header.style'
import { TextHeaderTitleView } from './text-header-title.view'

interface HeaderViewProps {
  title: string
  renderTitle?: (title: string) => React.ReactNode
  actions?: HeaderActionProps[]
  hasShadow?: boolean

  interpolatedStyles: StackHeaderInterpolatedStyle
  insets: EdgeInsets

  goBack?: () => void
}

export function HeaderView (
  {
    title,
    renderTitle,
    actions,
    hasShadow = true,
    goBack,
    insets,
    interpolatedStyles: {
      backgroundStyle
    }
  }: HeaderViewProps
): React.ReactElement {
  return (
    <Container
      hasShadow={hasShadow}
      style={backgroundStyle}
    >
      <View style={{ height: insets.top }} />
      <HeaderFirstLine>
        {goBack == null ? null
          : (
            <GoBackContainer
              onPress={goBack}
            >
              <HeaderBackImage />
            </GoBackContainer>
          )}
        <HeaderTitleContainer
          indentLeft={goBack == null}
        >
          {renderTitle == null
            ? <TextHeaderTitleView title={title} />
            : renderTitle(title)}
        </HeaderTitleContainer>
        <HeaderActionsView actions={actions} />
      </HeaderFirstLine>
    </Container>
  )
}
