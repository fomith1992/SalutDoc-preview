import React from 'react'
import { Avatar } from '../../../components/avatar/avatar.view'
import { IconComponent } from '../../../images/icon-component'
import { AvatarContainer, Container, ContentContainer, Name, Specialization } from './chat-header-title.style'

export interface ChatHeaderProps {
  avatar?: string | null
  avatarPlaceholder?: IconComponent
  name: string
  specialization: string

  onPress?: () => void
}

export const ChatHeaderTitleView = (
  {
    avatar,
    avatarPlaceholder,
    name,
    specialization,
    onPress
  }: ChatHeaderProps
): React.ReactElement => {
  return (
    <Container onPress={onPress} disabled={onPress == null}>
      <AvatarContainer>
        <Avatar url={avatar} placeholder={avatarPlaceholder} />
      </AvatarContainer>
      <ContentContainer>
        <Name
          minimumFontScale={1}
        >
          {name}
        </Name>
        <Specialization>
          {specialization}
        </Specialization>
      </ContentContainer>
    </Container>
  )
}
