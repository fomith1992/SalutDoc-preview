import React from 'react'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { Modal } from '../index'
import { DropDown, DropDownItemProps } from '../../../components/drop-down/drop-down.view'
import {
  HeaderButton,
  Title,
  TitleDown,
  ModalContainer
} from './text-header-title.style'

export interface HeaderTitleViewProps {
  title: string
  dropdownItems?: DropDownItemProps[]
}

export function TextHeaderTitleView (
  {
    title,
    dropdownItems
  }: HeaderTitleViewProps
): React.ReactElement {
  const { state: modalVisible, setTrue: showModal, setFalse: hideModal } = useBooleanFlag(false)
  if (dropdownItems == null) {
    return <Title>{title}</Title>
  } else {
    return (
      <HeaderButton onPress={showModal}>
        <Title>{title}</Title>
        <TitleDown />
        <Modal visible={modalVisible}>
          <ModalContainer>
            <DropDown
              data={dropdownItems}
              onClose={hideModal}
            />
          </ModalContainer>
        </Modal>
      </HeaderButton>
    )
  }
}
