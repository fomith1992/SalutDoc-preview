import { Text, TouchableOpacity } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const SaveButtonText = styled(Text, (props: { enabled: boolean }) => ({
  ...font({ type: 'h3', weight: 'strong' }),
  color: color(props.enabled ? 'accent' : 'inactive')
}) as const)

export const SaveButton = styled(TouchableOpacity, {
  paddingVertical: sp(16)
})
