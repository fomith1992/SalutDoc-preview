import { Animated, Platform, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const Container = styled(Animated.View, (props: { hasShadow: boolean }) => ({
  backgroundColor: color('surface'),
  ...(!props.hasShadow ? {} : Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  }))
}))

export const HeaderFirstLine = styled(View, {
  flexDirection: 'row',
  alignItems: 'center',
  height: sp(56)
})

export const GoBackContainer = styled(TouchableOpacity, {
  padding: sp(16)
})

export const HeaderTitleContainer = styled(View, (props: { indentLeft: boolean }) => ({
  flex: 1,
  marginLeft: props.indentLeft ? sp(16) : 0
}) as const)
