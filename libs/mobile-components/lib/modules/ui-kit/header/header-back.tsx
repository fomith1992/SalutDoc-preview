import { ArrowIcon16 } from '../../../images/arrow.icon-16'
import { styled } from '../../../style/styled'
import { Text } from 'react-native'
import { font } from '../../../style/text'
import { sp } from '../../../style/size'
import { color } from '../../../style/color'

export const HeaderBackImage = styled(ArrowIcon16, {
  width: sp(16),
  height: sp(16),
  color: color('accent')
})

export const HeaderLeftButton = styled(Text, {
  ...font({ type: 'h3', weight: 'light' }),
  color: color('accent')
})
