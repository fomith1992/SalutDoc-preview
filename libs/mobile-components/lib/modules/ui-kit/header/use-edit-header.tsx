import { useNavigation } from '@react-navigation/native'
import { StackNavigationOptions, StackNavigationProp } from '@react-navigation/stack'
import React, { useLayoutEffect } from 'react'
import { HeaderLeftButton } from './header-back'
import { HeaderSaveButton, HeaderSaveButtonProps } from './header-save-button'

export const editHeaderBaseline: Readonly<StackNavigationOptions> = {
  headerTitle: () => null,
  headerLeftContainerStyle: { marginLeft: 10 },
  headerRightContainerStyle: { marginRight: 20 },
  headerBackImage: () => <HeaderLeftButton>Отмена</HeaderLeftButton>,
  headerBackTitleVisible: false,
  header: undefined
}

export interface EditHeaderOptions {
  cancelText?: string
  saveButton?: HeaderSaveButtonProps | null
}

export function useEditHeader (
  {
    cancelText,
    saveButton
  }: EditHeaderOptions,
  reset = true
): void {
  const navigation = useNavigation<StackNavigationProp<any>>()
  useLayoutEffect(() => {
    const options: StackNavigationOptions = reset ? { ...editHeaderBaseline } : {}
    if (cancelText !== undefined) {
      options.headerBackImage = () => <HeaderLeftButton>{cancelText}</HeaderLeftButton>
    }
    if (saveButton !== undefined) {
      if (saveButton === null) {
        options.headerRight = () => null
      } else {
        const { text, enabled, onPress } = saveButton
        options.headerRight = () => <HeaderSaveButton text={text} enabled={enabled} onPress={onPress} />
      }
    }
    navigation.setOptions(options)
  }, [navigation, cancelText, saveButton, reset])
}
