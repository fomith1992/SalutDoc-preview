import React from 'react'
import { SaveButtonText, SaveButton } from './header-save-button.style'

export interface HeaderSaveButtonProps {
  text: string
  enabled: boolean
  onPress: () => void
}

export function HeaderSaveButton ({ text, enabled, onPress }: HeaderSaveButtonProps): React.ReactElement {
  return (
    <SaveButton onPress={onPress} disabled={!enabled}>
      <SaveButtonText enabled={enabled}>
        {text}
      </SaveButtonText>
    </SaveButton>
  )
}
