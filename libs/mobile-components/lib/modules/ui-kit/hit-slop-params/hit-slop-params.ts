import { Insets } from 'react-native'

type hitSlopParamsType = 'S' | 'M' | 'L' | 'XL' | number

export const hitSlopParams = (indent: hitSlopParamsType): Insets => {
  switch (indent) {
    case 'S':
      return { top: 12, bottom: 12, left: 0, right: 0 }
    case 'M':
      return { top: 8, bottom: 8, left: 0, right: 0 }
    case 'L':
      return { top: 4, bottom: 4, left: 0, right: 0 }
    case 'XL':
      return { top: 0, bottom: 0, left: 0, right: 0 }
    default:
      return { bottom: indent, left: indent, right: indent, top: indent }
  }
}
