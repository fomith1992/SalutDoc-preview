import React, { useCallback } from 'react'
import { Alert, Platform, StyleProp, Text, TextStyle } from 'react-native'
import InAppBrowser from 'react-native-inappbrowser-reborn'

export interface ExternalLinkTextProps {
  href: string
  dismissButtonStyle?: 'done' | 'close' | 'cancel'
  style?: StyleProp<TextStyle>

  children: React.ReactNode
}

export function ExternalLinkText (
  {
    href,
    dismissButtonStyle,
    style,
    children
  }: ExternalLinkTextProps
): React.ReactElement {
  const handlePress = Platform.OS === 'web' ? undefined : useCallback((): void => {
    InAppBrowser.open(href, { dismissButtonStyle }).catch(error => {
      console.log(error)
      Alert.alert('Не удалось открыть ссылку')
    })
  }, [href, dismissButtonStyle])

  return (
    <Text
      style={style}
      accessibilityRole='link'
      target='_blank'
      href={href}
      onPress={handlePress}
    >
      {children}
    </Text>
  )
}
