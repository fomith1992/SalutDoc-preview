import React, { useCallback } from 'react'
import { Alert, GestureResponderEvent } from 'react-native'
import InAppBrowser from 'react-native-inappbrowser-reborn'

export interface ExternalLinkProps {
  href: string
  dismissButtonStyle?: 'done' | 'close' | 'cancel'

  children: (props: ExternalLinkRenderProps) => React.ReactElement
}

export interface ExternalLinkRenderProps {
  onPress?: (event: GestureResponderEvent) => void
}

export function ExternalLink (
  {
    href,
    dismissButtonStyle,
    children
  }: ExternalLinkProps
): React.ReactElement {
  const handlePress = useCallback((): void => {
    InAppBrowser.open(href, { dismissButtonStyle }).catch(error => {
      console.log(error)
      Alert.alert('Не удалось открыть ссылку')
    })
  }, [href, dismissButtonStyle])

  return children({
    onPress: handlePress
  })
}
