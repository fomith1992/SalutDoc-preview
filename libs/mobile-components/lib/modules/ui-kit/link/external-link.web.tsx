import React from 'react'
import { ExternalLinkProps } from './external-link'

export function ExternalLink (
  {
    href,
    children
  }: ExternalLinkProps
): React.ReactElement {
  return (
    <a
      href={href}
      target='_blank'
      rel='noreferrer'
      style={{
        textDecoration: 'none'
      }}
    >
      {children({})}
    </a>
  )
}
