import { View } from 'react-native'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const ScreenBlock = styled(View, {
  marginBottom: sp(8)
})
