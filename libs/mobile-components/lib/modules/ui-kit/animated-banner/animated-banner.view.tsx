import React, { useRef, useState } from 'react'
import {
  ScrollView,
  View,
  useWindowDimensions
} from 'react-native'
import { IconComponent } from '../../../images/icon-component'
import { TextButton } from '../buttons'
import {
  IconContainer,
  Title,
  Description,
  ButtonContainer,
  Container,
  DescriptionStrong
} from './animated-banner.style'

export interface AnimatedBannerData {
  Icon: IconComponent
  title: string
  description?: string
  descriptionStrong?: string
}

interface AnimatedBannerProps {
  data: AnimatedBannerData[]
  onEndAnimated: () => void
}

export function AnimatedBanner ({ data, onEndAnimated }: AnimatedBannerProps): React.ReactElement {
  const scrollX = useRef<ScrollView>(null)
  const [page, setPage] = useState(0)

  const { width: windowWidth } = useWindowDimensions()

  const moveBody = (index: number): void => {
    scrollX.current?.scrollTo({
      x: index * windowWidth,
      animated: true
    })
    setPage(index)
  }

  return (
    <Container>
      <ScrollView
        horizontal
        pagingEnabled
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        ref={scrollX}
        scrollEventThrottle={1}
      >
        {data.map((item, idx) => {
          const { Icon, description, descriptionStrong, title } = item
          return (
            <View
              style={{ flex: 1, width: windowWidth }}
              key={idx}
            >
              <IconContainer>
                <Icon />
                <Title>
                  {title}
                </Title>
                <Description>
                  {description}
                  <DescriptionStrong>
                    {descriptionStrong}
                  </DescriptionStrong>
                </Description>
              </IconContainer>
            </View>
          )
        })}
      </ScrollView>
      <ButtonContainer>
        <TextButton
          size='XL'
          onPress={() => {
            if (data.length - 1 === page) {
              onEndAnimated()
            } else {
              moveBody(page + 1)
            }
          }}
        >
          Далее
        </TextButton>
      </ButtonContainer>
    </Container>
  )
}
