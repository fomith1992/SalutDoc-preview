import { Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'
import { container } from '../../../style/view'

export const Container = styled(View, {
  flex: 1
})

export const IconContainer = styled(View, {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
})

export const ButtonContainer = styled(View, {
  ...container(),
  marginBottom: sp(24)
})

export const Title = styled(Text, {
  ...font({ type: 'h1' }),
  fontSize: sp(20),
  color: color('text'),
  marginTop: sp(40)
})

export const Description = styled(Text, {
  ...font({ type: 'text1' }),
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(12)
})

export const DescriptionStrong = styled(Text, {
  ...font({ type: 'text1' }),
  fontWeight: '700',
  color: color('text'),
  textAlign: 'center',
  marginTop: sp(12)
})
