import React, { useCallback, useMemo, useState } from 'react'
import { LayoutChangeEvent, View } from 'react-native'
import { LinearGradient, Rect, Stop } from 'react-native-svg'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { CollapsibleContainer, GradientSvg } from './collapsible.style'

export interface CollapsibleProps {
  collapsedInitially?: boolean
  renderFooter?: (props: RenderFooterProps) => React.ReactNode

  threshold: number
  collapsedHeight: number

  children: React.ReactNode
}

export interface RenderFooterProps {
  collapsed: boolean
  collapse: () => void
  expand: () => void
  toggle: () => void
}

export function Collapsible (
  {
    collapsedInitially = true,
    renderFooter,
    threshold,
    collapsedHeight,
    children
  }: CollapsibleProps
): React.ReactElement {
  const [contentHeight, setContentHeight] = useState<number>()
  const { state: collapsed, setTrue: collapse, setFalse: expand, toggle } = useBooleanFlag(collapsedInitially)
  const active = contentHeight !== undefined && contentHeight >= threshold
  const handleContentLayout = useCallback((event: LayoutChangeEvent) => {
    const { height } = event.nativeEvent.layout
    setContentHeight(height)
  }, [setContentHeight])
  const footer = useMemo(() => !active ? null : renderFooter?.({
    collapsed,
    collapse,
    expand,
    toggle
  }), [renderFooter, active, collapsed, collapse, expand, toggle])

  return (
    <>
      <CollapsibleContainer
        maxHeight={!active ? threshold : collapsed ? collapsedHeight : contentHeight}
      >
        <View onLayout={handleContentLayout}>
          {children}
        </View>
        {!active || !collapsed ? null : (
          <GradientSvg preserveAspectRatio='none' viewBox='0 0 1 40'>
            <LinearGradient id='grad' x1='0' y1='0' x2='0' y2='1'>
              <Stop offset='0' stopColor='#FFF' stopOpacity='0' />
              <Stop offset='1' stopColor='#FFF' stopOpacity='1' />
            </LinearGradient>
            <Rect width='1' height='40' fill='url(#grad)' />
          </GradientSvg>
        )}
      </CollapsibleContainer>
      {footer}
    </>
  )
}
