import { Platform, View } from 'react-native'
import Svg from 'react-native-svg'
import { styled } from '../../../style/styled'

export interface CollapsibleContainerProps {
  maxHeight?: number | undefined
}

export const CollapsibleContainer = styled(View, ({ maxHeight }: CollapsibleContainerProps) => ({
  maxHeight,
  overflow: Platform.OS === 'ios' ? 'scroll' : 'hidden'
}) as const)

export const GradientSvg = styled(Svg, {
  position: 'absolute',
  bottom: 0,
  left: 0,
  right: 0
})
