import React from 'react'
import {
  StarEmptyImg,
  StarHalfFillImg,
  StarFillImg,
  StarMargin,
  RatingContainer
} from './rating-stars-row.style'

interface RatingStarsRowProps {
  rating: number
}

export function RatingStarsRow ({ rating }: RatingStarsRowProps): React.ReactElement {
  const data = [
    ...Array.from(Array(5).keys(), x => {
      if (rating >= x + 1) {
        return <StarMargin key={x}><StarFillImg /></StarMargin>
      } else if (rating < x + 1 && rating > x - 1 + 1) {
        return <StarMargin key={x}><StarHalfFillImg /></StarMargin>
      } else return <StarMargin key={x}><StarEmptyImg /></StarMargin>
    })]
  return (
    <RatingContainer>
      {data}
    </RatingContainer>
  )
}
