import { View } from 'react-native'
import { StarFilledIcon24 } from '../../../images/star-filled.icon-24'
import { StarHalfFilledIcon12 } from '../../../images/star-half-filled.icon-12'
import { StarIcon12 } from '../../../images/star.icon-12'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const StarEmptyImg = styled(StarIcon12, {
  width: sp(12),
  height: sp(12),
  color: color('accent')
})

export const StarHalfFillImg = styled(StarHalfFilledIcon12, {
  width: sp(12),
  height: sp(12),
  color: color('accent')
})

export const StarFillImg = styled(StarFilledIcon24, {
  width: sp(12),
  height: sp(12),
  color: color('accent')
})

export const StarMargin = styled(View, {
  marginRight: sp(4)
})

export const RatingContainer = styled(View, {
  flexDirection: 'row',
  alignItems: 'center'
})
