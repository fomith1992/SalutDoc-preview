import React from 'react'
import type { ImagePickerProps } from './image-picker'

export function ImagePicker (
  {
    children
  }: ImagePickerProps
): React.ReactElement {
  return children({
    image: null,
    pickImage: async () => await Promise.resolve(null)
  })
}
