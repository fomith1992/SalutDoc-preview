import React, { useCallback, useState } from 'react'
import { Alert, Platform } from 'react-native'
import RNImagePicker from 'react-native-image-picker'

export interface ImagePickerProps {
  title?: string

  maxResolution?: {
    width: number
    height: number
  }
  maxBytes?: number
  quality?: number

  onImagePicked?: (image: Image) => void

  children: (props: ImagePickerRenderProps) => React.ReactElement
}

export interface ImagePickerRenderProps {
  image: Image | null
  pickImage: () => Promise<Image | null>
}

export interface Image {
  uri: string
  fileName: string | undefined
  width: number
  height: number
  bytes: number
  mimeType: string
}

export function ImagePicker (
  {
    title,
    maxResolution = { width: 2000, height: 2000 },
    maxBytes = 2 * 1024 * 1024,
    quality = 0.8,
    onImagePicked,
    children
  }: ImagePickerProps
): React.ReactElement {
  const [image, setImage] = useState<Image | null>(null)
  const pickImage = useCallback(async () => {
    const { width: maxWidth, height: maxHeight } = maxResolution
    return await new Promise<Image | null>((resolve, reject) => RNImagePicker.showImagePicker({
      cancelButtonTitle: 'Отмена',
      takePhotoButtonTitle: 'Сделать фото...',
      chooseFromLibraryButtonTitle: 'Выбрать из галереи...',
      title: title,
      mediaType: 'photo',
      maxWidth: maxWidth,
      maxHeight: maxHeight,
      quality: quality,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    }, (response) => {
      try {
        if (response.didCancel) {
          console.log('User cancelled image picker')
          return resolve(null)
        }
        if (response.error !== undefined) {
          console.log('ImagePicker Error: ', response.error)
          Alert.alert('Ошибка', 'Не удалось прочитать изображение')
          return resolve(null)
        }
        if (response.width > maxWidth || response.height > maxHeight) {
          Alert.alert('Ошибка', `Размер изображения не должен превышать ${maxWidth}x${maxHeight}px.`)
          return resolve(null)
        }
        if (response.fileSize > maxBytes) {
          Alert.alert('Ошибка', `Вес изображения не должен превышать ${maxBytes} байт.`)
          return resolve(null)
        }
        const image: Image = {
          uri: Platform.OS === 'ios' ? `data:image/jpeg;base64,${response.data}` : response.uri,
          fileName: response.fileName,
          bytes: response.fileSize,
          width: response.width,
          height: response.height,
          mimeType: 'image/jpeg'
        }
        setImage(image)
        try {
          onImagePicked?.(image)
        } catch (e) {
          console.log('onImagePicked callback threw an error', e)
        }
        return resolve(image)
      } catch (e) {
        return reject(e)
      }
    }))
  }, [title, maxResolution.width, maxResolution.height, maxBytes, quality, onImagePicked])
  return children({
    image,
    pickImage
  })
}
