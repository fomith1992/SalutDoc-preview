import { Alert, Platform } from 'react-native'
import RNImagePicker, { ImagePickerOptions, ImagePickerResponse } from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import { Image } from './image-picker'

export interface FilePickerProps {
  type?: 'camera' | 'imageLibrary' | 'document'
  maxResolution?: {
    width: number
    height: number
  }
  maxBytes?: number
  quality?: number

  onImagePicked?: (image: Image) => void
  onDocumentPicked?: (document: Document) => void
}

export interface Document {
  uri: string
  type: string
  fileName: string | undefined
  size: number
}

export async function filePickerFn (
  {
    type = 'camera',
    maxResolution = { width: 2000, height: 2000 },
    maxBytes = 2 * 1024 * 1024,
    quality = 0.8,
    onImagePicked,
    onDocumentPicked
  }: FilePickerProps
): Promise<void> {
  const { width: maxWidth, height: maxHeight } = maxResolution
  const initialValues: ImagePickerOptions = {
    mediaType: 'photo',
    maxWidth: maxWidth,
    maxHeight: maxHeight,
    quality: quality,
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  }

  const imagePickerParser = (response: ImagePickerResponse): void => {
    try {
      if (response.didCancel) {
        console.log('User cancelled image picker')
      }
      if (response.error !== undefined) {
        console.log('ImagePicker Error: ', response.error)
        Alert.alert('Ошибка', 'Не удалось прочитать изображение')
      }
      if (response.width > maxWidth || response.height > maxHeight) {
        Alert.alert('Ошибка', `Размер изображения не должен превышать ${maxWidth}x${maxHeight}px.`)
      }
      if (response.fileSize > maxBytes) {
        Alert.alert('Ошибка', `Вес изображения не должен превышать ${maxBytes} байт.`)
      }
      const image: Image = {
        uri: Platform.OS === 'ios' ? `data:image/jpeg;base64,${response.data}` : response.uri,
        fileName: response.fileName,
        bytes: response.fileSize,
        width: response.width,
        height: response.height,
        mimeType: 'image/jpeg'
      }
      try {
        onImagePicked?.(image)
      } catch (e) {
        console.log('onImagePicked callback threw an error', e)
      }
    } catch (e) {
      return Alert.alert('Произошла ошибка')
    }
  }

  switch (type) {
    case 'camera':
      return RNImagePicker.launchCamera(initialValues, imagePickerParser)
    case 'imageLibrary':
      return RNImagePicker.launchImageLibrary(initialValues, imagePickerParser)
    case 'document':
      try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles]
        })
        onDocumentPicked?.({
          uri: res.uri,
          type: res.type,
          fileName: res.name,
          size: res.size
        })
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err
        }
      }
  }
}
