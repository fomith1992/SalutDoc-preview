import * as React from 'react'
import type { PortalMethods } from './portal-host'

type Props = {
  manager: PortalMethods | null
  children: React.ReactNode
}

export default class PortalConsumer extends React.Component<Props> {
  private key!: number

  async componentDidMount (): Promise<void> {
    const manager = this.checkManager()

    // Delay updating to prevent React from going to infinite loop
    await Promise.resolve()

    this.key = manager.mount(this.props.children)
  }

  componentDidUpdate (): void {
    const manager = this.checkManager()

    manager.update(this.key, this.props.children)
  }

  componentWillUnmount (): void {
    const manager = this.checkManager()

    manager.unmount(this.key)
  }

  private checkManager (): PortalMethods {
    if (this.props.manager == null) {
      throw new Error(
        'Looks like you forgot to wrap your root component with `PortalHost` component.'
      )
    }
    return this.props.manager
  }

  render (): React.ReactNode {
    return null
  }
}
