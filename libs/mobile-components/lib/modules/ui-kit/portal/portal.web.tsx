import React from 'react'
import ReactDOM from 'react-dom'
import css from './portal.module.scss'
import type { PortalProps } from './portal'

export default function Portal (
  {
    children
  }: PortalProps
): React.ReactElement {
  return ReactDOM.createPortal(
    <div className={css.portal}>
      {children}
    </div>,
    document.body
  )
}
