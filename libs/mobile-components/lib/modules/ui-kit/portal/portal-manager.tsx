import * as React from 'react'
import { StyleSheet, View } from 'react-native'

type State = {
  portals: Array<{
    key: number
    children: React.ReactNode
  }>
}

/**
 * Portal host is the component which actually renders all Portals.
 */
export default class PortalManager extends React.PureComponent<{}, State> {
  state: State = {
    portals: []
  }

  mount = (key: number, children: React.ReactNode): void => {
    this.setState((state) => ({
      portals: [...state.portals, { key, children }]
    }))
  }

  update = (key: number, children: React.ReactNode): void =>
    this.setState((state) => ({
      portals: state.portals.map((item) => {
        if (item.key === key) {
          return { ...item, children }
        }
        return item
      })
    }))

  unmount = (key: number): void =>
    this.setState((state) => ({
      portals: state.portals.filter((item) => item.key !== key)
    }))

  render (): React.ReactNode {
    return this.state.portals.map(({ key, children }) => (
      /* Need collapsable=false here to clip the elevations, otherwise they appear above sibling components */
      <View
        key={key}
        collapsable={false}
        pointerEvents='box-none'
        style={StyleSheet.absoluteFill}
      >
        {children}
      </View>
    ))
  }
}
