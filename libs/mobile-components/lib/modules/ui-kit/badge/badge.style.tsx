import { View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const BadgeContainer = View

export const BadgeImg = styled(View, (props: { active: boolean }) => ({
  position: 'absolute',
  top: sp(-4),
  right: sp(-4),
  width: sp(8),
  height: sp(8),
  borderRadius: sp(4),
  backgroundColor: props.active ? color('error') : 'tranparent'
}) as const)
