import React from 'react'
import { BadgeContainer, BadgeImg } from './badge.style'

interface BadgeProps {
  active?: boolean
  children: React.ReactElement
}

export function Badge ({ active = false, children }: BadgeProps): React.ReactElement {
  return (
    <BadgeContainer>
      {children}
      <BadgeImg active={active} />
    </BadgeContainer>
  )
}
