import React from 'react'
import { GestureResponderEvent } from 'react-native'
import { hitSlopParams } from '../hit-slop-params'
import { Button, Icon, Title } from './text-button.style'

export interface TextButtonProps {
  children?: React.ReactNode
  onPress?: (event: GestureResponderEvent) => void
  type?: 'primary' | 'secondary' | 'link' | 'cancel-button'
  icon?: React.ReactNode
  size?: 'S' | 'M' | 'L' | 'XL'
  disabled?: boolean
}

export function TextButton (
  {
    children,
    onPress,
    type = 'primary',
    icon,
    size = 'S',
    disabled = false
  }: TextButtonProps
): React.ReactElement {
  return (
    <Button
      hitSlop={hitSlopParams(size)}
      type={type}
      size={size}
      onPress={onPress}
      disabled={disabled}
    >
      {icon !== undefined ? <Icon>{icon}</Icon> : null}
      <Title size={size} type={type} disabled={disabled}>{children}</Title>
    </Button>
  )
}
