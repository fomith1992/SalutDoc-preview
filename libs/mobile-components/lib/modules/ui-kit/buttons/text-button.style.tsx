import { Text, TouchableOpacity, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled, variants } from '../../../style/styled'
import { font } from '../../../style/text'

const ButtonSizeVariants = styled(TouchableOpacity, variants('size', {
  S: {
    paddingVertical: sp(4),
    paddingHorizontal: sp(16)
  },
  M: {
    paddingVertical: sp(8),
    paddingHorizontal: sp(16)
  },
  L: {
    paddingVertical: sp(12),
    paddingHorizontal: sp(20)
  },
  XL: {
    paddingVertical: sp(16),
    paddingHorizontal: sp(20)
  }
}))

const ButtonTypeVariants = styled(ButtonSizeVariants, variants('type', (props: { disabled: boolean }) => ({
  primary: {
    backgroundColor: color(props.disabled ? 'inactive' : 'accent'),
    borderRadius: sp(4)
  },
  secondary: {
    backgroundColor: color('surface'),
    borderColor: color(props.disabled ? 'inactive' : 'accent'),
    borderRadius: sp(4),
    borderWidth: 1
  },
  link: {
    paddingHorizontal: 0,
    backgroundColor: 'transparent'
  },
  'cancel-button': {}
})))

export const Button = styled(ButtonTypeVariants, {
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center'
})

const TitleSizeVariants = styled(Text, variants('size', {
  S: font({ type: 'h4', weight: 'strong' }),
  M: font({ type: 'h4', weight: 'strong' }),
  L: font({ type: 'h3', weight: 'strong' }),
  XL: font({ type: 'h3', weight: 'strong' })
}))

const TitleTypeVariants = styled(TitleSizeVariants, variants('type', (props: { disabled: boolean }) => ({
  primary: {
    color: color('surface')
  },
  secondary: {
    color: color(props.disabled ? 'inactive' : 'accent')
  },
  link: {
    color: color(props.disabled ? 'inactive' : 'accent')
  },
  'cancel-button': {
    color: color('error')
  }
})))

export const Title = styled(TitleTypeVariants, {
  textAlign: 'center'
})

export const Icon = styled(View, {
  marginRight: sp(12)
})
