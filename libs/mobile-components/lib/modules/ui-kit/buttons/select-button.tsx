import React from 'react'
import {
  SelectedInfoContainer,
  ActivePlusIcon,
  ActiveText,
  InfoContainer,
  InfoPlusIcon,
  DefaultText
} from './select-button.style'

export interface SelectButtonProps {
  defaultText: string
  value: string | undefined
  resetValue: () => void
  onPress: () => void
}

export function SelectButton ({ defaultText, value, resetValue, onPress }: SelectButtonProps): React.ReactElement {
  return (
    value !== undefined
      ? (
        <SelectedInfoContainer onPress={() => resetValue()}>
          <ActivePlusIcon />
          <ActiveText>
            {value}
          </ActiveText>
        </SelectedInfoContainer>
      ) : (
        <InfoContainer onPress={() => onPress()}>
          <InfoPlusIcon />
          <DefaultText>
            {defaultText}
          </DefaultText>
        </InfoContainer>
      )
  )
}
