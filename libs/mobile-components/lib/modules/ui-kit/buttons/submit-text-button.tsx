import React from 'react'
import { TextButton, TextButtonProps } from './text-button.view'

export interface SubmitTextButtonProps extends Omit<TextButtonProps, 'onPress' | 'disabled'> {
  onSubmit?: () => void
  valid: boolean
}

export function SubmitTextButton ({
  valid,
  onSubmit,
  size,
  ...rest
}: SubmitTextButtonProps): React.ReactElement {
  return (
    <TextButton
      {...rest}
      onPress={onSubmit}
      disabled={!valid}
      size={size}
    />
  )
}
