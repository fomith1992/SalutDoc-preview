import { styled } from '../../../style/styled'
import { TouchableOpacity, Text } from 'react-native'
import { PlusIcon16 } from '../../../images/plus.icon-16'
import { sp } from '../../../style/size'
import { color } from '../../../style/color'
import { font } from '../../../style/text'
import { CrossIcon16 } from '../../../images/cross.icon-16'

export const InfoContainer = styled(TouchableOpacity, {
  marginRight: 'auto',
  padding: sp(12),
  flexDirection: 'row',
  alignItems: 'flex-start',
  borderRadius: sp(32),
  backgroundColor: color('surface')
})

export const SelectedInfoContainer = styled(TouchableOpacity, {
  marginRight: 'auto',
  padding: sp(12),
  flexDirection: 'row-reverse',
  alignItems: 'flex-start',
  borderRadius: sp(32),
  backgroundColor: color('surface')
})

export const InfoPlusIcon = styled(PlusIcon16, {
  color: color('subtext')
})

export const ActivePlusIcon = styled(CrossIcon16, {
  color: color('accent')
})

export const DefaultText = styled(Text, {
  marginLeft: sp(12),
  ...font({ type: 'h3', weight: 'light' }),
  color: color('subtext')
})

export const ActiveText = styled(Text, {
  marginRight: sp(4),
  ...font({ type: 'h3', weight: 'light' }),
  color: color('accent')
})
