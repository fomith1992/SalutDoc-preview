import { View } from 'react-native'
import { color } from '../../../style/color'
import { sp, SpScale } from '../../../style/size'
import { styled } from '../../../style/styled'

export const IconContainer = styled(View, (props: {width: SpScale, filled: boolean}) => ({
  width: sp(props.width),
  height: sp(props.width),
  borderRadius: sp(props.width / 2 as SpScale),
  overflow: 'hidden',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: props.filled ? color('secondaryLight') : undefined
}) as const)
