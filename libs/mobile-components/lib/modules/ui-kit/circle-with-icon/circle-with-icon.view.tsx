import React from 'react'
import { SpScale } from '../../../style/size'
import { IconContainer } from './circle-with-icon.style'

interface CircleWithIconProps {
  children: React.ReactNode
  width?: SpScale
  filled?: boolean
}

export function CircleWithIcon ({
  children,
  width = 104,
  filled = false
}: CircleWithIconProps): React.ReactElement {
  return (
    <IconContainer
      width={width}
      filled={filled}
    >
      {children}
    </IconContainer>
  )
}
