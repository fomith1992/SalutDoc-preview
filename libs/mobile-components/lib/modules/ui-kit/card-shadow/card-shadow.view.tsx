import React from 'react'
import { ScreenBlock } from '../index'
import { CardShadowContainer } from './card-shadow.style'

export interface CardShadowProps {
  children?: React.ReactNode
}

export function CardShadow ({ children }: CardShadowProps): React.ReactElement {
  return (
    <ScreenBlock>
      <CardShadowContainer>
        {children}
      </CardShadowContainer>
    </ScreenBlock>
  )
}
