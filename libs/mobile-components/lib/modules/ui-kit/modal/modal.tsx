import React, { useEffect, useRef, useState } from 'react'
import { Animated, Keyboard, useWindowDimensions } from 'react-native'
import { Portal } from '../portal'

export interface ModalProps {
  visible?: boolean
  animation?: 'fade' | 'slide' | 'none'
  children?: React.ReactNode
}

const springAnimationSpec = {
  stiffness: 1000,
  damping: 500,
  mass: 3,
  overshootClamping: true,
  restDisplacementThreshold: 0.1,
  restSpeedThreshold: 0.1,
  useNativeDriver: true
}

export function Modal (
  {
    visible = true,
    animation = 'none',
    children
  }: ModalProps
): React.ReactElement {
  const [active, setActive] = useState<boolean>(false)
  const { height } = useWindowDimensions()
  const animatedValue = useRef(new Animated.Value(0)).current
  const enterAnimation = useRef(Animated.spring(animatedValue, {
    ...springAnimationSpec,
    toValue: 1
  })).current
  const leaveAnimation = useRef(Animated.spring(animatedValue, {
    ...springAnimationSpec,
    toValue: 0
  })).current

  useEffect(() => {
    if (visible) {
      setActive(true)
      enterAnimation.start()
      Keyboard.dismiss()
    } else {
      leaveAnimation.start(({ finished }) => {
        if (finished) {
          setActive(false)
        }
      })
    }
  }, [visible])

  const opacity = animation === 'fade' ? animatedValue : undefined
  const transform = animation === 'slide' ? [{
    translateY: animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [height, 0]
    })
  }] : undefined
  const rendered = animation === 'none' ? visible : active

  return rendered ? (
    <Portal>
      <Animated.View
        style={{
          flex: 1,
          opacity,
          transform
        }}
      >
        {children}
      </Animated.View>
    </Portal>
  ) : <></>
}
