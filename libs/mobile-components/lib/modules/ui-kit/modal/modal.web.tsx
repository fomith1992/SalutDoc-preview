import classNames from 'classnames'
import React from 'react'
import ReactDOM from 'react-dom'
import type { ModalProps } from './modal'
import css from './modal.module.scss'

export function Modal (
  {
    visible = true,
    animation,
    children
  }: ModalProps
): React.ReactElement {
  if (visible) {
    return ReactDOM.createPortal(
      <div
        className={classNames(css.modalContainer, {
          [css.fade]: animation === 'fade',
          [css.slide]: animation === 'slide'
        })}
      >
        {children}
      </div>,
      document.body
    )
  } else {
    return <></>
  }
}
