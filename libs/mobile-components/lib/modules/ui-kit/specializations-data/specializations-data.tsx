export const specializationsData = [
  {
    title: 'А',
    data: [
      {
        value: 'Акушерство',
        specializationRu: 'Акушер',
        specializationEn: 'Obstetrician',
        patientConsultation: 'К акушеру'
      },
      {
        value: 'Аллергология',
        specializationRu: 'Аллерголог',
        specializationEn: 'Allergist',
        patientConsultation: 'К аллергологу'
      },
      {
        value: 'Ангиохирургия',
        specializationRu: 'Ангиохирург',
        specializationEn: 'Angiosurgeon',
        patientConsultation: 'К ангиохирургу'
      },
      {
        value: 'Андрология',
        specializationRu: 'Андролог',
        specializationEn: 'Andrologist',
        patientConsultation: 'К андрологу'
      },
      {
        value: 'Анестезиология',
        specializationRu: 'Анестезиолог',
        specializationEn: 'Anesthesiologist',
        patientConsultation: 'К анестезиологу'
      },
      {
        value: 'Аудиология',
        specializationRu: 'Аудиолог',
        specializationEn: 'Audiologist',
        patientConsultation: 'К аудиологу'
      }
    ]
  },
  {
    title: 'В',
    data: [
      {
        value: 'Венерология',
        specializationRu: 'Венеролог',
        specializationEn: 'Venereologist',
        patientConsultation: 'К венерологу'

      },
      {
        value: 'Вертебрология',
        specializationRu: 'Вертебролог',
        specializationEn: 'Vertebrologist',
        patientConsultation: 'К вертебрологу'
      }
    ]
  },
  {
    title: 'Г',
    data: [
      {
        value: 'Гастроэнтерология',
        specializationRu: 'Гастроэнтеролог',
        specializationEn: 'Gastroenterologist',
        patientConsultation: 'К гастроэнтерологу'
      },
      {
        value: 'Гематология',
        specializationRu: 'Гематолог',
        specializationEn: 'Hematologist',
        patientConsultation: 'К гематологу'
      },
      {
        value: 'Генетика',
        specializationRu: 'Генетик',
        specializationEn: 'Geneticist',
        patientConsultation: 'К генетику'
      },
      {
        value: 'Гепатология',
        specializationRu: 'Гепатолог',
        specializationEn: 'Hepatologist',
        patientConsultation: 'К гепатологу'
      },
      {
        value: 'Гериатрия',
        specializationRu: 'Гериатр',
        specializationEn: 'Geriatrician',
        patientConsultation: 'К гериатру'
      },
      {
        value: 'Гигиена',
        specializationRu: 'Гигиенист',
        specializationEn: 'Hygienist',
        patientConsultation: 'К гигиенисту'
      },
      {
        value: 'Гинекология',
        specializationRu: 'Гинеколог',
        specializationEn: 'Gynecologist',
        patientConsultation: 'К гинекологу'
      },
      {
        value: 'Гирудотерапевтия',
        specializationRu: 'Гирудотерапевт',
        specializationEn: 'Hirudotherapist',
        patientConsultation: 'К гирудотерапевту'
      },
      {
        value: 'Гомеопатия',
        specializationRu: 'Гомеопат',
        specializationEn: 'Homeopath',
        patientConsultation: 'К гомеопату'
      }
    ]
  },
  {
    title: 'Д',
    data: [
      {
        value: 'Дерматология',
        specializationRu: 'Дерматолог',
        specializationEn: 'Dermatologist',
        patientConsultation: 'К дерматологу'
      },
      {
        value: 'Детская хирургия',
        specializationRu: 'Детский хирург',
        specializationEn: 'Pediatric surgeon',
        patientConsultation: 'К детскому хирургу'
      },
      {
        value: 'Диетология',
        specializationRu: 'Диетолог',
        specializationEn: 'Nutritionist',
        patientConsultation: 'К диетологу'
      },
      {
        value: 'Другая специальность',
        specializationRu: 'Другая специальность',
        specializationEn: 'Other',
        patientConsultation: 'Другое'
      }
    ]
  },
  {
    title: 'И',
    data: [
      {
        value: 'Иммунология',
        specializationRu: 'Иммунолог',
        specializationEn: 'Immunologist',
        patientConsultation: 'К иммунологу'
      },
      {
        value: 'Инфекционные болезни',
        specializationRu: 'Инфекционист',
        specializationEn: 'Infectiologist',
        patientConsultation: 'К инфекционисту'
      }
    ]
  },
  {
    title: 'К',
    data: [
      {
        value: 'Кардиология',
        specializationRu: 'Кардиолог',
        specializationEn: 'Cardiologist',
        patientConsultation: 'К кардиологу'
      },
      {
        value: 'Кинезитерапевтия',
        specializationRu: 'Кинезитерапевт',
        specializationEn: 'Kinesitherapist',
        patientConsultation: 'К кинезитерапевту'
      },
      {
        value: 'Клиническая фармакология',
        specializationRu: 'Клинический фармаколог',
        specializationEn: 'Сlinical pharmacologist',
        patientConsultation: 'К клиническому фармакологу'
      },
      {
        value: 'Колопроктология',
        specializationRu: 'Колопроктолог',
        specializationEn: 'Coloproctologist',
        patientConsultation: 'К колопроктологу'
      },
      {
        value: 'Косметология',
        specializationRu: 'Косметолог',
        specializationEn: 'Cosmetologist',
        patientConsultation: 'К косметологу'
      }
    ]
  },
  {
    title: 'Л',
    data: [
      {
        value: 'Логопедия',
        specializationRu: 'Логопед',
        specializationEn: 'Speech therapist',
        patientConsultation: 'К логопеду'
      },
      {
        value: 'Лабораторная диагностика',
        specializationRu: 'Лабораторная диагностика',
        specializationEn: 'Laboratory diagnostics',
        patientConsultation: 'На лабораторную диагностику'
      },
      {
        value: 'Лечебная физкультура',
        specializationRu: 'Лечебная физкультура',
        specializationEn: 'Therapeutic gymnastics',
        patientConsultation: 'На лечебную физкультуру'
      },
      {
        value: 'ЛОР (Оториноларингология)',
        specializationRu: 'ЛОР (Оториноларинголог)',
        specializationEn: 'Otorhinolaryngologist',
        patientConsultation: 'К оториноларингологу'
      }
    ]
  },
  {
    title: 'М',
    data: [
      {
        value: 'Маммология',
        specializationRu: 'Маммолог',
        specializationEn: 'Mammologist',
        patientConsultation: 'К маммологу'
      },
      {
        value: 'Магнитно-резонансная томография',
        specializationRu: 'Магнитно-резонансная томография',
        specializationEn: 'Magnetic resonance imaging',
        patientConsultation: 'На магнитно-резонансную томографию'
      },
      {
        value: 'Мануальная терапевтия',
        specializationRu: 'Мануальный терапевт',
        specializationEn: 'Manual therapist',
        patientConsultation: 'К мануальному терапевту'
      },
      {
        value: 'Массаж',
        specializationRu: 'Массажист',
        specializationEn: 'Massage Therapist',
        patientConsultation: 'К массажисту'
      },
      {
        value: 'Микология',
        specializationRu: 'Миколог',
        specializationEn: 'Mycologist',
        patientConsultation: 'К микологу'
      }
    ]
  },
  {
    title: 'Н',
    data: [
      {
        value: 'Наркология',
        specializationRu: 'Нарколог',
        specializationEn: 'Narcologist',
        patientConsultation: 'К наркологу'
      },
      {
        value: 'Неврология',
        specializationRu: 'Невролог',
        specializationEn: 'Neurologist',
        patientConsultation: 'К неврологу'
      },
      {
        value: 'Нейрофизиология',
        specializationRu: 'Нейрофизиолог',
        specializationEn: 'Neurophysiologist',
        patientConsultation: 'К нейрофизиологу'
      },
      {
        value: 'Нейрохирургия',
        specializationRu: 'Нейрохирург',
        specializationEn: 'Neurosurgeon',
        patientConsultation: 'К нейрохирургу'
      },
      {
        value: 'Неонатология',
        specializationRu: 'Неонатолог',
        specializationEn: 'Neonatologist',
        patientConsultation: 'К неонатологу'
      },
      {
        value: 'Нефрология',
        specializationRu: 'Нефролог',
        specializationEn: 'Nephrologist',
        patientConsultation: 'К нефрологу'
      }
    ]
  },
  {
    title: 'О',
    data: [
      {
        value: 'Офтальмология',
        specializationRu: 'Окулист (Офтальмолог)',
        specializationEn: 'Ophthalmologist',
        patientConsultation: 'К офтальмологу'
      },
      {
        value: 'Онкология',
        specializationRu: 'Онколог',
        specializationEn: 'Oncologist',
        patientConsultation: 'К онкологу'
      },
      {
        value: 'Ортопедия',
        specializationRu: 'Ортопед',
        specializationEn: 'Orthopedist',
        patientConsultation: 'К ортопеду'
      },
      {
        value: 'Общая практика',
        specializationRu: 'Общая практика',
        specializationEn: 'General practice',
        patientConsultation: 'Общая практика'
      },
      {
        value: 'Остеопатия',
        specializationRu: 'Остеопат',
        specializationEn: 'Osteopath',
        patientConsultation: 'К остеопату'
      }
    ]
  },
  {
    title: 'П',
    data: [
      {
        value: 'Паразитология',
        specializationRu: 'Паразитолог',
        specializationEn: 'Parasitologist',
        patientConsultation: 'К паразитологу'
      },
      {
        value: 'Пародонтология',
        specializationRu: 'Пародонтолог',
        specializationEn: 'Periodontist',
        patientConsultation: 'К пародонтологу'
      },
      {
        value: 'Патологоанатомия',
        specializationRu: 'Патологоанатом',
        specializationEn: 'Pathologist',
        patientConsultation: 'К патологоанатому' // :^)
      },
      {
        value: 'Педагогика',
        specializationRu: 'Педагог',
        specializationEn: 'Pedagogue',
        patientConsultation: 'К педагогу'
      },
      {
        value: 'Педиатрия',
        specializationRu: 'Педиатр',
        specializationEn: 'Pediatrician',
        patientConsultation: 'К педиатру'
      },
      {
        value: 'Пластическая хирургия',
        specializationRu: 'Пластический хирург',
        specializationEn: 'Plastic surgeon',
        patientConsultation: 'К пластическому хирургу'
      },
      {
        value: 'Проктология',
        specializationRu: 'Проктолог',
        specializationEn: 'Proctologist',
        patientConsultation: 'К проктологу'
      },
      {
        value: 'Профпатология',
        specializationRu: 'Профпатолог',
        specializationEn: 'Profpathologist',
        patientConsultation: 'К профпатологу'
      },
      {
        value: 'Психиатрия',
        specializationRu: 'Психиатр',
        specializationEn: 'Psychiatrist',
        patientConsultation: 'К психиатру'
      },
      {
        value: 'Психология',
        specializationRu: 'Психолог',
        specializationEn: 'Psychologist',
        patientConsultation: 'К психологу'
      },
      {
        value: 'Психотерапевтия',
        specializationRu: 'Психотерапевт',
        specializationEn: 'Psychotherapist',
        patientConsultation: 'К психотерапевту'
      },
      {
        value: 'Пульмонология',
        specializationRu: 'Пульмонолог',
        specializationEn: 'Pulmonologist',
        patientConsultation: 'К пульмонологу'
      }
    ]
  },
  {
    title: 'Р',
    data: [
      {
        value: 'Радиология',
        specializationRu: 'Радиолог',
        specializationEn: 'Radiologist',
        patientConsultation: 'К радиологу'
      },
      {
        value: 'Реабилитология',
        specializationRu: 'Реабилитолог',
        specializationEn: 'Rehabilitation therapist',
        patientConsultation: 'К реабилитологу'
      },
      {
        value: 'Реаниматология',
        specializationRu: 'Реаниматолог',
        specializationEn: 'Intensivist',
        patientConsultation: 'К реаниматологу'
      },
      {
        value: 'Ревматология',
        specializationRu: 'Ревматолог',
        specializationEn: 'Rheumatologist',
        patientConsultation: 'К ревматологу'
      },
      {
        value: 'Рентгенология',
        specializationRu: 'Рентгенолог',
        specializationEn: 'Radiographer',
        patientConsultation: 'К рентгенологу'
      },
      {
        value: 'Репродуктология (ЭКО)',
        specializationRu: 'Репродуктолог (ЭКО)',
        specializationEn: 'Reproductologist',
        patientConsultation: 'К репродуктологу'
      },
      {
        value: 'Рефлексотерапевтия',
        specializationRu: 'Рефлексотерапевт',
        specializationEn: 'Reflexologist',
        patientConsultation: 'К рефлексотерапевту'
      }
    ]
  },
  {
    title: 'С',
    data: [
      {
        value: 'Сексология',
        specializationRu: 'Сексолог',
        specializationEn: 'Sexologist',
        patientConsultation: 'К сексологу'
      },
      {
        value: 'Сомнология',
        specializationRu: 'Сомнолог',
        specializationEn: 'Somnologist',
        patientConsultation: 'К сомнологу'
      },
      {
        value: 'Стоматология',
        specializationRu: 'Стоматолог',
        specializationEn: 'Dentist',
        patientConsultation: 'К стоматологу'
      },
      {
        value: 'Стоматология детская',
        specializationRu: 'Стоматолог детский',
        specializationEn: 'Pediatric dentist',
        patientConsultation: 'К детскому стоматологу'
      },
      {
        value: 'Стоматология (гигиена)',
        specializationRu: 'Стоматолог-гигиенист',
        specializationEn: 'Dentist hygienist',
        patientConsultation: 'К стоматологу-гигиенисту'
      },
      {
        value: 'Стоматология (ортодонтия)',
        specializationRu: 'Стоматолог-ортодонт',
        specializationEn: 'Dentist orthodontist',
        patientConsultation: 'К стоматологу-ортодонту'
      },
      {
        value: 'Стоматология (ортопедия)',
        specializationRu: 'Стоматолог-ортопед',
        specializationEn: 'Dentist orthopedist',
        patientConsultation: 'К стоматологу-ортопеду'
      },
      {
        value: 'Стоматология (терапевтия)',
        specializationRu: 'Стоматолог-терапевт',
        specializationEn: 'Dentist therapist',
        patientConsultation: 'К стоматологу-терапевту'
      },
      {
        value: 'Стоматология (хирургия)',
        specializationRu: 'Стоматолог-хирург',
        specializationEn: 'Dentist surgeon',
        patientConsultation: 'К стоматологу-хирургу'
      },
      {
        value: 'Спортивная медицина',
        specializationRu: 'Спортивная медицина',
        specializationEn: 'Sports medicine',
        patientConsultation: 'На спортивную медицину'
      },
      {
        value: 'Сурдология',
        specializationRu: 'Сурдолог',
        specializationEn: 'Surdologist',
        patientConsultation: 'К сурдологу'
      }
    ]
  },
  {
    title: 'Т',
    data: [
      {
        value: 'Терапия',
        specializationRu: 'Терапевт',
        specializationEn: 'Therapist',
        patientConsultation: 'К терапевту'
      },
      {
        value: 'Токсикология',
        specializationRu: 'Токсиколог',
        specializationEn: 'Toxicologist',
        patientConsultation: 'К токсикологу'
      },
      {
        value: 'Травматология',
        specializationRu: 'Травматолог',
        specializationEn: 'Traumatologist',
        patientConsultation: 'К травматологу'
      },
      {
        value: 'Трансплантология',
        specializationRu: 'Трансплантолог',
        specializationEn: 'Transplantologist',
        patientConsultation: 'К трансплантологу'
      },
      {
        value: 'Трансфузиология',
        specializationRu: 'Трансфузиолог',
        specializationEn: 'Transfusiologist',
        patientConsultation: 'К трансфузиологу'
      },
      {
        value: 'Трихология',
        specializationRu: 'Трихолог',
        specializationEn: 'Trichologist',
        patientConsultation: 'К трихологу'
      }
    ]
  },
  {
    title: 'У',
    data: [
      {
        value: 'Ультразвуковая диагностика',
        specializationRu: 'Ультразвуковая диагностика',
        specializationEn: 'Ultrasound diagnostician',
        patientConsultation: 'На ультразвуковую диагностику'
      },
      {
        value: 'Урология',
        specializationRu: 'Уролог',
        specializationEn: 'Urologist',
        patientConsultation: 'К урологу'
      }
    ]
  },
  {
    title: 'Ф',
    data: [
      {
        value: 'Фармацевтика',
        specializationRu: 'Фармацевт',
        specializationEn: 'Pharmacist',
        patientConsultation: 'К фармацевту'
      },
      {
        value: 'Фельдшерство',
        specializationRu: 'Фельдшер',
        specializationEn: 'Feldsher',
        patientConsultation: 'К фельдшеру'
      },
      {
        value: 'Физиотерапевтия',
        specializationRu: 'Физиотерапевт',
        specializationEn: 'Physiotherapist',
        patientConsultation: 'К физиотерапевту'
      },
      {
        value: 'Флебология',
        specializationRu: 'Флеболог',
        specializationEn: 'Phlebologist',
        patientConsultation: 'К флебологу'
      },
      {
        value: 'Фониатрия',
        specializationRu: 'Фониатр',
        specializationEn: 'Phoniatrist',
        patientConsultation: 'К фониатру'
      },
      {
        value: 'Функциональная диагностика',
        specializationRu: 'Функциональная диагностика',
        specializationEn: 'Functional diagnostician',
        patientConsultation: 'На функциональную диагностику'
      },
      {
        value: 'Фтизиатрия',
        specializationRu: 'Фтизиатр',
        specializationEn: 'Phthisiatrician',
        patientConsultation: 'К фтизиатру'
      }
    ]
  },
  {
    title: 'Х',
    data: [
      {
        value: 'Хирургия',
        specializationRu: 'Хирург',
        specializationEn: 'Surgeon',
        patientConsultation: 'К хирургу'
      },
      {
        value: 'Хирургия-имплантология',
        specializationRu: 'Хирург-имплантолог',
        specializationEn: 'Implant surgeon',
        patientConsultation: 'К хирургу-имплантологу'
      }
    ]
  },
  {
    title: 'Ч',
    data: [
      {
        value: 'Челюстно-лицевая хирургия',
        specializationRu: 'Челюстно-лицевой хирург',
        specializationEn: 'Oral and maxillofacial surgeon',
        patientConsultation: 'К челюстно-лицевому хирургу'
      }
    ]
  },
  {
    title: 'Э',
    data: [
      {
        value: 'Эмбриология',
        specializationRu: 'Эмбриолог',
        specializationEn: 'Embryologist',
        patientConsultation: 'К эмбриологу'
      },
      {
        value: 'Эндокринология',
        specializationRu: 'Эндокринолог',
        specializationEn: 'Endocrinologist',
        patientConsultation: 'К эндокринологу'
      },
      {
        value: 'Эндоскопистия',
        specializationRu: 'Эндоскопист',
        specializationEn: 'Endoscopist',
        patientConsultation: 'К эндоскописту'
      },
      {
        value: 'Эпидемиология',
        specializationRu: 'Эпидемиолог',
        specializationEn: 'Epidemiologist',
        patientConsultation: 'К эпидемиологу'
      },
      {
        value: 'Эпилептология',
        specializationRu: 'Эпилептолог',
        specializationEn: 'Epileptologist',
        patientConsultation: 'К эпилептологу'
      }
    ]
  }
]
