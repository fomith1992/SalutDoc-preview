import { StyleSheet, View } from 'react-native'
import { styled, variants } from '../../../style/styled'

export const Overlay = styled(View, StyleSheet.absoluteFill, variants('background', {
  gray: {
    backgroundColor: 'rgba(148, 148, 148, 0.4)'
  },
  transparent: {
    backgroundColor: 'transparent'
  }
}))

export const Container = styled(View, {
  flex: 1
})
