import React from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import { Modal } from '../modal'
import { Container, Overlay } from './dismissible-modal.style'

export interface DismissibleModalProps {
  background?: 'gray' | 'transparent'
  visible: boolean
  onDismiss: () => void
  children?: React.ReactNode
}

export function DismissibleModal (
  {
    background = 'transparent',
    visible,
    children,
    onDismiss
  }: DismissibleModalProps
): React.ReactElement {
  return (
    <Modal
      visible={visible}
      animation='fade'
    >
      <Container>
        <TouchableWithoutFeedback onPress={onDismiss}>
          <Overlay background={background} />
        </TouchableWithoutFeedback>
        {children}
      </Container>
    </Modal>
  )
}
