export const oidcFirebaseConfig = {
  client_id: 'mobile-native',
  scope: 'openid',
  grant_type: 'firebase_token'
}
