import { useCallback } from 'react'
import { expectDefined } from '@salutdoc/react-components'
import { oidcFirebaseConfig } from './oidc-firebase-config'
import { useLogoutFirebase } from '../../hooks/firebase/use-logout-firebase'
import { useCredentialsRef, useUpdateCredentials, credentialsSchema } from './credentials-provider'

interface OidcFirebaseAuthenticationHook {
  login: (token: string) => Promise<void>
  logout: () => Promise<void>
}

export function useOidcFirebaseAuthentication (): OidcFirebaseAuthenticationHook {
  const credentialsRef = useCredentialsRef()
  const updateCredentials = useUpdateCredentials()
  const logoutFirebase = useLogoutFirebase()
  const FRONT_URL = expectDefined(process.env.FRONT_URL)

  const login = useCallback(async function login (token: string): Promise<void> {
    try {
      const response = await fetch(`${FRONT_URL}auth/v1/oidc/token`, {
        method: 'post',
        body: Object.entries({
          ...oidcFirebaseConfig,
          token
        }).map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`).join('&'),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      if (response.ok) {
        const data = await response.json()
        const credentials = credentialsSchema.check({
          idToken: data.id_token,
          accessToken: data.access_token,
          accessTokenExpiresAt: data.expires_in.toString(),
          refreshToken: data.refresh_token
        })
        updateCredentials(credentials)
      }
    } catch (e) {
      console.log(e)
      throw new Error('failed to load oidc')
    }
  }, [updateCredentials])

  const logout = useCallback(async function logout (): Promise<void> {
    const idToken = credentialsRef.current?.idToken
    if (idToken == null) {
      throw new Error('not authenticated')
    } else {
      updateCredentials(null)
      logoutFirebase()
    }
  }, [credentialsRef])

  return { login, logout }
}
