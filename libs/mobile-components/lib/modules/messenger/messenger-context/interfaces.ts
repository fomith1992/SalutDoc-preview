export type MessengerEventTypes = {
  disconnected: () => void
  connected: () => void
}

export type Unregister = () => void
