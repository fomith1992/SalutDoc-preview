import { useApolloClient } from '@apollo/react-hooks'
import { NavigationContainerRef } from '@react-navigation/native'
import { EventEmitter } from 'events'
import gql from 'graphql-tag'
import React, { useEffect, useRef, useState } from 'react'
import useThrottle from 'react-use/lib/useThrottle'
import { useAuthenticatedUserOrNull } from '../../../components/user-context/user-context-provider'
import {
  ChatEventsDocument,
  ChatEventsQuery,
  MessengerUpdatesDocument,
  MessengerUpdatesSubscription,
  MessengerUpdatesSubscriptionVariables
} from '../../../gen/graphql'
import { CheckMarkIcon20 } from '../../../images/check-mark.icon-20'
import { ListIcon20 } from '../../../images/list.icon-20'
import { playLocalSoundFile } from '../../../utils/play-sounds/play-sound-file'
import { useShowNotification } from '../../ui-kit/notifications'
import { context, ContextType } from './messenger-context'
import { ProfileIcon24 } from '../../../images/profile.icon-24'

export type MessengerContextProviderProps = {
  children?: React.ReactNode
  navigationRef: React.RefObject<NavigationContainerRef>
}

export function MessengerContextProvider (
  {
    navigationRef,
    children
  }: MessengerContextProviderProps
): React.ReactElement {
  const subscriptionTokenRef = useRef<string | null>(null)
  const connectedStateRef = useRef(false)
  const mutedChatsMap = useRef<Record<string, Set<unknown>>>({}).current
  const [attemptNum, setAttemptNum] = useState(0)
  const currentAttemptNum = useThrottle(attemptNum, 1000) // retry no more than once a second
  const apollo = useApolloClient()
  const showNotification = useShowNotification()
  const eventEmitter = useRef(new EventEmitter()).current

  const user = useAuthenticatedUserOrNull()

  const openChat = (consultationId: string | null): void => {
    if (navigationRef != null && user != null && consultationId != null) {
      switch (user.role) {
        case 'USER':
          navigationRef.current?.navigate('PatientConsultationChat', { consultationId })
          break
        case 'DOCTOR':
        case 'MODERATOR':
          navigationRef.current?.navigate('DoctorConsultationChat', { consultationId })
          break
      }
    }
  }

  useEffect(() => {
    if (user == null) {
      return undefined
    }
    const subscription = apollo.subscribe<MessengerUpdatesSubscription, MessengerUpdatesSubscriptionVariables>({
      query: MessengerUpdatesDocument,
      variables: {
        subscriptionToken: subscriptionTokenRef.current
      }
    }).subscribe(
      subscriptionData => {
        const event = subscriptionData.data?.messengerUpdates
        if (event != null) {
          if (!connectedStateRef.current) {
            connectedStateRef.current = true
            eventEmitter.emit('connected')
          }
          subscriptionTokenRef.current = event.subscriptionToken
          switch (event.__typename) {
            case 'MessengerNewChatEventUpdate': {
              try {
                const data = apollo.readQuery<ChatEventsQuery>({
                  query: ChatEventsDocument,
                  variables: { id: event.chatEvent.chat.id }
                })
                if (data?.chatById?.events != null) {
                  apollo.writeQuery({
                    query: ChatEventsDocument,
                    variables: { id: event.chatEvent.chat.id },
                    data: {
                      ...data,
                      chatById: {
                        ...data.chatById,
                        events: {
                          ...data.chatById.events,
                          edges: [
                            {
                              __typename: 'ChatEventEdge',
                              node: event.chatEvent
                            },
                            ...data.chatById.events.edges.filter(item =>
                              item.node.__typename !== 'Message' || event.chatEvent.__typename !== 'Message' ||
                              event.chatEvent.nonce !== item.node.id)
                          ]
                        }
                      }
                    }
                  })
                }
              } catch (e) {
                console.debug('Failed to update chat events on subscription event')
              }
              const consultationId: string | null = event.chatEvent.chat.__typename === 'ConsultationChat'
                ? event.chatEvent.chat.consultation.id
                : null
              const mutedChatTokens = mutedChatsMap[event.chatEvent.chat.id]
              if (mutedChatTokens == null || mutedChatTokens.size === 0) {
                switch (event.chatEvent.__typename) {
                  case 'ConsultationStarted': {
                    if (event.chatEvent.chat.consultation.patient.id === user.id) {
                      showNotification({
                        avatar: event.chatEvent.chat.__typename === 'ConsultationChat'
                          ? event.chatEvent.chat.consultation.doctor?.avatar
                          : null,
                        title: 'Консультация началась',
                        message: 'Врач подключился к консультации',
                        onDisplayed: () => playLocalSoundFile('notifications'),
                        onPress: () => openChat(consultationId)
                      })
                    }
                    break
                  }
                  case 'ConsultationFinished': {
                    let message = 'Подготовьте медзаключение'
                    if (event.chatEvent.chat.consultation.patient.id === user.id) {
                      message = 'Врач начал готовить медзаключение'
                    }
                    showNotification({
                      icon: CheckMarkIcon20,
                      title: 'Консультация завершена',
                      message,
                      onDisplayed: () => playLocalSoundFile('notifications'),
                      onPress: () => openChat(consultationId)
                    })
                    break
                  }
                  case 'ConsultationReportAdded': {
                    if (event.chatEvent.chat.consultation.patient.id === user.id) {
                      showNotification({
                        icon: ListIcon20,
                        title: 'Медзаключение готово',
                        message: 'Врач подготовил медзаключение',
                        onDisplayed: () => playLocalSoundFile('notifications'),
                        onPress: () => openChat(consultationId)
                      })
                    }
                    break
                  }
                  case 'Message': {
                    if (event.chatEvent.author.id !== user.id) {
                      showNotification({
                        title: `${event.chatEvent.author.firstName} ${event.chatEvent.author.lastName}`,
                        message: event.chatEvent.text,
                        icon: ProfileIcon24,
                        avatar: event.chatEvent.author.avatar,
                        onDisplayed: () => playLocalSoundFile('notifications'),
                        onPress: () => openChat(consultationId)
                      })
                    }
                    break
                  }
                }
              }

              try {
                const unreadMessagesCountFragment = gql`
                    fragment UnreadMessagesCount on Chat {
                        unreadMessagesCount(userId: $userId)
                    }
                `
                const chatCacheId = `Chat:${event.chatEvent.chat.id}`
                const data = apollo.readFragment<{ unreadMessagesCount: number }, { userId: string }>({
                  id: chatCacheId,
                  variables: { userId: user.id },
                  fragment: unreadMessagesCountFragment
                })
                if (data?.unreadMessagesCount != null) {
                  apollo.writeFragment<{ unreadMessagesCount: number }, { userId: string }>({
                    id: chatCacheId,
                    fragment: unreadMessagesCountFragment,
                    variables: { userId: user.id },
                    data: {
                      ...data,
                      unreadMessagesCount: data.unreadMessagesCount + (event.chatEvent.__typename === 'Message' && event.chatEvent.author.id === user.id ? 0 : 1)
                    }
                  })
                }
              } catch (e) {
                console.debug('Failed to update chat unread messages count', e)
              }
              break
            }
          }
        }
      },
      error => {
        console.warn('MessengerUpdates subscription error', error)
        eventEmitter.emit('disconnected')
        connectedStateRef.current = false
        setAttemptNum(x => x + 1)
      },
      () => {
        console.warn('MessengerUpdates subscription unexpectedly completed')
        eventEmitter.emit('disconnected')
        connectedStateRef.current = false
        setAttemptNum(x => x + 1)
      }
    )
    return () => subscription.unsubscribe()
  }, [currentAttemptNum, user])

  const value = useRef<ContextType>({
    muteChatNotifications: chatId => {
      const token = {}
      ;(mutedChatsMap[chatId] = mutedChatsMap[chatId] ?? new Set()).add(token)
      return () => mutedChatsMap[chatId]?.delete(token)
    },
    handleEvent: (event, handler) => {
      eventEmitter.addListener(event, handler)
      return () => eventEmitter.removeListener(event, handler)
    }
  }).current

  return <context.Provider value={value}>{children}</context.Provider>
}
