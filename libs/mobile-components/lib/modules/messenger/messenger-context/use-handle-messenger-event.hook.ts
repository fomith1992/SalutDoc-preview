import { useEffect } from 'react'
import { MessengerEventTypes } from './interfaces'
import { useContextRequired } from './messenger-context'

export function useHandleMessengerEvent<T extends keyof MessengerEventTypes> (
  event: T,
  handler: MessengerEventTypes[T]
): void {
  const { handleEvent } = useContextRequired()
  useEffect(() => handleEvent(event, handler), [handleEvent, event, handler])
}
