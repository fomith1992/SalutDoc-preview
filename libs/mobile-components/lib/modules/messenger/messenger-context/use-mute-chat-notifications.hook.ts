import { useEffect } from 'react'
import { useContextRequired } from './messenger-context'

export function useMuteChatNotifications (chatId: string): void {
  const { muteChatNotifications } = useContextRequired()
  useEffect(() => muteChatNotifications(chatId), [muteChatNotifications, chatId])
}
