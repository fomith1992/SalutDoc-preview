import React, { useContext } from 'react'
import { MessengerEventTypes, Unregister } from './interfaces'

export type ContextType = {
  muteChatNotifications: (chatId: string) => Unregister
  handleEvent: <T extends keyof MessengerEventTypes>(event: T, handler: MessengerEventTypes[T]) => Unregister
}

export const context = React.createContext<ContextType | null>(null)

export function useContextRequired (): ContextType {
  const ctx = useContext(context)
  if (ctx == null) {
    throw new Error('Messenger context not provided, ' +
      'ensure to have MessengerContextProvider upper in the component tree')
  }
  return ctx
}
