import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { useFocusEffect } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React, { useCallback } from 'react'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { GlobalNavigatorParamList } from '../../../navigation/global-navigator-param-list'
import { PatientConsultationChat } from '../patient-consultation-chat'

export function PatientConsultationChatScreen (
  {
    route,
    navigation
  }: StackScreenProps<GlobalNavigatorParamList, 'PatientConsultationChat'>
): React.ReactElement {
  const bottomTabNavigation = navigation.dangerouslyGetParent<BottomTabNavigationProp<any>>()

  useFocusEffect(
    useCallback(() => {
      bottomTabNavigation.setOptions({ tabBarVisible: false })
      return () => bottomTabNavigation.setOptions({ tabBarVisible: true })
    }, [bottomTabNavigation])
  )

  return (
    <ScreenContentView>
      <PatientConsultationChat consultationId={route.params?.consultationId} />
    </ScreenContentView>
  )
}
