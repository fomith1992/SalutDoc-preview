import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { StackScreenProps } from '@react-navigation/stack'
import React, { useCallback } from 'react'
import { GlobalNavigatorParamList } from '../../../navigation/global-navigator-param-list'
import { FreeConsultationChat } from '../free-consultation-chat'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { useFocusEffect } from '@react-navigation/native'

export function FreeConsultationChatScreen (
  {
    route,
    navigation
  }: StackScreenProps<GlobalNavigatorParamList, 'FreeConsultationChat'>
): React.ReactElement {
  const bottomTabNavigation = navigation.dangerouslyGetParent<BottomTabNavigationProp<any>>()

  useFocusEffect(
    useCallback(() => {
      bottomTabNavigation.setOptions({ tabBarVisible: false })
      return () => bottomTabNavigation.setOptions({ tabBarVisible: true })
    }, [bottomTabNavigation])
  )

  return (
    <ScreenContentView>
      <FreeConsultationChat consultationId={route.params?.consultationId} />
    </ScreenContentView>
  )
}
