import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import { useFocusEffect } from '@react-navigation/native'
import { StackScreenProps } from '@react-navigation/stack'
import React, { useCallback } from 'react'
import { ScreenContentView } from '../../../components/screen-content-view/screen-content-view'
import { GlobalNavigatorParamList } from '../../../navigation/global-navigator-param-list'
import { DoctorConsultationChat } from '../doctor-consultation-chat'

export function DoctorConsultationChatScreen (
  {
    route,
    navigation
  }: StackScreenProps<GlobalNavigatorParamList, 'DoctorConsultationChat'>
): React.ReactElement {
  const bottomTabNavigation = navigation.dangerouslyGetParent<BottomTabNavigationProp<any>>()

  useFocusEffect(
    useCallback(() => {
      bottomTabNavigation.setOptions({ tabBarVisible: false })
      return () => bottomTabNavigation.setOptions({ tabBarVisible: true })
    }, [bottomTabNavigation])
  )

  return (
    <ScreenContentView>
      <DoctorConsultationChat consultationId={route.params?.consultationId} />
    </ScreenContentView>
  )
}
