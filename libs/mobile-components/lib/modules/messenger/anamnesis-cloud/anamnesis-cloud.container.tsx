import React from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useAnamnesisCloudQuery } from '../../../gen/graphql'
import { MessageCloud } from '../message/message-cloud.view'

export type AnamnesisCloudProps = {
  consultationId: string
}

export function AnamnesisCloud (
  {
    consultationId
  }: AnamnesisCloudProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const { data } = useAnamnesisCloudQuery({
    variables: { consultationId }
  })

  if (data?.consultationById?.anamnesis == null) {
    return <></>
  }
  const { patient, anamnesis, createdAt } = data.consultationById

  return (
    <MessageCloud
      isMy={patient.id === user.id}
      avatar={patient.avatar ?? undefined}
      time={new Date(createdAt)}
      content={{
        text: anamnesis.complaints ?? '',
        attachments: anamnesis.files.map(file => ({
          type: 'file',
          id: file.id,
          url: file.url
        }))
      }}
      sending={false}
      isTopMessageInGroup
      isBottomMessageInGroup
    />
  )
}
