import { useApolloClient } from '@apollo/react-hooks'
import React from 'react'
import { v4 as uuid } from 'uuid'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import {
  ChatEventDataFragment,
  ChatEventsDocument,
  ChatEventsQuery,
  useMessageComposeBarQuery,
  useSendMessageMutation
} from '../../../gen/graphql'
import { useShowNotification } from '../../ui-kit/notifications'
import { MessageComposeBarView } from './message-compose-bar.view'

export type MessageComposeBarProps = {
  chatId: string
}

export function MessageComposeBar (
  {
    chatId
  }: MessageComposeBarProps
): React.ReactElement {
  const apollo = useApolloClient()
  const user = useAuthenticatedUser()
  const showNotification = useShowNotification()
  const [sendMessage] = useSendMessageMutation()
  const { data } = useMessageComposeBarQuery({
    variables: { chatId }
  })

  type ChatEventEdge = {
    __typename?: 'ChatEventEdge'
    node: ChatEventDataFragment
  }

  function updateMessagesCache (
    updateFn: (edges: ChatEventEdge[]) => ChatEventEdge[]
  ): void {
    try {
      const data = apollo.readQuery<ChatEventsQuery>({
        query: ChatEventsDocument,
        variables: { id: chatId }
      })
      if (data?.chatById?.events != null) {
        apollo.writeQuery({
          query: ChatEventsDocument,
          variables: { id: chatId },
          data: {
            ...data,
            chatById: {
              ...data.chatById,
              events: {
                ...data.chatById.events,
                edges: updateFn(data.chatById.events.edges)
              }
            }
          }
        })
      }
    } catch (e) {
      console.error('Failed to update messages cache on send message', e)
    }
  }

  const { userMyself, chatById } = data ?? {}
  if (userMyself == null || chatById == null) return <></>

  const handleSendMessageFailed = (nonce: string): void => {
    updateMessagesCache(edges => edges.filter(({ node }) => node.id !== nonce))
    showNotification({
      title: 'Ошибка',
      message: 'Не удалось отправить сообщение'
    })
  }

  return (
    <MessageComposeBarView
      onSendMessage={text => {
        const nonce = uuid()
        sendMessage({
          variables: {
            chatId,
            content: {
              text,
              nonce,
              attachments: [] // TODO: SD-1100
            }
          }
        }).then(res => {
          if (res.data?.chatSendMessage.node == null) {
            handleSendMessageFailed(nonce)
            console.error('Failed to send message', res)
          }
        }).catch(error => {
          handleSendMessageFailed(nonce)
          console.error('Failed to send message', error)
        })
        updateMessagesCache(edges => [
          {
            __typename: 'ChatEventEdge',
            node: {
              __typename: 'Message',
              id: nonce,
              nonce,
              eventTime: new Date().toISOString(),
              sending: true,
              text,
              author: {
                __typename: 'User',
                id: user.id,
                avatar: user.avatar,
                firstName: userMyself.firstName,
                lastName: userMyself.lastName
              },
              chat: chatById
            }
          },
          ...edges
        ])
      }}
    />
  )
}
