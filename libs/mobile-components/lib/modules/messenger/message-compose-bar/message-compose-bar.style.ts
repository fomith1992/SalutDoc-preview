import { Platform, TextInput, TouchableOpacity, View } from 'react-native'
import { ClipIcon24 } from '../../../images/clip.icon-24'
import { SendIcon24 } from '../../../images/send.icon-24'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const ChatFooter = styled(View, {
  paddingVertical: sp(16),
  paddingLeft: sp(16),
  paddingRight: sp(8),
  backgroundColor: color('surface'),
  flexDirection: 'row',
  alignItems: 'flex-end',
  ...Platform.select({
    android: {
      elevation: 2
    },
    ios: {
      shadowColor: color('shadow'),
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.06,
      shadowRadius: 2
    }
  })
})

export const TextInputMessage = styled(TextInput, (props: {
  isEmpty: boolean
}) => ({
  ...font({ type: 'text1' }),
  color: color(props.isEmpty ? 'inactiveLight' : 'text'),
  maxHeight: sp(
    12 * 2 + // padding
    0.4 + // border
    20 * 5 as any // lines
  ),
  flex: 1,
  padding: sp(12), // 10 on design
  paddingTop: sp(12 as any), // overriding for iOS
  borderRadius: sp(24),
  borderColor: color('inactiveLight'),
  borderWidth: 0.2,
  overflow: 'hidden'
}) as const)

export const ButtonFooter = styled(TouchableOpacity, {
  padding: sp(12),
  alignItems: 'center',
  justifyContent: 'center'
})

export const SendImage = styled(SendIcon24, (props: {active: boolean}) => ({
  color: color(props.active ? 'accent' : 'subtext')
}) as const)

export const ClipImg = styled(ClipIcon24, {
  color: color('subtext')
})

export const Spring = styled(View, {
  width: sp(8)
})
