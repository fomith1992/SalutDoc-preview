import React, { useState } from 'react'
import { NewMessageContextMenu } from '../../../components/chat/new-message-context-menu/new-message-context-menu'
import { useBooleanFlag } from '../../../hooks/use-boolean-flag'
import { ButtonFooter, ChatFooter, SendImage, Spring, TextInputMessage } from './message-compose-bar.style'

// max length outcoming message
const inputMessageMaxLength = 10000

export type MessageComposeBarViewProps = {
  onSendMessage: (text: string) => void
}

export function MessageComposeBarView (
  {
    onSendMessage
  }: MessageComposeBarViewProps
): React.ReactElement {
  const { state: contextMenuVisible, setFalse: hideContextMenu } = useBooleanFlag(false)
  //! test state
  const [value, setValue] = useState('')
  const isMessageEmpty = /^\s*$/.test(value)
  return (
    <ChatFooter>
      <TextInputMessage
        maxLength={inputMessageMaxLength}
        value={value}
        isEmpty={isMessageEmpty}
        onChangeText={setValue}
        placeholder='Введите текст'
        multiline
      />
      <ButtonFooter
        disabled={isMessageEmpty}
        onPress={() => {
          onSendMessage(value)
          setValue('')
        }}
      >
        <SendImage active={!isMessageEmpty} />
      </ButtonFooter>
      <Spring />
      <NewMessageContextMenu
        visible={contextMenuVisible}
        onHideMenu={hideContextMenu}
      />
    </ChatFooter>
  )
}

/*
      <ButtonFooter
        onPress={showContextMenu}
      >
        <ClipImg />
      </ButtonFooter>
 */
