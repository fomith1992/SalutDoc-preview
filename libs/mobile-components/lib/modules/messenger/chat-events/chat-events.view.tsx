import React, { useRef } from 'react'
import { FlatList, ViewabilityConfigCallbackPairs } from 'react-native'
import useLatest from 'react-use/lib/useLatest'
import { LoadingIndicator } from '../../../components/loading-indicator/loading-indicator.view'
import { useMaterialized } from '../../../style/styled'
import { listStyleSheet } from './chat-events.style'

export type ChatViewProps<T> = {
  items: readonly T[]
  renderItem: (item: T, index: number) => React.ReactElement | null
  keyExtractor: (item: T, index: number) => string
  onVisibleItemsChange?: (items: T[]) => void

  header?: React.ReactElement
  footer?: React.ReactElement

  hasMore: boolean
  loadingInitial: boolean
  loadingMore: boolean
  loadMore: () => void
}

export function ChatEventsView<T> (
  {
    items,
    renderItem,
    keyExtractor,
    onVisibleItemsChange,
    header,
    footer,
    hasMore,
    loadingMore,
    loadingInitial,
    loadMore
  }: ChatViewProps<T>
): React.ReactElement {
  const listStyle = useMaterialized(listStyleSheet)
  const handleViewableItemsChanged = useLatest(onVisibleItemsChange)
  const viewabilityConfigCallbackPairs = useRef<ViewabilityConfigCallbackPairs>([
    {
      viewabilityConfig: {
        minimumViewTime: 500,
        viewAreaCoveragePercentThreshold: 100
      },
      onViewableItemsChanged: ({ viewableItems }) => {
        handleViewableItemsChanged.current?.(viewableItems.map(({ item }) => item))
      }
    }
  ]).current // because viewabilityConfigCallbackPairs cannot be changed on fly
  return (
    <FlatList
      inverted
      data={items}
      renderItem={({ item, index }) => renderItem(item, index)}
      keyExtractor={keyExtractor}
      contentContainerStyle={listStyle}
      ListFooterComponent={!hasMore && !loadingInitial
        ? footer
        : <LoadingIndicator visible={loadingInitial || loadingMore} />}
      ListHeaderComponent={header}
      onEndReached={hasMore ? loadMore : null}
      viewabilityConfigCallbackPairs={viewabilityConfigCallbackPairs}
    />
  )
}
