import { ViewStyle } from 'react-native'
import { sp } from '../../../style/size'
import { lift } from '../../../style/styled'

export const listStyleSheet = lift<ViewStyle>({
  paddingTop: sp(16) // because inverted
})
