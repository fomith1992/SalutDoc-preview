import { appendEdges } from '@salutdoc/react-components'
import { assertNonNil, expectNonNil } from '@salutdoc/utils/lib/fp/maybe'
import React, { useCallback } from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { ChatEventDataFragment, useChatEventsQuery, useMarkChatEventsReadMutation } from '../../../gen/graphql'
import { useMemoizedFn } from '../../../hooks/use-memoized-fn'
import { ConsultationFinished } from '../consulation-finished'
import { ConsultationReportAdded } from '../consultation-report-added'
import { ConsultationStarted } from '../consultation-started'
import { Message } from '../message'
import { useMuteChatNotifications } from '../messenger-context'
import { useHandleMessengerEvent } from '../messenger-context/use-handle-messenger-event.hook'
import { UserJoin } from '../user-join'
import { ChatEventsView } from './chat-events.view'

export type ChatEventsProps = {
  id: string
  header?: React.ReactElement
  footer?: React.ReactElement
}

export function ChatEvents (
  {
    id,
    header,
    footer
  }: ChatEventsProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  useMuteChatNotifications(id)
  const { data, loading, fetchMore, refetch } = useChatEventsQuery({
    variables: { id },
    notifyOnNetworkStatusChange: true
  })
  useHandleMessengerEvent('connected', useCallback(() => {
    refetch().catch(/* handled in hook */)
  }, [refetch]))

  const [markChatEventsRead] = useMarkChatEventsReadMutation({
    onCompleted: data => {
      if (data.markMessagesAsRead.chat == null) {
        console.error(
          'Failed to mark chat events read',
          data.markMessagesAsRead.message,
          data.markMessagesAsRead.errors
        )
      }
    }
  })

  const loadMore = useMemoizedFn((before: string) => {
    fetchMore({
      variables: { id, before },
      updateQuery: (previousQueryResult, { fetchMoreResult }) => {
        assertNonNil(previousQueryResult.chatById?.events)
        return {
          ...previousQueryResult,
          chatById: {
            ...previousQueryResult.chatById,
            events: appendEdges(previousQueryResult.chatById.events, fetchMoreResult?.chatById?.events)
          }
        }
      }
    }).catch(e => {
      console.error('Failed to load more chat events', e)
    })
  })

  const items = data?.chatById?.events?.edges ?? []
  return (
    <ChatEventsView
      items={items}
      keyExtractor={item => item.node.id}
      renderItem={(item, index) => {
        switch (item.node.__typename) {
          case 'Message':
            return (
              <Message
                id={item.node.id}
                isBottomMessageInGroup={!shouldBeGrouped(item.node, items[index - 1]?.node)}
                isTopMessageInGroup={!shouldBeGrouped(item.node, items[index + 1]?.node)}
              />
            )
          case 'ConsultationStarted':
            return <ConsultationStarted id={item.node.id} />
          case 'ConsultationFinished':
            return <ConsultationFinished id={item.node.id} />
          case 'ConsultationReportAdded':
            return <ConsultationReportAdded id={item.node.id} />
          case 'UserJoin':
            return <UserJoin id={item.node.id} />
          default:
            return <></>
        }
      }}
      onVisibleItemsChange={visibleItems => {
        if (visibleItems.length !== 0) {
          const lastVisibleItemDate = visibleItems
            .map(item => new Date(item.node.eventTime))
            .reduce((a, b) => a.getTime() > b.getTime() ? a : b)
          markChatEventsRead({
            variables: {
              chatId: id,
              userId: user.id,
              readTill: lastVisibleItemDate.toISOString()
            }
          }).catch(/* ignored */)
        }
      }}
      loadingInitial={loading && data?.chatById?.events == null}
      loadingMore={loading && data?.chatById?.events != null}
      hasMore={data?.chatById?.events?.pageInfo.hasPreviousPage ?? false}
      loadMore={() => loadMore(expectNonNil(data?.chatById?.events?.pageInfo.startCursor))}
      header={header}
      footer={footer}
    />
  )
}

function shouldBeGrouped (a: ChatEventDataFragment | undefined, b: ChatEventDataFragment | undefined): boolean {
  return a != null && b != null &&
    a.__typename === 'Message' && b.__typename === 'Message' &&
    a.author.id === b.author.id &&
    Math.abs(new Date(a.eventTime).getTime() - new Date(b.eventTime).getTime()) < 20000 // 20sec
}
