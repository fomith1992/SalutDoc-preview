import React from 'react'
import { SystemMessageContainer, SystemMessageSubtext, SystemMessageText } from '../system-message'

export type UserJoinViewProps = {
  firstName: string
  lastName: string
  specialization: string | null
}

export function UserJoinView (
  {
    firstName,
    lastName
  }: UserJoinViewProps
): React.ReactElement {
  // TODO specialization ?!
  return (
    <SystemMessageContainer>
      <SystemMessageSubtext>
        Подключен <SystemMessageText>{`${firstName} ${lastName}`}.</SystemMessageText>
      </SystemMessageSubtext>
    </SystemMessageContainer>
  )
}
