import React from 'react'
import { useUserJoinQuery } from '../../../gen/graphql'
import { UserJoinView } from './user-join.view'

export type UserJoinProps = {
  id: string
}

export function UserJoin (
  {
    id
  }: UserJoinProps
): React.ReactElement {
  const { data } = useUserJoinQuery({
    variables: { id }
  })

  if (data?.userJoinById == null) {
    return <></>
  }

  const {
    firstName,
    lastName,
    specialization
  } = data.userJoinById.user

  return (
    <UserJoinView
      firstName={firstName}
      lastName={lastName}
      specialization={specialization}
    />
  )
}
