import React from 'react'
import { View } from 'react-native'
import { Role } from '../../../gen/graphql'
import { SystemMessageContainer, SystemMessageText } from '../system-message'

interface ConsultationFinishedViewProps {
  site: Role
}

export function ConsultationFinishedView ({ site }: ConsultationFinishedViewProps): React.ReactElement {
  return (
    <View>
      <SystemMessageContainer>
        <SystemMessageText>
          Время вашей консультации подошло к концу.
        </SystemMessageText>
      </SystemMessageContainer>
      <SystemMessageContainer>
        <SystemMessageText>
          {site === 'DOCTOR'
            ? 'Чтобы завершить консультацию, cоздайте медзаключение, с рекомендациями для пациента.'
            : `Врач готовит ваше медзаключение ${'\n'} и скоро оно появится здесь.`}
        </SystemMessageText>
      </SystemMessageContainer>
    </View>
  )
}
