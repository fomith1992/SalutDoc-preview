import React from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useConsultationFinishedQuery } from '../../../gen/graphql'
import { ConsultationFinishedView } from './consultation-finished.view'

export type ConsultationFinishedProps = {
  id: string
}

export function ConsultationFinished (
  {
    id
  }: ConsultationFinishedProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const { data } = useConsultationFinishedQuery({
    variables: { id }
  })

  if (data?.consultationFinishedById == null) {
    return <></>
  }

  return (
    <ConsultationFinishedView
      site={user.role ?? 'USER'}
    />
  )
}
