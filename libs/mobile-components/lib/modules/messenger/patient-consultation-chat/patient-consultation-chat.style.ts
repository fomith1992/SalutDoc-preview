import { View } from 'react-native'
import { color } from '../../../style/color'
import { styled } from '../../../style/styled'

export const ChatContainer = styled(View, {
  backgroundColor: color('surface'),
  flex: 1
})
