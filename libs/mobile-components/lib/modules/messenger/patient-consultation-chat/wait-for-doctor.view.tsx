import React from 'react'
import {
  SystemMessageContainer,
  SystemMessageSubtext,
  SystemMessageText
} from '../system-message'

export function WaitForDoctorView (): React.ReactElement {
  return (
    <SystemMessageContainer>
      <SystemMessageSubtext>
        Найдем свободного врача и ответим вам <SystemMessageText>в течение 15 мин.</SystemMessageText> Ожидайте.
      </SystemMessageSubtext>
    </SystemMessageContainer>
  )
}
