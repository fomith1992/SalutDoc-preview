import React, { useEffect } from 'react'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { usePatientConsultationChatQuery } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { specializationName } from '../../consultations/i18n/specialization'
import { SpecializationIcon } from '../../consultations/specialization-icon'
import { useChatHeader } from '../../ui-kit/header'
import { useShowNotification } from '../../ui-kit/notifications'
import { AnamnesisCloud } from '../anamnesis-cloud'
import { ChatEvents } from '../chat-events'
import { MessageComposeBar } from '../message-compose-bar'
import { ChatContainer } from './patient-consultation-chat.style'
import { WaitForDoctorView } from './wait-for-doctor.view'

export type PatientConsultationChatProps = {
  consultationId: string
}

export function PatientConsultationChat (
  {
    consultationId
  }: PatientConsultationChatProps
): React.ReactElement {
  const { data, loading } = usePatientConsultationChatQuery({
    variables: { consultationId }
  })
  const showNotification = useShowNotification()
  const navigator = useStackNavigation()

  const doctor = data?.consultationById?.doctor
  const specialization = data?.consultationById?.specialization ?? null
  useChatHeader({
    chatData: {
      avatar: doctor?.avatar ?? null,
      avatarPlaceholder: props => <SpecializationIcon {...props} specialization={specialization} />,
      name: doctor != null ? `${doctor.firstName} ${doctor.lastName}` : 'Поиск врача',
      specialization: (specialization != null ? specializationName[specialization] : null) ?? specialization ?? '',
      onPress: doctor == null ? undefined : () => navigator.push('Profile', { userId: doctor.id })
    }
  })

  useEffect(() => {
    if (!loading && data?.consultationById?.chat == null) {
      showNotification({
        title: 'Ошибка',
        message: 'Консультация не найдена'
      })
      navigator.goBack()
    }
  }, [data, loading])

  if (loading || data?.consultationById?.chat == null) {
    return <ModalLoadingIndicator visible />
  }

  const chatId = data.consultationById.chat.id
  return (
    <ChatContainer>
      <ChatEvents
        footer={
          <>
            <AnamnesisCloud consultationId={consultationId} />
            {data.consultationById.startedAt != null ? null : <WaitForDoctorView />}
          </>
        }
        id={chatId}
      />
      {data.consultationById.startedAt != null && !data.consultationById.finished
        ? <MessageComposeBar chatId={chatId} />
        : null}
    </ChatContainer>
  )
}
