import React from 'react'
import { ChatEvents } from '../chat-events'
import { MessageComposeBar } from '../message-compose-bar'
import { ChatContainer } from './chat.style'

export type ChatProps = {
  id: string
}

export function Chat (
  {
    id
  }: ChatProps
): React.ReactElement {
  return (
    <ChatContainer>
      <ChatEvents id={id} />
      <MessageComposeBar chatId={id} />
    </ChatContainer>
  )
}
