import { Text, View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { font } from '../../../style/text'

export const SystemMessageContainer = styled(View, {
  marginTop: sp(16),
  marginHorizontal: sp(48),
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
})

export const SystemMessageText = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('text'),
  textAlign: 'center'
})

export const SystemMessageSubtext = styled(Text, {
  ...font({ type: 'caption' }),
  color: color('subtext')
})
