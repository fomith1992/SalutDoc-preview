import React from 'react'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useConsultationReportAddedQuery } from '../../../gen/graphql'
import { MessageCloud } from '../message/message-cloud.view'

export type ConsultationReportAddedProps = {
  id: string
}

export function ConsultationReportAdded (
  {
    id
  }: ConsultationReportAddedProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const { data } = useConsultationReportAddedQuery({
    variables: { id }
  })

  const event = data?.consultationReportAddedById
  const { doctor, report } = event?.chat.consultation ?? {}

  if (event == null || report == null || doctor == null) {
    return <></>
  }

  return (
    (
      <MessageCloud
        isMy={doctor.id === user.id}
        time={new Date(event.eventTime)}
        avatar={doctor.avatar ?? undefined}
        isBottomMessageInGroup
        isTopMessageInGroup
        sending={false}
        content={{
          attachments: [],
          text: `Наиболее вероятное заболевание
${report.diagnosis}

Рекомендуемая диагностика
${report.diagnostics}

Рекомендуемые врачи
${report.doctors}

Рекомендации по лекарствам
${report.medicines}

Общие рекомендации
${report.recommendation}`
        }}
      />
    )
  )
}
