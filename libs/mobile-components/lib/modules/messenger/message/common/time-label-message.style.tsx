import { View, Text } from 'react-native'
import { color } from '../../../../style/color'
import { sp } from '../../../../style/size'
import { mapStyle, styled, variants } from '../../../../style/styled'
import { font } from '../../../../style/text'
import { pipe } from 'fp-ts/lib/function'
import Color from 'color'

const TimeLableTypeVariants = styled(View, variants('type', {
  default: {},
  shadow: {
    backgroundColor: pipe(color('text'), mapStyle(c => Color(c).alpha(0.5).string())),
    borderRadius: sp(8)
  }
}))

export const TimeLableContainer = styled(TimeLableTypeVariants, {
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: sp(8)
})

const TimeTextVariants = styled(Text, variants('type', {
  default: {
    color: color('subtext')
  },
  shadow: {
    color: color('surface')
  }
}))

export const TimeText = styled(TimeTextVariants, {
  ...font({ type: 'caption' })
})

export const SendingMessageContainer = styled(View, {
  paddingLeft: sp(4)
})
