import moment from 'moment'
import React from 'react'
import { LoadingIndicator } from '../../../../components/loading-indicator/loading-indicator.view'
import { SendingMessageContainer, TimeLableContainer, TimeText } from './time-label-message.style'

interface TimeLabelMessageProps {
  time: Date
  sendingMessage: boolean
  type?: 'default' | 'shadow'
}

export function TimeLabelMessage (
  {
    time,
    sendingMessage,
    type = 'default'
  }: TimeLabelMessageProps
): React.ReactElement {
  return (
    <TimeLableContainer
      type={type}
    >
      <TimeText
        type={type}
      >
        {moment(time).format('LT')}
      </TimeText>
      {sendingMessage
        ? (
          <SendingMessageContainer>
            <LoadingIndicator size='small' visible />
          </SendingMessageContainer>
        )
        : null}
    </TimeLableContainer>
  )
}
