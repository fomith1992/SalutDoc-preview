import React from 'react'
import { MessageText } from './text-message.style'

export interface TextMessageProps {
  text: string
}

export function TextMessage (props: {isMy: boolean, text: string}): React.ReactElement {
  const { isMy, text } = props
  return (
    <MessageText
      colored={isMy}
      row={text.length < 20}
    >
      {text}
    </MessageText>
  )
}
