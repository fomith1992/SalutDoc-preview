import Color from 'color'
import { pipe } from 'fp-ts/lib/function'
import { View } from 'react-native'
import { color } from '../../../../style/color'
import { sp } from '../../../../style/size'
import { mapStyle, styled } from '../../../../style/styled'

export const ImageMessageContainer = styled(View, {
  position: 'relative'
})

export const TimeLabelContainer = styled(View, {
  position: 'absolute',
  bottom: sp(16),
  right: sp(16)
})

export const ImageContainer = styled(View, {
  overflow: 'hidden',
  borderRadius: sp(16)
})

export const LoadingOverlayContainer = styled(View, {
  position: 'absolute',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100%',
  width: '100%',
  borderRadius: sp(16),
  backgroundColor: pipe(color('text'), mapStyle(c => Color(c).alpha(0.5).string()))
})
