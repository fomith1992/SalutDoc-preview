import React, { useState } from 'react'
import {
  ImageMessageContainer,
  TimeLabelContainer,
  LoadingOverlayContainer,
  ImageContainer
} from './image-message.style'
import { LoadingIndicator } from '../../../../components/loading-indicator/loading-indicator.view'
import { ImageResizeView } from '../../../../components/image-resize/image-resize.view'

export interface ImageMessageProps {
  url: string
}

export function ImageMessage (
  props: {
    image: ImageMessageProps
    timeLabelComponent: React.ReactElement
  }): React.ReactElement {
  const [loading, setLoading] = useState(true)
  const { image, timeLabelComponent } = props
  return (
    <ImageMessageContainer>
      <ImageContainer>
        <ImageResizeView
          uri={image.url ?? undefined}
          onLoadingState={setLoading}
        />
      </ImageContainer>
      <TimeLabelContainer>
        {timeLabelComponent}
      </TimeLabelContainer>
      {loading ? (
        <LoadingOverlayContainer>
          <LoadingIndicator size='normal' visible colorType='inactive' />
        </LoadingOverlayContainer>
      ) : null}
    </ImageMessageContainer>
  )
}
