import React from 'react'
import { LoadingIndicator } from '../../../../../components/loading-indicator/loading-indicator.view'
import {
  FileMessageContainer,
  DocumentImage,
  FileMessageText
} from './file-message.style'

export interface FileMessageProps {
  name: string
  loading: boolean
  onPressFile: () => void
}

export function FileMessageView (props: {isMy: boolean, file: FileMessageProps}): React.ReactElement {
  const { isMy, file } = props
  return (
    <FileMessageContainer onPress={() => file.onPressFile()}>
      {file.loading
        ? <LoadingIndicator size='normal' visible />
        : <DocumentImage colored={isMy} />}
      <FileMessageText
        numberOfLines={1}
        colored={isMy}
      >
        {file.name}
      </FileMessageText>
    </FileMessageContainer>
  )
}
