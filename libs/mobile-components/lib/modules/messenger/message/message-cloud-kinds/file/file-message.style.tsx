import { Text, TouchableOpacity } from 'react-native'
import { DocumentIcon20 } from '../../../../../images/document.icon-20'
import { color } from '../../../../../style/color'
import { sp } from '../../../../../style/size'
import { styled } from '../../../../../style/styled'
import { font } from '../../../../../style/text'

export const FileMessageContainer = styled(TouchableOpacity, {
  flex: 1,
  flexDirection: 'row'
})

export const FileMessageText = styled(Text, (
  props: {
    colored?: boolean
  }) => ({
  ...font({ type: 'text1' }),
  color: color(props.colored ?? false ? 'accentDark' : 'text'),
  marginLeft: sp(8)
}) as const)

export const DocumentImage = styled(DocumentIcon20, (props: {colored?: boolean}) => ({
  color: props.colored ?? false ? color('accentDark') : undefined
}) as const)
