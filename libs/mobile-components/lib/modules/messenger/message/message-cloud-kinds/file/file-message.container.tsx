import React from 'react'
import { Alert } from 'react-native'
import RNFetchBlob from 'rn-fetch-blob'
import { useFileQuery } from '../../../../../gen/graphql'
import { useCredentialsRef } from '../../../../oidc/credentials-provider'
import { useShowNotification } from '../../../../ui-kit'
import { FileMessageView } from './file-message.view'

function handlePressFile (url: string, token: string, filename: string): void {
  RNFetchBlob
    .config({
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        title: filename,
        mediaScannable: true
      }
    })
    .fetch('GET', url, {
      Authorization: token
    })
    .then((res) => {
      const status = res.info().status

      if (status === 200) {
        // TODO: open file in app
      } else {
        Alert.alert('Статус не 200', status.toString())
      }
    })
    .catch((errorMessage) => {
      console.log(JSON.stringify(errorMessage))
    })
}

export interface FileMessageProps {
  id: string
  url: string
}

export function FileMessage (props: {isMy: boolean, fileInfo: FileMessageProps}): React.ReactElement {
  const { isMy, fileInfo } = props
  const { id, url } = fileInfo
  const result = useFileQuery({
    variables: { id }
  })
  const credentialsRef = useCredentialsRef()
  const file = result.data?.fileById
  const showNotification = useShowNotification()

  return (
    <FileMessageView
      isMy={isMy}
      file={{
        name: file?.id ?? 'Загружаем...', // TODO: SD-1408
        loading: result.loading,
        onPressFile: () => {
          showNotification({
            title: 'Загружаем файл',
            message: 'По окончании придет уведомление'
          })
          handlePressFile(url, credentialsRef.current?.accessToken ?? '', 'Название файла')
        }
      }}
    />
  )
}
