import { View, Text } from 'react-native'
import { color } from '../../../../style/color'
import { sp } from '../../../../style/size'
import { styled } from '../../../../style/styled'
import { font } from '../../../../style/text'

export const MessageText = styled(Text, (
  props: {
    row: boolean
    colored?: boolean
  }) => ({
  ...font({ type: 'text1' }),
  color: color(props.colored ?? false ? 'accentDark' : 'text'),
  alignSelf: props.row ? undefined : 'stretch'
}) as const)

export const SendingContainer = styled(View, {
  padding: sp(4)
})
