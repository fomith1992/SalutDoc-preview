import { View } from 'react-native'
import { color } from '../../../style/color'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'
import { container } from '../../../style/view'

export const MessageContainer = styled(View, (props: {
  isMy: boolean
  isTopMessageInGroup: boolean
}) => ({
  ...container(),
  alignSelf: props.isMy ? 'flex-end' : 'flex-start',
  flexDirection: 'row',
  marginTop: sp(props.isTopMessageInGroup ? 16 : 8)
} as const))

export const MessageContent = styled(View, (props: {
  isSingleRow: boolean
  isTopMessageInGroup: boolean
  isBottomMessageInGroup: boolean
  isMy: boolean
  isOnlyImage: boolean
}) => ({
  maxWidth: '75%',
  padding: sp(props.isOnlyImage ? 4 : 16),
  flexDirection: props.isSingleRow ? 'row' : undefined,
  justifyContent: props.isSingleRow ? 'space-between' : undefined,
  ...(props.isMy ? {
    backgroundColor: color('secondaryLight'),
    borderBottomLeftRadius: sp(16),
    borderTopLeftRadius: sp(16),
    borderBottomRightRadius: sp(props.isBottomMessageInGroup ? 16 : 4),
    borderTopRightRadius: sp(props.isTopMessageInGroup ? 16 : 4)
  } : {
    backgroundColor: color('background'),
    borderBottomRightRadius: sp(16),
    borderTopRightRadius: sp(16),
    borderBottomLeftRadius: sp(props.isBottomMessageInGroup ? 16 : 4),
    borderTopLeftRadius: sp(props.isTopMessageInGroup ? 16 : 4)
  })
}) as const)

export const AvatarContainer = styled(View, {
  alignSelf: 'flex-end',
  width: sp(32),
  height: sp(32),
  marginRight: sp(16)
})

export const TimeContainer = styled(View, {
  paddingLeft: sp(8),
  alignSelf: 'flex-end'
})

export const Spring = styled(View, {
  height: sp(4)
})
