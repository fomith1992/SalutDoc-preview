import React from 'react'
import { Avatar } from '../../../components/avatar/avatar.view'
import { TimeLabelMessage } from './common/time-label-message.view'
import { FileMessage, FileMessageProps } from './message-cloud-kinds/file/file-message.container'
import { ImageMessage, ImageMessageProps } from './message-cloud-kinds/image-message.view'
import { TextMessage } from './message-cloud-kinds/text-message.view'
import { AvatarContainer, MessageContainer, MessageContent, Spring, TimeContainer } from './message-cloud.style'

type MessageAttachmentProps = FileMessageProps & { type: 'file' } | ImageMessageProps & { type: 'image' }

interface MessageContentProps {
  text: string | null
  attachments: MessageAttachmentProps[]
}

interface MessageCloudProps {
  isMy: boolean
  avatar?: string
  time: Date
  content: MessageContentProps
  sending: boolean

  isTopMessageInGroup: boolean
  isBottomMessageInGroup: boolean
}

function isRowCalculate (content: MessageContentProps): boolean {
  if (content.text != null && content.attachments.length > 0) return false
  if (content.text != null && content.text.length > 20) return false
  return true
}

function isOnlyImage (content: MessageContentProps): boolean {
  return content.text == null && content.attachments.length === 1 && content.attachments[0]?.type === 'image'
}

function MessageAttachmentView (
  props: {
    attachment: MessageAttachmentProps
    isMy: boolean
    time: Date
    sending: boolean
    isOnlyImage: boolean
  }): React.ReactElement {
  const { attachment, isMy, time, sending, isOnlyImage } = props
  switch (attachment.type) {
    case 'file':
      return (
        <FileMessage
          isMy={isMy}
          fileInfo={{
            id: attachment.id,
            url: attachment.url
          }}
        />
      )
    case 'image':
      return (
        <ImageMessage
          image={attachment}
          timeLabelComponent={
            isOnlyImage ? (
              <TimeLabelMessage
                type='shadow'
                time={time}
                sendingMessage={sending}
              />
            ) : <></>
          }
        />
      )
  }
}

export function MessageCloud (
  {
    isMy,
    avatar,
    content,
    sending,
    time,
    isTopMessageInGroup,
    isBottomMessageInGroup
  }: MessageCloudProps
): React.ReactElement {
  const onlyImageInMessage = isOnlyImage(content)
  const isRowDisplay = isRowCalculate(content)
  return (
    <MessageContainer
      isMy={isMy}
      isTopMessageInGroup={isTopMessageInGroup}
    >
      {
        isMy ? null : (
          <AvatarContainer>
            {isBottomMessageInGroup
              ? <Avatar shape='circle' url={avatar} />
              : null}
          </AvatarContainer>
        )
      }
      <MessageContent
        isMy={isMy}
        isOnlyImage={onlyImageInMessage}
        isSingleRow={isRowDisplay}
        isTopMessageInGroup={isTopMessageInGroup}
        isBottomMessageInGroup={isBottomMessageInGroup}
      >
        {content.text == null ? null : (
          <TextMessage
            isMy={isMy}
            text={content.text}
          />
        )}
        {content.attachments.map(attachment => (
          <>
            {isRowDisplay ? null : <Spring />}
            <MessageAttachmentView
              attachment={attachment}
              isMy={isMy}
              time={time}
              sending={sending}
              isOnlyImage={onlyImageInMessage}
            />
          </>
        ))}
        {isRowDisplay ? null : <Spring />}
        {onlyImageInMessage ? null : (
          <TimeContainer>
            <TimeLabelMessage
              time={time}
              sendingMessage={sending}
            />
          </TimeContainer>
        )}
      </MessageContent>
    </MessageContainer>
  )
}
