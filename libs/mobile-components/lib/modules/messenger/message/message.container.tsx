import React from 'react'
import { Text } from 'react-native'
import { MessageCloud } from './message-cloud.view'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useMessageQuery } from '../../../gen/graphql'

export type MessageProps = {
  id: string

  isTopMessageInGroup?: boolean
  isBottomMessageInGroup?: boolean
}

export function Message (
  {
    id,
    isTopMessageInGroup = true,
    isBottomMessageInGroup = true
  }: MessageProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const { data, loading } = useMessageQuery({
    variables: { id }
  })

  if (loading || data?.messageById == null) {
    return <Text>MESSAGE</Text>
  }

  const {
    author,
    text,
    eventTime,
    sending
  } = data.messageById

  return (
    <MessageCloud
      avatar={author.avatar ?? undefined}
      isMy={author.id === user.id}
      content={{
        text: text,
        attachments: []
      }}
      sending={sending}
      time={new Date(eventTime)}
      isTopMessageInGroup={isTopMessageInGroup}
      isBottomMessageInGroup={isBottomMessageInGroup}
    />
  )
}
