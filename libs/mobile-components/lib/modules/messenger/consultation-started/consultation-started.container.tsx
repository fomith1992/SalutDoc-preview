import moment from 'moment'
import React from 'react'
import { useConsultationStartedQuery } from '../../../gen/graphql'
import { consultationDuration } from '../../config'
import { ConsultationStartedView } from './consultation-started.view'

export type ConsultationStartedProps = {
  id: string
}

export function ConsultationStarted (
  {
    id
  }: ConsultationStartedProps
): React.ReactElement {
  const { data } = useConsultationStartedQuery({
    variables: { id }
  })

  if (data?.consultationStartedById == null) {
    return <></>
  }

  return (
    <ConsultationStartedView
      consultationEndsAt={moment(data.consultationStartedById.chat.consultation.startedAt).add(consultationDuration, 'm').toDate()}
    />
  )
}
