import moment from 'moment'
import React from 'react'
import { consultationDuration } from '../../config'
import { SystemMessageContainer, SystemMessageSubtext, SystemMessageText } from '../system-message'

export type ConsultationStartedViewProps = {
  consultationEndsAt: Date
}

export function ConsultationStartedView (
  {
    consultationEndsAt
  }: ConsultationStartedViewProps
): React.ReactElement {
  return (
    <SystemMessageContainer>
      <SystemMessageSubtext>
        Длительность консультации {consultationDuration} минут.
      </SystemMessageSubtext>
      <SystemMessageText>
        Время окончания консультации: {moment(consultationEndsAt).format('HH:mm')}
      </SystemMessageText>
    </SystemMessageContainer>
  )
}
