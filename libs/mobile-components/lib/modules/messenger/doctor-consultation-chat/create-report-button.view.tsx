import React from 'react'
import { TextButton } from '../../ui-kit/buttons'
import { CreateReportButtonContainer } from './create-report-button.style'

export type CreateReportButtonViewProps = {
  onPress: () => void
}

export function CreateReportButtonView (
  {
    onPress
  }: CreateReportButtonViewProps
): React.ReactElement {
  return (
    <CreateReportButtonContainer>
      <TextButton
        size='L'
        type='primary'
        onPress={onPress}
      >
        Создать медзаключение
      </TextButton>
    </CreateReportButtonContainer>
  )
}
