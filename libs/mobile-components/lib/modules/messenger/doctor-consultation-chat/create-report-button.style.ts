import { View } from 'react-native'
import { sp } from '../../../style/size'
import { styled } from '../../../style/styled'

export const CreateReportButtonContainer = styled(View, {
  marginTop: sp(24),
  alignItems: 'center'
})
