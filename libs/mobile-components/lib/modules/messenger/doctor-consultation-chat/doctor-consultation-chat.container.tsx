import React, { useEffect } from 'react'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useDoctorConsultationChatQuery } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { consultationToDoctor } from '../../consultations/i18n/specialization'
import { SpecializationIcon } from '../../consultations/specialization-icon'
import { useChatHeader } from '../../ui-kit/header'
import { useShowNotification } from '../../ui-kit/notifications'
import { AnamnesisCloud } from '../anamnesis-cloud'
import { ChatEvents } from '../chat-events'
import { MessageComposeBar } from '../message-compose-bar'
import { CreateReportButtonView } from './create-report-button.view'
import { ChatContainer } from './doctor-consultation-chat.style'

export type DoctorConsultationChatProps = {
  consultationId: string
}

export function DoctorConsultationChat (
  {
    consultationId
  }: DoctorConsultationChatProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const showNotification = useShowNotification()
  const navigator = useStackNavigation()

  const { data, loading } = useDoctorConsultationChatQuery({
    variables: { consultationId }
  })

  const patient = data?.consultationById?.patient
  const specialization = data?.consultationById?.specialization ?? null
  useChatHeader({
    chatData: {
      avatar: patient?.avatar ?? null,
      avatarPlaceholder: props => <SpecializationIcon {...props} specialization={specialization} />,
      name: patient != null ? `${patient.firstName} ${patient.lastName}` : '',
      specialization: (specialization != null ? consultationToDoctor[specialization] : null) ?? specialization ?? '',
      onPress: patient == null ? undefined : () => navigator.push('Profile', { userId: patient.id })
    }
  })

  useEffect(() => {
    if (!loading) {
      if (data?.consultationById?.chat == null) {
        showNotification({
          title: 'Ошибка',
          message: 'Консультация не найдена'
        })
        navigator.goBack()
        return
      }
      if (data?.consultationById?.doctor?.id !== user.id && user.role === 'DOCTOR') {
        showNotification({
          title: 'Ошибка',
          message: 'Консультация проводится другим врачом'
        })
        navigator.goBack()
      }
    }
  }, [data, loading])

  if (loading || data?.consultationById?.chat == null) {
    return <ModalLoadingIndicator visible />
  }

  const chatId = data.consultationById.chat.id
  return (
    <ChatContainer>
      <ChatEvents
        id={chatId}
        header={data.consultationById.finished && data.consultationById.report == null
          ? <CreateReportButtonView onPress={() => navigator.push('CreateMedicalReport', { consultationId })} />
          : undefined}
        footer={<AnamnesisCloud consultationId={consultationId} />}
      />
      {data.consultationById.startedAt != null && !data.consultationById.finished
        ? <MessageComposeBar chatId={chatId} />
        : null}
    </ChatContainer>
  )
}
