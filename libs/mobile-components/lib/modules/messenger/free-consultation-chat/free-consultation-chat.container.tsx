import React, { useEffect } from 'react'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { useAuthenticatedUser } from '../../../components/user-context/user-context-provider'
import { useConsultationStartMutation, useFreeConsultationChatQuery } from '../../../gen/graphql'
import { useStackNavigation } from '../../../navigation/use-stack-navigation'
import { newDoctorSiteConsultation } from '../../consultations/cache/new-doctor-site-consultation'
import { consultationToDoctor } from '../../consultations/i18n/specialization'
import { SpecializationIcon } from '../../consultations/specialization-icon'
import { useChatHeader } from '../../ui-kit/header'
import { useShowNotification } from '../../ui-kit/notifications'
import { AnamnesisCloud } from '../anamnesis-cloud'
import { FreeConsultationView } from './free-consultation-chat.view'

export type FreeConsultationChatProps = {
  consultationId: string
}

export function FreeConsultationChat (
  {
    consultationId
  }: FreeConsultationChatProps
): React.ReactElement {
  const user = useAuthenticatedUser()
  const showNotification = useShowNotification()
  const navigator = useStackNavigation()

  const { data, loading } = useFreeConsultationChatQuery({
    variables: { consultationId },
    pollInterval: 1000
  })

  const [startConsultation, { loading: consultationStarting }] = useConsultationStartMutation({
    variables: {
      consultationId,
      doctorId: user.id
    },
    onCompleted: ({ consultationAddDoctorAndStart: { errors, message: dbgMsg, consultation } }) => {
      console.log(dbgMsg, errors, consultation)
      if (consultation == null) {
        showNotification({
          title: 'Ошибка',
          message: 'Не удалось начать консультацию'
        })
      } else {
        console.log('Консультация успешно начата!')
      }
    },
    update: (cache, { data }) => {
      const consultation = data?.consultationAddDoctorAndStart.consultation
      if (consultation == null || consultation.doctor?.id == null) {
        return
      }
      newDoctorSiteConsultation(cache, consultation.doctor.id, consultation)
    },
    onError: () => {
      showNotification({
        title: 'Ошибка',
        message: 'Не удалось начать консультацию'
      })
    }
  })

  const patient = data?.consultationById?.patient
  const specialization = data?.consultationById?.specialization ?? null
  useChatHeader({
    chatData: {
      avatar: patient?.avatar ?? null,
      avatarPlaceholder: props => <SpecializationIcon {...props} specialization={specialization} />,
      name: patient != null ? `${patient.firstName} ${patient.lastName}` : '',
      specialization: (specialization != null ? consultationToDoctor[specialization] : null) ?? specialization ?? '',
      onPress: patient == null ? undefined : () => navigator.push('Profile', { userId: patient.id })
    }
  })

  useEffect(() => {
    if (!loading && data?.consultationById == null) {
      showNotification({
        title: 'Ошибка',
        message: 'Консультация не найдена'
      })
      navigator.goBack()
      return
    }
    if (data?.consultationById?.doctor != null && data.consultationById.doctor.id !== user.id) {
      showNotification({
        title: 'Ошибка',
        message: 'Консультация проводится другим врачом'
      })
      navigator.goBack()
      return
    }
    if (data?.consultationById?.doctor?.id === user.id && data.consultationById.chat != null) {
      navigator.replace('DoctorConsultationChat', { consultationId })
    }
  }, [data, loading])

  if (loading || data?.consultationById == null) {
    return <ModalLoadingIndicator visible />
  }

  return (
    <FreeConsultationView
      anamnesisBlock={<AnamnesisCloud consultationId={consultationId} />}
      loading={consultationStarting || data.consultationById.doctor?.id === user.id}
      onStartConsultation={() => {
        startConsultation().catch(/* handled in hook */)
      }}
    />
  )
}
