import { ScrollView, View } from 'react-native'
import { sp } from '../../../style/size'
import { color } from '../../../style/color'
import { lift, styled } from '../../../style/styled'

export const ChatContainer = styled(ScrollView, {
  backgroundColor: color('surface'),
  flex: 1
})

export const StartConsultationButtonContainer = styled(View, {
  marginVertical: sp(24),
  alignItems: 'center'
})

export const Spring = styled(View, {
  flex: 1
})

export const contentViewStyleSheet = lift({
  flexGrow: 1
})
