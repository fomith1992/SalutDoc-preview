import React from 'react'
import { ModalLoadingIndicator } from '../../../components/loading-indicator/modal-loading-indicator.view'
import { useMaterialized } from '../../../style/styled'
import { TextButton } from '../../ui-kit/buttons'
import {
  ChatContainer,
  StartConsultationButtonContainer,
  Spring,
  contentViewStyleSheet
} from './free-consultation-chat.style'

export type FreeConsultationViewProps = {
  anamnesisBlock?: React.ReactNode
  onStartConsultation?: () => void
  loading: boolean
}

export function FreeConsultationView (
  {
    anamnesisBlock,
    onStartConsultation,
    loading
  }: FreeConsultationViewProps
): React.ReactElement {
  const contentViewStyle = useMaterialized(contentViewStyleSheet)
  return (
    <ChatContainer
      contentContainerStyle={contentViewStyle}
    >
      <Spring />
      {anamnesisBlock}
      <StartConsultationButtonContainer>
        <TextButton
          size='L'
          type='primary'
          onPress={onStartConsultation}
        >
          Начать консультацию
        </TextButton>
      </StartConsultationButtonContainer>
      <ModalLoadingIndicator visible={loading} />
    </ChatContainer>
  )
}
