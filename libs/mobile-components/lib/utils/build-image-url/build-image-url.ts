import URI from 'urijs'

type FilterProps = 'bw'

export function buildImageUrl (url?: string | null, filter?: FilterProps): string | undefined {
  if (url == null) return undefined
  const customUri = new URI(url)
  if (filter != null) customUri.addSearch({ filter })
  return customUri.href()
}
