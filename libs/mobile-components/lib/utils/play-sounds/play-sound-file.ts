import { LocalFileType, localSoundMap } from './local-sound-file.map'

export function playLocalSoundFile (type: LocalFileType): void {
  const sound = localSoundMap[type]
  if (sound == null) {
    console.log('failed to load the sound')
    return
  }
  sound.play((success) => {
    if (success) {
      console.log('successfully finished playing')
    } else {
      console.log('playback failed due to audio decoding errors')
      sound.reset()
    }
  })
}
