import Sound from 'react-native-sound'

export type LocalFileType = 'notifications'

/* Destination
  * Android: android/app/src/main/res/raw
  * IOS: add by XCode
  * link: https://www.npmjs.com/package/react-native-sound#basic-usage
  * */
export const localSoundMap: Record<LocalFileType, Sound | undefined> = {
  notifications: undefined
}

export function initializeSounds (): void {
  localSoundMap.notifications = new Sound('notifications.wav', Sound.MAIN_BUNDLE, (error) => {
    if (error != null) {
      console.log('failed to load notification sound', error)
    }
  })
}
