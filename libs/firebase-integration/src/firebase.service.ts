import { Inject, Injectable } from '@nestjs/common'
import { ConfigType } from '@nestjs/config'
import { Logger } from '@salutdoc/nest-logging'
import { NotificationDataJson } from '@salutdoc/push-notification-schema'
import * as admin from 'firebase-admin'
import { ServiceAccount } from 'firebase-admin'
import { firebaseConfig } from './firebase.config'
import * as maybe from '@salutdoc/utils/lib/fp/maybe'
import * as either from 'fp-ts/lib/Either'
import { Either } from 'fp-ts/lib/Either'

type TopicName = 'doctors.consultation-start'

@Injectable()
export class FirebaseService {
  private readonly app: admin.app.App

  constructor (
    private readonly logger: Logger,
    @Inject(firebaseConfig.KEY) config: ConfigType<typeof firebaseConfig>
  ) {
    this.app = admin.initializeApp({
      credential: admin.credential.cert(config.FBSK as ServiceAccount)
    })
    this.logger.setContext('FirebaseService')
  }

  async sendMessageToDevice (
    tokens: string[],
    notification: admin.messaging.Notification,
    data?: NotificationDataJson
  ): Promise<number> {
    if (tokens.length === 0) return 0
    this.logger.debug('Sending push notification', { tokens, notification, data })
    const responseSendMulticast = await this.app.messaging().sendMulticast({
      tokens,
      notification,
      data
    })
    const { responses, successCount, failureCount } = responseSendMulticast
    const errors = responses.map(res => res.error).filter(maybe.isNonNil)
    this.logger.log(errors.length === 0 ? 'debug' : 'warn', 'Push notification sent', {
      successCount,
      failureCount,
      messageIds: responses.map(res => res.messageId)
    }, ...errors)
    return successCount
  }

  async sendMessageToTopic (
    topic: TopicName,
    notification: admin.messaging.NotificationMessagePayload,
    data?: NotificationDataJson
  ): Promise<void> {
    const { messageId } = await this.app.messaging().sendToTopic(
      topic,
      {
        notification,
        data
      }
    )
    this.logger.debug('Push notification sent to topic', {
      topic,
      notification,
      data,
      messageId
    })
  }

  async subscribeToTopic (
    registrationTokens: string[],
    topic: TopicName
  ): Promise<number> {
    if (registrationTokens.length === 0) return 0
    const response = await this.app.messaging().subscribeToTopic(registrationTokens, topic)
    if (response.failureCount > 0) {
      for (const error of response.errors) {
        var invalidToken = registrationTokens[error.index]
        this.logger.warn(
          `Failed subscribe "${invalidToken ?? ''}" token to ${topic} with error `, error.error
        )
      }
    }
    return response.successCount
  }

  async unsubscribeFromTopic (
    registrationTokens: string[],
    topic: TopicName
  ): Promise<number> {
    if (registrationTokens.length === 0) return 0
    const response = await this.app.messaging().unsubscribeFromTopic(registrationTokens, topic)
    if (response.failureCount > 0) {
      for (const error of response.errors) {
        var invalidToken = registrationTokens[error.index]
        this.logger.warn(
          `Failed unsubscribe "${invalidToken ?? ''}" token from ${topic} with error `, error.error
        )
      }
    }
    return response.successCount
  }

  async verifyCode (
    verificationCode: string
  ): Promise<Either<'INVALID_CODE', { phoneNumber: string | null }>> {
    return await this.app.auth().verifyIdToken(verificationCode)
      .then((decodedToken) => {
        return either.right({ phoneNumber: decodedToken.phone_number ?? null })
      })
      .catch(() => {
        return either.left('INVALID_CODE' as const)
      })
  }
}
