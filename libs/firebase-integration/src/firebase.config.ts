import { registerAs } from '@nestjs/config'
import * as t from '@salutdoc/utils/lib/transform'
import { parseJSON, toError } from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/function'

const firebaseSchema = t.type({
  FBSK: pipe(
    t.string,
    t.chain(s => parseJSON(s, e => t.error(toError(e).message)))
  )
})

export const firebaseConfig = registerAs('firebase-config',
  async () => t.transformOrThrow(firebaseSchema)(process.env)
)
