declare module 'recombee-js-api-client' {
  export interface ApiClientOptions {
    baseUri?: string // process.env.RAPI_URI || baseUri || 'rapi.recombee.com'
    useHttps?: boolean // default true
    async?: boolean // default true
  }

  interface Request<R> {
    __responseType?: R
  }

  export class ApiClient {
    constructor (databaseId: string, publicToken: string, options?: ApiClientOptions)

    send<R> (request: Request<R>): Promise<R>
    send<R> (request: Request<R>, callback: (error: Error | null, response?: R) => void): void
  }

  export interface AddDetailViewOptions {
    /**
     * Description: UTC timestamp of the view as ISO8601-1 pattern or UTC epoch time. The default value is the current time.
     */
    timestamp?: string | number
    /**
     * Duration of the view
     */
    duration?: number
    /**
     * Sets whether the given user/item should be created if not present in the database.
     */
    cascadeCreate?: boolean
    /**
     * If this detail view is based on a recommendation request, `recommId` is the id of the clicked recommendation.
     */
    recommId?: string
    /**
     * A dictionary of additional data for the interaction.
     */
    additionalData?: object
  }

  /**
   * Adds a detail view of a given item made by a given user.
   */
  export class AddDetailView implements Request<unknown> {
    /**
     * Construct the request
     * @param {string} userId - User who viewed the item
     * @param {string} itemId - Viewed item
     * @param {Object} optional - Optional parameters given as an object with structure name of the parameter: value
     * - Allowed parameters:
     *     - *timestamp*
     *         - Type: string|number
     *         - Description: UTC timestamp of the view as ISO8601-1 pattern or UTC epoch time. The default value is the current time.
     *     - *duration*
     *         - Type: number
     *         - Description: Duration of the view
     *     - *cascadeCreate*
     *         - Type: boolean
     *         - Description: Sets whether the given user/item should be created if not present in the database.
     *     - *recommId*
     *         - Type: string
     *         - Description: If this detail view is based on a recommendation request, `recommId` is the id of the clicked recommendation.
     *     - *additionalData*
     *         - Type:
     *         - Description: A dictionary of additional data for the interaction.
     */
    constructor (userId: string, itemId: string, optional?: AddDetailViewOptions)
  }

  export interface SearchItemsOptions {
    /**
     * Scenario defines a particular search field in your user interface.
     * You can set various settings to the [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com). You can also see performance of each scenario in the Admin UI separately, so you can check how well each field performs.
     * The AI which optimizes models in order to get the best results may optimize different scenarios separately, or even use different models in each of the scenarios.
     */
    scenario?: string
    /**
     * If the user does not exist in the database, returns a list of non-personalized search results and creates the user in the database. This allows for example rotations in the following recommendations for that user, as the user will be already known to the system.
     */
    cascadeCreate?: boolean
    /**
     * With `returnProperties=true`, property values of the recommended items are returned along with their IDs in a JSON dictionary. The acquired property values can be used for easy displaying of the recommended items to the user.
     * Example response:
     * ```
     *   {
     *     "recommId": "ce52ada4-e4d9-4885-943c-407db2dee837",
     *     "recomms":
     *       [
     *         {
     *           "id": "tv-178",
     *           "values": {
     *             "description": "4K TV with 3D feature",
     *             "categories":   ["Electronics", "Televisions"],
     *             "price": 342,
     *             "url": "myshop.com/tv-178"
     *           }
     *         },
     *         {
     *           "id": "mixer-42",
     *           "values": {
     *             "description": "Stainless Steel Mixer",
     *             "categories":   ["Home & Kitchen"],
     *             "price": 39,
     *             "url": "myshop.com/mixer-42"
     *           }
     *         }
     *       ]
     *   }
     * ```
     */
    returnProperties?: boolean
    /**
     * Allows to specify, which properties should be returned when `returnProperties=true` is set. The properties are given as a comma-separated list.
     * Example response for `includedProperties=description,price`:
     * ```
     *   {
     *     "recommId": "a86ee8d5-cd8e-46d1-886c-8b3771d0520b",
     *     "recomms":
     *       [
     *         {
     *           "id": "tv-178",
     *           "values": {
     *             "description": "4K TV with 3D feature",
     *             "price": 342
     *           }
     *         },
     *         {
     *           "id": "mixer-42",
     *           "values": {
     *             "description": "Stainless Steel Mixer",
     *             "price": 39
     *           }
     *         }
     *       ]
     *   }
     * ```
     */
    includedProperties?: string[]
    /**
     * Boolean-returning [ReQL](https://docs.recombee.com/reql.html) expression which allows you to filter recommended items based on the values of their attributes.
     * Filters can be also assigned to a [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com).
     */
    filter?: string
    /**
     * Number-returning [ReQL](https://docs.recombee.com/reql.html) expression which allows you to boost recommendation rate of some items based on the values of their attributes.
     * Boosters can be also assigned to a [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com).
     */
    booster?: string
    /**
     * Logic specifies particular behavior of the recommendation models. You can pick tailored logic for your domain and use case.
     * See [this section](https://docs.recombee.com/recommendation_logics.html) for list of available logics and other details.
     * The difference between `logic` and `scenario` is that `logic` specifies mainly behavior, while `scenario` specifies the place where recommendations are shown to the users.
     * Logic can be also set to a [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com).
     */
    logic?: string
    /**
     * Dictionary of custom options.
     */
    expertSettings?: object
    /**
     * If there is a custom AB-testing running, return name of group to which the request belongs.
     */
    returnAbGroup?: boolean
  }

  /**
   * Full-text personalized search. The results are based on the provided `searchQuery` and also on the user's past interactions (purchases, ratings, etc.) with the items (items more suitable for the user are preferred in the results).
   * All the string and set item properties are indexed by the search engine.
   * This endpoint should be used in a search box at your website/app. It can be called multiple times as the user is typing the query in order to get the most viable suggestions based on current state of the query, or once after submitting the whole query.
   * It is also possible to use POST HTTP method (for example in case of very long ReQL filter) - query parameters then become body parameters.
   * The returned items are sorted by relevancy (first item being the most relevant).
   */
  export class SearchItems implements Request<unknown> {
    /**
     * Construct the request
     * @param {string} userId - ID of the user for whom personalized search will be performed.
     * @param {string} searchQuery - Search query provided by the user. It is used for the full-text search.
     * @param {number} count - Number of items to be returned (N for the top-N results).
     * @param {Object} optional - Optional parameters given as an object with structure name of the parameter: value
     * - Allowed parameters:
     *     - *scenario*
     *         - Type: string
     *         - Description: Scenario defines a particular search field in your user interface.
     * You can set various settings to the [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com). You can also see performance of each scenario in the Admin UI separately, so you can check how well each field performs.
     * The AI which optimizes models in order to get the best results may optimize different scenarios separately, or even use different models in each of the scenarios.
     *     - *cascadeCreate*
     *         - Type: boolean
     *         - Description: If the user does not exist in the database, returns a list of non-personalized search results and creates the user in the database. This allows for example rotations in the following recommendations for that user, as the user will be already known to the system.
     *     - *returnProperties*
     *         - Type: boolean
     *         - Description: With `returnProperties=true`, property values of the recommended items are returned along with their IDs in a JSON dictionary. The acquired property values can be used for easy displaying of the recommended items to the user.
     * Example response:
     * ```
     *   {
     *     "recommId": "ce52ada4-e4d9-4885-943c-407db2dee837",
     *     "recomms":
     *       [
     *         {
     *           "id": "tv-178",
     *           "values": {
     *             "description": "4K TV with 3D feature",
     *             "categories":   ["Electronics", "Televisions"],
     *             "price": 342,
     *             "url": "myshop.com/tv-178"
     *           }
     *         },
     *         {
     *           "id": "mixer-42",
     *           "values": {
     *             "description": "Stainless Steel Mixer",
     *             "categories":   ["Home & Kitchen"],
     *             "price": 39,
     *             "url": "myshop.com/mixer-42"
     *           }
     *         }
     *       ]
     *   }
     * ```
     *     - *includedProperties*
     *         - Type: string[]
     *         - Description: Allows to specify, which properties should be returned when `returnProperties=true` is set. The properties are given as a comma-separated list.
     * Example response for `includedProperties=description,price`:
     * ```
     *   {
     *     "recommId": "a86ee8d5-cd8e-46d1-886c-8b3771d0520b",
     *     "recomms":
     *       [
     *         {
     *           "id": "tv-178",
     *           "values": {
     *             "description": "4K TV with 3D feature",
     *             "price": 342
     *           }
     *         },
     *         {
     *           "id": "mixer-42",
     *           "values": {
     *             "description": "Stainless Steel Mixer",
     *             "price": 39
     *           }
     *         }
     *       ]
     *   }
     * ```
     *     - *filter*
     *         - Type: string
     *         - Description: Boolean-returning [ReQL](https://docs.recombee.com/reql.html) expression which allows you to filter recommended items based on the values of their attributes.
     * Filters can be also assigned to a [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com).
     *     - *booster*
     *         - Type: string
     *         - Description: Number-returning [ReQL](https://docs.recombee.com/reql.html) expression which allows you to boost recommendation rate of some items based on the values of their attributes.
     * Boosters can be also assigned to a [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com).
     *     - *logic*
     *         - Type: string|
     *         - Description: Logic specifies particular behavior of the recommendation models. You can pick tailored logic for your domain and use case.
     * See [this section](https://docs.recombee.com/recommendation_logics.html) for list of available logics and other details.
     * The difference between `logic` and `scenario` is that `logic` specifies mainly behavior, while `scenario` specifies the place where recommendations are shown to the users.
     * Logic can be also set to a [scenario](https://docs.recombee.com/scenarios.html) in the [Admin UI](https://admin.recombee.com).
     *     - *expertSettings*
     *         - Type:
     *         - Description: Dictionary of custom options.
     *     - *returnAbGroup*
     *         - Type: boolean
     *         - Description: If there is a custom AB-testing running, return name of group to which the request belongs.
     */
    constructor (userId: string, searchQuery: string, count: string, optional?: SearchItemsOptions)
  }
}
