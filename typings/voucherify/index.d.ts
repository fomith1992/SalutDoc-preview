/* https://github.com/voucherifyio/voucherify-nodejs-sdk */

declare module 'voucherify' {
  /* https://docs.voucherify.io/reference?utm_source=github&utm_medium=sdk&utm_campaign=acq#versioning */
  export type ApiVersion = 'v2017-04-05' | 'v2017-04-20' | 'v2018-08-01'
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk#setup */
  /* https://docs.voucherify.io/docs/authentication */
  export interface ApiClientOptions {
    apiUrl: string
    applicationId: string
    clientSecretKey: string
    apiVersion?: ApiVersion
    channel?: string
  }
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk/blob/master/src/ApiClient.js#L42 */
  export type VoucherifyClient = {

  }
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk/blob/master/src/ApiClient.js#L42 */
  export default class Voucherify {
    constructor (options: ApiClientOptions)
    vouchers: Vouchers
    redemptions: Redemptions
    customers: Customers
    validations: Validations
  }

  ////////////////
  /// Vouchers ///
  ////////////////

  /* https://docs.voucherify.io/reference?utm_source=github&utm_medium=sdk&utm_campaign=acq#create-voucher */
  export type Metadata = {
    [key: string]: string | boolean | number | object
  }
  export type Discount = {
    type: 'AMOUNT'
    amount_off: number // Amount that will be taken off the subtotal of a price. Value is multiplied by 100 to precisely represent 2 decimal places.
  } | {
    type: 'PERCENT'
    percent_off: number // Percent that will be taken off the subtotal of any amount to pay.
  } | {
    type: 'UNIT'
    unit_off: number // Number of units that will be taken off the subtotal of any bill to pay.
    unit_type: string // Type of unit discount (e.g. time, items).
    effect?: 'ADD_MISSING_ITEMS' | 'ADD_NEW_ITEMS' //
  }
  export type Gift = {
    amount: number // Amount associated with the voucher. Value is multiplied by 100 to precisely represent 2 decimal places. For example, $100 amount is written as 10000.
    balance: number
  }
  export type Asset = {
    id: string
    url: string
  }
  export type Assets = {
    barcode: Asset
    qr: Asset
  }
  /* https://docs.voucherify.io/reference#the-voucher-object */
  /* https://docs.voucherify.io/docs/vouchers-1 */
  export type Voucher = {
    object: 'voucher' // Type of the object represented by JSON. Value is voucher.
    id: string
    active: boolean // A flag that allows to disable a voucher even though it's within its activity period.
    code: string // A code that identifies the voucher. It is optional. Code will be generated automatically if not provided.
    is_referal_code: boolean
    campaign: string | null // The campaign name this voucher belongs to.
    category: string // The category this voucher belongs to.
    type: 'DISCOUNT_VOUCHER' | 'GIFT_VOUCHER'
    gift: Gift | null // This object represent gift parameters. Present only if type is GIFT_VOUCHER.
    discount: Discount // This object represents discount parameters. Present only if type is DISCOUNT_VOUCHER
    loyality_card: string | null
    start_date: string | null // Voucher's activation date in ISO 8601 date format.
    expiration_date: string | null // Voucher's expiration date in ISO 8601 date format.
    updated_at: string | null
    metadata: Metadata | null // A set of key/value pairs that you can attach to a voucher object. It can be useful for storing additional information about the voucher in a structured format.
    additional_info: string | null // A field to keep any extra textual information.
    assets: Assets // An object stores links to images with QR codes in which Voucherify encrypts voucher code.
    publish: any // An object gets updated whenever a voucher has been published
    redemption: any // A list of redemptions that have been applied to the voucher.
    validation_rules_assignments: any // List of The validation rule assignment object
  }
  /* https://docs.voucherify.io/reference#create-voucher */
  export type CreateVoucherRequest = {
    code: string
    type: 'DISCOUNT_VOUCHER' | 'GIFT_VOUCHER'
    discount: Discount
    gift?: Gift
    category?: string
    additional_info?: string
    start_date?: string
    expiration_date?: string
    active: boolean
    redemption?: {
      quantity: number | null
    }
    metadata?: Metadata
  }
  /* https://docs.voucherify.io/reference?utm_source=github&utm_medium=sdk&utm_campaign=acq#update-voucher */
  export type UpdateVoucherRequest = {
    code: string
    category?: string | null
    start_date?: string | null
    expiration_date?: string | null
    active?: boolean
    additional_info?: string | null
    metadata?: Metadata
  }
  /* https://docs.voucherify.io/reference?utm_source=github&utm_medium=sdk&utm_campaign=acq#delete-voucher */
  export type DeleteVoucherRequest = {}
  /* https://docs.voucherify.io/reference?utm_source=github&utm_medium=sdk&utm_campaign=acq#list-vouchers */
  export type ListVouchersRequest = {}
  export type ListVouchersResponse = {
    object: 'list'
    total: number
    dataRef: 'vouchers'
    vouchers: Voucher[]
  }
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk/blob/c63f738c5a53ecb9a5006a04d80d612ee70bdeae/src/Vouchers.js#L5 */
  export class Vouchers {
    get (code: string): Promise<Voucher>
    create (options: CreateVoucherRequest): Promise<Voucher>
    update (options: UpdateVoucherRequest): Promise<Voucher>
    delete (code: string, options?: DeleteVoucherRequest): Promise<void>
    list (options?: ListVouchersRequest): Promise<ListVouchersResponse>
    enable (code: string): Promise<Voucher>
    disable (code: string): Promise<Voucher>
  }

  /////////////////
  /// Customers ///
  /////////////////

  export type Address = {
    country?: string | null
    city?: string | null
    state?: string | null
    line_1?: string | null
    line_2?: string | null
    postal_code?: string | null
  }
  export type CustomerSummaryRedemptions = {
    total_redeemed: number
    total_failed: number
    total_succeeded: number
    total_rolled_back: number
    total_rollback_failed: number
    total_rollback_succeeded: number
    gift: any
    loyalty_card: any
  }
  export type CustomerSummaryRedemptions = {
    total_amount: number
    total_count: number
    average_amount: number
    last_order_amount: number
    last_order_date: string | null
  }
  /* https://docs.voucherify.io/reference#the-customer-object */
  /* https://docs.voucherify.io/docs/customers */
  export type Customer = {
    object: 'customer' // Type of the object represented by JSON. Value is customer.
    id: string
    source_id: string // A unique customer identifier. You can provide your own (e.g. CRM id) or use the one returned by Voucherify
    name: string | null
    email: string | null
    description: string | null // An arbitrary string that you can attach to a customer object. It is displayed alongside a customer in the dashboard.
    address: Address | null // A set of key/value pairs which describes address. Allowed keys: city, state, line_1, line_2, country, postal_code.
    phone: string | null
    metadata: Metadata | null // A set of custom key/value pairs that you can attach to a customer object for segment building.
    created_at: string
    summary: {
      redemptions: CustomerSummaryRedemptions
      orders: any
    }
  }
  /* https://docs.voucherify.io/reference#create-customer */
  export type CreateCustomerRequest = {
    name?: string
    email?: string
    metadata?: Metadata
    description?: string
    source_id?: string
    address?: Address
    phone?: string
  }
  /* https://docs.voucherify.io/reference#list-customers */
  export type ListCustomersRequest = {
    limit?: number
    page?: number
    email?: string
    city?: string
    name?: string
    order?: string
    starting_after?: string
  }
  export type ListCustomersResponse = {
    object: 'list'
    has_more: boolean
    total: number
    data_ref: 'customers'
    customers: Customer[]
  }
  /* https://docs.voucherify.io/reference#update-customer */
  export type UpdateCustomerRequest = {
    id: string
    source_id?: string
    name?: string | null
    email?: string | null
    metadata?: Metadata | null
    description?: string | null
    address?: Address| null
    phone?: string| null
  }
  /* https://docs.voucherify.io/reference#update-customers-consents */
  export type CustomerOptions = {
    id: string
    source_id?: string
  }
  export type Consents = {
    [key: string]: boolean
  }
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk/blob/c63f738c5a53ecb9a5006a04d80d612ee70bdeae/src/Customers.js#L6 */
  export class Customers {
    create (options: CreateCustomerRequest): Promise<Customer>
    get (customerId: string): Promise<Customer>
    list (params?: ListCustomersRequest): Promise<ListCustomersResponse>
    update (options: UpdateCustomerOptions): Promise<Customer>
    delete (customerId: string): Promise<void>
    updateConsents (options: CustomerOptions, consents: Consents): Promise<void>
  }

  ///////////////////
  /// Redemptions ///
  ///////////////////

  export type Order = {
    amount: number // A positive integer representing total order value. Value is multiplied by 100 to precisely represent 2 decimal places.
    total_discount_amount?: number // A positive integer representing total discount value. Value is multiplied by 100 to precisely represent 2 decimal places
    total_amount?: number // A positive integer representing total order value after applying the discount. Value is multiplied by 100 to precisely represent 2 decimal places
    items?: any // (Order[]) // A list of Order Items that have been applied to the order
  }
  /* https://docs.voucherify.io/reference#the-redemption-object */
  /* https://docs.voucherify.io/docs/redemption */
  export type Redemption = {
    object: 'redemption' // Type of the object represented by JSON. Value is redemption.
    id: string
    date: string
    customer_id: string // An identifier of a customer stored in Voucherify that redeemed a voucher.
    tracking_id: string // A tracking identifier of a user that redeemed a voucher. Identifier generated during voucher validation.
    gift: { amount: number } | null // A positive integer representing total value redeemed for gift voucher. It is not presented for discount vouchers. Value is multiplied by 100 to precisely represent 2 decimal places.
    order: Order | null // The purchase of previously defined products by end customers is handled through the creation of order objects. Order object is used to evaluate redemption rules based on products and total order volume.
    metadata: Metadata | null
    voucher: Voucher // Object presenting redeemed voucher.
    customer: Customer | null // Object of the customer this redemption is for if one exists.
    result: 'SUCCESS' | 'FAILURE' // The status of the redemption
    failure_code: string | null // Error code explaining reason for redemption failure if available
  }
  /* https://docs.voucherify.io/reference#redeem-voucher */
  export type RedeemVoucherRequest = {
    // tracking_id: string => Handled by SDK
    customer: {
      id?: string
      source_id?: string,
      name?: string
      email?: string
      description?: string
      metadata?: Metadata
    } | null
    order?: {
      id: string
      source_id: string
      amount: number
      items: any
      status: 'CREATED' | 'PAID' | 'CANCELLED' | 'FULFILLED'
      metadata: Metadata | null
    } | null
    metadata?: Metadata | null
    reward?: {
      id: string
      points: number
    } | null
    gift?: {
      credits: number
    } | null
    // session: { key: string } => Handled by SDK
  }
  /* https://docs.voucherify.io/reference#list-redemptions */
  export type ListRedemptionsRequest = {
    page?: number
    result?: 'SUCCESS' | 'FAILURE'
    campaign?: string
    customer?: string
    created_at?: {
      before?: string
      after?: string
    }
  }
  export type ListRedemptionsResponse = {
    object: 'list'
    total: number
    data_ref: 'redemptions'
    redemptions: Redemption[]
  }
  /* https://docs.voucherify.io/reference#vouchers-redemptions */
  export type ListVoucherRedemptionsResponse = {
    object: 'list'
    total: number
    data_ref: 'redemption_entries'
    quantity: number | null
    redeem_quantity: number | null
    redeem_amount: number | null
    redemption_entries: Redemption[]
  }
  /* https://docs.voucherify.io/reference#rollback-redemption */
  export type RollbackRedemptionRequest = {
    // tracking_id: string => Handled by SDK
    reason?: string
    customer?: {
      id: string
      source_id: string,
      name?: string
      email?: string
      description?: string
      metadata?: Metadata
    }
  }
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk/blob/c63f738c5a53ecb9a5006a04d80d612ee70bdeae/src/Redemptions.js#L5 */
  export class Redemptions {
    redeem (code: string, params: RedeemVoucherRequest): Promise<Redemption>
    list (params?: ListRedemptionsRequest): Promise<ListRedemptionsResponse>
    getForVoucher (code: string): Promise<ListVoucherRedemptionsResponse>
    rollback (redemptionId: string, params?: RollbackRedemptionRequest): Promise<Redemption>
  }

  /////////////////
  // Validations //
  /////////////////

  /* https://docs.voucherify.io/reference#the-validation-object */
  export type Validation = {
    valid: true
    object: 'validation'
    id: string
    discount: Discount
    code: string
    order: Order
    tracking_id?: string | null
    date?: string | null
    metadata?: Metadata | null
    customer?: Customer | null
    failure_code?: string | null
    reason?: string | null
    applicable_to?: {
      data: any
      object: string
      total: number
      data_ref: string
    } | null
    inapplicable_to?: {
      data: any
      object: string
      total: number
      data_ref: string
    } | null
  } | {
    valid: false
    code: string
    tracking_id?: string | null
    reason?: string | null
    metadata?: Metadata | null
  }
  /* https://docs.voucherify.io/reference#validate-voucher */
  export type ValidateVoucherRequest = {
    customer: {
      id?: string
      source_id?: string
      name?: string
      email?: string
      description?: string
      metadata?: Metadata
    }
    order: {
      id?: string
      source_id?: string
      amount?: number
      metadata?: Metadata
      items?: any
    }
    gift?: {
      credits?: nubmer
    }
  }
  /* https://github.com/voucherifyio/voucherify-nodejs-sdk/blob/c63f738c5a53ecb9a5006a04d80d612ee70bdeae/src/Validations.js#L5 */
  export class Validations {
    validateVoucher (code: string, params: ValidateVoucherRequest): Promise<Validation>
  }
}
