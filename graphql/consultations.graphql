extend type Query {
    consultationById(id: ID!): Consultation
}

extend type Mutation {
    consultationCreate(userId: ID!, specialization: String!): ConsultationMutationResult!
    consultationAddAnamnesis(consultationId: ID!, content: AnamnesisInput!): ConsultationMutationResult!
    consultationAddReport(consultationId: ID!, content: ReportInput!): ConsultationMutationResult!
    consultationAddDoctorAndStart(consultationId: ID!, doctorId: ID!): ConsultationMutationResult!
    consultationPay(consultationId: ID!, userId: ID!, promocode: String): ConsultationMutationResult!
    consultationApplyPromocode(code: String!, userId: ID!): ConsultationApplyPromocodeResult!
}

extend type User {
    freeConsultations(first: Int!, after: Token): ConsultationsConnection
    consultationsAsPatient(first: Int!, after: Token): ConsultationsConnection
    consultationsAsDoctor(first: Int!, after: Token): ConsultationsConnection
    consultationsAsModerator(first: Int!, after: Token): ConsultationsConnection

    consultationsCount: Int
    freeConsultationsCount: Int
}

type ConsultationsConnection {
    edges: [ConsultationEdge!]!
    pageInfo: PageInfo!
}

type ConsultationEdge {
    node: Consultation!
}

type ConsultationMutationResult {
    errors: [MutationError!]!
    message: String!
    consultation: Consultation
}

input AnamnesisInput {
    complaints: String!
    fileIds: [ID!]!
}

input ReportInput {
    diagnosis: String!
    diagnostics: String!
    doctors: String!
    medicines: String!
    recommendation: String!
}

type Consultation {
    id: ID!
    patient: User!
    specialization: String!
    anamnesis: Anamnesis
    createdAt: Timestamp!
    payment: Payment
    startedAt: Timestamp
    doctor: User
    finished: Boolean!
    report: Report
    chat: ConsultationChat
}

type Anamnesis {
    complaints: String!
    files: [FileAnamnesis!]!
}

type FileAnamnesis {
    url: FileURL!
    id: ID!
}

type Report {
    diagnosis: String!
    diagnostics: String!
    doctors: String!
    medicines: String!
    recommendation: String!
}

type ConsultationApplyPromocodeResult {
    errors: [MutationError!]!
    message: String!
    amount: String
}
