type Community {
    id: ID!
    name: String!
    specialization: String!
    description: String!
    network: String
    codex: String
    type: CommunityType!
    admins: [CommunityAdmin!]!

    avatar: ImageURL
    cover: ImageURL

    posts(first: Int!, after: Token): PostsConnection
    works(first: Int!, after: Token): WorksConnection
}

type CommunityAdmin {
    user: User!
    position: String!
    about: String!
}

input CommunityCreateInput {
    name: String!
    specialization: String!
    description: String!
    type: CommunityType!
}

input CommunityUpdateInfoInput {
    name: String!
    specialization: String!
    description: String!
    codex: String!
    network: String!
}

type CommunityMutationResult {
    errors: [MutationError!]!
    message: String
    community: Community
}

enum CommunityType {
    PRIVATE
    PUBLIC
}

input MembershipUserFilter {
    country: String
    city: String
}

input CommunityUpsertAdminInput {
    position: String!
    about: String!
}

input CommunitiesFilter {
    query: String
    memberUserId: String
    specialization: String
}

type CommunitiesConnection {
    edges: [CommunitiesEdge!]!
    pageInfo: PageInfo!
}

type CommunitiesEdge {
    node: Community!
}

extend type Query {
    communityById(id: ID!): Community
    communities(first: Int!, after: Token, filter: CommunitiesFilter): CommunitiesConnection
}

extend type Mutation {
    communityCreate(input: CommunityCreateInput!): CommunityMutationResult!
    communityUpdateInfo(communityId: ID!, input: CommunityUpdateInfoInput!): CommunityMutationResult!
    communityUpdateAvatar(communityId: ID!, avatar: Upload!): CommunityMutationResult!
    communityUpdateCover(communityId: ID!, cover: Upload!): CommunityMutationResult!

    communityUpsertAdmin(communityId: ID!, userId: ID!, input: CommunityUpsertAdminInput!): CommunityMutationResult!
    communityRemoveAdmin(communityId: ID!, userId: ID!): CommunityMutationResult!
}
