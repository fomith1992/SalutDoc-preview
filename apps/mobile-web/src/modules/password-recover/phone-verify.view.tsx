import { toCaseCount } from '@salutdoc/mobile-components/lib/i18n/utils'
import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { useInterval, useUpdate } from 'react-use'
import { yup } from '@salutdoc/mobile-components/lib/modules/form-kit'
import { Input } from '../ui-kit/input'
import { Description, SubInformation } from '../ui-kit/text'
import css from './password-recover-mobile.module.scss'
import { SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { Button } from '../ui-kit/button'

export interface PhoneVerifyViewProps {
  phoneNumber: string
  resendCodeAfter: Date

  onSubmit: SubmissionHandler<FormData>
  onResendCode: () => void
}

type FormData = yup.InferType<typeof formSchema>

const formSchema = yup.object({
  verificationCode: yup.string().required('Пожалуйста, укажите код')
    .required()
}).required()

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export function PhoneVerifyView (
  { phoneNumber, resendCodeAfter, onSubmit, onResendCode }: PhoneVerifyViewProps
): React.ReactElement {
  const timerRemaining = Math.round((resendCodeAfter.getTime() - Date.now()) / 1000)
  useInterval(useUpdate(), timerRemaining > 0 ? 1000 : null)

  return (
    <Form onSubmit={onSubmit}>
      <div className={css.container}>
        <Description>
          Смс с кодом подтверждения было отправлено на номер {phoneNumber}.
        </Description>
        <Field name='verificationCode'>
          {props => (
            <div className={css.inputContainerNoLabel}>
              <Input
                {...props}
                placeholder='111111'
                type='number'
              />
            </div>
          )}
        </Field>
        <div className={css.mainBtn}>
          <Submit>
            {props => (
              <SubmitTextButton
                type='primary'
                size='XL'
                {...props}
              >
                Подтвердить
              </SubmitTextButton>
            )}
          </Submit>
        </div>
        <div className={css.bottomButton}>
          {timerRemaining > 0
            ? (
              <SubInformation>
                {`Отправить смс повторно можно через ${timerRemaining} ${toCaseCount(['секунду', 'секунды', 'секунд'], timerRemaining)}`}
              </SubInformation>
            ) : (
              <Button
                type='link'
                onPress={onResendCode}
              >
                Отправить смс повторно
              </Button>
            )}
        </div>
      </div>
    </Form>
  )
}
