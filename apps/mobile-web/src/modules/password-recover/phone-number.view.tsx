import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { yup } from '@salutdoc/mobile-components/lib/modules/form-kit'
import { MaskedInput } from '../ui-kit/input'
import { Description } from '../ui-kit/text'
import css from './password-recover-mobile.module.scss'
import { SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'

export type PhoneNumberFormData = yup.InferType<typeof formSchema>

export interface PhoneNumberViewProps {
  initialValues?: Partial<PhoneNumberFormData>
  onSubmit: SubmissionHandler<PhoneNumberFormData>
}

const formSchema = yup.object({
  region: yup.string()
    .oneOf(['+7'] as const)
    .required(),
  phoneNumber: yup.string()
    .matches(/\+7 \(\d{3}\) \d{3}-\d\d-\d\d/)
    .required()
}).required()

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export function PhoneNumberView (
  { onSubmit, initialValues }: PhoneNumberViewProps
): React.ReactElement {
  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{
        region: '+7',
        ...initialValues
      }}
    >
      <div className={css.container}>
        <Description>
          Введите номер телефона, на который был зарегистрирован ваш аккаунт.
        </Description>
        <Field name='phoneNumber'>
          {props => (
            <div className={css.inputContainerNoLabel}>
              <MaskedInput
                {...props}
                type='tel'
                mask='+7 (999) 999-99-99'
                placeholder='+7 (900) 000-00-00'
                ignoreErrors
              />
            </div>
          )}
        </Field>
        <div className={css.mainBtn}>
          <Submit>
            {props => (
              <SubmitTextButton
                type='primary'
                size='XL'
                {...props}
              >
                Получить код
              </SubmitTextButton>
            )}
          </Submit>
        </div>
      </div>
    </Form>
  )
}
