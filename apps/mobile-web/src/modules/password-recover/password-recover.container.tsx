import ky from 'ky/umd'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { getPublicConfig } from '../../config/public.config'
import { Header } from '../ui-kit/header'
import { PasswordView } from './password.view'
import { PhoneNumber } from './phone-number.container'
import { PhoneNumberFormData } from './phone-number.view'
import { VerificationSession } from './phone-verification.types'
import { PhoneVerifyView } from './phone-verify.view'

export interface VerificationSessionData {
  id: string
  resendCodeAfter: Date
}

type CommonState = {
  phoneNumber?: Partial<PhoneNumberFormData>
  phoneNumberCertificate?: string
}

type PhoneNumberState = {
  step: 'number'
}

type PhoneNumberVerificationState = {
  step: 'verification'
  phoneNumber: PhoneNumberFormData
  verificationSession: VerificationSessionData
}

type PasswordState = {
  step: 'password'
  phoneNumber: PhoneNumberFormData
  phoneNumberCertificate: string
}

type RegistrationState = CommonState & (PhoneNumberState | PhoneNumberVerificationState | PasswordState)

export function PasswordRecovery (): React.ReactElement {
  const [state, setState] = useState<RegistrationState>({ step: 'number' })
  const { API_URL, FRONT_URL } = getPublicConfig()

  const router = useRouter()
  const { uid } = router.query

  const id = Array.isArray(uid) ? uid[0] : uid

  if (id == null) {
    return <div>Произошла ошибка</div>
  }

  switch (state.step) {
    case 'number': {
      return (
        <>
          <Header
            title='Введите номер телефона'
            onBack={() => {
              window.history.back()
            }}
          />
          <PhoneNumber
            initialValues={state.phoneNumber}
            onComplete={(phoneNumber, verificationSession) => setState({
              ...state,
              step: 'verification',
              phoneNumber,
              verificationSession: {
                id: verificationSession.id,
                resendCodeAfter: new Date(Date.now() + verificationSession.resendCodeDelayMillis)
              }
            })}
          />
        </>
      )
    }
    case 'verification': {
      const sessionId = state.verificationSession.id
      return (
        <>
          <Header
            title='Введите код из смс'
            onBack={() => setState({
              ...state,
              step: 'number'
            })}
          />
          <PhoneVerifyView
            phoneNumber={state.phoneNumber.phoneNumber}
            resendCodeAfter={state.verificationSession.resendCodeAfter}
            onSubmit={async (data) => {
              try {
                const { certificate } = await ky.post(
                  new URL(`/api/v1/registration/phone/verifications/${sessionId}/verify`, API_URL),
                  {
                    json: {
                      code: data.verificationCode
                    }
                  }
                ).json<{ certificate: string }>()
                setState({
                  ...state,
                  step: 'password',
                  phoneNumberCertificate: certificate
                })
                return {}
              } catch (e) {
                console.error(e)
                if (e instanceof ky.HTTPError && e.response.status === 400) {
                  return { verificationCode: 'Неверный код' }
                } else {
                  alert('Не удалось проверить код, попробуйте позже')
                  return {}
                }
              }
            }}
            onResendCode={async () => {
              try {
                const verificationSession = await ky.post(
                  new URL(`/api/v1/registration/phone/verifications/${sessionId}/resend`, API_URL)
                ).json<VerificationSession>()
                setState({
                  ...state,
                  verificationSession: {
                    id: verificationSession.id,
                    resendCodeAfter: new Date(Date.now() + verificationSession.resendCodeDelayMillis)
                  }
                })
              } catch (e) {
                console.error(e)
                alert('Не удалось отправить код')
              }
            }}
          />
        </>
      )
    }
    case 'password':
      return (
        <>
          <Header
            title='Придумайте новый пароль'
            onBack={() => setState({
              ...state,
              step: 'number'
            })}
          />
          <PasswordView
            phoneNumber={state.phoneNumber.phoneNumber}
            onSubmit={async (data) => {
              if (id == null) {
                alert('Interaction id not defined')
                return {}
              }
              try {
                await ky.post(new URL('/api/v1/registration/users/recover-password', API_URL), {
                  json: {
                    phoneNumber: state.phoneNumber.phoneNumber,
                    phoneNumberCertificate: state.phoneNumberCertificate,
                    password: data.password
                  }
                })
              } catch (e) {
                console.error(e)
                alert('Не удалось создать пользователя, попробуйте позже')
                return {}
              }
              try {
                type ResponseData = {
                  ok: true
                  returnTo: string
                } | {
                  ok: false
                  message: string
                }
                const response = await ky.post(new URL(`/auth/v1/interaction/${id}`, FRONT_URL), {
                  credentials: 'include',
                  body: new URLSearchParams({
                    phoneNumber: state.phoneNumber.phoneNumber,
                    password: data.password
                  })
                }).json<ResponseData>()
                if (response.ok) {
                  window.location.replace(`/recover/success?returnTo=${encodeURIComponent(response.returnTo)}`)
                } else {
                  console.error(response.message)
                  alert('Не удалось войти в аккаунт, попробуйте позже')
                }
              } catch (e) {
                console.error(e)
                alert('Не удалось войти в аккаунт, попробуйте позже')
              }
              return {}
            }}
          />
        </>
      )
  }
}
