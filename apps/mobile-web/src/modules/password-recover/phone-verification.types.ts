export interface VerificationSession {
  id: string
  expiresAt: string
  resendCodeDelayMillis: number
}
