import React from 'react'
import { Button } from '../ui-kit/button'
import { Input } from '../ui-kit/input'
import { Description, Title } from '../ui-kit/text'
import css from './registration-mobile.module.scss'

export function Education (
  props: { onNext: () => void }
): React.ReactElement {
  return (
    <div className={css.container}>
      <div className={css.title}>
        <Title>
          Почти готово!
        </Title>
      </div>
      <Description>
        Для того что-бы полноценно пользоваться данным сайтом вам необходимо заполнить ваш профиль
      </Description>
      <Input placeholder='МГУ' title='Университет' />
      <Input placeholder='Кибернетики' title='Специализация' />
      <div className={css.mainBtn}>
        <Button onPress={props.onNext}>Зарегестрироваться</Button>
      </div>
      <div className={css.linkBtn}>
        <Button onPress={props.onNext} type='link'>Заполнить позже</Button>
      </div>
    </div>
  )
}
