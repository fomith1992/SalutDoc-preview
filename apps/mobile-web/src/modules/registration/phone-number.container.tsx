import ky from 'ky/umd'
import React, { useRef } from 'react'
import ReCAPTCHA from 'react-google-recaptcha'
import { getPublicConfig } from '../../config/public.config'
import { PhoneNumberFormData, PhoneNumberView } from './phone-number.view'
import { VerificationSession } from './phone-verification.types'

export interface PhoneNumberProps {
  initialValues?: Partial<PhoneNumberFormData>
  onComplete: (data: PhoneNumberFormData, session: VerificationSession) => void
}

export function PhoneNumber (
  { initialValues, onComplete }: PhoneNumberProps
): React.ReactElement {
  const { REGISTRATION_RECAPTCHA_SITEKEY, API_URL } = getPublicConfig()

  const recaptchaRef = useRef<ReCAPTCHA>(null)
  return (
    <>
      <ReCAPTCHA
        ref={recaptchaRef}
        size='invisible'
        sitekey={REGISTRATION_RECAPTCHA_SITEKEY}
      />
      <PhoneNumberView
        initialValues={initialValues}
        onSubmit={async (data) => {
          try {
            const recaptcha = await recaptchaRef.current?.executeAsync()
            if (recaptcha != null) {
              const verificationSession = await ky.post(new URL('/api/v1/registration/phone/verifications', API_URL), {
                json: {
                  phoneNumber: data.phoneNumber,
                  recaptcha,
                  existingAccount: false
                }
              }).json<VerificationSession>()
              onComplete(data, verificationSession)
            }
          } catch (e) {
            console.error(e)
            if (e instanceof ky.HTTPError && e.response.status === 400) {
              alert('Профиль с указанным мобильным телефоном уже существует')
            } else {
              alert('Не удалось отправить код на указанный номер')
            }
          }
          recaptchaRef.current?.reset()
          return {}
        }}
      />
    </>
  )
}
