import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { yup } from '@salutdoc/mobile-components/lib/modules/form-kit'
import { MaskedInput } from '../ui-kit/input'
import { Description, SubInformation } from '../ui-kit/text'
import css from './registration-mobile.module.scss'
import { SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'

const privacyPolicy = 'https://about.salutdoc.com/terms-of-service'
const userAgreement = 'https://about.salutdoc.com/privacy/'

export type PhoneNumberFormData = yup.InferType<typeof formSchema>

export interface PhoneNumberViewProps {
  initialValues?: Partial<PhoneNumberFormData>
  onSubmit: SubmissionHandler<PhoneNumberFormData>
}

const formSchema = yup.object({
  region: yup.string()
    .oneOf(['+7'] as const)
    .required(),
  phoneNumber: yup.string()
    .matches(/\+7 \(\d{3}\) \d{3}-\d\d-\d\d/)
    .required()
}).required()

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export function PhoneNumberView (
  { onSubmit, initialValues }: PhoneNumberViewProps
): React.ReactElement {
  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{
        region: '+7',
        ...initialValues
      }}
    >
      <div className={css.container}>
        <Description>
          Ваш номер телефона нужен приложению для процедуры идентификации.
        </Description>
        <Field name='phoneNumber'>
          {props => (
            <div className={css.inputContainerWithoutLabel}>
              <MaskedInput
                {...props}
                type='tel'
                mask='+7 (999) 999-99-99'
                placeholder='+7 (900) 000-00-00'
                ignoreErrors
                autoCompleteType='username'
              />
            </div>
          )}
        </Field>
        <div className={css.mainBtn}>
          <Submit>
            {props => (
              <SubmitTextButton
                type='primary'
                size='XL'
                {...props}
              >
                Получить код
              </SubmitTextButton>
            )}
          </Submit>
        </div>
        <div className={css.bottomInfo}>
          <SubInformation>
            Нажимая «Получить код», вы соглашаетесь с <a href={privacyPolicy} className={css.linkBtn}>политикой конфиденциальности</a> и <a href={userAgreement} className={css.linkBtn}>пользовательским соглашением</a>.
          </SubInformation>
        </div>
      </div>
    </Form>
  )
}
