import React from 'react'
import { yup } from '@salutdoc/mobile-components/lib/modules/form-kit'
import { RadioButton } from '@salutdoc/mobile-components/lib/modules/form-kit/radio-button/index'
import css from './registration-mobile.module.scss'
import { SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'

const formSchema = yup.object({
  role: yup.string()
    .oneOf(['USER', 'DOCTOR'] as const)
    .required()
}).required()

export type AccountTypeFormData = yup.InferType<typeof formSchema>

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export interface AccountTypeViewProps {
  initialValues?: Partial<AccountTypeFormData>
  onSubmit: SubmissionHandler<AccountTypeFormData>
}

export function AccountTypeView (
  { initialValues, onSubmit }: AccountTypeViewProps
): React.ReactElement {
  return (
    <Form onSubmit={onSubmit} initialValues={initialValues}>
      <div className={css.container}>
        <Field name='role'>
          {props => (
            <div className={css.correction}>
              <RadioButton
                label='Пациент'
                active={props.value === 'USER'}
                disabled={props.active}
                onPress={() => props.onChange('USER')}
              />
              <RadioButton
                label='Врач'
                active={props.value === 'DOCTOR'}
                disabled={props.active}
                onPress={() => props.onChange('DOCTOR')}
              />
            </div>
          )}
        </Field>
        <div className={css.mainBtn}>
          <Submit>
            {props => (
              <SubmitTextButton
                size='XL'
                type='primary'
                {...props}
              >
                Далее
              </SubmitTextButton>
            )}
          </Submit>
        </div>
      </div>
    </Form>
  )
}
