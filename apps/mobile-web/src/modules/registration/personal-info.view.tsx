import { LocalDate, ZonedDateTime } from '@js-joda/core'
import { createFormDescriptor, SubmissionHandler, Formatter } from '@salutdoc/react-components'
import React from 'react'
import { yup } from '@salutdoc/mobile-components/lib/modules/form-kit'
import { DatePicker } from '../ui-kit/date-picker'
import { Input } from '../ui-kit/input'
import { Description } from '../ui-kit/text'
import css from './registration-mobile.module.scss'
import { SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { RadioButton } from '@salutdoc/mobile-components/lib/modules/form-kit/radio-button/index'
import { AccountTypeFormData } from './account-type.view'

export type PersonalInfoFormData = yup.InferType<typeof formSchema>

export interface PersonalInfoViewProps {
  initialValues?: Partial<PersonalInfoFormData>
  onSubmit: SubmissionHandler<PersonalInfoFormData>
  accountType: AccountTypeFormData
}

const nameMaxLength = 30
const birthdayMin = LocalDate.of(1900, 1, 1)
const birthdayMax = LocalDate.now().minusDays(1)

const formSchema = yup.object({
  firstName: yup.string()
    .max(nameMaxLength)
    .required('Пожалуйста, укажите ваше имя'),
  lastName: yup.string()
    .max(nameMaxLength)
    .required('Пожалуйста, укажите вашу фамилию'),
  birthday: yup.date()
    .required()
    .min(birthdayMin.toString())
    .max(birthdayMax.toString()),
  gender: yup.string()
    .oneOf(['MALE', 'FEMALE'] as const)
    .required()
}).required()

const { Form, Field, Submit } = createFormDescriptor(formSchema)

const localDateFormatter: Formatter<Date, LocalDate | undefined> = {
  format: date => date == null ? undefined : ZonedDateTime.parse(date.toISOString()).toLocalDate(),
  parse: date => date == null ? undefined : new Date(date.toString())
}

export function PersonalInfoView (
  { initialValues, onSubmit, accountType }: PersonalInfoViewProps
): React.ReactElement {
  return (
    <Form onSubmit={onSubmit} initialValues={initialValues}>
      <div className={css.container}>
        <Description>
          {accountType.role === 'USER'
            ? 'Эта информация понадобится врачу для того, чтобы провести корректную консультацию.'
            : 'Эта информация нужна нам для того, чтобы пациенты могли больше доверять сервису.'}
        </Description>
        <div className={css.inputContainer}>
          <Field name='firstName'>
            {props => <Input {...props} title='Ваше имя' />}
          </Field>
        </div>
        <div className={css.inputContainer}>
          <Field name='lastName'>
            {props => <Input {...props} title='Ваша фамилия' />}
          </Field>
        </div>
        <div className={css.inputContainer}>
          <Field
            name='birthday'
            formatter={localDateFormatter}
          >
            {props => (
              <DatePicker
                {...props}
                min={birthdayMin}
                max={birthdayMax}
                title='Дата рождения'
                placeholder='ДД.ММ.ГГГГ'
              />
            )}
          </Field>
        </div>
        <div className={css.titleInput}>
          Пол
        </div>
        <Field name='gender'>
          {props => (
            <div className={css.radioButtonContainer}>
              <RadioButton
                label='Мужчина'
                active={props.value === 'MALE'}
                disabled={props.active}
                onPress={() => props.onChange('MALE')}
              />
              <RadioButton
                label='Женщина'
                active={props.value === 'FEMALE'}
                disabled={props.active}
                onPress={() => props.onChange('FEMALE')}
              />
            </div>
          )}
        </Field>
        <div className={css.mainBtn}>
          <Submit>
            {props => (
              <SubmitTextButton
                size='XL'
                type='primary'
                {...props}
              >
                Далее
              </SubmitTextButton>
            )}
          </Submit>
        </div>
        {
          // TODO display on web-site, not in native app
          // <div className={css.signinContainer}>
          //   Уже зарегестрированны? <Link href='/m/login'><a>Войти</a></Link>
          // </div>
        }
      </div>
    </Form>
  )
}
