import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import { yup } from '@salutdoc/mobile-components/lib/modules/form-kit'
import { Input } from '../ui-kit/input'
import { Description } from '../ui-kit/text'
import css from './registration-mobile.module.scss'
import { SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'

export interface PasswordViewProps {
  phoneNumber: string
  onSubmit: SubmissionHandler<PasswordFormData>
}

type PasswordFormData = yup.InferType<typeof formSchema>

const passwordMinLength = 8
const passwordMaxLength = 32

const formSchema = yup.object({
  phoneNumber: yup.string()
    .matches(/\+7 \(\d{3}\) \d{3}-\d\d-\d\d/),
  password: yup.string().required()
    .matches(
      /^[a-zA-Z0-9!@#$%^&_-]+$/,
      'Пароль может содержать только латинские буквы, цифры и символы !@#$%^&_-'
    )
    .min(passwordMinLength)
    .max(passwordMaxLength),
  verifyPassword: yup.string()
    .oneOf([yup.ref('password')], 'Введенные пароли должны совпадать')
}).required().test({
  name: 'passwords match',
  message: 'Введенные пароли должны совпадать',
  test: ({ password, verifyPassword }) => password === verifyPassword
})

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export function PasswordView (
  { onSubmit, phoneNumber }: PasswordViewProps
): React.ReactElement {
  return (
    <form>
      <Form
        initialValues={{
          phoneNumber,
          password: undefined,
          verifyPassword: undefined
        }}
        onSubmit={onSubmit}
      >
        <div className={css.container}>
          <Description>
            Минимум 8 латинских букв и/или символов.
          </Description>
          <div style={{ display: 'none' }}>
            <Field name='phoneNumber'>
              {props => (
                <Input
                  {...props}
                  autoCompleteType='username'
                />)}
            </Field>
          </div>
          <Field name='password'>
            {props => (
              <div className={css.inputContainer}>
                <Input
                  {...props}
                  placeholder='Введите пароль'
                  type='password'
                  autoCompleteType='new-password'
                />
              </div>)}
          </Field>
          <Field name='verifyPassword'>
            {props => (
              <div className={css.inputContainerWithoutLabel}>
                <Input
                  {...props}
                  placeholder='Повторите пароль'
                  type='password'
                  autoCompleteType='new-password'
                />
              </div>)}
          </Field>
          <div className={css.mainBtn}>
            <Submit>
              {props => (
                <SubmitTextButton
                  type='primary'
                  size='XL'
                  {...props}
                >
                  Подтвердить
                </SubmitTextButton>
              )}
            </Submit>
          </div>
        </div>
      </Form>
    </form>
  )
}
