import {
  AuthenticationContext,
  AuthenticationProvider
} from '@salutdoc/mobile-components/lib/contexts/authentication-context'
import React from 'react'
import { getPublicConfig } from '../../config/public.config'

export interface WebAuthenticationProviderProps {
  children?: React.ReactNode
}

export function WebAuthenticationProvider (
  {
    children
  }: WebAuthenticationProviderProps
): React.ReactElement {
  const { FRONT_URL } = getPublicConfig()

  const value: AuthenticationContext = {
    login: ({ page }) => window.location.replace(new URL(`/api/auth/${page}`, FRONT_URL).href),
    logout: () => window.location.replace(new URL('/api/auth/logout', FRONT_URL).href)
  }

  return <AuthenticationProvider value={value}>{children}</AuthenticationProvider>
}
