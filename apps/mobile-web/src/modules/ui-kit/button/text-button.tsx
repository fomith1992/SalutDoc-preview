import classNames from 'classnames'
import React from 'react'
import css from './text-button.module.scss'

export interface ButtonProps {
  type?: 'primary' | 'link'
  onPress?: () => void
  children?: React.ReactText
  loading?: boolean
  disabled?: boolean
  borderRadius?: boolean
}

const typeToClassMapping = {
  primary: css.primary,
  link: css.link
}

export function Button ({
  children,
  onPress,
  type = 'link',
  loading = false,
  borderRadius = false,
  disabled = false
}: ButtonProps): React.ReactElement {
  return (
    <div>
      <button
        className={classNames(typeToClassMapping[type], {
          [css.loading]: loading,
          [css.border]: borderRadius,
          [css.disabled]: disabled && !loading
        })}
        onClick={onPress}
        disabled={disabled}
      >
        {children}
      </button>
    </div>
  )
}
