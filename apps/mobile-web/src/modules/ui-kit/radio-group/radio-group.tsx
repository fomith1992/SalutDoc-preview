import classNames from 'classnames'
import React from 'react'
import css from './radio-group.module.scss'
import { FieldState, FieldHandlers } from '@salutdoc/react-components'

export interface RadioGroupProps<T extends string | number> extends Partial<FieldState<T> & FieldHandlers<T>> {
  title: string
  options: Array<{
    value: T
    label: string
  }>
}

export function RadioGroup<T extends string | number> (
  { title, options, name, value, onChange, onBlur, onFocus }: RadioGroupProps<T>
): React.ReactElement {
  return (
    <div className={css.container}>
      <div className={css.title}>
        {title}
      </div>
      <div className={css.radioToolbar}>
        {options.map((option, idx) => (
          <label
            key={idx}
            className={classNames(css.node, {
              [css.checked]: value === option.value
            })}
          >
            {option.label}
            <input
              type='radio'
              name={name}
              value={option.value}
              checked={value === option.value}
              onChange={() => onChange?.(option.value)}
              onBlur={onBlur}
              onFocus={onFocus}
            />
          </label>
        ))}
      </div>
    </div>
  )
}
