import { FieldHandlers, FieldState } from '@salutdoc/react-components'
import classNames from 'classnames'
import React from 'react'
import ReactInputMask from 'react-input-mask'
import css from './input.module.scss'

interface MaskedInputTextProps extends Partial<FieldState<string> & FieldHandlers<string>> {
  mask: string
  type?: 'text' | 'tel' | 'number'
  title?: string
  placeholder?: string
  ignoreErrors?: boolean
  autoCompleteType?: string
}

export function MaskedInput (
  {
    mask,
    type = 'text',
    title = '',
    placeholder,
    ignoreErrors = false,
    name,
    onChange,
    onFocus,
    onBlur,
    value = '',
    submitting = false,
    active = false,
    error = '',
    touched = false,
    autoCompleteType = undefined
  }: MaskedInputTextProps
): React.ReactElement {
  const showError = !ignoreErrors && error !== null && touched && !active
  return (
    <div>
      {title !== '' ? <div className={css.title}>{title}</div> : null}
      <ReactInputMask
        className={classNames(css.input, {
          [css.invalid]: showError
        })}
        autoComplete={autoCompleteType}
        mask={mask}
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={({ target }) => onChange?.(target.value)}
        onFocus={onFocus}
        onBlur={onBlur}
        disabled={submitting}
      />
      {showError ? <div className={css.error}>{error}</div> : null}
    </div>
  )
}
