import { FieldHandlers, FieldState } from '@salutdoc/react-components'
import classNames from 'classnames'
import React from 'react'
import css from './input.module.scss'

interface InputTextProps extends Partial<FieldState<string> & FieldHandlers<string>> {
  type?: 'text' | 'password' | 'number' // default text
  title?: string
  placeholder?: string
  autoCompleteType?: string
}

export function Input (
  {
    type = 'text',
    title = '',
    placeholder,
    name,
    onChange,
    onFocus,
    onBlur,
    value = '',
    submitting = false,
    active = false,
    error = '',
    touched = false,
    autoCompleteType = undefined
  }: InputTextProps
): React.ReactElement {
  const showError = error !== null && touched && !active
  return (
    <div>
      {title !== '' ? <div className={css.title}>{title}</div> : null}
      <input
        className={classNames(css.input, {
          [css.invalid]: showError
        })}
        autoComplete={autoCompleteType}
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={({ target }) => onChange?.(target.value)}
        onFocus={onFocus}
        onBlur={onBlur}
        disabled={submitting}
      />
      {showError ? <div className={css.error}>{error}</div> : null}
    </div>
  )
}
