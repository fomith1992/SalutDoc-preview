import { FieldHandlers, FieldState } from '@salutdoc/react-components'
import classNames from 'classnames'
import React from 'react'
import css from './select-input.module.scss'

export interface SelectInputProps<T extends string | number> extends Partial<FieldState<T> & FieldHandlers<T>> {
  title?: string
  options: Array<{
    value: T
    label: string
  }>
}

export function SelectInput<T extends string | number> (
  {
    title = '',
    options,
    name,
    onChange,
    onFocus,
    onBlur,
    value,
    submitting = false,
    active = false,
    error = '',
    touched = false
  }: SelectInputProps<T>
): React.ReactElement {
  const showError = error !== null && touched && !active
  return (
    <div
      className={css.container}
    >
      {title !== '' ? <div className={css.title}>{title}</div> : null}
      <select
        className={classNames(css.input, {
          [css.invalid]: showError
        })}
        name={name}
        value={value}
        onChange={event => onChange?.(event.target.value as T)}
        onFocus={onFocus}
        onBlur={onBlur}
        disabled={submitting}
      >
        {options.map((option, idx) => (
          <option
            key={idx}
            value={option.value}
          >
            {option.label}
          </option>
        ))}
      </select>
      {showError ? <div className={css.error}>{error}</div> : null}
    </div>
  )
}
