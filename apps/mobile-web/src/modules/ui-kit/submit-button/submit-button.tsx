import React from 'react'
import { Button, ButtonProps } from '../button'

export interface SubmitButtonProps extends Omit<ButtonProps, 'onPress' | 'disabled' | 'loading'> {
  valid: boolean
  submitting: boolean
  onSubmit: () => void
}

export function SubmitButton (
  { valid, submitting, onSubmit, ...rest }: SubmitButtonProps
): React.ReactElement {
  return (
    <Button
      {...rest}
      onPress={onSubmit}
      disabled={!valid || submitting}
      loading={submitting}
    />
  )
}
