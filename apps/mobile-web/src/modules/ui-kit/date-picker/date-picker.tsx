import { LocalDate } from '@js-joda/core'
import { FieldHandlers, FieldState } from '@salutdoc/react-components'
import classNames from 'classnames'
import React from 'react'
import css from './date-picker.module.scss'

export interface DatePickerProps extends Partial<FieldState<LocalDate> & FieldHandlers<LocalDate>> {
  title?: string
  placeholder?: string
  min?: LocalDate | string
  max?: LocalDate | string
}

export function DatePicker (
  {
    title = '',
    placeholder,
    min,
    max,
    name,
    value,
    onChange,
    onFocus,
    onBlur,
    submitting = false,
    active = false,
    error = '',
    touched = false
  }: DatePickerProps
): React.ReactElement {
  const showError = error !== null && touched && !active
  return (
    <div>
      {title !== '' ? <div className={css.title}>{title}</div> : null}
      <input
        className={classNames(css.input, {
          [css.invalid]: showError
        })}
        type='date'
        placeholder={placeholder}
        name={name}
        value={value?.toString()}
        min={min?.toString()}
        max={max?.toString()}
        onChange={event => {
          const value = event.target.value
          onChange?.(value === '' ? undefined : LocalDate.parse(value))
        }}
        onFocus={onFocus}
        onBlur={onBlur}
        disabled={submitting}
      />
      {showError ? <div className={css.error}>{error}</div> : null}
    </div>
  )
}
