import React from 'react'
import css from './header.module.scss'
import { ArrowIcon16 } from '@salutdoc/mobile-components/lib/images/arrow.icon-16'
import { useMaterialized } from '@salutdoc/mobile-components/lib/style/styled'
import { color } from '@salutdoc/mobile-components/lib/style/color'

interface HeaderProps {
  title?: string
  onBack?: () => void
}

export function Header (
  { onBack, title = 'Регистрация' }: HeaderProps
): React.ReactElement {
  const arrowColor = useMaterialized(color('accent'))

  return (
    <div className={css.container}>
      {onBack == null ? null : (
        <button
          className={css.backBtn}
          onClick={onBack}
        >
          <ArrowIcon16 style={{ color: arrowColor }} />
        </button>
      )}
      <div className={css.title}>
        {title}
      </div>
    </div>
  )
}
