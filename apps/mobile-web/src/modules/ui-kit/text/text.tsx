import React from 'react'
import css from './text.module.scss'

interface TextProps {
  children: React.ReactNode
}

export function Title (
  { children }: TextProps
): React.ReactElement {
  return (
    <div className={css.title}>
      {children}
    </div>
  )
}

export function Description (
  { children }: TextProps
): React.ReactElement {
  return (
    <div className={css.description}>
      {children}
    </div>
  )
}

export function Information (
  { children }: TextProps
): React.ReactElement {
  return (
    <div className={css.information}>
      {children}
    </div>
  )
}

export function SubInformation (
  { children }: TextProps
): React.ReactElement {
  return (
    <div className={css.subInformation}>
      {children}
    </div>
  )
}
