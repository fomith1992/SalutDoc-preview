import { TextButton, SubmitTextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { createFormDescriptor, SubmissionHandler } from '@salutdoc/react-components'
import React from 'react'
import * as yup from 'yup'
import { Input, MaskedInput } from '../ui-kit/input'
import { Title } from '../ui-kit/text'
import css from './login-form.module.scss'

const formSchema = yup.object({
  phoneNumber: yup.string()
    .required('Необходимо указать номер телефона!'),
  password: yup.string()
    .required('Необходимо указать пароль!')
}).required()

export type LoginFormData = yup.InferType<typeof formSchema>

const { Form, Field, Submit } = createFormDescriptor(formSchema)

export interface LoginFormProps {
  onSubmit: SubmissionHandler<LoginFormData>
  onRecoverPass: () => void
  onRegister: () => void
}

export const LoginForm = ({
  onSubmit,
  onRecoverPass,
  onRegister
}: LoginFormProps): React.ReactElement => (
  <div className={css.pageLayout} style={{ height: window.innerHeight }}>
    <div style={{ flex: 1, display: 'grid', alignItems: 'center' }}>
      <div style={{ alignSelf: 'center' }}>
        <div className={css.pageTitle}>
          <Title>Войти в аккаунт</Title>
        </div>

        <Form onSubmit={onSubmit}>
          <Field name='phoneNumber'>
            {props => (
              <div className={css.inputContainer}>
                <MaskedInput
                  mask='+7 (999) 999-99-99'
                  placeholder='Телефон'
                  autoCompleteType='username'
                  {...props}
                />
              </div>
            )}
          </Field>
          <Field name='password'>
            {props => (
              <div className={css.inputContainerNoLabel}>
                <Input
                  type='password'
                  placeholder='Пароль'
                  autoCompleteType='current-password'
                  {...props}
                />
              </div>)}
          </Field>
          <Submit>
            {props => (
              <div className={css.mainBtn}>
                <SubmitTextButton
                  type='primary'
                  size='XL'
                  {...props}
                >
                  Войти
                </SubmitTextButton>
              </div>
            )}
          </Submit>
          <div className={css.forgotPassContainer}>
            <TextButton
              onPress={onRecoverPass}
              type='link'
            >
              Забыли пароль?
            </TextButton>
          </div>
        </Form>
      </div>
    </div>
    <div className={css.inviteLinkContainer}>
      <div className={css.inviteText}>Нет аккаунта?</div>
      <SubmitTextButton
        valid
        type='link'
        onSubmit={onRegister}
      >
        Зарегистрируйтесь
      </SubmitTextButton>
    </div>

  </div>
)
