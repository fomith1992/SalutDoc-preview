import {
  useAuthenticatedUserOrNull
} from '@salutdoc/mobile-components/lib/components/user-context/user-context-provider'
import { useAuthentication } from '@salutdoc/mobile-components/lib/contexts/authentication-context'
import { SubmissionHandler } from '@salutdoc/react-components'
import { useRouter } from 'next/router'
import React, { useCallback, useEffect } from 'react'
import { getPublicConfig } from '../../config/public.config'
import { LoginForm, LoginFormData } from './login-form.view'

export const LoginContainer = (): React.ReactElement => {
  const user = useAuthenticatedUserOrNull()
  const { login } = useAuthentication()
  const router = useRouter()

  const { FRONT_URL } = getPublicConfig()

  const { uid } = router.query

  useEffect(() => {
    if (user != null) {
      router.push('/user/[userId]', `/user/${user.id}`)
    } else if (uid == null) {
      login({ page: 'login' })
    }
  }, [uid, user])

  const id = Array.isArray(uid) ? uid[0] : uid

  const handleSubmit = useCallback<SubmissionHandler<LoginFormData>>(async ({ phoneNumber, password }) => {
    if (id == null) {
      alert('Interaction id not defined')
      return {}
    }
    const response = await fetch(new URL(`/auth/v1/interaction/${id}`, FRONT_URL).href, {
      method: 'post',
      credentials: 'include',
      body: new URLSearchParams({
        phoneNumber,
        password
      })
    })
    const data = await response.json()
    if (data.ok === true) {
      window.location.replace(`/login/success?returnTo=${encodeURIComponent(data.returnTo)}`)
    } else {
      alert(data.message)
    }
    return {}
  }, [uid])

  const recoveryPassRelocate = (): void => {
    if (id != null) {
      window.location.assign(`/recover?uid=${id}`)
    } else {
      login({ page: 'recovery' })
    }
  }
  const registerRelocate = (): void => {
    if (id != null) {
      window.location.assign(`/register?uid=${id}`)
    } else {
      login({ page: 'registration' })
    }
  }

  return (
    <LoginForm
      onSubmit={handleSubmit}
      onRecoverPass={() => recoveryPassRelocate()}
      onRegister={() => registerRelocate()}
    />
  )
}
