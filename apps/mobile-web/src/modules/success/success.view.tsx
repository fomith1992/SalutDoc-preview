import { useRouter } from 'next/router'
import React from 'react'
import { Title } from '../ui-kit/text'
import css from './success.module.scss'
import { TextButton } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { SuccessIcon } from '@salutdoc/mobile-components/lib/images/success.icon'
import { ProfileCircleIcon } from '@salutdoc/mobile-components/lib/images/profile-circle.icon'

export interface PhoneVerifyViewProps {
  description: string
  iconType?: 'profile' | 'success'
}

export function SuccessView ({
  description,
  iconType = 'success'
}: PhoneVerifyViewProps): React.ReactElement {
  const router = useRouter()
  const { returnTo } = router.query

  const redirectUrl = Array.isArray(returnTo) ? returnTo[0] : returnTo

  if (redirectUrl == null) {
    return <div>Произошла ошибка</div>
  }

  return (
    <div className={css.center}>
      <div className={css.iconContainer}>
        {iconType === 'profile'
          ? <ProfileCircleIcon />
          : <SuccessIcon />}
      </div>
      <Title>
        {description}
      </Title>
      <div className={css.buttonContainer}>
        <TextButton
          onPress={() => window.location.replace(redirectUrl)}
          type='primary'
          size='L'
        >
          Продолжить
        </TextButton>
      </div>
    </div>
  )
}
