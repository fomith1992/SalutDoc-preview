import { ApolloProvider } from '@apollo/react-common'
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { empty, split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { createUploadLink } from 'apollo-upload-client'
import { getMainDefinition } from 'apollo-utilities'
import fetch from 'isomorphic-unfetch'
import withApollo from 'next-with-apollo'
import React from 'react'
import { getPublicConfig } from './config/public.config'
import introspectionQueryResultData from '@salutdoc/mobile-components/lib/gen/introspection-result'

// Export a HOC from next-with-apollo
// Docs: https://www.npmjs.com/package/next-with-apollo
export const withApolloClient = withApollo(
  ({ initialState, headers }) => {
    const { FRONT_URL } = getPublicConfig()

    const graphqlUrl = new URL('/graphql', FRONT_URL)
    const graphqlWsUrl = new URL('', graphqlUrl)
    graphqlWsUrl.protocol = graphqlUrl.protocol === 'http:' ? 'ws:' : 'wss:'

    const httpLink = createUploadLink({
      fetch,
      uri: graphqlUrl.href,
      credentials: 'include',
      headers
    })
    const subscriptionLink = process.browser ? new WebSocketLink({
      uri: graphqlWsUrl.href,
      options: {
        reconnect: true,
        lazy: true
      }
    }) : empty()
    return new ApolloClient({
      link: split(
        ({ query }) => {
          const definition = getMainDefinition(query)
          return definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        },
        subscriptionLink,
        httpLink
      ),
      cache: new InMemoryCache({
        fragmentMatcher: new IntrospectionFragmentMatcher({ introspectionQueryResultData })
      }).restore(initialState != null ? initialState : {})
    })
  },
  {
    render: ({ Page, props }) => (
      <ApolloProvider client={props.apollo}>
        <Page {...props} />
      </ApolloProvider>
    )
  }
)
