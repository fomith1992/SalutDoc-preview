import { getPublicConfig } from '../config/public.config'

export function pixelId (): string | undefined {
  return getPublicConfig().FB_PIXEL_ID
}
