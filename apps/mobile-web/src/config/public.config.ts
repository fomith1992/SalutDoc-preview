import getConfig from 'next/config'
import * as yup from 'yup'

const publicConfigSchema = yup.object({
  FRONT_URL: yup.string().required(),
  REGISTRATION_RECAPTCHA_SITEKEY: yup.string().required(),
  API_URL: yup.string().required(),
  GA_TRACKING_ID: yup.string().required(),
  FB_PIXEL_ID: yup.string()
}).required()

export type PublicConfig = yup.InferType<typeof publicConfigSchema>

export function getPublicConfig (): PublicConfig {
  return publicConfigSchema.validateSync(getConfig().publicRuntimeConfig, {
    abortEarly: false,
    stripUnknown: true
  })
}
