import { getPublicConfig } from '../config/public.config'

interface GtagParameters {
  groups?: string | string[]
  send_to?: string | string[]
  event_callback?: () => void
  event_timeout?: number
  event_category?: string
  event_label?: string
  value?: number

  [param: string]: any
}

declare global {
  interface Window {
    gtag: {
      (command: 'config', measurementId: string, parameters?: GtagParameters): void

      (command: 'event', eventName: string, parameters?: GtagParameters): void
    }
  }
}

export function trackingId (): string {
  return getPublicConfig().GA_TRACKING_ID
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export function pageview (url: string): void {
  window.gtag('config', trackingId(), {
    page_path: url
  })
}

export interface EventParameters {
  action: string
  category?: string
  label?: string
  value: number
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export function event ({ action, category, label, value }: EventParameters): void {
  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value: value
  })
}
