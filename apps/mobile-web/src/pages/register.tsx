import React from 'react'
import { Registration } from '../modules/registration/registration.container'

export default function (): React.ReactElement {
  return <Registration />
}
