import { NavigationContainer } from '@react-navigation/native'
import { RootNavigator } from '@salutdoc/mobile-components/lib/navigation/root-navigator'
import React from 'react'

export default function Index (): React.ReactElement {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  )
}
