import React from 'react'
import { SuccessView } from '../../modules/success/success.view'

export default function (): React.ReactElement {
  return (
    <SuccessView
      description='Пароль изменен'
    />
  )
}
