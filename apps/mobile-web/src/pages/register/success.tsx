import React, { useEffect } from 'react'
import { fbPixel } from '../../fbpixel/fp-pixel'
import { SuccessView } from '../../modules/success/success.view'

export default function (): React.ReactElement {
  useEffect(() => {
    fbPixel?.track('CompleteRegistration', {})
  })
  return (
    <SuccessView
      description='Ваш аккаунт создан'
      iconType='profile'
    />
  )
}
