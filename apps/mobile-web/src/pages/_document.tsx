import Document, { Head, Html, Main, NextScript } from 'next/document'
import React from 'react'
import * as gtag from '../gtag'

export default class MyDocument extends Document {
  render (): React.ReactElement {
    const GA_TRACKING_ID = gtag.trackingId()
    return (
      <Html>
        <Head>
          {/* Global Site Tag (gtag.js) - Google Analytics */}
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `
            }}
          />
          <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
          <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css' />
          <link rel='shortcut icon' href='/favicon.ico' />
        </Head>
        <body>
          {/* https://lab.laukstein.com/bug/input */}
          <script>;</script>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
