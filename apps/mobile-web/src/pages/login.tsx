import React from 'react'
import { LoginContainer } from '../modules/login/login-form.container'

export default function (): React.ReactElement {
  return <LoginContainer />
}
