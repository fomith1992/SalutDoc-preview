import React from 'react'
import { PasswordRecovery } from '../modules/password-recover/password-recover.container'

export default function (): React.ReactElement {
  return <PasswordRecovery />
}
