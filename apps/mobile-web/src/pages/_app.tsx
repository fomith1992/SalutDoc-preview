import { getDataFromTree } from '@apollo/react-ssr'
import { LoadingIndicator } from '@salutdoc/mobile-components/lib/components/loading-indicator/loading-indicator.view'
import { AuthenticatedUserProvider } from '@salutdoc/mobile-components/lib/components/user-context/user-context-provider'
import { NotificationsHost } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { defaultTheme, ThemeProvider } from '@salutdoc/mobile-components/lib/style/theme'
import { AppProps } from 'next/app'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { initialWindowSafeAreaInsets, SafeAreaProvider } from 'react-native-safe-area-context'
import { pixelId } from '../fbpixel'
import { fbPixel } from '../fbpixel/fp-pixel'
import * as gtag from '../gtag'
import { WebAuthenticationProvider } from '../modules/authentication'
import { withApolloClient } from '../with-apollo'

function MyApp (
  {
    Component,
    pageProps
  }: AppProps
): React.ReactElement {
  const router = useRouter()
  const FB_PIXEL_ID = pixelId()
  useEffect(() => {
    fbPixel?.init(FB_PIXEL_ID)
    const handleRouteChange = (url: string): void => {
      gtag.pageview(url)
      fbPixel?.pageView()
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return (
    <div style={{ height: '100vh', display: 'flex' }}>
      <SafeAreaProvider
        initialSafeAreaInsets={initialWindowSafeAreaInsets}
      >
        <ThemeProvider value={defaultTheme}>
          <WebAuthenticationProvider>
            <AuthenticatedUserProvider
              fallback={
                <div style={{ margin: 'auto' }}>
                  <LoadingIndicator visible size='enormous' />
                </div>
              }
            >
              <NotificationsHost>
                <Component {...pageProps} />
              </NotificationsHost>
            </AuthenticatedUserProvider>
          </WebAuthenticationProvider>
        </ThemeProvider>
      </SafeAreaProvider>
    </div>
  )
}

export default withApolloClient(MyApp, { getDataFromTree })
