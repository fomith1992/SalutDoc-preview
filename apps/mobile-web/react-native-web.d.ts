import 'react-native'

declare module 'react-native' {
  export interface TextProps {
    // @platform web
    href?: string
    // @platform web
    rel?: string
    // @platform web
    target?: string
    onTextLayout?: (event: NativeSyntheticEvent<TextLayoutEventData>) => void
  }

  interface TextLayoutLine {
    ascender: number
    capHeight: number
    descender: number
    height: number
    text: string
    width: number
    x: number
    xHeight: number
    y: number
  }

  /**
  * @see TextProps.onTextLayout
  */
  export interface TextLayoutEventData extends TargetedEvent {
    lines: TextLayoutLine[]
  }
}
