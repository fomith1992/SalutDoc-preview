const images = require('next-images')
const sass = require('@zeit/next-sass')
const { withPlugins } = require('next-compose-plugins')
const withTranspile = require('next-transpile-modules')
const { pick } = require('lodash')

module.exports = withPlugins([
  [sass, { // built-in sass support does not work for us for some reason :(
    // This uses css-loader v1.0.0 !!! don't be fooled by latest version options
    cssModules: true,
    cssLoaderOptions: {
      camelCase: 'dashesOnly',
      localIdentName: '[path][name]__[local]--[hash:hex:8]'
    }
  }],
  images,
  withTranspile([
    '@react-navigation/bottom-tabs',
    '@react-navigation/core',
    '@react-navigation/native',
    '@react-navigation/stack',
    'libs/mobile-components',
    'react-native-gesture-handler',
    'react-native-image-picker',
    'react-native-document-picker',
    'react-native-inappbrowser-reborn',
    'react-native-keychain',
    'react-native-safe-area-context',
    'react-native-safe-area-view',
    'react-native-screens',
    'react-native-svg',
    'react-native-webview',
    'react-native-youtube-iframe'
  ], { resolveSymlinks: true })
], {
  webpack: (config, options) => {
    if (options.isServer) {
      // otherwise SSR uses 2 instances of react for some reason
      config.externals = ['react', 'react-dom', ...config.externals]
    }
    config.resolve.alias = {
      ...(config.resolve.alias || {}),
      // Transform all direct `react-native` imports to `react-native-web`
      'react-native$': 'react-native-web'
    }
    config.resolve.extensions = [
      '.web.js',
      '.web.ts',
      '.web.tsx',
      ...config.resolve.extensions
    ]
    return config
  },
  publicRuntimeConfig: pick(process.env, [
    'FRONT_URL',
    'REGISTRATION_RECAPTCHA_SITEKEY',
    'API_URL',
    'GA_TRACKING_ID',
    'FB_PIXEL_ID'
  ]),
  redirects: () => [
    {
      source: '/m/register',
      destination: '/register',
      permanent: true
    }
  ],
  headers: () => [
    {
      source: '/.well-known/apple-app-site-association',
      headers: [
        {
          key: 'Content-Type',
          value: 'application/json'
        }
      ]
    }
  ]
})
