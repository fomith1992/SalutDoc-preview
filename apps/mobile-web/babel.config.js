module.exports = {
    presets: [
      'next/babel',
      '@babel/preset-flow' // some react-native libraries are distributed in Flow
    ],
    plugins: [
      ['react-native-web', { commonjs: true }]
    ]
}
