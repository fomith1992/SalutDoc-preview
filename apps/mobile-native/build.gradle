import org.apache.commons.io.output.TeeOutputStream

apply from: npmPlugin

pnpmRunGenerate {
    inputs.files('codegen.yml')
            .withPropertyName('codegenConfig')
            .withPathSensitivity(PathSensitivity.RELATIVE)
    inputs.files(fileTree("src") {
        include("**/*.graphql")
    })
            .withPropertyName('graphqlDocuments')
            .withPathSensitivity(PathSensitivity.RELATIVE)
    inputs.files(fileTree("$rootDir/graphql") {
        include("**/*.graphql")
    })
            .withPropertyName('graphqlSchema')
            .withPathSensitivity(PathSensitivity.RELATIVE)
    outputs.dir('gen/graphql')
            .withPropertyName('generatedSources')
    outputs.cacheIf { true }
}

pnpmRunTypecheck {
//    inputs.files(pnpmRunGenerate)
//            .withPropertyName('generatedSources')
//            .withPathSensitivity(PathSensitivity.RELATIVE)
    inputs.dir('src')
            .withPropertyName('sources')
            .withPathSensitivity(PathSensitivity.RELATIVE)
    inputs.files(configurations.npmDevelopment)
            .withPropertyName('dependencies')
            .withPathSensitivity(PathSensitivity.RELATIVE)

    doFirst {
        standardOutput = new TeeOutputStream(standardOutput,
                new FileOutputStream("$buildDir/typescript-reports/output.txt"))
    }

    outputs.file("$buildDir/typescript-reports/output.txt")
    outputs.cacheIf { true }
}

pnpmRunEslint {
//    inputs.files(pnpmRunGenerate)
//            .withPropertyName('generatedSources')
//            .withPathSensitivity(PathSensitivity.RELATIVE)
    inputs.dir('src')
            .withPropertyName('sources')
            .withPathSensitivity(PathSensitivity.RELATIVE)
    inputs.files(configurations.npmDevelopment)
            .withPropertyName('dependencies')
            .withPathSensitivity(PathSensitivity.RELATIVE)

    commandLine(commandLine + ['-f', 'junit', '-o', "$buildDir/eslint-reports/junit.xml"])

    outputs.file("$buildDir/eslint-reports/junit.xml")
    outputs.cacheIf { true }
}

check.dependsOn pnpmRunEslint, pnpmRunTypecheck

task androidBundleInstall(type: Exec) {
    workingDir "$projectDir/android"
    executable "bundle"
    args "install"

    inputs.file "$projectDir/android/Gemfile"
}

task androidFastlaneDeploy(type: Exec) {
    dependsOn androidBundleInstall, build

    workingDir "$projectDir/android"
    executable 'bundle'

    doFirst {
        args 'exec', 'fastlane', 'deploy',
                "upload_key_password:${project.property('android.upload.key.password')}",
                "upload_keystore_password:${project.property('android.upload.keystore.password')}",
                "upload_json_key:${project.property('android.upload.json_key')}"
    }
}

task androidFastlaneProductionApk(type: Exec) {
    dependsOn androidBundleInstall, build

    workingDir "$projectDir/android"
    executable 'bundle'

    doFirst {
        args 'exec', 'fastlane', 'apkProd',
                "upload_key_password:${project.property('android.upload.key.password')}",
                "upload_keystore_password:${project.property('android.upload.keystore.password')}",
                "upload_json_key:${project.property('android.upload.json_key')}"
    }
}

task androidFastlaneTestingApk(type: Exec) {
    dependsOn androidBundleInstall, build

    workingDir "$projectDir/android"
    executable 'bundle'

    doFirst {
        args 'exec', 'fastlane', 'apkTest',
                "upload_key_password:${project.property('android.upload.key.password')}",
                "upload_keystore_password:${project.property('android.upload.keystore.password')}",
                "upload_json_key:${project.property('android.upload.json_key')}"
    }
}

task androidFastlaneProductionBundle(type: Exec) {
    dependsOn androidBundleInstall, build

    workingDir "$projectDir/android"
    executable 'bundle'

    doFirst {
        args 'exec', 'fastlane', 'bundleProd',
                "upload_key_password:${project.property('android.upload.key.password')}",
                "upload_keystore_password:${project.property('android.upload.keystore.password')}",
                "upload_json_key:${project.property('android.upload.json_key')}"
    }
}

task androidFastlaneTestingBundle(type: Exec) {
    dependsOn androidBundleInstall, build

    workingDir "$projectDir/android"
    executable 'bundle'

    doFirst {
        args 'exec', 'fastlane', 'bundleTest',
                "upload_key_password:${project.property('android.upload.key.password')}",
                "upload_keystore_password:${project.property('android.upload.keystore.password')}",
                "upload_json_key:${project.property('android.upload.json_key')}"
    }
}

task iosBundleInstall(type: Exec) {
    workingDir "$projectDir/ios"
    executable "bundle"
    args "install"

    inputs.file "$projectDir/ios/Gemfile"
}

task iosFastlaneProductionAlpha(type: Exec) {
    dependsOn iosBundleInstall, build

    workingDir "$projectDir/ios"
    executable 'bundle'

    doFirst {
        args 'exec', 'fastlane', 'production_alpha',
            "build_number:${project.property('ios.build.number')}"
    }
}
