import 'react-native-get-random-values' // polyfill for uuid
import { AppRegistry } from 'react-native'
import { App } from './src/app'
import { name as appName } from './app.json'

AppRegistry.registerComponent(appName, () => App)
