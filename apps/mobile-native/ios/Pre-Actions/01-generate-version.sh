#!/bin/sh

VERSION_TAG=$(git -C $SRCROOT describe --abbrev=0 --first-parent --match v\*)
MARKETING_VERSION=${VERSION_TAG:1}
echo "MARKETING_VERSION = $MARKETING_VERSION" > $SRCROOT/Configs/Version.generated.xcconfig
