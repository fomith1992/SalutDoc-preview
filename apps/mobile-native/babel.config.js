const path = require('path')

module.exports = {
  presets: [
    'module:@haul-bundler/babel-preset-react-native'
  ],
  plugins: [
    'babel-plugin-transform-inline-environment-variables'
  ],
  overrides: [
    {
      test: (filename) => filename.includes(`node_modules${path.sep}@react-native-community${path.sep}picker${path.sep}`),
      plugins: ['@babel/plugin-transform-modules-commonjs']
    }
  ]
}
