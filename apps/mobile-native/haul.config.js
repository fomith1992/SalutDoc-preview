import { makeConfig, withPolyfills } from '@haul-bundler/preset-0.60'
import loadEnv from 'dotenv-load'
import path from 'path'

loadEnv()

// these libraries need to be precompiled
const whitelistModules = [
  'react-native',
  'react-native-appsflyer',
  'react-native-device-info',
  'react-native-fbsdk-next',
  'react-native-gesture-handler',
  'react-native-image-picker',
  'react-native-document-picker',
  'react-native-inappbrowser-reborn',
  'react-native-keychain',
  'react-native-reanimated',
  'react-native-safe-area-context',
  'react-native-safe-area-view',
  'react-native-screens',
  'react-native-svg',
  'react-native-tracking-transparency',
  'react-native-push-notification',
  'react-native-webview',
  'react-native-youtube-iframe',
  'rn-fetch-blob',
  '@react-native-async-storage/async-storage',
  // all react-navigation pieces
  '@react-navigation',
  // all @react-native-community libs
  '@react-native-community'
]

function includesSegment (a, b) {
  return a.includes(`/${b}/`.replace(/[/]/g, path.sep))
}

export default makeConfig({
  server: {
    host: '0.0.0.0'
  },
  bundles: {
    index: {
      entry: withPolyfills('./index'),
      transform({ bundleName, env, runtime, config }) {
        runtime.logger.info(
          `Altering Webpack config for bundle ${bundleName} for ${env.platform}`
        )
        // replacing this mindblowing non-working regexp with easy to understand working one
        // /node_modules(?!.*[\/\\](react|@react-navigation|@react-native-community|@expo|pretty-format|@haul-bundler|metro))/
        delete config.module.rules[1].exclude
        // only include our sources and whitelisted libraries from node_modules
        config.module.rules[1].include = (path) =>
          !includesSegment(path, 'node_modules') ||
          whitelistModules.some(mod => includesSegment(path, `node_modules/${mod}`))
      }
    }
  }
})
