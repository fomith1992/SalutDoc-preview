import { NavigationContainerRef } from '@react-navigation/native'
import React, { RefObject, useEffect } from 'react'
import messaging from '@react-native-firebase/messaging'
import useAsync from '../../../libs/mobile-components/node_modules/react-use/lib/useAsync'
import {
  useOnNotificationOpenedApp,
  useOnNotificationShowInAppMessage
} from '@salutdoc/mobile-components/lib/modules/push-notifications'
import { useAuthenticatedUserOrNull } from '@salutdoc/mobile-components/lib/components/user-context/user-context-provider'

interface FirebaseNotificationsContainerProps {
  children: React.ReactElement
  navigationRef: RefObject<NavigationContainerRef>
}

export function FirebaseNotificationsContainer ({
  children,
  navigationRef
}: FirebaseNotificationsContainerProps): React.ReactElement {
  const user = useAuthenticatedUserOrNull()

  useOnNotificationOpenedApp(navigationRef, user?.role ?? null)
  useOnNotificationShowInAppMessage(navigationRef)

  useEffect(() => messaging().onMessage((message) => {
    console.log('Message', message)
  }), [])
  useAsync(async () => {
    const authStatus = await messaging().requestPermission()
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL

    if (enabled) {
      console.log('Authorization status:', authStatus)
    }
  }, [])
  useAsync(async () => {
    console.log('Token', await messaging().getToken())
  }, [])
  useEffect(() => messaging().onTokenRefresh((token) => {
    console.log('Token refreshed', token)
  }), [])
  return children
}
