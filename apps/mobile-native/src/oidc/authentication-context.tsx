import { AuthenticationProvider, LoginProps } from '@salutdoc/mobile-components/lib/contexts/authentication-context'
import React, { useCallback, useMemo } from 'react'
import { authorize, revoke } from 'react-native-app-auth'
import { useCredentialsRef, useUpdateCredentials } from '@salutdoc/mobile-components/lib/modules/oidc/credentials-provider'
import { oidcConfig } from './oidc-config'

export function OidcAuthenticationProvider ({ children }: React.PropsWithChildren<{}>): React.ReactElement {
  const credentialsRef = useCredentialsRef()
  const updateCredentials = useUpdateCredentials()

  const login = useCallback(async function login ({ page }: LoginProps): Promise<void> {
    try {
      const result = await authorize({
        ...oidcConfig,
        additionalParameters: {
          page,
          prompt: 'login'
        }
      })
      updateCredentials({
        idToken: result.idToken,
        accessToken: result.accessToken,
        accessTokenExpiresAt: result.accessTokenExpirationDate,
        refreshToken: result.refreshToken
      })
    } catch (e) {
      console.log(e)
    }
  }, [updateCredentials])

  const logout = useCallback(async function logout (): Promise<void> {
    const idToken = credentialsRef.current?.idToken
    if (idToken == null) {
      throw new Error('not authenticated')
    } else {
      updateCredentials(null)
      await revoke(oidcConfig, {
        tokenToRevoke: idToken
      })
    }
  }, [credentialsRef])

  const value = useMemo(() => ({
    login,
    logout
  }), [login, logout])

  return (
    <AuthenticationProvider
      value={value}
    >
      {children}
    </AuthenticationProvider>
  )
}
