import { useMemo } from 'react'
import { refresh } from 'react-native-app-auth'
import { TokenProvider } from '../apollo/init-apollo'
import { useCredentialsRef, useUpdateCredentials } from '@salutdoc/mobile-components/lib/modules/oidc/credentials-provider'
import { oidcConfig } from './oidc-config'

const refreshThreshold = 30000 // 30sec

export function useRefreshingTokenProvider (): TokenProvider {
  const credentialsRef = useCredentialsRef()
  const updateCredentials = useUpdateCredentials()

  return useMemo(() => {
    console.debug('creating refreshing token provider')
    let refreshPromise: Promise<void> | null = null

    async function refreshTokenIfNeeded (): Promise<void> {
      const credentials = credentialsRef.current
      if (credentials != null && Date.parse(credentials.accessTokenExpiresAt) - Date.now() < refreshThreshold) {
        if (refreshPromise == null) {
          console.debug('refreshing access token')
          refreshPromise = refresh(oidcConfig, {
            refreshToken: credentials.refreshToken
          }).then(result => {
            console.debug('access token refreshed')
            updateCredentials({
              idToken: result.idToken,
              accessToken: result.accessToken,
              accessTokenExpiresAt: result.accessTokenExpirationDate,
              refreshToken: result.refreshToken ?? credentials.refreshToken
            })
          }).catch(async error => {
            console.error('Failed to refresh access token', error)
          }).finally(() => {
            refreshPromise = null
          })
        }
        await refreshPromise
      }
    }

    return async function refreshingTokenProvider () {
      await refreshTokenIfNeeded()
      const credentials = credentialsRef.current
      return credentials?.accessToken ?? null
    }
  }, [credentialsRef, updateCredentials])
}
