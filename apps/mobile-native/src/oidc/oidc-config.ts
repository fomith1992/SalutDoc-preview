import { AuthConfiguration } from 'react-native-app-auth'
import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'

const issuer: string = expectNonNil(process.env.FRONT_URL)
const authUrl: string = expectNonNil(process.env.AUTH_FRONT_URL)

export const oidcConfig: AuthConfiguration = {
  issuer,
  clientId: 'mobile-native',
  scopes: ['openid'],
  serviceConfiguration: {
    authorizationEndpoint: `${authUrl}auth/v1/oidc/auth`,
    tokenEndpoint: `${authUrl}auth/v1/oidc/token`,
    revocationEndpoint: `${authUrl}auth/v1/oidc/session/end`
  },
  redirectUrl: 'com.salutdoc.mobile://auth/callback',
  dangerouslyAllowInsecureHttpRequests: process.env.NODE_ENV === 'development'
}
