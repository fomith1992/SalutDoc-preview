import introspectionQueryResultData from '@salutdoc/mobile-components/lib/gen/introspection-result'
import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'
import {
  defaultDataIdFromObject,
  InMemoryCache,
  IntrospectionFragmentMatcher,
  NormalizedCacheObject
} from 'apollo-cache-inmemory'
import { ApolloClient, Resolvers } from 'apollo-client'
import { from, split } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { WebSocketLink } from 'apollo-link-ws'
import { createUploadLink } from 'apollo-upload-client'
import { getMainDefinition } from 'apollo-utilities'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import URI from 'urijs'

const graphqlUri = URI(expectNonNil(process.env.FRONT_URL)).path('/graphql')

const graphqlUrl = graphqlUri.href()
const graphqlWsUrl = graphqlUri.scheme(
  graphqlUri.scheme() === 'http' ? 'ws' : 'wss'
).href()

export type TokenProvider = () => Promise<string | null>

const createApolloCache = (): InMemoryCache => new InMemoryCache({
  fragmentMatcher: new IntrospectionFragmentMatcher({ introspectionQueryResultData }),
  cacheRedirects: {
    Query: {
      userById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'User', id: args.id }),
      communityById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Community', id: args.id }),
      postById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Post', id: args.id }),
      commentById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Comment', id: args.id }),
      workById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Article', id: args.id }),
      questionById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Question', id: args.id }),
      answerById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Answer', id: args.id }),
      messageById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Message', id: args.id }),
      consultationById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'Consultation', id: args.id }),
      consultationStartedById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'ConsultationStarted', id: args.id }),
      consultationFinishedById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'ConsultationFinished', id: args.id }),
      consultationReportAddedById: (_, args, { getCacheKey }) =>
        getCacheKey({ __typename: 'ConsultationReportAdded', id: args.id })
    }
  },
  dataIdFromObject: (value: any) => {
    switch (value.__typename) {
      case 'UserFollow': {
        const followerId = value.follower?.id
        const followeeId = value.followee?.id
        if (typeof followerId !== 'string' || typeof followeeId !== 'string') {
          return null
        } else {
          return `UserFollow:${followerId}:${followeeId}`
        }
      }
      case 'CommunityMembership': {
        const userId = value.user?.id
        const communityId = value.community?.id
        if (typeof userId !== 'string' || typeof communityId !== 'string') {
          return null
        } else {
          return `CommunityMembership:${userId}:${communityId}`
        }
      }
      case 'Dialog':
      case 'ConsultationChat': {
        const chatId = value.id
        if (typeof chatId !== 'string') {
          return null
        } else {
          return `Chat:${chatId}`
        }
      }
      default:
        return defaultDataIdFromObject(value)
    }
  }
})

const resolvers: Resolvers = {
  Message: {
    sending: () => {
      // all messages from server are already sent
      return false
    }
  }
}

export type Apollo = {
  client: ApolloClient<NormalizedCacheObject>
  subscriptionClient: SubscriptionClient
}

export function initApollo (tokenProvider: TokenProvider): Apollo {
  const httpLink = createUploadLink({
    fetch,
    uri: graphqlUrl
  })

  const errorLink = onError(({ operation, graphQLErrors, networkError }) => {
    graphQLErrors?.forEach(error => {
      console.error(
        `[GraphQL error]: Operation ${operation.operationName} with variables`,
        operation.variables,
        error
      )
    })
    if (networkError != null) {
      console.error(
        `[GraphQL network error]: ${operation.operationName} with variables`,
        operation.variables,
        networkError
      )
    }
  })

  const subscriptionClient = new SubscriptionClient(graphqlWsUrl, {
    reconnect: true,
    connectionParams: async () => {
      const accessToken = await tokenProvider()
      if (accessToken == null) {
        return {}
      } else {
        return { accessToken }
      }
    },
    connectionCallback: (error, result) => {
      if (error != null) {
        console.error('[GraphQL WS] Subscription connection failed', error)
      } else {
        console.log('[GraphQL WS] Subscription connected', result)
      }
    }
  })
  subscriptionClient.onConnected((...args) => {
    console.log('[GraphQL WS] WebSocket connected', ...args)
  })
  subscriptionClient.onConnecting((...args) => {
    console.log('[GraphQL WS] WebSocket connecting', ...args)
  })
  subscriptionClient.onDisconnected((...args) => {
    console.log('[GraphQL WS] WebSocket disconnected', ...args)
  })
  subscriptionClient.onReconnecting((...args) => {
    console.log('[GraphQL WS] WebSocket reconnecting', ...args)
  })
  subscriptionClient.onReconnected((...args) => {
    console.log('[GraphQL WS] WebSocket reconnected', ...args)
  })
  subscriptionClient.onError((...args) => {
    console.error('[GraphQL WS] WebSocket Error', ...args)
  })

  const subscriptionLink = new WebSocketLink(subscriptionClient)

  const termLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query)
      return definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
    },
    subscriptionLink,
    httpLink
  )

  const authLink = setContext(async (_, { headers }) => {
    const token = await tokenProvider()
    if (token == null) {
      return {}
    } else {
      return {
        headers: {
          ...headers,
          authorization: `Bearer ${token}`
        }
      }
    }
  })

  const client = new ApolloClient({
    link: from([errorLink, authLink, termLink]),
    cache: createApolloCache(),
    resolvers
  })

  return {
    client,
    subscriptionClient
  }
}
