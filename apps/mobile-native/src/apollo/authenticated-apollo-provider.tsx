import { ApolloProvider } from '@apollo/react-hooks'
import React, { useMemo } from 'react'
import useAsync from 'react-use/lib/useAsync'
import { useCredentials } from '@salutdoc/mobile-components/lib/modules/oidc/credentials-provider'
import { useRefreshingTokenProvider } from '../oidc/refreshing-token-provider'
import { initApollo } from './init-apollo'

export interface AuthenticatedApolloProviderProps {
  children: React.ReactNode
  fallback?: React.ReactNode
}

/**
 * Must be used inside credentials context
 */
export function AuthenticatedApolloProvider (
  {
    // fallback,
    children
  }: AuthenticatedApolloProviderProps
): React.ReactElement {
  const refreshingTokenProvider = useRefreshingTokenProvider()
  const { client, subscriptionClient } = useMemo(() => initApollo(refreshingTokenProvider), [refreshingTokenProvider])
  const idToken = useCredentials()?.idToken
  /* const { loading } = */ useAsync(async () => {
    try {
      subscriptionClient.close(false, true) // websocket reconnect
      await client.clearStore()
      await client.resetStore()
    } catch (e) {
      console.error('Failed to clear Apollo cache', e)
    }
  }, [idToken])
  /* if (loading) {
    return <>{fallback}</>
  } */
  return (
    <ApolloProvider client={client}>
      {children}
    </ApolloProvider>
  )
}
