import appsFlyer from 'react-native-appsflyer'
import { expectNonNil } from '@salutdoc/utils/lib/fp/maybe'

appsFlyer.initSdk(
  {
    devKey: expectNonNil(process.env.APPSFLYER_DEV_KEY),
    appId: expectNonNil(process.env.APPSFLYER_APP_ID),
    isDebug: process.env.NODE_ENV !== 'production'
  },
  result => {
    console.info('[AppsFlyer] SDK initialized', result)
    appsFlyer.stop(true, res => {
      console.info('[AppsFlyer] stopped', res)
    })
  },
  error => {
    console.error('[AppsFlyer] SDK init failed', error)
  }
)
