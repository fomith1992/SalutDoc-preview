import React, { useEffect } from 'react'
import RNBootSplash from 'react-native-bootsplash'

// SplashScreen is global, so no need for context
let hideCounter = 0

export const HideSplashScreen: React.FunctionComponent = ({ children }) => {
  useEffect(() => {
    if (hideCounter++ === 0) {
      RNBootSplash.hide({ fade: true }).catch(err => {
        console.error('[RNBootSplash] Failed to hide splash screen', err)
      })
    }
    return () => {
      if (--hideCounter === 0) {
        RNBootSplash.show({ fade: false }).catch(err => {
          console.error('[RNBootSplash] Failed to show splash screen', err)
        })
      }
    }
  })
  return <>{children}</>
}
