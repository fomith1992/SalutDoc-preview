import React, { useCallback, useContext, useEffect, useRef, useState } from 'react'
import * as Keychain from 'react-native-keychain'
import useAsync from 'react-use/lib/useAsync'
import * as rt from 'runtypes'
import * as rx from 'rxjs'
import { BehaviorSubject } from 'rxjs'
import * as $ from 'rxjs/operators'

const credentialsSchema = rt.Record({
  idToken: rt.String,
  accessToken: rt.String,
  accessTokenExpiresAt: rt.String,
  refreshToken: rt.String
})

export type Credentials = rt.Static<typeof credentialsSchema>

export interface CredentialsProviderProps {
  fallback?: React.ReactNode
  children: React.ReactNode
}

export interface CredentialsContext {
  credentials$: BehaviorSubject<Credentials | null>
}

const credentialsContext = React.createContext<CredentialsContext | null>(null)

function useRequiredCredentialsContext (): CredentialsContext {
  const ctx = useContext(credentialsContext)
  if (ctx == null) {
    throw new Error('Credentials context not initialized')
  }
  return ctx
}

export type UpdateCredentialsFn = (credentials: Credentials | null) => void

export function useUpdateCredentials (): UpdateCredentialsFn {
  const { credentials$ } = useRequiredCredentialsContext()
  return useCallback(credentials => {
    credentials$.next(credentials)
  }, [credentials$])
}

export function useCredentials (): Credentials | null {
  const { credentials$ } = useRequiredCredentialsContext()
  const [credentials, setCredentials] = useState(credentials$.getValue())
  useEffect(() => {
    const subscription = credentials$.subscribe(setCredentials)
    return () => subscription.unsubscribe()
  }, [credentials$])
  return credentials
}

export function useCredentialsRef (): React.RefObject<Credentials> {
  const { credentials$ } = useRequiredCredentialsContext()
  const credentialsRef = useRef(credentials$.getValue())
  useEffect(() => {
    const subscription = credentials$.subscribe(credentials => {
      credentialsRef.current = credentials
    })
    return () => subscription.unsubscribe()
  }, [credentials$])
  return credentialsRef
}

export function CredentialsProvider ({ fallback, children }: CredentialsProviderProps): React.ReactElement {
  const credentials$ = useRef(new BehaviorSubject<Credentials | null>(null)).current
  const { loading } = useAsync(async () => {
    try {
      const result = await Keychain.getGenericPassword()
      if (result !== false) {
        credentials$.next(credentialsSchema.check(JSON.parse(result.password)))
      }
    } catch (e) {
      console.error('Failed to load credentials', e)
    }
  })
  useEffect(() => {
    if (loading) return undefined
    const subscription = credentials$.pipe(
      $.skip(1),
      $.concatMap(credentials => rx.defer(async () => {
        try {
          if (credentials == null) {
            await Keychain.resetGenericPassword()
          } else {
            credentialsSchema.check(credentials)
            if (await Keychain.setGenericPassword('salutdoc', JSON.stringify(credentials)) === false) {
              console.error('Failed to store credentials')
            }
          }
        } catch (e) {
          console.error('Failed to store credentials', e)
        }
      }))
    ).subscribe()
    return () => subscription.unsubscribe()
  }, [loading])
  if (loading) {
    return <>{fallback}</>
  }
  return (
    <credentialsContext.Provider
      value={{ credentials$ }}
    >
      {children}
    </credentialsContext.Provider>
  )
}
