import { LinkingOptions, NavigationContainer, NavigationContainerRef, PathConfig } from '@react-navigation/native'
import { AnalyticsContextProvider } from '@salutdoc/mobile-components/lib/analytics/analytics-context'
import { sendScreenChangeEvent } from '@salutdoc/mobile-components/lib/analytics/facebook/screen-change'
import {
  AuthenticatedUserProvider
} from '@salutdoc/mobile-components/lib/components/user-context/user-context-provider'
import { ConnectionStateGate } from '@salutdoc/mobile-components/lib/gates/connection-state-gate'
import { VersionGate } from '@salutdoc/mobile-components/lib/gates/version-gate'
import { MessengerContextProvider } from '@salutdoc/mobile-components/lib/modules/messenger'
import { NotificationsHost, PortalHost } from '@salutdoc/mobile-components/lib/modules/ui-kit'
import { RootNavigator } from '@salutdoc/mobile-components/lib/navigation/root-navigator'
import {
  PushNotificationTargetRegistrator
} from '@salutdoc/mobile-components/lib/modules/push-notifications'
import {
  AnalyticsEnabledProvider
} from '@salutdoc/mobile-components/lib/modules/analytics-enabled'
import { ConnectionFailedScreen } from '@salutdoc/mobile-components/lib/screens/connection-failed.screen'
import { InvalidVersionScreen } from '@salutdoc/mobile-components/lib/screens/invalid-version.screen'
import { RootStoreProvider } from '@salutdoc/mobile-components/lib/stores/root.store'
import { defaultTheme, ThemeProvider } from '@salutdoc/mobile-components/lib/style/theme'
import { initializeSounds } from '@salutdoc/mobile-components/lib/utils/play-sounds/local-sound-file.map'
import { expectDefined } from '@salutdoc/react-components'
import React, { useEffect } from 'react'
import { StatusBar } from 'react-native'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { AuthenticatedApolloProvider } from './apollo/authenticated-apollo-provider'
import './appsflyer'
import { CredentialsProvider } from '@salutdoc/mobile-components/lib/modules/oidc/credentials-provider'
import { OidcAuthenticationProvider } from './oidc/authentication-context'
import { HideSplashScreen } from './splash-screen'
import { FirebaseNotificationsContainer } from './firebase-notifications-container'

export function App (): React.ReactElement {
  const navigationRef = React.useRef<NavigationContainerRef>(null)
  const routeNameRef = React.useRef<string>()

  useEffect(initializeSounds, [])
  const config: PathConfig = {
    screens: {
      Search: 'search',
      Feed: 'feed',
      PostComments: 'post/:postId',
      QuestionFull: 'question/:id',
      Work: 'work/:workId'
    }
  }
  const appCustomScheme = expectDefined(process.env.APP_CUSTOM_SCHEME)
  const mobileHostName = expectDefined(process.env.MOBILE_HOST_NAME)
  const linking: LinkingOptions = {
    prefixes: [`https://${mobileHostName}/`, `${appCustomScheme}://`],
    config
  }
  return (
    <SafeAreaProvider>
      <ThemeProvider value={defaultTheme}>
        <StatusBar
          animated
          backgroundColor='#fff'
          barStyle='dark-content'
        />
        <NavigationContainer
          ref={navigationRef}
          onStateChange={() => {
            const previousRouteName = routeNameRef.current
            const currentRouteName = navigationRef.current?.getCurrentRoute()?.name
            sendScreenChangeEvent(previousRouteName, currentRouteName)
            routeNameRef.current = currentRouteName
          }}
          linking={linking}
        >
          <CredentialsProvider>
            <OidcAuthenticationProvider>
              <ConnectionStateGate
                failedConnectionScreen={<ConnectionFailedScreen />}
              >
                <VersionGate
                  invalidScreen={<HideSplashScreen><InvalidVersionScreen /></HideSplashScreen>}
                  errorConnectServerScreen={<HideSplashScreen><ConnectionFailedScreen /></HideSplashScreen>}
                >
                  <AuthenticatedApolloProvider>
                    <AuthenticatedUserProvider>
                      <AnalyticsContextProvider>
                        <RootStoreProvider>
                          <PortalHost>
                            <NotificationsHost>
                              <MessengerContextProvider
                                navigationRef={navigationRef}
                              >
                                <FirebaseNotificationsContainer
                                  navigationRef={navigationRef}
                                >
                                  <HideSplashScreen>
                                    <AnalyticsEnabledProvider
                                      fallback={<></>}
                                    >
                                      <PushNotificationTargetRegistrator />
                                      <RootNavigator />
                                    </AnalyticsEnabledProvider>
                                  </HideSplashScreen>
                                </FirebaseNotificationsContainer>
                              </MessengerContextProvider>
                            </NotificationsHost>
                          </PortalHost>
                        </RootStoreProvider>
                      </AnalyticsContextProvider>
                    </AuthenticatedUserProvider>
                  </AuthenticatedApolloProvider>
                </VersionGate>
              </ConnectionStateGate>
            </OidcAuthenticationProvider>
          </CredentialsProvider>
        </NavigationContainer>
      </ThemeProvider>
    </SafeAreaProvider>
  )
}
