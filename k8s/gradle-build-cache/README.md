### How to install gradle build cache:

```shell script
kubectl apply -f ./namespace.yaml
kubectl apply -f ./configmap.yaml
kubectl apply -f ./persistent-volume.yaml
helm repo add slamdev https://slamdev.github.io/helm-charts/
helm install --namespace gradle-build-cache -f ./gradle-build-cache-values.yaml gradle-build-cache slamdev/gradle-cache 
```
