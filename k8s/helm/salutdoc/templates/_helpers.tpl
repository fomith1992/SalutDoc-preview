{{- define "image.tag" -}}
{{- .Values.image.tag | default .Values.global.image.tag -}}
{{- end -}}

{{- define "image" -}}
{{- .Values.image.repository -}}:{{- include "image.tag" . -}}
{{- end -}}

{{- define "frontUrl" -}}
{{- .Values.frontUrl | default .Values.global.frontUrl -}}
{{- end -}}
