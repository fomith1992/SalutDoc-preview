## Install cert-manager
Documentation: 
* https://cert-manager.io/docs/installation/kubernetes/#installing-with-helm
* https://cert-manager.io/docs/configuration/acme/
```shell script
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install -n cert-manager --set installCRDs=true cert-manager jetstack/cert-manager
kubectl apply -f ./cert-manager
```

## Install nginx-ingress-controller
Documentation: 
* https://github.com/bitnami/charts/tree/master/bitnami/nginx-ingress-controller
```shell script
kubectl create namespace nginx-ingress-controller
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install -n nginx-ingress-controller nginx-ingress-controller bitnami/nginx-ingress-controller
```

## Install strimzi
Documentation: 
* https://github.com/strimzi/strimzi-kafka-operator/tree/master/helm-charts/helm3/strimzi-kafka-operator
```shell script
kubectl create namespace strimzi
helm repo add strimzi https://strimzi.io/charts/
helm repo update
helm install -n strimzi --set watchAnyNamespace='true' kafka-operator strimzi/strimzi-kafka-operator
```


## Install AKHQ
https://github.com/tchiotludo/akhq#running-in-kubernetes-using-a-helm-chart

```shell script
helm repo add akhq https://akhq.io/
helm repo update
kubectl create namespace akhq
helm upgrade -i -n akhq -f akhq/values.yaml -f akhq/values.$ENV.yaml akhq akhq/akhq
kubectl apply -f ./akhq/resources
```

## Add GitLab registry credentials
https://gitlab.com/groups/salutdoc/-/settings/repository/deploy_token/create
```shell script
kubectl create secret docker-registry regcred \
  --docker-server=<your-registry-server> \
  --docker-username=<your-name> \
  --docker-password=<your-pword>
```

## Create ConfigMaps with Yandex Cloud root CA
```shell script
kubectl create configmap yandex-cloud-ca --from-file=CA.pem
kubectl create configmap mongo-keystore --from-file=mongo-keystore.jks
```

## Install Sealed Secrets controller
```shell script
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
helm repo update
helm upgrade -i -n kube-system -f ./sealed-secrets/values.yaml sealed-secrets sealed-secrets/sealed-secrets
```

## Install Prometheus
https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/
```shell script
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
kubectl create namespace prometheus
helm upgrade -i -n prometheus -f ./prometheus/values.yaml -f ./prometheus/values.$ENV.yaml prometheus prometheus-community/kube-prometheus-stack
kubectl apply -f ./prometheus/resources
``` 

## Install Grafana
https://github.com/bitnami/charts/tree/master/bitnami/grafana-operator
```shell script
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
kubectl create namespace grafana
helm upgrade -i -n grafana -f ./grafana/values.yaml -f ./grafana/values.$ENV.yaml grafana bitnami/grafana-operator
kubectl apply -f ./grafana/resources
```

## Install Loki and Promtail
https://github.com/grafana/helm-charts/tree/main/charts/loki-stack
```shell script
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
kubectl create ns loki
helm upgrade -i -n loki loki grafana/loki-stack
```
