#  Инструкция по запуску проекта под ОС Windows

### 1. Установите node.js (Предпочтительно ставить LTS версию) [Скачать](https://nodejs.org/en/download/)
Обязательно поставить галочку напротив "Automatically install the necessary tools. Note that this will also install Chocolatey. The script will pop-up in a new window after the installation completes." как показано на снимке ниже:

![nodejs-install](./docs/nodejs-install.png)

### 2. Установите JDK 11 [Скачать](https://github.com/corretto/corretto-11/releases)

### 3. Установите PNPM, он используется вместо npm. `npm install -g pnpm` 

### 4. Установите Python v2.7.17 [Скачать](https://www.python.org/downloads/release/python-2717/)
Пропишите путь к Python v2.7 в переменную окружения Path, вместо Python v3.9 (если есть).

### 5. Установите [Android Studio](https://developer.android.com/studio/index.html) 
Выберите "пользовательскую" установку, когда вам будет предложено выбрать тип установки.
Убедитесь, что флажки рядом со всеми следующими пунктами установлены  
:white_check_mark: Android SDK  
:white_check_mark: Android SDK Platform  
:white_check_mark: Performance  
:white_check_mark: Android Virtual Device    

![android-studio](./docs/android-studio.png)

#### Откройте SDK Manager и установите необходимые компоненты:
Выберите вкладку "платформы SDK", а затем установите флажок рядом с надписью "показать сведения о пакете" в правом нижнем углу.
Найдите и разверните Android 9 (Pie) запись, а затем убедитесь, что следующие элементы отмечены:
:white_check_mark: Android SDK Platform 28
:white_check_mark: Intel x86 Atom_64 System Image или Google APIs Intel x86 Atom System Image
Затем выберите вкладку "SDK Tools" и установите флажок рядом с надписью "Show Package Details" здесь же. Найдите и разверните запись "Android SDK Build-Tools", а затем убедитесь, что `28.0.3` выбрана.
Наконец, нажмите кнопку "Применить", чтобы загрузить и установить Android SDK и связанные с ним инструменты сборки.

#### Откройте AVD Manager и создайте новый AVD.
Выберите Пункт **"Create Virtual Device"**, затем выберите любой телефон из списка и нажмите кнопку **"Далее"**, а затем выберите **Pie API Level 28.** нажмите кнопку **"Далее"**, а затем **"Готово"**, чтобы создать свой AVD. 
Теперь можно нажать на значок **Play**, чтобы запустить Ваш виртуальный девайс.

### 6. Настройте переменную окружения ANDROID_HOME
Откройте системную панель в разделе Система и безопасность на панели управления Windows, а затем нажмите кнопку Изменить настройки.
Откройте вкладку Дополнительно и нажмите на переменные среды. Нажмите на кнопку Создать.
чтобы создать новую **ANDROID_HOME** пользовательскую переменную, указывающую на путь к вашему Android SDK:
:white_check_mark: SDK устанавливается по умолчанию в следующем расположении: `c:\Users\YOUR_USERNAME\AppData\Local\Android\Sdk`
Вы можете найти фактическое местоположение SDK в диалоговом окне Android Studio, **"Preferences", Appearance & Behavior → System Settings → Android SDK.**

### 7. Добавьте инструменты платформы в **Path**
Откройте системную панель в разделе **Система и безопасность** на панели управления Windows, а затем нажмите кнопку **Изменить настройки.** Откройте вкладку **Дополнительно** и нажмите на **переменные среды**. Выберите переменную **Path** и нажмите кнопку **Изменить.** Нажмите кнопку **Создать** и добавьте путь к **platform-tools** в список.
:white_check_mark: Расположение этой папки по умолчанию: `c:\Users\YOUR_USERNAME\AppData\Local\Android\Sdk\platform-tools`

### Типичные сценарии
#### Сборка всех модулей проекта
```shell script
./gradlew clean build
```  

#### Сборка и запуск Android приложения
```shell script
./gradlew clean apps:mobile-native:build
cd apps/mobile-native
pnpm android
```

#### Сборка и запуск iOS приложения
```shell script
./gradlew clean apps:mobile-native:build
cd apps/mobile-native
pnpm pod
pnpm ios
```

#### Запуск haul
При работе с приложением всегда должен быть запущен haul - это замена стандартному сборщику metro лучше работающая в моно-репозиториях.
```shell script
cd apps/mobile-native
pnpm haul
```
Haul не поддерживает HMR (hot module reload), поэтому после внесения изменений необходимо перезагружать js bundle (дважды нажать кнопку `r` в android emulator, или `cmd+r` в iOS simulator). 

#### Генерация кода для GraphQL запросов
Вообще генерация кода для GraphQL запросов происходит при запуске `./gradlew clean apps:mobile-native:build`,
но при активном изменении GraphQL схемы и запросов бывает удобнее запустить генерацию в режиме наблюдения.
```shell script
cd libs/mobile-components
pnpm generate -- --watch
```

#### Запуск iOS приложения на нескольких симуляторах
При сборке и запуске iOS приложения есть возможность указать устройство, на котором его следует запустить:
```shell script
pnpm ios -- --simulator "<ИМЯ СИМУЛЯТОРА>"
``` 
например
```shell script
pnpm ios -- --simulator "iPhone 12"
``` 

#### Запуск Android приложения на нескольких эмуляторах
Создайте и запустите дополнительный эмулятор (см. настройку Android studio выше), затем выясните его id:
```shell script
adb devices
```
При сборке и запуске Android приложения есть возможность указать устройство, на котором его следует запустить:
```shell script
pnpm android -- --deviceId "<DEVICE ID>"
```

#### Отладка Android приложения на физическом устройстве
Для использования своего мобильного телефона подключите его к кабелю, включите: "Режим разработчика", "Отладка по USB", "Установка по USB" 

### :white_check_mark: Прежде чем делать commit
Проверьте свой код на наличие ошибок с помощью: `./gradlew clean build`, или `./gradlew clean build -x pnpmRunTestE2e` чтобы пропустить запуск e2e тестов.
